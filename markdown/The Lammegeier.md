
- [Tex the Sheepdog](#tex-the-sheepdog)
- [Konstantin the Pug](#konstantin-the-pug)
- [Jordan the Lammergeier](#jordan-the-lammergeier)
- [Snowball and the Farm](#snowball-and-the-farm)
- [Buck and the Wolf Pack](#buck-and-the-wolf-pack)
- [Todd and the Hound](#todd-and-the-hound)
- [Justin and the Rats](#justin-and-the-rats)
- [Ceasar and the Apes](#ceasar-and-the-apes)
- [Simba and the Lions](#simba-and-the-lions)

<br>
<br>

## Industrial Complexes 

|||
|-|-|
|Military|War is always last resort|
|Medical|Sanctivy/recodify informed consent|
|Censorship|Banish state media control, surveillance|
|Immigration|Enact a rational border policy|
|Injustice|End lawfare and abuse of the judicial system|
|Finance|Secure monetary freedom|
|Developmental|Restore family sovereignty|
|Academic|Return to truth-seeking and open dialogue|

<br>



## The Whales 

<br>

- [The Faculties of Education](#the-faculties-of-education) -> OWLS -> CRT in a whale 
- [The Media Machine](#the-media-machine) -> PUGS -> OATH in a whale  
- [Legacy Finance](#legacy-finance) -> SHARKS -> Profit in a whale  
- [Legacy Healthcare](#legacy-healthcare) -> RABBITS -> Compassion in a whale 
- [Big Pharma](#big-pharma) -> 800 POUND GORILLAS -> Profit in a whale 
- [Non-Government Organizations](#non-government-organizations) -> RACOONS -> Morality in a whale 
- [Foundations](#foundations) -> ELK -> Compassion in a whale 
- [Big Tech](#big-tech) -> BEARS -> Innovation in a whale 
- [The Government Machine](#the-government-machine) -> LIONS -> Credibility in a whale 
- [Think Tanks](#think-tanks) -> TIGERS -> Discovery in a whale 



<br>

### Legacy Media 

- largest source of information for every animal 

### Legacy Pharmacy 

- largest source of revenue for nearly every MSM outlet 
- 80% of deaths and 90% of healthcare costs tied to preventable/reversible lifestyle diseases 
- heart disease, diabetes, kidney disease, cancer, alzheimers are caused by poisoned food supply 
- too many drugs from a system that is incentivized by sickness 

<br>







- [The Pugs](#the-pugs)
- [The Pugs Oath (OATH)](#the-pugs-oath)
- [Chollima (North Korea)](#chollima)
- [Bear (Russia)](#bear)
- [Kitten (Iran)](#kitten)
- [Panda (China)](#panda)
- [The Wolves](#the-wolves)
- [The Whales](#the-whales)
- [The Lammergeier](#the-lammergeier)
- [The Boiling Frog](#the-boiling-frog)
- [The Camels Nose](the-camels-nose)
- [The Crocodiles Tears](#the-crocodiles-tears)
- [The Hedeghogs Dilemma](#the-hedgehogs-dilemma)
- [The Post Turtle](#the-post-turtle)
- [The Purple Squirrel](#the-purple-squirrel)
- [The Weasel Words](#the-weasel-words)
- [The Ostrich Effect](#the-ostrich-effect)
- [The Raven Paradox](#the-raven-paradox)
- [The Wolf in Sheeps Clothing](#the-wolf-in-sheeps-clothing)
- [The Farmer and the Viper](#the-farmer-and-the-viper)
- [The Black Sheep](#the-black-sheep)


<br>




<br>




<br>

### The Goat 

|||
|-|-|
|[The Goatherd and the Wild Goats](https://read.gov/aesop/096.html)|It is unwise to treat old friends badly for the sake of new one|
|[The Gnat and the Bull](https://read.gov/aesop/008.html)|We are often of greater importance in our own eyes than in the eyes of our neighbor. The smaller the mind the greater the conceit|
|[The Two Goats](https://read.gov/aesop/013.html)|It is better to yield than to come to misfortune through stubbornness|
|[The Bull and the Goat](https://read.gov/aesop/105.html)|It is wicked to take advantage of another's distress|
|[The Goatherd and the Goat](https://read.gov/aesop/111.html)|Wicked deeds will not stay hid|

<br>

|||
|-|-|
|[The Ass Carrying the Image](https://read.gov/aesop/052.html)|Do not try to take the credit to yourself that is due to others|
|[The Ass and the Load of Salt](https://read.gov/aesop/055.html)|The same measures will not suit all circumstances|
|[The Ass and His Shadow](https://read.gov/aesop/135.html)|In quarreling about the shadow we often lose the substance|
|[The Ass and the Grasshoppers](https://read.gov/aesop/083.html)|The laws of nature are unchangeable|
|[The Owl and the Grasshopper](https://read.gov/aesop/010.html)|Flattery is not a proof of true admiration. Do not let flattery throw you off your guard against an enemy|
|[The Oak and the Reeds](https://read.gov/aesop/011.html)|Better to yield when it is folly to resist, than to resist stubbornly and be destroyed|
|[The Crow and the Pitcher](https://read.gov/aesop/012.html)|In a pinch a good use of our wits may help us out|
|[The Stag and His Reflection](https://read.gov/aesop/017.html)|We often make much of the ornamental and despise the useful.|
|[The Frog and the Mouse](https://read.gov/aesop/021.html)|Those who seek to harm others often come to harm themselves through their own deceit|
|[The Eagle and the Beetle](https://read.gov/aesop/023.html)|Even the weakest may find means to avenge a wrong|
|[The Eagle and the Jackdaw](https://read.gov/aesop/036.html)|Do not let your vanity make you overestimate your powers|
|[The Serpent and the Eagle](https://read.gov/aesop/104.html)|An act of kindness is well repaid|


<br>

### The Insects 

|||
|-|-|
|[The Flies and the Honey](https://read.gov/aesop/127.html)|Be not greedy for a little passing pleasure. It may destroy you|
|[The Bees and Wasps and the Hornet](https://read.gov/aesop/132.html)|Ability proves itself by deeds|
|[The Bear and the Bees](https://read.gov/aesop/080.html)|It is wiser to bear a single injury in silence than to provoke a thousand by flying into a rage|
|[The Rose and the Butterfly](https://read.gov/aesop/119.html)|Do not expect constancy in others if you have none yourself|
|[The Ant and the Dove](https://read.gov/aesop/028.html)|A kindness is never wasted|
|[The Ants and the Grasshopper](https://read.gov/aesop/051.html)|There's a time for work and a time for play|
<br>

### The Rest 

|||
|-|-|
|[The Young Crab and His Mother](https://read.gov/aesop/034.html)|Do not tell others how to act unless you can set a good example|
|[The Frog Who Wished for a King](https://read.gov/aesop/048.html)|Be sure you can better your condition before you seek to change|
|[The Monkey and the Camel](https://read.gov/aesop/059.html)|Do not try to ape your betters|
|[The Vain Jackdaw and his Borrowed Feathers](https://read.gov/aesop/073.html)|Borrowed feathers do not make fine birds|
|[The Monkey and the Dolphin](https://read.gov/aesop/074.html)|One falsehood leads to another|
|[The Fighting Bulls and the Frog](https://read.gov/aesop/092.html)|When the great fall out, the weak must suffer for it|
|[The Quack Toad](https://read.gov/aesop/116.html)|Those who would mend others, should first mend themselves|
|[Two Travelers and a Bear](https://read.gov/aesop/124.html)|Misfortune is the test of true friendship|
|[The Porcupine and the Snakes](https://read.gov/aesop/125.html)|Give a finger and lose a hand|


<br>



<br>
<br>

## Tex the Sheepdog 

A long, long time ago almost all the land was undiscovered, and the parts that had been discovered were dark and dangerous.  There were many different kinds of animals, but not many tribes.  It was very difficult to tell if other animals were friends, enemies, or neither.

In one part of the land was a sheepdog who didn't know he was a sheepdog. The animals didn't know who other animals were and some didn't even know themselves that well.  He knew his name, Tex, which he learned from his Mama.  He also knew he was some sort of dog, which he learned from his family and other dog families.

But there were many types of dogs, and different types of dogs were good at doing different types of jobs.  

One day he became friends with a Pug named Konstantin. 

<br>

### Konstantin the Pug 

motto of the breed is the Latin phrase multum in parvo, or "much in little" 

Pugs tend to be intuitive and sensitive to the moods of their owners and are usually eager to please them. Pugs are playful and thrive on human companionship. Pugs are often called "shadows" because they follow their owners around and like to stay close to the action, craving attention and affection from their owner

Pugs are known for being sociable and gentle companion dogs.[4] The American Kennel Club describes the breed's personality as "even-tempered and charming"

<br>


### Jordan the Lammergeier 
 

|||
|-|-|
|[The Cock and the Jewel](https://read.gov/aesop/058.html)|Precious things are without value to those who cannot prize them|
|[A Raven and a Swan](https://read.gov/aesop/053.html)|A change of habits will not alter nature|
|[The Birds the Beasts and the Bat](https://read.gov/aesop/061.html)|The deceitful have no friends|
|[The Peacock](https://read.gov/aesop/068.html)|Do not sacrifice your freedom for the sake of pomp and show|
|[The Peacock and the Crane](https://read.gov/aesop/087.html)|The useful is of much more importance and value, than the ornamental|
|[The Goose and the Golden Egg](https://read.gov/aesop/091.html)|Those who have plenty want more and so lose all they have|
|[The Lark and her Young Ones](https://read.gov/aesop/133.html)|Self-help is the best help|
|[The Swallow and the Crow](https://read.gov/aesop/138.html)|Friends in fine weather only, are not worth much|
|[The Tortoise and the Ducks](https://read.gov/aesop/033.html)|Foolish curiosity and vanity often lead to misfortune|
|[The Spendthrift and the Swallow](https://read.gov/aesop/097.html)|One swallow does not make a summer|
|[The Cat and the Birds](https://read.gov/aesop/098.html)|Be wise and shun the quack|

<br>

Knowledge is information and comprehension acquired through sensory input: reading, listening, watching, and touching. Knowledge is theoretical understanding of concepts, facts, principles, and information. It is a process--the union of an abstract principle with a set of concrete examples.

A familiarity with factual information and theoretical concepts.  Can be self acquired through observation and study or transferred.
Knowledge is primarily cognitive and involves gathering information and understanding

Skills are the application of knowledge in practical scenarios.  The ability to perform tasks or activities.

Skills are the application developed through practice and experience over time 
Skills are action-oriented and involve practical demonstration of abilities.
They are about doing and applying knowledge effectively to perform specific tasks or solve problems
Ability to do something, coming from one's knowledge, practice, aptitude, etc.
Implant knowledge and transform that knowledge into skill through physical training
Lectures, presentations, quizzes, theoretical content don't improve performance. 
Activities, scenarios, simulations.. Performing the actions does.
Skills can be developed more easily if one has prior knowledge of the task to be accomplished.
Practice is the only way to develop skills.
Knowledge provides the foundation, skills are teh tangible results of applying that knowledge 
    
We are not all-seeing, all-knowing creatures. Best we can do with our knowledge is observe its functionality and improve it when it fails  ( mental model )

Projection (victim, rescuer, persecutor) is a tool which protects us from having to take personal responsibility and look at ourselves. The problem is always “out there,” “in them,” “over there,” but never “in me.”


<br>


### Snowball and the Farm

<br>

|||
|-|-|
|[The Farmer and the Snake](https://read.gov/aesop/094.html)|Learn from my fate not to take pity on a scoundrel.|
|[The Farmer and the Stork](https://read.gov/aesop/044.html)|You are judged by the company you keep|
|[The Farmers and the Cranes](https://read.gov/aesop/088.html)|Bluff and threatening words are of little value with rascals. Bluff is no proof that hard fists are lacking|
|[The Sheep and The Pig](https://read.gov/aesop/045.html)|It is easy to be brave when there is no danger|
|[The Oxen and the Wheels](https://read.gov/aesop/042.html)|The complain most who suffer least|
|[The Animals and the Plague](https://read.gov/aesop/130.html)|The weak are made to suffer for the misdeeds of the powerful|
|[The Sick Stag](https://read.gov/aesop/095.html)|Good will is worth nothing unless it is accompanied by good acts|

<br>


### Buck and the Wolf Pack

<br>

|||
|-|-|
|[The Dog and the Oyster](https://read.gov/aesop/099.html)|Act in haste and repent at leisure—and often in pain|
|[The Dog and His Reflection](https://read.gov/aesop/026.html)|It is very foolish to be greedy|
|[The Dog and His Master's Dinner](https://read.gov/aesop/072.html)|Do not stop to argue with temptation|
|[The Dogs and the Hides](https://read.gov/aesop/078.html)|Do not try to do impossible things|
|[The Dog in the Manger](https://read.gov/aesop/081.html)|Do not grudge others what you cannot enjoy yourself|
|[The Mischievous Dog](https://read.gov/aesop/118.html)|Notoriety is not fame|
|||
|-|-|
|State|[Big Government (Lion)](#big-government) &emsp; [Big Healthcare (Rabbit)](#big-healthcare) &emsp;|
|Institutions| [Big Banks (Shark)](#big-banks) &emsp; [The Owl (Academia) and the Grasshopper (Students)](https://read.gov/aesop/010.html) &emsp; ;  [Big Tech (Panda)](#big-tech) &emsp; [Legacy Media (Pug)](#legacy-media)|
|Groups|[The 800 lb Gorilla (Big Pharma)](#big-pharma) &emsp; [Foundations (Elk)](#foundations) &emsp; [The Paper Tiger (Think Tanks)](#think-tanks) &emsp; [Non-governmental Organizations (Raccoon)](#non-governmental-organizations)|

<br>

## The Wolf 

<br>

|||
|-|-|
|[The Wolf and the Lamb](https://read.gov/aesop/063.html)|The tyrant can always find an excuse for his tyranny. The unjust will not listen to the reasoning of the innocent|
|[The Wolf and the Sheep](https://read.gov/aesop/064.html)|A knave's hypocrisy is easily seen through|
|[The Wolf and the Lion](https://read.gov/aesop/067.html)|What is evil won is evil lost|
|[The Wolf and the Ass](https://read.gov/aesop/075.html)|Stick to your trade|
|[The Wolf and the Goat](https://read.gov/aesop/082.html)|An invitation prompted by selfishness is not to be accepted|
|[The Wolf and the Shepherd](https://read.gov/aesop/086.html)|Once a wolf, always a wolf|
|[The Wolf and the Shepherd](https://read.gov/aesop/110.html)|Men often condemn others for what they see no wrong in doing themselves|
|[The Stag the Sheep and the Wolf](https://read.gov/aesop/129.html)|Two blacks do not make a white|
|[The Wolf the Kid and the Goat](https://read.gov/aesop/137.html)|Two sureties are better than one|
|[The Wolves and the Sheep](https://read.gov/aesop/144.html)|Do not give up friends for foes|
|[The Wolf and The Kid](https://read.gov/aesop/032.html)|Do not let anything turn you from your purpose|
|[The Kid and the Wolf](https://read.gov/aesop/039.html)|Do not say anything at any time that you would not say at all times|
|[The Wolf in Sheep's Clothing](https://read.gov/aesop/022.html)|The evil doer often comes to harm through his own deceit|
|[The Mother and the Wolf](https://read.gov/aesop/024.html)|Do not believe everything you hear|
|[The Shepherd Boy and the Wolf](https://read.gov/aesop/043.html)|Liars are not believed even when they speak the truth|
|[The Wolf and His Shadow](https://read.gov/aesop/049.html)|Do not let your fancy make you forget realities|
|[The Wolf and the Lean Dog](https://read.gov/aesop/070.html)|Do not depend on the promises of those whose interest it is to deceive you|
|[The Wolf and the House Dog](https://read.gov/aesop/113.html)|There is nothing worth so much as liberty|

<br>

### Todd and the Hound

|||
|-|-|
|[The Hare and the Tortoise](https://read.gov/aesop/025.html)|The race is not always to the swift|
|[The Hare and His Ears](https://read.gov/aesop/030.html)|Do not give your enemies the slightest reason to attack your reputation. Your enemies will seize any excuse to attack you|
|[The Hares and the Frogs](https://read.gov/aesop/065.html)|However unfortunate we may think we are there is always someone worse off than ourselves|
|[The Fox and the Grapes](https://read.gov/aesop/005.html)|There are many who pretend to despise and belittle that which is beyond their reach|
|[The Cock and the Fox](https://read.gov/aesop/018.html)|The trickster is easily tricked|
|[The Fox and the Goat](https://read.gov/aesop/019.html)|Look before you leap|
|[The Fox and the Leopard](https://read.gov/aesop/020.html)|A fine coat is not always an indication of an attractive mind|
|[The Wild Boar and the Fox](https://read.gov/aesop/014.html)|Preparedness for war is the best guarantee of peace|
|[The Fox and the Stork](https://read.gov/aesop/016.html)|Do not play tricks on your neighbors unless you can stand the same treatment yourself|
|[The Dog the Cock and the Fox](https://read.gov/aesop/035.html)|Those who try to deceive may expect to be paid in their own coin|
|[The Fox and the Crow](https://read.gov/aesop/027.html)|The flatterer lives at the expense of those who will listen to him|
|[The Fox and the Crab](https://read.gov/aesop/103.html)|Be content with your lot|
|[The Dogs and the Fox](https://read.gov/aesop/077.html)|It is easy and also contemptible to kick a man that is down|
|[The Fox Without a Tail](https://read.gov/aesop/117.html)|Do not listen to the advice of him who seeks to lower you to his own level|
|[The Cat and the Fox](https://read.gov/aesop/120.html)|Common sense is always worth more than cunning|
|[The Fox and the Pheasants](https://read.gov/aesop/123.html)|Too much attention to danger may cause us to fall victims to it|
|[The Fox and the Hedgehog](https://read.gov/aesop/114.html)|Better to bear a lesser evil than to risk a greater in removing it|
|[The Fox and the Monkey](https://read.gov/aesop/126.html)|The true leader proves himself by his qualities|
|[The Cock and the Fox](https://read.gov/aesop/145.html)|The wicked deserve no aid|

<br>

### Justin and the Rats

<br>

|||
|-|-|
|[The Rat and the Elephant](https://read.gov/aesop/050.html)|A resemblance to the great in some things does not make us great|
|[The Mice and the Weasels](https://read.gov/aesop/069.html)|Greatness has its penalties|
|[The Mouse and the Weasel](https://read.gov/aesop/093.html)|Greediness leads to misfortune|
|[The Bat and the Weasels](https://read.gov/aesop/115.html)|Set your sails with the wind|
|[The Mole and His Mother](https://read.gov/aesop/142.html)|Boast of one thing and you will be found lacking in that and a few other things as well|
|[Belling the Cat](https://read.gov/aesop/003.html)|It is one thing to say that something should be done, but quite a different matter to do it|
|[The Cat the Cock and the Young Mouse](https://read.gov/aesop/085.html)|Do not trust alone to outward appearances|
|[The Monkey and the Cat](https://read.gov/aesop/0.html)|The flatterer seeks some benefit at your expense|
|[The Cat and the Old Rat](https://read.gov/aesop/134.html)|The wise do not let themselves be tricked a second time|
|[The Rabbit the Weasel and the Cat](https://read.gov/aesop/079.html)|The strong are apt to settle questions to their own advantage|

<br>

### Ceasar and the Apes



### Simba and the Lions 

<br>

|||
|-|-|
|[The Mouse and the Lion](https://read.gov/aesop/007.html)| A kindness is never wasted|
|[The Ass in the Lion's Skin](https://read.gov/aesop/146.html)|A fool may deceive by his dress and appearance, but his words will soon show what he really is|
|[The Lion the Ass and the Fox](https://read.gov/aesop/1.html)|Learn from the misfortunes of others|
|[The Lion's Share](https://read.gov/aesop/141.html)|Might makes right|
|[The Shepherd and the Lion](https://read.gov/aesop/131.html)|We are often not so eager for what we seek, after we have found it. Do not foolishly ask for things that would bring ruin if they were granted.|
|[The Old Lion](https://read.gov/aesop/122.html)|It is cowardly to attack the defenseless, though he be an enemy|
|[The Man and the Lion](https://read.gov/aesop/107.html)|It all depends on the point of view, and who tells the story|
|[The Old Lion and the Fox](https://read.gov/aesop/106.html)|Take warning from the misfortunes of others|
|[Three Bullocks and a Lion](https://read.gov/aesop/101.html)|In unity is strength.|
|[The Fox and the Lion](https://read.gov/aesop/071.html)|Familiarity breeds contempt. Acquaintance with evil blinds us to its dangers|
|[The Ass the Fox and the Lion](https://read.gov/aesop/060.html)|Traitors may expect treachery|
|[The Lion the Bear and the Fox](https://read.gov/aesop/062.html)|Those who have all the toil do not always get the profit|
|[The Lion and the Gnat](https://read.gov/aesop/056.html)|The least of our enemies is often the most to be feared. Pride over a success should not throw us off our guard.|
|[The Lion and the Ass](https://read.gov/aesop/047.html)|Do not resent the remarks of a fool. Ignore them.||[The Ass and the Lap Dog](https://read.gov/aesop/108.html)|Behavior that is regarded as agreeable in one is very rude and impertinent in another. Do not try to gain favor by acting in a way that is contrary to your own nature and character||[The Fighting Cocks and the Eagle](https://read.gov/aesop/147.html)|Pride goes before a fall|

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>










## Moses 

Spent years developing a hierarchy of values, mapping out higher order values 

## Jesus 

When targeted with traps and accusations, responded with higher order values 




## The Lammergeier 

The bearded vulture.  The bearded vulture is a scavenger, feeding mostly on the remains of dead animals. Its diet comprises mammals (93%), birds (6%) and reptiles (1%), with medium-sized ungulates forming a large part of the diet.[33] It usually disdains the actual meat and typically lives on 85–90% bones including bone marrow.[34] This is the only living bird species that specializes in feeding on bones. The bearded vulture can swallow whole or bite through brittle bones up to the size of a lamb's femur[36] and its powerful digestive system quickly dissolves even large pieces. The bearded vulture has learned to crack bones too large to be swallowed by carrying them in flight to a height of 50–150 m (160–490 ft) above the ground and then dropping them onto rocks below, which smashes them into smaller pieces and exposes the nutritious marrow.[13] They can fly with bones up to 10 cm (3.9 in) in diameter and weighing over 4 kg (8.8 lb), or nearly equal to their own weight

Bearded vultures sometimes attack live prey, with perhaps greater regularity than any other vulture.[13] Among these, tortoises seem to be especially favored depending on their local abundance. Tortoises preyed on may be nearly as heavy as the preying vulture. To kill tortoises, bearded vultures fly with them to some height and drop them to crack open the bulky reptiles' hard shells

Larger animals have been known to be attacked by bearded vultures, including ibex, Capra goats, chamois, and steenbok.[13] These animals have been killed by being surprised by the large birds and battered with wings until they fall off precipitous rocky edges to their deaths; although in some cases these may be accidental killings when both the vulture and the mammal surprise each other

 Iranian mythology considers the rare bearded vulture (Persian: هما; lit. 'Homa') the symbol of luck and happiness. It was believed that if the shadow of a Homa fell on one, he would rise to sovereignty[49] and anyone shooting the bird would die in forty days. The habit of eating bones and apparently not killing living animals was noted by Sa'di in Gulistan, written in 1258, and Emperor Jahangir had a bird's crop examined in 1625 to find that it was filled with bones

 The Greek playwright Aeschylus was said to have been killed in 456 or 455 BC by a tortoise dropped by an eagle who mistook his bald head for a stone—if this incident did occur, the bearded vulture is a likely candidate for the "eagle" in this story.

In the Bible/Torah, the bearded vulture, as the ossifrage, is among the birds forbidden to be eaten (Leviticus 11:13).


The name "lammergeier" originates from the German word Lämmergeier, which means "lamb-vulture". The name stems from the belief that it attacked lambs

Wingspans of 7 to 9 feet. The proportions of the species have been compared to a falcon, scaled to an enormous size

This species is relatively small-headed, although its neck is powerful and thick. It has a generally elongated, slender shape, sometimes appearing bulkier due to the often hunched back of these birds. The gait on the ground is waddling and the feet are large and powerful. The adult is mostly dark gray, rusty, and whitish in color. It is grey-blue to grey-black above. The creamy-coloured forehead contrasts against a black band across the eyes and lores and bristles under the chin, which form a black beard that give the species its English name. Bearded vultures are variably orange or rust of plumage on their head, breast, and leg feathers, but this is thought to be cosmetic. This colouration comes from dust-bathing or rubbing iron-rich mud on its body.[16][17] They also transfer the brown colour to the eggs.[18] The tail feathers and wings are gray. The juvenile bird is dark black-brown over most of the body, with a buff-brown breast and takes five years to reach full maturity. The bearded vulture is silent, apart from shrill whistles in their breeding displays and a falcon-like cheek-acheek call made around the nest

The high fat content of bone marrow makes the net energy value of bone almost as good as that of muscle, even if bone is less completely digested. A skeleton left on a mountain will dehydrate and become protected from bacterial degradation, and the bearded vulture can return to consume the remainder of a carcass even months after the soft parts have been consumed by other animals, larvae, and bacteria

. It occurs in mountainous regions in the Pyrenees, the Alps, the Arabian Peninsula, the Caucasus region, the Zagros Mountains, the Alborz,Iran, the Koh-i-Baba in Bamyan, Afghanistan, the Altai Mountains, the Himalayas, Ladakh in northern India, and western and central China.[1] In Africa, it lives in the Atlas Mountains, the Ethiopian Highlands and south from Sudan to northeastern Democratic Republic of the Congo, central Kenya, and northern Tanzania. An isolated population inhabits the Drakensberg in South Africa.[13] It has been reintroduced in several places in Spain, such as the Sierras de Cazorla, Segura and Las Villas Jaén, the Province of Castellón and Asturias. The resident population as of 2018 was estimated at 1,200 to 1,500 individuals.[20]


## Snowball and the Animal Farm 


Animal Farm reflects events leading up to the Russian Revolution of 1917 and then on into the Stalinist era of the Soviet Union, a period when Russia lived under the communist ideology of Joseph Stalin

poorly run Manor Farm near Willingdon, England, is ripened for rebellion by neglect at the hands of the irresponsible and alcoholic farmer, Mr. Jones. One night, the exalted boar, Old Major, holds a conference, at which he calls for the overthrow of humans and teaches the animals a revolutionary song called "Beasts of England". When Old Major dies, two young pigs, Snowball and Napoleon, assume command and stage a revolt, driving Mr. Jones off the farm and renaming the property "Animal Farm". They adopt the Seven Commandments of Animalism, the most important of which is, "All animals are equal". The decree is painted in large letters on one side of the barn. Snowball teaches the animals to read and write, while Napoleon educates young puppies on the principles of Animalism. To commemorate the start of Animal Farm, Snowball raises a green flag with a white hoof and horn. Food is plentiful, and the farm runs smoothly. The pigs elevate themselves to positions of leadership and set aside special food items, ostensibly for their health. Following an unsuccessful attempt by Mr. Jones and his associates to retake the farm (later dubbed the "Battle of the Cowshed"), Snowball announces his plans to modernise the farm by building a windmill. Napoleon disputes this idea, and matters come to a head, which culminates in Napoleon's dogs chasing Snowball away and Napoleon effectively declaring himself supreme commander.

Napoleon enacts changes to the governance structure of the farm, replacing meetings with a committee of pigs who will run the farm. Through a young porker named Squealer, Napoleon claims credit for the idea of building the windmill, claiming that Snowball was only trying to win animals to his side. The animals work harder with the promise of easier lives with the windmill. When the animals find the windmill collapsed after a violent storm, Napoleon and Squealer persuade the animals that Snowball is trying to sabotage their project, and begin to purge the farm of animals accused by Napoleon of consorting with his old rival. When some animals recall the Battle of the Cowshed, Napoleon (who was nowhere to be found during the battle) gradually smears Snowball to the point of saying he is a collaborator of Mr. Jones, even dismissing the fact that Snowball was given an award of courage, while falsely representing himself as the main hero of the battle. "Beasts of England" is replaced with "Animal Farm", while an anthem glorifying Napoleon, who is presumably adopting the lifestyle of a man ("Comrade Napoleon"), is composed and sung. Napoleon then conducts a second purge, during which many animals who are alleged to be helping Snowball in plots are executed by Napoleon's dogs, which troubles the rest of the animals. Despite their hardships, the animals are easily pacified by Napoleon's retort that they are better off than they were under Mr. Jones, as well as by the sheep's continual bleating of "four legs good, two legs bad".

Mr. Frederick, a neighbouring farmer, attacks the farm, using blasting powder to blow up the restored windmill. Although the animals win the battle, they do so at great cost, as many, including Boxer the workhorse, are wounded. Although he recovers from this, Boxer eventually collapses while working on the windmill (being almost 12 years old at that point). He is taken away in a knacker's van and a donkey called Benjamin alerts the animals of this, but Squealer quickly waves off their alarm by persuading the animals that the van had been purchased from the knacker by an animal hospital and that the previous owner's signboard had not been repainted. Squealer subsequently reports Boxer's death and honours him with a festival the following day. In truth, Napoleon had engineered the sale of Boxer to the knacker, allowing him and his inner circle to acquire money to buy whisky for themselves.

Years pass, the windmill is rebuilt and another windmill is constructed, which makes the farm a good amount of income. However, the ideals that Snowball discussed, including stalls with electric lighting, heating, and running water, are forgotten, with Napoleon advocating that the happiest animals live simple lives. Snowball has been forgotten, alongside Boxer, with "the exception of the few who knew him". Many of the animals who participated in the rebellion are dead or old. Mr. Jones is also now known to be dead, having "died in an inebriates' home in another part of the country". The pigs start to resemble humans, as they walk upright, carry whips, drink alcohol, and wear clothes. The Seven Commandments are abridged to just one phrase: "All animals are equal, but some animals are more equal than others". The maxim "Four legs good, two legs bad" is similarly changed to "Four legs good, two legs better". Other changes include the Hoof and Horn flag being replaced with a plain green banner and Old Major's skull, which was previously put on display, being reburied.

Napoleon holds a dinner party for the pigs and local farmers, with whom he celebrates a new alliance. He abolishes the practice of the revolutionary traditions and restores the name "The Manor Farm". The men and pigs start playing cards, flattering and praising each other while cheating at the game. Both Napoleon and Mr. Pilkington, one of the farmers, play the ace of spades at the same time and both sides begin fighting loudly over who cheated first. When the animals outside look at the pigs and men, they can no longer distinguish between the two.

Characters
Pigs
Old Major – An aged prize Middle White boar provides the inspiration that fuels the rebellion. He is also called Willingdon Beauty when showing. He is an allegorical combination of Karl Marx, one of the creators of communism, and Vladimir Lenin, the communist leader of the Russian Revolution and the early Soviet nation, in that he draws up the principles of the revolution. His skull being put on revered public display recalls Lenin, whose embalmed body was left in indefinite repose.[16] By the end of the book, the skull is reburied.
Napoleon – "A large, rather fierce-looking Berkshire boar, the only Berkshire on the farm, not much of a talker, but with a reputation for getting his own way".[17] An allegory of Joseph Stalin,[16] Napoleon is the leader of Animal Farm.
Snowball – Napoleon's rival and original head of the farm after Jones's overthrow. His life parallels that of Leon Trotsky,[16] although there is no reference to Snowball having been murdered (as Trotsky was); he may also combine some elements from Lenin.[18][c]
Squealer – A small, white, fat large white who serves as Napoleon's second-in-command and minister of propaganda, is a collective portrait of the Soviet nomenklatura and journalists, such as of the national daily Pravda (The Truth), able to justify every twist and turn in Stalin's policy.[16]
Minimus – A poetic pig who writes the second national anthem of Animal Farm after the singing of "Beasts of England" is banned; later he composes a poem "Comrade Napoleon". Literary theorist John Rodden compares him to the poet Vladimir Mayakovsky,[19] who eulogized Lenin and the Soviet Union, although Mayakovsky neither wrote anthems nor praised Stalin in his poems.
The piglets – Hinted to be the children of Napoleon and are the first generation of animals subjugated to his idea of animal inequality.
The young pigs – Four pigs who complain about Napoleon's takeover of the farm but are quickly silenced and later executed, the first animals killed in Napoleon's farm purge. Probably based on the Great Purge of Grigory Zinoviev, Lev Kamenev, Nikolai Bukharin, and Alexei Rykov.
Pinkeye – A minor pig who is mentioned only once; he is the taste-tester that samples Napoleon's food to make sure it is not poisoned, in response to rumours about an assassination attempt on Napoleon.
Humans
Mr. Jones – A heavy drinker who is the original owner of Manor Farm, a farm in disrepair with farmhands who often loaf on the job. He is an allegory of Russian Tsar Nicholas II,[20] who was forced to abdicate following the February Revolution of 1917 and was executed, along with the rest of his family, by the Bolsheviks on 17 July 1918. The animals revolt after Jones goes on a drinking binge, returns hungover the following day and neglects them completely. Jones is married, but his wife plays no active role in the book. She seems to live with her husband's drunkenness, going to bed while he stays up drinking until late into the night. In her only other appearance, she hastily throws a few things into a travel bag and flees when she sees that the animals are revolting. Towards the end of the book, Napoleon's "favourite sow" wears her old Sunday dress.
Mr. Frederick – The tough owner of Pinchfield Farm, a small but well-kept neighbouring farm, who briefly allies with Napoleon.[21][22][23][24] Animal Farm shares land boundaries with Pinchfield on one side and Foxwood on another, making Animal Farm a "buffer zone" between the two bickering farmers. The animals of Animal Farm are terrified of Frederick, as rumours abound of him abusing his animals and entertaining himself with cockfighting. Napoleon allies with Frederick to sell surplus timber that Pilkington also sought, but is enraged to learn Frederick paid him in counterfeit money. Shortly after the swindling, Frederick and his men invade Animal Farm, killing many animals and destroying the windmill. The brief alliance and subsequent invasion may allude to the Molotov–Ribbentrop Pact and Operation Barbarossa.[23][25][26]
Mr. Pilkington – The easy-going but crafty and well-to-do owner of Foxwood Farm, a large neighbouring farm overgrown with weeds. Pilkington is wealthier than Frederick and owns more land, but his farm needs care as opposed to Frederick's smaller but more efficiently run farm. Although on bad terms with Frederick, Pilkington is also concerned about the animal revolution that deposed Jones and is worried that this could also happen to him.
Mr. Whymper – A man hired by Napoleon to act as the liaison between Animal Farm and human society. At first, he acquires necessities that cannot be produced on the farm, such as dog biscuits and paraffin wax, but later he procures luxuries like alcohol for the pigs.
Equines
Boxer – A loyal, kind, dedicated, extremely strong, hard-working, and respectable cart-shire horse, although quite naive and gullible.[27] Boxer does a large share of the physical labour on the farm. He is shown to hold the belief that "Napoleon is always right." At one point, he questions Squealer's statement that Snowball was always against the welfare of the farm, causing him to be attacked by Napoleon's dogs, however Boxer's immense strength repels the attack, worrying the pigs that their authority can be challenged. Boxer has been compared to Alexey Stakhanov, a diligent and enthusiastic role model of the Stakhanovite movement.[28] He has been described as "faithful and strong";[29] he believes any problem can be solved if he works harder.[30] When Boxer is injured, Napoleon sells him to a local knacker to buy himself whisky, and Squealer gives a moving account, falsifying the circumstances of Boxer's death.
Mollie – A self-centred, self-indulgent, and vain young white mare who quickly leaves for another farm after the revolution, like those who left Russia after the fall of the Tsar.[31] She is only once mentioned again.
Clover – A gentle, caring mare, who shows concern, especially for Boxer, who often pushes himself too hard. Clover can read all the letters of the alphabet, but cannot "put words together".
Benjamin – A donkey, one of the oldest, wisest animals on the farm, and one of the few who can read properly. He is sceptical, temperamental and cynical: his most frequent remark is, "Life will go on as it has always gone on – that is, badly". Academic Morris Dickstein has suggested there is "a touch of Orwell himself in this creature's timeless scepticism"[32] and indeed, friends called Orwell "Donkey George", "after his grumbling donkey Benjamin, in Animal Farm".[33] Benjamin manages to evade the purges and survive despite the threat he potentially poses given his knowledge, his age, and his equivocal, albeit apolitical, positions.
Other animals
Muriel – A goat who is another of the oldest, wisest animals on the farm and friends with all of the animals on the farm. Similar to Benjamin, Muriel is one of the few animals on the farm who is not a pig but can read. She survives, as does Benjamin, by eschewing politics.
The puppies – Offspring of Jessie and Bluebell, the puppies were taken away at birth by Napoleon and raised by him to serve as his powerful security force.
Moses – The Raven, "Mr. Jones's especial pet, was a spy and a tale-bearer, but he was also a clever talker".[34] Initially following Mrs. Jones into exile, he reappears several years later and resumes his role of talking but not working. He regales Animal Farm's denizens with tales of a wondrous place beyond the clouds called "Sugarcandy Mountain, that happy country where we poor animals shall rest forever from our labours!" Orwell portrays established religion as "the black raven of priestcraft – promising pie in the sky when you die, and faithfully serving whoever happens to be in power". His preaching to the animals heartens them, and Napoleon allows Moses to reside at the farm "with an allowance of a gill of beer daily", akin to how Stalin brought back the Russian Orthodox Church during the Second World War.[32]
The sheep – They are not given individual names or personalities. They show limited understanding of Animalism and the political atmosphere of the farm, yet nonetheless, they are the voice of blind conformity[32] as they bleat their support of Napoleon's ideals with jingles during his speeches and meetings with Snowball. Their constant bleating of "four legs good, two legs bad" was used as a device to drown out any opposition or alternative views from Snowball, much as Stalin used hysterical crowds to drown out Trotsky.[35] Towards the end of the book, Squealer (the propagandist) trains the sheep to alter their slogan to "four legs good, two legs better", which they dutifully do.
The hens – The hens are promised following the rebellion that they will get to keep their eggs, which are stolen from them under Mr. Jones, however, their eggs are soon taken from them under the premise of buying goods from outside the farm. The hens are among the first to rebel, albeit unsuccessfully, against Napoleon, being brutally suppressed through starvation. They represent the Ukrainian victims of the Holodomor.[36][37]
The cows – Unnamed. The cows are enticed into the revolution by promises that their milk will not be stolen but can be used to raise their calves. Their milk is then stolen by the pigs, who learn to milk them. The milk is stirred into the pigs' mash every day, while the other animals are denied such luxuries.
The cat – Unnamed and never seen to carry out any work. The cat is absent for long periods and is forgiven because her excuses are so convincing and she "purred so affectionately that it was impossible not to believe in her good intentions".[38] She has no interest in the politics of the farm, and the only time she is recorded as having participated in an election, she is found to have actually "voted on both sides". [39]
The ducks – Unnamed.
The roosters – One arranges to wake Boxer early, and a black one acts as a trumpeter for Napoleon.
The geese – Unnamed. One gander commits suicide by eating nightshade berries.
The rats — Unnamed. Classed among the wild animals, unsuccessful attempts were made to civilise them and teach them the principles of Animalism.

## The Fox and the Hound 

After a young red fox is orphaned, Big Mama the owl and her friends, Dinky the finch and Boomer the woodpecker, arrange for him to be adopted by a kindly farmer named Widow Tweed, who names him Tod. Meanwhile, her neighbor, hunter Amos Slade, brings home a young hound puppy named Copper and introduces him to his hunting dog, Chief, who is at first annoyed by him but then learns to love him. One day, Tod and Copper meet and become best friends, pledging eternal friendship. Amos grows frustrated at Copper for constantly wandering off to play and places him on a leash. While playing with Copper outside his barrel, Tod accidentally awakens Chief. Amos and Chief chase him until they are stopped by Tweed. After an argument, Amos threatens to kill Tod if he trespasses on his property again. Hunting season comes, and Amos takes Chief and Copper into the wilderness for the interim. Meanwhile, Big Mama, Dinky, and Boomer attempt to explain to Tod that Copper will soon become his enemy. However, he naively insists that they will remain friends forever.

The following spring, Tod and Copper reach adulthood. Copper returns as an expert hunting dog who is expected to track down foxes. Late at night, Tod sneaks over to visit him. Their conversation awakens Chief, who alerts Amos. A chase ensues, and Copper catches Tod but lets him go while diverting Amos. Chief catches Tod as he attempts an escape on a railroad track, but an oncoming train strikes him, resulting in him falling into the river below and breaking his leg. Enraged by this, Copper and Amos blame Tod for the accident and vow vengeance. Realizing Tod is no longer safe with her, Tweed leaves him at a game reserve. After a disastrous night on his own in the woods, Big Mama introduces him to Vixey, a female fox who helps him adapt to life there.

Amos and Copper trespass into the reserve and hunt Tod and Vixey. The chase climaxes when they inadvertently provoke an attack from a giant bear. Amos trips and falls into one of his own traps, dropping his rifle slightly out of reach. Copper violently fights the bear, but is almost killed by it. Tod comes to his rescue and battles it until they both fall down a waterfall. As Copper approaches Tod as he lies wounded in the lake below, Amos appears, ready to shoot him. Copper positions himself in front of him to prevent Amos from doing so, refusing to move away. Amos, understanding Tod had saved their lives from the bear, decides to spare Tod for Copper, lowers his rifle, and leaves with Copper. Tod and Copper share one last smile before parting.

At home, Tweed nurses Amos back to health, much to his humiliation. As he lies down to take a nap, Copper smiles as he remembers the day when he first met Tod. At the same moment, Vixey joins Tod on top of a hill as they both look down on Amos' and Tweed's homes.

<br>

## The Rats of Nimh 

Mrs. Brisby,[a] a widowed field mouse, lives in a cinder block with her children on the Fitzgibbons' farm. Brisby longs to move her family out of the field as plowing time approaches, but her son Timothy has fallen ill, confining them.

Brisby visits Mr. Ages, a friend of her late husband, Jonathan, who diagnoses the illness as pneumonia, provides Brisby with medicine and warns her that Timothy must stay inside for at least three weeks to avoid death. On her way home, Brisby befriends Jeremy, a clumsy but friendly crow, before they both narrowly escape from the Fitzgibbons' vicious cat, Dragon. The next morning, Brisby discovers that Farmer Fitzgibbons has started plowing early. Although her neighbor, Auntie Shrew, helps her disable his tractor, Brisby knows she must devise another plan. Jeremy takes her to meet the Great Owl, who tells her to visit a colony of rats that live beneath a rose bush on the farm and ask for the services of Nicodemus, their wise and mystical leader.

Brisby enters the rose bush and encounters an aggressive guard rat named Brutus, who chases her away. She is led back in by Ages and is amazed to see the rats' use of electricity and other technology. Brisby meets Justin, the friendly captain of the guard; Jenner, a ruthless and power-hungry member opposed to Nicodemus; and finally Nicodemus himself. From Nicodemus, she learns that many years ago the rats, along with her husband and Ages, were part of a series of experiments at the National Institute of Mental Health (NIMH for short). The experiments boosted their intelligence, enabling them to escape, as well as extending their lifespans and slowing their aging processes. However, they are unable to live as typical rats would and need human technology to survive, which they have accomplished only by stealing. The rats have eventually settled on a decision for them to leave the farm and live independently in an area they refer to as Thorn Valley. Nicodemus then gives Brisby a magical amulet that will activate when the wearer is courageous. Meanwhile, Jenner, who wishes for the rats to remain in the rose bush, plots with his reluctant accomplice, Sullivan, to eliminate Nicodemus.

Because of the rats' relationship with Jonathan, they agree to help her move her home. First, they need to drug Dragon so that it can be done safely. Only Brisby can do this, as the rats cannot fit through the hole leading into the house; Jonathan was killed by Dragon in a previous attempt, while Ages broke his leg in another. That night, she puts the drug into Dragon's dish, but the Fitzgibbons' young son, Billy, catches her. While trapped in a birdcage, she overhears a telephone conversation between Farmer Fitzgibbons and the staff of NIMH and learns that the institute intends to exterminate the rats in the morning. Brisby then escapes from the cage and runs off to warn them.

As a rainstorm approaches, the rats begin moving the Brisby home, with the children and Auntie Shrew inside, using a rope and pulley system. Disregarding Sullivan's protests, Jenner soon sabotages the assembly, causing it to fall apart and crush Nicodemus to death. Brisby soon arrives to warn the rats about NIMH's arrival, but Jenner attacks her and attempts to steal the amulet as Sullivan alerts Justin, who comes to Brisby's aid. Jenner mortally wounds Sullivan and engages Justin in a sword fight that ends with him being killed with a dagger thrown by the dying Sullivan.

The Brisby home suddenly begins to sink into the muddy ground it landed on and Brisby and the rats are unable to raise it. All appears lost until Brisby's will to save her family gives power to the amulet, which she uses to lift the house and move it to safety. The next day, the rats, with Justin as their new leader, have departed for Thorn Valley as Timothy begins to recover. Jeremy then meets and falls in love with a female crow who is just as clumsy yet good-hearted as he is.


