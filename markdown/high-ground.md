<br>

|||
|-|-|
|[Fallacies of Inconsistency](#fallacies-of-inconsistency)|[Activist Scholars for CRT](#activist-scholars-for-crt) <br> [Affirms Not Defines](#affirms-but-does-not-define) <br> [Anti-Racism is Demonizing Whites](#anti-racism-is-demonizing-whites) <br> [Biden Idea of Justice](#biden-idea-of-justice) <br> [Biden Leading A Nation](#biden-leading-a-nation) <br> [Christianity as a Tool](#christianity-as-a-tool) <br> [Complement & Compete](#roles-complement-and-compete) <br> [Context Matters and Does Not Matter](#context-matters-and-does-not-matter) <br> [Convictions for Unbacked Claims](#strong-convictions-for-unbacked-claims) <br> [Convictions for Untrusted Sources](#strong-convictions-for-untrusted-sources) <br> [Deception Claims From Liars](#deception-claims-from-liars) <br> [DEI As Conditional Racism](#dei-as-conditional-racism) <br> [DEI At The Same Time](#dei-at-the-same-time) <br>  [DEI Not Job Wide](#dei-but-not-job-wide) <br> [Do Not Celebrate Ancestry](#do-not-celebrate-ancestry) <br> [Educate and Indoctrinate](#educate-and-indoctrinate) <br> [Election Interference by Others](#election-interference-by-others) <br> [Elon Musk Threat to US](#elon-musk-threat-to-us) <br> [Fight for Human Rights Sometimes](#fight-for-human-rights-sometimes) <br> [Free Speech with Censorship](#support-of-free-speech-and-censorship) <br> [Government-Funded Propaganda Not News](#goverment-funded-propaganda-not-news) <br> [Greatest Threat to US](#greatest-threat-to-us) <br> [Gun Control Fixes Gun Crime](#gun-control-fixes-gun-crime) <br> [Harmful is Harmless](#harmful-is-harmless) <br> [Hasty and Superficial Press](#hasty-and-superficial-press) <br> [Human Rights Are Important](#human-rights-are-important) <br> [ID For Everything Except Voting](#id-for-everything-except-voting) <br> [Immigration Is Compassion](#immigration-is-compassion) <br> [Influencers Investigating Influence](#influencers-investigating-influence) <br> [Islam Is A Religion Of Peace](#islam-is-a-religion-of-peace) <br> [Kamala is Qualified](#kamala-is-qualified) <br> [Killing is Reproductive Freedom](#killing-is-reproductive-freedom) <br> [Law and Order and Rioting](#law-and-order-and-rioting) <br> [Lost Money Need More](#we-lost-money-we-need-more) <br> [My Body My Choice](#my-body-my-choice) <br> [Others Are Radicalized](#others-are-radicalized) <br> [Others Save the Planet](#others-save-the-planet) <br> [Outrage or Silence](#outrage-or-silence) <br> [Oversight Opposing Accountability](#oversight-opposing-accountability) <br> [Perpetrators of Misinformation](#greatest-perpetrator-of-misinformation) <br> [Prosecute or Excuse](#prosecute-or-excuse) <br> [Psycopath Claims From Psychopaths](#psychopath-claims-from-psychopaths) <br> [Racist Claims From Racists](#racist-claims-from-racists) <br>  [Secure Elections are Vulnerable](#make-secure-elections-more-vulnerable) <br> [Smart Kids Are Confused](#smart-kids-are-confused) <br> [Supreme Beings are Understandable](#supreme-beings-are-understandable) <br> [Results Matter Not Merit](#results-matter-not-merit) <br> [Tolerance and Inclusion Sometimes](#tolerance-and-inclusion-sometimes) <br> [Trans is Hard and Easy](#trans-is-hard-and-easy) <br> [Treat Symptoms without Root Causes](#treat-symptoms-without-root-causes) <br> [Trump is a Dictator](#trump-is-a-dictator) <br> [Trump is a Fascist](#trump-is-a-fascist) <br> [Trump is a Felon](#trump-is-a-felon) <br> [Trump will Weaponize Government](#trump-will-weaponize-government) <br> [Upside Down Immigration](#upside-down-immigration) <br> [Upside Down UK](#upside-down-uk) <br> [Vote With Your Race](#vote-with-your-race) <br> [Walz is Normal](#walz-is-normal) <br> [What Women Want](#what-women-want) <br> [Words Cause Violence](#words-cause-violence) <br> |
|[Quote Mining](#quote-mining)|[Drink the Bleach](#drink-the-bleach) <br> [Fine People on Both Sides](#fine-people-on-both-sides) <br>  [Georgia Votes](#georgia-votes) <br> [Hitler Did Some Good Things](#hitler-did-some-good-things) <br> [I Just Want Your Votes](#i-just-want-your-votes) <br> [No One Wants To See That](#no-one-wants-to-see-that) <br> [Whats In It For Them](#whats-in-it-for-them) <br> [Why Did He Join Military](#why-did-he-join-military) <br>  [You Wont Have To Vote Anymore](#you-wont-have-to-vote-anymore)|
|[Shape and Suppress](#shape-and-suppress)| [Border Agents Whipped Migrants](#border-agents-whipped-migrants) <br> [Bubba Wallace Garage Noose](#bubba-wallace-garage-noose) <br> [COVID Lab Origin](#covid-19-lab-origin) <br> [COVID Prevented by Masks](#covid-prevented-by-masks) <br> [COVID Shot Stops Transmission](#covid-shot-stops-transmission) <br> [Covington Kids](#covington-kids) <br> [Hunter Biden Laptop](#hunter-biden-laptop) <br> [Ivermectin is Horse Dewormer](#ivermectin-is-horse-dewormer) <br> [January 6th Insurrection](#january-6th-insurrection) <br> [Mocking Reporter Disability](#mocking-reporter-disability) <br> [Most Secure Election in History](#most-secure-election-in-history) <br> [Mostly Peaceful Protests](#mostly-peaceful-protests) <br> [Muslim Travel Ban](#muslim-travel-ban) <br> [Save Environment not Poor](#save-environment-not-poor) <br> [Steele Dossier](#steele-dossier) <br> [Trump and Koi Fish](#trump-and-koi-fish) <br> [Trump and Teargas](#trump-and-teargas) <br> [Trump Rally Empty](#trump-rally-empty) <br> |
|[False Flag](#false-flag-operations)|[Jussie Smollett](#jussie-smollett) <br> [Governor Whitmer Kidnapping Plot](#governor-whitmer-kidnapping-plot) <br> [Russia Pipeline](#russia-pipeline)|
|[No Evidence](#imaginary-evidence)|[Trump and Pee Tape](#trump-and-pee-tape) <br> [Kavanaugh Allegations](#kavanaugh-allegations) <br> [Trump Removed MLK Bust](#trump-removed-mlk-bust) <br> [Russian Collusion](#russian-collusion) <br> [New York Hush Money](#new-york-hush-money) <br> [Trump and Migrant Cages](#trump-and-migrant-cages) <br> [Mein Kampf Had Profound Affect](#mein-kampf-had-profound-effect) <br> [Hitler Did Some Good Things](#hitler-did-some-good-things) <br> [Trump Impeachment On Ukraine](#trump-impeachment-on-ukraine) <br> [Soldiers are Losers and Suckers](#soldiers-are-losers-and-suckers) <br> [Trump 2017 Tax Cuts](#trump-2017-tax-cuts) <br> [Polish First Lady Refuses Handshake](#polish-first-lady-refuses-handshake)|
|[Other](#other)|[Trump and Nuclear Secrets](#trump-and-nuclear-secrets) <br> [Chinese Weather Balloon](#chinese-weather-balloon) <br> [Russian Bounties on US Soldiers](#russian-bounties-on-us-soldiers) <br> [Cuomo COVID Leadership](#cuomo-covid-leadership) <br> [Ghost of Kyiv](#ghost-of-kyiv) <br> [al Baghdadi Religious Scholar](#al-baghdadi-religious-scholar) <br> [SUV Killed Parade Marchers](#suv-killed-parade-marchers) <br> [Dont Say Gay Bill](#dont-say-gay-bill) <br> [Putin Price Hike](#putin-price-hike) <br> [Trump and Wheel of Beast](#trump-and-wheel-of-beast) <br> [Officer Sicknick Murdered](#officer-sicknick-murdered)|
|[Misc](#misc)|[Latin Phrases](#latin-phrases)|



<br>


## Fallacies of Inconsistency


- [Activist Scholars for CRT](#activist-scholars-for-crt)
- [Affirms Not Defines](#affirms-not-defines)
- [Anti-Racism is Demonizing Whites](#anti-racism-is-demonizing-whites)
- [Biden Idea of Justice](#biden-idea-of-justice) 
- [Biden Leading A Nation](#biden-leading-a-nation)
- [Christianity as a Tool](#christianity-as-a-tool)
- [Complement and Compete](#complement-and-compete)
- [Context Matters and Does Not Matter](#context-matters-and-does-not-matter) 
- [Convictions for Unbacked Claims](#convictions-for-unbacked-claims)
- [Convictions for Untrusted Sources](#convictions-for-untrusted-sources)
- [DEI As Conditional Racism](#dei-as-conditional-racism)
- [DEI At The Same Time](#dei-at-the-same-time)
- [DEI Not Job Wide](#dei-not-job-wide)
- [Do Not Celebrate Ancestry](#do-not-celebrate-ancestry)
- [Educate and Indoctrinate](#educate-and-indoctrinate)
- [Election Interference by Others](#election-interference-by-others)
- [Elon Musk Threat to US](#elon-musk-threat-to-us)
- [Fight for Human Rights Sometimes](#fight-for-human-rights-sometimes) <br> 
- [Free and Hemmed In Scholars](#free-and-hemmed-in-scholars)
- [Free Speech with Censorship](#free-speech-with-censorship)
- [Government-Funded Propaganda Not News](#goverment-funded-propaganda-not-news)
- [Greatest Threat to US](#greatest-threat-to-us)
- [Gun Control Fixes Gun Crime](#gun-control-fixes-gun-crime)
- [Harmful is Harmless](#harmful-is-harmless)
- [Hasty and Superficial Press](#hasty-and-superficial-press)
- [Human Rights Are Important](#human-rights-are-important)
- [ID For Everything Except Voting](#id-for-everything-except-voting)
- [Influencers Investigating Influence](#influencers-investigating-influence)
- [Immigration Is Compassion](#immigration-is-compassion)
- [Islam Is A Religion Of Peace](#islam-is-a-religion-of-peace)
- [Kamala is Qualified](#kamala-is-qualified)
- [Killing is Reproductive Freedom](#killing-is-reproductive-freedom)
- [Law and Order and Rioting](#law-and-order-and-rioting)
- [Liberty and Justice For All](#liberty-and-justice-for-all)
- [Lost Money Need More](#lost-money-need-more)
- [My Body My Choice](#my-body-my-choice)
- [Others Are Radicalized](#others-are-radicalized)
- [Others Save the Planet](#others-save-the-planet)
- [Outrage or Silence](#outrage-or-silence) 
- [Oversight Opposing Accountability](#oversight-opposing-accountability)
- [Perpetrators of Misinformation](#perpetrators-of-information)
- [Prosecute or Excuse](#prosecute-or-excuse)
- [Psychopath Claims From Psychopaths](#psychopath-claims-from-psychopaths)
- [Racist Claims From Racists](#racist-claims-from-racists)
- [Results Matter Not Merit](#results-matter-not-merit) 
- [Save Environment Not Poor](#save-environment-not-poor)
- [Secure Elections are Vulnerable](#secure-elections-are-vulnerable)
- [Smart Kids Are Confused](#smart-kids-are-confused)
- [Supreme Beings are Understandable](#supreme-beings-are-understandable)
- [Tolerance and Inclusion Sometimes](#tolerance-and-inclusion-sometimes) 
- [Trans is Hard and Easy](#trans-is-hard-and-easy) 
- [Treat Symptoms without Root Causes](#treat-symptoms-without-root-causes)
- [Trump is a Dictator](#trump-is-a-dictator)
- [Trump is a Fascist](#trump-is-a-fascist)
- [Trump is a Felon](#trump-is-a-felon)
- [Trump will Weaponize Government](#trump-will-weaponize-government)
- [Upside Down Immigration](#upside-down-immigration)
- [Upside Down UK](#upside-down-uk)
- [Vote With Your Race](#vote-with-your-race)
- [Walz is Normal](#walz-is-normal)
- [What Women Want](#what-women-want)
- [Words Cause Violence](#words-cause-violence)

<br>

### Activist Scholars for CRT

- Movements have an activist dimension, they seek not to understand it, but to transform it 
- Using lived experience ("my truth") used to contradict reality.  Conveniently, it can't be fully understood 
- 2001, CRT admits it uses economics, history, context group and self interest, feelings and the unconscious 
- It questions equality theory, legal reasoning, enlightenment rationalism, and neutral principles of constitutional law 


<br>


### Affirms Not Defines 

If genitals don't define gender, how does removing them affirm it?

<br>


### Anti-Racism is Demonizing Whites

An individual's skills, abilities, and life experience are dismissed, even villified, because of skin color.   

Reducing individuals to membership in a group they didn't choose and have had from birth.

<br>


### Biden Idea of Justice 

- Biden prosecuted pro-lifers—including an 89-year-old concentration camp survivor—for singing hymns in a clinic hallway
- And then Biden turned around & handed out pardons for criminal murderers on his last day in office
- The first president to weaponize the Justice Department against a former President is also the first man to issue blanket pardons for his own entire extended family against Justice Department investigations into his own corruption



<br>


### Biden Leading A Nation

- [44 Years](#44-years)
- [Hunter Biden](#hunter-biden)
- [Not Dimentia](#not-dimentia)
- [What Vacation](#what-vacation)
- [Afghanistan Exit](#afghanistan-exit)
- [Ukraine War](#ukraine-war)
- [COVID Response](#covid-response)
- [Money Printing](#money-printing)
- [Money Laundering](#money-laundering)
- [Erosion of Rights](#erosion-or-rights)

<br>


### Christianity as a Tool

Abortion is evil  “why do you have to be Christian“

Close the border  “why can’t you be Christian“


<br>


### Complement and Compete

Men and Women are not the same and that's a blessing 

He provides and protects, she multiplies 
He is the head of the family, she is the heart 
He provides the shelter, she gives it a heart beat 
He provides the ingredients, she makes the meal
He provides the house, she makes it into a home 
He takes care of her outer world, she takes care of his inner world 
He gives her love, she gives him life 
Their roles complement rather than compete 

<br>


### Context Matters and Does Not Matter 

- When Hamas raped and killed they said context matters. When Elon makes movement alleged to be salute, context doesn’t matter anymore


<br>


### Convictions for Unbacked Claims

You share opinions with celebrities, major corporations, and media talking heads but are unable to discuss, explain, or present supporting evidence.

<br>


### Convictions for Untrusted Sources

In 2019, AOC is a broke bartender who gets elected to Congress with an annual salary of $155,000.
In 2024, AOC is worth $29 million dollars.

<br>


### Deception Claims From Liars 

Wants to be decider on truth but
- Says men can get pregnant
- Can't distinguish between those who use human shields and those deterred by use of human shields 
- Can't distinguish between those who target innocent civilians and those who inadvertently kill innocent civilians while trying to kill enemy 
- Named COVID shots that don't prevent infections as vaccines  

<br>


### DEI As Conditional Racism

- Usha Vance just became the first Asian American and Hindu American to serve as Second Lady of the United States in American history.
- The lack of celebration from those who typically champion diversity reflects the sentiments of the movement as not “advancing people of color,” but “advancing people of color who share my views.”
- This is not “anti-racism,” but conditional racism. It is to be insincere about diversity as an objective, reducing it to a Trojan horse for political partisanship.



<br>



### DEI At The Same Time

- Celebrate being <ins>maximally different</ins> but also <ins>eradicate differences</ins>, all at the same time

<br>


### DEI Not Job Wide

Why doesn't anyone complain about equity for these categories:
- Cable installer- 95% men
- Road builders- 96% men
- Construction- 94% men
- Garbagemen- 95% men
- Iron workers- 94% men
- Coal miners- 96% men
- Electricians- 96% men
- Firefighters- 88% men
- Plumbers- 99% men
- Combat- 84% men

<br>


### Do Not Celebrate Ancestry 

- You shouldn't feel proud of your ancestor's accomplishments 

- But you should feel crippling shame over their wrong-doings.



<br>


### Educate and Indoctrinate

- Schools indoctrinating instead of educating 




<br>


### Elon Musk Threat to US

- Elon paid the most taxes in history: $10 billion 
- SpaceX never received subsidies from govt, won all contracts with NASA by offering better service at lower price 
- SpaceX received only half the money that Boeing did to develop a crewed spacecraft and did ALL the completed missions
- Starlink illegally denied contracts by FAA
- Bailed out NC in hurricane, NASA astronauts 
- Bought Twitter, turned it into free speech platform 
- Works overtly to improve efficiency of bureaucratic machine through DOGE and X while Soros works covertly to implement two-tiered justice   

<br>


### Election Interference by Others

If Russia censors Hunter Biden laptop story it's election interference, not when we do it.

- On eve of election, evidence that president's family was compromised by foreign interests
- Gov/Big Tech systematic collusion and suppression of story as misinformation, New York Post twitter account locked 
- Tech companies were threatened to implement policies or face consequences 
- Unelected government actors used private sector actors to censor information that would impact election  
- Based on polling afterwards, it did impact the outcome of election 

<br>


### Fight for Human Rights Sometimes
 
- 29 people were hanged to death in Iran for opposing the Islamic regime, one of the deadliest single days of executions in recent history.

- Where’s the UN? Where’s the university students and pro-Palestinian protesters? Where’s the so-called human rights activists?!


<br>



### Free and Hemmed In Scholars 

- Scholars hemmed in by the idols of prevailing fad. The need to accomodate mass standards prevents independent-minded contributions.

- Herd instincts that run contrary to development.

<br>


### Free Speech with Censorship

- First Amendment is getting in the way.

<br>

### Government-Funded Propaganda Not News


- Politico funded by USAID is the biggest scandal in news media history
- People conducting "fact checks" on the government being paid by the government
- State-funded narrative control, designed to protect the regime and attack its enemies
- Reverse Robin Hood - taking from hard working Americans and giving to Christina Aguilera, Katy Perry, Oprah
- Hunter / Biden admin / non-partisan agencies like FBI two-tier justice and efforts to cover up drug use, lying, gun charges, laptop, tax evasion, energy deal for Burisma

<br>

### Greatest Threat to US 

- Democrats Weaponized FBI To Falsely Claim White Supremacists Posed Greater Terror Threat Than Radical Islamists

- The Department of Homeland Security (DHS) declared, for the first time in its history, that domestic violent extremists, rather than foreign terrorists, posed “the most persistent and lethal threat” to the United States in its October 2020 Homeland Threat Assessment. 

- The “top threat we face from DVEs [Domestic Violent Extremist] continues to be those we identify as Racially or Ethnically Motivated Violent Extremists (‘RMVEs’), specifically those who advocate for the superiority of the white race,” FBI Director Christopher Wray told Senate Judiciary Committee on March 2, 2021.

- "According to the intelligence community," said Biden in 2021, "terrorism from white supremacy is the most lethal threat to the homeland today. Not ISIS, not al Qaeda — white supremacists."

- In August 2024, the New Yorker reported "around 2018 the F.B.I. began seeing an increase in racially or ethnically motivated violent extremists—in particular, ‘individuals espousing the superiority of the white race.’"

- For over 4 years, the FBI said the threat of terrorism from white supremacists exceeded that from radicalized Muslims.
But the data never supported the claim.


|||||
|-|-|-|-|
|2001|World Trade Center|2977|AQ|
|2009|Fort Hood|13|AQ|
|2013|Boston Marathon Bombing|3|Islamic|
|2015|San Bernardino|14|ISIS|
|2016|Orlando|49|ISIS|
|2017|NYC Truck|8|ISIS|
|2019|Pensacola Naval Air Station shooting|3|Islamic|
|2025|New Orleans Truck|15|ISIS|
||Total|3082||

<br>

|||||
|-|-|-|-|
|2012|Wisconsin Sikh Temple Shooting|6|White Supremacist|
|2015|Charleston Church Shooting|9|White Supremacist|
|2018|Pittsburgh Synagogue Shooting|11|Anti-Semitic|
|2019|El Paso Walmart Shooting|23|Anti-immigrant|
|2022|Buffalo Supermarket Shooting|10|Anti-immigrant|
||Total|59||

<br>

- Even if the numbers were reversed, number of cases of domestic terrorism is too small a samlpe size to claim any kind of discernible trend about DVE.

- Most of the white supremacist killers appeared to suffer from mental illness (alcoholism, depression, paranoia, schizoaffective disorder), whereas most of the radical Islamicists appeared sane and driven overwhelmingly by ideology.

- In 2022, there were 24,849 homicides.  White supremacist related were 0.08% of these.

- Multiple former FBI officials have testified that the FBI encourages and rewards its agents and executives to increase the number of cases that can be called cases of domestic extremism.

- In the past, the FBI entrapped Muslims and mentally impaired teenagers in terrorism plots. Previously, Public reported that the FBI was deliberately padding its Domestic Violent Extremism (DVE) numbers by incentivizing field offices to inflate statistics and pursue investigations without sufficient proof.

- In 2012 and 2013, when Robert Mueller was FBI Director, explained Hill, the FBI hired McKinsey consultants to create a series of metrics for evaluating its agents. Those metrics incentivize entrapping mentally disabled individuals.

- "Ninety percent of them are EDP, emotionally disturbed persons," said Hill, who was a Supervisory Intelligence Analyst (SIA) at the Bureau. "From them, a few come out to be jihadist wannabes." 

- FBI misled the public, policymakers, and other law enforcement and intelligence agencies.

- We need sweeping reform of our politically corrupt FBI.

https://x.com/shellenberger/status/1874951863121899520

<br>


### Gun Control Fixes Gun Crime

- If guns are outlawed, criminals won't own any.

<br>


### Harmful is Harmless 

|||
|-|-|
|Thalidomide||
|DDT||
|Tobacco||
|Asbestos||
|Mercury||
|Vioxx||
|Opioids||
|Vaccines||


<br>


### Hasty and Superficial Press

|||
|-|-|
[Use Sensational Formulas](#use-sensational-formulas)|- In-depth analysis of a problem runs contrary to media's nature, goals, objectives<br>- "The press merely picks out sensational formulas."<br>- People trust the media, who are clearly paid to sensationalize stories and draw clicks and viewership|
|[Back Claims With Claims](#back-claims-with-claims)|When they make a claim, they support it with unsupported claims instead of supporting evidence|
|[Never Contradict Trends](#never-contradict-trends)|- The media does not openly contradict its own and general trends<br>- Gives birth to strong mass prejudices, blindness |

<br>


### Human Rights Are Important 

- China and others lecturing us on human rights 
- Rich want socialism 
- Inherited money is wrong but taking from others is right 
- Majority of the world treats minorities way worse (China) 
- Majority of the world treats gays way worse (Islam)

<br>


### ID For Everything Except Voting

- Require ID for banks, airports, doctors, pharmacy, school, gun store, fishing, hunting, daycare, hospital, drinking 
- But not for voting

<br>


### Immigration Is Compassion

- Millions of immigrants let in, shipped to swing states, provided free services (food stamps, EBT, housing), a path to amnesty, and allowed to vote



<br>


### Influencers Investigating Influence

- DOJ investigating Russian influence in American media 
- Tenet Media founded by Lauren Chen and Liam Donovan in Jan 2022 

<br>


### Islam Is A Religion Of Peace

"Islam is not a religion of peace, it's a political theory of conquest that seeks domination by any means it can."

- Ayaan Hirsi Ali 

List of attacks:

- Nice Attack
- Paris Attaks
- Shoe Bomber
- Orlando Attack
- Beltway Snipers
- Fort Hood Shooter
- Underwear Bomber
- Westminster Attack
- 2005 Bali Bombings 
- Murder of Lee Rigby
- USS Cole Bombers 
- London Bridge Attack 
- Madrid Train Bombers 
- Charlie Hebdo Attacks
- San Bernadino Attacks
- Davao Philippines Attack 
- 2018 Surabaya Bombings 
- Minnesota Mall Stabbings 
- London Subway Bombers 
- Moscow Theatre Attackers 
- Boston Marathon Bombers 
- Ankara Airport Turkey Attack 
- Manchester Arena Bombing
- Pan-Am Flight 103 Bombers 
- Iranian Embassy Takeover 
- Air France Entebbe Hijackers 
- 2002 Bali Nightclub Bombers 
- Batta Meena Pakistan Attacks 
- Beirut US Embassy Bombers 
- Libyan US Embassy Attack
- Yazidi Sinjar Massacre of 2014 
- Beheading of French Priest 
- Buenos Aires Suicide Bombers 
- Israeli Olympic Team Attackers
- Kenyan US Embassy Bombers 
- Saudi, Khobar Towers Bombers 
- Beirut Marine Barracks Bombers
- Besian Russian School Attackers 
- First World Trade Center Bombers 
- Beheading of Daniel Pearl 
- Achille Lauro Cruise Ship Hijackers 
- Bombay & Mumbai Indian Attackers 
- September 11 2001 Airline Hijackers 
- 1915 Ottoman Government Genocide killing 2M Armenians in Turkey  

<br>

20 largest religious terrorist organizations all adhere to Islam religion:

1. ISIS 
2. Al-Qaeda
3. Boko Haram
4. Taliban
5. Al-Shabaab
6. Hezbollah
7. Hamas
8. Lashkar-e-Taiba
9. Islamic Jihad Movement in Palestine 
10. Tehrik-i-Taliban Pakistan 
11. Jaish-e-Mohammed
12. Jemaah Islamiyah 
13. Abu Sayyaf Group 
14. Haqqani Network 
15. Harkat-ul-Mujahideen 
16. Ansar al-Sharia 
17. Ansar al-Islam 
18. Harkat-ul-Jihad al Islami 
19. Ansar Bayt al-Maqdis 
20. Tehreek-e-Nafaz-e-Shariat-e-Mohammadi



<br>


### Kamala is Qualified 

Kamala Harris plagiarized material in:

- Her book Smart on Crime
- An attorney general report on human trafficking 
- An attorney general report on organized crime
- Testimony before the House
- Testimony before the Senate

<br>

Ran a deceptive campaign:

- Didn't receive a single vote for nomination as Democratic candidate
- Pushed different stances on issues in different areas/demographics 
- Spent millions buying celebrity endorsements and staging concerts that didn't work  
- Spent over $1 Billion but ended up $20 Million dollars in debt 
- Lied to donors to keep the cash flowing 
- Paid half a million to put her face on the Sphere in Vegas, got killed in Nevada 
- Paid for drone shows in Pennsylvania 
- After loss, asking for donations for a recount while she lost by a landslide (3 million votes)

<br>

### Killing is Reproductive Freedom



<br>


### Law and Order and Rioting 

- Claim to want law and order but show support for disorder, violence, and destruction

- Diversity/Equity recruiting at the FBI has degraded standards in physical fitness, illicit drug use, mental health and integrity, and threatens the FBI’s ability to protect America from harm, says a report by ex- and serving agents

https://nypost.com/2024/01/24/news/dei-hires-are-making-the-fbi-more-woke-than-qualified/?utm_campaign=iphone_nyp&utm_source=twitter&utm_medium=social


<br>


### Liberty and Justice For All

American institutions are broken and corrupt.  The political class is betraying working class Americans.

- Liberal prosecutors weaponize political power to target opponents and coddle criminals
- Use of unelected beauracracy to impose will on us without our consent 
- Federal police force sent to round up innocent people and put them in jail for opposing your leader 
- Imprisoning your political opponent 
- Censoring true stories that are unfavorable to the regime 
- Calling a segment of the population an "existential threat to the nation"
- Forcing unwanted medical treatments 
- Having a propaganda force in the media while silencing dissent 


<br>


### Lost Money Need More


<br>


### My Body My Choice

- "It's my choice on abortion, but not on mandated vaccines."

<br>



### Others Are Radicalized 

Of all the Islamic nations in the Middle East, you support the one that is:

- Sworn death the USA since 1979 
- Has the worst records for human rights 
- Has the worst records for tolerance, diversity, etc
- Only chance to win is with psychological operations 



<br>


### Others Save the Planet

||||
|-|-|-|
|Mark Zuckerberg|$300 million megayacht powered by four gigantic diesel engines|"Stop climate change before we destroy the planet"|
|Taylor Swift|2 Private jets flew 178K miles in 2023, emitting ~1.200 tons of CO2 in the process|Concerned about climate change|


<br>


### Outrage or Silence

- The outrage over Daniel Penny acting to keep subway safe and the silence when an illegal immigrant sets a sleeping woman on fire who burns to death
- 40K child slaves forced to work in cobalt mines in Congo so we can drive electric cars



<br>


### Oversight Opposing Accountability

- Apr 2024, GAO 2018-2022 data shows Govt loses up to $500B annually to fraud 
- Activist groups and think tanks actively oppose strengthening digital authentication measures, even on programs where losses are estimated at $100B annually
- GAO is 31 departments and over a billion budget 
 
<br>


### Perpetrators of Misinformation

Ironically, USG who claims to fight against it.

- Vaccines prevent infection and stop transmission 
- Masks work 
- Virus did not originate in Wuhan Lab 
- Ivermectin is not a solution 
- Kids are at risk 
- School closures do not adversely affect kids 
- Natural immunity is not a solution 

<br>


### Prosecute or Excuse

- All of the rioting and illegal immigrant crimes but prosecute peaceful protests and parents
- Soros funded lawyers trying subway guy for 15 years claiming racial reasons. They are actually doing it for racial reasons themselves.


<br>


### Psychopath Claims From Psychopaths 

<br>


### Racist Claims From Racists 

<br>



### Results Matter Not Merit 

- Outrage about wage gaps, income gaps, and wealth gaps. Silent on skill gaps, effort gaps, and tale gaps
- DEI has placed an asterisk on the achievements of every black person and woman in a prestigious role
- It’s caused those people and the public to reasonably doubt whether they earned their role by merit or agenda
- This hurts black people, women, and public trust


<br>


### Save Environment Not Poor 

Sacrificing the poor for political ideologies 
- Coal is better in all regards, then transition to natural gas, then nuclear 
- Left always picks environment over the poor 
- We can prove fossil fuels save lives, they can't prove restrictions save lives 
- Priority is meeting people's basic needs 
- Cheap energy and cheap food helps poor people 
- High energy costs lead to lower quality of life 
- We're a rounding error. Nothing we do is going to make a difference in atmosphere 
- Be a good example? We're not plus we're not innovative enough

<br>


### Secure Elections are Vulnerable

- No requirement to prove residency or citizenship
- Phaseout of in-person voting in favor of absentee ballots 


<br>

### Smart Kids Are Confused 

Our smartest kids can't figure out who the bad guys are
- Qatar is the top funder to universities now
- Ignoring use of human shields, civilian targeting, innocents killed in their beds
- No understanding of private property incentive structures 
- Champions Russia/China/N.Korea incentives of serving the state and their corrupt needs
- Enables and supports weaponization of high integrity values against our establishment 
- Support for Islamist terrorists claiming torture
- Gorbachev richest man in Russia, gave to political opponents of Putin, stripped of everything, jailed 10 years, deported, exiled
- Jack Mar, billionaire, criticizes banking regulations, disappears for a year, no longer criticizing 

<br>



### Supreme Beings are Understandable

- If you could accurately understand its nature and identify its works, it’s hardly a supreme being, don’t you think?
- But to claim that if you could, that it would be too simple of an explanation for something is… laughable.

<br>


### Tolerance and Inclusion Sometimes 

- Activists pour soup on things but tolerance and inclusion is the goal


<br>


### Trans is Hard and Easy

1) No predator would go through the devastating, nigh Herculean struggle of becoming trans in order to access women and children.

2) All it takes to be trans is to say you are.

Others

- Predatory men won’t make a simple declaration to access vulnerable women and children, this includes criminals in prison
- People are not attracted to another persons sex but the stereotypes they perform
- Two men who have sex while pretending to be women are lesbians
- You don’t need to disclose your sex before going to bed with someone
- People with difference in sex development are neither male nor female
- Children who are attracted to the wrong sex stereotypes should have their puberty stoped in preparation for their genitals to be surgically altered
- Men included in women’s sport have no natural advantage
- Trans identified men are literally women
- Men turn into women with a declaration and are then free of male pattern violence and male pattern sexual deviance
- Everyone is born with a sexed soul that must be affirmed over the sex of the body or the person will die
- Transgenderism is innate like left-handedness



<br>

### Treat Symptoms without Root Causes

We have poisoned our food supply
- Engineered highly addictive chemicals to put into our food
- Spray it with pesticides that kill pests and destroy gut lining and our microbiome
- The GI tract is reacts, low grade chronic inflammation that permeates and drives chronic diseases  

We have the most over-medicated, sickest population in the world and don't know why 
- Haven't found the root causes of the chronic disease epidemic
- Doctors use endless short visits, are driven by billing and coding, and measured by throughput

<br>


### Trump is a Dictator 

- Trump works to cut waste, root out corruption, and drain the swamp by shutting down USAID 
- The "dictators" are the unelected bureaucrats at USAID. No one voted for them. No one even knows their names 
- They regularly lied about where your money was going
- They spent money without congressional authorization in order to overthrow foreign governments
- And yet, no one in the Democrat Party ever objected to it

<br>


### Trump is a Fascist

Whose ideas were these:

- controlling the media 
- censoring information 
- going after guns 
- using DOJ/FBI to go after opposition
- mandating vaccines and masks 
- fine/imprison people who reject gender insanity 
- fine/imprison/quarantine the unvaccinated 
- take children from unvaccinated parents 
- fine/imprison critics of vaccine 









<br>


### Trump is a Felon 

Trump is convicted for paying his attorney but 

- Hillary Clinton deleted 30K emails 
- Hunter Biden is a crack addict with hookers
- Joe Biden sold access to US Government to Chinese communists
- Nancy Pilosi is rich from insider trading 
- Steven Dantwana FBI staged Gretchen Whitmer kidnapping and January 6th 
- Mayorkas allowed the invasion of our border
- Epsteins clients walked free 
- Dr Fauci lied about funding GoF research and experiments that caused the COVID-19 virus  
- Pfizer and Moderna lied about safety of their mRNA shots which killed/injured millions
- Andrew Cuomo killed 11,000 elderly in NY 
- Planned Parenthood sold aborted baby body parts 


<br>


### Trump will Weaponize Government 

Instances of Biden Admin weaponizing government:

- DOJ lawsuits against Elon Musk
- DOJ criminal charges against Trump
- DOJ intimidation of whistleblowers like Eithan Haim 
- FBI spying on churches, Rudy Giuliani, Trump campaign
- Dirty 51 letter to cover up election interference scandal 
- FBI raids on Christians, pro-life 
- Excessive prison sentences on peaceful pro-life demonstrators
- Censoring/suppressing factual information to silence opposition on COVID shot, elections, crime statistics 
- Using FDA/CDC to villify Ivermectin/others and push throuh COVID shot as only option  


<br>


### Upside Down Immigration

- We have an upside down system that makes it hard for highly talented people to come to America legally, but trivial for criminals to come here illegally.

- Why is easier to get in illegally as a murderer than legally as a Nobel Laureate?

Rohit Krishnan

<br>


### Upside Down UK

- The UK will arrest you for retweets but will protect migrant gangs and child r*pists

- 250,000+ British girls have endured decades of gang rape by Pakistani men, with no justice or recourse

- Weaponization and politicization of judicial system

- Stories reported to make you think a certain way, to fit a narrative

- Media, justice system, and politicians conspire to censor free speech, pervert democracy 

- Cultures and societies that do not allow free speech, use our free speech to damage and divide us 



<br>


### Vote With Your Race 

- Obama says blacks should vote for a black president. But Kamala chose a white husband. And MLK says the direct opposite. 

    "Let me speak to the brothers.. when you have a choice that is this clear.. (Harris) grew up like you, knows you, went to college with you, understands the struggles, pain, and joy that comes from those experiences.  Who's had to work harder, and do more, and overcome and achieves the second highest office in the land.  And is putting forward concrete proposals to directly address the things that are vital in our neighborhoods and our communitites." Housing, afford medicine, high prices, etc "You're supporting someone who has a history of denegrating you?"

- 2008 Election, black turnout exceeded white turnout (69 to 65 percent) for the first time in history.  Gap was even larger in 2012.

- 1996 to 2008, black turnout jumped from 48 to 69 percent.

<br>


### Walz is Normal

- Walz is a middle-class normal guy who likes working on his truck.

- Walz is a servant to global elitist billionaire international terrorists like Alexander Soros.

<br>


### What Women Want 

- Murder babies and be celebrated

- Invite illegals in that will not think twice to kill and rape them

- Allow mentally ill men in private spaces and sports so they can hurt women

<br>


### Words Cause Violence

MSM and Democrats says Trump is responsible for:
- January 6th riots
- Bomb threats against hotels housing Haitians
- 2022 attack on Nancy Pelosi's husband by mentally ill / drug-addicted man 

<br>

But they deny responsibility for:
- dehumanized Trump
- depicting him as a Hitler
- joking about holding up his bloodied head.


<br>


# Quote Mining 

AKA Contextomy. Informal fallacy. Removing passage from its surrounding matter to distort its intended meaning.

- [Trump and Fine People](#trump-and-fine-people)
- [Trump and Drinking Bleach](#trump-and-drinking-bleach)
- [Trump and Georgia Votes](#trump-and-georgia-votes)
- [Whats In It For Them](#whats-in-it-for-them)
- [Why Did He Join Military](#why-did-he-join-military)
- [No One Wants To See That](#no-one-wants-to-see-that)
- [I Just Want Your Votes](#i-just-want-your-votes)
- [You Wont Have To Vote Anymore](#you-wont-have-to-vote-anymore)

<br>


## Trump and Fine People 

- Aug 2017, Trump: "very fine people on both sides" in Charlottesville, VA rally protesting removal of Confederate statue
- Trump was referring to protesters and counterprotesters, not neo-Nazis and white nationalists
- Trump said neo-Nazis/white nationalists should be "condemned totally"

<br>


## Trump and Drinking Bleach

- Apr 2020, Trump asked COVID task force to look into disinfectants to be injected inside people to treat COVID-19
- Reporter asked whether cleaning products like bleach and isopropyl alcohol would be injected into a person
- Trump said those products would be used for sterilizing an area, not for injections

<br>


### Trump and Georgia Votes 




<br>


### Whats In It For Them 

|||
|-|-|
|Feb 2018|- Kelly involved in multiple controversies (DACA, Lewandowski, Rob Porter)<br>-Known hardliner on immigration, supported separating children from parents to deter illegal immigration<br>- Rejected idea that family separation was inhumane|
|June 2018|- At North Korea–U.S. summit, Kelly said White House was a miserable place to work|
|July 2018|- Months of speculation Kelly would resign from his job of White House Chief of Staff<br>- His WH influence was waning, Trump made several key decisions without him|
|Dec 2018|- Kelly & Trump not speaking, Kelly expected to resign, Trump replaced him a week later|
|Sep 2020|- 2 months before election, story about Kelly runs in Atlantic article 
<br>

- Trump and Kelly visit section 60 where Kelly’s son Robert is buried (killed in 2010 in Afghanistan). Trump wanted to join John Kelly in paying respects at his son’s grave, and to comfort the families of other fallen service members. 

- "According to sources with knowledge of this visit" while they stood by Robert's grave, Trump turned and said, “I don’t get it. What was in it for them?”.  Kelly initially believed Trump meant selflessness of military but later realized Trump doesn't get it.

<br>

## Why Did He Join Military 

- General Joe Dunford, Trump turned to aides and said, “That guy is smart. Why did he join the military?”

<br>

## No One Wants To See That 

- 2018 saying not to use wounded veterans because no one wants to see that 

<br>

## I Just Want Your Votes

- Jun 2024, Trump commented on excessively hot temperatures in Las Vegas (over 100 F)
– USSS expressed concern for crowd's safety, Trump joked about not wanting them to leave
- Claimed he didn't care about them, only their vote which the crowd met with laughter


"And by the way, isn't that breeze nice? Do you feel the breeze? Cause I don't want anybody going on me, we need every voter. I don't care about you, I just want your vote. I don't care. See now the press will take that, they'll say he said a horrible thing."

<br>


## You Wont Have To Vote Anymore

- July 2024, while criticizing Democrat approach to voter-identification laws
- Critics framed it as evidence of an alleged plan to change constitutional provisions governing presidential terms
- Full quote 

"Get out and vote! Just this time. You won't have to do it anymore! Four more years, you know what? It'll be fixed, it'll be fine, you won't have to vote anymore."

<br>


# Shape and Suppress 

- [2020 Election Most Secure in History](#2020-election-most-secure-in-history)
- [Border Agents Whipped Migrants](#border-agents-whipped-migrants)
- [Bubba Wallace Garage Noose](#bubba-wallace-garage-noose)
- [COVID Lab Origin](#covid-19-lab-origin)
- [COVID Prevented by Masks](#covid-prevented-by-masks)
- [COVID Shot Stops Transmission](#covid-shot-stops-transmission)
- [Covington Kids](#covington-kids)
- [Hunter Biden Laptop](#hunter-biden-laptop)
- [Ivermectin is Horse Dewormer](#ivermectin-is-horse-dewormer)
- [January 6th Insurrection](#january-6th-insurrection)
- [Mostly Peaceful Protests](#mostly-peaceful-protests)
- [Muslim Travel Ban](#muslim-travel-ban)
- [Steele Dossier](#steele-dossier)
- [Trump and Koi Fish](#trump-and-koi-fish)
- [Trump and Teargas](#trump-and-teargas)
- [Trump Mocked Reporter Disability](#trump-mocked-reporter-disability)
- [Trump Rally Empty](#trump-rally-empty)



<br>

## 2020 Election Most Secure in History

- Nov 2020, [CISA reported](https://www.cisa.gov/news-events/news/joint-statement-elections-infrastructure-government-coordinating-council-election) election was the most secure in American history
- Claim: States with close results have paper records of each vote, can recount ballots and correct mistakes or errors
- Claim: No evidence any voting system deleted or lost votes, changed votes, or was in any way compromised

<br>


## Bubba Wallace Garage Noose

- June 2020, [NASCAR reports](https://x.com/NASCAR/status/1275041182880579584) noose was found in the 3 team garage, claims racism 
- Feds announced investigation showed noose was hanging there since Oct 2019, long before it was assigned to Wallace

<br>


## Border Agents Whipped Migrants 

- Sept 2021 Border agents in Texas engaging group of migrants crosse into Mexico to purchase food
- Viral [photograph](https://media.snopes.com/2021/09/GettyImages-1235366682-border-patrol-haitian-refugees.jpg) of a white, cowboy-hat-wearing border agent on horseback grabbing back of man's shirt
- A leather rein flies out freely, appearing to make contact with the migrant
- Photo evoked historical comparisons to American slavery

<br>


## COVID-19 Lab Origin 

- For 3 years, USG official narrative was COVID virus spontaneously arose out of nature. 
- Now agencies responsible for oversight (FBI, CDC, DOE, NSA) admit it was developed in a lab.

<br>


## COVID Prevented by Masks


<br>


## COVID Shot Stops Transmission



<br>



## Covington Kids

- Jan 2019, students from Covington Catholic HS in KY attended anti-abortion rally at Lincoln Memorial, had MAGA hats on 
- Group of black nationalists & indigenous rights activists shouting insults at boys
- A Native American activist Phillips approached teens while percussing a drum and singing a Native American song
- He stood face-to-face with Sandmann 
- Viral video misleadingly showed smirking Sandmann/students surrounding Phillips blocking his path
- Drew national anger, death threats directed at Sandmann, sued CNN, NBC, Washington Post 
- CNN settled for $275 million 

<br>


## Hunter Biden Laptop

An organized effort by intelligence community (IC) to discredit leaked information about Hunter Biden before and after it was published

- Hunter got tens of millions of dollars from Barisma, a Ukranian natural gas company
- Computer shop owner found criminal evidence on laptop, called Giuliani, who was under FBI surveillance
- FBI took possession of laptop in December 2019
- Giuliani called New York Post who ran story on October 14, 2020, 
- CIA/FBI broke Wiretap Act & Hatchet Act, misused & improperly disclosed information obtained from spying on Giuliani 
- FBI spread information it knew to be false to interfere with the 2020 election 
- Warned FB/Twitter/others that forthcoming info on Barisma/Biden laptop was Russian hack-and-leak operation 
- The Stanford Internet Observatory said focus on the perpetrators rather than the contents of the leak
- Aspen Institute hosted tabletop exercise (pre-bunking operation) to shape reporting around a potential Biden hack-and-leak
- Facebook, Twitter,NYT, WP, CNN, in the summer of 2020, months before the October 14 publication.
- Ex-FBI Jim Baker and Dawn Burton joined Twitter, were ones who initiated investigating Trump

<br>


## Ivermectin is Horse Dewormer




<br>



## January 6th Insurrection 

- Trump did not explicitly tell people to "storm" or "breach" or "break into" the Capitol.
- president called on supporters to "peacefully and patriotically" march or walk to the Capitol 
- encouraged supporters to descend on the Capitol grounds and "cheer" on senators who would break laws governing U.S. elections
- but did not explicitly tell people to commit crimes themselves.
- used phrases "you have to show strength" and "demand that Congress do the right thing"
- In a video displayed during U.S. President Donald Trump's second impeachment trial on Feb. 9, 2021, House Democrats did not show footage of Trump telling supporters on Jan. 6 to "peacefully and patriotically make your voices heard."

<br>


## Mostly Peaceful Protests 

- Aug 2020, Kenosha, WI cop shot Jacob Blake several times in the back, it was recorded on cellphone video
- Several days of riots, fires, destroying buildings 
- CNN reporter stood in front of burning parts of city while ticker read "mostly peaceful protests"

<br>


## Muslim Travel Ban 

- Jan 2017, Trump EO limited travel into the US from Iran, Iraq, Libya, Syria, Somalia, Sudan and Yemen
- Hawaii led the challenge claiming policy was a “Muslim ban” discriminating against religion
- SCOTUS upheld 5-4 rejecting arguments that it was prompted by discrimination against Muslims or went beyond his authority

<br>


## Steele Dossier


<br>
















## Trump and Koi Fish 

- Nov 2017, Trump dumped box of fish food into a koi pond at the Akasaka Palace in Japan
- Tweets such as Alex Pfeiffer (@PfeifferDC) and [@VeronicaRochaLA](https://x.com/VeronicaRochaLA/status/927400669996130305) made false claims about meaning/context
- Trump emptying box of fish food into pond was somehow rude, rash, or disrespectful
- Video shows Trump was following Japanese prime minister Shinzo Abe's lead 

<br>


## Trump Mocked Reporter Disability

- November 2015, Trump said there were people cheering across the river on 9/11.
- Referenced a [Washington Post article](https://www.washingtonpost.com/archive/politics/2001/09/18/northern-new-jersey-draws-probers-eyes/40f82ea4-e015-4d6e-a87e-93aa433fafdc/) by Serge Kovaleski that said the same thing.
- Kovaleski pointed out there weren't "hundreds, thousands"
- Trump impersonation was similar to arthrogryposis condition in Kovaleski's right arm and hand 

<br>






## Trump and Teargas 

- Jun 2020, Law enforcement forcibly removed protesters from front of St. John's Church in Lafayette Park, LA
- Many claimed Trump responsible for the use of force on protesters "for a photo-op"
- [Department of Interior report](https://www.aljazeera.com/news/2021/6/10/report-concludes-protesters-not-removed-for-trump-photo-op) found US Park Police had planned it in advance

<br>

## Trump Rally Empty

- [Washington Post reported](https://www.snopes.com/news/2017/12/09/was-trumps-pensacola-rally-packed-rafters/) rally was empty and used picture of arena hours before it began 

<br>




# False Flag Operations 

- [Jussie Smollett](#jussie-smollett)
- [Governor Whitmer Kidnapping Plot](#governor-whitmer-kidnapping-plot)
- [Russia Pipeline](#russia-pipeline)

<br>

## Jussie Smollett

- Jan 2019, actor staged a fake hate crime against himself in Chicago
- Convicted of 5 felony accounts of disorderly conduct and 150 days in jail 
- Paid $3500 to 2 friends from Empire show shout racist/homophobic insults, pour bleach, noose 
- Kamala Harris and Cory Booker both described the attack as an attempted modern-day lynching
- Booker urged Congress to pass a federal anti-lynching bill co-sponsored by him and Harris

<br>

## Governor Whitmer Kidnapping Plot

- Dec 2020, Federal Grand Jury indicted five Michigan residents on conspiring to kidnap Governor Whitmer 

<br>


## Russia Pipeline

<br>

# Imaginary Evidence 

- [Trump and Pee Tape](#trump-and-pee-tape)
- [Kavanaugh Allegations](#kavanaugh-allegations)
- [Trump Removed MLK Bust](#trump-removed-mlk-bust)
- [Russian Collusion](#russian-collusion)
- [New York Hush Money](#new-york-hush-money)
- [Trump and Migrant Cages](#trump-and-migrant-cages)
- [Mein Kampf Had Profound Affect](#mein-kampf-had-profound-effect)
- [Hitler Did Some Good Things](#hitler-did-some-good-things)
- [Trump Impeachment On Ukraine](#trump-impeachment-on-ukraine)
- [Soldiers are Losers and Suckers](#soldiers-are-losers-and-suckers)
- [Trump 2017 Tax Cuts](#trump-2017-tax-cuts)
- [Polish First Lady Refuses Handshake](#polish-first-lady-refuses-handshake)


<br>

## Trump and Pee Tape

<br>


## Kavanaugh Allegations

- Oct 2018, investigation into sexual assault allegations against Supreme Justice nominee Brett Kavanaugh
- Primary accuser, psychology Dr. Christine Blasey Ford, accused him of sexual assault at HS party in MD in 80s
- FBI did background investigation into Judge Kavanaugh (7th in 25 years going back to 1993)
- FBI reached out to 11 people, 10 were interviewed, all witnesses with potential firsthand knowledge of the allegations
- Investigation confirmed Senate Judiciary Committee conclusion: no corroboration of allegations made by Dr. Ford or Ms. Ramirez

<br>


## Trump Removed MLK Bust 

- [Time Magazine reported](https://www.snopes.com/fact-check/mlk-bust-oval-office/) Trump removed MLK bust from White House 

<br>


## Russian Collusion

- Trump colluded with Russia to win in 2016 and might be a Russian agent
- Crooked FBI agents and appointment of Special Counsel Robert Mueller, 2 years later concluded no evidence found 
- 2017: US Intel assessed Putin and his government developed a "clear preference" for Donald Trump
- 2018: Govt-funded analysis companies (New Knowledge, Graphika) reported Russians elected Trump via 10M people (Facebook ads)
- Exaggerated impact of foreign interference while no evidence of any impact on elections by Russia exists
- USG provided 3M in grants to UW and SIO to censor election misinformation
- USG provided 5M in grants and 2M in contracts to Graphika

<br>

## New York Hush Money 

- Adult-film actress Stormy Daniels paid $130K to keep quiet about claim of sex with Trump, which he denies
- Hush-money is not illegal, case was how Trump's former lawyer paid Ms Daniels, recording it as legal fees
- Prosecution's key witness Michael Cohen testified Trump knew about scheme to disguise the payment
- Jurors found Trump guilty of all 34 counts of fraud under campaign finance laws
- In NY, falsifying business records felony requires intent of committing/concealing a separate underlying crime
- The Manhattan district office charged Trump without specifying a particular underlying crime 

<br>

## Trump and Migrant Cages 

- Trump pointing to border crisis, Dems disagreeing and refusing requests for funding 
- Thousands of children were taken from their parents under the Trump administration's 2018 zero-tolerance policy
- Months of Democrats insisting border crisis was Trump thing, refused to even vote on its requests for funds
- Jun 2019, (Wendi) Warren Binford visited U.S. CBP facility in Clint, TX to assess conditions of immigrant detention center
- Binford reported unlawful mistreatment of migrant children which led to national backlash, resignation of Dir John Sanders
- Democrats (AOC, others) rushed to border, created drama and wild accusations, tweeted photos of kids in cages 
- Revealed that Obama-Biden admin built the cages and the photos were from 2014

<br>


## Mein Kampf Had Profound Affect

Original source is a meme that surfaced in April 2019, falsely claiming Trump said this in a 2002 interview with Time magazine.

Interestingly, Snopes found that people (including some close to him) have been insinuating that Trump has an affinity for Hitler for the better part of 30 years

<br>


## Hitler Did Some Good Things

Also, "I imagine myself like him" 

Secondhand accounts from General John Kelly, former Trump Chief of Staff

2021 - allegedly while on trip to Paris to commemorate the armistice after WWI, shared with Michael Bender who was writing a book called Frankly, We Did Win This Election: The Inside Story of How Trump Lost.

2024 - allegedly ho shared with CNN correspondent Jim Sciutto for his book "The Return of Great Powers" released March 2024.

<br>


## Trump Impeachment on Ukraine 

What Trump actually did was threaten the scam Joe Biden and son Hunter created that involved selling the suggestion that Joe’s influence could be had by hiring Hunter. 

That’s the sort of thing reporters are supposed to expose, not protect. 
“The impeachment inquiry into Donald J. Trump, the 45th President of the United States, uncovered a months-long effort by President Trump to use the powers of his office to solicit foreign interference on his behalf in the 2020 election,” the report stated.

“President Trump’s scheme subverted US foreign policy toward Ukraine and undermined our national security in favor of two politically motivated investigations that would help his presidential reelection campaign.”

<br>


## Soldiers are Losers and Suckers 

- Sept 2020, Jeffrey Goldberg [Atlantic (Emerson Collective)](#emerson-collective) wrote [hit piece](https://web.archive.org/web/20230401023100/https://www.theatlantic.com/politics/archive/2020/09/trump-americans-who-died-at-war-are-losers-and-suckers/615997/) claimed in 2018 Trump canceled trip to Aisne-Marne American Cemetery near Paris where WWI vets buried
- Claim: Trump said "Why should I go to that cemetery? It's filled with losers." 
- Claim: Trump said 1800 marines who lost their lives at Belleau Wood were "suckers" for getting killed
- These serve as set-up for bashing "Trump’s understanding of concepts such as patriotism, service, and sacrifice"
- Weaves through McCain, Bush family, 2017 Arlington Cemetery, "senior officers", General Mattis denouncing [teargas incident](#trump-and-teargas), [Whats In it For Them](#whats-in-it-for-them), [Why Did He Join Military](#why-did-he-join-military), [No One Wants To See That](#no-one-wants-to-see-that)


[James Laporta AP News article](https://apnews.com/article/election-2020-ap-top-news-politics-b823f2c285641a4a09a96a0b195636ed) claimed "A senior Defense Department official with firsthand knowledge of events and a senior U.S. Marine Corps officer <ins>who was told about Trump’s comments</ins> confirmed <ins>some</ins> of the remarks to The Associated Press, including the 2018 cemetery comments.

<br>


## Trump 2017 Tax Cuts 

- Dec 2017, tax law signed by Trump capped state and local tax deductions at $10,000 a year
- The Cuomo administration fought back aggressively against the so-called SALT deduction limit
- Cuomo with NJ, CT, and MD in a lawsuit for unfairly targeting high-tax, Democratic states
- New Yorkers payed 2.6% less tax as a result of the new law 

<br>


## Polish First Lady Refuses Handshake

- [Newsweek reported](https://www.snopes.com/fact-check/polish-first-lady-trump-handshake/) Polish First Lady refused to shake Trump's hand 

<br>
































- [Institutions are Broken](#institutions-are-broken)


## Chinese Weather Balloon

<br>

## Trump and Nuclear Secrets 

- Jack Smith’s service as special counsel violates both the Appointments Clause and the Appropriations Clause of Article II of the Constitution

<br>








## Institutions are Broken 

Institutions cannot be trusted.  They are either corrupt, captured, or both.

|PoO|Agent|Context|Effort|Result|
|-|-|-|-|-|
|-<br>Admission|FDA|COVID-19|Deception: Use Vax not Ivermectin|EUA for COVID vax|
|-|WHO|Driven by donor interests|Deception: Vax is the solution|USG pushed Vax compliance|
|-|Fauci|National Institute of Allerge/Infections Diseases<br>NIAID|Deception: Vaxes are safe & effective|Drop in Vax hesitancy|
|-|Collins|National Institute of Health<br>NIH|Deception: Funded virus at Wuhan Lab after ban on GoF research|Creation of COVID-19|
|-|USG|Govern social media companies|Deception: Pressure to censor accurate COVID info|Drop in Vax hesitancy|
|-|BMGF|NGO|Deception: Paid WHO to push Vax|World governments pushed Vax compliance|
|-|WellCome Trust|NGO|Deception: Paid WHO to push Vax|World governments pushed Vax compliance|
|-|GAVI|NGO|Deception: Paid WHO to push Vax|World governments pushed Vax compliance|
|-|CEPI|NGO|Deception: Paid WHO to push Vax|World governments pushed Vax compliance|
|Zuck<br>Twitter Files|Biden|Presidential Candidate|Deception: Hunter laptop is Russian Op|Won election|
|-|National Institute of Health<br>NIH||||
|-|EcoHealth|NGO|||
|-|USIC|FBI Big Tech/Media Collusion|Deception: Shape & Suppress|- Hunter Biden Laptop<br>- COVID Info/Treatments<br>Devin Nunes Memo on FBI FISA Abuse|

## Emerson Collective 

- Leader in philanthrocapitalism, founded by billionaire Laurene Powell Jobs
- Uses impact investing, advocacy, and community engagement as tools to spur change in U.S. and abroad
- The Atlantic, Axios, Ozy, 


<br>


- [Tulsi Gabbard on Watchlist](#tulsi-gabbard-on-watchlist)

Added to the Quiet Skies domestic terror watchlist as political retaliation, she and her family had to do surveillance, searching, other 

Reported by [Matt Taibi](https://www.racket.news/p/comment-from-the-tsa-on-tulsi-gabbard)



# Latin Phrases 

|||
|-|-|
|Dominus illuminatio mea et salus mea: quem timebo?|The Lord is the source of my light and my safety, so whom shall I fear?|
|Non nobis, Domine, non nobis; sed nomini tuo da gloriam.|Not unto us, Lord, not unto us; but to thy name give glory.|
|Magnificat anima mea Dominum|My soul doth magnify the Lord|
|Magna est veritas, et praevalet|Great is truth, and it prevails|



# X Journalists

|||
|-|-|
|@KanekoaTheGreat||
|@TheChiefNerd||
|@MidwesternDoc|exposed decades of lies and abuse by the medical establishment (midwesterndoctor.com)|
|@MazeMoore||
|@Overton_News||
|@WesternLensman||
|@VigilantFox||
|@DschlopesIsBack||
|@VigilantNews|vigilantnews.com|
|@wideawake_media||
|@zeee_media|zeeemedia.com|
|@RenzTom|TomRenz.com|
|@ElijahSchaffer||
|@thecoastguy|Neil Oliver, Scottish news broadcaster, anti-globalist thought leader, freedom, transparency, and the sovereignty of mankind|
|@sayerjigmi|Follow him for eye-opening threads that expose the censorship cartel, uncover global power plays, and deliver truthful health information.|
|@stkirsch|Steve Kirsch found COVID shots were unsafe, destructive force against the COVID propaganda campaign|
|@PierreKory|developer of COVID Rx protocols  drpierrekory.com|
|@P_McCulloughMD|Peter McCullough is one of the most published cardiologists ever in America |
|@Fynnderella1|Dr. Lynn Fynn-derella, SC2 origin|
|@Shellenberger|Twitter files, Censorship Industrial Complex|
|@NicHulscher|Epidemiology, medical freedom movement, COVID-19 shots|
|@AaronSiriSG|Attorney, medical freedom, Pfizer documents|
|@tomselliott|Citizen journalist, founder of Grabien|
|@Ayaan|Ayaan Hirsi Ali|Individual, economic, and political freedom, founder Courage Media (courage.media)|
|@DrBenTapper1|Ben Tapper, medical freedom|
|@DrSuneelDhand|Dr. Suneel Dhand, medical freedom|
|@molsjames|Dr Mollie James|
|@MdBreathe|Mary Talley Bowden MD|
|@KonstantinKisin|@triggerpod|
|@jordanbpeterson|Jordan Peterson|
|@DoublasKMurray|Douglas Murray|
|@SebGorka|Sebastian Gorka|
|@R_H_Ebright|Richard H. Ebright, COVID, Fauci|
|@robbystarbuck|Robby Starbuck, Woke corporations|
|@PageauJonathan|Jonathan Pageau, symbolism|
|@BarbaraOneillAU|Barbara Oneill|
|