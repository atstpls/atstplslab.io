---
title: our world
date: 2023-04-08 13:58:43
layout: post
---

[<img src="../../../../assets/img/posters/battles.png"  width="360" />](jiu-jitsu/intro/)&emsp;[<img src="../../../../assets/img/posters/history.png"  width="360" />](history/intro/)&emsp;[<img src="../../../../assets/img/posters/stories.png"  width="360" />](heuristics/intro/)&emsp;[<img src="../../../../assets/img/posters/resilience.png"  width="360" />](resilience/intro/)

[<img src="../../../../assets/img/posters/trading1.png"  width="360" />](trading/intro/)&emsp;[<img src="../../../../assets/img/posters/infosec1.png"  width="360" />](infosec/intro/)&emsp;[<img src="../../../../assets/img/posters/abilities.png"  width="360" />](performance/intro/)&emsp;[<img src="../../../../assets/img/posters/leglocks1.png"  width="360" />](leglocks/intro/)



<br><br><br><br>
<br><br><br><br>
<br><br><br><br>

&nbsp;&nbsp;&nbsp;&nbsp;The <a href="context/spiritual-world" style="font-size:24px;color:#77aaff">SPIRITUAL WORLD</a> contains instances of unity and multiplicity which we perceive as <a href="completion/abstractions" style="font-size:24px;color:#77aaff">ABSTRACTIONS</a>---the world of values

<br>

```mermaid
flowchart TD

    subgraph SP [ VALUES ]
        AB((ABSTRACT\nPRINCIPLE))
        VE((VEHICLE))
        GD((GOOD DAD))
    end

    subgraph PW [ \nFACTS  ]
        CI1{CONCRETE\nINSTANCE}
        CI2{CONCRETE\nINSTANCE}
        MUS{TRUCK}
        CHA{CHARIOT}
        HWP{HELPS WITH\nPROBLEMS}
        TTT{TELLS THE\nTRUTH}
    end

    AB <-- Express\n\n\n\n\nInform --> CI1 
    AB <-- Express\n\n\n\n\nInform --> CI2 
    
    VE <-- Express\n\n\n\n\nInform --> MUS 
    VE <-- Express\n\n\n\n\nInform --> CHA

    GD <-- Express\n\n\n\n\nInform --> HWP 
    GD <-- Express\n\n\n\n\nInform --> TTT


    classDef Core stroke:#4e4c4c,stroke-width:3px,fill:#4d88ff,color:#ffffff,font-size:18px
    classDef Area fill:#34495e,opacity:0.2,color:#000000,font-size:120px
    classDef INST fill:green,opacity:0.9,stroke:#ffffff,color:#ffffff,font-size:16px,stroke:#4e4c4c,stroke-width:3px

    class SP,PW Area
    class AB,VE,GD Core
    class CI1,CI2,MUS,CHA,HWP,TTT INST

```

&nbsp;&nbsp;&nbsp;&nbsp;The <a href="context/physical-world" style="font-size:24px;color:green">PHYSICAL WORLD</a> contains instances of <a href="point-of-origin/objects-and-states" style="font-size:24px;color:green">OBJECTS & THEIR STATES</a>---the world of facts

<br><br><br><br>

 <div class="dropdown">
  <button onclick="myFunction()" class="dropbtn">Dropdown</button>
  <div id="myDropdown" class="dropdown-content">
    <input type="text" placeholder="Search.." id="myInput" onkeyup="filterFunction()">
    <a href="jiujitsu/flow/#Major-Flow">Flow</a>
    <a href="events/lore">Lore</a>
    <a href="market/chaotic-markets">Chaotic Markets</a>
    <a href="#Mount-Dilemma">Mount Dilemma</a>
    <a href="#Guard-Dilemma">Guard Dilemma</a>
    <a href="#In-Guard-Dilemma">In Guard Dilemma</a>
    <a href="#Rear-Mount-Dilemma">Rear Mount Dilemma</a>
    <a href="#Rear-Mounted-Dilemma">Rear Mounted Dilemma</a>
  </div>
</div> 


Academic disciplines are conventionally divided into the 
    humanities
        language
        art
        cultural studies
    scientific disciplines
        physics
        chemistry
        biology
    social sciences


<br>

##### THE PHYSICAL WORLD


{% blockquote %}
    *"Objects can be perceived in an infinite number of ways. And each action or event has an infinite number of potential consequences. The world does not present itself neatly.  Nature cannot be easily cut at her joints."*
{% endblockquote %}

<br>


##### THE PRESENT 


<br>


&nbsp;&nbsp;&nbsp;&nbsp;Our <a href="point-of-origin/meaning" style="font-size:24px">GOALS</a> and their inherent <a href="point-of-origin/value" style="font-size:24px">VALUES</a> determine our worldviews using <a href="context/frames" style="font-size:24px">FRAMES</a> through which we see the world

{% blockquote %}
    *"Motivation does not drive behavior, deterministically; nor does it simply set goals. Instead, it provides the current state of being with boundaries and values."*
{% endblockquote %}

<br>

##### THE PAST 

&nbsp;&nbsp;&nbsp;&nbsp;Humans obtained <a href="point-of-origin/meaning" style="font-size:24px">KNOWLEDGE</a> from raw, subjective experience and shared it through <a href="point-of-origin/stories" style="font-size:24px">STORIES</a> which acted as pragmatic guides to action

{% blockquote %}
    *"The great myths of mankind are not theories of objective existence. They are, instead, imaginative roadmaps to being. We tell stories about how to play: not about how to play the game, but about how to play the metagame, the game of games."*
{% endblockquote %}

<br>

##### THE PATTERN

&nbsp;&nbsp;&nbsp;&nbsp;The arc of evolution is humans adapting to <a href="point-of-origin/meaning" style="font-size:24px">MEANING</a>, not <a href="point-of-origin/objects-and-states" style="font-size:24px">OBJECTS</a>---to the ***significance*** of things, ***not to things themselves*** 

{% blockquote %}
    *"The historical process of things we did that resulted in peace and progress revealed the guiding principles that caused it. These were codified and sharpened over time by dialogue, philosophical inquiry, and aesthetic efforts. A phenomenological, pragmatic philosophy. Our ancestors really lived by it for nearly all human history (far before anyone could afford "luxury beliefs"), means that it is evolutionarily vetted. That we're alive now is powerful evidence, if not outright proof, that it worked."*
{% endblockquote %}

<br>

##### THE HUMAN

&nbsp;&nbsp;&nbsp;&nbsp;Humans evolved using induction-built <a href="point-of-origin/heuristics" style="font-size:24px">HEURISTICS</a>, not exhaustive observation and analysis of infinite <a href="point-of-origin/objects-and-states" style="font-size:24px">OBJECTS & THEIR STATES</a> 

{% blockquote %}
    *"The lens through which we see the world is an ethic, and that ethic is described in stories. And the fundamental foundation piece of the stories through which we view the world is Biblical. And the Biblical corpus has a language of symbolic representation and grammar."*
{% endblockquote %}

<br>

##### THE HERO

&nbsp;&nbsp;&nbsp;&nbsp;The <a href="completion/stories" style="font-size:24px">HERO MYTH</a> is an evolutionarily vetted, entirely abstract, universal, meta-functional guide to action & perception in all contexts



<br>

##### THE MODERN WORLD 

Central banking is the primary socio-economic problem in the world.  We have a monetary system built on theft and deception. It's used to fund warfare, mainstream media bullshit propoganda narratives. And so we're just dumber, we're more destructive, and we're incentivized to steal and kill one another in a fiat paradigm.
<a href="agent/legacy-banks" style="font-size:24px">CENTRAL BANKS</a> and <a href="context/fiat" style="font-size:24px">MONETARY POLICIES</a> are causing <a href="result/inflation" style="font-size:24px">INFLATION</a>, enabling confiscation of wealth, and incentivizing greed and corruption.

Currency was engineered to decline over time, real estate has become a store of value, and a key component of the money printing business.


Water has fluoride (reduces IQ) and chlorine (causes clogged arteries)

Food you eat is deficient in essential nutrients, damages your digestive tract, introduces inflammatory biochemicals into your bloodstream

Pay a small fortune for medical insurance, only gives you access to one system of medicine--the one you got sick

The doctors in system are more or less drug dealers who don't know how to cure you and whose treatments are inconsistent with natural law 

Their treatments are also leading cause of bankruptcy and 3rd leading cause of death 

ALternative treatments which attempt to cure your condition are outlawed or slandered and the only thing that can legally treat a disease is a drug 

Naturopathic medicine is direct competition to big pharma... 

<a href="government-corruption" style="font-size:24px">government corruption</a> and <a href="captured-agencies" style="font-size:24px">captured agencies</a> are eroding our individual soveriegnty, civil liberties, trust and tolerance.

<a href="context/wokecraft" style="font-size:24px">wokecraft</a>, <a href="victim-culture" style="font-size:24px">victim culture</a>, and <a href="tribalism" style="font-size:24px">tribalism</a> cripple the ability for individuals to cooperate with each other and thrive.

<a href="agent/legacy-healthcare" style="font-size:24px">HEALTHCARE SYSTEMS</a>, <a href="agent/legacy-media" style="font-size:24px">LEGACY MEDIA</a>, <a href="agent/big-tech" style="font-size:24px">BIG TECH</a>, <a href="agent/big-pharma" style="font-size:24px">BIG PHARMA</a> repeatedly prioritize money over truth.

Humans are not teetering on the brink of apocalyptic disaster
Humans are not primarily motivated by lust for power and desire to dominate
Humans are not destructive forces in a pristine and natural world

Epidemic mental health crisis worldwide
Due to breakdown of social fabric and confusion of values 
Products of a vision and narrative that humans are consumers, parasites, 

Rejection of traditional beliefs have not produced viable alternatives, only deconstructionist theories
We focus on identity and self-serving hedonism... instead we need to focus on responsibility and service to others

You cannot trust institutions. They are corrupt or captured or both.

Universities must write DEI statements to obtain research grants. The diversity ideology is the enemy of competence and justice.

Radical left think Marxism (will to power) that structures our relations.



USG tried to directly influence what content was allowed on media outlets and social media platforms.

Pressured Twitter to elevate certain content and supress other about corona in pandemic.

Legacy media operated as messaging platform for public health institutions.

Job of press is be skeptical of power, espeically power of government.

Non-experts making decisions on complex topics like myocarditis and vaccine efficacy.

Doctors censored for speaking opinions and true information.

USG pushed narrative that mitigation was highest priority was best approach and censored everything else

Twitter used liss to control which stories were covered and how, shielded users from other perspectives while pretending it wasn't.



##### COMMAND


MONETARY POLICY

Tax your income, property, capital gains, and business profits.

Tax it again when you spend it, gift it, or leave it to your kids.

Then, after all that, they inflate the currency and devalue whativer you managed to save.

As money loses value, so does the principal and interest. Zombie companies propped up by low inte



<br>

##### THE MISSION

Things work better when they are based on reality ( truth vs false ) --- money, social issues, politics

Take responsibility for yourself, If you can do that, you can help others

To create content and tools that will help individuals develop:

1. The [knowledge](/knowledge-development) and [skills](/skills-development) needed for individual sovereignty

2. The most effective [tactics](/tactics-development) for advancing and succeeding in the digital world

3. The ability to use [campaigns](/campaign-development) to achieve long-term objectives

<br>


The fundamental basis of human social organization and individual motivation is <a href="completion/reciprocity" style="font-size:24px">reciprocity</a>, the <a href="completion/play" style="font-size:24px">spirit of play</a>, and the <a href="completion/conflict-resolution" style="font-size:24px">ability to reconcile</a>.


<br>

The NAC model:  Negate, Advantage, Completion 


###### NEGATE

|||
|-|-|
|[BUFFALO INCEPTION](negate/buffalo-inception)|Full control over an individual's property and assets|
|[PREPARE](negate/prepare)|The study of human choices under scarcity and their consequences|
|[PRESERVE](negate/preserve)|Assets with the highest strength, property rights, and durability|
|[PROTECT](negate/protect)|Government-run financial institutions managing defaults on gold obligations|

<br>

###### ADVANTAGE

|||
|-|-|
|[KNOWLEDGE DEVELOPMENT](advantage/knowledge-development)|Establish basic understanding of facts, truths, and principles from study and investigation|
|[SKILLS DEVELOPMENT](advantage/skills-development)|Learn and practice essential techniques for managing and protecting digital assets, social media content, and private data
|
|[TACTICS DEVELOPMENT](advantage/tactics-development)|Apply knowledge and skill to build the simplest, most effective methods for operating in physical, digital, and spiritual spaces
|
|[CAMPAIGN DEVELOPMENT](advantage/campaign-development)|Conceptualize and execute connected series of operations designed to achieve specific objectives|

<br>

###### COMPLETION

|||
|-|-|
|[OWNERSHIP](/ownership)|Full control over an individual's property and assets|
|[MEDIA ECOLOGY](/media-ecology)|The study of media, technology, and communications and their effects|
|[ECONOMICS](/economics)|The study of human choices under scarcity and their consequences|
|[APEX ASSETS](/apex-assets)|Assets with the highest strength, property rights, and durability|
|[FIAT](/fiat)|Government-run financial institutions managing defaults on gold obligations|
|[OWNERSHIP](/ownership)|Full control over an individual's property and assets|
|[MEDIA ECOLOGY](/media-ecology)|The study of media, technology, and communications and their effects|
|[ECONOMICS](/economics)|The study of human choices under scarcity and their consequences|
|[APEX ASSETS](/apex-assets)|Assets with the highest strength, property rights, and durability|
|[FIAT](/fiat)|Government-run financial institutions managing defaults on gold obligations|
|[OWNERSHIP](/ownership)|Full control over an individual's property and assets|
|[MEDIA ECOLOGY](/media-ecology)|The study of media, technology, and communications and their effects|



{% blockquote %}
    *"We must learn how to look at the world from a very different point of view, 
     to see reality not as a heap of meaningless atoms and energy, 
     but as the physical expression of metaphysical truth."*
{% endblockquote %}

Conspiracy theorists propose and defend unconventional interpretations of events and motives.

Their opponents are Consensus Reality Ontologically Certain Knowing (CROCK) upholders of truth.

A mutual exchange of support, emotional investment, care, and love.

