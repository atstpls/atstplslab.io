---
title: leglocks
date: 2023-05-01 09:09:17
tags: [trading]
category: [training, fractalflow]
---

# top

&emsp;[<img src="../../../../assets/img/posters/leglocks-intro.png" width="460" />](#leglocks-intro)&emsp;[<img src="../../../../assets/img/posters/ashi-versions.png" width="460" />](#ashi-versions)&emsp;[<img src="../../../../assets/img/posters/inside-control.png" width="460" />](#inside-control)


&emsp;[<img src="../../../../assets/img/posters/positive-gripping.png" width="460" />](#positive-gripping)&emsp;[<img src="../../../../assets/img/posters/two-on-one-gripping.png" width="460" />](#2-on-1-gripping)&emsp;[<img src="../../../../assets/img/posters/push-pull.png" width="460" />](#push-pull)


&emsp;[<img src="../../../../assets/img/posters/leglock-entries.png" width="460" />](#leglock-entries)&emsp;[<img src="../../../../assets/img/posters/ashi-vs-sitting.png" width="460" />](#ashi-vs-sitting)&emsp;[<img src="../../../../assets/img/posters/ashi-vs-standing.png" width="460" />](#ashi-vs-standing)

&emsp;[<img src="../../../../assets/img/posters/cross-ashi-vs-standing.png" width="460" />](#cross-ashi-vs-standing)


<br><br><br><br>

### leglocks intro

<video id="leglocks-intro" class="tut"  width="90%"  controls="controls" poster="../../../../assets/img/posters/leglocks-intro.png" src="../../../../assets/videos/jiu-jitsu/leglocks/Leglocks-Intro.mp4" type="video/mp4"></video>

<br>

### ashi versions 

<video id="ashi-versions" class="tut"  width="90%"  controls="controls" poster="../../../../assets/img/posters/ashi-versions.png" src="../../../../assets/videos/jiu-jitsu/leglocks/Ashi-Versions.mp4" type="video/mp4"></video>

<br>

### inside control 

<video id="inside-control" class="tut"  width="90%"  controls="controls" poster="../../../../assets/img/posters/inside-control.png" src="../../../../assets/videos/jiu-jitsu/leglocks/Inside-Control.mp4" type="video/mp4"></video>

<br>

### positive gripping

<video id="positive-gripping" class="tut"  width="90%"  controls="controls" poster="../../../../assets/img/posters/positive-gripping.png" src="../../../../assets/videos/jiu-jitsu/leglocks/Positive-Gripping.mp4" type="video/mp4"></video>

<br>

### 2-on-1 gripping

<video id="two-on-one-gripping" class="tut"  width="90%"  controls="controls" poster="../../../../assets/img/posters/two-on-one-gripping.png" src="../../../../assets/videos/jiu-jitsu/leglocks/Two-On-One-Gripping.mp4" type="video/mp4"></video>

<br>

### immovable elbow

<video id="immovable-elbow" class="tut"  width="90%"  controls="controls" poster="../../../../assets/img/posters/immovable-elbow.png" src="../../../../assets/videos/jiu-jitsu/leglocks/Immovable-Elbow.mp4" type="video/mp4"></video>

<br>

### push pull

<video id="push-pull" class="tut"  width="90%"  controls="controls" poster="../../../../assets/img/posters/push-pull.png" src="../../../../assets/videos/jiu-jitsu/leglocks/Push-Pull.mp4" type="video/mp4"></video>

<br>

### leglock entries

<video id="leglock-entries" class="tut"  width="90%"  controls="controls" poster="../../../../assets/img/posters/leglock-entries.png" src="../../../../assets/videos/jiu-jitsu/leglocks/Leglock-Entries.mp4" type="video/mp4"></video>

<br>

### ashi vs sitting

<video id="ashi-vs-sitting" class="tut"  width="90%"  controls="controls" poster="../../../../assets/img/posters/ashi-vs-sitting.png" src="../../../../assets/videos/jiu-jitsu/leglocks/Ashi-Vs-Sitting.mp4" type="video/mp4"></video>

<br>

### ashi vs standing

<video id="ashi-vs-standing" class="tut"  width="90%"  controls="controls" poster="../../../../assets/img/posters/ashi-vs-standing.png" src="../../../../assets/videos/jiu-jitsu/leglocks/Ashi-Vs-Standing.mp4" type="video/mp4"></video>

<br>

### cross ashi vs standing

<video id="cross-ashi-vs-standing" class="tut"  width="90%"  controls="controls" poster="../../../../assets/img/posters/cross-ashi-vs-standing.png" src="../../../../assets/videos/jiu-jitsu/leglocks/Cross-Ashi-Vs-Standing.mp4" type="video/mp4"></video>

<br>

