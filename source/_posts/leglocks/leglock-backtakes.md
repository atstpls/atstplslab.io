---
title: leglock backtakes
date: 2023-05-01 09:09:17
tags: [trading]
category: [training, fractalflow]
---

&emsp;<video id="intro" class="tut"  width="460" height="260"  controls="controls"  poster="../../../assets/img/posters/leglocks-intro.png"><source src="../../../../assets/videos/jiu-jitsu/leglocks/Leglocks-Intro.mp4" type="video/mp4"></video> &emsp; <video id="leglocks-2" class="tut"  width="460" height="260"   controls><source src="../../../../assets/videos/FractalFlow/Lecture 2 - Contextual Trading.mp4" type="video/mp4"></video> &emsp; <video id="lecture-3" class="tut"  width="460" height="260"   controls><source src="../../../../assets/videos/FractalFlow/Lecture 3 - Multitimeframe Analysis.mp4" type="video/mp4"></video>


