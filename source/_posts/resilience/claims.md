---
title: claimsitrust
date: 2023-01-18 03:00:42
tags: [trading]
category: [ecosystem, concepts] 
---

<br>

# GENESIS

|||
|-|-|
|::::::::::::::::::::::::::::::::::::::::::::::::::::::::::<center><img height=300 width=260 src="../../../assets/img/posters/bible/genesis.jpg"></img></center>::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|[God]() is that which confronts potential chaos and generates habitable order that is good<br>-  |

<br>

# ADAM & EVE 

|||
|-|-|
|::::::::::::::::::::::::::::::::::::::::::::::::::::::::::<center><img height=300 width=260 src="../../../assets/img/posters/bible/The.Story.Of.Adam.And.Eve.jpg"></img></center>::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|Life requires you make the proper sacrifices<br>- [Adam]() and [Eve]() tricked, premature conceptualization (post-modernists)  <br>- Problems they posed were fine, it was the solutions that were damaging<br>- How make canonical sense out of texts, how rank order importance of facts<br>- The prideful error was the presupposition that our perceptions serve nothing but compulsion, self-interest and power<br>- The question was structured properly but the answer was literally Luciferian: that the spirit that orders the world is predicated on nothing but the imposition of compulsion and power<br><br>The story of the Garden of Eden describes a state of innocence,where humans occupied a central place in a relatively natural environment. This naive perspective conveniently provided humans with a full understanding of their role in the universe and the spiritual purpose <br><br>of existence. However, the garden was also a sheltered place; it was a tiny portion of a much greater world. Therefore, as soon as humans looked beyond the limits of the garden, their “eyes were opened” to a strange universe devoid of spiritual meaning. They “knew that they were naked” and saw their previous worldview as somewhat illusory. Once the bubble had been shattered, all attempts at “covering it up” were in vain, and humans had no choice but to wander in a meaningless universe until they return to the ground. At the meta-cognitive level, the narrative of the fall perfectly matches the plight of humanity since the scientific revolution. So, even though these technical discoveries have locally debunked traditional cosmology, they have ironically proven its significance at a higher level. In general, the narratives of the Bible are about humanity’s conflicted attempts to reconcile spiritual truth with physical reality in the hopes of acquiring divine knowledge. For that reason, the ancient stories of the Bible will survive and eventually transcend any scientific discovery, as long as we recognize that level of interpretation|

<br>

# CAIN & ABEL 

|||
|-|-|
|::::::::::::::::::::::::::::::::::::::::::::::::::::::::::<center><img height=300 width=260 src="../../../assets/img/posters/bible/cain-and-abel.png"></img></center>::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|Life requires you make the proper sacrifices<br>- The Biblical corpus insists sacrifice is required and is built into the structure of reality itself<br><br>- If you get the sacrifices right the future is paradisal<br>- Cain doesn't make the proper sacrifices, does everything 2nd rate<br>- He lies, he omits, he prevaracates, he pretends<br>- People tear things down so they don't have anything to contrast themselves against<br>- The fact that it exists judges them<br><br>[Cain]() and [Abel]() represent rival responses to the suffering inherent in the human condition following the rise of self-consciousness<br><br>- Abel’s suffering leads to his self-development as a warrior<br>- Cain’s suffering leads to envy, malevolence, and murder.<br><br>[Peterson on Sacrificial Relationship](https://odysee.com/Sacrificial-Relationship:0)|

<br>

# THE FLOOD 

<br>

|||
|-|-|
|::::::::::::::::::::::::::::::::::::::::::::::::::::::::::<center><img height=300 width=260 src="../../../assets/img/posters/bible/noah-and-the-flood.png"></img></center>::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|There are consequences for disharmony with God<br><br>[Noah]() has faith and intuition and acts on it, God is source of intuition, Noah's faith is willingness to act regardless of everyone else actions|

<br>

# TOWER OF BABEL 

<br>

|||
|-|-|
|::::::::::::::::::::::::::::::::::::::::::::::::::::::::::<center><img height=300 width=260 src="../../../assets/img/posters/bible/tower-of-babel.jpg"></img></center>::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|A house divided cannot stand<br>Post-modern Neo-Marxists, In the last 30 years, in the fields of literary criticism, We live by stories.  Descriptions of value structures by which we look at the world. Stories matter.  Literary critics who assess and analyze stories matter.  If done in ideologically biased manner, things go wrong. French intellecuals figured out we see the world through stories.  Science isn't something you follow, it's a set of facts.  A story is something to follow, it guides you. Once they figured out the story, they jumped to the premature conclusion---that proclamation of Marxist, that we live by power When individuals in society become corrupt, they operate on a power hierarchy, Marxist, economic systme is based on oppression, man woman based on oppression, family is power dynamic, Explain whole world on basis of power, but it's not the story of success, When you accept doctrine that only power rules, you only use power, Just a battleground of power claims, world turns to hell, If only power governs relationship, there's no game but power|

<br>

# ABRAHAM & ISAAC

|||
|-|-|
|::::::::::::::::::::::::::::::::::::::::::::::::::::::::::<center><img height=300 width=260 src="../../../assets/img/posters/bible/abraham-and-isaac.png"></img></center>::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|Long-term proper sacrifice produces success<br>The Biblical corpus insists you should adopt a mode of behavior that works best all things considered across multiple generations, and to sacrifice everything to that<br><br>- Reproduction isn't sex. Sex is a necessary but insufficient precondition for reproduction.<br>- [Abraham]() offers his best, he is fully committed<br>- He adopts a sacrificial mode of being that establishes the optimal environment for his sons, who do the same for their sons, who do the same for their sons<br><br>The Spirit of the Father is male evaluation of men over millenia that acts as the primary selection mechanism in evolution<br><br> - Men get together in productive groups<br> - orient themselves toward a task<br> - produce a hierarchy around the task<br> - they vote up the most competent men to the top<br> - the women select the competent men from top of hierarchy<br>hat's highest form of sacrifice?  If you raise your children properly, you teach them to be good players, courageous, You sacrifice yourself, it's the basis of community.  Crucifix is symbol of the ultimate sacrifice. Highest form of service is full expression of yourself in service of what's beneficial to your family and community.<br><br>Long-term proper sacrifice produces success<br>The Biblical corpus insists you should adopt a mode of behavior that works best all things considered across multiple generations, and to sacrifice everything to that<br><br>- Reproduction isn't sex. Sex is a necessary but insufficient precondition for reproduction.<br>- Abraham offers his best, he is fully committed<br>- He adopts a sacrificial mode of being that establishes the optimal environment for his sons, who do the same for their sons, who do the same for their sons<br><br>The Spirit of the Father is male evaluation of men over millenia that acts as the primary selection mechanism in evolution<br><br> - Men get together in productive groups<br> - orient themselves toward a task<br> - produce a hierarchy around the task<br> - they vote up the most competent men to the top<br> - the women select the competent men from top of hierarchy<br>hat's highest form of sacrifice?  If you raise your children properly, you teach them to be good players, courageous, You sacrifice yourself, it's the basis of community.<br><br>[source](https://player.odycdn.com/v6/streams/962803ee9e6ace63159dfe51247abcb50c5d3698/624645.mp4)|

<br>

# BURNING BUSH 

<br>

|||
|-|-|
|::::::::::::::::::::::::::::::::::::::::::::::::::::::::::<center><img height=180 width=220 src="../../../assets/img/posters/bible/moses-and-the-burning-bush.png"></img></center>::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|There is nothing more real than meaning<br>- [Moses]() pursues the thing that calls to him<br>- He understands he's on sacred ground and humbled in the presence of God<br>- Desires intimacy with God (takes off shoes)<br>- Once Moses is grounded in Being, he becomes capable of his mission<br><br>Regardless of limitations of fate and circumstance, if you follow your calling with sufficient integrity, you'll form a covenental relationship with God, and be able to oppose tyranny and lead people out of slavery<br>Your deepest instincts align with this.  Align with maximum meaning and maximum adventure.  Makes you everything you could be and stabilizes the community. It's embedded in our underlying stories.<br><br>God is spirit that reveals itself in burning bush, punishes the tyrant and calls the slaves out of slavery <br><br>[source](https://player.odycdn.com/v6/streams/21d06ce6793929e21ed39ac4d3987c0c352bf20a/4021d8.mp4)|

<br>

# CAST THE FIRST STONE 

<br> 

|||
|-|-|
|::::::::::::::::::::::::::::::::::::::::::::::::::::::::::<center><img height=180 width=200 src="../../../assets/img/posters/bible/cast-the-first-stone.png"></img></center>::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|Victimhood is the polar opposite of responsibility<br>Universities have taught idiot meta-marxism: The way to look at every social relationship is through the lens of power (oppressor vs oppressed)<br><br> - Marriage is a patriarchal institution<br> - Business is oppression<br> - History is full of oppression<br><br>Victims are seen as morally righteous and those that stand for them are morally righteous<br>Adopted a pathway of maximum responsibility.  You're not a burden to anyone else or yourself.  Then you can take care of your wife/husband/kids/employees/citizens.The alternative to tyranny and slavery is maximal responsibility you take on yourself. If you aim up, take responsibility, everyone serves as their own king.Freedom and maximal responsibility is the way. Victimhood and power claims are not<br><br>Jesus told the parable of a woman <br>Fundamental attribution error - personal attribution is easier than situational analysis, it also supports personal defense, presuming you are innocent, start with presumption of innocence and situational factors. Don't convict until all arguments are exhausted.<br><br>[source](https://player.odycdn.com/v6/streams/a08d6b2cab9c5a9b19dfdaa93a83ffc0ea115342/e4dff0.mp4)|

<br>

# FALL OF LUCIFER 

<br>

|||
|-|-|
|::::::::::::::::::::::::::::::::::::::::::::::::::::::::::<center><img height=300 width=260 src="../../../assets/img/posters/bible/fall-of-lucifer.png"></img></center>::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|Sacrificial patterns are the opposite of power, trust the opposite of betrayal</h3><br><br><br>Identities are embedded in relationships, not in your head</h3><br> - You are not your base desires or what you feel<br> - You negotiate your identity with others<br> - Sanity is the concordance of and individual and the world<br><br>In domain of power, no sacrifice is necessary.  If it's beyond you, sacrifice is required.<br><br>Pride is the cardinal sin. Gratitude is the opposite of arrogance and resentment<br><br>"The story of the fall is really about the process of knowledge itself and the dangers of acquiring greater material knowledge at the expense of spiritual insight."<br><br>[source](https://player.odycdn.com/v6/streams/1db7fef0c7fb8b5ad27878a03f9fcfee3a319037/d948a4.mp4)|

<br>

# LOVE YOUR ENEMY 

<br>

|||
|-|-|
|::::::::::::::::::::::::::::::::::::::::::::::::::::::::::<center><img height=300 width=260 src="../../../assets/img/posters/bible/love-your-enemy.png"></img></center>::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|Long-term reciprocal altruism emerged as the basis for the stable quality in societyLove is when you recognize something other than your current self as real... relationship to other, to your future self, etc....Underlying ethic is knock and the door will open.  Seek and you shall find. Looking at your future self is an aesthetic judgement, some want to look and others have a hard time <br>|

<br>

# CHRIST IN THE DESERT 

<br>

|||
|-|-|
|::::::::::::::::::::::::::::::::::::::::::::::::::::::::::<center><img height=300 width=260 src="../../../assets/img/posters/bible/Christ.In.The.Wilderness.jpg"></img></center>::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|Christ in the wilderness<br>|
||Confronting the unknown makes you braver, stronger, and more successful<br><br>- You're the consequence of all the ancestors that came before you, all the ancestral wisdom locked inside of you biologically<br>- But it won't come out unless you stress yourself, challenge yourself, the bigger the more it turns on<br>- As you push yourself harder, the more of you turns on<br>- If you put yourself in new situations, genes code for new proteins and build new neural structures and new nervous system structures<br>- There's a lot of potential you locked in your genetic code, unlocked by life experiences<br>- Heavier and heavier loads, darker and darkest things, harshest suffering and malevolence, being able to look at it directly, that would turn you on maximally<br>- Christ does two things that are Messainic, he takes the suffering of the world onto himself, the world's full of suffering and you're suppsoed to do something about it, past, present, and future, part of bearing a cross, accepting that as the price of being.  Christ met the Devil in the desert.  You must look at the capacity for human evil as clearly as you possibly can, encounter with malevolence<br>- If you can face the malevolence, if you can face the suffering, that opens the door to your maximal potential. Although the malevolence is deep and suffering are great, your capacity to transcend it is stronger.<br>- Despite how tragic life is and how malevolent things are, fundamentally our spirit has the capacity to face it courageously and to deal with it practically<br><br>[Peterson on Rescuing the Father](https://odysee.com/Rescuing-The-Father:0)|

<br>

# JACOBS LADDER 

<br>

|||
|-|-|
|::::::::::::::::::::::::::::::::::::::::::::::::::::::::::<center><img height=300 width=260 src="../../../assets/img/posters/bible/jacobs-ladder.png"></img></center>::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|Identity is the proper union of faith and responsibility<br><br>Identity is the proper union of faith and responsibility which is subsidiary in structure. [Jacobs ladder]() is what unites the material and proximal realm of Earth with the eternal realm of Heaven. We scaffold ourselves up from the finite to the infinite, and that entire scaffold constitutes our identity. Meaning is found in service of harmony that makes up Jacob's ladder|

<br>

# JONAH & THE WHALE 

|||
|-|-|
|Jonah Whale| Nineveh to tell them to repent, Jonah gets on a ship going the opposite direction.Storms come, sailors throw him overboard, beast from the abyss swallows him and he spends 3 days there,  If you hold your tongue when you're called upon to speak, the ship sinks, you drown, and then you wish you'd drowned 
He repents and goes to Nineveh and they repent |

<br>
