---
title: preserve
date: 2023-01-01 09:20:00
tags: ["security", "defense", "tools", "tactics"]
---
<br>
    
<h1>Maintain complete control of long-term assets into the future</h1>

<br>

##### [DECENTRALIZE](/decentralization)

<h5>Guard against the effects of monetary policy and middle man economics</h5>

<br>

##### [APEX ASSETS](/apex-assets)

<h5>Move wealth into apex assets with the highest strength and durability</h5>

<br>

##### [RECOVERY PLAN](/recovery-plan)

<h5>Plan for contingencies and worst case scenarios</h5>

<br>

##### [SURVIVE AND THRIVE](/survive-and-thrive)

<h5>Utilize events and conditions in the financial world to maintain and grow your wealth</h5>

<br>

