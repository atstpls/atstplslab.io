---
title: war on the west
date: 2023-01-17 19:00:42
tags: [money]
category: [ecosystem, resources]
---

<br>

There's a cultural war against western tradition and everything good it's produced

One-sided arguments, unfair claims, corrupt language of ideas 

- Equality stopped meaning equal rights 
- Anti-racism is racist 
- Justice is revenge 

Strange Death of Europe - mass migration PROBLEMS

Western countries were told they had to change their demographic to become legitimate 

The Madness of Crowds - Identity politics, breaking down Western societies along lines of sex, sexuality, and race 

Gays, feminists, race-baiters who wanted to tear the West down were exploited 

West gets held to a different standard while China, Iran, others are excused 

Be colorblind but super race-aware  everything in the past is seen through a radical racist lens 

### Beginnings 

- Jesse Jackson protested against Stanford's introductory program to Western culture, gave in and studied multiple cultures 
- Hostility towards studying Western Civilization 
- Critics venerate every culture so long as it is not Western, celebrate all native cultures except ours 
- This allows non-Western countries to get away with monstrous crimes, while West assaulted for everything it does wrong 
- West gets no credit for what it does right--development of individual rights, religious liberty, and pluralism are held against them 
- West gave lifesaving advances in science, medicine, free market raised billions out of poverty all over the world 
- Western arrogance, elitism, and undeserved superiority 
- Started education, then media, institutions, and then government (calling for equity and eradicating systemic racism)

### Race 

- Demonizing of white people...  Jimmy Fallon announces in 2020, census shows number of whites went down, audience cheers 
- ISIS built up their forces in Syria and Iraq
- "We're lectured by two white men"  all experience and knowledge, skills dismissed because of skin color 
- Discourteous, dismissing, villifying, generalizing becasue of skin color is definition of racism 
- Led to racial segregation, racial violence, 
- Academics looked for hidden mechanisms of racism to account for this--CRT emerged 
- bell hooks, Derek Bell, Kimberly Crenshaw, created movement of activists to interpret everything through the lens of race 
- First asserted that race was single most important lens to understand society (racialized everything)
- CRT revolutionary sect made progress in a place not known for its heroism (academies)
- CRT assertions not based on evidence, but on interpretations and attitude, lived experience 
- Papers written solely based on assertions (my truth) 
- Things were taken over by bad faith actors, things are how we say they are 
- All the progress was nothing, all tools were useless, it was prejudice plus power and it must be wrestled away and wielded back at oppressors 
- They said you can't be racist if you didn't have power, and whites had power, so only white people could be racist (or internalized whiteness)
- Attitudes shifted to the mainstream, 2001 Michael Moore published Stupid White Men - Black plague, warfare, chemicals, holocaust, slavery, genocide of native americans, job layoffs in corporate america
- Whites responsible for everything bad , race hustlers adn politicians kept the ball rolling 
- They announced the rules and named themselves as the referee 
- Robin DeAngelo published White Fragility (750K copies sold) - whites are racist, if they don't like it that proves they are a racist (witch hunt logic)
- She demands the whites grow up and make the world right "The U.S was founded on the principle
- Purposely and regularly make Faulty Generalization Error (or attrubution error)
- May 2020, George Floyd protests, month after White Fragility sold 500K copies, wasn't something that happened IN america, but something emblamatic of America 
- Young people raised on CRT were persuaded that race relations in their country were wildly worse than they were 
- Professor's injected racialized terms (white supremacist society, white tears, white women's tears, whiteness, enacting whiteness)
- Floyd's death was seen by millions through a lens that it was a racist killing that told us about racial policing all over the U.S. 
- 22% liberals thought over 10K unarmed blacks killed every year 
- 40% liberals thought between 1K and 10K 
- Actual figure was 10 
- WP database, more police killed by black americans than unarmed black americans killed by police
- Still no evidence Floyd was a racist killing 
- Academies had primed students for a white supremicist racist interpretation of their own society, media had played along, corporations joined,
- Moral Panics.
    - April 2016 someone spotted KKK at campus in BLoomington--actually a Dominican Monk 
    - University of Missourri KKK sighted - apologized for sharing misinformation 
    - University of Maryland - noose spotted, actually knotted piece of plastic 
    - Dorm room noose outside door, turns out half of pair of shoelaces 
    - Viensens - man approached, incident never happened 
    - Sarah Silverman saw Swastikas on sidewalk, was chalk signs made by construction workers  
    - Coffman restaurant, black paint swastikas 
    - Jessie Smollet faked an attack, noose, homophobic slurs 


401 - 3ec852283bc25bf7e64271634d72be30
501 - a16f9f7020894129963fee966c279f02
503 - 8e11d8295ab9e5b57213f2d116db7240
504 - 7c4cdba9f93bb30128132c5c71f1e28f
507 - afbdb269fca3d7aa499859b8f657f558
508 - 8b4b37bfe75621b229837bb85f0ac946
512 - 27fd062082e19cda5812ed7e665bcee2
610 - ed26c3435de733cd25fab5fc227faf32
617 - ea14d0ddfe6d08bd6603248df3483aa7








### LION BADGER   


FHL-Advances 


FHL
- Arm drag 
    - same side post on elbow 
    - same side knee post on elbow, 1 on 1, crucifix 
    - same side hand post on elbow, 1 on 1, crucifix 
    - arm drag 
    - shrug head to side, then other side arm drag 
    - get outside same side elbow
        - kata gatame 
            - scorpion backtake/cross body ride 
            - kneeblock, step over, cross body ride 
            - chestlock, step over, cross body ride 
    - Darce 

    -   
- Circle down and pull 
- Post, arm drag grip, move around for butt drag, trap arm crucifix 
- Head block elbow, reach across for tight waist grip on far hip, seatbelt and sag 
- Leg cradle, darce choke 
    - Darce to bolo action to cross body ride 

Puma 
    - 2 on 1 - Sweep arm, tilt on either side ( pinch knees on his knee, stepping on achilles, or stepping through legs )


    - Opponent raises to Four Point 
        - Thigh pry + Claw 
        - Always can replace thigh pry with foot hook 


    - Puma Trilemma 



@Sim89776996 @stillcantbgigi @boooooo50891527 @wordy_fiend @sookiemuy @sappholives83 @TortieCats @PinPoin1950207

6,000 law enforcement agencies aren't providing data, meaning that 25% of the country's crime data is not captured by the FBI.

Democrat-controlled cities from New York to San Francisco have effectively decriminalized violent crime. For instance, in New York City, 52% of violent felony cases are downgraded to misdemeanors, and offenders are typically offered diversion agreements that keep these offenses out of crime statistics.

Soros-backed prosecutors are refusing to prosecute violent criminals or downgrading their charges in record numbers. This systematic underreporting and leniency are tactics used by Democrats to create a misleading narrative about public safety, while communities continue to suffer from unchecked crime.

70 Soros-backed prosecutors, representing 72 million Americans and half of the nation's 50 most populous cities and counties, have made it their mission to implement so-called restorative justice. This approach often means refusing to prosecute violent criminals based on factors such as race and gender identity.


