---
title: book stories
tags: [resiliency]
category: [ecosystem, concepts]
---

<br>

## Pinocchio 

<br>

<iframe id="odysee-iframe" style="width:60%; aspect-ratio:16 / 9;" src="https://odysee.com/$/embed/jp-pinocchio:c?r=B6f1mjUy7ezh82xyUa4Q5pLkEiipwm5K&autoplay=true" allowfullscreen></iframe>


There's nature (fairy), culture (Gepetto) and you (Pinocchio).  

You and your conscience (cricket) have to learn how to establish a conscious dialogue in a manner that propels your development over time.

Gepetto makes a wish that Pinocchio could be a real boy. Nature animates Pinocchio.

Four temptations:

1. Be an Actor 

2. Be Deceitful 

3. Adopt 

4. Hedonism

After escaping, he gets intuition that facing the worst thing at bottom of ocean, he can rescue his father 

Gepetto is old culture composed of the dead past that can't figure out a solution to broader problems. 

Pinocchio is that which can do new things, the hero. It's better to learn how to do things than to benefit from the thing itself.

Last temptation:  Save yourself or save your father?  Save both.

<br>

## Of Mice And Men 

Truth:  Lennie is mentally disabled, is dangerous to others, and can't keep a job 

Declaration:  George tells others that Lennie is harmless and is a good worker 

The other characters accept the declaration (lies) and problems begin

Finally Lennie kills a puppy and then kills a woman 

Once all the other workers discover Lennie is dangerous (truth), they set out to find and kill him

George realizing no one will believe the lies anymore, kills Lennie himself before the workers can kill him 

<br>

The Predatory Nature of Human Existence
Of Mice and Men teaches a grim lesson about the nature of human existence. Nearly all of the characters, including George, Lennie, Candy, Crooks, and Curley’s wife, admit, at one time or another, to having a profound sense of loneliness and isolation. Each desires the comfort of a friend, but will settle for the attentive ear of a stranger. Curley’s wife admits to Candy, Crooks, and Lennie that she is unhappily married, and Crooks tells Lennie that life is no good without a companion to turn to in times of confusion and need. The characters are rendered helpless by their isolation, and yet, even at their weakest, they seek to destroy those who are even weaker than they. Perhaps the most powerful example of this cruel tendency is when Crooks criticizes Lennie’s dream of the farm and his dependence on George. Having just admitted his own vulnerabilities, Crooks zeroes in on Lennie’s own weaknesses.

In scenes such as this one, Steinbeck records a profound human truth: oppression does not come only from the hands of the strong or the powerful. Crooks seems at his strongest when he has nearly reduced Lennie to tears for fear that something bad has happened to George, just as Curley’s wife feels most powerful when she threatens to have Crooks lynched. The novella suggests that the most visible kind of strength—that used to oppress others—is itself born of weakness.

Fraternity and the Idealized Male Friendship
One of the reasons that the tragic end of George and Lennie’s friendship has such a profound impact is that one senses that the friends have, by the end of the novella, lost a dream larger than themselves. The farm on which George and Lennie plan to live—a place that no one ever reaches—has a magnetic quality, as Crooks points out. After hearing a description of only a few sentences, Candy is completely drawn in by its magic. Crooks has witnessed countless men fall under the same silly spell, and still he cannot help but ask Lennie if he can have a patch of garden to hoe there. The men in Of Mice and Men desire to come together in a way that would allow them to be like brothers to one another. That is, they want to live with one another’s best interests in mind, to protect each other, and to know that there is someone in the world dedicated to protecting them. Given the harsh, lonely conditions under which these men live, it should come as no surprise that they idealize friendships between men in such a way.

Ultimately, however, the world is too harsh and predatory a place to sustain such relationships. Lennie and George, who come closest to achieving this ideal of brotherhood, are forced to separate tragically. With this, a rare friendship vanishes, but the rest of the world—represented by Curley and Carlson, who watch George stumble away with grief from his friend’s dead body—fails to acknowledge or appreciate it.

The Impossibility of the American Dream
Most of the characters in Of Mice and Men admit, at one point or another, to dreaming of a different life. Before her death, Curley’s wife confesses her desire to be a movie star. Crooks, bitter as he is, allows himself the pleasant fantasy of hoeing a patch of garden on Lennie’s farm one day, and Candy latches on desperately to George’s vision of owning a couple of acres. Before the action of the story begins, circumstances have robbed most of the characters of these wishes. Curley’s wife, for instance, has resigned herself to an unfulfilling marriage. What makes all of these dreams typically American is that the dreamers wish for untarnished happiness, for the freedom to follow their own desires. George and Lennie’s dream of owning a farm, which would enable them to sustain themselves, and, most important, offer them protection from an inhospitable world, represents a prototypically American ideal. Their journey, which awakens George to the impossibility of this dream, sadly proves that the bitter Crooks is right: such paradises of freedom, contentment, and safety are not to be found in this world.

Lenny doesn’t understand the social and physical rules of the human world, which often gets him into trouble. Ultimately, he cannot foresee the severe repercussions of his actions. In this way, his perception of the world is as simple and innocent as an animal’s.

Curly’s wife meets the same fate as Lenny’s pets, which he accidentally kills due to his strength. In giving Curly’s wife the same death as Lenny’s mice and puppy, Steinbeck comments on her lack of agency, and how she is treated as a possession or pet of the men in her life. Although Candy loves his dog as a companion, Curly shoots it when it grows too old to be of use on the farm. Just like Candy’s dog, the ranch workers are valued only for their labor, and not for their worth as humans. Ultimately, the title of the novel puts the comparison between animals and humans front and center. The “best-laid schemes of mice and men” often go wrong, as is tragically the case for George and Lenny

Lennie’s Puppy
Lennie’s puppy is one of several symbols that represent the victory of the strong over the weak. Lennie kills the puppy accidentally, as he has killed many mice before, by virtue of his failure to recognize his own strength. Although no other character can match Lennie’s physical strength, the huge Lennie will soon meet a fate similar to that of his small puppy. Like an innocent animal, Lennie is unaware of the vicious, predatory powers that surround him.

Candy’s Dog
In the world Of Mice and Men describes, Candy’s dog represents the fate awaiting anyone who has outlived his or her purpose. Once a fine sheepdog, useful on the ranch, Candy’s mutt is now debilitated by age. Candy’s sentimental attachment to the animal—his plea that Carlson let the dog live for no other reason than that Candy raised it from a puppy—means nothing at all on the ranch. Although Carlson promises to kill the dog painlessly, his insistence that the old animal must die supports a cruel natural law that the strong will dispose of the weak. Candy internalizes this lesson, for he fears that he himself is nearing an age when he will no longer be useful at the ranch, and therefore no longer welcome.



## The Count of Monte Cristo 

The Limits of Human Justice
Edmond Dantès takes justice into his own hands because he is dismayed by the limitations of society’s criminal justice system. Societal justice has allowed his enemies to slip through the cracks, going unpunished for the heinous crimes they have committed against him. Moreover, even if his enemies’ crimes were uncovered, Dantès does not believe that their punishment would be true justice. Though his enemies have caused him years of emotional anguish, the most that they themselves would be forced to suffer would be a few seconds of pain, followed by death.

Considering himself an agent of Providence, Dantès aims to carry out divine justice where he feels human justice has failed. He sets out to punish his enemies as he believes they should be punished: by destroying all that is dear to them, just as they have done to him. Yet what Dantès ultimately learns, as he sometimes wreaks havoc in the lives of the innocent as well as the guilty, is that justice carried out by human beings is inherently limited. The limits of such justice lie in the limits of human beings themselves. Lacking God’s omniscience and omnipotence, human beings are simply not capable of—or justified in—carrying out the work of Providence. Dumas’s final message in this epic work of crime and punishment is that human beings must simply resign themselves to allowing God to reward and punish—when and how God sees fit.

Relative Versus Absolute Happiness

 In his parting message to Maximilian, Dantès claims that “[t]here is neither happiness nor misery in the world; there is only the comparison of one state with another, nothing more.” In simpler terms, what separates the good from the bad in The Count of Monte Cristo is that the good appreciate the good things they have, however small, while the bad focus on what they lack.

The Dantès of the early chapters, perfectly thrilled with the small happiness that God has granted him, provides another example of the good and easily satisfied man, while the Dantès of later chapters, who has emerged from prison unable to find happiness unless he exacts his complicated revenge, provides an example of the bad and unsatisfiable man.

## A Tale Of Two Cities 




## A Farewell To Arms 

Reality punishes those who cannot face it

Throughout the novel, characters have sought whatever means possible to shield themselves from the pain of the world. Rinaldi finds comfort in sex, the priest in God, Catherine and Henry in love, and almost everybody in alcohol. Each of these things acts as a form of anesthetic, a temporary dulling of a pain that, in the end, cannot be conquered.



A Farewell to Arms follows Frederic Henry’s desperate attempt to find comfort and escape from the horrors of World War I in a world that punishes people for finding comfort and escape. Henry meets Catherine, which comprises the inciting incident. She is a kindred spirit, also longing for escape. The first time Catherine asks Henry if he loves her, she appears to be thinking not of him, but of her dead fiancé. She drops the act and apologizes, but Henry doesn’t seem bothered by her trauma. This scene cements their relationship as one of two desperate people who need each other to escape from the death around them.

However, Henry’s unit is immediately deployed to the front, separating him from Catherine. During this first deployment, any possibility of escape is called into question. Passini explains how in one instance when soldiers refused to fight, the Carabinieri (military police) lined them up and shot every tenth man. This story highlights the way those in power coerce people into participating in the horrific reality of war and punish those who cannot cope. Not long after Passini’s story, he dies from a mortar shell blast, and Henry is gravely wounded. He’s brought to a hospital in Milan to recover, where fortunately he is reunited with Catherine.

Henry’s convalescence proves to be a glorious period of escapism. Catherine takes night shifts so that they can abandon themselves in sex, and Henry drinks frequently to take the edge off the pain and trauma. Catherine pretends she’s married to Henry, and they act as if their love is all that matters. By the time he’s nearly healed, Henry has drunk so much his liver is jaundiced, and Catherine is pregnant. When Miss Van Campen notices Henry’s drinking, she treats his jaundice as self-inflicted to keep himself away from the fighting instead of a consequence of self-medicating. This moment is the first time Henry experiences first-hand punishment for attempting to cope with his reality through escapism.

Back at the front, the war has turned against Italy. Henry attempts to play his role, even shooting two engineers who disobey his orders during the retreat. However, when he realizes the Carabinieri will shoot him and others for merely trying to survive, he cracks. At this climactic moment, Henry’s escapism turns into literal escape, and he dives into the river and flees the army, attempting to run away from the horrors of war for good. He reunites with Catherine in Stresa. Catherine is all too delighted to slip back into their fantasy of husband and wife with a baby on the way. However, as the novel has shown, no escape attempt goes unpunished. Catherine and Henry must flee Stresa to Switzerland to avoid Henry being arrested and likely executed as a deserter.

Even though Catherine and Henry appear to succeed in their escape, it becomes disturbingly clear that neither of them has truly considered what it might mean for them to have a baby. Catherine doesn’t change her habits, drinking excessively throughout her pregnancy. They don’t consider citizenship, marriage issues, or any other practicalities until very late into Catherine’s pregnancy. When Catherine notes that the doctor was concerned about her narrow hips, she quickly waves it away, refusing to allow any specter of grim reality into their game of playing house.  It’s inevitable that Catherine’s death is the grim conclusion to the novel. She can no longer outrun the cruel and unjust consequences of escapism. After barging his way into her hospital room, Henry realizes that even attempting to farewell Catherine cannot work. Faced with her corpse, the reality of what happened is so stark, he cannot find any comfort and solace. Henry is left bereft of comfort and escape, trapped in a cruel and cold reality that punishes those who cannot face it.

Henry watches Catherine suffer through the agony of delivering their child, Henry performs the narrative equivalent of shaking his fist at the heavens and cursing the universe. 



## Crime and Punishment 

The Idea of the Superman
At the beginning of the novel, Raskolnikov sees himself as a “superman,” a person who is extraordinary and thus above the moral rules that govern the rest of humanity. His vaunted estimation of himself compels him to separate himself from society. His murder of the pawnbroker is, in part, a consequence of his belief that he is above the law and an attempt to establish the truth of his superiority. Raskolnikov’s inability to quell his subsequent feelings of guilt, however, proves to him that he is not a “superman.” Although he realizes his failure to live up to what he has envisioned for himself, he is nevertheless unwilling to accept the total deconstruction of this identity. He continues to resist the idea that he is as mediocre as the rest of humanity by maintaining to himself that the murder was justified. It is only in his final surrender to his love for Sonya, and his realization of the joys in such surrender, that he can finally escape his conception of himself as a superman and the terrible isolation such a belief brought upon him.

Nihilism
Nihilism was a philosophical position developed in Russia in the 1850s and 1860s, known for “negating more,” in the words of Lebezyatnikov. It rejected family and societal bonds and emotional and aesthetic concerns in favor of a strict materialism, or the idea that there is no “mind” or “soul” outside of the physical world. Linked to nihilism is utilitarianism, or the idea that moral decisions should be based on the rule of the greatest happiness for the largest number of people. Raskolnikov originally justifies the murder of Alyona on utilitarian grounds, claiming that a “louse” has been removed from society. Whether or not the murder is actually a utilitarian act, Raskolnikov is certainly a nihilist; completely unsentimental for most of the novel, he cares nothing about the emotions of others. Similarly, he utterly disregards social conventions that run counter to the austere interactions that he desires with the world. However, at the end of the novel, as Raskolnikov discovers love, he throws off his nihilism. Through this action, the novel condemns nihilism as empty.

## Hamlet 

The Impossibility of Certainty
What separates Hamlet from other revenge plays (and maybe from every play written before it) is that the action we expect to see, particularly from Hamlet himself, is continually postponed while Hamlet tries to obtain more certain knowledge about what he is doing. This play poses many questions that other plays would simply take for granted. Can we have certain knowledge about ghosts? Is the ghost what it appears to be, or is it really a misleading fiend? Does the ghost have reliable knowledge about its own death, or is the ghost itself deluded? Moving to more earthly matters: How can we know for certain the facts about a crime that has no witnesses? Can Hamlet know the state of Claudius’s soul by watching his behavior? If so, can he know the facts of what Claudius did by observing the state of his soul? Can Claudius (or the audience) know the state of Hamlet’s mind by observing his behavior and listening to his speech? Can we know whether our actions will have the consequences we want them to have? Can we know anything about the afterlife? Many people have seen Hamlet as a play about indecisiveness, and thus about Hamlet’s failure to act appropriately. It might be more interesting to consider that the play shows us how many uncertainties our lives are built upon, and how many unknown quantities are taken for granted when people act or when they evaluate one another’s actions.

The Complexity of Action
Directly related to the theme of certainty is the theme of action. How is it possible to take reasonable, effective, purposeful action? In Hamlet, the question of how to act is affected not only by rational considerations, such as the need for certainty, but also by emotional, ethical, and psychological factors. Hamlet himself appears to distrust the idea that it’s even possible to act in a controlled, purposeful way. When he does act, he prefers to do it blindly, recklessly, and violently. The other characters obviously think much less about “action” in the abstract than Hamlet does, and are therefore less troubled about the possibility of acting effectively. They simply act as they feel is appropriate. But in some sense they prove that Hamlet is right, because all of their actions miscarry. Claudius possesses himself of queen and crown through bold action, but his conscience torments him, and he is beset by threats to his authority (and, of course, he dies). Laertes resolves that nothing will distract him from acting out his revenge, but he is easily influenced and manipulated into serving Claudius’s ends, and his poisoned rapier is turned back upon himself.

The Mystery of Death
In the aftermath of his father’s murder, Hamlet is obsessed with the idea of death, and over the course of the play he considers death from a great many perspectives. He ponders both the spiritual aftermath of death, embodied in the ghost, and the physical remainders of the dead, such as by Yorick’s skull and the decaying corpses in the cemetery. Throughout, the idea of death is closely tied to the themes of spirituality, truth, and uncertainty in that death may bring the answers to Hamlet’s deepest questions, ending once and for all the problem of trying to determine truth in an ambiguous world. And, since death is both the cause and the consequence of revenge, it is intimately tied to the theme of revenge and justice—Claudius’s murder of King Hamlet initiates Hamlet’s quest for revenge, and Claudius’s death is the end of that quest. The question of his own death plagues Hamlet as well, as he repeatedly contemplates whether or not suicide is a morally legitimate action in an unbearably painful world. Hamlet’s grief and misery is such that he frequently longs for death to end his suffering, but he fears that if he commits suicide, he will be consigned to eternal suffering in hell because of the Christian religion’s prohibition of suicide. In his famous “To be or not to be” soliloquy (III.i), Hamlet philosophically concludes that no one would choose to endure the pain of life if he or she were not afraid of what will come after death, and that it is this fear which causes complex moral considerations to interfere with the capacity for action.

The Nation as a Diseased Body
Everything is connected in Hamlet, including the welfare of the royal family and the health of the state as a whole. The play’s early scenes explore the sense of anxiety and dread that surrounds the transfer of power from one ruler to the next. Throughout the play, characters draw explicit connections between the moral legitimacy of a ruler and the health of the nation. Denmark is frequently described as a physical body made ill by the moral corruption of Claudius and Gertrude, and many observers interpret the presence of the ghost as a supernatural omen indicating that “[s]omething is rotten in the state of Denmark” (I.iv.67). The dead King Hamlet is portrayed as a strong, forthright ruler under whose guard the state was in good health, while Claudius, a wicked politician, has corrupted and compromised Denmark to satisfy his own appetites. At the end of the play, the rise to power of the upright Fortinbras suggests that Denmark will be strengthened once again.

Performance
Hamlet includes many references to performance of all kinds – both theatrical performance and the way people perform in daily life. In his first appearance, Hamlet draws a distinction between outward behavior— “actions that a man might play”— and real feelings: “that within which passeth show” (I.ii.). However, the more time we spend with Hamlet, the harder it becomes to tell what he is really feeling and what he is performing. He announces in Act One, scene five that he is going to pretend to be mad (“put an antic disposition on”.) In Act Two, scene one, Ophelia describes Hamlet’s mad behavior as a comical performance. However, when Hamlet tells Rosencrantz and Guildenstern that “I have lost all my mirth,” he seems genuinely depressed.

Generations of readers have argued about whether Hamlet is really mad or just performing madness. It’s impossible to know for sure – by the end of the play, even Hamlet himself doesn’t seem to know the difference between performance and reality. Hamlet further explores the idea of performance by regularly reminding the audience that we are watching a play. When Polonius says that at university he “did enact Julius Caesar” (III.ii), contemporary audiences would have thought of Shakespeare’s own Julius Caesar, which was written around the same time as Hamlet. The actor who played Polonius may have played Julius Caesar as well. The device of the play within the play gives Hamlet further opportunities to comment on the nature of theater. By constantly reminding the audience that what we’re watching is a performance, Hamlet invites us to think about the fact that something fake can feel real, and vice versa. Hamlet himself points out that acting is powerful because it’s indistinguishable from reality: “The purpose of playing […] is to hold as ’twere the mirror up to Nature” (III.ii.). That’s why he believes that the Players can “catch the conscience of the King” (II.ii.). By repeatedly showing us that performance can feel real, Hamlet makes us question what “reality” actually is.

Read about another Shakespeare play that utilizes a play within a play, A Midsummer Night’s Dream.

Madness
One of the central questions of Hamlet is whether the main character has lost his mind or is only pretending to be mad. Hamlet’s erratic behavior and nonsensical speech can be interpreted as a ruse to get the other characters to believe he’s gone mad. On the other hand, his behavior may be a logical response to the “mad” situation he finds himself in – his father has been murdered by his uncle, who is now his stepfather. Initially, Hamlet himself seems to believe he’s sane – he describes his plans to “put an antic disposition on” and tells Rosencrantz and Guildenstern he is only mad when the wind blows “north-north-west” – in other words, his madness is something he can turn on and off at will. By the end of the play, however, Hamlet seems to doubt his own sanity. Referring to himself in the third person, he says “And when he’s not himself does harm Laertes,” suggesting Hamlet has become estranged from his former, sane self. Referring to his murder of Polonius, he says, “Who does it then? His madness.” At the same time, Hamlet’s excuse of madness absolves him of murder, so it can also be read as the workings of a sane and cunning mind.

Read more about a character’s madness in another Shakespeare play,
King Lear.

Doubt
In Hamlet, the main character’s doubt creates a world where very little is known for sure. Hamlet thinks, but isn’t entirely sure, that his uncle killed his father. He believes he sees his father’s Ghost, but he isn’t sure he should believe in the Ghost or listen to what the Ghost tells him: “I’ll have grounds / More relative than this.” In his “to be or not to be” soliloquy, Hamlet suspects he should probably just kill himself, but doubt about what lies beyond the grave prevents him from acting. Hamlet is so wracked with doubt, he even works to infect other characters with his lack of certainty, as when he tells Ophelia “you should not have believed me” when he told her he loved her. As a result, the audience doubts Hamlet’s reliability as a protagonist. We are left with many doubts about the action – whether Gertrude was having an affair with Claudius before he killed Hamlet’s father; whether Hamlet is sane or mad; what Hamlet’s true feelings are for Ophelia.


## Frankenstien

Dangerous Knowledge
The pursuit of knowledge is at the heart of Frankenstein, as Victor attempts to surge beyond accepted human limits and access the secret of life. Likewise, Robert Walton attempts to surpass previous human explorations by endeavoring to reach the North Pole. This ruthless pursuit of knowledge, of the light (see “Light and Fire”), proves dangerous, as Victor’s act of creation eventually results in the destruction of everyone dear to him, and Walton finds himself perilously trapped between sheets of ice. Whereas Victor’s obsessive hatred of the monster drives him to his death, Walton ultimately pulls back from his treacherous mission, having learned from Victor’s example how destructive the thirst for knowledge can be.

Alienation
Frankenstein suggests that social alienation is both the primary cause of evil and the punishment for it. The Monster explicitly says that his alienation from mankind has caused him to become a murderer: “My protectors had departed, and had broken the only link that held me to the world. For the first time the feelings of revenge and hatred filled my bosom.” His murders, however, only increase his alienation.

For Frankenstein, too, alienation causes him to make bad decisions and is also the punishment for those bad decisions. When Frankenstein creates the Monster he is working alone, in a “solitary chamber, or rather cell.” Being “solitary” has caused his ambition to grow dangerously, but this isolation is already its own punishment: his laboratory feels like a “cell.” Once he has created the Monster, Frankenstein becomes even more alienated from the people around him because he can’t tell anyone about his creation.

Both Frankenstein and the Monster compare themselves to the character of Satan in Paradise Lost: alienation from God is both Satan’s crime and his punishment. The novel presents the idea that alienation from other people is caused, at root, by alienation from oneself. Frankenstein’s father points out the link between self-hatred and alienation: “I know that while you are pleased with yourself, you will think of us with affection, and we shall hear regularly from you.” As long as a person feels they have self-worth, they’ll maintain contact with others. The Monster feels that he is alienated from human society because he looks monstrous. He first recognizes that he is ugly not through someone else’s judgement but through his own: “when I viewed myself in a transparent pool[…]I was filled with the bitterest sensations.”

At the end of the novel, with Frankenstein dead, the Monster is alone in the world. His alienation is complete, and so is his self-hatred: “You hate me; but your abhorrence cannot equal that with which I regard myself.” The ultimate consequence of alienation is self-destruction. Frankenstein drives himself to death chasing the Monster, while the Monster declares his intention to kill himself.




## Robinson Crusoe 

The Ambivalence of Mastery 
Crusoe’s success in mastering his situation, overcoming his obstacles, and controlling his environment shows the condition of mastery in a positive light, at least at the beginning of the novel. Crusoe lands in an inhospitable environment and makes it his home. His taming and domestication of wild goats and parrots with Crusoe as their master illustrates his newfound control. Moreover, Crusoe’s mastery over nature makes him a master of his fate and of himself. Early in the novel, he frequently blames himself for disobeying his father’s advice or blames the destiny that drove him to sea. But in the later part of the novel, Crusoe stops viewing himself as a passive victim and strikes a new note of self-determination. In building a home for himself on the island, he finds that he is master of his life—he suffers a hard fate and still finds prosperity.

But this theme of mastery becomes more complex and less positive after Friday’s arrival, when the idea of mastery comes to apply more to unfair relationships between humans. In Chapter XXIII, Crusoe teaches Friday the word “[m]aster” even before teaching him “yes” and “no,” and indeed he lets him “know that was to be [Crusoe’s] name.” Crusoe never entertains the idea of considering Friday a friend or equal—for some reason, superiority comes instinctively to him. We further question Crusoe’s right to be called “[m]aster” when he later refers to himself as “king” over the natives and Europeans, who are his “subjects.” In short, while Crusoe seems praiseworthy in mastering his fate, the praiseworthiness of his mastery over his fellow humans is more doubtful. Defoe explores the link between the two in his depiction of the colonial mind.

The Necessity of Repentance
Crusoe’s experiences constitute not simply an adventure story in which thrilling things happen, but also a moral tale illustrating the right and wrong ways to live one’s life. This moral and religious dimension of the tale is indicated in the Preface, which states that Crusoe’s story is being published to instruct others in God’s wisdom, and one vital part of this wisdom is the importance of repenting one’s sins. While it is important to be grateful for God’s miracles, as Crusoe is when his grain sprouts, it is not enough simply to express gratitude or even to pray to God, as Crusoe does several times with few results. Crusoe needs repentance most, as he learns from the fiery angelic figure that comes to him during a feverish hallucination and says, “Seeing all these things have not brought thee to repentance, now thou shalt die.” Crusoe believes that his major sin is his rebellious behavior toward his father, which he refers to as his “original sin,” akin to Adam and Eve’s first disobedience of God. This biblical reference also suggests that Crusoe’s exile from civilization represents Adam and Eve’s expulsion from Eden.

For Crusoe, repentance consists of acknowledging his wretchedness and his absolute dependence on the Lord. This admission marks a turning point in Crusoe’s spiritual consciousness, and is almost a born-again experience for him. After repentance, he complains much less about his sad fate and views the island more positively. Later, when Crusoe is rescued and his fortune restored, he compares himself to Job, who also regained divine favor. Ironically, this view of the necessity of repentance ends up justifying sin: Crusoe may never have learned to repent if he had never sinfully disobeyed his father in the first place. Thus, as powerful as the theme of repentance is in the novel, it is nevertheless complex and ambiguous.

Read more about Job, the biblical figure Crusoe compares himself to.
  

The Importance of Self-Awareness
Crusoe’s arrival on the island does not make him revert to a brute existence controlled by animal instincts, and, unlike animals, he remains conscious of himself at all times. Indeed, his island existence actually deepens his self-awareness as he withdraws from the external social world and turns inward. The idea that the individual must keep a careful reckoning of the state of his own soul is a key point in the Presbyterian doctrine that Defoe took seriously all his life. We see that in his normal day-to-day activities, Crusoe keeps accounts of himself enthusiastically and in various ways. For example, it is significant that Crusoe’s makeshift calendar does not simply mark the passing of days, but instead more egocentrically marks the days he has spent on the island: it is about him, a sort of self-conscious or autobiographical calendar with him at its center. Similarly, Crusoe obsessively keeps a journal to record his daily activities, even when they amount to nothing more than finding a few pieces of wood on the beach or waiting inside while it rains. Crusoe feels the importance of staying aware of his situation at all times. We can also sense Crusoe’s impulse toward self-awareness in the fact that he teaches his parrot to say the words, “Poor Robin Crusoe. . . . Where have you been?” This sort of self-examining thought is natural for anyone alone on a desert island, but it is given a strange intensity when we recall that Crusoe has spent months teaching the bird to say it back to him. Crusoe teaches nature itself to voice his own self-awareness.

The Footprint
Crusoe’s shocking discovery of a single footprint on the sand in Chapter XVIII is one of the most famous moments in the novel, and it symbolizes our hero’s conflicted feelings about human companionship. Crusoe has earlier confessed how much he misses companionship, yet the evidence of a man on his island sends him into a panic. Immediately he interprets the footprint negatively, as the print of the devil or of an aggressor. He never for a moment entertains hope that it could belong to an angel or another European who could rescue or befriend him. This instinctively negative and fearful attitude toward others makes us consider the possibility that Crusoe may not want to return to human society after all, and that the isolation he is experiencing may actually be his ideal state.


## Call of the Wild 

The Indispensable Struggle for Mastery
The Call of the Wild is a story of transformation in which the old Buck—the civilized, moral Buck—must adjust to the harsher realities of life in the frosty North, where survival is the only imperative. Kill or be killed is the only morality among the dogs of the Klondike, as Buck realizes from the moment he steps off the boat and watches the violent death of his friend Curly. The wilderness is a cruel, uncaring world, where only the strong prosper. It is, one might say, a perfect Darwinian world, and London’s depiction of it owes much to Charles Darwin, who proposed the theory of evolution to explain the development of life on Earth and envisioned a natural world defined by fierce competition for scarce resources. The term often used to describe Darwin’s theory, although he did not coin it, is “the survival of the fittest,” a phrase that describes Buck’s experience perfectly. In the old, warmer world, he might have sacrificed his life out of moral considerations; now, however, he abandons any such considerations in order to survive.

But London is not content to make the struggle for survival the central theme of his novel; instead, his protagonist struggles toward a higher end, namely mastery. We see this struggle particularly in Buck’s conflict with Spitz, in his determination to become the lead dog on Francois and Perrault’s team, and, at the end of the novel, in the way that he battles his way to the leadership of the wolf pack. Buck does not merely want to survive; he wants to dominate—as do his rivals, dogs like Spitz.

In this quest for domination, which is celebrated by London’s narrative, we can observe the influence of Friedrich Nietzsche, a German philosopher of the late 19th century. Nietzsche’s worldview held that the world was composed of masters, those who possessed what he called “the will to power,” and slaves, those who did not possess this will. Nietzsche delighted in using animal metaphors, comparing masters to “birds of prey” and “blonde beasts” and comparing slaves to sheep and other herd animals. London’s Buck, with his indomitable strength and fierce desire for mastery, is a canine version of Nietzsche’s masterful men, his Napoleon Bonapartes and Julius Caesars. Buck is a savage creature, in a sense, and hardly a moral one, but London, like Nietzsche, expects us to applaud this ferocity. His novel suggests that there is no higher destiny for man or beast than to struggle, and win, in the battle for mastery.

The Power of Ancestral Memory and Primitive Instincts
When Buck enters the wild, he must learn countless lessons in order to survive, and he learns them well. But the novel suggests that his success in the frozen North is not merely a matter of learning the ways of the wild; rather, Buck gradually recovers primitive instincts and memories that his wild ancestors possessed, which have been buried as dogs have become civilized creatures. The technical term for what happens to Buck is atavism—the reappearance in a modern creature of traits that defined its remote forebears.

London returns to this theme again and again, constantly reminding us that Buck is “retrogressing,” as the novel puts it, into a wilder way of life that all dogs once shared. “He was older than the days he had seen and the breaths he had drawn,” we are told. “He linked the past with the present, and the eternity behind him throbbed through him in a mighty rhythm to which he swayed as the tides and seasons swayed.” Buck even has occasional visions of this older world, when humans wore animal skins and lived in caves, and when wild dogs hunted their prey in the primeval forests. His connection to his ancestral identity is thus more than instinctual; it is mystical. The civilized world, which seems so strong, turns out to be nothing more than a thin veneer, which is quickly worn away to reveal the ancient instincts lying dormant underneath. Buck hears the call of the wild, and London implies that, in the right circumstances, we might hear it too.

The Laws of Civilization and of Wilderness
While the two lives that Buck leads stand in stark contrast to each other, this contrast does not go unchallenged throughout the novel. His life with Judge Miller is leisurely, calm, and unchallenging, while his transition to the wilderness shows him a life that is savage, frenetic, and demanding. While it would be tempting to assume that these two lives are polar opposites, events later in the novel show some ways in which both the wild and civilization have underlying social codes, hierarchies, and even laws. For example, the pack that Buck joins is not anarchic; the position of lead dog is coveted and given to the most powerful dog. The lead dog takes responsibility for group decisions and has a distinctive style of leadership; the main factor in the rivalry between Buck and Spitz is that Buck sides with the less popular, marginal dogs instead of the stronger ones. Buck, then, advocates the rights of a minority in the pack—a position that is strikingly similar to that of his original owner, the judge, who is the novel’s most prominent example of civilization.

The rules of the civilized and uncivilized worlds are, of course, extremely different—in the wild, many conflicts are resolved through bloody fights rather than through reasoned mediation. But the novel suggests that what is important in both worlds is to understand and abide by the rules which that world has set up, and it is only when those rules are broken that we see true savagery and disrespect for life. Mercedes, Hal, and Charles enter the wild with little understanding of the rules one must follow to become integrated and survive. Their inability to ration food correctly, their reliance upon their largely useless knife and gun, and their disregard for the dogs’ suffering all attest to laws of the wilderness that they misunderstand or choose to ignore. As a result, the wilderness institutes a natural consequence for their actions. Precisely because they do not heed the warnings that the wild provides via one of its residents, John Thornton, they force the team over unstable ice and fall through to their deaths. The novel seems to say that the wild does not allow chaos or wanton behavior but instead institutes a strict social and natural order different from, but not inferior to, that of the civilized world.

The Membership of the Individual in the Group
When Buck arrives in the wild, his primordial instincts do not awaken immediately, and he requires a great deal of external help before he is suited to life there. Help arrives in realizations about the very different rules that govern the world outside of civilization, but also in the support of the pack of which he becomes a part. Two dogs in particular, Dave and Sol-leks, after having established their seniority, instruct Buck in the intricacies of sled pulling. Furthermore, the group members take pride in their work, even though they are serving men. When they make trips in good time, they congratulate themselves—they all participate in a common enterprise.

At the same time, however, one of the most valued traits in the wilderness is individualism. If The Call of the Wild is a story about ultimately achieving mastery over a foreign, primal world, that mastery is achieved only through separation from the group and independent survival. Throughout much of the story, Buck is serving a master or a pack; even as a leader he is carrying out someone else’s commands and is responsible for the well-being of the group. In many ways, then, when John Thornton cuts Buck free from his harness, he is also beginning the process of Buck’s separation from a pack mentality. Although Buck continues to serve Thornton, his yearnings for a solitary life in the wild eventually overcome him.

The balance between individual and group is disrupted once more, however, toward the end of the novel, when Buck becomes the leader of a wolf pack. Although the pack is much different from the dog pack whose responsibility was to serve humans by pulling sleds, the message seems to be that, while encouraging the skills to survive on one’s own, the wild ultimately requires the cooperation of a group in order to ensure individual survival.


Mercedes’s Possessions
Mercedes loads the sled up with so many of her things that the dogs cannot possibly pull it; later, she herself gets on the sled, making the load even heavier. Her insistence on having all of her possessions with her emphasizes the difference between the wild, where the value of an object lies in its immediate usefulness, and civilization, where the value of an object lies in its ability to symbolize the wealth of its possessor. Material possessions and consumerism have no place in the wild, and it is at least partly Mercedes’ inability to recognize this fact that leads to her death when the overburdened sled falls through the ice.


Buck’s Traces

The significance of Buck’s traces—the straps that bind him to the rest of the team—changes as the plot develops. The novel initially charts his descent from his position as the monarch of Judge Miller’s place in civilization to a servile status in which it is his duty to pull the sled for humans. But as he becomes more a part of the wild, Buck begins to understand the hierarchy of the pack that pulls the sled, and he begins to gain authority over the pack. After his duel with Spitz, he is harnessed into the lead dog’s position; his harness now represents not servitude to the humans but leadership over the dogs. Finally, however, John Thornton cuts Buck free from his traces, an act that symbolizes his freedom from a world in which he serves humans. Now a companion to Thornton rather than a servant, Buck gradually begins to enter a world of individual survival in the wild.

Buck’s First Beatings with the Club; Curly’s Death

When Buck is kidnapped, he attempts to attack one of the men who has seized him, only to be beaten repeatedly with a club. This moment, when his fighting spirit is temporarily broken, along with the brutal killing of Curly by a group of vicious sled dogs, symbolizes Buck’s departure from the old, comfortable life of a pet in a warm climate, and his entrance into a new world where the only law is “the law of club and fang.”

Buck’s Attack on the Yeehats

In the closing chapters of the novel, Buck feels the call of life in the wild drawing him away from mankind, away from campfires and towns, and into the forest. The only thing that prevents him from going, that keeps him tied to the world of men, is his love for John Thornton. When the Yeehat Indians kill Thornton, Buck’s last tie to humanity is cut, and he becomes free to attack the Yeehats, killing a number of them. To attack a human being would once have been unthinkable for Buck, and his willingness to do so now symbolizes the fact that his transformation is complete—that he has truly embraced his wild nature.

## Dante's Inferno 

The Perfection of God’s Justice
Dante creates an imaginative correspondence between a soul’s sin on Earth and the punishment he or she receives in Hell. The Sullen choke on mud, the Wrathful attack one another, the Gluttonous are forced to eat excrement, and so on. This simple idea provides many of Inferno’s moments of spectacular imagery and symbolic power, but also serves to illuminate one of Dante’s major themes: the perfection of God’s justice. The inscription over the gates of Hell in Canto III explicitly states that God was moved to create Hell by Justice (III.7). Hell exists to punish sin, and the suitability of Hell’s specific punishments testify to the divine perfection that all sin violates. This notion of the suitability of God’s punishments figures significantly in Dante’s larger moral messages and structures Dante’s Hell.

To modern readers, the torments Dante and Virgil behold may seem shockingly harsh: homosexual people must endure an eternity of walking on hot sand; those who charge interest on loans sit beneath a rain of fire. However, when we view the poem as a whole, it becomes clear that the guiding principle of these punishments is one of balance. Sinners suffer punishment to a degree befitting the gravity of their sin, in a manner matching that sin’s nature. The design of the poem serves to reinforce this correspondence: in its plot it progresses from minor sins to major ones (a matter of degree); and in the geographical structure it posits, the various regions of Hell correspond to types of sin (a matter of kind). Because this notion of balance informs all of God’s chosen punishments, His justice emerges as rigidly objective, mechanical, and impersonal; there are no extenuating circumstances in Hell, and punishment becomes a matter of nearly scientific formula.

Early in Inferno, Dante builds a great deal of tension between the objective impersonality of God’s justice and the character Dante’s human sympathy for the souls that he sees around him. As the story progresses, however, the character becomes less and less inclined toward pity, and repeated comments by Virgil encourage this development. Thus, the text asserts the infinite wisdom of divine justice: sinners receive punishment in perfect proportion to their sin; to pity their suffering is to demonstrate a lack of understanding.

Read about the related theme of the limits of human justice in Alexandre Dumas’s The Count of Monte Cristo.

Evil as the Contradiction of God’s Will
In many ways, Dante’s Inferno can be seen as a kind of imaginative taxonomy of human evil, the various types of which Dante classifies, isolates, explores, and judges. At times we may question its organizing principle, wondering why, for example, a sin punished in the Eighth Circle of Hell, such as accepting a bribe, should be considered worse than a sin punished in the Sixth Circle of Hell, such as murder. To understand this organization, one must realize that Dante’s narration follows strict doctrinal Christian values.

His moral system prioritizes not human happiness or harmony on Earth but rather God’s will in Heaven. Dante thus considers violence less evil than fraud: of these two sins, fraud constitutes the greater opposition to God’s will. God wills that we treat each other with the love he extends to us as individuals; while violence acts against this love, fraud constitutes a perversion of it. A fraudulent person affects care and love while perpetrating sin against it. Yet, while Inferno implies these moral arguments, it generally engages in little discussion of them.

In the end, it declares that evil is evil simply because it contradicts God’s will, and God’s will does not need further justification. Dante’s exploration of evil probes neither the causes of evil, nor the psychology of evil, nor the earthly consequences of bad behavior. Inferno is not a philosophical text; its intention is not to think critically about evil but rather to teach and reinforce the relevant Christian doctrines.

Read about the related theme of sin, redemption, and damnation in Christopher Marlowe’s Doctor Faustus.

Storytelling as a Way to Achieve Immortality
Dante places much emphasis in his poem on the notion of immortality through storytelling, everlasting life through legend and literary legacy. Several shades ask the character Dante to recall their names and stories on Earth upon his return. They hope, perhaps, that the retelling of their stories will allow them to live in people’s memories. The character Dante does not always oblige; for example, he ignores the request of the Italian souls in the Ninth Pouch of the Eighth Circle of Hell that he bring word of them back to certain men on Earth as warnings.

However, the poet Dante seems to have his own agenda, for his poem takes the recounting of their stories as a central part of its project. Although the poet repeatedly emphasizes the perfection of divine justice and the suitability of the sinners’ punishments, by incorporating the sinners’ narratives into his text he also allows them to live on in some capacity aboveground. Yet, in retelling the sinners’ stories, the poet Dante may be acting less in consideration of the sinners’ immortality than of his own. Indeed, Dante frequently takes opportunities to advance his own glory.

For example, in Canto XXIV, halfway through his description of the Thieves’ punishment, Dante declares outright that he has outdone both Ovid and Lucan in his ability to write scenes of metamorphosis and transformation (Ovid’s Metamorphoses focuses entirely on transformations; Lucan wrote the Pharsalia, an account of the Roman political transition and turmoil in the first century b.c.). By claiming to have surpassed two of the classical poets most renowned for their mythological inventions and vivid imagery, Dante seeks to secure his own immortality.

Thus, Dante presents storytelling as a vehicle for multiple legacies: that of the story’s subject as well as that of the storyteller. While the plot of a story may preserve the living memory of its protagonist, the story’s style and skill may serve the greater glory of its author. Although many of his sinners die a thousand deaths—being burned, torn to bits, or chewed to pieces, only to be reconstituted again and again—Dante emphasizes with almost equal incessancy the power of his narrative to give both its subjects and its author the gift of eternal life.

## The Prince 



## Beowulf 


Beowulf is an Old English epic poem set in 6th century Scandinavia

- The story begins with a group of Danes partying in a mead hall

- All is joyful in their kingdom — but an evil “shadow walker” named Grendel despises the sounds of joy and descends into the town 

- The monster bursts into the mead hall, attacking the Danes

- He easily overpowers them and kills dozens before retreating back into the shadows. He returns often, dealing out death each time

- Across the sea, Beowulf hears of the Danes’ troubles and sails to help them, and promises to slay the monster
- That night, he and his men lay a trap — they wait for Grendel in the mead hall
- Beowulf pretends to sleep as the creature lurks closer…. As Grendel bursts in, the men draw their swords
- But Beowulf says he’s Grendel’s equal, and fights without weapons. After a fierce battle, he rips off the monster’s arm
- Grendel flees from the hall and dies from his wound — but Beowulf’s troubles are far from over...
- The next night, Grendel’s mother attacks the Danes as they celebrate
- Beowulf pursues her to her underwater lair, and another battle (packed w/ sexual & religious symbolism) ensues. Finally, Beowulf emerges triumphant
- He returns home a rich man — but the worst is yet to come
- 50 years later, Beowulf is king of his people — but now he faces the ultimate crisis:
- A dragon is on the rampage, burning everything in sight after gold was stolen from his lair
- Though Beowulf is well past his prime, he sets out for one last battle.Beowulf fights and slays the dragon, but he’s killed in the process
- His people mourn his loss and build a funeral pyre, dreading what will happen without his leadership
- It’s a tragic ending — but where does Tolkien come into play?


Tolkien said it is the MONSTERS that unlock the key to the story's meaning…

"The monsters are symbols of the inevitable hostility of the world itself to mortal men…they do not only bring physical ruin but spiritual despair"

The story of Beowulf, then, teaches you how to triumph in the face of evil

Tolkien continues:

"Beowulf is not a hero because he wins but because he fights, even when he knows the battle will bring his doom. His death is the crown of his life."

In other words, glory isn't won only in victory — it's won by giving your all to fight for the good

Tolkien formalized this argument in a famous 1936 lecture. Tolkien sparked a revolution that brought Beowulf back into the popular conscience

It means fighting for the good, even — or better yet, especially — in the face of all odds

It proves that the values of adventure, honor, heroism, and sacrifice are truly timeless

They’re what make for the best stories — in literature, in film, and in your own life

Monsters
In Christian medieval culture, monster was the word that referred to birth defects, which were always understood as an ominous sign from God—a sign of transgression or of bad things to come. In keeping with this idea, the monsters that Beowulf must fight in this Old English poem shape the poem’s plot and seem to represent an inhuman or alien presence in society that must be exorcised for the society’s safety. They are all outsiders, existing beyond the boundaries of human realms. Grendel’s and his mother’s encroachment upon human society—they wreak havoc in Heorot—forces Beowulf to kill the two beasts for order to be restored.

To many readers, the three monsters that Beowulf slays all seem to have a symbolic or allegorical meaning. For instance, since Grendel is descended from the biblical figure Cain, who slew his own brother, Grendel often has been understood to represent the evil in Scandinavian society of marauding and killing others. A traditional figure of medieval folklore and a common Christian symbol of sin, the dragon may represent an external malice that must be conquered to prove a hero’s goodness. Because Beowulf’s encounter with the dragon ends in mutual destruction, the dragon may also be interpreted as a symbolic representation of the inevitable encounter with death itself.

The Cyclical Nature of Life
Beowulf is structured primarily around three main battles with great foes. Though Beowulf’s fight with Grendel immediately incurs the retaliation of Grendel’s mother, the fight with the dragon doesn’t occur until fifty years later. These three antagonist fights serve as an allegorical tableau of the life of a leader. Conflict arises in the form of Grendel, and the fallout from his defeat leads to direct, almost immediate consequences. In the fifty years that follow, seasons of peace and disruption ebb and flow, and even though it is ultimately the dragon that kills Beowulf, the cyclical nature of life is such that if it wasn’t the dragon, it would be something else; death is inevitable, whether it be at the hands of this foe or the next, as exemplified in the concept of wyrd. 

Throughout the poem, the reader sees old kings pass on and new kings rise and grow old themselves, weaving a pattern of ongoing change, conflict, and resolution. Perhaps best exemplifying the cyclical nature of life is Hrothgar, who has lived through enough to know that there is no true sense of stability. Kingdoms will see times of peace and times of adversity, and a wise king not only knows to anticipate both, but also appreciates the value in savoring beauty and hope when it is present.

Tensions Between the Heroic Code and Other Value Systems

Much of Beowulf is devoted to articulating and illustrating the Germanic heroic code, which values strength, courage, and loyalty in warriors; hospitality, generosity, and political skill in kings; ceremoniousness in women; and good reputation in all people. Traditional and much respected, this code is vital to warrior societies as a means of understanding their relationships to the world and the menaces lurking beyond their boundaries. All of the characters’ moral judgments stem from the code’s mandates. Thus individual actions can be seen only as either conforming to or violating the code.

The Difference Between a Good Warrior and a Good King

Over the course of the poem, Beowulf matures from a valiant combatant into a wise leader. His transition demonstrates that a differing set of values accompanies each of his two roles. The difference between these two sets of values manifests itself early on in the outlooks of Beowulf and King Hrothgar. Whereas the youthful Beowulf, having nothing to lose, desires personal glory, the aged Hrothgar, having much to lose, seeks protection for his people. Though these two outlooks are somewhat oppositional, each character acts as society dictates he should given his particular role in society.


While the values of the warrior become clear through Beowulf’s example throughout the poem, only in the poem’s more didactic moments are the responsibilities of a king to his people discussed. The heroic code requires that a king reward the loyal service of his warriors with gifts and praise. It also holds that he must provide them with protection and the sanctuary of a lavish mead-hall. Hrothgar’s speeches, in particular, emphasize the value of creating stability in a precarious and chaotic world. He also speaks at length about the king’s role in diplomacy, both with his own warriors and with other tribes.

Beowulf’s own tenure as king elaborates on many of the same points. His transition from warrior to king, and, in particular, his final battle with the dragon, rehash the dichotomy between the duties of a heroic warrior and those of a heroic king. In the eyes of several of the Geats, Beowulf’s bold encounter with the dragon is morally ambiguous because it dooms them to a kingless state in which they remain vulnerable to attack by their enemies. Yet Beowulf also demonstrates the sort of restraint proper to kings when, earlier in his life, he refrains from usurping Hygelac’s throne, choosing instead to uphold the line of succession by supporting the appointment of Hygelac’s son. But since all of these pagan kings were great warriors in their youth, the tension between these two important roles seems inevitable and ultimately irreconcilable.

Evil

Many readers have seen Beowulf’s monsters as embodiments of evil, representing the idea that evil is a mysterious, inhuman force. All three monsters emerge from darkness, inflicting fear and suffering on the poem’s human characters. Grendel, in particular, is closely allied with the forces of evil. He is a “fiend out of hell” (l.100) and a descendant of the cursed sinner Cain. However, none of the monsters acts out of sheer evil alone. Grendel’s mother is legitimately seeking vengeance for her son’s death. Even Grendel nurses “a hard grievance” (l.87), and we understand that even if his deeds are evil, Grendel acts out of isolation, envy, and fear. By giving the monsters comprehensible, human motives and at moments even showing us their points of view, Beowulf humanizes evil, suggesting that evil is both an unspeakable threat from the darkness and at the same time an ordinary part of human life. When we hear the poem’s stories of war between humans, of Beowulf and Hygelac emerging from the sea to slaughter their enemies, we might begin to wonder if there’s anything inhuman at all about Grendel or his mother.

Treasure

Although “glory” (l.1388), is what motivates Beowulf and the other heroic warriors of the poem, they measure their glory in treasure. The gloriousness of Beowulf’s achievement in killing Grendel is measured by the amount of treasure Hrothgar gives him as a reward. At the same time, Hrothgar’s gloriousness as a king can be measured by his generosity with his treasure. When Beowulf gives the lion’s share of his reward to Hygelac, it shows us in quantifiable terms how loyal Beowulf is to his king, and therefore how well he upholds the warrior code, while also indicating how excellent a king Hygelac is. However, Beowulf is deeply skeptical about the value of treasure. The poem’s biggest hoard of treasure belongs to the monstrous dragon, and it does him no good. When Wiglaf enters the barrow to examine the hoard, he finds it already “tarnished and corroding” (ll.2761-2). Many readers have found Beowulf’s dying wish to see the treasure he has won disquieting. To the poem’s original Christian audience, it may have been even more disquieting: it’s a reminder that, in his final moments, Beowulf’s mind is on temporary, worldly things instead of God and eternal life.

Mortality

On one level, Beowulf is from beginning to end a poem about confronting death. It begins with a funeral, and proceeds to the story of a murderous monster. Beowulf enters the story as a hero who has chosen to risk death in order to achieve fame. As Beowulf fights Grendel’s mother at the bottom of the mere, even his close friends believe he has died. Some readers have seen his journey to the bottom of the mere as a symbolic death, drawing on the Christian story of the “Harrowing of Hell,” in which Jesus, after dying on the Cross, descends to Hell in order to divide the saved from the damned. The final third of the poem is devoted to Beowulf’s death and funeral. Some readers have argued that the poem presents pagan mortality as tragic: Beowulf and the other heroes lead frightening, death-filled lives, and die without any hope of salvation. However, other readers have found Beowulf all the more heroic because he accomplishes his deeds in the shadow of certain death, without hope of resurrection. For these readers, Beowulf suggests that a good, brave life is worth living at any cost.


## Animal Farm 

The Abuse of Language as Instrumental to the Abuse of Power
One of Orwell’s central concerns, both in Animal Farm and in 1984, is the way in which language can be manipulated as an instrument of control. In Animal Farm, the pigs gradually twist and distort a rhetoric of socialist revolution to justify their behavior and to keep the other animals in the dark. The animals heartily embrace Major’s visionary ideal of socialism, but after Major dies, the pigs gradually twist the meaning of his words. As a result, the other animals seem unable to oppose the pigs without also opposing the ideals of the Rebellion.

By the end of the novella, after Squealer’s repeated reconfigurations of the Seven Commandments in order to decriminalize the pigs’ treacheries, the main principle of the farm can be openly stated as “all animals are equal, but some animals are more equal than others.” This outrageous abuse of the word “equal” and of the ideal of equality in general typifies the pigs’ method, which becomes increasingly audacious as the novel progresses. Orwell’s sophisticated exposure of this abuse of language remains one of the most compelling and enduring features of Animal Farm, worthy of close study even after we have decoded its allegorical characters and events.

Corruption

Animal Farm demonstrates the idea that power always corrupts. The novella’s heavy use of foreshadowing, especially in the opening chapter, creates the sense that the events of the story are unavoidable. Not only is Napoleon’s rise to power inevitable, the novella strongly suggests that any other possible ruler would have been just as bad as Napoleon. Although Napoleon is more power-hungry than Snowball, plenty of evidence exists to suggest that Snowball would have been just as corrupt a ruler. Before his expulsion, Snowball goes along with the pigs’ theft of milk and apples, and the disastrous windmill is his idea. Even Old Major is not incorruptible. Despite his belief that “all animals are equal,” (Chapter 1) he lectures the other animals from a raised platform, suggesting he may actually view himself as above the other animals on the farm. In the novel’s final image the pigs become indistinguishable from human farmers, which hammers home the idea that power inevitably has the same effect on anyone who wields it.

The Failure of Intellect

The pigs intelligence rarely produces anything of value. Instead, the pigs use their intelligence to manipulate and abuse the other animals.
Other ways in which intelligence fails to be useful or good:
- Benjamin is literate, but he refuses to read >> intelligence is worthless without the moral sense to engage in politics and the courage to act
- The dogs are nearly as literate as the pigs, but they are “not interested in reading anything except the Seven Commandments” >> Intellect is useless—even harmful—when it is combined with a personality that prefers to obey orders rather than question them.




