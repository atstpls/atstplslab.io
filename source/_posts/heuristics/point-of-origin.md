---
title: point of origin
date: 2023-04-10 09:53:32
tags: [money]
category: [ecosystem, resources]
---

<br>

#### ROOT CONCEPTS 

Can't hear it, need to do it    ------>     Too complex, must embody the principle to truly understand it


I've been making charts/graphs  --->  Stories are best, Bible,  Million dollar baby


https://www.youtube.com/shorts/cqliqDVxQhQ



##################

1. Goals, not objects, determine worldviews 

2. Worldviews evolve through learning experiences that produce knowledge

3. Valued knowledge is shared using abstractions 

4. Valued abstractions are shared across time using stories

5. Valued stories contain valued knowledge

6. Divine stories contain divine knowledge




#### CATEGORIZATION

Science can't tell us what ought to be (values) only what is (facts). 

It can tell us how we implement and pursue the values that we have predicated.

#### IDEALS 

Deduction - making an inference of concrete instances based on an abstract principle 

We see the world through a narrative framework

We need a mechanism to must prioritize our attention (imposing a value structure) because it is a finite resource and it's costly 

Mechanisms are stories, The people who criticize our stories, criticize the mechanism through which we look at the world 



#### MATTER and WHAT MATTERS


Ancient stories of the Bible are about humanity’s conflicted attempts to reconcile spiritual truth with physical reality in the hopes of acquiring divine knowledge

Assuming that ancient words had the same meaning as their modern counterparts is a mistake

There are tremendous gaps between ancient and modern definitions such that every word must be reconstructed
within the framework of an archaic cosmology.

We need frames that faithfully recapture the biblical worldview 

In the Bible, raw ‘earth’ refers to matter without meaning

pure ‘heaven’ refers to spiritual meaning without corporeal existence




In the beginning, God created the heaven [spiritual reality] and the earth [corporeal reality].

From a material perspective, the universe resembles a gigantic machine composed of meaningless matter and mindless energy. This
worldview has little to do with the spiritual perspective, which likens the universe to a written language.

From the spiritual perspective, creation is viewed as the manifestation of divine language. 
To understand the book of Genesis and its ancient cosmology, we must posit a universe founded on meaning and language instead of mindless mechanical causality. 
A divine language organizes facts and events in the world to embody metaphysical truth 



In this context, the notion that Adam is “in the image of God”
means that humanity is a symbol of the Creator within creation. Thus,
Adam is the embodiment of divine knowledge in the world. Unlike
regular knowledge, which involves the union of spirit and matter for
created things, this type of knowledge transcends itself into a form of
metacognition.

Even though God’s name is extremely simple, it fully encapsu-
lates the agreement between Israel and God to “perform with deeds
everything spoken with words” or “I am [in deed] what I am [in word]”
or “I am [on earth] what I am [in heaven].” So even though it appears
to say very little, this name perfectly symbolizes the seed that contains
the entire agreement in principle. This “cosmic seed” resembles an axi-
om in mathematics, a self-evident principle that cannot be proven due
to its simplicity but from which everything must derive.
As such, the name of God is the principle of all Mosaic Laws. For
example, a law like “thou shall not lie” is an obvious expression of this
identity at the human level. The only difference, in this case, is that

it was expressed in negative terms. When articulated positively, this
law becomes: “thou shall be true” or “words must agree with facts” or
“heaven must agree with earth.” All of these formulations express the
same identity (I am what I am) at different scales of reality.
In theory, every single Mosaic Law can be derived as a practical
implication of God’s identity. 

However, it would be counterproductive
to examine the corpus of the law with this intention. Instead, we will
focus entirely on a single example—thou shall not kill—to demonstrate
how a trivial law expresses its ramifications or “materializes itself” at
the level of human interactions.

Thou shall not kill . . . But are all murders equal? What if I kill
my enemy? My parents? My child? My servant? My dog? What if I kill
someone in an accident or in self-defense? What if I injure someone in
a fight and they lose their livelihood and eventually die?

specific laws are formed by the descent of a metaphysical
principle into the details of physical reality. This material obscurity
is necessary to reveal the identity of God to humanity. Otherwise, the
highest principles would be too abstract and simple to know.

On the whole, there must be an exchange between heaven and
earth for the knowledge of God within creation. God’s identity must
lower itself into practical reality by elaborating laws, and the masses of
Israelites must raise themselves into significance by embodying them
as deeds. Laws bring light and meaning to human events, and human
questions or “problems” provide tangible expressions for God’s spirit.
Ultimately, the purpose of these interactions is for Adam to fully em-
body the image of God at the communal scale


Crucially, having a goal (end, desire, wish, purpose) means that you now have a pragmatic (satisficing) way to judge relevance, and therefore a stopping rule for categorization. This is a solution that works for finite creatures like us.

A contextual standard of value is basically a quasi-Aristotelian teleological morality in a new pragmatic form, which flips around Aristotle's original perspective: instead of the formal cause (essence) defining the final cause (purpose), it's now that the final cause (goal) defines the formal cause (functional category).



#### CHAOS, ORDER, and LOGOS 


Peterson (2007) says that chaos, order, and Logos are the most fundamental (archetypal) categories of experience, on which all particularized narratives and philosophies are based:

    ". . . these [three] domains are fundamental to instinctual religious thought . . . these categories of experience are not derived from anything more fundamental. Instead, they are the axiomatic entities from which everything else is, in turn, derived."

Two philosophical archetypes

Finally, we can directly relate this to the philosophy discussed previously. Essentially, abstractionism is the worldview of the left hemisphere, and contextualism is the worldview of the right hemisphere. Roughly, the LH (the "emissary") sees order (being; stability; objectivity; deductive reasoning; literalness; etc) as prior to chaos (becoming; change; subjectivity; inductive/abductive reasoning; metaphor; etc) — and the RH (the "master") sees things exactly the other way around. 


Vervake

Concepts are generative models that allow us to predict and explain how things are going to behave especially in relation to us  
Method moving forward is not
Incremental fact gathering... they're infinite, there are too many.  Without unifying theories, you have no way to integrate across facts in a coherent manner, all you have is endless generation of valid but pointless facts  

The opposite is an elegant unifying theory that equivocates to something profound without coherent set of supporting facts 

