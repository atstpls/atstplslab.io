---
title: weaksauce playbook
date: 2023-01-16 16:00:42
tags: [fiat, crisis]
category: [ecosystem, events]
---

<br>

- [That is easy](#that-is-easy)
- [Not fair cheating](#not-fair-cheating)
- [Make excuses](#make-excuses)
- [Interfere with goals](#interfere-with-goals)
- [Call names](#call-names)
- [Push and hit](#push-and-hit)

<br>


Weaksauce               PLAN B 


1. That's easy          Be a
2. Not fair/cheating    Be a solver
3. Make excuses         Be a walker
4. Interfere with       Be a teammate
5. Call names           Be a builder
6. Push/hit             Be a protector



    <img alt=" " width="400" src="../../assets/img/posters/weak1.jpg"></img>
    <img alt=" " width="400" src="../../assets/img/posters/weak2.jpg"></img>
    <img alt=" " width="400" src="../../assets/img/posters/weak3.jpg"></img>
    <img alt=" " width="400" src="../../assets/img/posters/weak4.jpg"></img>
    <img alt=" " width="400" src="../../assets/img/posters/weak5.jpg"></img>
    <img alt=" " width="400" src="../../assets/img/posters/weak6.jpg"></img>




Biblical corpus is a practice of sacrifice devoted to atonement

There is a pattern of sacrifice that emerges as the Biblical Corpus progresses

Pattern of sacrifice culminates in proposition

"Salvation and redemption as such are dependent on the voluntary willingness to confront the worst of tragedies and the deepest of possible acts of malevolence. That that's the universal pathway to salvation and redemption. And that's exemplified as far as I can tell in the Passion story."  -Jordan Peterson









Dry erase board, 

Thoughts, words, actions 

Goggins, Brooks, Leon Edwards, patterns of actions, 
Goggins - overcoming incredible obstacles
Edwards - Losing 4 rounds, all the doubters, pound for pound what 
Brooks - 2 points in 1st half, 24 points in second with dagger 3 against old team

Nancy - Captain of my Soul 

"People that live different lives" not something that people that don't say 



"Listen, Stop feeling sorry for your f'n self"

"Well come on then, what's wrong with you"

"You're 2 down, now you gotta pull the s--- out of the fire"

"Come on Leon man, you got it man come on"

"C'mon Leon. Let's go"

They all doubted me, said I couldn't do it
They all said I couldn't do it
Look at me now--Living it
Pound for pound
Headshot
Dead



Russell's paradox is proof that a logical system can't accurately evaluate itself 

We're trying to explaing the frame we're using to view the world 


M - made up things, saved, 

V - Vague terminology - saved, paid in full, 


O - Overt communication


R - Rituals

S - Stories


Instead of "you didnt do this" say "you would have better success if you did this"

Frame in way that if it works or doesn't it's a win.

Team members should seek to understand other members before calling them out on what they think happened

criticize, empathize, 

Influenced by perception
- sensation. immediate reality using 5 senses
- intuition. grasp complex patterns beyond data from 5 senses



resentful people want others to change

scale back the dragon, get a little gold

Individuals have their metaphysics supplied for them by others, often without them realizing 

People want to signal the right thing instead of determine facts and what is right

Well-thought out actions guided by principles...
- Climb don't jump down in hole
- Inspect pipe from their perspective
- Ask questions to understand their story
- Get them to teach you

Freedom to pursue (intellectual) truth

Generate new knowledge and seek to maximally disseminate it

Quest for truth should always supercede one's ego-defensive desire to be proven right - Gaad Assad

Epistemially humility

"What were once centers of intellectual development have become retreats for the emotionally fragile. The driving motto of university is no longer the pursuit of truth but the coddling of hurt feelings."  - Gaad Saad, The Parasitic Mind

Imagine two versions of truth---one is repeatable and verifiable, one saves innocent lives and eliminates entire category of suffering. Best truth for everyone? For the team?

Wealth

Trust or verify

Plato's Cave page

Quis custodiet ipsos custodes   "Who watches the watchmen?"

Homo homini lupus (man is wolf to man)

Bellum se ipsum alet ( the war that feeds itself )

Molon labe ( come and take it )

Concrete walls on bathrooms - work required to bypass... like Bitcoin

Canonical axioms are deep and profound, others on the fringe are experimental

Axiomatic presuppostions are present in bt

Numological - cross culture, cross species, cross era, cross methodology, cross theoretical frameworks, all triangulate to demonstrate universal truths

Jordan found overlap between mythological analysis with behavioral neuroscience in Maps of Meaning

Hero archetype advances courageously in the face of threat in between you and valid goal. Encounter threat, explore it until you master it. Get virgin and gold.

Rabbit mythology when see predator, freezes.

Matthew 12:11... Luke 6:9....

The psychologically and conceptually painful juxtoposition of two moral stances (keeping of Sabbath vs injunction to do good) is part of the series of events that eventually leads to Christ's arrest and crucifixion

These stories portray the existential dilemma that eternally characterizes human life:  it is necessary to conform, to be disciplined, and to follow the rules--to do humbly w hat others do; but it is also necessary to use judgment, vision, and the truth that guides conscience to tell what is right, when the rules suggest otherwise.  It is the ability to manage this combination that truly characterizes the fully developed personality: the true hero







