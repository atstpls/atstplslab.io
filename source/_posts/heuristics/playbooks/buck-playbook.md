---
title: buck playbook
date: 2023-01-16 16:00:42
tags: [fiat, crisis]
category: [ecosystem, events]
---

<br>

- [Faith](#faith)
- [Pinocchio](#pinocchio)
- [Buck](#buck)

<br>

### Faith 

All humans move forward with faith in something--a set of principles or propositions.  There are two broad categories:

    Faith in the power of lies     - act/speak in whatever way gives you advantage in the moment  
    Faith in the power of truth    - act/speak the truth independent of the short-term consequences for you


One is selfish, short-term thinking, contradicts reality, and becomes more and more difficult to maintain over time.  

One is selfless, long-term thinking, aligns with reality, and becomes more and more dependable and productive over time.


I've found the best way to conceptualize these two strategies for decision-making and observe it taking place in real life is by using two movies:

    - 1940 cartoon Pinocchio with Jordan Peterson's analysis which I tried to demonstrate here          
    - 2020 film The Call of the Wild starring Harrison Ford  


Humans embody the stages of each fictional character depending on their faith in lies or truth:

                PINOCCHIO                                                                                               BUCK 

Actor          - Present fake persona to avoid truth                     Sled Dog           - Prove value of self through actions

Liar            - Present fake ideas to avoid truth                          Lead Dog           - Prove value of team through actions

Victim         - Reject responsibility to avoid truth                       Pack Wolf          - Embrace responsibility of self through actions

Jackass       - Reject reality to avoid truth                                 Lead Wolf          - Embrace responsibility of team through actions

<br>

### Pinocchio 

Pinocchio in the movie faces four temptations:

    The first is to become an actor, using a constructed persona making him appear far more valuable than he is, as a way to solve his life's problems.  
   
    When things go south, he lies several times in an attempt to avoid the consequences of his actions, his nose growing after each one.
   
    He's then offered an opportunity to abandon responsibility and accept a vacation to Pleasure Island as a reward for adopting the guise of a victim, which he takes.
   
    Pinocchio and other kids, isolated from reality and the responsibilities of life, do whatever they want on the island until reality catches up with them and they realize their rejection of reality for the pursuit of impulsive pleasures is the very thing that has made them slaves (jackasses).

<br>

### Buck 

Buck, the dog in Call of the Wild, does things differently:

    He's kidnapped from his comfortable life in a mansion, sold and shipped to the Alaskan frontier, and forced to be a Sled Dog having absolutely no idea what to do or who he can trust.  He chooses to accept his fate, give all of himself for the good of the team, and in the process proves to the other dogs he truly is the leader they all want.  
   
    After the selfish dog Spitz is forced out of the Lead Dog position, the entire sled team refuses to cooperate until Buck is strapped into the lead spot. As Lead Dog he proves his team right by saving the team multiple times and consistently putting the welfare of his team members before himself.  
   
    The sled team eventually breaks up, Buck finds himself in the wilderness and realizes he doesn't have the knowledge or skills required to survive.  He accepts this responsibility and sets his sights on becoming a Pack Wolf, learning from and trying to get to know a pack of wolves living in the area.  

    After he selflessly saves another wolf in the pack from drowning when the other wolves couldn't, the pack accepts him as one of their own and eventually makes him their leader as he continues to demonstrate the growing value of the pack under his leadership--even single-handedly making a Grizzly bear back down from the pack at one point.  


I see people demonstrate the different stages of these characters all the time.  Pinocchio with deception and refusing accountability and Buck with honesty, reciprocity, and responsibility.

You and I are no different.  In our discussion yesterday, Pinocchio and Buck can easily be seen to the careful observer.


And of course, I am teaching Dub to recognize these "Pinocchio plays" and how to handle them.  And also how to strive to live like Buck, accepting that life requires sacrifice, committing yourself to truth, and proving your value through your actions and service to others.

Teams work together to reach a common goal

###### Heuristics

Be more flexible than rigid

Live a life of appreciation rather than a life of expectation 

Build up yourself and others

Forgive yourself and others

Take responsibility for everything in my life

Be the man, request don't command

Speak all five, keep hope alive

Help others succeed

Work hard, learn from others, stay humble

<br>

12 rules for life

1. Stand up straight with your shoulders back
2. Treat yourself like someone you are responsible for helping
3. Make friends with people who want the best for you
4. Compare yourself to who you were yesterday, not to who someone else is today
5. Do not let your children do anything that makes you dislike them
6. Set your house in perfect order before you criticize the world
7. Pursue what is meaningful, not what is expedient
8. Tell the truth, or atleast don't lie
9. Assume that the person you are listening to might know something you don't
10. Be precise in your speech
11. Do not bother children when they are skateboarding
12. Pet a cat when you encounter one on the street, dogs are ok too

<br>

12 more rules for life

1. Do not carelessly denigrate social institutions or creative achievement
2. Imagine who you could be and then aim single-handedly at that
3. Do not hide unwanted things in the fog
4. Notice that opportunity lurks where responsibility has been abdicated
5. Do not do what you hate
6. Abandon ideology
7. Work as hard as you possibly can on one thing and see what happens
8. Try to make one room in your home as beautiful as possible
9. If old memories still upset you, write them down carefully and completely
10. Plan and work diligently to maintain the romance in your relationship
12. Do not allow yourself to become resentful, deceitful, or arrogant
13. Be grateful in spite of your suffering
    