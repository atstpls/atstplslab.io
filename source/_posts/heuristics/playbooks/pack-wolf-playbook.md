---
title: pack wolf playbook
date: 2023-01-16 16:00:42
tags: [fiat, crisis]
category: [ecosystem, events]
---

<br>

|Victim Plays|Pack Wolf Plays|
|-|-|
|[Someone was wronged](#someone-was-wronged)            |[Something is Off](#something-is-off)|
|[Others are the problem](#others-are-the-problem)      |[Split Up the Fault](#split-up-the-fault)|
|[There is nothing I can do](#there-is-nothing-i-can-do)|[Lean On the Wall](#lean-on-the-wall)|
|[I can not explain](#i-can-not-explain)                |[Take the Lead](#take-the-lead)|
|[I can not understand](#i-can-not-understand)          |[Eliminate the Need](#eliminate-the-need)|
|[My truth](#my-truth)                                  |[Accept and Test](#accept-and-test)|
|[I give up](#i-give-up)                                |[Accept and Bless](#accept-and-bless)|

<br>

### Something Is Off

    Victim play #1 frames events in terms of "justice" and not "soundness"

    Establish the flaw in the system

<br>

### Split Up the Fault

    Victim play #2 attributes fault to someone else 

    Distribute the fault across multiple parties, take a small piece of your own  

<br>

### Lean On the Wall

    Victim play #3 claims inability to contribute to the solution    

    Very gradually break down this wall by introducing small things you/we can do

<br>


7 patterns

- Backpedal 
- Jumping
- Sprinting 
- Acceleration 
- Hip Turn 
- Lateral Shuffle
- Lateral Run
