---
title: atlas
date: 2023-01-16 16:00:42
tags: [fiat, crisis]
category: [ecosystem, events]
---

<br>

- [Seek Truth](#seek-truth)                   Use and maintain accurate maps ( **experience-based stories**, **convergent validation** )
- [Own Everything](#own-everything)           Accept responsibility (**subsidary identity**, **embedded in higher good**)
- [Suffer to Grow](#suffer-to-grow)           Make required sacrifices ( **hierarchy**, **hero myth** ) to better serve others

<br>


There is no perception independent of action. There is no action independent of goal-directed motivation. So all perception is associated with motivation. 

Every aim requires sacrifice 

    - reject that which doesn't fit 
    - offer up yourself 


## Victim Playbook 

<br>

- [Someone was wronged](#someone-was-wronged)
- [Others are the problem](#others-are-the-problem)
- [There is nothing I can do](#there-is-nothing-i-can-do)
- [I can not explain](#i-can-not-explain)
- [I can not understand](#i-can-not-understand)
- [My truth](#my-truth)
- [I give up](#i-give-up)

<br>

## Pinocchio Playbook 





## Buck Playbook 







Biblical corpus is a practice of sacrifice devoted to atonement

There is a pattern of sacrifice that emerges as the Biblical Corpus progresses

Pattern of sacrifice culminates in proposition

"Salvation and redemption as such are dependent on the voluntary willingness to confront the worst of tragedies and the deepest of possible acts of malevolence. That that's the universal pathway to salvation and redemption. And that's exemplified as far as I can tell in the Passion story."  -Jordan Peterson









Dry erase board, 

Thoughts, words, actions 

Goggins, Brooks, Leon Edwards, patterns of actions, 
Goggins - overcoming incredible obstacles
Edwards - Losing 4 rounds, all the doubters, pound for pound what 
Brooks - 2 points in 1st half, 24 points in second with dagger 3 against old team

Nancy - Captain of my Soul 

"People that live different lives" not something that people that don't say 






Russell's paradox is proof that a logical system can't accurately evaluate itself 

We're trying to explaing the frame we're using to view the world 


M - made up things, saved, 

V - Vague terminology - saved, paid in full, 

O - Overt communication

R - Rituals

S - Stories


Instead of "you didnt do this" say "you would have better success if you did this"

Frame in way that if it works or doesn't it's a win.

Team members should seek to understand other members before calling them out on what they think happened

criticize, empathize, 

Influenced by perception
- sensation. immediate reality using 5 senses
- intuition. grasp complex patterns beyond data from 5 senses



resentful people want others to change

scale back the dragon, get a little gold

Individuals have their metaphysics supplied for them by others, often without them realizing 

People want to signal the right thing instead of determine facts and what is right

Well-thought out actions guided by principles...
- Climb don't jump down in hole
- Inspect pipe from their perspective
- Ask questions to understand their story
- Get them to teach you

Freedom to pursue (intellectual) truth

Generate new knowledge and seek to maximally disseminate it

Quest for truth should always supercede one's ego-defensive desire to be proven right - Gaad Assad

Epistemially humility

"What were once centers of intellectual development have become retreats for the emotionally fragile. The driving motto of university is no longer the pursuit of truth but the coddling of hurt feelings."  - Gaad Saad, The Parasitic Mind

Imagine two versions of truth---one is repeatable and verifiable, one saves innocent lives and eliminates entire category of suffering. Best truth for everyone? For the team?

Wealth

Trust or verify

Plato's Cave page

Quis custodiet ipsos custodes   "Who watches the watchmen?"

Homo homini lupus (man is wolf to man)

Bellum se ipsum alet ( the war that feeds itself )

Molon labe ( come and take it )

Concrete walls on bathrooms - work required to bypass... like Bitcoin

Canonical axioms are deep and profound, others on the fringe are experimental

Axiomatic presuppostions are present in bt

Numological - cross culture, cross species, cross era, cross methodology, cross theoretical frameworks, all triangulate to demonstrate universal truths

Jordan found overlap between mythological analysis with behavioral neuroscience in Maps of Meaning

Hero archetype advances courageously in the face of threat in between you and valid goal. Encounter threat, explore it until you master it. Get virgin and gold.

Rabbit mythology when see predator, freezes.

Matthew 12:11... Luke 6:9....

The psychologically and conceptually painful juxtoposition of two moral stances (keeping of Sabbath vs injunction to do good) is part of the series of events that eventually leads to Christ's arrest and crucifixion

These stories portray the existential dilemma that eternally characterizes human life:  it is necessary to conform, to be disciplined, and to follow the rules--to do humbly w hat others do; but it is also necessary to use judgment, vision, and the truth that guides conscience to tell what is right, when the rules suggest otherwise.  It is the ability to manage this combination that truly characterizes the fully developed personality: the true hero







