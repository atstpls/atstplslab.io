---
title: victim playbook
date: 2023-01-16 16:00:42
tags: [fiat, crisis]
category: [ecosystem, events]
---

<br>

- [Someone was wronged](#someone-was-wronged)
- [Others are the problem](#others-are-the-problem)
- [There is nothing I can do](#there-is-nothing-i-can-do)
- [I can not explain](#i-can-not-explain)
- [I can not understand](#i-can-not-understand)
- [My truth](#my-truth)
- [I give up](#i-give-up)

<br>

### Someone was wronged

   ( moral virtue instead of truth )

    The first play in the Victim's Playbook is to frame events in terms of "justice" and not "soundness"

    Root cause analysis is then incorrectly reduced to a simple declaration of a moral wrong

<br>

### Others are the problem

    The supposed problem--incorrectly understood as a moral wrong--is attributed to someone else 

    The proposed solution--incorrectly understood as expected behavior--is demanded from someone else 

<br>

### There's nothing I can do

    This play attempts to establish that the victim is unable to contribute in any way to the solution    

    It insinuates that if they could, they would... but unfortunately it is an impossibility

<br>

### I can not explain

    This play is used in an attempt to supplement or defend one or more of the first three

    Since 1-3 are built atop a flawed framework, genuine solution-seeking discussions inevitably decompose

    While this should be cause for retrospection, the victim's stance becomes "others don't understand"

<br>

### I do not understand

    This play is used in an attempt to supplement or defend one or more of the first three

    Confessing one's cognitive limitations during the problem-solving process is admirable
    
    Except when it is intentionally deployed to stall or escape discussions with actual problem-solvers 

<br>

### My truth

    This play is used in an attempt to supplement or defend one or more of the first three 
    
    Rejecting reality and accepting an artificial one eliminates the need to explain or understand 

    The victim is able to maintain beliefs that contradict reality in the face of emprical and logical arguments  

<br>

### I give up

    Sometimes spoken, sometimes not, this play signals the desire to end reciprocal problem-solving

    This move undermines--even contradicts--the initial importance assigned to both the problem and the proposed solutions

    But by only giving up on a single instance of problem-solving, the victim can still maintain a general sense of duty and moral virtue
        
<br>

