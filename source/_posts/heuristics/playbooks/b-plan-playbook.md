---
title: b plan playbook
date: 2023-01-16 16:00:42
tags: [fiat, crisis]
category: [ecosystem, events]
---

<br>

- [Be a doer](#be-a-doer)
- [Be a solver](#be-a-solver)
- [Be a walker](#be-a-walker)
- [Be a teammate](#be-a-teammate)
- [Be a builder](#be-a-builder)
- [Be a protector](#be-a-protector)

<br>


