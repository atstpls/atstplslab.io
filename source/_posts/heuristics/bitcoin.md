---
title: bitcoin
description: An ethical commodity and store of value
tags: [resiliency, bitcoin]
category: [ecosystem, resources]
---

<br>
  
<b>Bitcoin</b> is a digital currency that uses <b>encryption instead of banks</b> to verify transactions.

<br>

##### BITCOIN

A blockchain is a P2P network of nodes where each node contains a copy of code and data.

Bitcoin's blockchain is a decentralized ledger that tracks accounts, balances, and transactions. All computers running the app (nodes) keep a copy of the ledger.  Every transaction is sent to all the other nodes and added to the blockchain.

To keep a node from adding a fraudulent transaction, a complex math problem must be solved by a node before they can add a block into the ledger.  This requires significant computing power and the first node to do it gets a reward and is allowed to write the block into the ledger. (Proof of work)

<br>

##### MECHANICS

- Cannot increase supply
- Cannot be kept from moving
- Cannot be seized (true custody when done right)
- Universal market
- Can be easily and instantly converted to other assets
- Long duration safehaven store of value
- In monetary energy, more successful than Google, Apple, Amazon
- Decentralization combined with cryptography makes it most secure network in world
- With Bitcoin, all accounts, balances, and transactions are tracked on a public ledger. There is no third-party intermediary. There are no admins, only users.
- Decentralized currency is the best way to move value into the future
- Bitcoin is globally accessible, politically neutral, and free for anyone to use
- It is not inflationary, provides final settlement in minutes, and doesn't require trusting any authority
- It can economically empower everyone in the world
- Because a single entity can't possibly make the best decisions for billions of people and their specific situations. Only decentralized system can do this
- A currency that holds its value
- A trusted method for long-term
- Bitcoin is an apex property, most robust, resilient thing we've built
- digital property has most options for custody
- 12 word phrase is highest form of property right... can be extracted but more difficult than anything else currently available 
- it's easier to confiscate things in physical world than digital world
- does not incentivize violence.  Only kind of property that you can take anywhere and to the grave
- uncorruptable software running a bank 
- impervious to cyber attacks, eliminates 

<br>

### PROS AND CONS

|USD|BTC|
|-|-|
|Requires trusting a third party|Does not rely on trusting a third party|
|Can be legally or illegally confiscated, stolen, or destroyed|Has apex property rights preventing seizure and theft|
|Elasticity encourages borrowing, overspending, corruption|Fixed supply encourages saving and fiscal responsibility|
|Takes days sometimes weeks for final settlement|Provides final settlement in minutes|
|Supply is used to control prices of real goods and services|Allows free market to set prices of real goods and services|
|Saving requires taking on risk, diversifying, beating inflation|Makes saving for the future simple... buy and hold|
|Allows GOVT to spend without real cost-benefit analysis|Fixed supply encourages true cost-benefit analysis|
|Allows GOVT to bail out corporations without citizen approval|Would require citizen approval for bailouts|
|Tightly restricted and cannot be accessed anytime |Globally accessible and continuously available|
|Controlled by politicians and one of many fiat currencies|Politically neutral and a free universal market|
|Does not have nor want inherent transparency|Fully auditable with every transaction visible to all|
|Conversion requires third parties, fees, or both|Easily converted to other assets in seconds|
|Completely centralized and inherently corruptible|Uses decentralization and cryptography to be incorruptible|
|Allows GOVT to finance long wars without going broke |Fixed supply discourages long, costly wars|
|Empowers the GOVT who controls it and corporations|Can economically empower everyone in the world|

<br>

##### ANALYSIS

Bitcoin is the hardest money ever invented. It is not inflationary, is controlled by no one, and its combination of speed and security in transferring value over time and space is unrivaled.

Decentralized systems such as Bitcoin incentivize their members to maintain integrity. Those who follow and help enforce the rules are rewarded.All accounts, balances, and transactions are tracked on a public ledger. It is not inflationary, provides final settlement in minutes, and doesn't require a third-party intermediary.It has the strongest property rights and the most optionality of any known asset. And it can allow anyone in the world to effectively store wealth.


- The first law of thermodynamics essentially states that energy cannot be created or destroyed, only transformed.
- Bitcoin mining, the process of creating new bitcoin and validating transactions, aligns with this principle.
- Miners use computational power, largely derived from electrical energy, to solve complex mathematical puzzles.
- In successfully solving these puzzles, this expended energy is not lost but transformed into a new form #bitcoin which @saylor  has often called "digital energy".
- This transformation of energy gives bitcoin a tangible value in the real world, mirroring the way precious metals like gold derive their value from the energy expended in their extraction.
- Gold would not have much value if it was easy to find and extract 'Au' from the ore form.
- The second law of thermodynamics introduces the concept of entropy, a measure of disorder in a system. 
- It states that the total entropy of any system absent external energy expended to bring order can only increase over time.
- Energy expended to mine bitcoin, with its fixed supply of 21M coins, is a resistance against the monetary entropy of fiat money.
- Fiat money can be modeled as "zero mass" since no energy was expended in its creation (technically, military expends energy to defend GRC status).
- Further linking Bitcoin to thermodynamics, the energy expended in #bitcoin mining also contributes to the network's security. 
- The energy requirement creates a 'thermodynamic wall' that is hard to breach. Again, this is explained simply by @saylor as "digital energy".
- For an attacker to alter transactions within the bitcoin network, they would need to expend a comparable amount of energy to the honest miners, which, given the global scale of bitcoin mining, is incredibly costly and unfeasible.
- Sound money is a concept where a monetary system has a stable value and is immune to excessive inflation. Bitcoin's energy-intensive creation process and fixed supply underline its qualifications as sound money.
- Unlike fiat currencies that central banks can inflate by printing more bank reserves, #bitcoin is resistant to supply inflation.
- Bitcoin bear case: The decentralization, scarcity and transparency of bitcoin makes it an ideal candidate for the role of digital gold.
- The principles of thermodynamics play a central role in Bitcoin's design and function. I wonder if Satoshi thought through this all. I'd bet yes.
- Bitcoin embodies the transformation and conservation of energy resisting the monetary entropy of fiat money regimes



|myth|truth|
|-|-|
|Bitcoin has no intrinsic value|The value of Bitcoin is:<br>- transfer value without government control/intervention<br>- have ability to verify transactions on a public ledger<br>- own an asset that can't be corrupted<br>- own an asset that can't be confiscated<br>- own an asset that becomes more scarce with time<br>- own an asset that's easily distributed and moved<br>- own an asset that is globally accessible and free for anyone to use<br>- own an asset that is politically neutral<br>- own asset that provides final settlement in minutes<br>- own asset that is resistant to inflation<br>- own asset that doesn't require trusting any authority<br>- asset that can economically empower everyone in the world<br>- own asset that has most options for custody<br>- own asset with highest form of property right|
|Bitcoin only has a price, and isn't a store of value||
|Bitcoin value comes from faith and confidence||
|Bitcoin is speculative||
|Bitcoin too volatile to be unit of account||
|Bitcoin too expensive to be a medium of exchange||
|Crypto backed by gold will work|- Requires trusting a third party<br>- Requires tokenizing something we don't know the total volume of<br>- Doesn't meet requirements of true ownership<br>|
|Money must start out as a commodity|- requires work to obtain<br>- Only kind of property that you can take anywhere and to the grave<br>- more robust defense built-in than any other money<br>|


Decentralized systems such as Bitcoin incentivize their members to maintain integrity. Those who follow and help enforce the rules are rewarded.

All accounts, balances, and transactions are tracked on a public ledger. It is not inflationary, provides final settlement in minutes, and doesn't require a third-party intermediary.

It has the strongest property rights and the most optionality of any known asset. And it can allow anyone in the world to effectively store wealth.


