---
title: completion
date: 2023-07-17 22:00:42
tags: [money]
category: [ecosystem, resources]
---

<br>

Aligning behavior with values and beliefs

Only ACTION drives change, not words

There is no teacher no pupil 

You yourself are the teacher and pupil

Complex organisms developed a complete internal model of the world, how it works, actions and results
    Internal model is constantly compared to what is occurring
    When things go according to plan, positive affect rules, ensuring that current goal-directed conceptions and actions dominate
    When something unexpected occurs, orienting reflex manifests itself
    
Orienting Reflex is a sequence of rapid preparatory responses
    Current goal-directed actions cease when mismatch between desire and world emerges, detected by the septalhippocampal comparator systems. 
    The autonomic nervous system is engaged.
    Heart rate rises, in preparation for non-specific action, and cortisol floods the bloodstream
    Startle responses, primitive but fast, governed by brainstem circuitry, produce virtually instantaneous physiological defensive postures, designed to protect the body, particularly the head and neck

This is followed by activation of circuits in the superior colliculus, which direct the sensory systems of the head
towards the environmental locale that quick-and-dirty systems have specified as the source of the
anomaly

Hypothalamic systems, ready fight or flight in concert with the pain-sensitive systems of the periaqueductal grey
    Finally, the extended amygdala (the bed nucleus of the stria terminalis) enhances vigilance, and provisionally
associates the anomalous occurrence with anomalies that in the past have produced negative
outcomes (Hooker, Germine, Knight & D’Esposito, 2006). 

Orienting only occurs toward "differences that make a difference"
    anomalies that interfere actively with current goaldirected activity
    not to all stimulus change
    Consciousness attends selectively, to the minimum set of elements necessary to bring about the desired transformation 
    Motivational systems that fundamentally give rise to MAP schema.

<br>

#### MISMATCHES

Mismatch is much more than the problem of erroneous action, but
    If the desired future fails to appear, it is not only current actions that might be wrong
    current desire might be wrong, as well.
    Perhaps the erroneous MAP schema is based in jealousy, for example, and the situation is suchthat jealousy merely makes things worse.

It is not reality that is compared with expectation (now: desire). 
    We are not privy to reality, even in the present. Current "actuality" is modeled, much as future "possibility."

    Sometimes you cannot get from point "a" to point "b," because you are not actually at point "a."
    We compare a motivated model of the present to a motivated model of the future. 
    In the case of error, this means that the very way we perceive things, past, present and future, might be incorrect. 

Failure re-presents us with the frame problem. 
    When what is desired does not manifest itself, motivation and perception, as well as cognition
and action, might all be incorrect – anywhere in their structure.

Implications of hierarchical MAP-schema structure
    In the absence of such nesting, it would be impossible to disinhibit motivation and emotion at different
levels of intensity, when anomalies of different significance emerge. 
    All errors would be equally overwhelming or irrelevant. 

    varying errors indeed produce various reactions. 
    Each mistake cannot be evaluated, cognitively, however. 
    There is insufficient time for that. 
    Instead, potential meaning is bounded, a priori, by the breadth/import of the current MAP schema. 

Development is simultaneous higher-order organization of intrapsychic and social MAP schemas. 
Affective stability, particularly at higherorder levels, is dependent upon the match between them (to say it again). 

Imagine an inverted neural hierarchy, representing MAP schema import
    mismatch disruption of schemas closer to the point of the V are more upsetting
    The meaning of a high-resolution schema depends on its role as a sub-element of a lower-resolution schema
    grades in a pre-med class only matter in the broader context of wanting to be a physician.

The objects specified by a given MAP schema are positively valenced
    the 1st dimension of emotion – if their appearance indicates (1) that progress is occurring, and (2) that
the structural integrity of the currently operative schema is valid and intact. 
    A working schema is therefore self-verifying, as well as providing direct, dopaminergically-mediated incentive reward
    Obstacles, by contrast, are negatively valenced (the 2nd basic emotional dimension). 
    Their appearance indicates a schema-world mismatch, danger to current progress, and the fact that the current MAP schema (or hierarchy) may not be functional. 

If an obstacle does appear, it should first be evaluated for significance at narrowest and most specific level
possible. 
    Such use of Occam’s razor limits the spread of chaotic emotion. 
    Elements of self differ in degree, not in kind: 
    the upheaval produced by an obstacle is proportionate to the area of space and time structured by the erroneous schema. 
    The solution may lie close to hand, if the obstacle is merely something expected under different circumstances. 
    Other times, however, the obstacle is too radically unknown for such easy dealings. 
    Then the complexity of things re-emerges, with incomputable consequences. 
    The borders between things become questionable, and everything is up for grabs. 
    This is the problem of chaos, vs. order – the eternal problem, and the ultimate reality of the world.

We derive one important form of meaning –security and hope – from the match between our personal MAP schemas and the social world. 
Such ordered meaning emerges as a consequence of the delimitation of its paired opposite, chaos, whose manifestation produces the
second kind of meaning. 

Maintenance of MAP-schema meaning keeps chaos in check, rather than revealing it
    Determinate positive and negative events occur, as the world manifests itself as tool and obstacle. 
    Irrelevant things occur, too, of course – but are in some important sense never realized. 
    No one can pay attention to all activity; only to all relevant activity. 
    But what of seriously anomalous events?
    Some occurrences are neither evidently good nor bad, nor immediately eradicable as meaningless. 
    They are not understood, not explored. 
    They cannot be placed into the context of the current MAP schema, nor encapsulated within that schema’s hierarchically-ordered largerscale conceptual surroundings. 
    They violate the frame, interfering with its operation, its integrity, and its relationship to other frames. What must happen in such cases?

What is not comprehended but is still extant must logically be experienced as paradoxical: negative, in potential, positive, in potential, irrelevant, in potential – and self and world in potential, as well. 
    That potential, the true complexity of the world, is chaos. 
    Its manifestation, no mere threat, constitutes a challenge to the full adaptive capacity of the individual. 
    The emergence of chaos produces more than mere anxiety; something more like generalized MAP schema disinhibition and competition, as new
and potentially appropriate means of framing war with each other for dominance. 

Motivation for maintaining meaning is thus not merely desire to reduce anxiety: 
    it is instead desire to avoid the internal and, frequently, external war of competing options – and there is something even deeper
about the anomalous event. 
    At some point in psychological development, however hypothetical, all events are anomalous, though they may be rapidly constrained by the social surround. 
    This means that the schemas allowing for the determinate utilization of objects, situations and abstractions are dependent for their construction, initially, on information extracted from the overarching, ever-emerging domain of the unknown. It is for such reasons that chaos is
meaningful, a priori, and the mother of determinate being itself.

The appearance of the anomalous involuntarily produces its own specific MAP schema,
the orienting reflex, or complex, in more modern terms (Halgren, 1992; Ohman, 1979, 1987).
    The beginning point of that schema is the insufficiency of present knowledge. \
    The desired endpoint is classification of the anomalous phenomenon, and its reduction to specified meaning.
    Increased sensory processing and exploratory activity is brought to bear on the uncomprehended
circumstance, examined from the perspective of varying MAP schemas: Is it relevant to another
motivational state? Can it serve as an affordance or obstacle, and at what level? It is like other
irrelevant "objects," and treatable as ground? Such effortful exploration constitutes 
        (1) the process by which identity originally comes to be; 
        (2) the elimination of possibility from the indeterminate domain of the anomalous to the finite domain of a determinate
MAP schema; and 
        (3) the reworking of identity, which is the sum total of all such schema. 
        
    It is here where Swanson’s work on hypothalamic function once again becomes relevant. 
    The hypothalamic "rostral behaviour control segment" establishes narrow, biologically relevant MAP schema, ingestive, reproductive and defensive. The caudal segment, by contrast, is the origin point of the ventral tegmental dopaminergic system, which governs approach and exploratory
behaviour, and whose activation is experienced as incentive reward. 
Thus, the hypothalamus has a powerful, primordial backup system, which grips control, when its more specific rostral
systems fail in their efforts. Exploration in the face of the unknown is thus as ancient as hunger,
thirst, sex and aggression. It is a primary "drive," manifesting itself in the form of the orienting
complex, under the control of the septal-hippocampal and anterior cingulate CNS systems. 

In a nonverbal animal, such as a rat, the transition from frozen anxiety to active
exploration and mapping begins with cautious sniffing, under the spell of brain systems that
minimize exposure to predators. The animal soon switches to vision, using appropriate head
movements; then begins to move, assessing territorial layout and significance as something
occurring in response to its own actions (Blanchard et al., 1991). For an isolated rat in a cage,
"territory" is something as simple as spatial layout – hence the cognitive map or spatial models of
hippocampus function (O’Keefe & Nadel, 1978), buttressed by findings of hippocampal "place
cells" (O’Keefe & Dostrovsky, 1971). Other researchers, however, note hippocampal enabling of
"transitive associations" (Bunsey & Eichenbaum, 1996) – relations between arbitrary stimuli
(Howard, Fotedar, Datey, & Hasselmo, 2005) – and suggest that place cell function is broader
identification of context. Context can also mean "behavioral task demand" or meaning (Smith &
Mizumori, 2006). Representation of such context may well be equivalent to episodic memory,
another hippocampal function (Milner, 1972).
Investigators analyzing "cognitive maps" study the behaviour of isolated animals.
However, many animals are social, Their primary "environment" is, therefore, the dominance
hierarchy they occupy locally. Primates, like rats, develop detailed maps of their social structures,
as they transform across generations and decades. The "place" mapped by the "cognitive map" is
thus a social structure, not just a geographical locale. This map is precisely the MAP schema
hierarchy, grounded in motivation, expanded through individual socialization into complex
human culture. Proper understanding of hippocampal function therefore appears dependent on
the assessment of certain features of territories currently given no consideration.
13 Territories are
not places of relatively predictable objects and their interactions, but complex and dynamic social

dramas, whose behaviorally-associated contextual meanings depend upon on the reactions of
potentially unpredictable conspecifics. Most animals solve this problem by consorting only with
familiar peers, whose behaviours have already been mapped, and which are additionally
constrained by their particular positions in the MAP schema hierarchy. The cortex can predict the
outcomes of interactions with such conspecifics, and work so they remain positive. These
predictions/desires generally match information about a known conspecific’s behavior, as it
occurs, and is fed, bottom up, into the hippocampus, through the brain stem systems. The
hippocampus registers "match," and the arousal systems (anxiety, aggression, panic, exploration,
etc.) remain tonically inhibited. No threat is detected. No possibility for damage manifests itself.
No disinhibition of motivation and emotion is necessary. No increase in allostatic load (McEwen,
2000), with its stress-induced physiological perturbation and damage, occurs.

The importance of the MAP schema hierarchy, the utility of conceptualizing it as the
structure within which experience manifests itself, and its simultaneous intrapsychic and social
existence, may be additionally illustrated by the fact that social status transformation produces
functional change in the most basic, serotonergic, neurotransmitter system.14 High status elevates
serotonergic tone, decreasing negative and increasing positive emotion. If your personal schemas
come first, in the social group, your "environment" is stable, productive, and safe, and you are
confident, upright, positive and emotionally stable. If your schemas come last, however,
everything is negative and dangerous, you are confused, anxious, and depressed, hovering close
to the edge of chaos and disintegration. It is for such reasons that hierarchy maintenance and
protection is so important, to animal and human alike, and that position within that hierarchy is
vital (see Wilkinson & Pickett, 2009). Meaning-system disruption affects serotonergic function (a
broader category, by far, than mere regulation of anxiety).

" It is probable that the ultimate assumptions of the MAP
schema hierarchy, derived from exploration, fixed through repetition, are precisely those
governing the rules of social interaction, encoded at the highest level in our explicit conceptions
of natural rights (Peterson, 2006). It is these universal "rules," after all, that best specify the
nature of peaceful, productive shared territory. Disruption of these most fundamental
presumptions – the active breaking of rules, or the verbal justification for such breaking – thus
presents a threat to the structure that inhibits hippocampal disinhibition of chaotic motivational
and emotional responses, corresponding in intensity to the hierarchical import of the MAP
schema level such disruption affects. Thus, it is human societies with the largest differences in
opinion with regards to "intrinsic human right" that possess the most capability for mutual
disruption of presumption, and its attendant chaotic psychophysiological and social
dysregulation.

The Balance between Order and Chaos: Meaning in its Redemptive Form
We have now considered two forms of meaning in detail: that of delimited, pragmatic
order, dependent on the match between the intrapsychic and social MAP schemas; and that of
chaos, the sum total of all meanings that all phenomena possess, in all the arrays they might
occupy. Order structures such chaotic meaning, letting it shine forth in measured doses. When
anomaly occurs, by contrast, chaos shines through of its own accord, with sometimes
revolutionary and devastating results, and forces the alteration of the structures that delimit and
constrain what would otherwise be the overwhelming significance of being.
Many approaches to the maintenance of meaning, including those focusing on terror
management (Greenberg, Solomon, Pyszczynski, et al., 1992), consider individual belief the
primary source of meaning, and the purpose of such belief the restriction of anxiety and fear.

Within such conceptualizations, following Becker (1973) human life is a futile battle: death is the
ultimate reality; all meaning systems serve to shield their adherents from that fact. Thus, the
maintenance of meaning requires rigid allegiance to a structured system, and morality is merely
the conventionality and cowardice described by Nietzsche. The fundamental problem of life,
however, is not the terror of death, although that is an important sub-problem. The fundamental
problem of life is the overwhelming complexity of being. Animals, like human beings, have to
deal with complexity, although they do not necessarily have to deal with the terrors of mortality,
at least in their self-conscious forms. They have, however, evolved means of dealing with chaotic
complexity, as embodied in their psychophysiological structures. The same is true of human
beings, although we have taken the elaboration of the psychological means of dealing with chaos
to unprecedented levels of abstraction (and are uniquely aware of our own mortal limitations). In
doing so, we have increasingly come to pursue a third class of meaning.
A human being comes into the world with a set of evolutionarily determined tools, some
in the form of the very MAP schemas we have discussed. These general-purpose tools help
individuals deal with the constant problems of existence, such as hunger and thirst. The problem
of the complexity of being is, however, equally constant, or even more so. Thus, very
sophisticated means of dealing with that problem have also evolved. The innovation of social
being itself is one such solution. Individuals group themselves into social dominance hierarchies,
find their position within the phalanx, and employ the resources of the entire group against the
challenges of nature and the unknown. To do so, they rearrange their internal natures, so that they
can exist in productive harmony within their group. This grouping requires conflict, war, within
or between individuals – and then its resolution. As a child, matures, for example, he has to
temper his passions so that they reflect his desires, and the desires of the group. Successful
negotiation of this conflict of interests is no simple matter of subjugation, either, no mere
dominance of the super-ego. The group wants the individual to manifest the possibilities of his
being in the manner most beneficial across different spans of time and place and to the smallest
and largest number of individuals, simultaneously. The group thus offers the individual the
opportunity to extend his powers, as well as forcing their limitation. In what manner must an
individual manifest himself, therefore, in order to address all of these intrapsychic and social
demands? The answer can be found in a more elaborated analysis of exploratory behaviour and
the communication and integration of its consequences.
Consider the game, once again – and then, the game of games. The best player is not
necessarily he or she who wins a given game, or even a sequence of games. The best player is he
who plays such that the game continues, and expands, so that he and others have the greatest
chance to play and to excel. When a child is told to be a good sport, this is how he is instructed to
behave. The precise rules comprising the meta-game, "be a good sport," may yet be implicit, in
large part, too complex to be fully formulated. This does not mean they do not exist. We dream
continuously of the individual who will manifest that pattern most successfully, and search for
him – or her – everywhere. What is the best way to successfully play the largest number of
games? The answer is not simply computable. Over time different modes of playing emerge, in
the attempt to seek the solution. Each individual wants to be maximally valued. Pure aggression
is one possible solution. The physically dominant individual can force others to value him as a
player. Sufficient display of negative emotion can have the same effect: someone may be invited
on multiple occasions into different games by appealing to the sympathy of the other players.
These are not optimal solutions, however. Even among chimps, rule of the merely strong is
unstable (De Waal, 1989b). Rule of the weak, likewise, breeds resentment: social animals want
, and will not give continually. Such behaviour is too costly and easily manipulated.
Multiple modes of potential playing compete for predominance during childhood. Such
competition, and cooperation, extends in a more sophisticated manner, across adult being. What
is the victor among those multiple modes, across many individuals? 

Extend that question further: What is the victorious mode of play across many
17
individuals, across many groups, over historically-significant epochs of time? Consideration of
the ancient Mesopotamian myth, the Enuma elish – one of many stories of its type (Peterson,
1999), helps answer that question. Two deities exist at the beginning of time, according to the
Enuma elish. Tiamat is the goddess of chaos, as chaos is the mother of all things. She is reptilian
in nature, logically enough, as the reptile has constantly threatened our lives and our societies,
while increasing our vision, for tens of millions of years (Isbell, 2009). Apsu, her husband, is the
god of order, the foundation of being. The pair nestles together, in the deep, just like the two
halves of the famous Taoist symbol. Their sexual, creative union gives rise to the elder gods, the
primary motivational states. Their dysregulated and careless behaviour results in the death of
Apsu, order, and the vengeful re-emergence of his bride. Hastily organizing themselves, in the
face of this threat, the elder gods elect Marduk, god of exploration, vision and speech, as King,
top of the sacred dominance hierarchy, and send him out to voluntarily confront Chaos, in the
guise of his great-grandmother. Emerging victorious, Marduk cuts Tiamat into pieces, and makes
the world (Peterson, 1999). This is the oldest and most fundamental story that mankind possesses.
It echoes through ancient Egypt, and that state’s conceptions of Horus, the redemptive, attentive
eye; Isis, the goddess of chaos; and Osiris, the god of the state. It serves as the source for the
creation story in the Hebrew bible, and profoundly influences Christianity; it is the story of St.
George, and of Christ, the perfect man, the second Adam, and the deadliest enemy of death, and
the eternal serpent (Peterson, 1999). Its existence and meaning should not be overlooked by
psychologists, increasingly cognizant of the evolutionary shaping of being.
It is time to understand these stories, instead of considering them the superstitious
enemy of science. The great myths of mankind are not theories of objective existence. They are,
instead, imaginative roadmaps to being. They have emerged, painstakingly, piecemeal, as a
consequence of our continual close self-observation, our developing understanding of the
patterns of action that are essentially adaptive, and their representation in symbolic, narrative and
dramatic form, during the transition from implicit behavioural pattern to explicit communicable
form. We tell stories about how to play: not about how to play the game, but about how to play
the metagame, the game of games. When chaos threatens, confront it, as quickly as possible, eyes
open, voluntarily. Activate the neural circuitry underlying active exploration, inhibiting
confusion, fear and the generation of damaging stress responses, and not the circuitry of freezing
and escape. Cut the unknown into pieces; take it apart with hands, thumbs and mind, and
formulate, or reformulate, the world. Free the valuable gold from the dragon of chaos, transform
leaden inertia into gilded action, enhance your status, and gain the virgin maiden – just like the
first of your tree-dwelling ancestors (Isbell, 2009) who struck a predatory snake with a stick,
chased it away, and earned the eternal gratitude of mistress, mother and group.
The third form of meaning has little to do with group identity, except insofar as that
serves as a precursor to its formation. It is instead the story of mankind, and the meaning to be
experienced when that heroic story is imitated, understood, and embodied. Under the loving
tutelage of the ever-virgin mother, guided by the wisdom of his forefathers, the alwaysthreatened nascent hero masters known territory, and becomes keenly aware of its limitations and
errors. He sees the danger threatening, before anyone else, because he is willing to see it, while
others turn away their eyes: The patriarchal structure has become too rigid and self-serving. The
widows and children are being ignored, and God’s wrath, in the form of a watery chaos,
threatens. Public morality has become too chaotic, and it is time for a return to the individual and
collective values that have always ensured the survival of mankind. The hero sacrifices himself to
God, offering up his own petty interests to the greatest possible good, and confronts the too-rigid
social structure or the looming chaos, with nothing but his own courage and truth. It is very easy
to be cynical about such things, but we have many modern examples to consider. Gandhi stopped
the British Empire dead in its tracks, following Tolstoy, whose morality was directly informed by
Orthodox Christianity. Tough-minded observers have noted that Gandhi’s strategies would not
have worked against Stalin, or Hitler, who would have just had him executed. Nonetheless, single
individuals brought down tyrannies of such magnitude in the 20th century, as well. Solzhenitsyn’s

(1975) Gulag Archipelago, an amazing example of individual courage – of individual use of the
Word – demolished the intellectual and moral credibility of communism, forever. Vaclav Havel
performed a similar role in Czechoslovakia. It is not for nothing that we consider the individual
of the highest value in the West.
The third form of meaning is not to be found in slavish allegiance to a system of beliefs,
nor to specific position in a given dominance hierarchy, nor to incautious and wanton exposure to
chaos. It is to be found on the border of chaos and order, Yin and Yang, as the Taoists have
always insisted. It is to be found in the voluntary pursuit of interest, that subtle prodding by the
orienting complex, which turns our heads involuntary towards the most informative places in our
experiential fields, and lets us see the glimmers of redemptive chaos shining through the
damaged structure of our current schemas. That glimmer is the star that has always guided us, the
star that signifies the birth of the hero, and, when followed, is the guardian angel who ensures
that the path we trod is meaningful enough so that we can bear the burden of mortal limitation
without resentment, arrogance, corruption and malevolence. Life is not the constant shrinking
away from the terror of death, hiding behind an easily pierced curtain of beliefs. Life is the
forthright challenging of the insufficiencies that confront us, and the powerful, life-affirming
existential meaning that such pursuit instinctively produces. It is that which keeps the spectre of
mortality at bay, while we work diligently, creatively, at work whose meaning is so powerful and
self-evident that the burden of existence seems well worth bearing. Terror management, be
damned! The path of the eternal hero beckons, and it is the doomed and dangerous fool who turns
his back on it. 



Anomalous things get our attention, predators, reflexes, 

Orienting reflex to the unknown, mix of curiousity, threat, fear, incentive, negative emotion, positive emotion, dose dependent

interplay between positive/negative emotion to the anomaly

meaning is a manifestation of the complex orienting reflex... not just order, not just chaos

We're order, confronting chaos to keep the order continually updated... 

Meaning is the most fundamental instinct and long-term guide for adaptation, it's also the antecdote to despair

imitate hero (Jesus Christ), antecdote to the suffering of life, 

We watch ourselves in the past and extract what is of value using stories , we are mapmakers


Analysis of our own behavior, we know the difference between pathway of good and pathway of evil and we believe above all in the existence of both 

Insistence of God on the goodness of creation reflected fact that truth, courage, and love were united in His creative action 

Thus, ethical claim embedded in Genesis account of creation - everything emerging from realm of possibility in the act of creation is good insofar as that the motive for its creation is good

To believe this, to act it out, is the fundamental act of faith 

Christ on potential for completing your life, reclaiming what you've lost, discovering what you did not know was there.

"And I say unto you, Ask and it shall be given you, Seek and ye shall find, Knock and it shall be opened unto you.   

For everyone that asketh receiveth, and he that seeketh findeth, and to him that knocketh it shall be opened."

Luke 11:9-13

Truly asking, being willing to let go of anything and everything that is not in keeping with the desire.  Otherwise there is no asking 

To ask, seek, and knock is to do everything required to gather what has been left unfinished, and to complete it now 

And to ask, seek, and knock is to determine what is to be asked for, and that has to be something that is worthy of God 

You know that when something does not go well, you should resolve it, analyze it, apologize, repent, and transform

One unsolved problem breeds the necessity for more, one act of deception, one devasted relationship, 

Refusal or inability to come to terms with past, expands the source of the error, expands the unknown, transforms the unknown into something predatory

You get weaker, you're less than you could be because you did not change, 

More likely to make same error, problem you failed to face has grown bigger, terrible feedback loop

You must repent and change because you were wrong, you must humbly ask and knock and seek 

That is the barrier to us all... it is our destiny to transform chaos into order 

There is information in memories that have affected us negatively 

map is insufficient in some vital manner to navigate the world.  

It's not the expression of emotion assiciated with unfavorable events that has curative power

It's development of a sophisticated causal theory. 

why was i at risk.  what about the world made it dangerous , what was i doing/not doing that contributed to my vulnerability

how can i change my value structure, how must I change my map 

why did it happen to me?  resentment,   victimization signifying injustice 

life requires exposure to tragedy, you fix them (your flaws) or overcome them (others flaws)

encouragement, face problems head on, become strong and courageous, 

people that can do this get resilient, more able, better at constraining malevolence and resentment, more honest, better friends, more productive 

meaningful career choices, reduce volume of what others must cope with, suffer less, families suffer less, communities suffer less

order the structure of the world so that you are the hero, and everyone benefits and the world is a better place 

Friendship is a game that can sustain itself across time 


it does not matter    -  judgement of existence, judgement and damnation of being 

if I don't use logic - self-imposed curse, if you work hard people trust you and you learn to do difficult things
                    -   the alternative is 
