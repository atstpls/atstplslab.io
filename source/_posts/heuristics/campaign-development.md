---
title: campaign-development
tags: [analysis]
category: [reference]
date: 00:00:00
---


<br>

Knowledge is information and comprehension acquired through sensory input: reading, listening, watching, and touching. Knowledge is theoretical understanding of concepts, facts, principles, and information. It is a process--the union of an abstract principle with a set of concrete examples.

<br>

##### KNOWLEDGE IS THEORETICAL AND SKILLS ARE PRACTICAL

A familiarity with factual information and theoretical concepts.  Can be self acquired through observation and study or transferred.
Knowledge is primarily cognitive and involves gathering information and understanding

Skills are the application of knowledge in practical scenarios.  The ability to perform tasks or activities.

Skills are the application developed through practice and experience over time 
Skills are action-oriented and involve practical demonstration of abilities.
They are about doing and applying knowledge effectively to perform specific tasks or solve problems
Ability to do something, coming from one's knowledge, practice, aptitude, etc.
Implant knowledge and transform that knowledge into skill through physical training
Lectures, presentations, quizzes, theoretical content don't improve performance. 
Activities, scenarios, simulations.. Performing the actions does.
Skills can be developed more easily if one has prior knowledge of the task to be accomplished.
Practice is the only way to develop skills.
Knowledge provides the foundation, skills are teh tangible results of applying that knowledge 
    

###### Knowledge 

We are not all-seeing, all-knowing creatures. Best we can do with our knowledge is observe its functionality and improve it when it fails  ( mental model )

Projection (victim, rescuer, persecutor) is a tool which protects us from having to take personal responsibility and look at ourselves. The problem is always “out there,” “in them,” “over there,” but never “in me.”

<br>


<br>

||||
|-|-|-|
|<h5>skill</h5>| <b> [skil] </b>  | Ability to do something, coming from one's knowledge, practice, aptitude, etc. |

<br>

Learn and practice essential techniques for managing and protecting digital assets, social media content, and private data

<br>


A <a href="#campaign">campaign</a> is a connected series of operations designed to achieve a specific objective


The greatest accomplishments in life are not close victories you luckily were able to tip in your favor. They are the near-impossible victories that required enormous amounts of time, effort, determination, and perseverance. Victories that were genuinely and completely earned. Victories that embody the very best of humanity. And ultimately help others to be better.

<br>



||||
|-|-|-|
|<h5>tactics</h5>| <b> [tak-tiks] </b>  |   Procedures or set of maneuvers engaged in to achieve an end, an aim, or a goal |

<br>

Apply knowledge and skill to build the simplest, most effective methods for operating in a specific environment.

- Decision making and calculating risk and reward
- Emotion doesn’t help decision making and calculation
- Most people do their best work in a calm and emotionless state

<br>

The history of all modes of combat, whether individual or group, clearly shows the <b><i>SMARTEST</i></b> fighter, the one who employs his resources best and to greatest effect while avoiding harm, is the one who usually prevails.

<br>

Therefore, make your primary commitment to be <b>THE SMARTEST FIGHTER YOU CAN BE</b>. The one who constantly looks to maximize damage to an opponent whilst minimizing damage to himself.

<br>

Let <b>THOUGHT</b> be your first reflex when entering confrontation and <b>KNOWLEDGE</b> and <b>SKILL</b> your sword and shield before all other resources.

<br>

###################

###   TACTICS  ###

##################

TACTIC - Establish detachment and good faith 

Him: Have you ready any books!?  Any books at all!?!?

Me:  Stay focused

##########################

The Woke, at bottom, only do three things, over and over again, in different ways and with different domain-specific jargon. 
   
That is, they advance their narrative and make their opponents out to be too evil or stupid to have legitimate disagreements with them.

1. They twist the dialectical ratchet (Aufheben der Kultur) to advance their own narrative

######################

2. They undermine the moral authority of their opponents by making out that they’re complicit in some evil

######################

3. They drain the epistemic authority (ability to feel like you know what you’re talking about) of those they disagree with
   

######################

Diaclectic Traps - 2nd amendment wasn't designed to allow slaughter.  When you rebut, you find yourself defending slaughter
                 - tactic wants you to react and defend something indefensible so they can feed their base
                 - best thing
  
######################

Double Meaning   - use words that have esoteric (internal) and exoteric (external) meanings
                 - cult uses external to push things through, but members understand and execute the esoteric
                 - democracy exoteric is fully understood by public, but esoteric means elevate members into desired positions and prevent non-members from advancing
                 - self awareness exoteric is fully understood by public, esoteric means finding sexuality
                 - unhoused, marginalized, minoritized, 

Gender ideology is a created term claiming sex is socially constructed, mutable, and completely detached from underlying sex of the body

To say biological sex is redundant.  It's the only type of sex that exists.

Social emotional learning, started with Alice, Annie Besent, Mao, Gramshi, 

Comprehensive Sexual Education - Groomer school, trying to apply queer theory to developmental psychology, so they can say sex can be age appropriate for 5 year olds

They do not respect norms, they violate every norm they can

Every person is accepted as a sexual being, age and stage appropriate, not just about pregnancy and medical health

Using dog whistles and code language, 

Really about teaching kids about their sexual and reproductive rights, sexual citizenship, agency to dispel myths, access to resources and services, 


######################

Do you believe Big Pharma values humans more than profits ?   If yes, then examples.

######################

https://twitter.com/sherobbedmyboot/status/1680279342205526017

Mental jiu-jitsu 

Used another level to make the point without saying it 

Need name for this technique

######################

  Do as I say, not as I do ( Ben & Jerry's )

######################

I dont like to dwell...

######################


fractal nature of narratives 

objects are viewed with narrative (uses)

meta ethic 

unity or conflict

for unity you need to be united by a vision

divided people can't cooperate and compete peacefully

apoclalypse fear mongers are attempting to compel unity with terror and fear

online world is a new sensory social system, gives a false sense of reality 

gameable by narcissists and sociopaths 

COUNTER ADVERSARY OPERATIONS - leverage collaborative power of hunting and intelligence to raise cost of doing business for threat actors and give the adversary nowhere to hide

Behavioral-based preventions and detections 

disrupt adversaries force them to change their approach


implicit regulatory functions
investigate truth mutually, exchanges are bounded by realization it is repeatable interactions indefinitely
We become an interacting and iterative community 
Out of iterative interactions is how fundamental morality emerges 

psychopaths and predators and parasites sacrifice possiblity of long-term relationship for immediate gratification 

responsible iterative interaction like a relationship

Do me a favor

You only need to think this ( don't say it out loud )

Say the following to yourself:

"This guy might be right, besides the video I have nothing to back my claim."

Now...

WHAT ARE YOU FEELING IMMEDIATELY AFTER THAT?

Anger? Denial?  I'm really curious, what is it??

Kleptocracy - stealing resources of your entire nation
1. nationalize your natural resources, all valuable resources controlled by the state
2. Put your people in charge, pay decision makers
3. Control all trade, set artificially high prices, hold back goods to create demand, exhorbitant taxes

-------------------------------------------------------------------------------------


##### CAMPAIGNS

<br>

|Campaign|Objective|Tools|
|-|-|-|
|Vision and Story|A unifying story that will guide us as we make our way forward|Rejection is not failure--it is a guide that helps you grow and improve and overcome<br>- Rejection drives a different kind of hard work and success|
|Responsible Citizenship|Facilitate the development of a responsible and educated citizenry|- Blockchains to verify authenticity of sources of information<br>- Tools/techniques providing accurate context for information and events<br>- Tools/techniques providing accurate history and behavioral patterns|
|Family and Social Fabric|Define role for family, the community, and the nation in creating the conditions for prosperity|- Build itself and others up, not tear them down<br>- Set the example for loving and forgiving others and work to discourage hate, revenge, and self-pity<br>- Lead by doing rather than saying, demonstrating the power of teamwork and preservation in everything|
|Free Enterprise and Good Governance|Govern our corporate, social and political organizations so that we promote free exchange and abundance while protecting ourselves against the ever-present danger of cronyism and corruption|- Decentralized, incorruptible, distributed networks<br>- When information can be relied upon for decision-making, corporations can focus more on providing goods and services, society can better evaluate events, technologies, and unique world views, and governments will be incentivized to assume a more servant-like role|
|Energy and Resources|Provide the energy and other resources upon which all economies depend in a manner that is inexpensive, reliable, safe and efficient, including in the developing world|- A true free market combined with protected, true ownership of property seems to be the only way|- If people can confidently acquire wealth, secure it, and preserve it, a fair and honest game begins where the winners are those helping the most people and satisfying the most needs<br>- If the rules and goals of this game are clear, we all win|
|Environmental Stewardship|Take responsibility of environmental stewardship seriously|Should be Results-driven, transparent, voluntary, and incentivized|

<br>


###### Future Ideas

1. Vision and Story - Can we find a unifying story that will guide us as we make our way forward?

2. Responsible Citizenship
    
- How do we facilitate the development of a responsible and educated citizenry?
- Tools for responsible educated citizenry
- Technologies such as blockchains for verifying the authenticity of sources of information
- Tools/techniques providing accurate context for information and events
- Tools/techniques providing accurate history and behavioral patterns

<br>

3. Family and Social Fabric

- What is the proper role for the family, the community, and the nation in creating the conditions for prosperity?
- Build itself and others up, not tear them down
- Set the example for loving and forgiving others and work to discourage hate, revenge, and self-pity
- Lead by doing rather than saying, demonstrating the power of teamwork and preservation in everything

<br>

4. Free Enterprise and Good Governance

- How do we govern our corporate, social and political organizations so that we promote free exchange and abundance while protecting ourselves against the ever-present danger of cronyism and corruption?
- Decentralized, incorruptible, distributed networks
- When information can be relied upon for decision-making, corporations can focus more on providing goods and services, society can better evaluate events, technologies, and unique world views, and governments will be incentivized to assume a more servant-like role

<br>

5. Energy and Resources

- How do we provide the energy and other resources upon which all economies depend in a manner that is inexpensive, reliable, safe and efficient, including in the developing world?
- A true free market combined with protected, true ownership of property seems to be the only way
- If people can confidently acquire wealth, secure it, and preserve it, a fair and honest game begins where the winners are those helping the most people and satisfying the most needs
- If the rules and goals of this game are clear, we all win

<br>

6. Environmental Stewardship

- How should we take the responsibility of environmental stewardship seriously?
- Environmental stewardship should be Results-driven, transparent, voluntary, and incentivized

<br>

- Use vision to move through life in the world, pathways, tools, obstacles
- Derive what is by observing with your vision.
- We ignore things that are changing or normal, we are drawn to abnormal, changing
- We can imagine our future selves acting in the future and evaluate it.
- We generate variant selves, play it out internally, and pick best one.
- seek and you shall find, ask and you shall receive,     except white privilege
- incoherence causes anxiety which causes hopelessness, negative emotion 
- is there a mode of vision that enables resiliency in the face of ultimate catastrophe, atrocity, suffering, and death
- we know face the possibility of the catastrophy, expose yourself 
- stations of the cross - opportunities to expose yourself, to prepare
- i you cannot envision yourself prevailing against the mob, you will be a victim of the mob 
- voluntary exposure to challenge is curative
- the better you can face catastrophe of life, the better you will be 
- you want to be of service to others, need to get your life together
- you can share thoughts, play, foster development, improve together, work to face catastrophe together
- educate yourself to the limits of your intelligence. books, podcasts, projects, creativity, 
- ritualization of time and space, get what iterates right, things that stabilize you 
- deception and manipulation are not effective long-term strategies
- The reality is the exclusive domain for objective 

<br>


###### Strategy

Break up your life into practicalities. 

Develop a vision for yourself on all those fronts, what's valuable in an implementable way.

Pick those pathways and dedicate yourself to their optimization on any of those axis.

Learn to optimize your aim, then you begin to aim at what unites those goods, a higher good, what makes them good.

When you improve at pursing goods, and making proper sacrifices in that direction, you simultaneously learn how to approach the essence of all proximal goods.

Essential insistence of Christianity - the good that unites all those goods is the same good that's reflected in the image of Christ which is the acceptance of the suffering of life and the necessity of serving the lowest as the highest calling.

Freest society we've ever know are predicated on that idea. If that's true, why would you do anything else?

If you're not afraid of what that vision and what that implies for you and your soul then you didn't understand it.  It's an unbelievably endlessly promising vision of what your life could be.

Need a life so rich that it could justify my suffering.  Maybe there's a possibility that there's an aim so high that even the attempt alone to move in that direction is of sufficient value as to act as a panacea of suffering.

So you can say at the end of your life, "Oh my God, that was so hard. But it was worth it."

That's the choice you make at the crossroads,  if you have any sense.

<br>

Maybe it's the ultimate expression of each individual's divine worth and the willingness to serve the highest good despite evidence to the contrary. 