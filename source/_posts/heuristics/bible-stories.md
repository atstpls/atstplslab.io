---
title: genesis
date: 2023-01-22 06:00:42
tags: [trading]
category: [ecosystem, concepts]

---

The Bible is a framework that describes what reality is and does

The stories describe reality. They tell us about the structure of being. 

The Bible contains multiple deep truths:

- Confronting the challenges and suffering in the world forthrightly makes you stronger 


|Book|Description|
|-|-|
|[Genesis](#genesis)|God is the thing that confronts the potential chaos and generates habitable order that is good out of it|
|[Exodus](#exodus)|Escape from tyranny and reemergence into the promised land.classic descent and reascent.structure of priority with something at the top, what should be at the top.highest ethical spirit to which we are beholden allies themselves with freedom against tyranny. Voice of God calls you out of slavery, into freedom even if it pulls you into the desert |
|[Gospels](#gospels)||
|[The Flood](#the-flood)|| 
|[Tower of Babel](#tower-of-babel)|False pyramids will fall. You pay for not putting the proper thing at the top, towers of abstractions become totalitarian, God fragments it and makes people confused|
|[Abraham](#abraham)|God is the spirit that calls even the comfortable out to the catastrophic adventure of their life |
|[Temptation of Christ](#temptation-of-christ)|Jesus breaks the frame. True sovereignty is through giving and emptying, not by grasping and holding. The antidote to the temptations is the higher relationship, heart and soul oriented to the highest good, in service to what's higher|
|[Story of Job](#story-of-job)|Even under the most dire conditions, you have the option of maintaining your upward aim, the promise is if you do, the best possible outcome will occur. It might not be what you want, but what the hell do you know | 

<br>


Something must unite our attention and action, so that we are integrated, psychologically. Something must unite our interests and endeavours, collectively, so that we can cooperate and compete peacefully, productively, reciprocally, and sustainably. How then should our identity be conceptualised and embodied, practically and ideally?

Simple conceptualizations of human identity separates and divides a person into opposing poles of sovereign individual and faceless automation of state
This makes the complex internal hierarchy of the person collapse into singularity of autonomous liberal man, with intrinsic rights, and segregated from social context 
Broader social context is collapsed: family, neighbourhood, workplace, city, province, nation are subsumed into society, or collective, or state, which becomes superordinate above the individual 
Individual becomes viewed as opposition to the collective, and people begin to believe all social bonds are contrary to the call of freedom, even indistinguishable from oppression 
they see individual existence as an impediment to the establishment of utopian collective 


Sermon on the Mount 

- Those poor in spirit are blessed... those who have been brought low enough to be humble enough to be ready to receive 



#### NO BOOKS ARE WRITTEN BEGINNING TO END 

Authors go back and adjust, enhance, polish 

Consciousness is a stumbling block for an objective world.  What is being in the absence of consciousness?

We encounter a formless chaotic potential, somehow we use our consciousness to give that form, it's how people act 

Humans understand you can bargain with the future... potential futures which you can impact 

There's something in us that's in all of us that transcends all of us allows us to grapple with chaos and bring forth reality  

- Father is the interpretive structure
- Son is the transcendant consciousness with the capacity for action in all of us
- the Holy Spirit is the active principle that turns chaos into order

Belief is demonstrated by actions 

Speech is a causal element you put out in the world 

<br>






Your conscience calls you out, it functions as something you can't control 

Tells you when you don't live up to your ideal, so you have an ideal 

Synchronicity - the narrative and the objective world touch... 

Everything in the world has an infinite number of details, ways to describe it 

World appears to us through this hierarchy of manifestations, hierarchy of meaning

Like a co-creation of the world, try to define chair, you can sit on stumps, bean bags, 

Object perceptions are Projected modes of being, objective world is contaminated with utility,  

Everything in the story of the Bible reaches the limits of storytelling 


## The Beast and the Whore of Babylon 

Two prominent images in the visionary work portray the two possible trajectories of civilisation. The
first is a hybrid: a great, seven-headed scarlet beast, with the mother of all prostitutes seated on its
back. The beast is civilisation and its leaders, the colour of earth and blood. The false princess or queen
is the licentiousness offered by the would-be totalising state to its once-citizens. The beast is a
representation of false hierarchy, a living Tower of Babel, the arrogant state/king and its vassals, hellbent on total subordination, marking everyone witting or unwitting with the mark or the number of the
beast.
The great prostitute—the Whore of Babylon—riding on the back of the beast, is the denizen and
temptress of the dark alleys, byways, and brothels of the “big city”, the embodiment of the
licentiousness of Rome. She is the dissolution of constraint that accompanies atomized individuality,
the dissociation of sex from the constraints and guides of tradition. She is the freedom of anonymity in
the “universal city”, a whore embodying all the desires, mixtures, deviations, and fetishes characterising
those hypothetically freed from all higher-level identities and obligations. She is the total temptation
offered by the totalitarian state. Ultimately—ironically and inevitably—the scarlet beast kills the
prostitute: the totalising state promises freedom, but kills even desire, let alone its satiation. 

The State of Absolute Control
This strange, surreal, and endlessly compelling tale is a vision of warning: the ultimate state, promising
the freedom to pursue every conceivable whim, accrues to itself all the power that remains on the table
as responsible conduct is abandoned, using that power not to free, but to enslave. The worship of
The Subsidiary Hierarchy
4
impulsive idiosyncrasy, and the accompanying destruction of subsidiary structure, invites, enables, and
even necessitates the “State of Absolute Control”. Someone, after all, has to pick up the pieces.
This pattern reveals itself when the pendulum swings, and the hedonism of Weimar is transformed into
the totalitarian Reich; when the Anarchism of the French Revolution transmutes into centralised
Napoleonic empire. This same dynamism ruled during the Covid-19 pandemic, which was, most truly,
a plague of authoritarianism. Our individualistic and hypothetically free societies re-organised
themselves in a heartbeat into a rigid and comprehensive totalitarianism, with all those who objected
demonised, punished, castigated, and excluded. The majority participated with enthusiasm, offered as
they were the tantalising opportunity to inform oh-so-moralistically on a neighbour.
It is in such moments that we can see the relationship between the Whore of Babylon in Revelation,
with all her idiosyncrasy, her easy desire, and the increasingly all-promising state, which can and does
all-too-easily metamorphose into its jack-booted and uniformed opposite. The punk rocker or the furry
with his loud and pathological impulsive idiosyncrasy and anti-authority individualism could not exist
for a moment in the Amazonian jungle. He is the eternal child of the atomised techno-society, the
beneficiary and infant of the state as Great Mother and Father—devouring parents, enabling, however
temporarily, his narrowly self-serving desires. 


the image of the
heavenly city is in fact the ultimate representation of structured harmony, a vision of the reality that
might obtain if the entirety of existence properly found its place, served what is highest, and integrated
itself into a transcendent whole. 


<br>


### GENESIS

The description of the world in Genesis is the best description of the world

It's a description in meaning, of how all of creation is a hierarchy of beings

That is joined together in this central focus

How reality is born in consciousness, in name, in identity, in meaning 

God creates the world to be good, everything you do is based on the perception of goodness

Identities are goodness, a thing's name and its goodness is identical 

Every time you see something that has an identity, I evaluate and measure it based on the identity that I perceive

I don't want to drink out of a glass that has holes in it 

Geneses 1 & 2 gives a descriptio of that process

Image of human as place where all multiplicity is gathered, heaven goes inside, man joins heaven and Earth 

Joins all invisible aspects of reality (names, identities, purposes) into the particular 

Not a scientific description because it's a better description 

It accounts for the mechanism of science, because science identifies something it's going to study

Posits a theory, analyzes phenomena, judges phenomena against the theory,

Always looking at things and judging if they're good 

Always decides whether theory accounts for phenomena

Geneses 1 describes this process... looking at the world, deciding if it's good 

In God's case it's always good, but we need to realign our thinking to see that it's good 


Athiests:  If we just have enough rationality, everything will be fine 

Wrong.  We need purpose.


Genesis is a map you can use to interpret the world, but also a way to inhabit the world 

##### Genesis

First book of the Hebrew Bible.  It's Hebrew name is the same as its first word, Bereshit ("in the beginning")

An account of the creation of the world, early history of humanity, and the origins of Jewish people.

Tradition credits Moses as the author of Genesis, Exodus, Leviticus, Numbers, and most of Deuteronomy.

- Creates Earth
- Tree of good and evolution
- Cain and Abel 
- Noah's Ark 
- Tower of Babel
- Sodom and Gomorrah
- Abraham Test 
- Jacob Wrestle with Angel 

##### Exodus 

The origin myth of the Ireaelites leaving slavery in Biblical Egypt. 

You experience joy by setting a goal and achieving it.

The higher the goal, the most joy.

With the highest ideal, you have the precondition to joy.  You aim for the highest goal and achieve it to obtain the highest joy.

- Pharaoh does not remember Joseph
- Fears that Issraelite slaves could overthrow him 
- Hardens labor and orders killing of all newborn boys
- Moses's mother sets him adrift and Pharaoh's daughter finds him and raises him 
- As an adult, he goes to find his kinsmen
- He witnesses abuse of a Hebrew slave by Egyptian and kills him 
- Flees to Midian, marries Zipporah
- Encounters burning bush, God tells him to return to Egypt, free Hebrews, take them to Canaan
- Pharaoh refuses to release slaves, ten terrible plagues
- Israelites trapped at Red Sea, God parts the sea
- God pronounces Ten Commandments
- God gives Moses instructions to build tabernacle and for priestly vestments, sacrifice offerings 
- Aaron, Moses's brother, becomes first hereditary high priest 

#### gospels

<br>

Enlightment doctrines have turned on themselves. 

We see the world through a story.  The description of the way someone sees the world is a story. Even our perceptions of objects are micro-narratives.

We can't derive the world with a list of facts. So what's the story?

There must be a story.  If there isn't, there is no aim, no hope, and despair. 

Candidates are the Story of Power, the Story of Hedonism, they cannot sustain themselves and cannot sustain themselves.

What could unite even power and sex harmoniously?  The ultimate Story of Sacrifice does and it benefits the community broadly.

It's deeply mysterious and not understandable from the purely rational perspective, but it seems to be right. 

Gospels call you to bear the weight of tragedy and malevolence on your shoulders along with the divine spirit that guides you. 

Climax of the New Testament is that God is love. The ultimate reality is love. Everything is subordinate.  Without this overarching, supreme value, we risk having Power unleashed or Hedonism for its own sake 

Arc of the narrative of Christ is the worst possible fate in the worst possible scenario overcome by love 

<br>

Baptism 

The story of Christ needs to make passage, he ends the old world and the new one begins, another pattern of undoing of the world before it's reborn 

The Spirit descends on Mary just like in Genesis, John is seen as beginning a new temple, outside the center of the community 

He's the end of the old temple and beginning of the new, old rituals to new rituals, 

The entire story is fractal, it recapitulates the initial state, dissolution of the old personality, generation of a new 1. 

Death of the old tyranny, Moses and Israelites saved from red sea, they are forgiven, Egyptians are so bent beyond redemption they die 

Washed clean of the Egyptians.  To be saved is to be forgiven from sins. 

It's a recapitulation of all the water crossings from Old Testament, the cleansing, the transformation, to a new way 

Glorification of the infant, Women look to Virgin Mary or Whore of Babylon, 

Whole Bible says if you give Glory to God, there will be peace on Earth, if you don't, tyranny rules 

-----

The opposite of the Incarnation is the disjoining of Heaven and Earth, saying one thing and doing another 

-----










<br>

Christ is a pivotal figure of the New Testament similar to Moses in the Old Testament.

Hero story that sets the stage for the most meaningful life and most peaceful future 





