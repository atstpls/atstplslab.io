---
title: context
date: 2023-07-18 22:00:42
tags: [money]
category: [ecosystem, resources]
---

<br>

Our perceptual representations of reality

<br>

##### Mechanics

ME FRAMES attempt to SOLVE problems to help the listener

TEAM FRAMES attempt to SURFACE problems to get the listener to think and help 

"Perception simplifies the world, without sacrificing functional grip." -Jordan Peterson

We frame our objects using abstract simplification.

We perceive meaningful phenomena, not the objective world.

We see low-resolution images of objects are bounded by their affective relationship to a goal.

We tune our perceptions using our motivational systems.

Simplified knowledge, constant blindness, and endless opportunity for error.



#### MOTIVATION ACTION PERCEPTION

Motivation doesn't drive behavior or set goals.
    It provides current state of being with boundaries and values.
    These remain unquestioned if current aciton produces its desired ends.
    As Motivation, Action and Perception (MAP) schemas.

basic MAP - perception of point A (undesired beginning-state)
          - point B (desired end-state)
          - motor actions designed to bring about the transformation of former to latter
          - objects and events relevant are perceived
          - objects and events irrelevant fade into non-existence

#### SPONGES

Sponges lack a CNS, they are composed of sensorimotor cells arranged in array
    The array maps detectible environmental patterns directly to range of motor actions
    With no perceptual intermediation
    No objects are percieved.  
    One pattern evokes another.

#### HYDRAS

Hydras have primitive CNS with sensory, neural and motor cells.
    Neural cell intermediation allows same patterns to produce different outputs.
    Drawback is speed due to more switches means longer reaction time.

#### SENSORY SYSTEMS

Sensory systems use two branches:

  1) One to Motor system for reflex-like speed
  2) One to Cortex for slower elaboration of response

Hypothalamus is a locomotor pattern controller
Hypothalamic animals are hyperactive

#### MAP PROCESS

1. external world is mapped on to motor output, before it is perceived
2. mapping transforms itself into object-perception, as CNS develops in complexity
3. tight connection between sensation and action, even when perceptual intermediation arises
4. schema which object is perceived is controlled by hypothalamically grounded, goal-directed motivation

    Cortex substitutes abstractions for primordial goals
    Goals considered a class so that diversity of potential goals can be ignored, and goal itself is object
    We test possible solutions to the now-bounded frame problem
    We embody a solution, as a consequence of favorable mutation, or stumble across an answer
    We communicate our successes and move up the dominance hierarchy
    We try many useless approaches, and conserve those that work

#### PROBLEMS

3 problems remain:
  - Sequence/Timeframe - in what order should MAP schemas manifest themselves over day, week, year
  - Importance - Which MAP schemas should be granted priority of value
  - Social Being - How should I adjust MAP schemas to those around me



<br>



##### Mechanics

question people's goals, motives, methods, and logic 
offer ideas, perspectives, lessons learned from experience

Science
- Formal: formal systems of logic, mathematics which use a priori not empirical
  - Logic, Math, statistics, game theory, computer science
- Natural: natural phenomena 
  - Physical: Non-living systems (Physics, Astronomy, Chemistry, Earth)
  - Life: Living systems (Biology, Virology, Zoology, Botany, Ecology)
- Social: human behavior and its social/culture aspects (Economics, Psychology, Sociology, Media Studies, Political Science, Law, History, Geography, Criminology)

Groups are any collection of at least two people who interact with some frequency and who share a sense that their identity is somehow aligned with the group
 groups can influence the attitudes, behavior, and perceptions of their members.

People who exist in the same place at the same time but who do not interact or share a sense of identity—such as a bunch of people standing in line at Starbucks—are considered an aggregate, or a crowd. Another example of a nongroup is people who share similar characteristics but are not tied to one another in any way. These people are considered a category, and as an example all children born from approximately 1980–2000 are referred to as "Millennials." Why are Millennials a category and not a group? Because while some of them may share a sense of identity, they do not, as a whole, interact frequently with each other.
Interestingly, people within an aggregate or category can become a group. During disasters, people in a neighborhood (an aggregate) who did not know each other might become friendly and depend on each other at the local shelter. 
. Consider teachers, for example. Within this category, groups may exist like teachers’ unions, teachers who coach, or staff members who are involved with the PTA.

an in-group is the group that an individual feels she belongs to, and she believes it to be an integral part of who she is. An out-group, conversely, is a group someone doesn’t belong to; often we may feel disdain or competition in relationship to an out-group.

Groupthink
Group Polarization
Social Loafing
Deindividuation

First principles thinking is the practice of obliterating this tendency to follow and breaking assumptions down until only basic factors remain. Reasoning by first principles removes the impurity of assumptions and conventions.

This method strips away the opinions and interpretations of other people and gets you to the essential elements that exist. From there, you can then build back up to a solution, often with an entirely new approach based on truths that are unimpeachable and indisputable—because you are not resting on any assumptions anymore.

Occam’s Razor was originally expressed as "Entities should not be multiplied beyond necessity"—simply, one should not over-complicate problem-solving by bringing in too many extra hypotheses, variables, or extraneous factors. Drawing from that original principle, Occam’s Razor is often stated in present times as "The simplest explanation is usually the correct one" as well as "The more assumptions you have to make, the more unlikely that explanation is."

Hanlon's Razor

It was originated in 1774 by Robert Hanlon as "Never attribute to malice that which can be adequately explained by neglect." The most modern and widespread version is "Never ascribe to malice that which is adequately explained by incompetence" and is often attributed to Napoleon Bonaparte, though author Robert Heinlein has a strong claim to it.

So how does this relate to Occam’s Razor and the preference for simple explanations with as few variables as possible? Because making assumptions about someone’s intentions and motivations based on their actions is, well, a rather large assumption. The most likely cause for malice, or any other negative intention, is neglect or incompetence.

In other words, it’s easier for a person to do something negative out of neglect or incompetence, and it takes another few steps to truly be able to say that malice is the cause. Being that we are not a species with psychic powers, we will never know people’s intentions.

This mental model assumes simplicity in the realm of social interaction. If you presume that people only want to be good to you, it has the power to massively improve your relationships.

The Pareto Principle was named for an Italian economist who accurately noted that 80% of the real estate in Italy was owned by only 20% of the population. He began to wonder if the same kind of distribution applied to other aspects of life. In fact, he was correct.

The Pareto Principle applies to everything about the human experience: our work, relationships, career, grades, hobbies, and interests. Most things follow a Pareto distribution, where there is a fairly skewed ratio between input and output. It’s about finding the best bang for your buck.

Sturgeion's Law

For the purposes of our discussion, Sturgeon’s Law means that the vast majority of information is low-quality. You could even say that 90% of what we think about on a daily basis isn’t worth the time. And that’s true to an extent. Our brains make a million neural connections every day—certainly most of them aren’t necessary or even useful.

With clear thinking, Sturgeon’s Law works in a twofold way. First, consider that much of the information we might use to assess something is inessential, poorly constructed, insignificant, or just plain wrong. Second, we shouldn’t get too consumed by how terrible these parts are; rather, we should focus on the thinking and processes that are good.

When we’re trying to solve a problem or understand something, therefore, we should concentrate on the most vital components or the most reliable, provable information. Don’t waste a lot of energy on the most common flaws or the most disparaged elements. Sturgeon’s Law says its low quality makes it unimportant, so it’s dispensable. And as Occam’s Razor suggests, giving too much attention to the inessential will only knock essential thinking off-course.

Mental Model #24: Murphy’s Law: Anything that can go wrong will go wrong, so make sure it doesn’t have the opportunity. Don’t rely on just getting by; make sure that you are as fail-safe as possible.
Mental Model #25: Occam’s Razor: The simplest explanation with the fewest variables is most likely to be the correct one. Our instinct is to go for the most mentally available explanation, which says more about what we want to see or avoid.
Mental Model #26: Hanlon’s Razor: Malicious acts are far more likely to be explained by incompetence, stupidity, or neglect; assumptions about one’s intentions are likely to be wrong. Improve your relationships by giving the benefit of the doubt and assuming, at worst, absent-mindedness.
Mental Model #27: Pareto Principle: There is a natural distribution that tends to occur, where 20% of the actions we take are responsible for 80% of the results; thus, we should focus on the 20% for maximum input-to-output ratio. This is in the name of becoming results-driven and simply following what the data is telling you. This is not about cutting corners; it is about understanding what causes an impact.
Mental Model #28: Sturgeon’s Law: Ninety percent of everything is crap, so be selective with your time. Start with the 10% absolute non-crap and slowly work your way out.
Mental Model #29-30: Parkinson’s Laws: First, triviality can easily set in because it feels good to feel productive and voice your opinion. Know your real priorities and ask if progress is actually being made toward them. Second, work expands to fill the time it is given, so give it less time. Wanting to work at a relaxed pace often just causes self-sabotage.
 
Pragmatism - evolutionary biology 

Victimization and Oppression is opposite of gratefulness and duty, responsibility

By giving in to victimization, you deprive yourself of agency

Victims seek redress, justice seekers move forward 

Injustice (negative rights):
    - violation of due process
    - granting of status for any reason other than merit
    - infringing on inalienable rights: life, liberty, property

Left's injustice (positive rights):
    - unequal salary, housing (equity)
    - unequal ethnicity/race (diversity)
    - healthcare is too expensive (inclusion)


Overwhelming compassion works on babies, not adults

A mental model, simply put, is a representation of the simple mechanics on something. This is a very broad statement, but mental models are inherently broad, as you can apply a model to literally anything in life. It’s impossible for us to keep every minute detail of everything we encounter in the world, so these models act to simplify the more complex aspects of life into more digestible and organizable units.

Once you have achieved cognitive defusion, you will notice that you are:

Becoming aware of the thoughts, as opposed to perceiving reality through those thoughts.
Noticing your thoughts, as opposed to being entangled in them.
Able to see thoughts cycle through, coming and going, as opposed to holding onto one so others can’t form.
This is the goal we’re working toward and, while it won’t be 100% perfect from the beginning, it will be something you’ll be working toward. Progress in any measure at the beginning is great. Once you can get to this line of thinking, and this position with your thoughts, you will find exponentially more control as time progresses.

When we look at the purpose of cognitive diffusion we find the purposes are thus:

To be able to notice the true nature of our thoughts; they are simply words and/or images in our minds.
To be able to respond to our thoughts with positive action if they call for it.
To take action only on what works, rather than what we feel is true.
To be aware of the thought process as it’s taking place
To recognize that thoughts cannot and should not dictate our behaviors
To use cognitive defusion when our thoughts impede you from staying true to your core values.
As previously mentioned, this all starts with taking a moment to pause and evaluate whether or not the thoughts you’re having about the current situation. Evaluate whether the thoughts about this situation have a positive or a negative sound to them. Try to trace that back and see where that negativity began as regards the current situation.

Actor-Observer Bias
This is the tendency to conclude that factors you’ve created were in fact inspired or created by outside factors. For example, assuming that your high blood pressure is the result of genetics while you readily assume that the cause for it in others is poor diet and health practices.

Anchoring Bias
This bias is characterized by the tendency to rely too firmly the first piece of information you heard on a particular subject. For instance, if you learn that houses in your neighborhood are the best-priced in the county, you may never check prices in other neighborhoods as a result. This is in spite of further development in other areas, new laws and more.

Attentional Bias
This is a sort of "tunnel-vision" when it comes to making a decision. Your attention may be focused too heavily on one or two factors while others may more heavily affect the quality of the decision being made. For instance, you want to know what school district your new house will be in, but don’t pay attention to things like possible termite damage, or the age of appliances.

Availability Heuristic
This heuristic device places more importance on the information that comes to mind most readily, whether it is the most relevant or not. You assign greater importance to that information and have a tendency to overestimate the possibility and likelihood of similar occurrences in the future.

Confirmation Bias
The tendency to favor information that conforms to or "confirms" your current beliefs and to discount information or evidence that doesn’t conform.

False Consensus Bias
This bias gives you the impression that others around you are agreeing with you more than they actually are.

Functional Fixedness
This is the conclusion that certain objects are solely useful for one specific purpose. An example of this would be giving up on erasing a pencil line from a page before realizing that a rubber band would do the trick as well.

Halo Effect
The Halo Effect is the way in which your opinion about someone’s attractiveness, validity, intellect, and other qualities are affected by your overall opinion of the person.

Misinformation Effect
This is an effect that can color your memories of something which transpired, based on information that came to light after the fact. This effect can call into question the validity of eyewitness testimony of someone who has stayed informed of further developments in a case.

Optimism Bias
This bias tells us what we are more likely to achieve success and less likely to suffer hardship or misfortune than our peers. You may recall hearing the phrase, "You never think something like this would ever happen to you," in testimonies following tragic incidents. 

Self-Serving Bias
This bias tells us to blame outside factors or forces for the misfortune that befalls us. In addition to this, it tells us to give ourselves credit for the successes that we have, or for the moments of good fortune. If you lose $50 on the street, it’s because of bad luck. If you find $50, it’s because of good karma.

<br>

##### Analysis

conspiracy
technological revolution, larger scale 

micro cabels actig out the same archetypical patterns 
network of associated ideas that have an animating spirit within thime that possesses people in mass and they act in accordance with its dictates

scholemitz appegoo -   horrors of the communist regimes weren't an anomaly of a potentially valid system
they were inevitable consequence of a non-playable game    ended in genocide
communist system laid itself out according to a certain set of animating principles and that turned into genocidal massacre 


interpretation of text - post modern say there's so many ways to interpret, if true for text true for all things, whose to say 
We've evolved perceptual structures oriented to specific goals

Kant objection to pure reason... problem is too many facts for them to speak for themselves, so overlay on top of them an interpretive framework

happens in a trial and error process, cooperative landscape means plans and interpretations are constrained, we must agree on the same game to play with others
Narrows it to interpretations that are deemed socially acceptable, but does it work, test the tool
number of interpretations are not limitless, they are constrained

3 levels of constraint:
- not true on sociocultural level - has to be negotiated, game you can play with other people
- not true on subjective desire - game you want to play
- not true on biological evolutionary grounds - effective in the world

pragmatic definition of truth.. if you use a map and plan and it gets you there, we validate the plan and map
we operate in a world where our knowledge is always insufficient
we never know anything about anything, how do you make a judgement about whether you're correct

<br>

### KayFabe

System of stratified lies that professional wrestling is undergirded by  

Carnival speak for the word "fake"

Layering of nonsense and reality   

https://frameproblems.substack.com/p/frame-problems-051921


Altered reality of KayFabe 

Work - the script 
Shooting - going off the script 
Worked shoot - a scripted shoot ( appears to breakout of the script )
Marks - audience 

Difficult for the brain to go beyond 4 levels of lies 

General monotony + extreme danger 

Unique psychology of Kayfabe - the audience believes and disbelieves at the same time 
                             - not clear where kayfabe stops and the real world begins 

Distinguishing actor from character they play 

Pro wrestling shows us what's possible 

Kayfabrication - transition from failed reality ( general monotony + extreme danger ) to a successful fakery 



Alien aircraft detected by USN cruiser 

https://frameproblems.substack.com/p/frame-problems-052621















