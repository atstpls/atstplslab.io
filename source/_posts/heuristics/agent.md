---
title: agent
date: 2023-10-18 01:00:42
tags: [resiliency, government]
category: [ecosystem, resources]
---

<br>

##### TYRANT PLAYBOOK

Hitler
  TACTIC:  Believe in Yourself
  TACTIC:  Outrage Sells          - understand the current resentment, sell yourself as the means to overcome that 
  TACTIC:  Be a Man of the People (Musollini, Idi Amin, Gaddafi, )
  TACTIC:  Build Your Squad 
  TACTIC:  Choose When to Strike

Sadam Crush Your Rivals
  TACTIC:  Establish Dominance
  TACTIC:  Be Everywhere
  TACTIC:  Buy Loyalty
  TACTIC:  Master Mind Games
  TACTIC:  Everyone is Expendable

Idi Amin - Reign Through Terror
  TACTIC:  Choose a Scapegoat
  TACTIC:  Bring the Pain
  TACTIC:  

Joseph Stalin - Control the truth
  TACTIC:  Rewrite history
  TACTIC:  Censor Everything
  TACTIC:  Destroy God
  TACTIC:  Corrupt Science - ideological dogma  Trofim lysenko & food shortage, applied marxism principles to plants, framed it marxian
  TACTIC:  Eliminate Trust - The Great Terror 1936 - 1938 

Gaddafi (Libya) - Create a new society
  TACTIC:  Be the Law - prohibited free speech, banned alcohol/night clubs, taxi cabs, adultery, trade unions, worker strikes
  TACTIC:  Build Your Legacy - projects to achieve immortality, upgrade nation/image, 



Mao (China) - Change culture
  TACTIC:  


Mao rook citizen's homes, land, belongings.
Herded villagers into communes and everything was collectivised. 
Food distributed according to merit, weaponizing state allegiance and obedience. 
Coercion and violence were used to incentivize work
Nation-wide famine, Tens of millions of people died of starvation
2-3 million victims tortured to death or summarily killed

BEAUTIFUL TROUBLE

REFRAME   ( narrative analysis, make invisible visible, )
          ( conflict, characters, imagery, foreshadowing, assumptions )


Religion usually answers the questions that cant be answered
What's the meaning of life?
Why are we here?

Dogma marxism-leninism  new religion  replace old religion with state 
created new idols like pavlik morozov (invented myth, propaganda)