---
title: soros
tags: ["analysis"]
category: [reference]

---

<br>

- George Soros is an American citizen. He was born in Hungary, but he came to the U.S. in 1956, obtained a green card, and then became a naturalized U.S. citizen in 1961. He has dual citizenship with Hungry and since 2019, he has owned several homes in the U.S., including an estate in Southampton, New York, a Fifth Avenue duplex in Manhattan, and a “residential compound” in Katonah, N.Y.
- one of the most rapacious global billionaires in the history of the world and the primary funder of anarchy in the U.S.
- the Soros network, which promotes a global open society and seeks to abolish national frameworks, is the greatest threat faced by the states of the European Union. The goals of the network are obvious: to create multi-ethnic, multicultural open societies by accelerating migration, and to dismantle national decision-making, placing it in the hands of the global elite.
- For many years, the $32 billion foundation has been preparing the world for globalism by destabilizing nations around the world. The foundation plays a major role in funding and training people for riots in American cities and in the election of woke mayors and district attorneys in cities who are catering to criminals, cracking down on law abiding citizens, encouraging sanctuary, and spending billions on lost progressive causes that often lead to massive homeless populations on the streets
- He bet $10 billion dollars that the British pound was going to drastically lose value against other currencies, essentially declaring war on Great Britain’s banking system, its currency, and its people. The pressure he put on the currency, including his public predictions about the pound being too highly valued, pushed it into falling. Soros made $1.5 billion dollars in September 1992 by this means. The event led to billions more being put into the Quantum Fund that he managed
- a billionaire who shares his views funnels hundreds of thousands of dollars to a state-registered political action committee, which then spends almost all it on TV advertising, research, polling, direct mail and other campaign related activities in support of the would-be defender. 
- The PAC money represents over 90% of the candidate’s fundraising and enables him to outspend the incumbent public defender by a margin of 3.5 to 1. 
-  once these candidates get elected, they quickly turn their anodyne statements into something different and engage in prosecutorial nullification by refusing to prosecute entire categories of crimes, watering down felonies, refusing to ask for bail, and refusing to prosecute violent juveniles as adults. Of course, violent crime rates often explode in their cities.  
