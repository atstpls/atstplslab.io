---
title: kisin
tags: ["analysis"]
category: [reference]

---

<br>

Konstantin Kisin is a Sunday Times bestselling author, satirist, social commentator, and creator and co-host of free speech podcast TRIGGERnometry. In 2022, he published An Immigrant's Love Letter to the West.


