---
title: mccullough
tags: ["analysis"]
category: [reference]

---

<br>

Peter A. McCullough, MD (Fighting for medical freedom through scientific data and analysis)
- internist, cardiologist, epidemiologist
- manages infectious diseases, cardiovascular complications of viral infections and COVID-19 injuries
- more than 1,000 publications, over 685 citations in National Library of Medicine 
- First widely utilized treatment regimen for ambulatory patients infected with SARS-CoV-2 in American Journal of Medicine 
- First detoxification approach "Clinical Rationale for SARS-CoV-2 Base Spike Protein Detoxification in Post COVID-19 and Vaccine Injury Syndromes 
- Testified multiple times in Senate, House, state committees
- McCullough Foundation produces high-quality audio-visual reports, presentations, documentaries to educate public about medical conditions, medical ethics, doctor-patient relationship, and lawful methods for citizen participation in debates about public health issues 
- Provides support to public officials demonstrating prudence and discernment in crafting public health policies free of Bio-Pharmaceutical interests
- Government for the people by the people can only exist with education about health, disease, and public health policies 

+ Fear of deadly infectious diseases was exploited by public health officials with commercial interests
+ They invoked emergency power under the guise of protecting us
+ Undue influence of Bio-Pharmaceutical Complex in our public institutions 

> Bird Flu: Separating Fact From Fiction and True Danger From Fear-mongering 
> Risk Stratification Approach for Future Cardiac Arrest after COVID-19 Vaccination 
> McCullough Protocol: Base Spike Detoxification (BSD) 
> McCullough Protocol for Early Treatment 
> The Wellness Company prescriptions, medical kits, guidebook

