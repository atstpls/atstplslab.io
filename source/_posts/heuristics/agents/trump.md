---
title: trump
date: 2023-01-14 14:00:42
tags: [debate]
category: [reference]
---

<br>

The Constitution requires, as a default rule, that “Officers of the United States” must be nominated by the president and confirmed by the Senate. 



### Capitol Riot and 2020 Election 

Federal prosecutors allege he pressured officials to reverse the results, spread lies about election fraud, and exploit Capitol riot on January 6th to stay in power.

four criminal counts, including conspiracy to defraud the US and conspiracy against the rights of citizens.

 denied wrongdoing and claimed the Biden administration is behind the prosecution.




### New York Hush Money 

adult-film actress Stormy Daniels shortly before the 2016 election.
Ms Daniels was paid $130,000 (£103,000) to keep quiet about her claim that she had sex with Trump, which he denies.
Providing so-called hush-money is not illegal.
Instead, this case was more technical and centred on how Trump's former lawyer, who paid Ms Daniels, had his reimbursement recorded as legal fees in Trump's accounts.
 prosecution's key witness was ex-lawyer Michael Cohen, who testified that his former boss knew about the elaborate scheme to disguise the payment.
In days of heated cross-examination, Trump's lawyers sought to undermine Cohen and paint him as an unreliable witness and convicted liar.
The jurors deliberated over two days before finding the former president guilty of all 34 counts of fraud under campaign finance laws.

In New York, falsifying business records is a felony only if defendant had an intent of committing or concealing a separate underlying crime. The Manhattan district office charged Trump with felony of falsifying business records to conceal criminal activity, including attempts to violate state and federal election laws," but did not specify a particular underlying crime. 


### Trump Fraud Case 

Over-valued Mara Lago 
Appeals court Oct 2024 
- Was the case worth hearing in the first place 

Judges asked them 
- Find one other time in history where borrower showed bank didn't think it was fraud 
- Find one other time in history where someone prosecuted for lying about property value when loan was paid back with interest 
- Find one other time in history where AG prosecute case where no damage to the public, or person you said was defrauded, no actual malice committed by borrower 






### Georgia 2020 Election 

Call with Brad Raffensperger on Jan 2, 2021 

Trump and some 18 other defendants are accused of criminally conspiring to overturn his very narrow defeat in the state of Georgia in the 2020 election.
The racketeering investigation, led by Georgia prosecutor Fani Willis, was sparked in part by a leaked phone call in which the former president asked the state's top election official to "find 11,780 votes"

13 criminal counts, subsequently reduced to 10. They include one alleged violation of Georgia's Racketeering Influenced and Corrupt Organizations Act (Rico).
The former president has pleaded not guilty.

- out of state and dead votes Dominion is removing parts of machines 
- drop box stuffing, Faulty election result, dishonesty or incompetence 



- No, there are only 2 dead votes, and I'm sure Dominion has not removed any parts of machines 
- We had LEO, FBI, GBI talk to those involved 


### CLAIMS  


- 5K voters from dead people, thousands were told they had already voted 
    - Trump report day before cited high confidence on only 23, GA found 4 

Security protections were laughable. Laws, updates, visual signature matching are useless. 

- Drop boxes picked up and not delivered for days 
    - it's rare, locks, seals, secure fastenings, video surveillance 

- 904 only voted from po box instead of permanent address 
    - not illegal, would exclude homeless and tribal communities 

- Out of state voters voted in GA 
    - records get updated 

- Absentee ballots sent to vacant addresses  2,326
    - procedures ensure requests for mail ballots come only from currently registered voters 
    - visual/scan verification of PII and signature against that stored on voter records 






### Classified Documents 

Whether Trump mishandled classified documents by taking them from the White House to his Mar-a-Lago residence after he left office.
The case against Trump is also about whether he obstructed the FBI's efforts to retrieve the files, as well as the criminal investigation into his handling of them.
The majority of the counts are for the wilful retention of national defence information, which falls under the Espionage Act.
There are then eight individual counts, which include conspiracy to obstruct justice, withholding a document or record and making false statements. Trump has pleaded not guilty on all counts.
Judge Aileen Cannon, a Trump appointee, made the dismissal on the basis that the Justice Department's appointment of special prosecutor Jack Smith violated the US Constitution.
But Smith is expected to appeal against the decision, which came after Judge Cannon cancelled the 20 May trial date without giving a new one.


### RFK AD 


- Historic inflation, illegal immigration, corporate corruption, forever wars, chronic disease epidemic 


- Media is run by Democrat Elite a corrupt oligarchy that 
- censors free speech, silences political opponents, supports forever wars, and abandons democracy by annointing its candidates 

- shape and suppress, orwellian totalitarianism, communist fiscal policies, 
- rational thought, identify propaganda, freedom of choice, loss of hatred, anti-narcissistic 

- hopelessly indoctrinated

- No policy vision

- Think independently, constructive critical thinking 
- 


### DARK TETRAD 

Machiavellianism        A tendency to use people as a means to achieve one's own goals 
Narcissism              A belief that one is special, gifted, and superior to others 
Psychopathy             A lack of affective empathy and a willingness to exploit others 
Sadism                  A tendency to derive pleasure from the pain or humiliation of other


### CLUSTER B 

Boderline personality       Difficulty managing emotions, emotional responses, impulsivity, unstable self-image 
Histrionic personality      Pervasive attention-seeking behaviors and exaggerated emotional displays
Antisocial personality      Not respecting authority, following rules, fitting in with cultural norms 
Narcissistic personality    Excessive need for admiration, lack of empathy, inability to handle any criticism, sense of entitlement 

Flattery, Gaslighting, emotional blackmail, love-bombing, seduction to obtain affection or avoid abandonment 


### BIG 5 

Agreeableness       94      Compassion (85) and politeness (96)         Interpersonal interaction, cooperative, considerate
Conscientiousness   75      Industriousness (67) and orderliness (76)   Obligation, attention to detail, hard work, persistence, efficiency, rules 
Extraversion        12      Enthusiasm (13) and assertiveness (18)      Sensitivity to hope, joy, anticipation, social situations 
Neuroticism         0       Withdrawal (1) and volatility (1)           Sensitivity to negative emotion, pain, sadness, defensive anger, fear, anxiety 
Openness            61      Intellect (54) and aesthetics (63)          Creativity, artistic interest, (verbal) intelligence 


                    Me                  Tracey 
Agreeableness       94  (85/96)         98  (96/97)
Conscientiousness   75  (67/76)         13  (4/41)
Extraversion        12  (13/18)         34  (70/12)
Neuroticism         0   (1/1)           98  (94/99)
Openness            61  (54/63)         3   (0/44) 

### GORKA 


Trump the most pro-Israel president ever, won over votes of millions of Jews

The only constituency he lost votes on was whites, every other demographic went up 


Soft immigration from countries that hate the USA encourages attacks and emboldens attackers 



### JUST BY WINNIN 


- Qatar, Iranian proxy, declares Hamas is not welcome 
- Putin ready to talk treaty 
- Hamas wants to deal 








### 2025 PLAN TO END THE DEEP STATE


- [Re-issue my 2020 Executive Order]()   restoring the President's authority to remove rogue bureaucrats
- [Clean out corrupt actors]()           in our National Security and Intelligence apparatus, departments and agencies that have been weaponized
- [Reform FISA courts]()                 corrupt judges, judges lied to in warrant applications
- [Establish Truth and Reconciliation Commission]()   to declassify and publish all documents on Deep State spying, censorship, and corruption
- [Crackdown on government collusion]()   fake news to deliberately weave false narratives and to subvert our government and our democracy
- [Make IGs independent]()                separated from the departments they oversee so they do not become the protectors of the Deep State
- [Intel Agency Monitoring]()             ensure no spying on citizens, disinformation campaigns, spying on campaigns
- [Move bureaucracy out of swamp]()       immediately out of Washington to places filled with patriots who love America
- [Ban Feds working at regulated companies]()      corruption, collusion, coopetition 
- [Amendment for term limits for Congress]()       shatter the deep state and restore government that is controlled by the people and for the people


<br>

### Re-issue my 2020 Executive Order


<br>

### Clean out corrupt actors


<br>

### Reform FISA courts


<br>

### Establish Truth and Reconciliation Commission

<br>


### Crackdown on government collusion


<br> 

### Make IGs independent


<br>

### Intel Agency Monitoring


<br>

### Move bureaucracy out of swamp

<br>


### Ban Feds working at regulated companies


<br>

### Amendment for term limits for Congress


<br>





## Destroy Censorship Cartel 

- [EO Banning Federal Collusion to Censor]
- [EO Ban Federal Funded Labeling Dis/Misinformation]
- [Fire Feds Engaged in Censorship]
- [DOJ Investigate Censorship Regime]
- [Remove BigTech from Censorship]
- [Stop Funding NP/NGOs for Censorship]
- [7 Year Cooling Off period for Fed Jobs with User Data]
- [Digital Bill of Rights]









