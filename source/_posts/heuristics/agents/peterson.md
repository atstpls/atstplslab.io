---
title: peterson
tags: ["analysis"]
category: [reference]

---

<br>

1. To see the world, we must prioritize our perceptions

Relevance realization 

- The amount of information available to you in the world is astronomically vast 

- The amount of information in your long-term memory, especially all the ways they can be combined

- The number of options of potential lines of behavior

- In many different dimensions we experience combinatorial explosion, can't go and check all that information, can't decide if all that's relevant 

Prioritization is different in this case, it's implicit, it's self-organizing, it's unconscious

Perception is in itself an act of implicit perception, it's a multi-dimensional, optimal gripping 


2. Any system of priorities is a structure of values--an ethic 

- an internally consistent hierarchy of values that is iterable and something that's embdied (can be acted out)

- 


3. Ideologies that attempt to remove the burden of responsibility for aiming up, making sacrifices, doing difficult heroic things


- Everything is pointless, all morality is relative, no one's life has any true significance
- I can do whatever I want with whoever I want to without any consequences whatsoever  
- Because in the final analysis, it doesn't really matter. 
- It does matter because that's the pathway to Hell 
- Even if Heaven doesn't exist, Hell matters 
- And you'll figure that out when you get there 




If it's not power that unites us, what is it?  religious systems, political systems, ideological systems try to solve this 

University accepted claim that only story that truly exists is one of power 

Stories of judeo christian is what this country was built on, says that the story is sacrifice 

Work, pleasures of the moment for future self, broader community, maturity, responsibility, 









  Science                           Spirituality 
Ontological                         Epistomologoy 
   Laws                               Narrative


  Too much                             Too much 
Meaning Crisis                      Material Crisis 
    Woke                             War, Disease


Bible is narrative describing a spirit, an embodied pattern 

Scientific knowledge is laws describing natural world 

Embodiment of the narrative is the ultimate reality

<br>

### Embodiment of Pattern 

Logos is embedded in the material prior to its manifestation 

The embodied pattern is not a story, but when you describe it it's a story  

<br>

### Higher Order Value 

Moses spends years developing a hierarchy of values, maps out higher order values 

Jesus targeted with traps/accusations, responds with higher order values 



https://www.youtube.com/watch?v=pzndbpwJtX0




##### JORDAN

Tyrants use fear to get compulsion, tyrants frighten you into compulsion, use crisis to justify compulsion

Leaders in a crisis stay calm and do not use compulsion


Energy prices should be increased   You'll kill people on the margin

Too many people on the planet


ESG serves BlackRock and United Nations

The Woke, at bottom, only do three things, over and over again, in different ways and with different domain-specific jargon. They twist the dialectical ratchet (Aufheben der Kultur) to advance their own narrative, undermine the moral authority of their opponents by making out that they’re complicit in some evil, and drain the epistemic authority (ability to feel like you know what you’re talking about) of those they disagree with. That is, they advance their narrative and make their opponents out to be too evil or stupid to have legitimate disagreements with them. That’s it. Those three things. Over and over again. Join me on this episode of my subscribers-only podcast, James Lindsay OnlySubs, as I outline these three tactics, where they come from, how they work, and what you might be able to do about them.


#### TIME - SHORT-TERM GOALS TO LONG-TERM GOALS

Human behavior has evolved over thousands of years.

People who had to work all day to survive learned to use <a href="" >short-term thinking</a> which prioritizes immediate needs over future needs.

People who worked and produced a surplus learned to use <a href="" >long-term thinking</a> which prioritizes future needs over immediate needs.

The focus changed from immediate needs to iterated needs across vast spans of time.

Consequences of your immediate actions were bounded by the future. 

<br>

#### GROUPS - INDIVIDUAL CONTEXT TO SOCIAL CONTEXT 

As more people lived together in groups, they began to discover optimal ways of operating in large social spaces.

Diverse individuals inhabiting the same space learned to cooperate to reach a goal and maintain the space's integrity. 

Fundamental agreements emerged when everyone played the same game, with the same rules, at the same time.

Consequences of one's immediate actions were now bounded by both the future and by the social context.

<br>

#### 



They also learned to prioritize apex behaviors over base behaviors:

Persistence   over   Giving up 
Honesty       over   Lying
Integrity     over   Deception
Ability       over   Learned Helplessness
Virtue        over   Nihilism
Reciprocity   over   Egocentrism
Courage       over   Cowardice
Humility      over   Conceit
Resilience    over   Fragility

Human flourishing, human prosperity, human well-being


Despair, cowardice, confusion, 

Faith, courage, competence, 


Reciprocal interactions that occur in large social spaces between many different people






There is utility in diversity.  Different problems arise at different times, and different people are required to develop solutions.

Many complex problems are solved by opponent processing. Moving right hand slowly requires left to push against it. Same with positions, politics, chaos and order.

The world is complex. For simple problems, you want a simple, low resolution map.

Challenging belief systems means challenging your map of the world, your status, which regulates your emotion.

Our perception is tied to our intent, which is tied to our values and goals. Gorilla basketball experiment is great example. This is how we simplify the world.

Pre-cognitive screening.  People are dealing with different facts, perceiving the world in different ways.

What you see is a reflection of your value structure. All perception windows to the world are goal directed.



Once in a social environment, our MAPS of needs and gratification switch from individual needs to iterated needs across vast spans of time in a complex social environment 


Winners are those who people want to play multiple games with you, not those that win a single game 

People naturally want to be in the place where future reward beckons in addtion to present rewards 

Concordance of fair play ethos and exploratory ethos 

Good player is not only trying to score the goal, also trying to play with various ways of scoring a goal with your teammates

In that play, you optimize exploration plus reward seeking 

Jack Panks outlines neurocircuitry of play, a separate circuit for play, 

Store up your treasure in heaven and not on Earth where moths can corrupt 

The bat has blood, a form of treasure, it's a finite resource, and hunting is sporadically successful 

Hunting is collective and success is erratic, Even best hunters fail time to time, so what makes you best of all hunters as far as your family is concerned

It would be your skill at distributing the fruits of your hunting among the other hunters so that theyre so thrilled with what a wonderful guy you are 

That every time they hunt, your family gets food 

Store your treasure in your reputation, which is the open book record of your reciprical actions (behaviors) across all hunts 

The more we grow apart, we lose the open book informing, we lose the motivation and effects of reciprocity, 

You use their mental representation of you as a reciprocal player,  if your ethos is generous, sacrificial, long-term oriented, player of multiple reciprocal games 

Protects you against the excigincies of fate, local failure in the food supply, other people take care of you in the worst of situations 

Accruing long-term reciprocal reward by treating others better than they treat you 



#####







[Germany’s Reserve Police Battalion 101](https://www.thepublicdiscourse.com/2018/04/21399/)

[Solzhenitsyn’s Gulag Archipelago](https://www.amazon.com/Gulag-Archipelago-Aleksandr-Solzhenitsyn/dp/1843430851)

[Milgram’s obedience experiment](https://en.wikipedia.org/wiki/Milgram_experiment)


[Augustine](https://www.newadvent.org/fathers/120115.htm)




Transcendentals:   Good, Truth, Beauty 

Realization is always a comparitive judgement.....   Look at this, it's true or it's good 

Appearances distract us, beauty is when appearances disclose reality 

Beauty - notion that God or ultimate reality discloses itself to - 







Moses when snakes are killing, you must gaze at that which is poisoning you.

Beauty can be experience instead of just looks.... older people are beautiful based on things other than looks 




Moses was judge for decades, saw millions of micro-narratives, induction produced ten commandments 

 Alchemy - we will find in material the solution to ill health, death, and privation.  The impulse spawned modern science. Redemption can be found in transformations of the material world

 Galileo says Aristotle is wrong, Plato was right, mathematics is the way to describe reality of things, myths and meaning doesn't have the same 



Franz de Wal - Chimps social hierarchy you see 


----------------


Something must unite our attention and our action, so that we are integrated, psychologically.

Something must unite our interests and endeavours, collectively, so that we can cooperate and compete peacefully, productively, reciprocally, and sustainably.

How then should our identity be conceptualised and embodied, practically and ideally?


### Individual vs State 

A simple conceptualization of human identity:  Opposing poles of sovereign individual and faceless NPC of the state 

In this way the complex internal hierarchy of the person, motivations, emotions, drives, impulses, physiological, physical, biological and chemical subsystems

Are collapsed to the autonomous liberal man, imbued mysteriously with intrinsic rights, and separated from social context 

The broader social context is collapsed: couple, family, neighborhood, workplace, city, province, nation into society, collective, the state 

Those who worship power trumpet the state, those who worship whim elevate the individual 

Individual is viewed in opposition to collective

Whim-seekers see all social bonds as contrary to call of freedom, indistinguishable from oppression 

State-seekers see all individuals as impediment to establishment of utopian collective 







### WORK 

- [Maps of Meaning Lectures on Mythology and Psychology of Religion](https://www.youtube.com/watch?v=bjnvtRgpg6g)
- [UnderstandMyself](http://www.understandmyself.com/)
- [SelfAuthoringSuite](http://selfauthoring.com/)
- [Essay.app](https://essay.app/)
- [Peterson Academy](https://petersonacademy.com/) 
- [Biblical Series on Geneses](https://www.youtube.com/playlist?list=PL22J3VaeABQD_IZs7y60I3lUrrFTzkpat)
- [Jordan B Peterson Podcast](https://www.jordanbpeterson.com/podcast/)
- [Maps of Meaning: The Architecture of Belief](http://amzn.to/2lvBEPe)
- [12 Rules For Life: An Antidote To Chaos](https://www.jordanbpeterson.com/wp-content/uploads/2023/07/001-12-Rules-for-Life-An-Antidote-to-Chaos-1-672x1024-1.jpg)
- [Beyond Order: 12 More Rules For Life](https://amzn.to/3wwuka2)
- [Blog](https://www.jordanbpeterson.com/blog/)
- [Scientific Articles](https://www.researchgate.net/profile/Jordan-Peterson-9)



### DEPRESSION 

Depression is a preponderance of negative emotion which seems self evident

All negative emotions are the manifest of the abstraction of a behavioral system that stops you from moving forward or moves you backwards, or in another variant, prepares you to fight 

All positive emotions are linked to movement forward, curiosity, desire to play, hope, marker you're moving towards valuable goals. 

Humans need to be engaged in something worthwhile

You can be depressed because you're ill or because your life sucks (exercise, social life, goals, education, career, family, hobbies, religion)

Deal with catastrophies of life in a manner that's engaging and productive and interesting instead of storing it up so that it envelopes you completely 

Dopamine is the fundamental chemical that mediates the kind of positive emotion that makes people feel alive and hopeful 

A scalable goal-setting intervention

Exposure Therapy is the use of voluntary confrontation with challenges 

### DARK TETRAD 

Machiavellianism        A tendency to use people as a means to achieve one's own goals 
Narcissism              A belief that one is special, gifted, and superior to others 
Psychopathy             A lack of affective empathy and a willingness to exploit others 
Sadism                  A tendency to derive pleasure from the pain or humiliation of other


### CLUSTER B 

Boderline personality       Difficulty managing emotions, emotional responses, impulsivity, unstable self-image 
Histrionic personality      Pervasive attention-seeking behaviors and exaggerated emotional displays
Antisocial personality      Not respecting authority, following rules, fitting in with cultural norms 
Narcissistic personality    Excessive need for admiration, lack of empathy, inability to handle any criticism, sense of entitlement 

Flattery, Gaslighting, emotional blackmail, love-bombing, seduction to obtain affection or avoid abandonment 


### BIG 5 

Agreeableness       94      Compassion (85) and politeness (96)         Interpersonal interaction, cooperative, considerate
Conscientiousness   75      Industriousness (67) and orderliness (76)   Obligation, attention to detail, hard work, persistence, efficiency, rules 
Extraversion        12      Enthusiasm (13) and assertiveness (18)      Sensitivity to hope, joy, anticipation, social situations 
Neuroticism         0       Withdrawal (1) and volatility (1)           Sensitivity to negative emotion, pain, sadness, defensive anger, fear, anxiety 
Openness            61      Intellect (54) and aesthetics (63)          Creativity, artistic interest, (verbal) intelligence 


                    Me                  Tracey 
Agreeableness       94  (85/96)         98  (96/97)
Conscientiousness   75  (67/76)         13  (4/41)
Extraversion        12  (13/18)         34  (70/12)
Neuroticism         0   (1/1)           98  (94/99)
Openness            61  (54/63)         3   (0/44) 

### PERSONALITY 

Everybody's personality is composed of two higher-order traits. 

The first higher-order trait is known as plasticity, and can be thought of as the tendency to be flexible, exploratory, curious, and quick to adapt. 

The second higher-order trait is known as stability, and can be thought of as the tendency to be structured, organized, emotionally stable and focused.

Plasticity, the first higher-order trait, can be further broken down into two sub-traits: Extraversion (the tendency to be enthusiastic and dominant) and Openness (the tendency to be open-minded and intelligent).

Extraversion (Outgoing vs Reserved)
- Sociable
- Active
- Adventurousness
- Positive
- Excitement-Seeking
- Gregarious
Openness (Original vs Traditional)
- Fantasy-prone
- Aesthetically-minded
- Philosophical
- Creative
- Intuitive
- Intellectual

Stability, the second higher-order trait, can be further broken down into three sub-traits: Conscientiousness (the tendency to be orderly and industrious), Emotional Stability (lack of negative emotional volatility and the tendency to withdraw), and Agreeableness (politeness and compassion, as opposed to belligerence or aggression).

Conscientiousness (Conscientious vs Carefree)
- Competent
- Orderly
- Decisive
- Achievement-oriented
- Self-disciplined
- Deliberate
- Industrious
Emotional Stability (Calm vs Nervous)
- Anxious (reversed)
- Angry (reversed)
- Hostile (reversed)
- Depressed (reversed)
- Self-Conscious (reversed)
- Vulnerable (reversed)
Agreeableness (Agreeable vs Assertive/Aggressive)
- Warm
- Trusting
- Straightforward
- Altruistic
- Modest
- Compliant
- Tender-minded
- Nice

There are advantages and disadvantages to each trait, particularly at the extremes. Extremely sociable, extraverted people can be dominant and impulsive, while introverted, quiet people can easily become isolated and depressed. Extremely open people can be scattered and overwhelmed by their own thoughts and ideas, while closed-minded people may become narrow and inflexible. Exceptionally conscientious people can be obsessive about order, judgemental and rigid, while their more carefree counterparts may be messy, undisciplined and careless. People very high in emotional stability may engage in risky, dangerous behavior, while those who are more neurotic can become so preoccupied by anxiety and pain that they are unable to function. Finally, extremely agreeable people may never stand up for themselves, while those who are too assertive can be aggressive, callous and bullying.

Personality is reasonably stable over the lifespan, and is also powerfully influenced by hereditary or genetic factors. Despite this, personality can broaden or even transform. As people age, for example, they tend to become more agreeable, conscientious and emotionally stable.
Changing personality means changing habits of action, presumption and perception. Personality change requires the formulation of clear future goals, as well as discipline and practice. People who are too agreeable can learn to stand up for themselves. Disorderly people can become more conscientious. Introverted people can become socially skilled. People who experience paralyzing levels of negative emotion can learn to explore.



Ferguson Effect - Police who have low confidence in their trust and authority are less willing to engage in proactive policing and community partnership which leads to increases in crime rates 

