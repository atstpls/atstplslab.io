---
title: john danaher
tags: ["analysis"]
category: [reference]

---

<br>


<b>Jiu Jitsu</b> is about compensating for inequalities in physical attributes between different people.

We use mechanics, technique and tactics to compensate for our physical and mental shortcomings.

### DOUBLE DIRECTION

- Sweeps open up subs, subs open up sweeps
- Neck attacks opens up Go-behind, Go-behinds open up neck attacks
- Body is relaxed for movement but tight when isolation of limb is goal
- Get inside position to isolate limbs, Get outside position to get back
- Keep strong base, attack opponent's base
- Constant pressure, advancing when on top, Kuzushi when on bottom


### PROBLEM SOLVING

- Skills perish, they must be maintained. 
- Most important thing is speed of decision making and problem solving
- Use systems to make decisions for you while your opponent is making decisions consciously 
- Have a proactive goal but be ready to employ reactive counters
- integrate systems of high percentage techniques, move across systems until breakthrough is achieved
- cut down opponent movement options into a few predictable alternatives,
- learn and map out the main reactions, funnel an opponent into progressively fewer options as you work towards finishing positions
- indirect thinking to remove obstacles, disguise true intentions 
- Anxiety and emotion don't help solve problems..condition yourself to maintain calm demeanor required for problem solving activity
- Move away from frames, not towards them, up is preferred option
- Chase the hip, not the head



<br>

### ADVANTAGE

- Impose mechanical advantage
- Train dexterity and control of legs, hips, and feet
- Rehearse finishing mechanics for success against knowledgeable and skilled resistance
- Get the sub at your discretion rather than rushing it while your opponent is escaping
- Develop depth of knowledge and skill in small set of moves, when fight goes there your opponent cannot match your knowledge and skills in that area
- Confidence in your offense rests upon an unshakable belief in your defense, which you get from understanding attacks
- Use Offbalancing to create opportunities, Use High/Hard Fulcrums and Ends of Levers
- Wedges to immobilize opponent 2-on-1 gripping with immovable elbows --- wrist, tricep, underhook
- All offense creates risk, only defense mitigates that risk
- In scrambles, look for three methods always available : KIMURA, FHL, ASH GARAMI
- Make strong first move to get immediate breakthrough or overly defensive reactions that setup other moves
- Be strong in the move from start to finish with step by step control
- Integrate systems of high percentage techniques. As resistance to one grows, switch to another. This creates pressure difficult for even experienced opponents to withstand. Move across sytems attacking the whole body equally until breakthrough is achieved
- Use positional pressure to cause opponent to defend and overextend limbs
- Enter and make contact in an advantageous position
- Create movement that we control better than our opponent. Have a proactive goal, but be ready to employ reactive counters

<br>

### IMPROVEMENT

Ultimately, any movement in the gym it doesn’t improve the skills you already have or build new skills is a waste of time waste of resources 
- Everything you do should be done with the aim and the understanding that this is going to make me better
- transmit knowledge then I have to create training programs with a path from knowledge to polished skill 
- Don't work numbers, work with mechanics
- ideal progression when your drill is zero resistance when you’re fighting competition is 100% resistance
- drills with slightly increasing increments of resistance
- This program will outline a roadmap, what skills you need to take control of your finances, your content, and data THEMES : Four methods stand out from the others, 
- Strong understanding of the precursor skills that make other skills possible METHODS : 
- Organize knowledge into simplified chunks you can remember under the stress of competition---It's not how much you know, but how much you can recall under stress
- Implant knowledge and transform that knowledge into skill through physical training

<br>

SCENARIOS : Have different manifestations (positions where methods can be applied )
- Use three methods in three different scenarios 6 Major/Essential Skills 
- Order of Operations 
- Ideal method for smaller, less flexible budgets 
- Proven method of money flow, make it the centerpiece of trading style
- scenario #1 >> guards, passes, etc >> ezekiel, cross chest to back >> turtle >> sliding What you can realistically learn and apply in 3-6 months 
- Master three areas and you'll have a great fundamental 
- Ignore exotic methods with poor to average percentage of success 
- Methods that will work for any skill level 
- You must become highly skilled at: 
- Simplest, effective moves come first
- heuristic rules and principles ensure that your focus is not endless computations which provides simplifications which enable us to take vast amounts of information and parry it down to a few simple rules that effectively guide our behavior

<br>

### PURPOSE

Why Jiu-Jitsu

1. Prevent being held down
2. Control a person without hurting
3. Slowly escalate if necessary
4. Defend chokes, head/joint locks
5. Train like you fight, hard sparring, go 100% without taking damage
6. Get good at solving dynamic problems under stress
7. Practice finding technical solutions to problems
8. Learn to attack roadblocks and failures
9. Learn humility


<br>

### 


1. HEURISTICS. These are general rules of thumb that guide behavior
2. technical DETAILS. These are situation specific and often difficult to remember under stress. In truth you need both. HEURISTICS WITHOUT DETAILS ARE INEFFECTIVE AND DETAILS WITHOUT HEURISTICS ARE PARALYZING. Make sure your study and general reflections on Jiu jitsu involve both




Kimura - Arm behind (break) or Arm in-front (restrain)

- Gateway to other systems

TKimura - Either top knee (get on top) or bottom knee (back, rear/side triangle, armbar)

Single Leg Defense:

- Stop them from going around, square up and FHL

- If can't, Wizzer and stuff head, go for Kimura


- Maia
- When in HG bottom, get underhook
- From top, use sit through to 3/4 mount or leg weave, or tripod over

FHL
- 3 PoC to keep distance & alignment 
- Defend against WristGrab, ElbowGrab, CircleAround & Tripod
- Create immediate submission threat
- Use reaction to complete a GoBehind, KataGatame, or Takedown 
- Defend Takedown 
- sprawl and FHL
          - High Elbow Guillotine 
          - Low Elbow Guillotine
          - Arm-In Guillotine 
- Stuff head and Kimura 

Ground Bottom 
- Trap Triangle -> triangle, trimura, armbar 

Entries 
- pronated, supinated, over-tie, elbow & bicep, elbow & other wrist

Standing
- collar ties, under hooks, arm drags
- ankle picks, snapdowns, trips, throws
- Single legs, Double legs

Ground
- Half guard bottom
  Undertook —> guillotine 


Main Opportunities 

Transitions -> FHL, Kimura
Ground -> Back 
Ground Bottom -> Legs, Triangle, Armbar 

<br>






<br>

## Layout 


- GuardRetention
    - Double Under Pass
    - Over Under Pass
    - Torreando Pass
    - Knee Slice Pass
    - Leg Drag Pass
- HalfGuard
    - Underhook/Tight Waist Attacks
    - Take back
    - Roll over sweep
    - Second leg Takedowns 
    - Overhook Sweeps
- PinEscapes
    - Mount
    - Side Control
    - Rear Mount
    - Turtle
- SubEscapes
    - Triangle
    - Armbar
    - Kimura
    - Leglock
- ClosedGuard
    - EACL              -> Side Scissor
    - 45 inside         -> TopLock
    - Inside Wrist Pos. -> Trap Triangle 
    - Hand on Floor     -> Clamp Pos.
    - Second Step       -> Overhead Sweep
    - Hips/Knees on Mat -> 4 Sweeps
- OpenGuard
    - Two knees
    - One knee
    - Standing
    - HQ
- Passing
    - Opening Closed Guard 
    - Inside Passing
    - Bodylock Passing
    - Loose Passing 
- Scrambling
    - Kimura
    - Leg Attacks
    - Front Headlocks
- Finishing
    - Back Attacks
    - Triangles
    - Arm Bars
    - Leg Attacks

<br>

