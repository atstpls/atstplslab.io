---
title: ramaswamy
tags: ["analysis"]
category: [reference]

---

<br>

Vivek Ramaswamy is a former Republican presidential candidate in the USA. He is an entrepreneur, political and cultural thought leader, and author. In 2022, he founded Strive Asset Management, an Ohio-based firm, that has a mission to restore the voices of everyday citizens in the American economy by leading companies to focus on excellence over politics.
