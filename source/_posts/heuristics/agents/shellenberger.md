---
title: shellenberger
tags: ["analysis"]
category: [reference]

---

<br>

Michael Shellenberger is an energy expert, a best-selling author, and a leading intellectual of the pro-human environmental movement. He is a co-founder of the Breakthrough Institute and the California Peace Coalition, and founder of Environmental Progress. He has also broken major stories including the 'Twitter Files'. 


- CIA and its allies created the Russian Collusion hoax 
- Organized censorship and content moderation at Twitter/others 
- Election interference with letter from 51 IC officials which falsely claimed Hunter Biden laptop was Russian disinformation 

- FBI persecuted employee whistleblowers 
- FBI inadequate investigation of pipebomb on Jan 6 
- Election interference by encouraging censorship of Hunter Biden laptop story 

- DHS ran multiple operations to suppress and shape information 
- Created public-private partnerships like Stanford Internet Observatory 

- FBI/CIA funding pro-censorship advocacy and advising Brazilian government on censorship 
