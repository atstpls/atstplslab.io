---
title: pageau
tags: ["analysis"]
category: [reference]

---

<br>

Jonathan Pageau is an artist, writer and public speaker. He is a pioneer in the revival of Liturgical Art for the 21st century. Through his podcast, The Symbolic World, he fosters the rediscovery of symbolic thinking and a vision for re-enchantment in the world. 


