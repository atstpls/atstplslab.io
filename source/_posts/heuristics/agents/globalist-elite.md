---
title: globalist-elite
date: 2023-05-20 01:00:42
tags: [resiliency, bitcoin]
category: [ecosystem, resources]
---


<b>BlackRock</b> is an investment firm which owns more stock in major companies than any other organization.

<br>

##### Mechanics

- <b>Larry Fink</b> joined Wall Street in 1976, pioneered idea of debt securitization packaging together loans as bonds
- Ran trading desk for Mortgage Backed Securities that led to 2008 crisis
- Lost 90M due to interest rates, used 5M loan from BlackStone to start Blackrock to focus on risk management and client trust
- Deep political connections, manage 9T worth of assets
- Client buys BR ETF/active fund, receives receipt for % ownership, tracks value and performance of underlying assets
- USG used them to manage toxic assets they took from Bear Stearns
- USG used them to buy corporate bonds to try countering 2020 crisis
- FDIC used them to manage SDC securities portfolio sales in 2023
- Joined Fidelity and others to invest 400M in Circle
- Alladin is to Blackrock what AWS is to Amazon
- They inherit the voting votes and have huge sway in boardrooms, can influence company policy, regulations, corporate governance, etc.

<br>

##### Analysis



##### THE GREAT NARRATIVE FOR A BETTER FUTURE

Centralization of power, we face multiple existential crisis, therefore we need greater global cooperation, which of course will be managed by stakeholders

1. Force all corporations to adopt ESG standards through top-down manipulation of private-public partnership (govts & big business) with NGOs being coordinating entities

2. Transform the youth culture so that youth demands ESG, and will only work for and buy from ESG-compliant companies

3. Rewrite social contract to accept transition from an economy of production and consumption (capitalism) to an economy of caring and sharing (communism)

<br>
