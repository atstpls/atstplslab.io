---
title: fiat
date: 2023-01-17 19:00:42
tags: [money]
category: [ecosystem, resources]
---

<br>

Fiat system is government-run financial institutions managing defaults on gold obligations

<br>

##### Mechanics

- Image showing evolution of money  
- Fed balance sheet is 5 Trillion 
- Analogy - Credit rating and debt to credit ratio is 33%.  You need 20%.  You choose to increase your credit limit to get the rate down to 20%.
- Governments suspended gold convertibility, then issued paper currency that was not redeemable for gold
- Price controls are always counterproductive resulting in surpluses and shortages
- Lower interest rate - makes it easier to get credit 
- QE
- Fiat was designed to manage defaults on gold obligations

<br>


<br>

##### Analysis

Our current financial system works against those trying to attain status through competency:

- We have a broken currency which loses its value over time
- We do not have an effective way to store and sustain wealth
- We do not have true property rights when we own something

<br>

In fiat world, the final layer is government credit. If you're friends with the government you never go bankrupt.

"The problem with government-provided money is that its hardness depends entirely on teh ability of those in charge to not inflate its supply." - bitcoin standard

Governments will always find a reason to print more money It rewards the producers of the monetary good and punishes the savers of the monetary good

Extreme financial instability is in the future. There are still opportunites to hold and grow wealth. What's the right policy we need to get to our economic goals? Inflation below 2% Labor market Monitor financial conditions, not a science, slow down growth DBDEv, about security, market analysis, and offensive techniques

Human civilization has always used hard money which allows us to successfully plan for future.

[Bitcoin](/bitcoin) was designed for user security/privacy, transefer and preservation of wealth



Fiat is systematic separating of actions from consequences, which usurps the reasoning mechanism of the human mind.

Individual ownership requires people understand they control their own health, that through foods they eat they can become strong, that if sick they have tools necessary to heal.

Fiat requires human dependence, authority substituting for individual decision-making, shifting blame instead of encouraging personal responsibility, human life has become monetized.  Free access to information is counterproductive to this goal.

Industrial, Mass-produced sugar and grain-based meal substitutes are metabolically destructive. Revolving cycle of addiction, lethargy, and depression. 


You're basically outsourcing digestion to animals, using them as a giant, external digestive system--an organism optimized for extracting nutrients from low-nutrient-density plants with high amounts of indigestible matter.

"A cow spends its entire day eating and deficating, turning largely indigestible matter into very easily digestible and high-nutrient-density delicious meat."