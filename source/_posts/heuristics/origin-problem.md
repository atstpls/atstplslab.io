---
title: origin problem
date: 2023-01-18 05:00:42
tags: [tools]
category: [ecosystem, concepts]
---

<br>



### ORIGIN PROBLEM

At the origin of anything, there's always a mystery

Can't account for that, but I'll account for everything downstream from it

That's how reality works---in all worlds, at the outset there is a miracle you cannot completely account for in the world it gives birth to

You can't account for the identity with the multiplicity of its parts

You can't account for the origin with the results that it gives

Pick a miracle... I pick Resurrection

Big bang doesn't explain much about why we live the way we live

Why we care about the things we care about, 

Why we have this hierarchy of values, 

Why we sacrifice things

Resurrection explains all of this, everything that makes society function 

All the mysteries about how multiplicity and unity function together 

The world is held by self-emptying

The idea that you do have to sacrifice something, you have to sacrifice yourself 