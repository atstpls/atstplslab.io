---
title: earth roamer
date: 2023-01-17 19:00:42
tags: [money]
category: [ecosystem, resources]
---

<br>

Advanced expedition vehicle 

Carbon fiber body, lithium ion battery bank, all digital electronic controls, dual inverter chargers

#### Interior 

King mattress, can upgrade to California King 

R-25 Insulation 

Charging 120 VAC, USB 

Reading lights, storage, digital controller ( security, engine, charge, water levels, lights, fan, carbon dioxide, smoke alarm, inverter, battery )

Entertainment center, satellite tv, bose surround, xm radio,    

Padded dishrack, induction stove/cookware, silverware, coffeemaker, 

Filtered water system, soap dispenser, deep sink, cutting board, convexion oven, microwave, fridge/freezer (12v)

soap/shampoo dispenser, 330 HP, 750 Torque, 10 speed auto transmission, 29' length, 8' width, max height 12', 

95 gallons diesel, 100 gallons water, 40 gallon grey water, 5 gallon black water, 1320 watts solar power, 12000 watt hour lithium ion battery bank 

6/7L turbo diesel V8, 10,000 lbs towing capacity, 43 inch tires on beadlock wheels, heavy duty shocks and anti-sway bars 

air ride suspension, led off-road lighting package, warn front and rear winches, rated 16,500 pounds synthetic rope 

