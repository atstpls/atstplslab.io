---
title: food
tags: [money, government, currency]
category: [ecosystem, concepts]
---

<br>

##### DON'T LIKE ANIMALS TO SUFFER

Animals raised to produce/be food suffer from:
    - living conditions
    - injuries
    - diseases
    - slaughterhouse

<br>

##### DON'T LIKE RAISING ANIMALS TO BE FOOD

In nature, wild hens:
    - live over 10 years
    - lay 15 eggs per year during breeding season
    - for the purpose of reproduction  

In hatcheries, chickens bred to lay eggs:
    - live under 2 years
    - are genetically manipulated and selectively bred to produce 25x more eggs per year
    - unfertilized eggs, not for the purpose of reproduction 

<br>

##### DON'T LIKE ANIMALS AS MIDDLE MAN

Chickens bred for unnaturally high rate of egg-laying suffer from a number of different diseases, some causing premature death.

Any nutrients you get from a chicken or egg comes from plants.  Animals are just the middle man.

Humans have no biological need to consume eggs, they manipulate and breed chickens to be used for meat and eggs---for convenience and for money.

<br>

damage to our conscience from exploiting sentient beings for food

damage to our resources, both plants and animals from misusing/wasting them

damage to our bodies from outsourcing digestion and prioritizing easy over right

the way we troubleshoot, the way we think through things