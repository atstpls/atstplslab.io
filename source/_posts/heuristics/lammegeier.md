---
title: lammegeier
date: 2023-04-11 10:08:31
tags: [reference]
category: [reference]
---

<br>

|Life|Story|
|-|-|
|There is a constant war between the individual and the collective<br>  &emsp;- Complex hierarchies of both get collapsed into simplified abstractions<br> &emsp;- individuals see the state and other groups as obstacles to freedom<br> &emsp;- state sees individuals as obstacles to a utopian collective|Animals balancing own goals with group dynamics<br> &emsp;- Animals want to play, explore, and build different things<br> &emsp;- Packs have formed around common goals and ideas<br> &emsp;- Own goals conflict with pack goals and vice versa<br> &emsp;- Continuous use of simplistic versions of both animals and packs|
|Individuals fall prey to 2 illusions:<br> &emsp;1. Anarchy is freedom<br> &emsp;2. It doesn't require sacrificing responsibility<br><br>Collectivists fall prey to 2 illusions:<br> &emsp;1. All groups are corrupt versions of ultimate group<br> &emsp;2. Groups have no value and can be destroyed<br>|Anarchist animals embrace freedom at any cost<br> &emsp;- They see packs as restricting freedom<br>&emsp;- They abandon intermediary roles & responsibilities<br><br>Collectivist animals embrace allegience at any cost<br> &emsp;- They see packs as a higher alliegence<br>&emsp;- They expand to occupy intermediary roles & responsibilities|
|Technology accelerates the atomisation of individuals<br> &emsp;- social media platforms exploit individuals<br> &emsp;- censorship, pressure, polarization<br> &emsp;- erosion of intermediary social structures<br>|Animals faced with technology that breaks up packs|
|Absolute State Subsumes Individual Identity<br> &emsp;- claim DEI is the replacement for social identity<br> &emsp;- universalist compassion that devours everything<br> &emsp;- diversity without unity is decomposition<br> &emsp;- nation, gender, family, religion are obstacles to the free individual<br> &emsp;- radical freedom will transform into totalitarian control|Leviathan Pack seeks responsibility for all plants and animals|
|Identity is fractal via unity and multiplicity<br> &emsp;- We're all bound into friendships, families, clubs, churches, sports teams, companies<br> &emsp;- We participate simultaneously in cities, states, and countries<br> &emsp;- Unity comes from sharing a common point of attention, purpose, goals, origin, story<br> &emsp;- To participate in unity is to sacrifice some aspect of multiplicity to its purpose|Animals must make compromises to make packs<br> &emsp;- higher-order purpose serves as judge of lower levels<br> &emsp;- Animals must resolve acting for self or for pack <br> &emsp;- Unite on a desirable and optimally voluntary purpose (telos)|
||Rampant Individualism Drives Collectivist Individualism|<blank>|[Identity: Individual and the State versus the Subsidiary Hierarchy of Heaven](https://static1.squarespace.com/static/6516e3215981fa376a3ea80d/t/653d0f1c1d77eb666a90c11b/1698500422709/The+Subsidiary+Hierarchy+-+Jonathan+Pageau+and+Jordan+Peterson+-+ARC+Research+Paper)|
||Technology Accelerates the Atomisation of Individuals|<blank>|[Identity: Individual and the State versus the Subsidiary Hierarchy of Heaven](https://static1.squarespace.com/static/6516e3215981fa376a3ea80d/t/653d0f1c1d77eb666a90c11b/1698500422709/The+Subsidiary+Hierarchy+-+Jonathan+Pageau+and+Jordan+Peterson+-+ARC+Research+Paper)|
||Identity can and should be fractally interpreted|<blank>|[Identity: Individual and the State versus the Subsidiary Hierarchy of Heaven](https://static1.squarespace.com/static/6516e3215981fa376a3ea80d/t/653d0f1c1d77eb666a90c11b/1698500422709/The+Subsidiary+Hierarchy+-+Jonathan+Pageau+and+Jordan+Peterson+-+ARC+Research+Paper)|
||The Spirit of the Father is the commonality of all manifestations of behavior and perception across all instances of paternal behavior, Rescuing the Father|<iframe width="400" height="220" src="https://player.odycdn.com/v6/streams/0a8549e5b0f43d70f4cb1571f7ebd0b123821515/e5344e.mp4" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|
||Kids act out the Spirit of the Father |<iframe width="400" height="220" src="https://player.odycdn.com/v6/streams/0bc83637d17bbcf01495cef917cbf9e7f5b65840/767704.mp4" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|



