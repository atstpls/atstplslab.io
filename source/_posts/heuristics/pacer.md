---
title: pacer
date: 2023-04-11 09:53:32
tags: [money]
category: [ecosystem, resources]
---

## Heuristics 

[Perception]() is based on [Individual Goals]() which is based on [Individual Values]()
    - Objects/actions can be perceived in infinite ways
    - Our individual goals and their inherent values determine our worldviews using frames 
    - We play mixed-motive games involving cooperation, conflict, and levels of common knowledge 

[Low Resolution Representations]() and [Heuristics]() drive survival and success
    - Human knowledge from experience was shared through stories acting as guides to action 
    - Humans adapted to meaning not objects--the significance of things, not the things themselves 

- [Reciprocal Altruism](#reciprocal-altruism)
    - Human interactions are cooperation regulated by social norms 
    - There are incentives for honesty, trust, and positive reputations 
    - There are disincentives for fake pretense, paltering, deception 

- [Alignment with Reality](#alignment-with-reality)

    - [Competency Hierarchies](#competency-hierarchies)
        - [Government](#government)
        - [Mainstream Media](#mainstream-media)
        - [Medical Establishment](#medical-establishment)
        - [Independent SMEs](#independent-smes)
            - [Jordan Peterson](#jordan-peterson)
            - [Rhonda Patrick](#ronda-patrick)
            - [Josh Hawley](#josh-hawley)
            - [Peter McCullough](#peter-mccullough)
            - [David Goggins](#david-goggins)
            - [John Danaher](#john-danaher)
            - [Ben Shapiro](#ben-shapiro)
            - [Jonathan Pageau](#jonathan-pageau)
            - [Douglas Murray](#douglas-murray)
            - [Riley Gaines](#riley-gaines)
            - [Helen Joyce](#helen-joyce)
            - [Michael Shellenberger](#michael-shellenberger)
            - [Jocko Willink](#jocko-willink)
            - [Rener Gracie](#rener-gracie)
            




https://threadreaderapp.com/thread/1598822959866683394.html
https://threadreaderapp.com/thread/1601007575633305600.html
https://threadreaderapp.com/thread/1601352083617505281.html
https://threadreaderapp.com/thread/1601720455005511680.html
https://threadreaderapp.com/thread/1602364197194432515.html
https://threadreaderapp.com/thread/1603857534737072128.html
https://threadreaderapp.com/thread/1604613292491538432.html
https://threadreaderapp.com/thread/1604871630613753856.html
https://threadreaderapp.com/thread/1605292454261182464.html
https://threadreaderapp.com/thread/1610372352872783872.html
https://threadreaderapp.com/thread/1610394197730725889.html
https://threadreaderapp.com/thread/1611568518612254722.html
https://threadreaderapp.com/thread/1612526697038897167.html
https://threadreaderapp.com/thread/1613589031773769739.html






<br>

#### Hack and Leak Roundtable 

||||
|-|-|-|
|Jessica Ashooh|Director of Policy|Reddit|
|Olga Belogolova|Policy Manager|Facebook|
|John Bennett|Director of Security|Wikimedia Foundation|
|Kevin Collier|Reporter|NBC News|
|Rick Davis|EVP, News Standards/Practices|CNN |
|Nathaniel Gleicher|Head of Cybersecurity Policy|Facebook|
|Garrett Graff |Director, Cyber Initiatives|Aspen Institute|
|Andy Grotto|Director|Stanford Cyber Policy Center|
|Steve Hayes|Co-Founder/Editor|The Dispatch|
|Susan Hennessey|Executive Editor|Lawfare|
|Kelly McBride|Senior VP|Poynter Institute|
|David McCraw|VP/General Counsel|New York Times|
|Ellen Nakashima|National Security Reporter|Washington Post|
|Evan Osnos|Staff Writer|The New Yorker|
|Donie O'Sullivan|Reporter|CNN|
|Dina Temple Raston|Investigations Correspondent|NPR|
|Yoel Roth|Head of Site Integrity|Twitter|
|Alan Rusbridger|Oversight Board|Facebook|
|David Sanger|Chief Washington Correspondent|New York Times |
|Noah Shachtman|Editor in Chief|Daily Beast|
|Vivian Schiller|Executive Director|Aspen Institute|
|Claire Wardle|Cofounder/Director|First Draft News |
|Clement Wolf | Public Policy/Information Integrity|Google|
|Janine Zacharia|Visiting Lecturer|Stanford|

<br>



- [Angle of Alignment](#angle-of-alignment)
- [Long AoA Timeframe](#long-aoa-timeframe)
- [Low Moe Methodology](#low-moe-methodology)
    - [Sensory Systems](#sensory-systems)
    - [Mathematical](#mathematical)
    - [Behavioral Patterns](#behavioral-patterns)
    - [High Competency SMEs](#high-competency-smes)
- [Construct Validation](#construct-validation)

<br>



1. LOW ANGLE - The fundamental driver to survival and success is one's angle of alignment (AoA) with reality

2. LONG DURATION - Survival/success are long-term games. The best players maintain low angles over long durations.

3. LOW MOE METHODS - The best methods for verifying authenticity have low margins of error.

                    - Two types:  Solo or Outsource verification 

    a. Solo 
                Disqualifying Logic/Reason : "They're not gonna stop... and they should not."

                Mathematical : Mesa County Image 

                Behavioral Patterns :  (Mainstream media, Social Media Platforms, Medical establishment, Biden admin) 
                                       - GOV bypassed vaccination process, collusion with Pfizer, MSM, SMPs 
                                       - FBI/DHS stonewalling at hearings, multiple times caught 
                                       - Sex change, mutilation, puberty blockers, SEL, woke propaganda 
                                       - Wrong reports by FDA, WHO, climate change fear mongering (175000 europeans di from extreme heat )

    d. Outsource 
            
                High competency SMEs:  
                
                            * P

                                PATRIOTTV for 10% off prescription medical emergency kit 
                                
    
                                (Ben Shapiro vs Candace Owens)
                               Eliminate private health insurance
                               control red meat consumption
                               eliminate fossil fuel jobs
                               Put government in charge of food prices 

4. CONTENT/CONSTRUCT VALIDATION - 





CENTRAL PROBLEMS

- COMPLEXITY. Tactical problems. Modern environments produce enormous amounts of information and endless potential scenarios
- COMPETITION. Specialists. Threat hunters must compete against specialists who are more knowledgable, and more-experienced with specific tools and techniques
- TRANSPARENCY. Games of incomplete information. There will always be unknowns and unavailable information. But even the information that is available is not guaranteed to be ACCURATE or AUTHENTIC

Observing conditions and actions is the most direct, most trusted way for threat hunters to find and track those attempting to operate within their environments

CONTENT VALIDATION happens when we use our senses to identify an apple. If it looks like an apple is expected to look...Then we conclude that it's an apple
CONSTRUCT VALIDATION happens when we use multiple distinct measurement techniques. Like our 5 senses. If different techniques converge on the same analysis, our confidence increases
The more techniques that report the same thing. The more confident we are in our conclusions

PACERS also work to substantiate their conclusions using ABSTRACTION


BOB DYLAN EFFECT - condenses a 1K page novel into 5-6 verses.  Uses lines that get the listener to generate parts of the story and fill in blanks 



Deception and self-serving behavior provide diminishing returns

Truth and reciprocal altruism provide compounding returns  


|Certification|Issued|Expires|
|-|-|-|
|[GIAC Security Essentials Certification (GSEC)](https://www.credly.com/badges/4164623e-c611-4cfc-a85b-4acb3e6dcda1)|5/30/14|10/31/28| 
|[GIAC Certified Incident Handler (GCIH)](https://www.credly.com/badges/ee104e0f-1d8a-4a7b-9f43-b62c416e9ec5)|6/20/14|10/31/28|
|[GIAC Certified Intrusion Analyst (GCIA)](https://www.credly.com/badges/fe017150-f8c7-4bbf-88ad-47b8dc51bc34)|7/21/14|10/31/28|
|[GIAC Security Leadership (GSLC)(https://www.credly.com/badges/185e773b-82f3-4cfc-8ae6-984da09de7f8)]|7/23/14|10/31/28|
|[GIAC Assessing and Auditing Wireless Networks (GAWN)](https://www.credly.com/badges/f9b890ed-edeb-40c5-ae52-e9ff194b6306)|10/6/14|10/31/28|
|[GIAC Certified Perimeter Protection Analyst (GPPA) - Retired](https://www.credly.com/badges/c2d887c4-1a46-4e28-9846-a67ac97e279b)|12/17/14|10/31/28|
|[GIAC Certified Enterprise Defender (GCED)](https://www.credly.com/badges/95436036-dea8-454e-a766-ec744227e947)|12/22/14|10/31/28|
|[GIAC Systems and Network Auditor (GSNA)](https://www.credly.com/badges/490c6566-7eb6-4c45-9b3f-7c117f4ead4a)|1/14/15|10/31/28|
|[GIAC Reverse Engineering Malware (GREM)](https://www.credly.com/badges/761abca8-4cd3-4f25-aaa7-5298a6cbde3a)|1/28/15|10/31/28|
|[GIAC Certified Forensic Analyst (GCFA)](https://www.credly.com/badges/f124c67c-ab1a-41d9-9400-787c320ee8ba)|3/10/15|10/31/28|
|[GIAC Security Professional (GSP)](https://www.credly.com/badges/75add03a-578d-490f-867f-d23e05ab6f6d)|4/24/23|8/31/28|
|[GIAC Experienced Cybersecurity Specialist Certification (GX-CS)](https://www.credly.com/badges/d4adf809-f44a-4ffc-b100-ad434e65f817)|4/24/23|8/31/28|
|[GIAC Experienced Intrusion Analyst Certification (GX-IA)](https://www.credly.com/badges/fc32cb75-a8cc-49c8-babe-5e7b03e1b13c)|4/24/23|8/31/28|
|[GIAC Experienced Incident Handler Certification (GX-IH)](https://www.credly.com/badges/9b7766d8-0003-488c-b171-4cd3e860cb64)|4/24/23|8/31/28|
|[GIAC Cyber Threat Intelligence (GCTI)](https://www.credly.com/badges/8ab03ffd-c2e9-4403-bdb3-347f2fecad5a)|8/7/24|8/31/28|
|[GIAC Security Expert (GSE)](https://www.credly.com/badges/9801d51a-07b1-438c-a548-6c3383476a7d)|9/12/16|8/31/28|
|[GIAC Advisory Board](https://www.credly.com/badges/e3c6a6f4-d4ca-48e3-9272-46aa2c9692cb)|5/30/14|8/31/28|
|[Offensive Security Wireless Professional (OSWP)](https://www.credly.com/badges/36d5af56-51fd-4ecf-8717-a79fbd623b70)|1/28/19|-|
|[Offensive Security Certified Professional (OSCP)](https://www.credly.com/badges/122c359c-9470-4a42-b691-85e27dc25b64)|7/12/17|-|


