---
title: cbdc
date: 2023-01-17 13:30:42
tags: [money]
category: [ecosystem, resources]
---

<br>
  
CBDCs are Central Bank Digital Currencies, regulated currencies issued by central banks

<br>

##### Mechanics

Bahamas was first country to launch CBDC in October 2020. Dcash is the digital currency issued by Eastern Caribean Central Bank (ECCB) and it is used by 7/8 of the countries under the EC Currency Union (ECCU)

China owns full scale CBDC, Thailand and Malaysia have pilot programs. Nigeria largest country in Africa, first country in region to launch CBDC in 2022.  They also banned Bitcoin and put withdrawal limits on ban accounts, attempting to convert deposits into the CBDC.

Only 0.35 percent of Nigerians adopted the government-mandated CBDC. 105 countries researching CBDC make up 95 percent of global GDP.

<br>

##### Analysis

Canada froze bank accounts of freedom protesters

Australia arrested people for social media posts

Dissidents in China are abused and killed

