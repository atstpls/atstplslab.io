---
title: advantage
date: 2023-04-07 12:56:24
tags: [resiliency]
category: [ecosystem, concepts]
---

<br>

||||
|-|-|-|
|<h5>knowledge</h5>| <b> [nä-lij] </b>  |    acquaintance with facts, truths, or principles, as from study or investigation |
|<h5>skill</h5>| <b> [skil] </b>  |    ability to do something, coming from one's knowledge, practice, aptitude, etc. |
|<h5>tactics</h5>| <b> [tak-tiks] </b>  |   procedures or set of maneuvers engaged in to achieve an end, an aim, or a goal |
|<h5>campaign</h5>| <b> [kam-peyn] </b>  |   a connected series of operations designed to achieve a specific objective|

<br>

##### Knowledge Development

Simplify crypto, digital assets, and personal finance
Establish a basic understanding of blockchains and dApps

Jordan Peterson
Professor, Author

<br>

##### Skill Development

Learn and practice essential techniques for managing and protecting digital assets, social media content, and private data

Bruce Lee
Martial Artist

<br>

##### Tactics Development

Apply knowledge and skill to build the simplest, most effective methods for operating in the crypto space
Bruce Lee spent years perfecting his craft, gaining the most effective skills, practicing and learning from his adversaries

Competency and tactical proficiency

John Danaher
Jiu-Jitsu Coach

<br>

##### Campaign Development

David Goggins
SEAL, Ultra Athlete
Known for his unstoppable and unique mindset.

