---
title: negate
date: 2023-04-07 13:58:48
tags: [resiliency]
category: [ecosystem, concepts]
--- 



```mermaid
flowchart LR
  
  subgraph BU [ BUILD UP ]
    YO[ Yourself and Others ]
    SAE[ Support and Encourage ]

  end
  
  subgraph FF [ FORGIVE FORGET ]
    YRO[ Yourself and Others ]
    AAA[ Armor and Appreciation ]
  end
  
  subgraph A [ ABSTRACTION and CATEGORIZATION ]
    LTP[ Low Time Preference ]
    IND[ Inductive Reasoning ]
  end

  subgraph L [ LEAD OTHERS ]
    BTE[ Be the example ]
    WTW[ Walk the walk ]
  end

  subgraph O [ OWN IT END IT ]
    TB[ Take the Blame ]
    PS[ Problem Solve ]
  end

  subgraph IN [ INVESTIGATIVE NETWORKS ]
    VAUTH( Verify Authenticity )
  end

  subgraph CE [ CONTEXT EVALUATION ]
    VAP( Verify all Perspectives )
  end

  subgraph PT [ PATTERNS TRENDS ]
    HI( History of Individual )
    CB( Cultural Behavior )
  end

  subgraph ION [ INDIVIDUAL ONTOLOGICAL NAVIGATION ]
    PT( Personality Types )
    TT( Truth Types )
  end



  IN ---> CE 
  CE ---> PT 
  PT ---> ION

  BU ---> FF
  FF ---> A 
  A ---> L 
  L ---> O 

```


List of doubts that must be proven unnecessary

|doubt|action needed|
|-|-|
|She's unsure you will fast for Lent|Prove that fasting for Lent is important|
|She's unsure you will love her forever|Prove you will love her forever|
|She's not sure you don't put her down for pleasure|Prove you never try to put her down and hate it when you do|
|She's not sure you if you think she's stupid|Prove you think she's smart|
|She's not sure you know what you're doing|Prove you have thought things out and understand the situation|
|She's unsure about our financial situation|Prove you have financial situation worked out|


List of intangibles to prioritize based on valuations from others

|intangible|action|
|-|-|
|Christmas activities|drive around looking at Christmas lights|
|School activities|school events, gatherings, projects|
|Truck location|Park truck in the driveway|
|Bedtime|Go to bed before midnight|
|AC on vacation|Leave it on|
|Don't like not having a vehicle|Buy the vehicle ASAP|


Hierarchy of competency

Pick fields where your intelligence matches/exceeds

Consider the complexity and cognitive abilities required


<a style="font-weight:bold">Behaviors</a> are the actions and patterns of actions you perform.


Very useful as heuristics.

Example. There are many people that may offer to help your kid.

It's impossible to tell them ahead of time which ones are dangerous and which ones are safe.

But a behavior can be used as a reliable indicator.

1. Attempting to prevent child from going to his parents

2. Attempting to convince child to keep secrets from his parents

3. Attempting to convince child his parents are lying, untrustworthy, angry, disappointed, etc. 

