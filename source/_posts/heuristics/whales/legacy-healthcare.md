---
title: legacy healthcare
date: 2023-06-08 11:38:11
description: Some description.
category: [reference]
---

<br>

Healthcare costs are outpacing inflation by 200 perc

<br>

##### Mechanics

- human capital
- average child is obese
- 6/10 adults have atleast one chronic illness, 17 medications prescribed every year
- we're not our healthieset, we're not our happiest, we're not at our best
- Spend way too much mental overhead on reading labels, research, sourcing

- metabolic and health issues continue to increase
- food and pharma companies fund thousands of studies to create confusion
- 50K nutrition studies in last two years, NIH spends 1/11 of big food/pharma

- Our tools are being weaponized to make it impossible to single out clear cause.
- Cigarette companies fund every possible study that makes it unclear that a link between cigarettes and lung cancer exists
- Americans are sicker, fatter, more depressed than ever before
- Amount of processed foods, amount of environmental toxins we're exposed to, massive health crisis right now

<br>

##### Analysis

- billions on Food subsidies on corn, soy, and wheat
- This artificially brings the price down at food/pharma are getting these
- real sugar to HFCS, soybean oil and canola oil, 
- Nixon began push to shelf stability, sustain food supply
- system has bad incentives that create bad outcomes
- tobacco get 4x more govt subsidies than all fruit and vegetable crops combined
- 80 perc of subsidies go towards corn, soy, wheat   under 1 perc go to fruit veg

- Food has been weaponized, doritos, salt sugar fat, engineered palatable food
- Not only cheap ingredients, engineered to be addictive
- More money for marketing, R&D, cycle continues
- weaponizing evolutionary senses to want more toxic addictive ultra-processed food
- For heart disease, we prescribe statins
- depression ssris
- pharma solution for everything, but core metrics we use to measure health continue to go up
- ozempic - now pharma looking at weekly injections you take for life, if you stop taking weight comes back, 40 perc of benefits come by muscle mass loss and bone density loss
- obesity is a symptom of metabolic disfunction
- using a drug to address a symptom will not address root cause of chronic disease
- will not address other symptoms like inflammation, infertility, mental health, and other issues related to toxins in our food system
