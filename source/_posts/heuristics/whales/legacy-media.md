---
title: media ecology
date: 2023-01-14 19:00:42
tags: [analysis, media]
category: [ecosystem, actors]
---
<br>

Every major media outlet targets the same audience.

<br>

##### Mechanics

Why does big media constantly create drama and spread it?

Because they are facing extinction--paying for news now is ludicrous.  Subscription-based models are dead, ask @theathletic

Ad revenue is now the only way they can profit.  All they need to do is just get you to CLICK.

And so big media has a very unusual business-customer relationship:

1. The customers do not pay for the product.

2. Ad companies pay the business in exchange for information about its customers.

3. The business uses polarizing rhetoric and partisan reporting to lure customers

<br>

<b>Big 5</b>

|||
|-|-|
|comcast|nbc,universal pictures, cnbc, msnb, usa, bravo, dreamworks|
|disney|abc, espn, natgeo, fx a&e, lefetime,marvel, lucasfilm,pixar,20thcen
|fox|fox news, fs1, fs2, wsj, ny post, barrons, smartmoney, harpercollins|
|warnerbros|cnn,hbo,cw,cinemax,cartoon,nbatv,tbs,dc comics,newline|
|paramount|mtv,nick,vh1,bet,comedy,miramax, paramount,cbs|

<br>

fake news categories:
    - propaganda
    - clickbait
    - sponsored content
    - satire and hoax
    - error
    - partisan content
    - conspiracy theory
    - pseudoscience
    - misinformation
    - bogus content 

<br>

- Media used to be able to count on subscriptions 
- New York Times bought subscription-based theAthletic and realized it doesnt make money 
- NYT forced ads
- TV                       vs         Internet 
- Limited time/cost                   Forever/free to viewer 
- phemeral, viewer memory            Viewer knowledgable, can look up everything                           
- arrative                           Logos 

<br>

Different socialogical perspectives 

functionalist view          =>  entertainment & collective experience
                                enforcer of social norms
                                agent of socialization
                                provides a standardized view of society
                                shows what society rewards and punishes
                                promotes consumer culture 
                                
conflict perspective        =>  media reflects, portrays, exacerbates divisions 
                                gatekeeping small # controlling what is presented
                                perpetuates narrow perspectives, promotes bias 
                                mass media reflects the dominant (ideal) ideology
                                actively limits other views, stereotypes 

interactionist view         =>  how media changes our day-to-day behavior 


- Labeling fake news is also a problem ( First Draft - fake news effort )
- Google owned by Alphabet Eric Schmit, campaign advisor and multimillion donor for Hillary 
- Media matters convinced Facebook to get onboard 
- powerful interests manipulate your opinion 

1. shaping and censor opinions rather than report them 

2. same stories, same narratives, same sources, same phrases 

3. media literacy is the new thing, trying to be the official expert, but they aren't 

<br>

- First Draft and other organizations are trying to define it 
- Everything is optimized for maximum viewers--the content (controversy, scandals), the personalities (anchors, analysts), and the format (docu-torial, staged disagreement).
- Journalism is meant to bear witness and hold power to account. Technology has enabled more partnerships which has brought more conflicts of interest. And so journalism has suffered.
- Media outlets are facing extinction--paying for news now is ludicrous. Subscription-based models are dead, ask @theathleticAd revenue is now the only way they can profit. All they need to do is just get you to CLICK.
- public broadcasting as broadcasting that is “made, financed and controlled by the public, for the public. It is neither commercial nor state-owned, free from political interference and pressure from commercial forces.”
- Deliver verifiable information and informed comment in the public interest8Ethical journalism requires tranaparency and accountability.
- We each are unique individuals with a wide array of traits and talents. Our differences determine our memberships in groups (ethnicity, race, sex).
- The more groups argue/fight, the more big media profits.Groups aren't the problem, it's that we fail to think individually.
- mainstream media willingly perpetuates partisan and polarizing information, in an attempt to drive traffic and social engagement.Rhetoric, polarizing views and vitriolic opinions spread.Distrustful public spending more time in homophilous networks
- viral conspiracy theories can be, spread by those looking to divide us even further or profit from our fears. So both the media and the platforms that control so much of our information ecosystem face a reckoning that was long overdue. We are seeing that play out in real time
- Parties who benefit from tribalism:
- Companies selling a product (Nike, Powerade)
- Social media platorms (Twitter, Facebook)
- Media outlets broadcasting lies (TheAthletic, Washington Post)

<br>

The digital age has disrupted the media ecosystem and opened up our access to information, in equal measures. Solving for a robust information ecosystem void of disinformation, hate, and polarization is the problem of our lifetime. Even the other great challenges we face, which are exceptional, from climate change to pandemics to failing trust in democratic institutions will fall prey to distrust and chaos without a strong media ecosystem.

At a time when we are drowning or thriving in a deluge of information (depending on your perspective), when sources of information have proliferated at an extraordinary pace, and when trust in news is being lost, an index of the newsmakers and their owners and funders seems necessary – a rethinking of what is gained and lost in this remarkable moment in time, irrefutable.

Our information ecosystem in the U.S. is no longer limited to an intensely competitive traditional media landscape that has long claimed to self-regulate one another to prevent government intervention over the news landscape and ensure a free press. Now the ecosystem is being run between traditional media and the titans of tech, who have created the greatest ad machine ever built. Tech titans have focused on building tools and infrastructure for the proliferation of user-generated content rather than fund strong and robust newsrooms that tend the garden of our civic information ecosystem. The digital information stream today is not only home to our traditional newsrooms but rather, populated and driven by bloggers on Reddit and columnists on Substack or Medium, inputs into our global history moderated on Google's Wikipedia with over 5 billion views a month, along with Facebook groups, user-generated Tweets, TikTok videos, LinkedIn posts, and cable news punditry that is often unchecked for legitimacy. It is chaos or liberation, pending your perspective. Our nascent project will explore the revenue models, social trust networks, media ownership, and power in today's media to bring forth useful solutions to this enormous challenge. We are living in an age where the fourth estate, long a pillar in our democracy, struggles to find sustainable revenue models that enable it to tend to our civic garden of information and we are left to ask how do we rebuild trust in our information ecosystem? smartphone displaying COVID news article on screen
