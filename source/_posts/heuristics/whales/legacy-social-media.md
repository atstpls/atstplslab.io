---
title: social media
date: 2023-01-14 18:00:42
tags: [analysis]
category: [ecosystem, actors]
---

<br>

Impressions: The total number of times a user has seen your name or tweet

Reach: The number of users who saw an impression of your post within their timeline

<br>

SOCIAL MEDIA TACTICS & TECHNIQUES

Communication - all the procedures by which one mind can influence and affect another    tone, timing, quality, quantity, behavior, action/inaction of message 

Audience Interprets message using identity

Communication is complex, unfolds over time, constant, symbolic, polysemic (messages carry multiple meanings, can be interpreted differently)

Interpreters - interpret based on perception, attitudes, beliefs, self-concept

<br>

A lie's value is in how well it feeds into people's existing prejudices.

Logical fallacies - common errors in reasoning, defects that weaken an argument 

hegemony - dominance by ideology, process of legitimizing and maintaining norms 

gatekeepers control flow of information ?  Socioeconomic class in control ?

average person has access to collective human knowledge.
Required to filter out junk 

utopia is an impractical scheme for social improvement, an imaginary place of ideal perfection in laws, government and social conditions

Tweetdeck = search only verified accounts, look for verifiable images 

Tabloid report - Do you sit there and get all your info from that ?
               - Be active not passive, use trusted sources to do active research 
               - bum tells you your wheels need to be aligned
               - Get more info from him or talk to your friend who is a mechanic?
               - 

<br>

TACTICS

Framing
    - Narrative - how media presents framing for specific interpretations 
    - Shapes priority, emphasis, sets baseline on future reporting 
    - Impacts interpreter emotion, attitude, decisions 


Priming
    - Activating cognitive learning structures to influence judgement process
    - Wording is used to illicit maximum emotion, sensationalism 
    - 

Sensationalism
    - focusing on the things that cause emotion 
    - most deadly, plaguing the nation, etc.

Agenda setting

Denial
    - rejecting blame 
Evasion of responsibility 
    - providing alternate explanations 
    - - Blame shifting, Defensibility, Accident, Good Intentions, Victimization
    - a) provocation, (b) defeasibility, (c) accident, or (d) good intentions.
    - 
Reducing offensiveness 
    - accepts some measure of responsibility, but offers reasons that would lessen the impact on their reputation.
    -  (a) bolstering the image of the communicator to lessen the impact of the harmful action; (b) minimization of the incident; (c) differentiation to contrast the specific act with even more serious transgressions; (d) transcendence, in which the specific act is placed in a separate light; (e) attacking the accuser; and (f) offering some form of compensation for the perceived harm caused by the communicator’s actions.
    -   

Corrective action 
    - the communicator promising steps that will correct the problem
    - Compensation, Mortification, Stonewalling, Journalistic norms, Statement or update, Call to accountability

Mortification
    - communicator expresses disappointment in his or her own actions or thoughts and seeks forgiveness

Self Presentation 
    - individual account 

Value-differentiation
    - organizational rhetorical device as an attempt to restore organizational value violated by a crisis

Stonewalling
    - withholding information, differentiated from silence and passiveness, without denial by managing the information released to internal and external publics In essence, the public figure who practices stonewalling can control the communication environment and stalls any additional questioning by external publics

substragegies:
 denial, shifting blame, provocation, defeasibility, accident, good intention, bolstering,
minimization, differentiation, transcendence, attacking accuser, resolve problem, prevent recurrence, admit wrongdoing, and
apologize

<br>

Social Media

partisan reporting by news outlets

polarizing rhetoric by media outlets

vitriolic opinions by citizens

The news cycle had more LEAD, less REACH, MORE CONTROL, and was more UNDERSTOOD.

2006 - 2016 exposure was reverse-chronological timeline based on accounts followed

2016 - PRESENT exposure is driven by algorithms based on users’ presumed interests and tastes

We're not seeking out the needed context for things. When you don't do it, someone else will.

Facebook has become the richest publisher in history by replacing editors with algorithms. They've made millions, probably billions doing this. - We create monetary systems and try to control them but we can't. - We damage our environment and try to manage it but we can't. - We cripple ecosystems and try to repair them but we can't. - We are the problem.


Key things that have changed:

- Interpreters in the past could explain controls for message authenticity and integrity

- Now most interpreters can't explain how the Internet and social media works or the controls that have been put in place to maintain integrity.

- Undermining credibility of news stories was more difficult back the

- There is more competition, more media/source partnerships

- More tasks are outsourced

- Less lead time for reflection, message development and verification

- Large demand for ongoing gossip, rumor, opinion

- More eyewitness accounts and visual content emerging from incidents

- Interpreters actively promoting, adding to, and helping to frame messages

- Interpreters now promote their own world views and spend more time in echo chambers

- Interpreters have near unlimited access to collective human knowledge

- Interpreters seek out facts that reinforce partisan ideologies and refute opposing viewpoints

- Interpreters value information that fits their world view more than information that is true

- Interpreters can follow stories they like with likes and hashtags

There has been an erosion of trust in journalism and mainstream media organizations

Coordinated Inauthentic Behavior

- doesnt matter what content is, if you're trying to mislead others about who you are or where you come from, it's CIB

Facebook and Twitter—the two platforms that are most transparent about the political disinformation networks they uncover—increasingly attribute operations not to governments, but to firms like Smaat, even though there are often strong suspicions that the firms are working at the behest of state actors.

Like Smaat, many of these digital marketing firms are headed by individuals with one foot in the media marketing world and one in the government. For example, in 2019, Facebook suspended a network of accounts linked to a firm called New Waves based in Egypt—which hosts many such firms. New Waves’s founder, Amr Hussein, is a former Egyptian army officer and previously worked at Al Bawaba News, a private pro-government Egyptian newspaper. He currently calls himself a social media consultant