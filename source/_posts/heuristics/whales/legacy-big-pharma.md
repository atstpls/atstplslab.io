---
title: legacy big pharma
date: 2023-11-16 20:00:42
tags: [concepts, training, debt]
category: [ecosystem, actors]
---

<br>

<b>Big pharma prioritizes money over public health.</b>  They use advertising to impact the news, lobby for protective legislation, and buy off news networks to influence debate.

<br>

##### Mechanics

- Pharmaceutical companies are the <b>largest source of revenue for nearly every mainstream media outlet</b>
- Drug is in question.  Pharma need government to commit to spending hundreds of billions to subsidize the drug as the first line of defense against obesity
- Media run major pieces with big pharm talking points saying obesity isn't in our control and we should prescribe drug to everyone including children
- Define obesity as <b>lifetime disease that must be managed so government will fund drug</b>
- Why not investigate cause of obesity?  Ultra-processed food our government has subsidized
- USDA panel on nutrition, <b>95 of members are paid for by big pharma</b>, recommend sugar, sugary products
- Clear policies like cutting subsidies and urgent stories on true cause of obesity would help

<br>

##### Analysis 

- 80% of deaths and 90% of healthcare costs in US are tied to preventable and reversible lifestyle diseases
- Heart disease, diabetes, kidney disease, many forms of cancer and Alzheimer's are essentially food-borne illnesses and would be wiped out with simple public policies to unpoison our food supply
- <b>Big pharma is exploiting sickness and mainstream media is playing along</b>
- Problem is not access to drugs, it's too many drugs from a system that is incentivized for us to get sick