---
title: legacy finance tools
date: 2023-06-08 11:38:11
description: Some description.
category: [reference]
---

<br>

|||
|-|-|
|credit default swap (CDS)|agreement that the seller of the CDS will compensate the buyer in the event of a debt default (by the debtor) or other credit event. That is, the seller of the CDS insures the buyer against some reference asset defaulting. The buyer of the CDS makes a series of payments (the CDS "fee" or "spread") to the seller and, in exchange, may expect to receive a payoff if the asset defaults.|
|US Dollars|Allows us to make valid exchanges:<br> - without having physical possession of currency<br> - without being in the same physical location as the payee<br> - without knowing/trusting the payee<br> - safekeep our money to reduce the risk of theft, loss, destruction, etc.<br>||
|Stocks|Corporations issue stocks (shares) to raise funds to operate their businesses<br>The shareholder buys a piece of the corporation or a claim to part of its assets and earnings<br>Shareholders do not own company assets and usually receive nothing after a bankruptcy<br>Shareholders get voting rights and sometimes dividends<br></br>||
|Bonds|Debt instruments used by govt/corps to raise capital<br> - Govt/corp issues bond, lender lends money, borrower repays full amount plus interest<br> - A bond with 1K face value, 4% coupon, 20 years maturity:<br> - Borrower pays lender 40 a year for 20 years, then full 1,000 at 20 year mark<br>Coupon rate can be variable and raise with interest rates<br>With stock you own equity, with bonds you only get the principal and interest<br></br>| - credit/default: The creditworthiness of the borrower<br> - interest rate: As interest rate rises, existing bond prices fall (because of their now lower interest rate)<br> - inflation: As money loses value, so does principal and interest<br> - Zombie companies pay more in debt servicing costs than they make in profits<br>  - When interest rates rise, some will default<br> |
|Gold|virtually impossible to destroy and impossible to create from other materials<br>can be mined and dumped on the market, inflating<br>can be stolen by randoms or siezed regimes/govts<br>can be kept from moving from one place to another<br>can be taxed<br>can be sold in warrants, derivatives without being held ( hypothecation )<br>can not easily be lent or rented<br>can not easily verified<br>can not be be used to generate yield in excess of maintenance<br>can not easily be used to start a business, buy other assets<br>final settlement would take weeks<br>|Gold supply increases 1-2% per year<br>Gold controllers can print more notes than there are gold<br>|
|Bitcoin|A global asset and store of value<br>Decentralization combined with cryptography makes it most secure network in world<br>can not increase in supply<br>can not be kept from moving<br>can not be seized (true custody when done right)<br>is a universal market<br>can be easily and instantly converted to other assets<br>can serve as a long duration safehaven store of value<br>can liquidate millions in any currency in seconds<br>can economically empower everyone in the world<br>final settlement takes minutes<br>can mine using electricity from anywhere (isolated energy plant with satellite internet)<br>actually incentivizes this... miners can't profit on normal electric grids<br>|

<br>

Bonds are debt instruments used by governments and corporations to raise capital. Bonds are bought with the intent of the borrower repaying the full amount plus interest.

<br>

|||
|-|-|
|Corporate|Debt securities issued by private and public corporations|
|Investment-grade|Have a higher credit rating, implying less credit risk, than high-yield corporate bonds|
|High-yield|Have a lower credit rating, implying higher credit risk, than investment-grade bonds and, therefore, offer higher interest rates in return for the increased risk|
|Municipal|Debt securities issued by states, cities, counties and other government entities.<br>Types of “munis” include:<br>General obligation bonds - not secured by any assets, backed by faith and credit of the issuer (taxes)<br>Revenue bonds - backed by revenues from a specific project or source, such as highway tolls or lease fees<br>Conduit bonds - bonds on behalf of private entities such as non-profit colleges or hospitals. These “conduit” borrowers typically agree to repay the issuer, who pays the interest and principal on the bonds. If the conduit borrower fails to make a payment, the issuer usually is not required to pay the bondholders.|
|U.S. Treasuries|Carry faith and credit of the U.S. Government<br>Types of U.S. Treasury debt include:<br>Treasury Bills - Short-term securities maturing in a few days to 52 weeks<br>Notes - Longer-term securities maturing within ten years<br>Bonds - Long-term securities that typically mature in 30 years and pay interest every six months<br>Treasury Inflation-Protected Securities (TIPS) - principal is adjusted based on changes in the Consumer Price Index. TIPS pay interest every six months and are issued with maturities of five, ten, and 30 years.|

<br>


Corporations issue stocks (shares) to raise funds to operate their businesses. The shareholder buys a piece of the corporation or a claim to part of its assets and earnings. Stocks are issued to raise capital. Shareholders do not own company assets, usually receive nothing after a bankruptcy Stocks are shares of companies. Securities. Shareholders get voting rights and sometimes dividends.

Value investing - buying something that is more valuable than its current price
Growth investing - buying something that has future earning capability
Momentum investing - buying something hot and riding the wave
Dollar Cost Averaging -


<b>Debt</b> is something, usually money, owed by one party to another.  It must be paid back, typically with added interest.

<br>

|Type|Description|
|-|-|
|Household|Mortgages, credit cards, auto loans, student loans, personal loans|
|Business/Corporation|Bank loans, bond debt, private debt|
|Financial sector|Liabilities of bank and non-bank institutions|

<br>


When you purchase a house, you agree to pay dollars every month for an asset over the next 15/30 years.

In general, it's more beneficial to borrow depreciating assets as they will have decreased value upon repayment.  Borrowing appreciating assets leads to the assets having an increased value upon repayment.

<br>

##### Borrow soaps, not oaks

<b>Soaps</b> are depreciating assets that loses value over time

<b>Oaks</b> are appreciating assets that gain value over time


