---
title: legacy education
date: 2023-05-20 01:00:42
tags: [resiliency, bitcoin]
category: [ecosystem, resources]
---

<br>

Our kids are targeted with social emotional learning and critical consciousness raising.

brain-washing, Maoist style thought-reform through social emotional learning, crtical consciousness raising, culturally-relevant education, comprehensive sexuality education

being trained to be activists for left-wing causes, being trained through equity lens, failing to learn math, reading, science in an adequate way

<br>

##### Mechanics

Hijacks the lesson and injects political literacy, generative approach, swap out actual learning for activist 

2nd grade word problem hijacked

Johnny riding in car with Mom and Dad on his way to amusement park.  50 miles to get there, they've already traveled 30.  How much further?

Raise hands if you've ever been to an amusement park? ( generative theme, emotionally exciting/interesting/relevant )

Oh some of you have and some of you haven't... what are some reasons why some have and some haven't  ( discussion pivots )

Teacher instructed to press on issue until  "Not everyone can afford it"   Why are some people poor and some people rich?  ( discussion pivots )

Should 3 people drive a car or use a bus?  Which one is more pollution?

Johnny has Mom and Dad?  Who here has one parent, two Moms, two Dads?

Economics and socialism discussion, environmental issues, gender identity, race, parental authority, feminism, sexuality