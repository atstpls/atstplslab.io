---
title: legacy banks
date: 2023-01-16 20:00:42
tags: [concepts, training, debt]
category: [ecosystem, actors]
---

<br>

Banks create money when they lend. Banks give money to borrower while keeping it available to the depositor which increases money supply.

<br>

##### Mechanics

When interest rates drop, more people borrow, more money is created. When interest rates rise, less people borrow, less money is created.

Consumption and capital investment are inextricably linked. The interest rate regulates this relationship. People demand investments, interest rate rises, people are incentivized to save. People demand saving, interest rate lowers, people are incentivized to invest in more technological advanced methods of production.

Savings and loanable funds is linked.  Money saved in banks is the money available to lend. Central banks end up making more loanable funds than savings causing artificially low interest rates.

When consumers save less, there is less capital available for investors.  Printing money doesn't increase capital stock, it just devalues the currency. 
Unsound money hides the problem because money is made available to borrowers still.

Sound money would immediately reveal the shortage in captial available for borrowers which would raise interest rates, which reduces demand for loans and raises supply of savings until the two match.

<br>

##### Analysis

When central banks lower interest rate
- banks lend more, creating more money
- reduce amount of savings available
- increase quantity demanded by borrowers
- direct borrowed capital to projects that will fail 

The more unsound the money, the easier it is to manipulate, the more severe business cycles will be

Printing money 
- Central bank creates monetary reserves by buying treasuries
- It then sends the funds to the commercial banks and pays them interest to hold it
- Banks can then make loans with that money, up to the reserve requirement limit
- So, if the Fed issues 1 billion in reserves to a bank, then at a normal reserve ratio of 10 perc, the bank can lend 900 million to borrowers
- These borrowers will deposit those funds back into the banking system
- Down the line, people paid with the loaned money will also deposit funds they receive
- Now this money can be loaned out, in an amount up to the reserve limit
- So, if that 900 million is deposited and then loaned out, an additional 810 million may be deposited
- Ultimately, through this money multiplier effect, the 1 billion in reserves can turn into 10 billion in new credit money in the economy
- pensions are screwed
- you can't control how they are invested
- you can't control how they are funded
- the ones who can control it use strategies that are outdated
- Every time banks loan funds to consumers and businesses they create new money
- That loaned money, in turn, gets deposited back into the banking system where it gets loaned again, creating more new money Interest rates have a direct effect on consumer behavior, impacting several facets of everyday life. When rates go down, borrowing becomes cheaper, making large purchases on credit more affordable, such as home mortgages, auto loans, and credit card expenses. When rates go up, borrowing is more expensive, putting a damper on consumption. Higher rates, however, do benefit savers who get more favorable interest on deposit accounts. When Interest Rate Goes Down - Easier to borrow, but less rates on savings - Bonds fall but stocks go up When Interest Rate Goes Up - More expensive to borrow, savings rates go up - Stocks fall but bonds go up
- The FED buys Treasury bonds in the market and deposits the money into the reserves of banks. 
- FED pays the banks interest to hold the reserves, but banks loan most of it out Each loan creates even more money 
- Printing money allows very wealthy to earn 20,30,40  perc off their hedge funds every year 10 perc inflation every year allows the government to suck value out of savings account without raising taxes
- Shutdowns kill small businesses, large corporations (Amazon,Starbucks) reap benefits The fed funds rate is an important tool used by the Fed to influence other interest rates and affect the money supply. For instance, by lowering the rate, banks follow suit and lower the rates they charge on products such as consumer loans and credit cards. Due to the severity of the COVID-19 pandemic and its negative effect on economic activity, in March 2020, the Fed Board reduced to zero the reserve requirement ratio banks must use. This eliminated the reserve requirement for all depository institutions.8 Sovereign debt is issued by a country's government to borrow money. Sovereign debt is also known as government debt, public debt, and national debt Governments borrow for a variety of reasons, from financing public investments to boosting employment The level of sovereign debt and its interest rates will also reflect the saving preferences of a country's businesses and residents, as well as the demand from foreign investors Treasury bonds (T-bonds) are government debt securities issued by the U.S. Federal government that have maturities greater than 20 years. T-bonds earn periodic interest until maturity, at which point the owner is also paid a par amount equal to the principal Treasury bonds are part of the larger category of U.S. sovereign debt known collectively as treasuries, which are typically regarded as virtually risk-free since they are backed by the U.S. government's ability to tax its citizens.
- Only diversification is non-sovereign stores of value that derive their value based on something other than their ability to generate cash flows 
- Prices drive consumption and production decisions, signals and incentive used to manage it
- Interest rate manipulation destroys incentive for capital accumulation.
- Central bank distorts prices and more projects are undertaken
- Investors miscalculate capital goods that are available and projects are not completed wasting capital
- Suspension of these projects creates unemployment



