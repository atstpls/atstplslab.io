---
title: flag football
date: 2023-01-18 03:00:42
tags: [sports]
category: [ecosystem, concepts] 
---
<br>

##### Commanders Call

We made the Commander's Call that you probably heard us use last game.  It's TEAMWORK, HARD WORK, and LET'S GO DOLA!!  The teamwork represents knowing your job on the team, the hard work represents practicing your job on the team, and the last is a salute to my all-time favorite NFL player Danny "Playoffs" Amendola who earned his nickname by making big plays when his team needed them the most.

We're excited to get back out on the field tomorrow night, hopefully they start on time and we can play our game and get back home.  I can tell you our team won't be making the game any longer than it has to be, the way we've been setting up on the line we're looking to get some more free plays again this game!!

##### Last Play

There may have been a few heads spinning after the game ended.  Why didn't the quarterback take a knee to run out the clock?  Why use the last play to throw a long pass and risk an interception that could have been run back for a game-tying touchdown?  

Because it's about the kids.  All the kids.

On the Cowboys team were several players that played for us last year, but every week it's our friends, neighbors, and classmates that are playing on the other side.  These kids are also trying to learn and get better, they want to make big plays for their family and friends watching, and they want to have fun this season too.  Why wouldn't we give them a chance to make a spectacular, end-of-the-game play as well?   Because we want to be 2-1 instead of 1-2 ?   Nah, we've got our eyes on a bigger prize.

As our players were setting up to run that last play, I heard what sounded like the mother of a Cowboys player telling her boy, "This is it, son, let's go. You know what to do."  I smiled because these are the kinds of moments we hope to create, where our kids begin to learn that they can do amazing things, even when they seem near impossible.  It was against all odds and our defense had worked so hard to keep the lead, but half of me was hoping this kid would somehow find a way to come through and make that big play that his team needed.

As for us, all of our receivers were more than happy to go long, looking to make one final big catch to finish the game.  As fate would have it, Sebastian, who we had been trying to pass to all night, was the first to break free, shooting across the middle of the field and catching a 20 something yard pass, pulling off what had to be the single most exciting play of the night---racing to the endzone as the buzzer sounded to end the game!!  What a big time play!!!  LET'S GO DOLAAAA!!!!

<br>

##### Execution

I told our players after the game, there was one single thing that separated us from them tonight. Both teams had fast players, both teams had guys who could catch, both teams had great defenders.

That one thing was execution. They were able to execute their plays as designed and we weren't.  
There were so many close plays that could have gone either way... but it was their guys that stepped up and made it happen.  I saw three receptions up close where a Commanders player was absolutely smothering his receiver and somehow that player found a way to catch the ball.  There were some big time plays... it was just the other team making them this time.

Our athletes played hard and I'm proud of them.  But we did have things that weren't as good as they could have been.  And I'm glad we learned about them now while there is still half the season left to get them straightened out.

When we do, we will be a very difficult matchup for the Colts.  

I told the team last night, all the plays the Colts use depend heavily on the speed, explosiveness, and determination of their receivers.  The QB throws a well-placed pass and the receiver, guarded or not, gets to the ball before his defender. It only works if you have very fast and explosive players which they do, and the Colts used it to great effect as we saw tonight.

But as Conor McGregor said, "Precision beats power, and timing beats speed."  All of our plays are coordinated team efforts based on dilemmas and misdirection. When we execute them with the right timing and precision, our opponent's speed and explosiveness becomes a disadvantage rather than an advantage.

A defender may be the fastest runner in the league or even the best jumper... but if he is tricked into moving in the opposite direction of the receiver he's covering, for those few seconds, his speed and explosiveness actually helps us get more separation!

This was only the second game using our new plays, and we were only able to get our timing right a few times, but when we did it was clear that we had completely fooled part or all of the defense and were able to create big opportunities for our receivers.  

Over time, the tactics we are using will be no match for those that are dependent on the physical abilities of a few key players. But only if we practice and fine-tune our fakes, routes and plays until they are smooth and precise and can be carried out quickly and efficiently.

When we can execute our game plan as designed, no team will be able to hang with us!

We’ll be out of town for the weekend so I didn’t schedule a practice for tomorrow morning, but we do have the field (2B South) reserved from 9-10:30 am so if anyone wants to use it, feel free!

We will be back for the Tuesday night practice at 5-7:30 pm. We will see everyone there if you can make it!

<br>

 
##### SKILLS

- HOLDING       >       Hold the ball with, fingers make a V, 5 points of contact
- ONE CUT       >       Beats defenders PLAYING THE ANGLE
- JUMP CUT      >       Beats defenders OVERCOMMITTING


##### DRILLS

- 4 CONE DRILL          1-CUT, 1-CUT, 1-CUT, JUMP CUT

- LADDER DRILL          ICKY SHUFFLE ( 2 feet in, 1 foot out

- CARIOCA               - 1 foot in (crossover), 2 feet out

<br>
  
##### SCENARIOS

- 1v1 and runner drags defender to sideline dilemma

<br>

##### SYSTEMS OF HPTs

- Throughout history, the smartest way (playing to your strengths while avoiding harm ) usually prevails         ( we specialize and become excellent at a few high percentage plays )

- SYSTEMS (and Heuristics) make decisions for you while your opponent is making decisions consciously            ( fake routes, fake moves, guarding receivers )
  
- HIGH PERCENTAGE TECHNIQUES are methods that are tested and proven to work against skilled opposition           ( hesi, back to defender pivot, jump cuts, crossovers  )

<br>

##### DILEMMAS/TRILEMMAS

- Good atheletes create problems for their opponents, great athletes create DILEMMAS/TRILEMMAS
  
- Every problem has a solution. Dilemmas/trilemmas don't have solutions, only bad outcomes that force you to choose the least damaging one      ( a receiver getting behind his defender )
  
- Use in combinations that go in opposite directions so that the more an opponent invests in defending one, the more vulnerable he becomes to the other  ( defending shallow vs deep )

<br>

##### OFFENSIVE ASYMMETRY

- Off-balancing, positional pressure, change of speed, feints, fakes... This is how we will disrupt and undermine our opponent's ATHLETICISM

- EXAMPLE: Plays where only one/two of our guys sprints, and ALL of their guys sprint ( reverse run, then at opposite sideline sharp cut back again to opposite sideline )

- EXAMPLE: Plays where opponent's speed/explosiveness works against them ( pump fake the hook pass, or draw in the blitz on purpose to open space for your receiver )

<br>

##### EXECUTION

- A team cannot defend everything at the same time. If defenses are spread out, opponent won't be able to stop a determined attack anywhere.

- So our 4 plays should target 4 different areas/things:   1) run  2) pass short   3) pass long   4) pass across

                                                      OR:  1) short left   2) deep left  3) short right  4) deep right 

                                                      OR:  1) our best receiver 2) their worst defender 3) our best runner 4) our best runner at passing


- Trick opponent into believing play is to one area to draw defenses and redirect to another which is now undefended
  
- Each play present a STRONG INITIAL THREAT to get either immediate breakthrough or overly defensive reactions that effectively setup other plays

- One or two plays should be guaranteed to work from the 5-yard and 10-yard out for extra point/first down/overtime scenarios

<br>
 
In the past we've worked more on routes and man-to-man defense, but we believe there are three big skills that have the biggest impact on the outcome of games:

CUTS - the ability to change direction quickly

CATCHING - receiving the ball with soft hands/fingertips

CRABRIDES - the name we give to "riding" the receiver you're guarding, maintaining inside position, and guiding him to the sidelines whenever possible

Other teams in this league will depend heavily on the speed, explosiveness, and determination of their receivers.  The QB throws a decent pass and the receiver, guarded or not, gets to the ball before his defender. This only works if you have very fast and explosive players which every team does as we saw in the evaluations.

But as Conor McGregor said, "Precision beats power, and timing beats speed."  As the season goes on, our plays will be designed more as coordinated team efforts based on dilemmas and misdirection. When we execute them with the right timing and precision, our opponent's speed and explosiveness becomes a disadvantage rather than an advantage.

A defender may be the fastest runner in the league or even the best jumper... but if he is tricked into moving in the opposite direction of the receiver he's covering, for those few seconds, his speed and explosiveness actually helps us get more separation!

When we get our timing right, we’ll regularly fool part or all of the defense, and this will result in big opportunities for our receivers.  

Over time, these tactics will be no match for those that are dependent on the physical abilities of a few key players. But only if we practice and fine-tune our fakes, routes and plays until they are smooth and precise and can be carried out quickly and efficiently.

Games are a critical part of this learning process.  To be able to play at their best, the athletes must become comfortable in all types of game scenarios, most importantly when we need to get to a specific point on the field and we only have one play to do it. Fourth downs are great for this, that's one reason we never punt.  

Another reason is that season wins and losses do not mean anything in this league.  Every team makes the playoffs regardless of their record.  The catch though is that the teams that regularly punt will miss out on anywhere from 10-30 opportunities to run their plays in a game setting.  Every season we have 10-15 big plays that would not have happened had we elected to give the ball up in exchange for better field position.

This means we may lose some games we could have won. Kids this age may not understand this at first, but I've used these two examples and they seem to help.

First, I ask them who is the 2021 Super Bowl Champs?  They all know it is the Rams.  Then I ask who beat the Rams in Week 9 that season?  No one knows.  Week 10?  Week 11?  No one knows because no one cares.  I had to look it up but was impressed to see that they lost those 3 weeks and still became the champs.

Even more impressive was that two of the teams they beat in the playoffs had beat them in the regular season. Cardinals beat them once, 49ers beat them twice.  The Rams understood that the season is for building, improving, and preparing for the Playoffs.  They learned from their regular season games, fixed what they needed to fix, and came ready to beat every team they had to beat in order to be Champions.

Another example I've used is to imagine the team has decided to build a boat.  We take whatever materials are available--cardboard, wood, rope, glue--and make them into a boat that can hold us and float on the water.  We start testing it out every day by trying to cross a small lake with it. Each time we spot things we can improve--a leak here, a hole there, or the cardboard keeps falling apart lol... You can see where this is going.

Some days we make it across the lake and some days we don't, but that's not what's important.  The goal is not crossing the lake, it's building the best boat we can build for the Playoffs.  And I'm telling you, this team we have here is in the process of building a warship!

Sports are a time for fun and achievement and we continuously work to create a positive experience for every single kid out there.  This is my 3rd season coaching flag football but I'm quickly discovering that this sport may be one of the best ways to learn lessons that some people spend a lifetime failing to understand:

- Hard work pays off regardless of whether you win anything

- Real satisfaction comes from the everyday experience of investing time and effort and moving towards your goals

- Kids who aren't afraid to lose learn more, have more fun, improve faster, and perform better

Throughout the season, I will do my best to make sure every athlete understands that they don't need to prove anything to others, they only need to prove to themselves that they can do anything.

