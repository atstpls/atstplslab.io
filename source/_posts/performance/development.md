---
title: result
date: 2023-04-07 12:56:24
tags: [resiliency]
category: [ecosystem, concepts]
---

<br>

||||
|-|-|-|
|<h5>knowledge</h5>| <b> [nä-lij] </b>  |    acquaintance with facts, truths, or principles, as from study or investigation |
|<h5>skill</h5>| <b> [skil] </b>  |    ability to do something, coming from one's knowledge, practice, aptitude, etc. |
|<h5>tactics</h5>| <b> [tak-tiks] </b>  |   procedures or set of maneuvers engaged in to achieve an end, an aim, or a goal |
|<h5>campaign</h5>| <b> [kam-peyn] </b>  |   a connected series of operations designed to achieve a specific objective|

<br>

##### Knowledge Development

Simplify crypto, digital assets, and personal finance
Establish a basic understanding of blockchains and dApps

Jordan Peterson
Professor, Author


Focus is the gateway to all aspects of thinking: perception, memory, learning, reasoning, problem solving, and decision making. Clear focus and the ability to stay concentrated is one of the most powerful skills a person can possess.

<br>

##### Skill Development

Mosty people think skill is something that magically forms over time-- you train long enough and the skills will come.

It's half true... it's impossible to practice for a long time without picking up skills along the way.

However, you will do much better in skill acquisition, to start the quest for skill with knowledge 

If you know what the right thing to do is from the start, it takes a lot less time to build that into a skill 

Skill is a knowledge that has been hardwired into the movements and reactions of your body.  Knowledge and skill are linked but not the same.

Seek knowleddge, then embody that knowledge through intelligently crafted physical training program to turn that mental knowledge into physical skill.

Learn and practice essential techniques for managing and protecting digital assets, social media content, and private data

Bruce Lee
Martial Artist


Operate with the spirit of incremental improvement over time via discipline in training schedule, extreme attention to detail in technique, endless experimentation in response to emerging problems in training, refusal to get distracted by other demands on attention and sustain motivation levels over time throughout an exhausting training program.

High levels of self confidence is a byproduct of technical training program and the success you have with it in the gym.

<br>



- Example with Ty of blocking guy out on OX 

- Mason as captain, future teams 

- Rebound game 2v2 while others shoot 






##### Tactics Development

Apply knowledge and skill to build the simplest, most effective methods for operating in the crypto space
Bruce Lee spent years perfecting his craft, gaining the most effective skills, practicing and learning from his adversaries

Competency and tactical proficiency

John Danaher
Jiu-Jitsu Coach

<br>

##### Campaign Development

David Goggins
SEAL, Ultra Athlete
Known for his unstoppable and unique mindset.

