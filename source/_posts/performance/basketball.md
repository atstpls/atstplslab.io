---
title: basketball
date: 2023-01-18 03:00:42
tags: [sports]
category: [ecosystem, concepts] 
---
<br>

<iframe id="odysee-iframe" style="width:60%; aspect-ratio:16 / 9;" src="https://odysee.com/$/embed/villain-vs-warriors:3?r=B6f1mjUy7ezh82xyUa4Q5pLkEiipwm5K&autoplay=true" allowfullscreen></iframe>

<iframe id="odysee-iframe" style="width:60%; aspect-ratio:16 / 9;" src="https://odysee.com/$/embed/villain-vs-warriors:3?r=B6f1mjUy7ezh82xyUa4Q5pLkEiipwm5K&autoplay=true" allowfullscreen></iframe>










12
The fundamental skills we're working are:

1)  One-handed passes - they're twice as fast and work over longer distances

2)  Two-handed dribbling - ability to dribble both hands well is a game changer

3)  Three step lay-ups - this is the number one scoring method in this league.  We need to practice doing it and practice defending it.


Our OFFENSIVE positions:

GUARDS - these (2) players, one on each side, are tasked with setting up our plays

WINGS  - (AKA Forwards, Corners) - these (2) players, one on each side, set up in the corner to make baseline cuts or set picks.

ORCA   - The ORCA acts as a last line of defense, like a sweeper in soccer.  His job will be to stay close to the center circle (middle of court) to gather up any loose balls, help out guards that lose their dribble, contain dribblers and/or intercept passes on opponent fast-breaks, and to set unexpected picks for the 2 guards


Our DEFENSIVE positions:

BADGER (1)  - pressures the main ball-handler
VIPERS ? (2)  - one on each side ready to anticipate and intercept the first pass
KODIAKS (2) - one on each side of the rim, ready to contest any passes/shots in the paint



Our offensive plays:

1)  Pick and Roll - The best and most reliable method for creating open space and mismatches

2)  Cuts ( baseline and 45 ) - Passing to a player on the move towards the basket

The idea is to run plays from the outside, keep the middle of the court as open as possible, to create driving lanes and 1v1, 2v1 match-ups for scoring.



Our defensive plays:

1)  Badger defense - a defense designed to pressure and punish dribblers who can only dribble with their strong hand

    We'll use this in every game to pressure ball-handlers forcing them to dribble to their weak side or make a rushed pass.  Then we'll try to capitalize with a fast-break.


Our audibles:

1)  "2K"  -  this means you need a pick set or a pick has been set for you

2)  "TORA"  -  this means a player lost his dribble and needs a quick pass/pass-back



For those that have played with us before, you already know we like to keep a fast pace the entire game.  We usually have large rosters on these teams and we've found that the more we speed up the game, the more opportunities our players have to make plays out on the court.  

An added bonus is that our team will become more and more accustomed to working in offensive cycles this way, which in turn puts our opponents in a defensive mindset---good for us.  The same thing tends to happen with physical (and mental) stamina---we stay stronger and sharper later in the game, and our opponents are more likely to fade and check out...

Keep in mind, this is a "long game" strategy... we all know that hard work pays off, and we also know that it hardly ever pays off in the beginning, more towards the end right? ha ha... The plan is to train our players to be able to work together to beat teams that are primarily dependent on the physical abilities of a few key players.

To do that, it's essential that we get plenty of practice making mistakes, working our way out of bad-to-worse situations, getting leads, losing leads, making comebacks, and at times taking big losses and using them to re-calibrate and get better...  We need all of these skills in order to be a complete, functional team.  

There are other "short game" strategies that focus on acquiring maximum goals and maximum wins.  I hear it's really fun and I should try it sometime.  But I won't.  Because in every single season of every sport I've coached, it's clear that nothing is more exciting and more satisfying for players (as well as for the fans) than making unexpected, big-time, spectacular plays that their team needs---and their family and friends get to watch.  

So this is the main focus of our efforts this season... teaching teamwork, teaching hard work, and practicing the skills and tactics that will put our players in the best positions to make big plays. Everything we are doing is designed to get us there. It's fun working to get there and it's fun when get there and all the work pays off.

This is the ultimate reward of team sports---the satisfaction that comes from the everyday experience of investing time and effort---individually and with a team----that moves athletes toward goals, and helps them prove to themselves that they can do anything.

Here are two videos I made for my team last year, I plan on making a few more this season.  No pressure for anyone to watch----It's pretty much the same things I tell them in practice.  I just like making videos and thought it might help to see some of
these concepts in action.   


https://odysee.com/@fortysixandtwo:0/game-within-the-game

https://odysee.com/@fortysixandtwo:0/Bear-Vs-Puma


### DRIBBLING MOVES 


Head on 


Side Skip Hesi 
Switch Hands Hesi 
Hesi from Stationary 
Normal Hesi 
BTL Hesi 
Under Legs Hesi 
Lateral Glide Hesi 
Cross Lateral Glide Hesi 

Delayed Slow BTL 
Slow BTL Counter Cross 
BTL Jab 
BTL Jab Cross 
Delayed BTB 
Hesi Cross 
Long Hang Cross 
Double Cross 
Push Out Cross 
In Out Cross 
Cross Jab Cross 



SIDE On 

Hip Swivel Hesi 
Hip Swivel Cross 
Closed Stance Explosion 
Switch Hands CSE 
