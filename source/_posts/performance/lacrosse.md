---
title: lacrosse
date: 2023-01-18 03:00:42
tags: [sports]
category: [ecosystem, concepts] 
---

<br>

Fundamentals:
- Beat a defender one-on-One
- Throw a perfect pass to a teammate
- Catch a ball in traffic 
- Score on a goalie 

Terminology
- Center Face-off Circle
- Dot or X 
- Parallel Lines - parallel to sideboards touching Face-off Circle 
- Restraining Line 
- Offensive Zone
- Defensive Zone 
- Imaginary Center Line 
- Imaginary Three Lanes 
- Crease 
- Net or Goal
- Left, Right Creaseman
- Left, Right Cornerman
- Stick-side, Non-stick-side 
- Right-shot, Left-shot 
- Off-ball side, strong-side, weak-side 

After you pass, move 

Loose balls - Run to where the ball will end up, sprint there
stay low, wide legs, block out opponent, if you can't then play opponent's stick side, slap his stick up right before he scoops

Offensive Techniques

OUTSIDE CUTS 
- Undercut                  - Pull stick across body and pass around defender 
- Fake shot to Undercut     - Fake the shot first 
- Sidestep Fake shot into an outside spin around 
- Outside Spin Around       - Spin from outside 
- Inside Body Fake Move     - Fake step to inside 
- Outside Pivot Move        - Push hard for outside, foot between defenders and spin to inside 

INSIDE CUTS
- Fake shot to Inside Slide  
- Fake shot with sidestep (drag) down side into inside slide 
- Inside Slide or Bull      - Turn body sideways to protect ball 
- Inside Spin Around        - Spin from inside 
- Outside Body Fake         - Fake step to outside 
- Inside Pivot              - Push hard across to inside, foot between defenders and spin to outside 


OFFENSIVE PLAYER MUSTS
- Have a good shot fake 
- Be a scoring threat first, passing threat second 
- Keep wide stance when being checked
- Try to start offensive moves from the outside instead of the middle 
- Head up and watching all the time
- Protecting stick and ball all the time 
- Keeps moving, doesn't hold the ball too long 
- If first move doesn't work, look for off-ball pass 
- Goes one-on-one when defender is off-balance, out of position, tired, over-aggressive, over-commits, or mismatch 


Backdoor Cut - when defender overguards the inside cut 





65      Matthew Foster        6th
23      Christian Grashot     6th  
41      Aaron Barkley         6th
1       Dean Morgan           6th
12      Sebastian Morton      6th
54      Asher Redekopp        6th
20      Reed Baumgartner      6th
72      Owen Parvin           6th
28      Derrick Johnson       6th
15      Dominic Vitrano       6th
50      Damon Neutz           6th
17      James Stettler        6th 

40      DJ Bourdon            5th
22      Dominic Staples       5th
37      Hayes Cowan           5th
24      Holt Cowan            5th
38      Brayden Maynes        6th
12      Jackson Thornbury     4th


phone  

ECS - CP, SOG missed deep 
ECS - 3 CP, 1 GB,   COL 1 CT, SOG, G (Dean)


|A|Assists|The player who passes the ball to the player who scores a goal is credited with an assist.|
|BkS|Blocked Shots|Player prevents the ball from reaching the goal.|
|C|Clear|A team passes the offensive restraining line and is clearly able to get an offensive attempt.|
|CF|Failed Clear|A team loses possession before getting an offensive attempt.
(1) If team is man-down an unsuccessful clear is not charged. (2) A failed clear also counts as a turnover.|
|CT|Caused Turnovers|Defense gains control of the ball after contact with the ball, crosse or player.|
|C%|Clear Percentage|Percentage of successful clears. Formula: (C)/(C+CF)|
|DC|Draw Controls|Player successfully gains control of the ball after a draw.|
|FO|Faceoffs|Total Faceoffs|
|FOL|Faceoffs Lost|Number of Faceoffs Lost|
|FOW|Faceoffs Won|Number of Faceoffs Won|
|FO%|Faceoff Percentage|Percentage of draws that are won.|
|G|Goals|A goal is scored when the player puts the ball in the opposing team's net.|
|GB|Ground Balls|A loose ball picked up with the crosse from the ground.|
|GB/G|Ground Balls Per Game|Average number of Ground Balls Per Game|
|GP|Games Played|The number of games played by a player.|
|GWG|Game Winning Goals|Goals that leave a team one goal ahead of the opponent.|
|Min|Minutes Played|The amount of time a player is on the field.|
|Pct|Shooting Percentage|The number of goals scored as a percentage of shots taken.|
|PF|Personal Fouls|Personal fouls can include cross-checking, slashing or any kind of rough play that the umpire thinks is unduly rough or extreme. A Personal Foul can result in the offending player being sent off the field for between 1 to 3 minutes and the ball being given to the opposing team.|
|PIM|Penalty Minutes|The total time a player receives for infractions.|
|PPG|Power Play Goals|Player scores a goal while one or more opposing players serves a penalty.|
|Pts|Points|A player gets one point for each goal scored and one point for each assist.Formula: (G+A)|
|Pts/G|Points Per Game|Number of Points Scored Per Game|
|SHG|Short Handed Goals|Goals scored while a man down due to penalties.|
|SOG|Shots On Goal|A ball thrown with the cross at the goal with the intent of scoring.|
|T/O|Turnovers|Player gives the ball to the other team.|
|TF|Technical Fouls|Technical Fouls are not the most serious type of foul in Lacrosse. Personal Fouls are more serious and have harsher penalties. Technical Fouls include: Handling the Ball, Holding, Illegal Action with the Crosse, Illegal Pick, Illegal Procedure, Interference, Kicking an Opponent's Crosse, Offside, Pushing, Stalling and Withholding the Ball From Play.|
|+/-|Plus-Minus|A plus is given to a player who is on the field when his team scores an even-strength or shorthanded goal, while a minus is given to players on the field when opponents score in those situations. The difference is the plus-minus rating.|
|GA|Goals Allowed|A goalkeeper's statistic referring to the number of scores he has allowed.|
|GAA|Goals Allowed Average|Refers to the number of goals allowed per standard game time.Formula: (GA * (Standard Game Minutes)) / (Min)|
|GP|Games Played|The number of games played by a goalie.|
|L|Losses|The number of defeats by a particular goalkeeper.|
|Min|Minutes Played|The number of minutes the goalie played during the game.|
|PIM|Penalty Minutes|The total time a player receives for infractions.|
|Saves|Saves|When a goalkeeper stops a shot on goal.|
|SHO|Shutouts|Refers to the number of games in which a goalkeeper does not allow a goal.|
|SV%|Percent Saved|Refers to the number of goals allowed per shots faced by a goalkeeper.|
|W|Wins|The number of victories by a particular goalkeeper.|