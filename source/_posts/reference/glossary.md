---
title: glossary
date: 2023-04-08 11:38:11
description: Some description.
category: [reference]
---

|term|definition|
|-|-|
|Ideology|A shared set of beliefs and ideas about what is right and what is wrong|
|Interactive media|Interactive media allows the user/consumer to take an active involvement in the media text, even by contributing to it|
|Mode of address|Involves the style and tone of a media message’s presentation; not so much what is being said but the way in which it is said. Formal/informal, direct/indirect are examples of modes of address|
|Narrative|The way in which a story or a sequence of events is put together. Narrative organises chains of events telling us why, when and where things are happening. A simple narrative structure is equilibrium, disruption, recognition of disruption, attempts to restore equilibrium, new equilibrium|
|Oligopoly|A market that is dominated by a few companies that control the supply of the products or services. There is very little competition within an oligopoly and the companies tend to cooperate with each other by keeping prices high|
|Prejudice|Preconceived ideas or opinions that have no basis in reason or evidence. Some stereotypes and representations are prejudiced|
|Pressure group|An organised group of people which tries to influence government policy in a particular area or in support of a particular course|
|Product placement|A marketing technique in which products or mentions of products are embedded within, for example, a film or a television show|
|Propaganda|Using the media to promote a biased viewpoint, usually for political purposes|
|Psychographic|Marketing and advertising often categories consumers using psychographic variables: different psychological categories that are linked to aspirations, lifestyle, personality or spending habits|
|Reception theory|These are theories of the media audience that see audiences as ‘active’ because they make their own meanings out of the messages received from the media. The meanings we make out of media products is just as much influenced by who we are as by the content of the message, according to reception theory|
|Self-regulation|This occurs when media industries set up and pay for their own regulatory bodies|
|Stereotyping|The reduction of a social group to a limited set of characteristics or pre-conceived ideas|
User generated content (UCG)|User generated content (UGC) is any content created and distributed on a particular platform by a user of that platform|
|Viral marketing|A method of marketing which encourages media consumers to share opinion and information about a media product on the internet and on social media|
|legacy finance|raditional financial systems and institutions|
|optics|monitoring and analysis|
|BO|base order; initial order size|
|SO|safety order; safety order size|
|SOS|safety order price deviation; percent deviation of the first safety order|
|OS|safety order volume scale; percent of volume of new safety orders|
|SS|safety order step scale; percent changes between safety orders|
|TP|ake profit; profit percentage of coin price|
|cultural truism|claims or beliefs so widely held that they are seldom questioned|
|yield farming|staking or lending crypto assets in order to generate high returns or rewards in the form of additional cryptocurrency|
|bearish divergence|he price of an asset is going up but the relative strength index (RSI) is going down|
|RSI|relative strength index; uses magnitude of recent price changes to generate overbought/oversold signals|
|schotastic|generates overbought (time to sell) and oversold (time to buy) signals|
|echnical indicators|specific asset or market conditions used to predict price movement|
|ay-trading|rading on short timeframe charts for quick profit|
|swing trading|rading with a focus on long-term trends of assets; https://www.youtube.com/watch?v=v7LyUzKWGfQ|
|stop loss|price to exit trade if price begins to drop|
|maker fee|fee for creating a market order|
|taker fee|fee for filling an order that's already there|
|yield farming| staking or lending crypto assets in order to generate high returns or rewards in the form of additional cryptocurrency|
|on-chain analysis|using metrics and statistics found on blockchains such as wallet addresses, balances, transactions|
|candle closing| staking or lending crypto assets in order to generate high returns or rewards in the form of additional cryptocurrency|
|tokenomics|using metrics and statistics found on blockchains such as wallet addresses, balances, transactions|
|dead cat bounce|a sudden drop followed by a sudden return back to the original value|
|AMM|automated market makers (AMM) are decentralized finance protocols that enable swapping of assets without a centralized intermediary |
|liquidity providing|the process of depositing assets into an AMM pool|
|impermanent loss|the difference in net worth between HODLing and LPing |
|liquidity mining|used to offset impermanent loss|
|HODL|to hold assets, traditionally for long periods of time |
|LP|liquidity provider/providing, depositing assets into an AMM pool|
|cryptography|the practice and study of techniques for secure communication in the presence of adversarial behavior|
|centralization|process where an organization's planning and decision-making become concentrated within a particular group|
|staking|temporarily locking tokens in exchange for rewards--typically a share of the transaction fees collected by the blockchain|
|wallet|Like a debit card. Crypto wallets do not store funds but instead provide people with access to their tokens and the ability to make transactions with them|
|fiat|money that derives its value by a government declaring that it has value|
|inflation|an expansion in the money supply which causes a general progressive increase in prices of goods and services in an economy|
|deflation|a contraction in the money supply which causes a general progressive decrease in prices of goods and services in an economy|
|blockchain| a growing list of records, called blocks, that are linked together using cryptography. Each block contains a cryptographic hash of the previous block, a timestamp, and transaction data|
|distributed ledger|a consensus of replicated, shared, and synchronized digital data geographically spread across multiple sites, countries, or institutions. Unlike with a centralized database, there is no central administrator|
|cryptocurrency|money in an electronic payment system in which a network of computers, rather than a single third-party intermediary, validates transactions|
|money|he set of assets in an economy that consumers, businesses, and governments regularly use to buy goods and services from others|
|commodity money|money made of materials that have intrinsic value, like gold and silver coins|
|central bank|a central bank conducts a nation's monetary policy and oversees its money supply.|
|horizontals|lines on chart marking a strong area where price turned on a dime|
|cognitive bias|the lens through which we view the world|
|share|(AKA equity) a security that represents the ownership of a fraction of a corporation entitling the owner to a proportion of the corporation's assets and profits equal to how much they own.|
|individual sovereignty|self-ownership, moral or natural right to be the exclusive controller of one's own body and life|
|unit bias|People prefer to buy a “whole unit” of a token, rather than a fraction of it. It's why meme coins exploded. Don't overweigh the value of a token because it's "cheap." Understanding how market caps work|
|anchoring bias|Over-reliance on the 1st piece of information you have. You heard about Bitcoin at 1K. You missed out. Then it GOES UP to 5K. You don't want to buy it anymore. It's "too expensive" in your head. Evaluate it based on its POTENTIAL not its PAST.|
|narrative bias|Humans love stories. It helps us make sense of the world. Some coins explode BECAUSE of the story. Remember GameStop last year? It was a revolution against Wallstreet. Ppl invested for the NARRATIVE.|
|sunk cost bias|A cost that has occurred and can't be recovered. We have a tendency to keep investing more money or OVER-committing because we're scared of losing our original investment. Sunk costs in real life You've invested 10K into your friend's Frozen yogurt shop. It's doing horrible. "I need another 5K for a milkshake machine" You might be tempted to invest more to "save" your 10K. Don't throw good money after the bad. Sunk cost in Crypto You're -70% on a coin. You still have 30% left. That's 70% is gone. That 30% could be better off investing somewhere else, rather than hoping the original investment bounces back.|
|recency bias|We overweight RECENT information and events. "ETH's price is boring. I'm going to chase after low cap coins" Then they get wrecked in bear markets. You can beat recency bias by zooming out on the charts.|
|overconfidence bias|We overestimate our own abilities. We get lucky a few times, and think we're smarter than we really are. The key to beating Overconfidence is solid risk management strategies|
|survivorship bias|Brad Pitt moved to LA and was a waiter before becoming a movie star. Many people have followed his path hoping to do the same. You don't hear about the THOUSANDS of other ppl who tried the same and failed. Someone turned 8K of Shiba Inu into 5.7B. You don't hear about the thousands of others who turned 8K into 500 The media prefers writing about the winners, and this skews your perception of the odds.|
|outcome bias|Outcome bias is an error made in evaluating the quality of a decision when the outcome of that decision is already known Imagine going all in with AA vs. JJ in Poker, and losing. You made a GREAT decision but it let to a BAD outcome. (AA has 80% chance of winning in this case) You invest 10K into a shitcoin that is now worth 100K. That had a great outcome, but it was a poor decision. Another angle of Outcome Bias Imagine if the scenario was replayed a thousand times. You have to account for variance. You can do everything right and the outcome STILL won't be right. That's life. But you should always make the decision with the best ODDS and PROBABILITY.|
|authority bias|This is our natural tendency to follow the leader. Once we believe someone is an expert, we trust everything they say. "They're the expert, they must be right!" • Experts can be wrong • Experts can have ulterior motives|
|confirmation bias|You only seek out information that tells you what you want to hear. You only follow people that say good things about X coin. You unfollow and block anyone that spreads “FUD.” When you invest, seek and research the FUD to see if it's valid.|
|hard mentality bias|Investors’ tendency to follow and copy what other investors are doing. They are largely influenced by emotion and instinct, rather than by their own independent analysis. If you've ever felt FOMO, it could be because of herd mentality|
|loss aversion|Losing money feels worse than gaining money. Anyone that has been in Vegas knows what I'm talking about. Winning 100 does not feel the same as losing 100. Losing is painful. A study shows that the brain typically assigns 2.5x to a loss. +250 = -100 An example is when your investment is down 50% and there's more bad news. You can close the trade and cut your losses. Loss aversion means some ppl will PREFER to wait it out. They're afraid of "losing" the money by selling. Loss Aversion leading to risk avoidance There has been a handful of rugs in DeFi. I've seen some ppl vow to never invest in DeFi again! Their loss aversion means they'll be missing out on life changing gains. Weigh the risks vs rewards properly.|
|hierarchy|a structure based on rank|
|logos|An internal coherency and transparency and comprehensibility to the cosmos, something we're called to put ourselves in alignment with|
|resilience|the ability to persist, adapt, and transform to disturbances within and beyond|
|logical fallacy| is an error in the logic of an argument that prevents it from being logically valid or logically sound|
| Genetic | Judging something good or bad based on where it comes from |
| Black-or-White | Presenting two alternative states as the only possibilities when more possibilities exist |
| Begging the Question | Presenting a circular argument in which the conclusion was included in the premise |
| Appeal to Nature | Claiming something is valid, justified, inevitable, good, or ideal because it is natural |
| Anecdotal | Using a personal experience or isolated example instead of sound argument or compelling evidence |
| Texas Sharpshooter| Using a cherry-picked data cluster to suit your argument, or a pattern to fit a presumption  |
| Middle Ground | Claiming a compromise, or middle point, between two extremes must be the truth |
| Strawman | Misrepresenting someone's argument to make it easier to attack |
| False Cause | Presuming that a real or perceived relationship between things means that one is the cause of the other |
| Appeal to Emotion | Attempting to manipulate an emotional response in place of a valid or compelling argument |
| Fallacy Fallacy | Presuming a claim must be wrong because it was poorly argued |
| Slippery Slope | Saying if we let A happen, Z will eventually happen, therefore A should not happen |
| Ad Hominem | Attacking someone's character or personal traits in an attempt to undermine their argument |
| Tu Quoque | Answering criticism with criticism to avoid addressing it |
| Personal Incredulity | Claiming something is false because it's difficult to understand or believe |
| Special Pleading | Moving goalposts or making exception when claim is shown to be false |
| Loaded Question | Asking a question with a presumption built into it |
| Burden of Proof | Believing it is up to someone else to disprove your claim |
| Ambiguity | Using double meaning or ambiguity of language to mislead or misrepresent the truth |
| Gamblers Fallacy | Believing "runs" occur on statistically independent phenomena such as roulette wheel spins |
| Bandwagon | Using popularity as an attempted form of validation  |
| Appeal to Authority | Believing something is true because an authority thinks it is |
| Composition/Division | Believing parts apply to whole or whole apply to its parts |
| No True Scotsman | Appeal to purity as a way to dismiss criticisms or flaws of an argument |
| Loaded Question | Asking a question with a presumption built into it |
|context|conditions, situations, and circumstances which surround an object or event and provides resources for its appropriate interpretation|
|endowment effect|You become emotionally attached to your bags. We place a higher value on an investment because we own it. I see this a lot with ETH maxis. They made great gains with it, and become emotionally attached. They ignore all other L1's.|
|availability heuristic|You make judgments based on how easy it is to remember information. After major airplane crashes, there's usually a fear of flying. However, • 1 in 9,821 die in a plane crash • 1 out of 114 die in a car accident Planes are much safer In Crypto, the availability bias presents itself in marketing. A coin might be pumping because it has great marketing. Marketing is important, but make sure that it isn't masking a terrible project.|
|fallacy|reasoning that is logically invalid or undermines the logical validity of an argument|
|straw man fallacy|misrepresenting an opponents argument by broadening or narrowing the scope of a premise or refuting a weaker version of their argument|
|appeals to emotion||
|appeal to flattery|using excessive or insincere praise to obtain common agreement|
|appeal to ridicule|mocking or stating the opponents position is laughable to deflect from the merits of the opponents argument|
|denialism|rhetorical tactics which reject propositions in a way that gives the appearance of argument and legitimate debate when there is none|
|logic chopping fallacy|focusing on trivial details of an argument rather than the main point|
|moralistic fallacy|inferring factual conclusions from evaluative premises|
|emotional reasoning|experiencing reality as a reflection of emotionally linked thoughts. Something is believed true solely based on a feeling|
|Dunning-Kruger effect|people with low ability, expertise, or experience in a task or area overestimate their ability or knowledge|
|red herring|something that misleads or distracts from the question issue at hand|
|contradiction|states opposing case with little or no supporting evidence|
|counterargument|contradicts and then backs it up with reasoning and/or supporting evidence|
|refutation|finds mistake and explains why it's mistaken using quotes|
|refuting the central point|explicitly refutes the central point|
|sound argument|a valid argument whose conclusion follows from its premises which are true|
|establish position|putting forth a central claim along with statements that logically support and lead to the stated conclusion|
|action bias|the tendency to act when not acting is the best play|
|reverse psychology Bias|Advocating a behavior that is opposite of what is desired|
|Von Restorff Effect|Tendency to notice outliers or information that stands out|
|bandwagon effect|Act of following the consensus of the crowd without a full understanding|
|stop hunting|Driving price up/down to trigger stop losses and force market participants out of their posistions|
|pump and dump|A coin is pumped to a high price.  When traders and investors buy in, the group dumps the coins for a profit |
|whale wall|a large set of buy/sell orders intended to create the illusion of large demand or supply in the market|
|wash trading|buying and trading the same asset to create the appearance of an active market for an asset|
|FUD|fear, uncertainty, and doubt.  False propoganda to impact prices|
|pullback|a pause or a moderate drop in price action from the recent peak in an ongoing trend|
|breakout trader|trader who places long just above last high anticipating a breakout|
|marketmaker|quotes both a buy and a sell price in a tradable asset held in inventory, hoping to make a profit on the bid–ask spread, or turn.[1] The function of a market maker is to help limit price variation (volatility) by setting a limited trading price range for the assets being traded|
|capital gain|profit realized when the value of an asset changes|
|capitalism|the economic system of private ownership and control of capital. Individuals can choose to do what they please with the capital they own|
|financial speculation|predicting price based on past information|
|type 1 error|incorrect analysis produces positive result (illusion of skill)|
|type 2 error|correct analysyis produces negative result (distrust of good technique) |
|market friction|the effect of trading costs on equity|
|hyperbolic discounting||
|Parrondo's Paradox|A combination of losing games can produce an overall winning strategy|
|hedging|acquiring assets that vary in |
|Elliot Wave Theory|Market advances in 5 waves then corrects in 3 waves|
|impulse wave|wave that moves in the same direction as the wave degree|
|correction wave|wave that moves in the opposite direction of the wave degree|
|econometrics|The application of statistical methods in economics|
|security||
|virtue signalling|expression of opinions or sentiments intended to demonstrate one's good character or moral correctness|
|economics| the study of human choices under scarcity and their consequences|
|exchange|willfully induced substitution of a more satisfactory state of affairs for a less satisfactory one|
|consumer goods|satisfy human wants directly, independent of other goods|
|producer's goods|higher order goods which satisfy wants indirectly when combined with other goods|
|property|the sum of goods at an individual's command|
|wealth|the sum of economic goods at an individual's command|
|formal fallacy|a pattern of reasoning rendered invalid by a flaw in its logical structure that can neatly be expressed in a standard logic system|
|Dimensional Loss|Loss incurred when viewing a multidimensional system through a bidimensional lense|
|Bonini's Paradox|Explaining complex systems becomes more difficult as it becomes more complete|
|Chaos Theory|Random and chaotic systems are not random, they just function within a higher degree of order that is not observable at first glance|
|Hierarchy|A structure where one element is more important than another|
|Cognitive Parallax|The difference between the theoretical thinking axis and the experienced axis of a trader|
|Complementarity Effect|When two apparently contradictory outcomes happen to make sense in reality|
|Fractal Pattern|A pattern that repeats itself inside and outside itself in multiple degrees|
|stablecoins|igital assets that are designed to maintain a stable value relative to a nationalcurrency or other reference assets.|
|Price Action Analysis|Use of logic to interpret price chart in order to find interconnected patterns that might shed light into the future of price|
|psychological high|The first high that forms at the beginning of each crypto trading week|
|psychological low|The first low that forms at the beginning of each crypto trading week|
|Cantillon Effect|First recipients of new money benefit on money expansion because they are able to spend it before it loses value. As the money is spent, it loses value over time until significant loss in real purchasing power impacts later recipients. Those with access to credit benefit the most while those on fixed incomes and minimum wages suffer the most|
|production|a process iwth a plan for combining labor, capital, and land to produce a final good|
|market depth|The necessary amount of money to move the market by one tick|
|order clusters|areas in chart where many orders are placed together due to widely known ideas about technical analysis|
|wash lines|Lines that pinpoint where retail traders got washed out of their positions|
| malinvestments|Investments that would not have been undertaken without the distortions in the capital market and whose completion is not possible once the misallcations are exposed|
|recession|The economy-wide simultaneous failure of overextended businesses|
|time preference|the ratio at which humans value the present over the future. An individual's time preference impacts economic transactions with others as well as economic transcations with their future self. Quality of life and net worth are more likely to be determined by tradeoffs with one's future self than by others or outside circumstances|
|value|a judgement made about the importance of goods driven by their scarcity|
|impartial spectator|Human nature drives price, but we can use metaphysics to understand and track causation.|
|reflexivity|Market vs Agents affect each other similiar to how objects interact in physical world|
|praxeology|the logic of action. Markets are a natural reflection of how humans behave.|
|chaos theory|fractal nature of vectors (Theory of Roughness) Browning motion (when to trade)
|geometry|Patterns that repeat themselves|
|statistics|frequency|
|metaphysics|identify flawed perception of reality (Four Elements of Causation) to extract narrative|
|classic mechanics|simple physics can be used to explain the fundamentals of the way the market behaves |
|Superimpose Market|flows are like pendulums, Human action is fractal in nature|
|Hausdorff dimension|is a measure of roughness of a complex structure|
|Simliar Motion|both flows go in same direction|
|Contrary Motion|major and minor flows are going in opposite directions|
|Oblique Motion|one flow is going sideways, other is trending in between the sideways move|
|Material Cause|Physical/Metaphysical elements that allow something to exist (What)|
|Formal Cause|The ideal or design of something exists (How)|
|Efficient Cause|The agent that brings something about (Who)|
|Final Cause|The purpose of something (Why)|
|Straw man fallacy|misrepresenting an opponents argument by broadening or narrowing the scope of a premise or refuting a weaker version of their argument|
|Appeals to Emotion||
|Appeal to flattery|using excessive or insincere praise to obtain common agreement|
|Appeal to ridicule|mocking or stating the opponents position is laughable to deflect from the merits of the opponents argument|
|Logic chopping fallacy|focusing on trivial details of an argument rather than the main point|
|I’m entitled to my opinion|an attempt to discredit opposition by claiming they are entitled to their opinion|
|Moralistic fallacy|inferring factual conclusions from evaluative premises|
|Emotional reasoning|experiencing reality as a reflection of emotionally linked thoughts. Something is believed true solely based on a feeling|
|Dunning-Kruger effect|people with low ability, expertise, or experience in a task or area overestimate their ability or knowledge|
|Social Bias||
|Reactance|the urge to do the opposite of what someone wants to do out of a need to resist a perceived attempt to constrain one’s freedom of choice|
|Hot hand fallacy|people who experience successful outcomes have a greater chance of success in further attempts|
|public relations|a variety of skills and tactics developed to create favorable opinion for a person, event, or product that ultimately supports the firm's bottom line. You turn to a public relations firm to help you achieve media coverage|
|reprint|copy of an article that mentions you or your company|
|syndicated|report that appears in more than one media outlet simultaneously|
|advertorial|An advertisement written in the style of a news item or feature, often provided by the publisher to complement adverts sold on that page. Ethically, advertorials should be clearly identified as such|
|impact investing|investments made into companies and organizations with intention to generate a social or environmental impact|
|gatekeeping|The way in which information is filtered by the media before it is prepared for publication, broadcast or distribution|
|guerilla marketing|Creative, unexpected, low-cost unconventional tactics which aim to capture the attention and interest of consumers|
|Coordinated Inauthentic Behavior|one or more actors trying to mislead others about their identity and intentions|
|Ad Hominem|Attacking a person/group instead of addressing their arguments.	“Climate science can’t be trusted because climate scientists are biased.”|
|Ambiguity|Using ambiguous language in order to lead to a misleading conclusion.	“Thermometer readings have uncertainty which means we don’t know whether global warming is happening.”|
|Anecdote|Using personal experience or isolated examples instead of sound arguments or compelling evidence.	“The weather is cold today—whatever happened to global warming?”|
|Blowfish|Focusing on an inconsequential aspect of scientific research, blowing it out of proportion in order to distract from or cast doubt on the main conclusions of the research.	“The hockey stick graph is invalid because it contains statistical errors.”|
|Bulk Fake Experts|Citing large numbers of seeming experts to argue that there is no scientific consensus on a topic.	“There is no expert consensus because 31,487 Americans with a science degree signed a petition saying humans aren’t disrupting climate.”|
|Cherry Picking|Carefully selecting data that appear to confirm one position while ignoring other data that contradicts that position.	“Global warming stopped in 1998.”|
|Contradictory|Simultaneously believing in ideas that are mutually contradictory. 	“The temperature record is fabricated by scientists… the temperature record shows cooling.”|
|Conspiracy Theory|Proposing that a secret plan exists to implement a nefarious scheme such as hiding a truth.	“The climategate emails prove that climate scientists have engaged in a conspiracy to deceive the public.”|
|Fake Debate|Presenting science and pseudoscience in an adversarial format to give the false impression of an ongoing scientific debate.	“Climate deniers should get equal coverage with climate scientists, providing a more balanced presentation of views.”|
|Fake Experts|Presenting an unqualified person or institution as a source of credible information.	“A retired physicist argues against the climate consensus, claiming the current weather change is just a natural occurrence.”|
|False Analogy|Assuming that because two things are alike in some ways, they are alike in some other respect.	“Climate skeptics are like Galileo who overturned the scientific consensus about geocentrism.”|
|False Choice|Presenting two options as the only possibilities, when other possibilities exist.	“CO2 lags temperature in the ice core record, proving that temperature drives CO2, not the other way around.”|
|False Equivalence|(apples vs. oranges)	Incorrectly claiming that two things are equivalent, despite the fact that there are notable differences between them.	“Why all the fuss about COVID when thousands die from the flu every year.”|
|Immune to evidence	|Re-interpreting any evidence that counters a conspiracy theory as originating from the conspiracy.	“Those investigations finding climate scientists aren’t conspiring were part of the conspiracy.”|
|Impossible Expectations|Demanding unrealistic standards of certainty before acting on the science.	“Scientists can’t even predict the weather next week. How can they predict the climate in 100 years?”|
|preference falsification|the act of misrepresenting one’s wants under perceived social pressures which distorts public opinion, corrupts public discourse and, hence, human knowledge|
|Logical Fallacies|Arguments where the conclusion doesn’t logically follow from the premises. Also known as a non sequitur.	“Climate has changed naturally in the past so what’s happening now must be natural.”|
|Magnified Minority|Magnifying the significance of a handful of dissenting scientists to cast doubt on an overwhelming scientific consensus.	“Sure, there’s 97% consensus but Professor Smith disagrees with the consensus position.”|
|Misrepresentation|Misrepresenting a situation or an opponent’s position in such a way as to distort understanding.	“They changed the name from ‘global warming’ to ‘climate change’ because global warming stopped happening.”|
|Moving Goalposts|Demanding higher levels of evidence after receiving requested evidence.	“Sea levels may be rising but they’re not accelerating.”
|Nefarious intent|Assuming that the motivations behind any presumed conspiracy are nefarious.	“Climate scientists promote the climate hoax because they’re in it for the money.”|
|Overriding suspicion|Having a nihilistic degree of skepticism towards the official account, preventing belief in anything that doesn’t fit into the conspiracy theory. “Show me one line of evidence for climate change… oh, that evidence is faked!”|
|Oversimplification|Simplifying a situation in such a way as to distort understanding, leading to erroneous conclusions.	“CO2 is plant food so burning fossil fuels will be good for plants.”|
|Persecuted victim|Perceiving and presenting themselves as the victim of organized persecution.	“Climate scientists are trying to take away our freedom.”|
|Quote Mining|Taking a person’s words out-of-context in order to misrepresent their position.	“Mike’s trick… to hide the decline.”|
|Re-interpreting randomness|Believing that nothing occurs by accident, so that random events are re-interpreted as being caused by the conspiracy.	“NASA’s satellite exploded? They must be trying to hide inconvenient data!”|
|patent|grant by the state that p ermits the patentee to use the state's court system to prohibit others from using their own property in certain ways|
|copyright|grant by the state that permits the copyright holder to prevernt others from using their property in certain ways|
|Red Herring|Deliberately diverting attention to an irrelevant point to distract from a more important point.	“CO2 is a trace gas so it’s warming effect is minimal.”|
|Single Cause|Assuming a single cause or reason when there might be multiple causes or reasons.	“Climate has changed naturally in the past so what’s happening now must be natural.”|
|Slippery Slope|Suggesting that taking a minor action will inevitably lead to major consequences.	“If we implement even a modest climate policy, it will start us down the slippery slope to socialism and taking away our freedom.”|
|Slothful Induction|Ignoring relevant evidence when coming to a conclusion.	“There is no empirical evidence that humans are causing global warming.”|
|Something must be wrong|Maintaining that “something must be wrong” and the official account is based on deception, even when specific parts of a conspiracy theory become untenable.	“Ok, fine, 97% of climate scientists agree that humans are causing global warming, but that’s just because they’re toeing the party line.”|
|Straw Man|Misrepresenting or exaggerating an opponent’s position to make it easier to attack.	“In the 1970s, climate scientists were predicting an ice age.”|
|hedge fund|managed portfolios of liquid assets built from pooled funds designed to provide highest investment returns possible as quickly as possible|
|private equity funds|long-term investments directly in companies such as purchasing private companies or acquiring a controlling interest in publicly traded companies through stock purchases|
|mutual funds|safer managed portfolios built from pooled funds|
|ESG|Environmental, Social, Governance framework that tracks the social responsibility and environmental impact of a company|
|Reactance|emotional and cognitive response when one perceives that their freedom or autonomy is being threatened or restricted. Characterized by a strong motivation to restore or assert one's freedom in the face of perceived constraints.|
|struggle session|induce shame until you sell your soul (integrity) by confessing to "wrongdoing" that wasn't wrong. That is, you lie on yourself to fit in, hoping to relieve the psychological and social pressure placed upon you.|
|Social philosophy|examines questions about the foundations of social institutions, social behavior, and interpretations of society in terms of ethical values rather than empirical relations|
|heuristics|Using simple philosophy ideas/concepts to deal with complexities in this world (Bruce Lee, Jordan Peterson)|
|materialism| is a form of philosophical monism which holds that matter is the fundamental substance in nature, and that all things, including mental states and consciousness, are results of material interactions of material things. According to philosophical materialism, mind and consciousness are by-products or epiphenomena of material processes (such as the biochemistry of the human brain and nervous system), without which they cannot exist. Materialism directly contrasts with idealism, according to which consciousness is the fundamental substance of nature. |
|Agape|In Christianity, from Ancient Greek ἀγάπη (agápē) is "the highest form of love, charity" and "the love of God for [human beings] and of [human beings] for God". This is in contrast to philia, brotherly love, or philautia, self-love, as it embraces a profound sacrificial love that transcends and persists regardless of circumstance.|
|Neoplatonism|Beliefs centered on the idea of a single supreme source of goodness and being in the universe from which all other things descend. Every iteration of an idea or form becomes less whole and less perfect. Neoplatonists also accept that evil is simply the absence of goodness and perfection. Finally, Neoplatonists support the idea of a world soul, which bridges the divide between the realms of forms and the realms of tangible existence.|
|Metaphysics|study of fundamental nature of reality--first principles of being or existence, identity, change, consciousness, space, and time, necessity, actuality, and possibility.|
|play|A mutual exchange of support, emotional investment, care, and love|
|humility|when what you don't know is more important that what you know|
|knowledge|Acquaintance with facts, truths, or principles, as from study or investigation |
|self-fulfilling prophecy|phenomenon where a belief ends up being confirmed because people act as if the belief is true |
|first-order chaos|when a system does not react to predictions about itself (weather)|
|second order chaos|when a system does react to predictions about itself (markets, politics)|
|General Theory of Reflexivity|Soros said trader perceptions influence the market which influences trader perceptions |
|smurfing|Money-laundering technique involving the structuring of large amounts of cash into multiple small transactions to keep them under regulatory reporting limits and avoid detection|
|information operations| coordinated military activities focused on influencing an adversary’s decisions, manipulating information and decision-making processes, information and information systems|
|psychological operations|pre-planned activities using communication methods
and other resources aimed at selected target audiences to influence their moods, attitudes, behaviour, perception and interpretation of reality|
|Overton window|mental model for understanding how political change happens. Politicians aren't leaders but followers. To get re-elected, they support proposals that already have some public appeal.  Think tanks use research, writing, films and advocacy to slide the window of political possibilities to make their proposal go from unthinkable to acceptable. Politics is downstream of song lyrics, viral tweets, movies.. first normalize, then legalize.|
|5GW|Fifth Gradient of War, victory without battles while populations remain unaware. Violence is dispersed so losing side may never realize it has been conquered|