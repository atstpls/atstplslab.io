---
title: faq
tags: [reference]
category: [reference]
---

<br>

##### What do you really own?

True ownership means nothing can be done to your property without your permission. Does your house fall into this category? Your car? Your savings, investments, the cash in your pocket? 

It can all be confiscated. It can all be destroyed. Without your permission.

<br>

##### How hard is your money?

Hard money is hard to produce. USD is easy to produce.  Gold is better but it's not hard enough.  Gold miners can control supply.

<br>

##### How are you beating inflation?

Average fiat annual inflation is 14%. This is by design. USD producers have a monopoly on the dollar while everyone else loses wealth.

<br>

##### Why choose Bitcoin?

Bitcoin is the hardest money ever invented. It is not inflationary, is controlled by no one, and its combination of speed and security in transferring value over time and space is unrivaled.
