---
title: quotes
date: 2023-04-11 10:08:31
tags: [reference]
category: [reference]
---


||
|-|
|{% blockquote Javier Milei %}
You can't give shit leftards an inch.
{% endblockquote %}|
|{% blockquote James Madison %}
Knowledge will forever govern ignorance, and a people who mean to be their own governors, must arm themselveees with the power knowledge gives.
{% endblockquote %}|
|{% blockquote C.S. Lewis %}
Of all tyrannies, a tyranny sincerely exercised for the good of its victims may be the most oppressive.  It would be better to live under robbe barons than under omnipotent moral busybodies. The robber baron's cruelty may sometimes sleep, his cupidity may at som point be satiated; but those who torment us for our own good will torment us without end for they do so with the approval of their own conscience. They may be more likely to go to Heaven 
To be 'cured' against one’s will and cured of states which we may not regard as disease is to be put on a level of those who have not reached the age of reason or those who never will; to be classed with infants, imbeciles, and domestic animals.
{% endblockquote %}|
|{% blockquote Douglas Murray %}
We want everybody to have religious freedom except ourselves. We want to praise every tradition apart from our own. And we want to promote every belief system apart from the one that got us here. That is madness.
{% endblockquote %}|
|{% blockquote Saifedean Ammous, <br>The Bitcoin Standard %}
Trade allows producers to increase their living standards through specialization in the goods in which they can produce at a lower relative cost. Only with accurate prices expressed in a common medium of exchange is it possible for people to identify their comparative advantage and specialize in it. Spcialization itself, guided by price signals, will lead to producers further improving their efficiency in the production of these goods through learning by doing, and more importantly, accumulating capital specific to it.
{% endblockquote %}|
|{% blockquote Mises %}
Tu ne cede malis sed contra audentior (Do not fear evil, procede ever more boldly against it)
{% endblockquote %}|
|{% blockquote John Danaher %}
Play the long game: We always tend to walk into the trap of thinking we are defined by our achievements of today - we aren't - ultimately you'll be judged by your achievements over the course of your career. Once you understand this you can begin to see that your central concern must always be improvement over time. Any failure today can be overturned by great success down the line.  Your jiu jitsu may well be lousy now, but that implies nothing about how it will be in five years. Focus on building to the future and be confident that any deficiencies here and now can be reversed with training and adaptation in the future. No one was born good at Jiu Jitsu, neither were the best at Jiu Jitsu ALWAYS good at Jiu Jitsu - they had their time as clumsy beginners just like you. Play the long game. Focus on gaining knowledge and developing skill and one day that will turn you into a very different person on the mat!
{% endblockquote %}|
|{% blockquote Aristotle, <br>Physics (Book I) %}
For we do not think that we know a thing until we are acquainted with its primary conditions or first principles, and have carried our analysis as far as its simplest elements
{% endblockquote %}|
|{% blockquote Thomas Reid, <br>Essays on the Intellectual Powers of Man %}
There is no greater impediment to the advancement of knowledge than the ambiguity of words
{% endblockquote %}|
|{% blockquote Ludwig von Mises, <br>Human Action: A Treatise on Economics %}
In nature there prevail irreconcilable conflicts of interests. The means of subsistence are scarce. Proliferation tends to outrun substisence. Only the fittest plants and animals survive. The antagonism between an animal starving to death and another that snatches the food away from it is implacable. Social cooperation under the division of labor removes such antagonisms. It substitutes partnership and mutuality for hostility. The members of society are united in a common venture.
{% endblockquote %}|
|{% blockquote Saifedean Ammous, The Bitcoin Standard %}
No matter how much objective data and knowledge the agency might collect, it can never know all the dispersed knowledge that bears on the decisions that each individual carries out, and that includes their own preferences and valuations of objects.  Prices, then, are not simply a tool to allow capitalists to profit; they are the information systems of economic production, communicating knowledge across the world and coordinating the complex processes of production.
{% endblockquote %}|
|{% blockquote Thomas Jefferson %}
The price of freedom is eternal vigilance
{% endblockquote %}|
|{% blockquote Ron DeSantis %}
In our country, a republic requires citizens to be engaged, and if you are a citizen who is accepting legacy 
media outlets uncritically, their narratives uncritically, without using your own indpendent judgement, 
you are failing at your duty of being a conscientious citizen.
{% endblockquote %}|
|{% blockquote Saifedean Ammous, <br>Principles of Economics %}
Inflation reduces the wealth and holdings of both worker and employer and incfeases the price of market goods they seek to purchase.
{% endblockquote %}|
|{% blockquote Saifedean Ammous, <br>The Bitcoin Standard %}
A free market is understood as one in which the buyers and sellers are free to transact on terms determined by them solely, and where entry and exit into the market are free: no third parties restrict sellers or buyers from etering the market, and no third parties stand to subsidize buyers and sellers who cannot transact in the market.
{% endblockquote %}|
|{% blockquote Saifedean Ammous, <br>The Bitcoin Standard %}
Whereas in a free market for capital the supply of loanable funds is determined by the market participants who decide to lend based on the interest rate, in an economy with a central bank and fractional reserve banking, the supply of loanable funds is directed by committee of economists under the influence of politicians, bankers, TV pundits, and sometimes, most spectacularly, military generals.
{% endblockquote %}|
|{% blockquote Thomas Sowell %}
One of the sad signs of our times is that we have demonized those who produce, subsidized those who refuse to produce, and canonized those who complain
{% endblockquote %}|
|{% blockquote Dan Crenshaw, <br>Fortitude %}
Passion successfully overrides reason and accomplishment
{% endblockquote %}|
|{% blockquote Dan Crenshaw, <br>Fortitude %}
Our outrage culture is increasingly drawn to the voices perceived as authentic, which is usually just code for excessive emotion. Thoughtful argument is downgraded while fist-shaking iactivism is rewarded. There is an assumption that anger must be connected to righteousness. Passion replaces reason.  Attitude--owning the libs or the cons--replaces sophisticated argument.
{% endblockquote %}|
|{% blockquote Dan Crenshaw, <br>Fortitude %}
What success could possibly be achievd from being outraged and victimized? - Dan Crenshaw
{% endblockquote %}|
|{% blockquote Saifedean Ammous,<br>Principles of Economics %}
Technological innovation is born from the desire to achieve ends and secure profits by serving others
{% endblockquote %}|
|{% blockquote Saifedean Ammous,<br>Principles of Economics %}
Abolishing intellectual property laws does not prevent producers from keeping trade secrets; it just places the cost of maintaining the secret on the producer and requires him to only resort to peaceful methods of enforcing it
{% endblockquote %}|
|{% blockquote Ben Shapiro %}
You were designed to use your reason and your natural gifts--and to cultivate those assets toward fulfillment of a higher end
{% endblockquote %}|
|{% blockquote Dan Crenshaw, <br>Fortitude %}
We are a nation, unique in world history, that is built upon purpose rather than geography or ethnicity. America exists because of a proposition about the nature of humankind--that our nature is not to be forcefully molded by government, but instead that government exists to protect our inalienable rights. In exchange, we expect our citizens to live dutifully, morally, and responsibly. That purpose has informed us and our national destiny at key moments throughout our existence.
Our purpose was independence and freedom, and it wasn't negotiable. It isn't just purpose that saved us--it is purpose that made us. It was this purpose that authored the American story as we know it. In our lives now, I think we have an oblication to live up to that truth.
{% endblockquote %}|
|{% blockquote Aleksandr Solzhenitsyn, <br>The Gulag Archipelago 1918–1956 %}
If only it were all so simple! If only there were evil people somewhere insidiously committing evil deeds, and it were necessary only to separate them from the rest of us and destroy them. But the line dividing good and evil cuts through the heart of every human being. And who is willing to destroy a piece of his own heart?
{% endblockquote %}
|{% blockquote Saifedean Ammous, <br>Principles of Economics %}
Abolishing intellectual property laws does not prevent producers from keeping trade secrets; it just places the cost of maintaining the secret on the producer and requires him to only resort to peaceful methods of enforcing it.
{% endblockquote %}|
|{% blockquote David Goggins, <br>Can't Hurt Me %}
Denial is the ultimate comfort zone
{% endblockquote %}|
|{% blockquote Dan Crenshaw, <br>Fortitude %}
When we ask ourselves who we want to be, we are defining the characer traits that we aspire to. Those character traits don't just appear out of nowhere; they are observed and then adopted. We identify them in others, and we make those people our "heroes".
{% endblockquote %}|
|{% blockquote Saifedean Ammous, The Bitcoin Standard %}
No matter how much objective data and knowledge the agency might collect, it can never know all the dispersed knowledge that bears on the decisions that each individual carries out, and that includes their own preferences and valuations of objects.  Prices, then, are not simply a tool to allow capitalists to profit; they are the information systems of economic production, communicating knowledge across the world and coordinating the complex processes of production.
{% endblockquote %}|
|{% blockquote Dan Crenshaw, <br>Fortitude %}
Any goal you set for yourself, by definition, requires you to excel within a competitive hierarchy, wheterh that be academics, sports, management, politics, or social networking. The fact is, there are people who have excelled in these hierarchies before you. All you need to do is observe the behavioral traits that made them successful.
{% endblockquote %}|
|{% blockquote Martin Luther King Jr. %}
Somewhere I read that the greatness of America is the right to protest for right.  -Dr. Martin Luther King Jr.
{% endblockquote %}|
|{% blockquote Ed McCaffrey %}
We’re here to work with others and accomplish great things 
{% endblockquote %}|
|{% blockquote Roy Sebag, <br>The Natural Order of Money %}
No matter how economically or politically complex our human societies appear, they nevertheless remain accountable to the regularities and vagaries of the natural world
{% endblockquote %}|
|{% blockquote Ed McCaffrey %}
Breathe, Focus, Explode
{% endblockquote %}|
|{% blockquote Jordan Peterson %}
Unless you can think the way an evil person thinks then you’re defenseless against them. Because they’ll go places you’ve never been and then they’ll win.
{% endblockquote %}|
|{% blockquote Martin Luther King Jr. %}
MLK darkness cannot drive out darkness
{% endblockquote %}|
|{% blockquote Jocko Willink %}
I understand you’re going to go out of the box now and then, but think about what you’re doing before you do it. There are some decisions that you can not recover from. 
{% endblockquote %}|
|{% blockquote Craig Wright %}
In every significant financial fraud, the accounting systems have been inadequate.
{% endblockquote %}|
|{% blockquote Craig Wright %}
The problem is we rely on humans for detection, reporting, and remediation.  Using technology to audit discrepancies would have prevented almost all.
{% endblockquote %}|
|{% blockquote Saifedean Ammous, The Bitcoin Standard %}
No matter how much objective data and knowledge the agency might collect, it can never know all the dispersed knowledge that bears on the decisions that each individual carries out, and that includes their own preferences and valuations of objects.  Prices, then, are not simply a tool to allow capitalists to profit; they are the information systems of economic production, communicating knowledge across the world and coordinating the complex processes of production.
{% endblockquote %}|
|{% blockquote Craig Wright %}
Fraud relies on opportunity.  If you minimise opportunity, you minimise fraud.
{% endblockquote %}|
|{% blockquote Roy Sebag %}
The explanations for the causes of inflation are unsatisfactory.
{% endblockquote %}|
|{% blockquote Jordan Peterson %}
We must orient ourselves.
{% endblockquote %}|
|{% blockquote Jordan Peterson, <br>Maps of Meaning %}
Behavior is imitated, then abstracted into play, formalized into drama and story, crystallized into myth, and codified into religion. And only then criticized in philosophy and provided post-hoc with rational underpinnings.
{% endblockquote %}|
|{% blockquote Siddhartha %}
Three things can not hide for long: the Moon, the Sun and the Truth.
{% endblockquote %}|
|{% blockquote Saifedean Ammous, <br>The Bitcoin Standard %}
Sound money is chosen freely on the market for its salability, because it holds its value across time, because it can transfer value effectively across space, and because it can be divided and grouped into small and large scales.  It is money whose supply cannot be manipulated by a coercive authority that imposes its use on others.
{% endblockquote %}|
|{% blockquote Saifedean Ammous, <br>The Bitcoin Standard %}
Sound money protects value across time, which gives people a bigger incentive to think of their future, and lowers their time preference. The lowering of the time preference is what initiates the process of human civilization and allows for humans to cooperate, prosper, and live in peace.
{% endblockquote %}|
|{% blockquote Saifedean Ammous, <br>The Bitcoin Standard %}
Sound money allows for trade to be based on a stable unit of measurement, facilitating ever-larger markets, free from government control and coercion, and with free trade comes peace and prosperity. Further, a unit of account is essential for all forms of economic calculation and planning and unsound money makes economic calculation unreliable and is the root cause of economic recessions and crises.
{% endblockquote %}|
|{% blockquote Saifedean Ammous, <br>The Bitcoin Standard %}
Sound money is an essential requirement for individual freedom from despotism and repression, as the ability of a coercive state to create money can give it undue power over its subjects, power which by its very nature will attract thte least worthy, and most immoral, to take its reins.
{% endblockquote %}|
|{% blockquote Saifedean Ammous, <br>The Bitcoin Standard %}
Increased government spending financed by inflation, growth in government expenditure and the national debt and a loss of the purchasing power of the dollar.  Every candidate can champion every cause.
{% endblockquote %}|
|{% blockquote The Edge %}
Why is the rabbit unafraid?<br></br>Because he is smarter than the panther. Through training, experience, and hardships the rabbit has honed it skills and thus is confident that it can outsmart the panther on any given day. It knows that it will be stalked but also that it has the skills and the tactics to evade the panther.
{% endblockquote %}
|{% blockquote Jordan Peterson %}
The thing about belief is it doesn't really matter what you think about belief; because you don't understand the structure of your own beliefs, and generally they're manifested in action and not in speech.
{% endblockquote %}|
|{% blockquote Jordan Peterson %}
There is no perception independent of action. There is no action independent of goal-directed motivation. So all perception is associated with motivation. 
{% endblockquote %}|
|{% blockquote Brett Weinstien,<br>A Hunter-Gatherer's Guide to the 21st Century  %}
"Science is a method that oscillates between induction and deduction--we observe patterns, propose explanations, and test them to see how well they predict things we do not yet know.  We then generate models of the world that, when we do the scintific work correctly, achieve three things: they predict more than what came before, assume less and come to fit with one another, merging into a seamless whole."
{% endblockquote %}|

 





Gonna be a hell of a story  - Edelman 

C'mon boys, Let's see what we got now. Let's see what we got  - Brady 

No fear, cut it loose  - Brady 

Gotta lock in now, laser focus 

Gotta play harder, gotta play tougher, harder, tougher, everything we got 

We've been here - hightower 




Leon Edwards


"I know, It doesn't matter. I'm from the trenches. I'm built like this."

"They all doubted me, said I couldn't do it. They all said I couldn't do it. Look at me now. Living it. Pound for Pound. Headshot dead. That's it."

"Pound for pound what?  There is no pound for pound.  The belt belongs to nobody. That's it."


"Listen, Stop feeling sorry for your f'n self"

"Well come on then, what's wrong with you"

"You're 2 down, now you gotta pull the s--- out of the fire"

"Come on Leon man, you got it man come on"

"C'mon Leon. Let's go"

They all doubted me, said I couldn't do it
They all said I couldn't do it
Look at me now--Living it
Pound for pound
Headshot
Dead


