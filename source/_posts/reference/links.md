---
date: 2023-03-03 19:21:48
title: links
description:
tags: [reference]
category: [reference]
---

<br>

[The Use of Knowledge in Society](https://www.kysq.org/docs/Hayek_45.pdf) by Friedrich Hayek (September 1945)

[Saylor Academy](https://learn.saylor.org/)

[Saifedean's Website - Economics](https://saifedean.com/eco11/)

[Principles of Economics](https://www.google.com/books/edition/Principles_of_Economics/pFva23_vWzkC?hl=en)

[Against Intellectual Property](https://www.researchgate.net/publication/278637139_The_Case_Against_Intellectual_Property)

<br>

- AnyWho1: a free white pages directory with a reverse look-up function.
- AllAreaCodes2: allows users to look up any name and address listed against a phone number. The service is free if the number is listed in the White Pages, and they provide details about unlisted numbers for a small price.
- Facebook Graph Search3: provides a streamlined method to locate individuals for the verification of information. Journalists do not need to know the name of the person they are searching for; instead, they can search based on other known criteria such as location, occupation and age.
- GeoSocial Footprint4: a website where one can track the users’ location “footprint” created from GPS enabled tweets, social check ins, natural language location searching (geocoding) and profile harvesting.
- Hoverme5: this plug-in for Google Chrome reveals social media users’ profiles on other networks from their Facebook news feed.
- Identify6: this Firefox plugin creates a profile 
- Muck Rack8: lists thousands of journalists on Twitter, Facebook, Tumblr, Quora, Google+, LinkedIn who are vetted by a team of Muck Rack editors.
- Numberway9: a directory of international phone books.
- Person Finder10: one of the most well-known open source databanks for individuals to post and search for the status of people affected by a disaster. Whenever a large scale disaster happens, the Google Crisis Team sets up a person finder.
- Pipl.com11: searches for an individual’s Internet footprint and can help identify through multiple social media accounts, public records and contact details.
- Rapportive12: this Gmail plugin gives users a profile on their contacts, including social media accounts, location, employment.
- Spokeo13: a people search engine that can find individuals by name, email, phone or username. Results are merged into a profile showing gender and age, contact details, occupation,
education, marital status, family background, economic profile and photos.
- WebMii14: searches for weblinks that match an individual’s name, or can identify unspecified individuals by keyword. It gives a web visibility score which can be used to identify fake profiles.
- WHOIS15: finds the registered users of a domain name and details the date of registration, location and contact details of the registrant or assignee.
- Flikr16: search for geolocated photos.
- free-ocr.com17: extracts text from images which can then be put into Google translate or searched on other mapping resources.
- Google Maps18: an online map providing high-resolution aerial or satellite imagery covering much of the Earth, except for areas around the poles. Includes a number of viewing options such as terrain, weather information and a 360-degree street level view.
- Google Translate19: can be used to uncover location clues (e.g. signs) written in other languages.
- Météo-France20: France’s meteorological agency makes freely available Europe focused radar and satellite images, maps and climate modelling data.
- NASA Earth Observatory21: the Earth Observatory was created to share satellite images and information with the public. It acts as a repository of global data imagery, with freely available maps, images and datasets.
- Panoramio22: photo-sharing website carrying millions of geolocated images uploaded to a Google Maps layer.
- Picasa23: search for geolocated photos.
- United States ZIP Codes24: an online map of the United States categorized according to ZIP code. Users are able to search for a specific ZIP code, or can explore the map for information about different ZIP codes.
- Wikimapia25: crowsourced version of Google maps containing points of interest and descriptions.
- Wolfram Alpha26: a computational answer engine that responds to questions using structured
and curated data from its knowledge base. Unlike search engines, which provide a list of relevant sites, Wolfram Alpha provides direct, factual answers and relevant visualizations.
- Findexif.com27: another tool that can be used to reveal EXIF information.
- Foto Forensics28: this website uses error level analysis (ELA) to indicate parts of an image that may have been altered. ELA looks for differences in quality levels in the image, highlighting where alterations may have been made.
- Google Search by Image29: by uploading or entering an image’s URL, users can find content such as related or similar images, websites and other pages using the specific image.
- Jeffrey’s Exif Viewer30: an online tool that reveals the Exchangeable Image File (EXIF) information of a digital photo, which includes date and time, camera settings and, in some cases GPS location.
- JPEGSnoop31: a free Windows-only application that can detect whether an image has been edited. Despite its name it can open AVI, DNG, PDF, THM and embedded JPEG files. It also retrieves metadata including: date, camera type, lens settings, etc.
- TinEye32: a reverse image search engine that connects images to their creators by allowing users to find out where an image originated, how it is used, whether modified versions exist 
- AIDR platform33: uses human and computer monitoring to weed out rumors on Twitter.
- Ban.jo34: aggregates all social media into one platform allowing images and events to be cross-checked against each other.
- Geofeedia35: allows a user to search and monitor social media contents by location. By selecting a location, crowd contents from Twitter, Flickr, Youtube, Instagram and Picasa in this area are gathered in real time. Geofeedia can assist in the verification process, by crossreferencing posts within a particular area to see if details match.
- HuriSearch36: enables you to search content from over 5,000 human rights related Web pages and easily filter these to find verifiable sources.
- InformaCam37: The app addresses the verification challenge by harnessing metadata to reveal the time, date and location of photos or videos. Users can send their media files, and
their metadata, to third parties by using digital signatures, encryption (PGP) and TOR secure servers.
- PeopleBrowsr38: a platform and tool on which the crowd can monitor and synthesize social media and news into location and time sequence, which can then also be filtered down. The platform also features a credibility score measuring users’ influence and outreach on social networks.
- SearchSystems.net39: an international directory of free public records.
- Snopes.com40: a site dedicated to debunking Internet hoaxes, which can be used to crosscheck UGC.
- Verily platform41
- YouTube Face Blur42: Developed out of concern for the anonymity of individuals who appear in videos in high-risk situations, this tool allows users to blur faces of people who appear in videos they upload. To use, when you upload a video on YouTube, go to Enhancements, and then Special Effects. There you can choose to blur all faces in the video.  

