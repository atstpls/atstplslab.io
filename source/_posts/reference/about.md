---
title: about
date: 2023-04-07 13:58:48
tags: ["security", "defense", "tools", "tactics"]
description: About
permalink: /about/

--- 
This project is designed to provide knowledge, skills, and tactics for optimizing purpose, abilities, and wealth.

A roadmap for taking control of your time, finances, assets, and data.

<br>


##### [Upgrade and Safeguard Assets](/assets)

<ul>
  <li>Acquire apex assets to store wealth</li>
  <li>Establish true ownership of property and information</li>
  <li>Maintain complete control of all digital assets</li>
</ul>

<br>


##### [Modernize Authentication Protocols](/authentication)

<ul>
  <li>Leverage multi-layer authentication controls</li>
  <li>Transition to Tamper-Resistant Hardware Devices</li>
  <li>Upgrade credential hygiene and management</li>
</ul>

<br>

##### [Overhaul and Harden Devices](/devices)

<ul>
  <li>Reduce attack surface of endpoint and network devices</li>
  <li>Identify threat vectors, vulnerabilities, and privacy concerns</li>
  <li>Verify the integrity of files, applications, and endpoints</li>
</ul>

<br></br>

We do THIS to prevent THIS

Credit Card Fees/ Debt 

Banks

COnfiscation

Inflation / DebasementD

Disinformation

Censorship

Collusion of Hedge funds & 

Capture of Corporate 

Corruption, negligence, incompetence, capture, collusion, disinformation, censorship 

Choose a wallet that is best for you [here](https://bitcoin.org/en/choose-your-wallet?step=5)

<br>

### Android

|Wallet|Website|Code|Info|
|-|-|-|-|
|Bitcoin Wallet |[website]()|[source code](https://github.com/bitcoin-wallet/bitcoin-wallet)|[information](https://bitcoin.org/en/wallets/mobile/android/bitcoinwallet/)|
|Electrum|[website](https://electrum.org/)|[source code](https://github.com/spesmilo/electrum)|[information](https://bitcoin.org/en/wallets/desktop/windows/electrum/)|
|Edge|[website](https://edge.app/)|[source code](https://github.com/EdgeApp/edge-react-gui)|[information](https://bitcoin.org/en/wallets/mobile/android/edgewallet/)|
|Unstoppable|[website](https://unstoppable.money/)|[source code](https://github.com/horizontalsystems/unstoppable-wallet-android)|[information](https://bitcoin.org/en/wallets/mobile/android/unstoppable/)|

<br>

### iPhone

|Wallet|Website|Code|Info|
|-|-|-|-|
|Edge|[website](https://edge.app/)|[source code](https://github.com/EdgeApp/edge-react-gui)|[information](https://bitcoin.org/en/wallets/mobile/android/edgewallet/)|
|Unstoppable|[website](https://unstoppable.money/)|[source code](https://github.com/horizontalsystems/unstoppable-wallet-android)|[information](https://bitcoin.org/en/wallets/mobile/android/unstoppable/)|

<br>

Make sure the trading app you’re using has MFA—multi factor authentication enabled. 

That means if:

1. someone gets your phone
     or
2. someone gets/guesses your password

...then they will have to use a second app (an authenticator app) in order to move coins out of your wallets.

Also, make sure this authenticator app doesn’t just open without making you put in password, PIN, or faceID to use.

