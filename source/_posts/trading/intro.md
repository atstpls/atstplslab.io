---
title: intro
date: 2023-10-18 01:00:42
tags: [resiliency, government]
category: [ecosystem, resources]
---

<br>


- [9 Principles](../9_principles/)
- [18 Patterns](../18_patterns/)
- [Candle Patterns](../candle-patterns/)
- [Chaos Theory in Trading](../chaos-theory-in-trading/)
- [Chaotic Markets](../chaotic-markets/)
- [Chart Patterns](../chart-patterns/)
- [Trend Reversals](../trend-reversals/)
- [Wyckoff](../wyckoff/)


## Main Ideas 

- [Intrinsic Forces](#intrinsic-forces)
    - [Bias and Body Size](#bias-and-body-size)
    - [Range Size](#range-size)
    - [Shadow Size](#shadow-size)
    - [Body Percentage and Position](#body-percentage-and-position)
- [Extrinsic Forces](#extrinsic-forces)
    - [Location or Trend Phase](#location-or-trend-phase)
    - [Overextension](#overextension)
    - [Price Barriers](#price-barriers)
    - [Prior Price Action Activity](#prior-price-action-activity)
- [Fractal Analysis](#fractal-analysis)
    - [Frequency Lines](#frequency-lines)
    - [Dynamic Frequency Stops and Breakouts](#dynamic-frequency-stops-and-breakouts)
    - [Fractal Manipulations](#fractal-manipulations)



<br><br><br>

Use broad aspects ([extrinsic](#Extrinsic-Forces)) to provide context and narrow aspects ([intrinsic](#Intrinsic-Forces)) to provide precision and timing 


### Intrinsic Forces 

- [Bias and Body Size](#bias-and-body-size)
- [Range Size](#range-size)
- [Shadow Size](#shadow-size)
- [Body Percentage and Position](#body-percentage-and-position)

#### Bias and Body Size 

- Who won the battle 
- Bias indicates Continuation, Reversal, or Disappearance 
- Body Size indicates 

<br>

#### Range Size 

- Measure of volatility 
- Use ATR1 + BB  
- Candles above 2 standard deviations are wide range candles 
- Candles above 3 standard deviations are outliers
- Range is cyclical   also heavily dependent on body percentage 

Range Dynamics 
- HH + HL   Uptrend
- LH + LL   Downtrend 
- HH + LL   Expansion ( increase in volatilty )
- LH + HL   Contraction ( decrease in volatilty )
- HH + FL   Upward Expansion ( support with upward leeway )
- LH + FL   Upward Contraction ( support with selling pressure )
- FH + LL   Downward Expansion ( resistance with downard leeway )
- FH + HL   Downward Contraction ( resistance with buying pressure )
- FH + FL   Sideways Market

<br>

#### Shadow Size

- Measure of pressure
- Used to detect Frequency, DFB and DFS, Fractal Manipulations, and Fractal Candles 

<br>

#### Body Percentage and Position

- Measures uncertainty and balance 
- Greater the shadow asymmetry, higher tendency towards side of smaller shadow 

<br>








### Extrinsic Forces 

- [Location or Trend Phase](#location-or-trend-phase)
    - [DOW Theory](#dow-theory)
    - [Elliot Wave Theory](#elliot-wave-theory)
    - [Wyckoff Method](#wyckoff-method)
- [Overextension](#overextension)
    - [Velocity Divergence](#velocity-divergence) 
    - [Acceleration Divergence](#acceleration-divergence)
    - [Volume Divergence](#volume-divergence) 
- [Price Barriers](#price-barriers)
- [Prior Price Action Activity](#prior-price-action-activity)

<br>

#### Location or Trend Phase

- [DOW Theory](#dow-theory)
- [Elliot Wave Theory](#elliot-wave-theory)
- [Wyckoff Method](#wyckoff-method)

- DOW Theory - volume confirms price 
- Elliot Wave Theory - markets move in patterns of 5/3 waves 
- Wyckoff Method - Accumulation, Spring, Distribution, UTAD 

<br>

#### Overextension

- [Velocity Divergence](#velocity-divergence) 
- [Acceleration Divergence](#acceleration-divergence)
- [Volume Divergence](#volume-divergence) 

- Implicit forces of price action: Velocity, Acceleration, Volume 
- Divergence Signals 
    - Velocity Divergence 
    - Acceleration Divergence
    - Volume Divergence 
- Key factors 
    - Signal Alternation 
    - Length Between Extremes 
    - Signal Self-Similarity


<br>

#### Price Barriers

- Intrinsic properties of candles interacting with the barrier 
- Test/Switch/Retest dynamics 
- Location of candles respective to the barrier 

- DFBs contextualized by intrinsic properties, extrinsic forces and complementary ideas are best signs price is leaving a barrier 

<br>

### Prior Price Action Activity

- Location is micro, trend is macro 

<br>




- Choose timeframe with smooth price action (1.0 fractal dimension like a line, 2.0 is rough like a surface)

- Use Dynamic Frequency Breakouts or Dynamic Frequency Stops in combination with price vectors 

    - In upward price vectors, Upper DFS + Lower DFB --> reversal 
    - In downward price vectors, Lower DFS + Upper DFB --> reversal  

- Most effective DFBs occur with low to medium volatility ( use BB + ATR to view )  * Butterfly Effect * 

- BULL TRAPS ( Spring ) approaches and tests a known high     ( BULL RAID ) 

- BEAR TRAP  ( UTAD ) approaches and tests a known low    ( BEAR RAID ) 

- Fractal Manipulations - pierces previous candle without breaking above or below it 

- Hybrid Manipulations - fractal RAID + TRAP  

- [Divergence Analysis](#divergence-analysis)
    - [Velocity](#velocity)
    - [Acceleration](#acceleration)
    - [Volume](#volume)
- [Fractal Analysis](#fractal-analysis)

<br>

### Divergence Analysis 

Continuations tend to favor the trend, continuation/reversal 
The larger length between extremes, the more its a reversal 
Signal self-similarity 

##### Velocity 

- Velocity Reversal Divergence   Price UP + Velocity DOWN = BEARISH REVERSAL 
                             
                                 Price DOWN + Velocity UP = BULLISH REVERSAL 
##### Acceleration

- Acceleration Divergence       

##### Volume 

- Volume Divergence - cumulative volume delta (CVD)

    CVD Exhaustion - Pric UP + CVD Down = Exhaustion  REVERSAL 

                     Price DOWN + CVD UP = Absorption REVERSAL 

### Fractal Analysis

Low fractal dimension (1.0) like a line is better to trade than high fractal dimension (2.0) like a surface 


### Fractal Manipulations 

- Bull Traps rise to known resistance for short entries 
- Bear Traps drop to known support for long entries 





