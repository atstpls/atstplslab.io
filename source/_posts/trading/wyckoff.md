---
title: the wyckoff method
date: 2023-04-13 14:52:15
tags: [trading]
category: [prepare, knowledge]
---

<br>

The Wyckoff Method uses price action and volume to identify how markets are manipulated.

||||
|-|-|-|
|Accumulation|Absorb supply within a range to prepare path of least resistance to the upside|V-shape|
|Uptrend||
|Distribution|Absorb demand within a range to prepare path of least resistance|
|Downtrend||

<br>

Throughout this cycle there are several fast reversal patterns:

<br>

||||
|-|-|-|
|Climax|Uptrend goes directly to downtrend|V-shape|
|Double Top/Bottom|Second hump doesn't break plane|W and M formations|
|Bull/Bear Trap|Second hump breaks plane ( W and M )

<br>

### Laws of Wyckoff Method

<br>

|||
|-|-|
|[law of supply and demand](#law-of-supply-and-demand) | MM enters against the trend |
|[law of cause and effect](#law-of-cause-and-effect)   | High volume pushes price to edge |
|[law of effort and result](#law-of-effort-and-result) | Sharp change of character to ranging |

<br>

### Law of Supply and Demand

Market always seeks equilibrium where supply curve and demand curve intersect

Two types: Aggressive (Market) orders causes price movement
        Passive (Limit) orders only slow movement down

Aggressive supply/demand can be absorbed by passive supply/demand
Large numbers of limit orders are hard to break through
Supply/demand absorption cause Narrow Range High Volume candles

<br>

### Law of Cause and Effect

Cause precedes effect.

Long distribution should result in long downtrend and vice versa

<br>

### Law of Effort and Result

Effort precedes results.

Price action should reflect volume.  When it does it's convergence, When it doesn't, it's divergence.

Divergence:  Narrow Range High Volume, Wide Range Low Volume
Convergence: Narrow Range Low Volume, Wide Range High Volume

<br>

### ANALYSIS METHODS

<br>

|||
|-|-|
|[volume spread analysis](.)|Low volume produces large candle or high volume produces a small candle |
|[subsequent shift analysis](.)  | Large candle should result in upward shift, small candle should result in downward shift |
|[price movement analysis](.)     |High volume should produce shift, low volume should not|
|[non-adjacent price movement analysis](.)  |Waves should produce higher/lower peaks |
|[support resistance breakout analysis](.)  |Support/Resistance lines should produce change of direction |

<br>

### 7 LOGICAL EVENTS

<br>

|||
|-|-|
|[Preliminary Stop](#preliminary-stop) | MM enters against the trend |
|[Climax](#climax)                     | High volume pushes price to edge |
|[Reaction](#reaction)                 | Sharp change of character to ranging |
|[Secondary Test](#secondary-test)     | Test climax but with lower volume |
|[False Breakout](#false-breakout)     | Attracts retail to generate liquidity |
|[Breakout](#breakout)                 | 2nd change of character to trending |
|[Confirmation](#confirmation)         | |

<br>

##### PRELIMINARY STOP

First sign trend might be ending.
Occurs while trend is still in effect, usually not the highest/lowest price
First sign of large traders entering against the main trend
NRCs plus constant high volume or large shadow plus high volume

<br>

##### CLIMAX

Usually highest high before distribution or lowest low before accumulation
Usually marked by a WR candle with high volume 
Could be WRC + HV  or multiple NRC + HV or hv candles with large shadows

Selling climax comes before accumulation
Buying climanx comes before distrubution

Exhaustion is progressively lower and lower

<br>

##### REACTION

Sharp movement and change of character
Indicates trend to sideways market
Automatic Reaction
Used as one of the boundaries, establishes upper range or resistance or lower range or support
Used to confirm the climax
Used to provide context, if reaction confirms climax, traders should expect second test next

<br>

##### SECONDARY TEST

Price movement that aims to test the level of the climax but with lower volume and narrower candle ranges

Generic test - test of a price level where there is lack of interest from well-informed traders.

High volume means still fighting. Low volume means ready to shift out.

NDC - No demand candle with low volume
NSC - No supply candle with low volume

Failed test results in high volume. Successful test results in low volume.

<br>

##### FALSE BREAKOUT

Point of manipulation 
Spring in accumulation and Upthrust in Distribution
End of cause (sideways market) and beginning of move
Attracts uninformed traders to generate liquidity so MM can enter on other side

Look for signs of rejection after the breakout. That is MM entering positions

Used to breakout traders as a liquidity source
Used to trigger stop loss orders

All events leading up to this must make sense.
Cannot be a false breakout in the opposite direction.

<br>

##### BREAKOUT

Second change of character, from ranging to trending market
AKA Sign of Strength (SOS) or Jump across the creek (JATC)
AKA Sign of Weakness (SOW) or Fall through the ice (FTI)

<br>

##### CONFIRMATION


<br>

### PHASES OF WYCKOFF CYCLE

<br>

|||
|-|-|
|[Phase A](#phase-a)|The stop of the previous trend|
|[Phase B](#phase-b)|Construction of the cause|
|[Phase C](#phase-c)|The test|
|[Phase D](#phase-d)|Trend inside the range|
|[Phase E](#phase-e)|Trend outside the range|

<br>

##### PHASE A

Composed of [Preliminary Stop](#preliminary-stop), [Climax](#climax), [Reaction](#reaction), and [Secondary Test](#secondary-test).

Look for climatic volume in PS and Climax.
Reaction event will be qualitatively different that the prior countertrend moves.
Phase B confirms Phase A.

Not a phase for trade opportunities but do take profits from previous trades.

<br>

##### PHASE B

Construction of the cause.

<br>

##### PHASE C

The test

<br>

##### PHASE D

Trend inside the range 

<br>

##### PHASE E

Trend outside the range.

<br>


AKA SPRING.


Volume and volatility tend to decline 
Volume will increase before price breaks out 
SPRING (BEAR TRAP) is a fake bearish breakout to entice traders
SPRING creates liquidity zone below lower limit of range where MM can buy at lowest level

Distribution |

.
UPTHRUST (BULL TRAP) is a fake bullish breakout to entice traders 
UPTHRUST creates liquidity zone upper limit of range where MM can sell at highest level 



Wyckoff Method 4 types of reversal patterns 
- Climax (V formation) 
- Double Top/Bottom ( W and M ) 
- Pullback ( W/M with smaller 2nd hump ) 
- Bull/Bear Trap ( W/M with larger 2nd hump ) 

VOLUME SPREAD ANALYSIS 
- Comparison of candle range and corresponding volume 
- WRHV = Wide Range High Volume 
- WRLV = Wide Range Low Volume 
- NRHV = Narrow Range High Volume 
- NRLV = Narrow Range Low Volume 

Shallow Liquidity causes big movements with low volume 
Deep Liquidity causes small movements with high volume 

WRHV (High Volume Continuation Candle) 
WRLV (Low Volume Reversal Candle) 
NRLV (Low Volume Rest Candle) 
NRHV (High Volume Reversal Candle) 

Holding BTC is like holding cash and should be protected by regulations. 
The more decentralized something is, the more difficult it becomes to regulate. 

---------------------------------------------------------------- 
Paredo's Principle - Square root of group produces half the group's work - With 10K workers, 100 workers do half the work Elements Gems are composed of elements and are not that rare. Precious metals have intrinsic qualities Their value is linked to the natural world Gold is 5,000 times rarer than Copper Gold is 5,000 times more valuable than Copper -

PROBLEM SOLVING - ROOT CAUSE ANALYSIS - Play outside > legs itchy > itch medicine stinks > cover nose > talk funny - pass to wing, drives in, shoots, gets rebound, shoot again, score - badger, wolf, cougar is defensive solution - spread defense and lay-up is offensive solution - 

REHEARSING MECHANICS - the movements required by the solutions - dribbling, passing, lay-ups 
Effort & Result Result ( Price Actio ) should reflect Effort ( Volume ) If it doesn't, it is DIVERGENCE 

Ways to analyze: 

1. Volume Spread Analysis (VSA) 
Identify divergence: High volume creates narrow range candle (NRC) Low volume creates wide range candle (WRC) 

2. Subsequent Shift Analysis 
HVWR BULL should lead to rise HVWR BEAR should lead to fall 

3. Price Movement Analysis Movement with trend should increase volume 
Movement against trend should be less volume 

4. Non-Adjacent Price Movement Analysis 
Each wave should produce higher peak in volume Lower peaks in volume means divergence between effort and result 

5. Support/Resistance Breakout Analysis 
Divergence happens when price breaks S/R level and doesn't hold 

ACCUMULATION 
Fundamental maneuver is the Bear Trap ( SPRING ) 
- shorts closed 
- longs opened 
- as it develops, volume and volatility declines 
- volume increases before price breakout 
- MM creates SPRING to lure shorts 
- MM buys below lower limit 
- bullish candles tend to be wider than bearish 
- minr HH and HL form in final stages 
- when selling is low, path to upside is created 

DISTRIBUTION 
Ranging maket that precedes a downtrend MMs absorb demand in order to create a path to downside 
Fundamental maneuver is the Bull Trap ( UPTHRUST ) 
- Triggers RT short stops 
- Triggers RT long buys 
- bearish candles wider than bullish 
- minor LHs and LLs form in final stages 
- when buying is low, path to downside is created 7 

LOGICAL EVENTS IN WYCKOFF 
1. Preliminary Stop 
2. Climax 
3. Reaction 
4. Secondary Test 
5. False Breakout 
6. Breakout 
7. Confirmation 

PRELIMINARY STOP 
First sign trend is near the end 
First entrance of large traders against main trend Two techniques: 
1. NRCs + Constant High Volume 
2. Large Shadow + High Volume 

May be multiple preliminary stops to reverse trend due to intertia CLIMAX ( or just EXHAUSTION ) 
Highest High before a distribution or Lowest Low before accumulation Techniques: 

1. WRC + High Volume REACTION 
Sharp movement in opposite direction of climax ss Change of Character 
- a change from trend to sideways - It sets one of the boundaries of the sideways market 
- it confirms the climax 
- it serves as context ( can expect test of the climax ) 

SECONDARY TEST 
tests level of climax but with lower volume and narrower candle ranges NDC 
- No Demand Candle narrow & bullish w/ lower volume than last 2 candles NSC 
- No Supply Candle narrow & bearish w/ lower volume than last 2 candles FALSE BREAKOUT 
MMs induce RT to wrong side of market to create liquidity Zone Look for signs of rejection after the breakout 
- induce RT orders 
- trigger stop loss orders 
- trigger stops from late entries BREAKOUT ChoCh 
- change from sideways to trend (SOS) Sign of strength, JAC 
- Jump across Creek harmonic relationship between effort and result 

CONFIRMATION 
Final sign that market is now trending 
NSC + SOS Candle NDC + SOW Candle 
Phases A Stop of previous trend B 
Construction of the cause C 
The test D Trend inside the range E Trend outside the range 
Market reverses through a slow pattern most of the time A: 
- preliminary stop 
- climax - reaction ( looks qualitatively different than previous counter-trend moves ) 
- secondary test B: 
- longer than A 
- C: 
- false breakout D: 
- breakout 
- confirmation E: 
- trending outside the range 
- erratic movements 

