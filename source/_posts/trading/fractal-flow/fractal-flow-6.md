---
title: fractal flow 6
date: 2023-05-01 09:09:09
tags: [trading]
category: [training, fractalflow]

---

<video id="gambler's" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 46 - Gambler's Fallacy.mp4" type="video/mp4"></video> <video id="hot-hand" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 47 - Hot-Hand Fallacy.mp4" type="video/mp4"></video> <video id="the" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 48 - The Bandwagon Effect.mp4" type="video/mp4"></video>


 <video id="zero-sum" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 49 - Zero-Sum Bias.mp4" type="video/mp4"></video> <video id="hindsight" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 50 - Hindsight Bias.mp4" type="video/mp4"></video> <video id="outcome" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 51 - Outcome Bias.mp4" type="video/mp4"></video>


 <video id="restraint" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 52 - Restraint Bias.mp4" type="video/mp4"></video> <video id="overconfidence" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 53 - Overconfidence Effect.mp4" type="video/mp4"></video> <video id="dunning-kruger" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 54 - Dunning-Kruger Effect.mp4" type="video/mp4"></video>




<br>


## FRACTAL FLOW 6

### Gambler's Fallacy

### Hot-Hand Fallacy

### The Bandwagon Effect

### Zero-Sum Bias

### Hindsight Bias

### Outcome Bias

### Restraint Bias

### Overconfidence Effect

### Dunning-Kruger Effect
