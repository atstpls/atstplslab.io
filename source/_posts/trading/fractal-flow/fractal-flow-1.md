---
title: fractal flow 1
date: 2023-05-01 09:09:17
tags: [trading]
category: [training, fractalflow]
---

<video id="lecture-1" class="tut"  width="440" height="228"  controls><source src="../../../../assets/videos/FractalFlow/Lecture 1 - The Six Dimensions of a Professional Trading Strategy.mp4" type="video/mp4"></video> <video id="lecture-2" class="tut"  width="440" height="228"   controls><source src="../../../../assets/videos/FractalFlow/Lecture 2 - Contextual Trading.mp4" type="video/mp4"></video> <video id="lecture-3" class="tut"  width="440" height="228"   controls><source src="../../../../assets/videos/FractalFlow/Lecture 3 - Multitimeframe Analysis.mp4" type="video/mp4"></video>


<video id="one-arm-strangle" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 4 - Hierarchy of Signals.mp4" type="video/mp4"></video> <video id="crucifix-arm-lock" class="tut"  width="440" height="228"   controls><source src="../../../assets/videos/FractalFlow/Lecture 5 - Trade Location.mp4" type="video/mp4"></video> <video id="crucifix-arm-lock" class="tut"  width="440" height="228"   controls><source src="../../../assets/videos/FractalFlow/Lecture 6 - Relative Recognition of Market Movements.mp4" type="video/mp4"></video>


<video id="one-arm-strangle" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 7 - Subjective Interpretation of Markets.mp4" type="video/mp4"></video> <video id="crucifix-arm-lock" class="tut"  width="440" height="228"   controls><source src="../../../assets/videos/FractalFlow/Lecture 8 - Interdependence of Strategy Dimensions.mp4" type="video/mp4"></video> <video id="crucifix-arm-lock" class="tut"  width="440" height="228"   controls><source src="../../../assets/videos/FractalFlow/Lecture 9 - Technical Analysis.mp4" type="video/mp4"></video>


<br>


## FRACTAL FLOW 1

### The Six Dimensions of a Professional Trading Strategy

You need underlying principles to hold a strategy together 

3 dimensions that make up the tripod of success: 
high quality technical analysis 
solid risk management 
deep trading psychology 

<br>

### Contextual Trading

2 techniques produce same price point 

Formation of logically consistent story

<br>

### Multitimeframe Analysis

Apply fractal analysis with multiple timeframes 

Helps pinpoint precise reversal points 

<br>




### Hierarchy of Signals

Trade near edges of market Small stops and large targets

<br>

### Trade Location

Different perspectives help give context Sideways markets are made up of trends

<br>

### Relative Recognition of Market Movements

### Subjective Interpretation of Markets

Cognitive biases in interpreting price action as well as risk Deal with subjectivity of the market Develop market intuition 

<br>

### Interdependence of Strategy Dimensions

### Technical Analysis

 FFA #5  FFA #6  FFA #7 FFA #10 Auxilliary Techniques Momentum Divergence + Fractal Analysis Divergence Fractal Reversal Fractal Hybrid = Fractal Bar Classic Reversal Classic Hidden Support/Resistance => Horizontal and Sloped Lines Price Action Reading => Volatility, Shape, Position, Quality FFA #11 Find the difference between raw price and its perceived momentum Reversal Price HH, RSI LH or Price LL, RSI HL Hidden Price HL, RSI LL, Price LH, RSI HH FFA #7 Support/Resistance Boundaries that are respected due to market memory FFA #2 You can't try to win every time Always risk a fixed percentage of your capital for better margin of error awareness TRADE SIZE (LOTS) = RISK ($) / SL (PIPS) / 10 EQUITY = $100,000 RISK = 1% RISK($) = 100,000 * 1000 = $1,000 $1,000 / 20 / 10 = 5 LOTS FFA #2 Position Sizing Linear threshold - risk the same amount every trade ($100) Higher risk amplifies emotions Moving threshold - risk same percentage every trade (3%)
 


 FRACTAL FLOW 


theory of reflexivity       (soros)     - two way loop between people and market, perceptions change market, market change perceptions, chaos theory - chaotic system of second order 
theory of relativity        (einstein)  - matter tells space how to curve, space tells matter how to move 
grav field equations and reflexivity  (weinstein) - loop between markets and people have geometric 
string theory               (simmons)   - greatest hedge fund 

1.  Interplay between major and minor flows 

        
Volume
- vsa
- v at price 
- v at time 
- anchored vwap 


- candlestick


- momentum divergence


- elliot wave analysis 


Metaphysical verification using Aristotle 4 elements of causation

It's only good for that time range, same thing, nature of it changes in an non-obvious manner

old school movie, series of frames, in a single moment only the frame in front of light is relevant

If that wasn't the case, all details in all of history would be relevant

You need relevant information, to explain what's going on now

Can't understand what's going on.

First instinct is to look further back for explanations about the present

You're creating the opposite effect.  That further obfuscates your perception even more

Different timeframes causes more confusion, too much info, 

Increase your capacity to analyze a small series of events, increase analytical power, tune perception to avoid bias 

We are pattern seeking creatures and the problem is we're too good at it, we'll find patterns when there are none

We need an impartial mechanism to 

The answer is metaphysics

Markets are natural 
praxeological   - the logic of human action
action axiom  - human beings purposely utilize means to achieve an end, purpose is to achieve a better state of affairs in our lives
life is full of entropy, failure to act causes things to fall apart quickly
We choose best action out of all possible actions
Trading is embedded in human nature, markets are not an invention, they are a discovery about best way to move towards better state of affairs
Every action you take is a trade, since there's time and opportunity cost 

Humans always trade looking for advantage, trade requires both sides must feel it gives them advantage, 
combination of objective and subjective that allows you to act

to understand praxeological layer, we need
 - delta between objective and subjective perception ( this drives humans to trade )
 - cognitive parallax between people  ( difference between how person A and person B perceive the same object )

Arisotle's Metaphysics
 - physical/metaphysical
 - formal cause       
 - efficient cause    agent who produces it
 - final cause        perso

linear narrative

nonlinear narrative
