---
title: fractal flow 2
date: 2023-05-01 09:09:15
tags: [trading]
category: [training, fractalflow]

---

<video id="fractal-flow-strategy-techniques-overview.mp4" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 10 - Fractal Flow Strategy Techniques Overview.mp4" type="video/mp4"></video> <video id="price,-momentum-and-simple-divergence.mp4" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 11 - Price, Momentum and Simple Divergence.mp4" type="video/mp4"></video> <video id="fractal-analysis-concept.mp4" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 12 - Fractal Analysis Concept.mp4" type="video/mp4"></video>

<video id="fractal-reversal-divergence.mp4" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 13 - Fractal Reversal Divergence.mp4" type="video/mp4"></video> <video id="fractal-hybrid-divergence.mp4" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 14 - Fractal Hybrid Divergence.mp4" type="video/mp4"></video> <video id="fractal-bar.mp4" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 15 - Fractal Bar.mp4" type="video/mp4"></video>

<video id="auxiliary-tools-introduction.mp4" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 16 - Auxiliary Tools Introduction.mp4" type="video/mp4"></video> <video id="support-and-resistance.mp4" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 17 - Support and Resistance.mp4" type="video/mp4"></video> <video id="candle-interpretation.mp4" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 18 - Candle Interpretation.mp4" type="video/mp4"></video>




<br>


## FRACTAL FLOW 2

### Fractal Flow Strategy Techniques Overview

|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 10 - Fractal Flow Strategy Techniques Overview.mp4" type="video/mp4"></video>|- Combine Momentum Divergence and Fractal Analysis<br>- Fractal Reversal Divergence and Fractal Hybrid Divergence<br>- Fractal Bar<br>- Classic Reversal Divergence<br>- Classic Hidden Divergence<br>- Auxiliary Techniques<br>- Support & Resistance<br>- Price Action Reading<br>Horizontal, Sloped<br>- Relative Volatility<br>- Relative Position<br>- Shape Interpretation<br>- Bar Quality|

<br>

### Price, Momentum and Simple Divergence

|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 11 - Price, Momentum and Simple Divergence.mp4" type="video/mp4"></video>|- Market Momentum can be misleading<br>RSI - Visual representation of market momentumDivergence is good idea but is incomplete without Fractal AnalysisThe market cannot be compressed down to the level of univariant toolsThe trader needs to adapt to the level of complexity of the marketThe market will not adapt to our primitive biases and need for cognitive comfortMarket can't be boxed into an unrealistically simple paradigm Using a one dimensional technique and thinking it's a multi-layered toolWe tend to look for SIMPLEST solutions due to our cognitive biasesSIMPLE solutions are not always the best solutionsPrice/Momentum can be coordinated or out of syncIf out of sync:Reversal Divergence (Price -> HH, RSI -> LH) signals reversalHidden Divergence (Price -> HL, RSI -> LL) signals continuationLayer divergence in different fractals and compare them|

<br>

### Fractal Analysis Concept

|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 12 - Fractal Analysis Concept.mp4" type="video/mp4"></video>|- When two or more techniques converge to the same point in price<br />- Need multiple techniques to overcome dimensional loss<br />- Market is multi-variant and multi-dimensionalProvides deeper level of analysis|


<br>

### Fractal Reversal Divergence

|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 13 - Fractal Reversal Divergence.mp4" type="video/mp4"></video>|- A divergence that happens within another divergence <br />- Can have multiple divergences within each other<br>- Higher timeframe: PRI(HH) but no RSI(HH)Lower timeframe may reveal 2nd degree<br>- Three is 3rd degree reversal divergence<br />- This needs convergence for successFor example, support/resistant levels|

<br>


### Fractal Hybrid Divergence

|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 14 - Fractal Hybrid Divergence.mp4" type="video/mp4"></video>|Higher timeframes are more meaningful than lower timeframes|

<br>

### Fractal Bar

|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 15 - Fractal Bar.mp4" type="video/mp4"></video>|Trading near the reversal points of the market|

<br>


### Auxiliary Tools Introduction

|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 16 - Auxiliary Tools Introduction.mp4" type="video/mp4"></video>|Recognize trends with Multitimeframe Analysis and Fractal Analysis|

<br>

### Support and Resistance

|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 17 - Support and Resistance.mp4" type="video/mp4"></video>|Every trader is a unique individual|

<br>


### Candle Interpretation

|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 18 - Candle Interpretation.mp4" type="video/mp4"></video>|Candle Interpretation<br />All principles must fully integrated and unconscious competency<br /><img src="/assets/img/cover/interdependency.png" width="240" height="160">| 

<br>
