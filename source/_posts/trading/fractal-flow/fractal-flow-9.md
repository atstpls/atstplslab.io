---
title: fractal flow 9
date: 2023-05-01 09:09:03
tags: [trading]
category: [training, fractalflow]

---

<img height="216" width="60" src="../../assets/img/attacks.png"> <video id="example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/FractalFlow/Lecture 73 - EXAMPLE 7 - GBPJPY H4 Short Trade with 2nd Degree Fractal Reversal Divergence.mp4" type="video/mp4"></video> <video id="example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/FractalFlow/Lecture 74 - EXAMPLE 8 - EURJPY H1 Long Trade using 3rd Degree Fractal Hybrid Divergence.mp4" type="video/mp4"></video>

### SUBJECTIVE INTERPRETATION OF MARKETS
|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 9 - Technical Analysis.mp4" type="video/mp4"></video>|- Raw Price Action Analysis and Technical Indicators<br />- Raw Price Action provides Price Nuances and Line Studies<br>- Technical Indicators provide smoothing algorithms (Bonini's Paradox)<br>- Smoother you get, the more lag you get, which is opposite of what you want<br>- Financial markets are Chaotic System of 2nd Order<br>- Pareto Principle - small number of traders make most of the money<br>- Traders are powerful enough to move market and influence retail traders through reverse psychology techniques<br>- Main differences between good and bad technical analysis<br>- Base analysis on multi layer analytical tools<br>- Use fractal analysis, have foundatioinal principles, use context<br>- Attempt to understand underlying order of chaotic price action<br>- Awareness of Pareto Principle and 2nd Order Chaotic Nature of Markets<br>- Respects Bonini Paradox<br>- Attempt to understand underlying order of chaotic price actionAttempt to understand underlying order of chaotic price action|

<br>

### FRACTAL FLOW STRATEGY TECHNIQUES

### PRICE MOMENTUM AND SIMPLE DIVERGENCE



### FRACTAL ANALYSIS CONCEPT



### FRACTAL REVERSAL DIVERGENCE


### FRACTAL HYBRID DIVERGENCE



### FRACTAL BAR


### AUXILIARY TOOLS INTRODUCTION



### SUPPORT AND RESISTANCE


### CANDLE INTERPRETATION


### TREND RECOGNITION


