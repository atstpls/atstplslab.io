---
title: fractal flow 3
date: 2023-05-01 09:09:13
tags: [trading]
category: [training, fractalflow]

---

<video id="trend" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 19 - Trend Recognition.mp4" type="video/mp4"></video> <video id="risk" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 20 - Risk Management Introduction.mp4" type="video/mp4"></video> <video id="risk" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 21 - Risk Reward Ratios.mp4" type="video/mp4"></video> 


 <video id="margins" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 22 - Margins of Error.mp4" type="video/mp4"></video> <video id="risk" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 23 - Risk Contraction and Expansion.mp4" type="video/mp4"></video> <video id="position" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 24 - Position Sizing.mp4" type="video/mp4"></video>


<video id="shooting" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 25 - Shooting Stops Forward.mp4" type="video/mp4"></video> <video id="shooting" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 26 - Trading Psychology.mp4" type="video/mp4"></video> <video id="prospect" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 27 - Prospect Theory.mp4" type="video/mp4"></video>




<br>


## FRACTAL FLOW 3

### Trend Recognition

|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 19 - Trend Recognition.mp4" type="video/mp4"></video>| |

<br>

### RISK MANAGEMENT INTRODUCTION

|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 20 - Risk Management Introduction.mp4" type="video/mp4"></video>| |

<br>

### RISK REWARD RATIOS

|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 21 - Risk Reward Ratios.mp4" type="video/mp4"></video>| |

<br>

### MARGINS OF ERROR

|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 22 - Margins of Error.mp4" type="video/mp4"></video>| |

<br>

### RISK CONTRACTION AND EXPANSION

|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 23 - Risk Contraction and Expansion.mp4" type="video/mp4"></video>| |

<br>

### POSITION SIZING
|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 24 - Position Sizing.mp4" type="video/mp4"></video>| |

<br>

### SHOOTING STOPS FORWARD

|||
|-|-|
|<video class="tut"  width="520" height="440"  controls><source src="assets/img/cover/videos/FFS/Lecture 25 - Shooting Stops Forward.mp4" type="video/mp4"></video>| |

### Trading Psychology

### Prospect Theory

