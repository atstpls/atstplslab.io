---
title: fractal flow 5
date: 2023-05-01 09:09:11
tags: [trading]
category: [training, fractalflow]

---

<video id="von" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 37 - Von Restorff Effect.mp4" type="video/mp4"></video> <video id="focusing" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 38 - Focusing Effect.mp4" type="video/mp4"></video> <video id="framing" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 39 - Framing Effect.mp4" type="video/mp4"></video>

<video id="the" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 40 - The Weber-Fechner Law.mp4" type="video/mp4"></video> <video id="confirmation" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 41 - Confirmation Bias.mp4" type="video/mp4"></video> <video id="semmelweis" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 42 - Semmelweis Reflex.mp4" type="video/mp4"></video> 

<video id="bias" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 43 - Bias Blind Spot.mp4" type="video/mp4"></video> <video id="clustering" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 44 - Clustering Illusion.mp4" type="video/mp4"></video> <video id="naive" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 45 - Naive Realism.mp4" type="video/mp4"></video>




<br>


## FRACTAL FLOW 5

### Von Restorff Effect

### Focusing Effect

### Framing Effect

### The Weber-Fechner Law

### Confirmation Bias

### Semmelweis Reflex

### Bias Blind Spot

### Clustering Illusion

### Naive Realism

