---
title: fractal flow 7
date: 2023-05-01 09:09:07
tags: [trading]
category: [training, fractalflow]

---

<video id="peltzman" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 55 - Peltzman Effect.mp4" type="video/mp4"></video> <video id="hyperbolic" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 56 - Hyperbolic Discounting.mp4" type="video/mp4"></video> <video id="sunk" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 57 - Sunk Cost Fallacy.mp4" type="video/mp4"></video>


 <video id="irrational" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 58 - Irrational Escalation.mp4" type="video/mp4"></video> <video id="zero" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 59 - Zero Risk Bias.mp4" type="video/mp4"></video> <video id="the" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 60 - The Disposition Effect.mp4" type="video/mp4"></video>


<video id="pseudo-certainty" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 61 - Pseudo-Certainty Effect.mp4" type="video/mp4"></video> <video id="backfire" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 62 - Backfire Effect.mp4" type="video/mp4"></video> <video id="reverse" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 63 - Reverse Psychology.mp4" type="video/mp4"></video>




<br>


## FRACTAL FLOW 7

### Peltzman Effect

### Hyperbolic Discounting

### Sunk Cost Fallacy

### Irrational Escalation

### Zero Risk Bias

### The Disposition Effect

### Pseudo-Certainty Effect

### Backfire Effect

### Reverse Psychology

