---
title: fractal flow 4
date: 2023-05-01 09:09:11
tags: [trading]
category: [training, fractalflow]

---

<video id="the" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 28 - The Intelligence Paradox.mp4" type="video/mp4"></video> <video id="availability" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 29 - Availability Heuristic.mp4" type="video/mp4"></video> <video id="attentional" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 30 - Attentional Bias.mp4" type="video/mp4"></video>


<video id="the" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 31 - The Illusory Truth Effect.mp4" type="video/mp4"></video> <video id="mere-exposure" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 32 - Mere-Exposure Effect.mp4" type="video/mp4"></video> <video id="mood-congruent" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 33 - Mood-Congruent Bias.mp4" type="video/mp4"></video>


 <video id="baader-meinhof" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 34 - Baader-Meinhof Phenomenom.mp4" type="video/mp4"></video> <video id="empathy" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 35 - Empathy Gap.mp4" type="video/mp4"></video> <video id="omission" class="tut"  width="440" height="228"  controls><source src="../../../assets/videos/FractalFlow/Lecture 36 - Omission Bias.mp4" type="video/mp4"></video>




<br>


## FRACTAL FLOW 4

### The Intelligence Paradox

### Availability Heuristic

### Attentional Bias

### The Illusory Truth Effect

### Mere-Exposure Effect

### Mood-Congruent Bias

### Baader-Meinhof Phenomenom

### Empathy Gap

### Omission Bias
