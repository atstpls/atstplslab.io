---
title: price action 2
date: 2023-04-09 12:56:22
tags: [trading]
category: [training, priceaction]
---



 <video id="inward-frequency" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Inward Frequency.mp4" type="video/mp4"></video> <video id="outward-frequency" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Outward Frequency.mp4" type="video/mp4"></video> <video id="precise-supply-and-demand-zones" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Precise Supply and Demand Zones.mp4" type="video/mp4"></video> <video id="dynamic-frequency-breakout" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Dynamic Frequency Breakout.mp4" type="video/mp4"></video> <video id="wick-expansion" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Wick Expansion.mp4" type="video/mp4"></video> 
 
 <video id="pressure-zones" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Pressure Zones.mp4" type="video/mp4"></video> <video id="volatility-shift-line" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Volatility Shift Line.mp4" type="video/mp4"></video> <video id="solid-structure" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Solid Structure.mp4" type="video/mp4"></video> <video id="fake-structure" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Fake Structure.mp4" type="video/mp4"></video> <video id="manipulation-pattern" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Manipulation Pattern.mp4" type="video/mp4"></video> 
 
 <video id="fractal-bar" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Fractal Bar.mp4" type="video/mp4"></video> <video id="inside-bar" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Inside Bar.mp4" type="video/mp4"></video> <video id="outside-bar" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Outside Bar.mp4" type="video/mp4"></video> <video id="pressure-bar" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Pressure Bar.mp4" type="video/mp4"></video> <video id="hybrid-bar" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Hybrid Bar.mp4" type="video/mp4"></video>



<br>

## PRICE ACTION 2

### Inward Frequency

### Outward Frequency

### Precise Supply and Demand Zones

### Dynamic Frequency Breakout

### Wick Expansion

### Pressure Zones

### Volatility SHift Line

### Solid Structure

### Fake Structure

### Manipulation Pattern

### Fractal Bar

### Inside Bar

### Outside Bar

### Pressure Bar

### Hybrid Bar