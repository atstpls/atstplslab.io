---
title: price action 1
date: 2023-04-09 12:56:24
tags: [trading]
category: [training, priceaction]
---

<video id="introduction" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Introduction.mp4" type="video/mp4"></video> <video id="the-nature-of-price" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/The Nature of Price.mp4" type="video/mp4"></video> <video id="the-anthropic-principle-and-econophysics" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/The Anthropic Principle and Econophysics.mp4" type="video/mp4"></video><video id="soros-einstein-weinstein-and-simmons" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Soros, Einstein, Weinstein and Simmons.mp4" type="video/mp4"></video> <video id="the-level-zero" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/The Level Zero.mp4" type="video/mp4"></video> 

<video id="the-level-one" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/The Level One.mp4" type="video/mp4"></video> <video id="aspects-of-trading-correctly" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Aspects of Trading Correctly.mp4" type="video/mp4"></video> <video id="counterpoint-types" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Counterpoint Types.mp4" type="video/mp4"></video> <video id="metaphysics-and-narrative" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Metaphysics and Narrative.mp4" type="video/mp4"></video>  <video id="efficient-market-hypothesis-and-the-observer-problem" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Efficient Market Hypothesis and The Observer Problem.mp4" type="video/mp4"></video> 

<video id="market-manipulation" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Market Manipulation.mp4" type="video/mp4"></video> <video id="praxeological-elements" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Praxeological Elements.mp4" type="video/mp4"></video> <video id="extremes" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Extremes.mp4" type="video/mp4"></video> <video id="midpoints" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Midpoints.mp4" type="video/mp4"></video> <video id="frequencies" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Frequencies.mp4" type="video/mp4"></video> 

<br>

## PRICE ACTION 1

### Introduction

### The Nature of Price

### The Anthropic Principle and Econophysics

### Soros, Einstein, Weinstein and Simmons

### The Level Zero

### The Level One

### Aspects of Trading Correctly

### Counterpoint Types

### Metaphysics and Narrative

### Market Manipulation

### Praxeological Elements

### Extremes

### Midpoints

### Frequencies