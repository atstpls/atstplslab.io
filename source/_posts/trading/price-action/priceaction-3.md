---
title: price action 3
date: 2023-04-09 12:56:20
tags: [trading]
category: [training, priceaction]
---

<video id="line-theory" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Line Theory.mp4" type="video/mp4"></video> <video id="anchoring" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Anchoring.mp4" type="video/mp4"></video> <video id="extension" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Extension.mp4" type="video/mp4"></video> <video id="validation" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Validation.mp4" type="video/mp4"></video> <video id="simple-line-extrapolation-and-non-equicisant-line-extrapolation" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Simple Line Extrapolation and Non-Equidistant Line Extrapolation.mp4" type="video/mp4"></video> 

<video id="newtonian-action-space-extrapolation-and-fibonacci-action-space-extrapolation" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Newtonian Action Space Extrapolation and Fibonacci Action Space Extrapolation.mp4" type="video/mp4"></video> <video id="vector-space-extrapolation" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Vector Space Extrapolation.mp4" type="video/mp4"></video> <video id="single-and-double-negative-vector-extrapolation" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Single and Double Negative Vector Extrapolation.mp4" type="video/mp4"></video> <video id="vector-decomposition" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Vector Decomposition.mp4" type="video/mp4"></video> <video id="vector-addition" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Vector Addition.mp4" type="video/mp4"></video> 

<video id="circular-decomposition" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Circular Decomposition.mp4" type="video/mp4"></video> <video id="frequency-shifting-and-frequency-tuning" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Frequency Shifting and Frequency Tuning.mp4" type="video/mp4"></video> <video id="convergence-square-fields-and-clusters" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Convergence, Square Fields and Clusters.mp4" type="video/mp4"></video> <video id="pitchforks-inward-parallels-fibforks-and-polygonal-fields" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Pitchforks, Inward Parallels, Fibforks and Polygonal Fields.mp4" type="video/mp4"></video> <video id="cross-dimensionality" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Cross Dimensionality.mp4" type="video/mp4"></video> 


<br>

## PRICE ACTION 3

### Line Theory

### Anchoring

### Extension

### Validation

### Simple Line Extrapolation and Non Equidistant Line Extrapolation

### Newtonian Action Space Extrapolation and Fibonacci Action Space Extrapolation

### Vector Space Extrapolation

### Single and Double Negative Vector Extrapolation

### Vector Decomposition

### Vector Addition

### Circular Decomposition

### Frequency Shifting and Frequency Tuning

### Convergence, Square Fields and Clusters

### Pitchforks, Inward Parallels, Fibforks and Polygonal Fields

### Cross Dimensionality
