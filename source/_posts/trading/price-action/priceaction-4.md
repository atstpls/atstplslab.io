---
title: price action 4
date: 2023-04-09 12:56:18
tags: [trading]
category: [training, priceaction]
---

<video id="vectorization" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Vectorization.mp4" type="video/mp4"></video> <video id="reverse-engineering" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Reverse Engineering.mp4" type="video/mp4"></video> <video id="entries-stops-exits" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Entries, Stops and Exits.mp4" type="video/mp4"></video> <video id="two-rules-for-risk-and-psychology" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Two Rules for Risk and Psychology.mp4" type="video/mp4"></video> <video id="step-by-step" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Step by Step.mp4" type="video/mp4"></video> 


<video id="nash-equilibrium-market-manipulation" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT2/Lecture 1 - Nash Equilibrium & Market Manipulation.mp4" type="video/mp4"></video> <video id="newtonian-action-space-extrapolation" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT2/Lecture 2 - Newtonian Action Space Extrapolation and the Counterpoint Between Players.mp4" type="video/mp4"></video> <video id="good-bad-dynamic-frequency-breakouts" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT2/Lecture 3 - The Good and The Bad of Dynamic Frequency Breakouts.mp4" type="video/mp4"></video> <video id="cross-dimensionality-reverse-engineering" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT2/Lecture 4 - Cross Dimensionality and Reverse Engineering.mp4" type="video/mp4"></video> <video id="extremes-neural-bias" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT2/Lecture 5 - Near Extremes, Non-Equidistant Extrapolation Lines and Neutral Bias.mp4" type="video/mp4"></video> 


<video id="standing-motion-triple-intersection" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT2/Lecture 6 - Standing Motion to Running Motion and Triple Intersection.mp4" type="video/mp4"></video> <video id="inward-parallels-feedback-loops" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT2/Lecture 7 - Inward Parallels, Good Stops and Positive Feedback Loops.mp4" type="video/mp4"></video> <video id="fibonacci-fields-circular-decomposition" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT2/Lecture 8 - Fibonacci Square Fields, Tuned Forks and Circular Decomposition.mp4" type="video/mp4"></video> <video id="attention-detail-barriers-price" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT2/Lecture 9 - Attention to Detail, Open Space and the Barriers of Price.mp4" type="video/mp4"></video> <video id="switching-quality-depth-analysis" class="tut"  width="284" height="216"  controls><source src="../../assets/videos/PriceAction/PAT2/Lecture 10 - Switching Quality of Subtle Lines and Depth of Analysis.mp4" type="video/mp4"></video> 


<br>

## PRICE ACTION 4

### Vectorization

### Reverse Engineering

### Entries, Stops and Exits

### Two Rules for Risk and Psychology

### Step by Step

### Nash Equilibrium and Market Manipulation

### Newtonian Action Space Extrapolation CounterPoints

### Dynamic Frequency Breakouts

### Cross Dimensionality and Reverse Engineering

### Near Extremes, Non-Equidistant Extrapolation Lines and Neutral Bias


### Standing Motion to Running Motion and Triple Intersection

### Inward Parallels, Good Stops and Positive Feedback Loops

### Fibonacci Square Fields, Tuned Forks and Circular Decomposition

### Attention to Detail, Open Space and the Barriers of Price

### Switching Quality of Subtle Lines and Depth of Analysis

