---
title: newtonian 1
date: 2023-04-08 12:56:24
tags: [trading]
category: [training, newtonian]

---

<video id="introduction-to-econophysics" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 1 - Introduction to Econophysics.mp4" type="video/mp4"></video> <video id="physics,-the-mind-and-market-prediction" class="tut"  width="324" height="216"   controls><source src="../../assets/videos/Newton/Lecture 2 - Physics, The Mind and Market Prediction.mp4" type="video/mp4"></video> <video id="price-as-a-moving-object-and-the-tools-of-the-trade" class="tut"  width="324" height="216"   controls><source src="../../assets/videos/Newton/Lecture 3 - Price as a Moving Object and The Tools of the Trade.mp4" type="video/mp4"></video> <video id="newton's-1st-law-and-market-trends" class="tut"  width="324" height="216"   controls><source src="../../assets/videos/Newton/Lecture 4 - Newton's 1st Law and Market Trends.mp4" type="video/mp4"></video> <video id="newton's-2nd-law-and-price-trajectory" class="tut"  width="324" height="216"   controls><source src="../../assets/videos/Newton/Lecture 5 - Newton's 2nd Law and Price Trajectory.mp4" type="video/mp4"></video> 

<video id="newton's-3rd-law-and-symmetry" class="tut"  width="324" height="216"   controls><source src="../../assets/videos/Newton/Lecture 6 - Newton's 3rd Law and Symmetry.mp4" type="video/mp4"></video> <video id="conservation-law-and-potential-energy" class="tut"  width="324" height="216"   controls><source src="../../assets/videos/Newton/Lecture 7 - Conservation Law and Potential Energy.mp4" type="video/mp4"></video> <video id="trend-recognition" class="tut"  width="324" height="216"   controls><source src="../../assets/videos/Newton/Lecture 8 - Trend Recognition.mp4" type="video/mp4"></video> <video id="the-pitchfork" class="tut"  width="324" height="216"   controls><source src="../../assets/videos/Newton/Lecture 9 - The Pitchfork.mp4" type="video/mp4"></video> <video id="types-of-pitchforks-and-auxiliary-tools" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 10 - Types of Pitchforks and Auxiliary Tools.mp4" type="video/mp4"></video> 

<video id="frequency-lines" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 11 - Frequency Lines.mp4" type="video/mp4"></video> <video id="frequency-shifting" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 12 - Frequency Shifting.mp4" type="video/mp4"></video> <video id="the-paradox-of-meaningful-lines" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 13 - The Paradox of Meaningful Lines.mp4" type="video/mp4"></video> <video id="reverse-engineering-of-market-contexts-and-the-nudge-hypothesis" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 14 - Reverse Engineering of Market Contexts and The Nudge Hypothesis.mp4" type="video/mp4"></video> <video id="dynamic-resolution-and-price-action-as-a-language" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 15 - Dynamic Resolution and Price Action as a Language.mp4" type="video/mp4"></video> 

<br>

## NEWTON SYSTEM 1

### Introduction to Econophysics

Intersection of physical and economic systems
Inherent problem in the way information flows in economic systems
Because of the nature of human decisions and artifacts
Human action is by definition subjective
It's impossible to model a subjective system in an objective sense
The decisions of an actor or actors cannot be objectively modelled
It cannot be predicted.. too complex, too much information is required
Certain things cause certain results, and we understand that
Look at markets through lens of physics
The decisions of an actor or actors cannot be objectively modelled
Physics and Market Prediction

Inherent problem in the way information flows in economic systems
Because of the nature of human decisions and artifacts
Human action is by definition subjective
It's impossible to model a subjective system in an objective sense
The decisions of an actor or actors cannot be objectively modelled
It cannot be predicted.. too complex, too much information is required
Certain things cause certain results, and we understand that
Look at markets through lens of physics
The decisions of an actor or actors cannot be objectively modelled
Physics, The Mind and Market Prediction

Events in the brain must work under the laws of physics
Physics helps describe human behavior
Multidisciplinary perspective is required to understand market
Focus on econophysics as an extra layer to market logic
Price as a Moving Object and The Tools of the Trade

Determine buying/selling, why trends start, actions and reactions
Newton's Laws of Motion
Conservation Law
Potential Energy
Newton's 1st Law and Market Trends

If an object is at rest, it will stay at rest until an external force acts on it
Trends have intertia and tend to keep going
Sideways markets are when price is at rest
Newton's 2nd Law and Price Trajectory

Rate of change of momentum is directly proportional to force applied to it
Purchasing power is origin of force
Major flows come from more power, minor flows come from less
High purchasing power makes steeper angle and longer distance
Low purchasing power makes lower angle and less distance
Newton's 3rd Law and Symmetry

For every action there is an equal and opposite reaction
Past price action creates mirrored reaction in future
Action and reaction lines find market edges and show angles of market
Conservation Law and Potential Energy

Nothing is ever created. Everything is transformed.
Uptrend: Buyer potential energy greater than Seller potential energy
Downtrend: Seller potential energy greater than Buyer potential energy
Pitchfork measures potential energy and projects probably path of exhaustion
Trend Recognition


Market is make of vectors
Market has major and minor flows
Build context and read market to identify flow shifts
Fake market structures, price pierces level to mislead traders
Simple Highs and Lows
Different Flows and Collision Points
Hierarchy Between Flows
Subjective Classification of Flows
The Pitchfork

Geometric relationship between alternating market extremes
Line from A to midpoint of BC is Tail/Center Line
Stronger validity by looking at frequencies in tail
Two outer lines come from B and C
PIdentifies entries and exits
Types of Pitchforks and Auxiliary Tools



Problem of Information Flow in Markets formed from subjective economic decisions 
Overwhelming information, millions of decisions, no mathematical formula 
You can't predict every single player's movements in sports But there are rules and a pre-determined framework that allow general predictions 
Markets are a byproduct of nature, and laws of physics can assist speculation 
Linear systems do not change and can be described with a formula Non-linear systems like the markets change 
Bonini paradox - more you try to simplify a complex system, the more difficult it becomes 
Price is a moving object on the charts. 
Need to understand market flows and major vs minor flows 
Place stop losses only on confirmed market structures - highs and lows - flows and collision points - hierarchy of flows - classification of flows 
Pitchfork measures potential energy and attempts to predict point of exhaustion Point A forms 
Tail and Centerline Point B forms Outerline POint C forms Outerline 
Standard, Modified, Width, Reverse, and Pendelum Pendelum - used to catch retrace points on up/down trends Width - adjust the C point to the outside Sliding Lines - Overthrows and Underthrows - pivot points inside and outside the fork Double Lines - lines outside fork keeping same line distances Pitchforks work better when less than 45 degrees Modified fork addresses this Frequency Lines - sloped and horizontal *** do this *** Paradox of Meaningful Lines Price respects lines that traders cannot see Technical indicators Good analysis requires multiple techniques converging 


### Pitchforks


Standard, Modified, Width, Reverse, Pendulum
Sliding lines are underthrows or overthrows
Double Lines repect the equidistant zones
45 degrees work better
Modified is used when Standard angle > 45 degrees
Tail is moved to midpoint of AB segment
Pendelum used to catch end of reversals
Width is when price drifts outside of outer lineso midpoint of AB segment
Very effective when used with the center line
Reverse updates frequency after the C pivot
Frequency Shift -> price falls short or exceeds line of pitchfork
Sliding lines
Frequency Lines

Show interesting things about candlestick tails
Frequency touches all tails in question
Combine with press zones, supply, sell zones
Used to find reversals
The Paradox of Meaningful Lines


Pitchforks, Frequency Lines point out non-obvious symmetries of price
Most traders do not see meaningful lines
Most trust shallow lines like EMAs and linear smoothing formulas
Meaninful lines describe the physical framework of price outside the realm of mass psychology
Reverse Engineering of Market Contexts and The Nudge Hypothesis

Using combinations of lines and techniques that were successful in the past
Multiple techniques converge at the same price
Together, they cause significant price reversals
Dynamic Resolution and Price Action as a Language

### Levels of Resolution

Low is big picture without details and nuances
High is attention to details instead of big picture
Best resolution is 100-200 candles
Close enough to see nuances, far enough to get context
LIVE Long Trade in the EURUSD H1 with Pendulum and Standard Pitchforks

Pitchforks and Supply and Demand Line converge
Lecture 17 - LIVE Short Trade in the AUDUSD H4 with a Standard Pitchfork

Recognize trends with Multitimeframe Analysis and Fractal Analysis
LIVE Short Trade in the USDCAD M15 with a Simple Pitchfork

Every trader is a unique individual
LIVE Short Trade in the GBPUSD M30 with Simple Lines

All principles must fully integrated and unconscious competency
LIVE Long Trade in the CADJPY M15 with a Perfect Triple Intersection

Raw Price Action Analysis and Technical Indicators
LIVE Short Trade in the EURJPY M30 with a Double Intersection


### Physics, The Mind and Market Prediction

### Price as a Moving Object and The Tools of the Trade

### Newton's 1st Law and Market Trends

### Newton's 2nd Law and Price Trajectory

### Newton's 3rd Law and Symmetry

### Conservation Law and Potential Energy

### Trend Recognition

### The Pitchfork

### Types of Pitchforks and Auxiliary Tools

### Frequency Lines

### Frequency Shifting
