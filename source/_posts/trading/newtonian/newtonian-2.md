---
title: newtonian 2
date: 2023-04-08 12:56:22
tags: [trading]
category: [training, newtonian]

---

<video id="live-long-trade-in-the-eurusd-h1-with-pendulum-and-standard-pitchforks" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 16 - LIVE Long Trade in the EURUSD H1 with Pendulum and Standard Pitchforks.mp4" type="video/mp4"></video> <video id="live-short-trade-in-the-audusd-h4-with-a-standard-pitchfork" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 17 - LIVE Short Trade in the AUDUSD H4 with a Standard Pitchfork.mp4" type="video/mp4"></video> <video id="live-short-trade-in-the-usdcad-m15-with-a-simple-pitchfork" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 18 - LIVE Short Trade in the USDCAD M15 with a Simple Pitchfork.mp4" type="video/mp4"></video> <video id="live-short-trade-in-the-gbpusd-m30-with-simple-lines" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 19 - LIVE Short Trade in the GBPUSD M30 with Simple Lines.mp4" type="video/mp4"></video> <video id="live-long-trade-in-the-cadjpy-m15-with-a-perfect-triple-intersection" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 20 - LIVE Long Trade in the CADJPY M15 with a Perfect Triple Intersection.mp4" type="video/mp4"></video> 


<video id="live-short-trade-in-the-eurjpy-m30-with-a-double-intersection" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 21 - LIVE Short Trade in the EURJPY M30 with a Double Intersection.mp4" type="video/mp4"></video> <video id="live-short-trade-in-the-eurusd-m30-with-triple-intersectionality" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 22 - LIVE Short Trade in the EURUSD M30 with Triple Intersectionality.mp4" type="video/mp4"></video> <video id="live-long-trade-in-the-usdjpy-m15-with-a-secondary-entry" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 23 - LIVE Long Trade in the USDJPY M15 with a Secondary Entry.mp4" type="video/mp4"></video> <video id="live-short-trade-in-the-usdchf-with-frequency-expansion" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 24 - LIVE Short Trade in the USDCHF with Frequency Expansion.mp4" type="video/mp4"></video> <video id="live-short-trade-in-the-gbpaud-h1-with-opposing-pitchforks" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 25 - LIVE Short Trade in the GBPAUD H1 with Opposing Pitchforks.mp4" type="video/mp4"></video> 

<br>

## NEWTON SYSTEM 2


### The Paradox of Meaningful Lines

### Reverse Engineering of Market Contexts and The Nudge Hypothesis

### Dynamic Resolution and Price Action as a Language

### LIVE Long Trade in the EURUSD H1 with Pendulum and Standard Pitchforks

### LIVE Short Trade in the AUDUSD H4 with a Standard Pitchfork

### LIVE Short Trade in the USDCAD M15 with a Simple Pitchfork

### LIVE Short Trade in the GBPUSD M30 with Simple Lines

### LIVE Long Trade in the CADJPY M15 with a Perfect Triple Intersection

### LIVE Short Trade in the EURJPY M30 with a Double Intersection

### LIVE Short Trade in the EURUSD M30 with Triple Intersectionality

### LIVE Long Trade in the USDJPY M15 with a Secondary Entry

### LIVE Short Trade in the USDCHF with Frequency Expansion



