---
title: newtonian 3
date: 2023-04-08 12:56:20
tags: [trading]
category: [training, newtonian]

---

<video id="historical-width-pitchfork-describing-the-eurusd-m30" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 26 - HISTORICAL Width Pitchfork Describing the EURUSD M30.mp4" type="video/mp4"></video> <video id="historical-modified-pendulum-and-edge-detection-in-the-usdchf-h4" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 27 - HISTORICAL Modified Pendulum and Edge Detection in the USDCHF H4.mp4" type="video/mp4"></video> <video id="historical-perfect-market-angles-in-the-audjpy-h1" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 28 - HISTORICAL Perfect Market Angles in the AUDJPY H1.mp4" type="video/mp4"></video> <video id="historical-modified-pitchfork-in-the-gbpusd-h1" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 29 - HISTORICAL Modified Pitchfork in the GBPUSD H1.mp4" type="video/mp4"></video> <video id="historical-hidden-symmetry-and-double-line-in-the-eurnok-m15" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 30 - HISTORICAL Hidden Symmetry and Double Line in the EURNOK M15.mp4" type="video/mp4"></video>


<video id="historical-frequency-shifting-in-the-weekly-eurcad" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 31 - HISTORICAL Frequency Shifting in the Weekly EURCAD.mp4" type="video/mp4"></video> <video id="historical-frequency-along-the-tail-and-double-line-in-the-eurgbp-h1" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 32 - HISTORICAL Frequency Along the Tail and Double Line in the EURGBP H1.mp4" type="video/mp4"></video> <video id="historical-standard-x-pendulum-pitchforks-in-the-audusd-m15" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 33 - HISTORICAL Standard x Pendulum Pitchforks in the AUDUSD M15.mp4" type="video/mp4"></video> <video id="historical-modified-pitchfork-with-double-line-in-the-usdjpy-m30" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 34 - HISTORICAL Modified Pitchfork with Double Line in the USDJPY M30.mp4" type="video/mp4"></video> <video id="historical-standard-to-width-fork-for-precision-in-the-zadjpy-h1" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 35 - HISTORICAL Standard to Width Fork for Precision in the ZADJPY H1.mp4" type="video/mp4"></video> 

<video id="historical-standard-pitchfork-and-frequency-shift-in-the-eurusd-m1" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/Newton/Lecture 36 - HISTORICAL Standard Pitchfork and Frequency Shift in the EURUSD M1.mp4" type="video/mp4"></video>


<br>

## NEWTON SYSTEM 3

### LIVE Short Trade in the GBPAUD H1 with Opposing Pitchforks

### HISTORICAL Width Pitchfork Describing the EURUSD M30

### HISTORICAL Modified Pendulum and Edge Detection in the USDCHF H4

### HISTORICAL Perfect Market Angles in the AUDJPY H1

### HISTORICAL Modified Pitchfork in the GBPUSD H1

### HISTORICAL Hidden Symmetry and Double Line in the EURNOK M15

### HISTORICAL Frequency Shifting in the Weekly EURCAD

### HISTORICAL Frequency Along the Tail and Double Line in the EURGBP H1

### HISTORICAL Standard x Pendulum Pitchforks in the AUDUSD M15

### HISTORICAL Modified Pitchfork with Double Line in the USDJPY M30

### HISTORICAL Standard to Width Fork for Precision in the ZADJPY H1

### HISTORICAL Standard Pitchfork and Frequency Shift in the EURUSD M1


