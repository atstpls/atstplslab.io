---
title: chart patterns
date: 2023-04-07 12:56:24
tags: [trading]
category: [reference]
---

<br>

||||||
|-|-|-|-|-|
|<img height=132px width=380px src="../assets/img/charts/bull_ascending_triangle.png"></img><br><center><h5>BULL ASCENDING TRIANGLE</h5></center>|<img height=132px width=380px src="../assets/img/charts/bear_descending_triangle.png"></img><br><center><h5>BEAR DESCENDING TRIANGLE</h5></center>|<img height=132px width=380px src="../assets/img/charts/bull_bat.png"></img><br><center><h5>BULL BAT</h5></center>|<img height=132px width=380px src="../assets/img/charts/bear_bat.png"></img><br><center><h5>BEAR BAT</h5></center>|<img height=132px width=380px src="../assets/img/charts/bear_pennant.png"></img><br><center><h5>BEAR PENNANT</h5></center>|
|<img height=132px width=380px src="../assets/img/charts/bull_rectangle.png"></img><br><center><h5>BULL RECTANGLE</h5></center>|<img height=132px width=380px src="../assets/img/charts/bear_rectangle.png"></img><br><center><h5>BEAR RECTANGLE</h5></center>|<img height=132px width=380px src="../assets/img/charts/bull_symmetrical_triangle.png"></img><br><center><h5>BULL SYMMETRICAL TRIANGLE</h5></center>|<img height=132px width=380px src="../assets/img/charts/bear_symmetrical_triangle.png"></img><br><center><h5>BEAR SYMMETRICAL TRIANGLE</h5></center>|<img height=132px width=380px src="../assets/img/charts/bull_expanding_triangle.png"></img><br><center><h5>BULL EXPANDING TRIANGLE</h5></center>|
|<img height=132px width=380px src="../assets/img/charts/bear_expanding_triangle.png"></img><br><center><h5>BEAR EXPANDING TRIANGLE</h5></center>|<img height=132px width=380px src="../assets/img/charts/bull_flag.png"></img><br><center><h5>BULL FLAG</h5></center>|<img height=132px width=380px src="../assets/img/charts/bear_flag.png"></img><br><center><h5>BEAR FLAG</h5></center>|<img height=132px width=380px src="../assets/img/charts/double_bottom.png"></img><br><center><h5>DOUBLE BOTOM</h5></center>|<img height=132px width=380px src="../assets/img/charts/double_top.png"></img><br><center><h5>DOUBLE TOP</h5></center>|
|<img height=132px width=380px src="../assets/img/charts/cup_and_handle.png"></img><br><center><h5>CUP & HANDLE</h5></center>|<img height=132px width=380px src="../assets/img/charts/rev_cup_and_handle.png"></img><br><center><h5>REVERSE CUP & HANDLE</h5></center>|<img height=132px width=380px src="../assets/img/charts/head_shoulders.png"></img><br><center><h5>HEAD & SHOULDERS</h5></center>|<img height=132px width=380px src="../assets/img/charts/rev_head_shoulders.png"></img><br><center><h5>REVERSE HEAD & SHOULDERS</h5></center>|<img height=132px width=380px src="../assets/img/charts/falling_wedge.png"></img><br><center><h5>FALLING WEDGE</h5></center>|
|<img height=132px width=380px src="../assets/img/charts/rising_wedge.png"></img><br><center><h5>RISING WEDGE</h5></center>|<img height=132px width=380px src="../assets/img/charts/triple_bottom.png"></img><br><center><h5>TRIPLE BOTTOM</h5></center>|<img height=132px width=380px src="../assets/img/charts/triple_top.png"></img><br><center><h5>TRIPLE TOP</h5></center>|<img height=132px width=380px src="../assets/img/charts/symmetrical_contracting_triangle.png"></img><br><center><h5>CONTRACTING TRIANGLE</h5></center>|<img height=132px width=380px src="../assets/img/charts/symmetrical_expanding_triangle.png"></img><br><center><h5>EXPANDING TRIANGLE</h5></center>|


