---
title: market maker 1
date: 2023-04-07 12:56:28
tags: [trading]
category: [training, marketmaker]
---

<video id="the-tridimensional-axis-of-market-manipulation" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 1 - The Tridimensional Axis of Market Manipulation.mp4" type="video/mp4"></video> <video id="plato-and-the-market-makers" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 2 - Plato and the Market Makers.mp4" type="video/mp4"></video> <video id="market-depth,-order-clusters-and-liquidity-pools" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 3 - Market Depth, Order Clusters and Liquidity Pools.mp4" type="video/mp4"></video> <video id="wash-lines" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 4 - Wash Lines.mp4" type="video/mp4"></video> <video id="organized-behavior" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 5 - Organized Behavior.mp4" type="video/mp4"></video> 

<video id="press-zones" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 6 - Press Zones.mp4" type="video/mp4"></video> <video id="frequency-lines" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 7 - Frequency Lines.mp4" type="video/mp4"></video> <video id="sloped-wash-lines" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 8 - Sloped Wash Lines.mp4" type="video/mp4"></video> <video id="step-by-step-manipulation-process" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 9 - Step by Step Manipulation Process.mp4" type="video/mp4"></video> <video id="trade-setups" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 10 - Trade Setups.mp4" type="video/mp4"></video> 

<video id="live-eurusd-m15-example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 11 - LIVE EURUSD M15 Example.mp4" type="video/mp4"></video> <video id="live-usdjpy-m30-example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 12 - LIVE USDJPY M30 Example.mp4" type="video/mp4"></video> <video id="live-gbpjpy-h1-example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 13 - LIVE GBPJPY H1 Example.mp4" type="video/mp4"></video><video id="live-audusd-h4-example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 14 - LIVE AUDUSD H4 Example.mp4" type="video/mp4"></video> <video id="live-gbpusd-m15-example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 15 - LIVE GBPUSD M15 Example.mp4" type="video/mp4"></video> 

<br>

## MARKET MAKER 1

### The Tridimensional Axis of Market Manipulation



### Plato and the Market Makers



The Allegory of the Cave
MMs influence retail trader decisions
Psychological techniques mislead traders
The answer is to acquire knowledge

<br>

### Market Depth, Order Clusters and Liquidity Pools

Order clusters serve as tipping points
MM use them to create deep liquidity pools
Deeper levels impact price and market edges
MMs want large positions without distorting it too much
Retail traders help move market in desired direction
MM take supply/demand mechanism to extremes
Any trusted tool is a potential manipulation tool

<br>

### Wash Lines

Large traders use and re-use wash lines

1) Hit order cluster
2) Poke the high
3) New low
4) Return to wash line

<br>

### Organized Behavior

Price behavior is naturally chaotic
Organization reveals manipulation
Recognize Von Restorff Effect
Higher highs, lower lows, and constant volatility

<br>

### Press Zones

Pushing market in a specific direction
1) Momentum candlesticks
2) Ignition point candle
3) Volatility after press zone
4) Price often returns

<br>

### Frequency Lines

Lines do not touch candle bodies
Catch underlying frequency of tails
Horizontal and sloped

<br>

### Sloped Wash Lines

Blends wash and frequency lines
Used to indicate reversals

<br>

### Step by Step Manipulation Process

Between 100-200 candles, one timeframe
Label pivots, consider narrative
Find price signatures and Von Restorff Effect
Observe how price reacts once it breaks
Is there a logically consistent story?
Trade Setups

Standard, Modified, Width, Reverse, Pendulum
Sliding lines are underthrows or overthrows
Double Lines repect the equidistant zones
45 degrees work better
Modified is used when Standard angle > 45 degrees
Tail is moved to midpoint of AB segment
Pendelum used to catch end of reversals
Width is when price drifts outside of outer lineso midpoint of AB segment
Very effective when used with the center line
Reverse updates frequency after the C pivot
Frequency Shift -> price falls short or exceeds line of pitchfork
Sliding lines
LIVE EURUSD M15 Example

Show interesting things about candlestick tails
Frequency touches all tails in question
Combine with press zones, supply, sell zones
Used to find reversals
LIVE USDJPY M30 Example

Pitchforks, Frequency Lines point out non-obvious symmetries of price
Most traders do not see meaningful lines
Most trust shallow lines like EMAs and linear smoothing formulas
Meaninful lines describe the physical framework of price outside the realm of mass psychology
LIVE GBPJPY H1 Example

Using combinations of lines and techniques that were successful in the past
Multiple techniques converge at the same price
Together, they cause significant price reversals
LIVE AUDUSD H4 Example

Levels of Resolution
Low is big picture without details and nuances
High is attention to details instead of big picture
Best resolution is 100-200 candles
Close enough to see nuances, far enough to get context

<br>

### Trade Setups

### LIVE EURUSD M15 Example

### LIVE USDJPY M30 Example

### LIVE GBPJPY H1 Example

### LIVE AUDUSD H4 Example

### LIVE GBPUSD M15 Example
