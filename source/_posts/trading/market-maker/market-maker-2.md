---
title: market maker 2
date: 2023-04-07 12:56:26
tags: [trading]
category: [training, marketmaker]
---


<video id="live-usdcad-m5-example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 16 - LIVE USDCAD M5 Example.mp4" type="video/mp4"></video> <video id="live-eurgbp-h1-example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 17 - LIVE EURGBP H1 Example.mp4" type="video/mp4"></video> <video id="live-eurjpy-m30-example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 18 - LIVE EURJPY M30 Example.mp4" type="video/mp4"></video> <video id="live-audjpy-m15-example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 19 - LIVE AUDJPY M15 Example.mp4" type="video/mp4"></video> <video id="live-usdcad-m30-example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 20 - LIVE USDCAD M30 Example.mp4" type="video/mp4"></video> 

<video id="live-eurusd-m30-example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 21 - LIVE EURUSD M30 Example.mp4" type="video/mp4"></video> <video id="live-usdjpy-m15-example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 22 - LIVE USDJPY M15 Example.mp4" type="video/mp4"></video> <video id="live-audusd-h1-example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 23 - LIVE AUDUSD H1 Example.mp4" type="video/mp4"></video> <video id="live-gbpusd-m15-example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 24 - LIVE GBPUSD M15 Example.mp4" type="video/mp4"></video> <video id="live-eurchf-m30-example" class="tut"  width="324" height="216"  controls><source src="../../assets/videos/MarketMaker/Lecture 25 - LIVE EURCHF M30 Example.mp4" type="video/mp4"></video>

<br>

## MARKET MAKER 2

### LIVE USDCAD M5 Example

### LIVE EURGBP H1 Example

### LIVE EURJPY M30 Example

### LIVE AUDJPY M15 Example

### LIVE USDCAD M30 Example

### LIVE EURUSD M30 Example

### LIVE USDJPY M15 Example

### LIVE AUDUSD H1 Example

### LIVE GBPUSD M15 Example

### LIVE EURCHF M30 Example