---
title: chaotic-markets
date: 2023-04-13 14:52:15
tags: [trading]
category: [prepare, knowledge]
---

<br>

# Fractal Trading - Mastering Price Action and Beyond 

- [New Framework of Analysis](#new-framework-of-analysis)


## Fractal Price Action 

Fractal analysis is based on 
- Fractals
- Butterfly Effect
- Attractors & Repellers
- Statistical Stability 
- Others 

|fractal|a pattern that repeats inside/outside itself across multiple scales|

See different timeframes in same chart without switching timeframes by switching perspective 



Financial markets are chaotic.

View markets through lens of [Nonlinear Dynamics]() and [Fractal Geometry]() to understand nature of chaos.

Chaos is not random. It's a higher degree of order that seems to be random, has simple logic at the base.


#### Nonlinear Dynamics 

Functions represent relationship among variables (dependence, )

Takes input (x, independent variable), perform action f(x), generates output (y, dependent variable)

Functions can be deterministic or stochastic 

- Deterministic:      1. output depends only on input, and 2. same input always yields same output 

- Stochastic:         1. output depends on input and on an element of chance, and 2. same input can yield different outputs 


Paradox of Chaos 

Unpredectable behavior emerges from the iteration of deterministic functions 

Chaos emerges out of order  

Iteration of functions - feed the output back into the input 


#### Atttractors & Repellers

Qualitative Dynamics - study of the long-term behavior of the orbit 

Attractors attracts the function's orbits

Repellers repel the function's orbits 

Orbit gets progressively larger ==> tends toward infinity from one which is a repeller, unstable fixed point

Orbit gets progressively smaller and smaller ==> tends toward zero which is a attractor, stable fixed point


#### Time Series Plot of Dynamical Systems

Allow us to observe how variables change over time 


#### The Geometry of Iteration 

Use Cobweb diagram to visualize functions 

Linear function f(x) = mx + b 

The slope (m) of a linear function changes the stability of the fixed point.

Positive slope: if m is > 1 OR m < -1 then unstable fixed point (Repller)

Negative slope: if 0 <= m < 1 OR -1 < m < 0 then stable fixed point (Attractor)


If slope = 1 and y-intercept = 0 then all points are fixed 

If slope = 1 and y-intercept != 0 then there are no fixed points 

If slope = -1 there is a neutral fixed point, doesn't attract or repel the orbit 

#### The Logistic Equation

Explains paradox of chaos, how iteration of deterministic functions can generate unpredictable behavior 

Exponential growth model (rabbits)

Every generation population doubles ( f(x) = 2x )

f(^n)(x0) = x02^n

f(Generation)(Seed) = x0(Rate of Reproduction)^n

f(^1)(2) = 2(2^1)
f(^1)(2) = 4

Understand the model with varying rates of reproduction (n)

Rate > 1 Population grows infinitely 
Rate = 1 Population stays same 
Rate < 1 Population decays infinitely 

Resources are limited 

A = Annihilation Parameter (Maximum Population)
P = Population 

f(P) = rP(1 - P/A)

If P becomes A, growth is zero 


Logistic Equation

f(x) = rx(1-x) 

This model allows for exponential growth and limits growth with annihalation parameter 

#### Parameter Variation 

#### Chaos Defined 

1. Rule that generates the dynamical system must be deterministic

2. Orbit must be aperiodic

3. Orbit must be bounded between two values 

4. System must have sensitive dependence on initial conditions (Butterfly Effect)

#### Butterfly Effect 

When small variations in initial conditions produce large differences in orbit after a few iterations 

#### Bifurcation Diagrams

Shows final states of orbit for various values of r 

Period-doubling route to chaos 






























