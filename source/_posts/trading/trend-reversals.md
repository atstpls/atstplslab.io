---
title: trend reversals
tags: ["analysis"]
category: [reference]
---

<br>

|||
|-|-|
|CYCLE ANALYSIS|- Gradual decrease in cycle amplitude while cycle length remains constant<br>Gradual decrease in cycle length while cycle amplitude remains constant |
|RETRACEMENT SIZE|- Larger retracements show trend is losing power|
|CANDLESTICK RANGE|- Average True Range (ATR) decreases as trend loses power|
|PRICE PERSISTENCE|- Narrowing or Widening candlestick rnnges |
|CANDLE STOCHASTIC|- Candles closing larger and larger shadows |
|BODY TO RANGE RATIO|- Lower Body-to-range ratio indicates uncertainty |
|OVERALL TREND ANGLE|- Accelerating or decelerating angles lead to exhaustion and reversal |
|STRONG BARRIERS|- Barrier strength, relevance, and history can indicate reversal |
|DEPTH OF TREND OSCILLATIONS|- Low oscillatory components are unsustainable |
|VOLUME SPREAD ANALYSIS|- LVWR at strong barrier indicates reversal<br>LVNR indicates low interest<br>HVNR indicates aggressive attack was suppressed|
|VELOCITY DIVERGENCE|- HH in Price Action and LH in Oscillator (RSI)<br>- LL in Price Action and HL in Oscillator (HL)|
|VOLUME DIVERGENCE|- HH in Price Action and LH in Volume Histogram <br>- LL in Price Action and LH in Volume Histogram |
|DOW THEORY|- Change in relationship among HIGHs and LOWs signal a change in trend|
|ELLIOT WAVE THEORY|- Five clear waves while 5th presses barrier<br>- Self-fulfilling prophecy effect - regardless of scientific validity, if enough traders believe and act as if they are true has an effect|
|WYCKOFF METHOD|- |
|CHART PATTERNS|- |
|HARMONIC PATTERNS|- |

<br>

### MEAN REVERSION
The further the price is from a moving average, the more likely it is to revert to that moving average.

<br>

### PSYCHOLOGICAL HIGH/LOW
These levels are set during the first session for the week, which in Cryptocurrency intraday trading starts at 21:00 UTC on the Saturday night, when the Sunday morning Asian session opens, and ends 8 hours later at 05:00 UTC the following morning. The highest and lowest prices of this session become important levels of support and resistance during the following week.

<br>

### DAILY HIGH/LOW/OPEN/M-PIVOT
RDH/RDL and M-Pivots are calculated based on the previous trading day's price action, and along with the Daily Open price, they act as important levels of support and resistance.

<br>

### AVERAGE DAILY RANGE (ADR)
Has the ADR been reached? Has price exceeded its expected range of movement?

<br>

### TRADER'S DYNAMIC INDICATOR (TDI)
If the RSI line forms a sharkfin at or outside the upper or lower volatility bands, it is likely to move back towards the Market Base line, and perhaps continue to the opposite side.

<br>

### PATTERN LEVELS
Be aware of which level in the Pattern price is currently at - is it at a peak formation, for example? Be aware that there are patterns within patterns at different timeframes - the higher the timeframe the greater the conviction.

<br>

### RETEST OF PREVIOUS AREAS
Is price returning to a previously tested level like the RDH/L, Daily Open or the Psychological High/Low.

<br>

### NEWS CYCLES
Be aware of general sentiment on sites like Cryptocraft or even Youtube. If price is in a sideways ranging movement, the markets may be waiting for important news to emerge which could determine the direction of price.

