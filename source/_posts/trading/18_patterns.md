---
title: 18 patterns
tags: [trading]
category: [training, reference]
---

<video id="prax-elements" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Praxeological Elements.mp4" type="video/mp4"></video> <video id="extremes" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Extremes.mp4" type="video/mp4"></video>  <video id="midpoints" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Midpoints.mp4" type="video/mp4"></video>

 <video id="frequencies" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Frequencies.mp4" type="video/mp4"></video> <video id="inward-frequency" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Inward Frequency.mp4" type="video/mp4"></video> <video id="outward-frequency" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Outward Frequency.mp4" type="video/mp4"></video>

<video id="supply-demand-zones" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Precise Supply and Demand Zones.mp4" type="video/mp4"></video> <video id="dynamic-frequency-breakout" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Dynamic Frequency Breakout.mp4" type="video/mp4"></video> <video id="reverse-engineering" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Reverse Engineering.mp4" type="video/mp4"></video>

<video id="wick-expansion" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Wick Expansion.mp4" type="video/mp4"></video> <video id="pressure-zones" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Pressure Zones.mp4" type="video/mp4"></video>  <video id="volatility-shift-line" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Volatility Shift Line.mp4" type="video/mp4"></video>

 <video id="solid-structure" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Solid Structure.mp4" type="video/mp4"></video> <video id="fake-structure" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Fake Structure.mp4" type="video/mp4"></video> <video id="manipulation-pattern" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Manipulation Pattern.mp4" type="video/mp4"></video>

<video id="fractal-bar" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Fractal Bar.mp4" type="video/mp4"></video> <video id="inside-bar" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Inside Bar.mp4" type="video/mp4"></video> <video id="outside-bar" class="tut"  width="360" height="216"  controls><source src="../../assets/videos/PriceAction/PAT1/Outside Bar.mp4" type="video/mp4"></video>

<br>


##### Praxeological Elements

##### Extremes

##### Midpoints

##### Frequencies

##### Inward Frequency

##### Outward Frequency

##### Precise S&D Zones

##### Dynamic Freq Breakout

##### Reverse Engineering

##### Wick Expansion

##### Pressure Zones

##### Volatility Shift Line

##### Solid Structure

##### Fake Structure

##### Manipulation Pattern

##### Fractal Bar

##### Inside Bar

##### Outside Bar

