---
title: 9 principles
tags: [trading]
category: [training, reference]
---

<video id="anchoring" class="tut"  width="360" height="216"  controls><source src="../assets/videos/PriceAction/PAT1/Anchoring.mp4" type="video/mp4"></video> <video id="extension" class="tut"  width="360" height="216"  controls><source src="../assets/videos/PriceAction/PAT1/Extension.mp4" type="video/mp4"></video>  <video id="validation" class="tut"  width="360" height="216"  controls><source src="../assets/videos/PriceAction/PAT1/Validation.mp4" type="video/mp4"></video>

 <video id="simple-line-extrapolation-and-non-equidistant-line-extrapolation" class="tut"  width="360" height="216"  controls><source src="../assets/videos/PriceAction/PAT1/Simple Line Extrapolation and Non-Equidistant Line Extrapolation.mp4" type="video/mp4"></video> <video id="frequency-shifting-and-frequency-tuning" class="tut"  width="360" height="216"  controls><source src="../assets/videos/PriceAction/PAT1/Frequency Shifting and Frequency Tuning.mp4" type="video/mp4"></video> <video id="convergence,-square-fields-and-clusters" class="tut"  width="360" height="216"  controls><source src="../assets/videos/PriceAction/PAT1/Convergence, Square Fields and Clusters.mp4" type="video/mp4"></video>

<video id="cross-dimensionality" class="tut"  width="360" height="216"  controls><source src="../assets/videos/PriceAction/PAT1/Cross Dimensionality.mp4" type="video/mp4"></video> <video id="vectorization" class="tut"  width="360" height="216"  controls><source src="../assets/videos/PriceAction/PAT1/Vectorization.mp4" type="video/mp4"></video> <video id="reverse-engineering" class="tut"  width="360" height="216"  controls><source src="../assets/videos/PriceAction/PAT1/Reverse Engineering.mp4" type="video/mp4"></video>

<br>


##### Anchoring

##### Extension

##### Validation

##### Simple Line Extrapolation

##### Frequency Shifting

##### Convergence, Square Fields

##### Cross Dimensionality

##### Vectorization

##### Reverse Engineering