---
title: chaos-theory-in-trading
date: 2023-04-13 14:52:15
tags: [trading]
category: [prepare, knowledge]
---

<br>

Problem: 

- Price action is **Chaotic**, but it can produce **Order**
- When to use broad perspective and when to use narrow focus 

Tactics: 

- Use **Descriptive theories** (how things are) rather than **Normative theories** (how things should be)
- Use Fractal Price Analysis which is based on Chaos Theory (fractals, butterfly effect, attractors/repellers, statistical stability, etc.)
- We switch our perceptions in order to see different timeframes in the same chart without switching timeframes
- We logically interpret candles (represent price action) to see what's *implied* in them, to extract out price vectors, highs/lows, broad price action
- Use Broad Aspects to provide **context**, Use Narrow aspects to provide **precision and timing**


<br>



Framework:

- Intrinsic properties, extrinsic forces, and fractal analysis 



# Fractal Trading - Mastering Price Action and Beyond 

- [Fractal Price Action Analysis](#fractal-price-action-analysis)
    - [Absolute Intrinsic Properties](#absolute-intrinsic-properties)
    - [Relative Intrinsic Properties](#relative-intrinsic-properties)
    - [Extrinsic Forces](#extrinsic-forces)
- [Fractal Price Action Properties](#fractal-price-action-properties)
- [Frequencies DFB and DFS](#frequencies-dfb-and-dfs)
- [Butterfly Effect](#butterfly-effect)
- [Fractal Manipulation Patterns](#fractal-manipulation-patterns)
- [Advanced Fractal Candles](#advanced-fractal-candles)
- [Linear Regression Channels](#linear-regression-channels)



## Fractal Price Action Analysis

- A **fractal** is a 

- Intrinsic properties can be absolute (in isolation) or relative (evolving over time)

- Extrinsic forces in market will alter interpretation of intrinsic properties (location/trend phase, overextension, price barriers, prior PA activity)

- Words > Phrases > Paragraphs > Narrative  Candles > Price Vectors > Highs/Lows > Broad Price Action 


Fractal analysis is based on 

|||
|-|-|
|[Fractals]()|A pattern that repeats inside/outside itself across multiple scales |
|[Butterfly Effect]()|When Small changes drive large changes in chaotic systems |
|[Attractors & Repellers]()| attracts or repels the function's orbits |
|[Statistical Stability]()||
|[Others]()|| 

<br>

Candles have directional bias/sentiment - bullish or bearish 

Intrinsic candle properties 
    - bias/sentiment (qualifies bullish or bearish)
    - body size (quantifies)
    - volatility 
    - volume 

<br>

- [Absolute Intrinsic Properties](#absolute-intrinsic-properties)
- [Relative Intrinsic Properties](#relative-intrinsic-properties)
- [Extrinsic Forces](#extrinsic-forces)

<br>



### Absolute Intrinsic Properties 

In isolation 

Assume path of least resistance = Ambiguity Reduction Concept 

Price goes to nearest of 4 prices first

Open > Nearest Extreme > Opposite Extreme > Close 

If extremes are same distance from opening, it's indecisive 

|||
|-|-|
|Bias/Sentiment|Qualification|
|Body Size|Quantification|
|Range|Volatility|
|Shadows|Pressure|
|Body%|Uncertainty|
|Shadow Symmetry/Body Position|Balance|


<br>

### Relative Intrinsic Properties

Evolve over time, gives insight into price action narrative 

|-|-|
|Bias/Sentiment Change|Continuation, Reversal, Disappearance (shift in control of market)|
|Body Size Change|Increase, Decrease, Sustain (power of dominant market player)|
|Range Change|Increase, Decrease, Sustain| Upward Progression (HH + HL) Uptrend in a lower timeframe<br><br> - Downward Progression (LH + LL) Downtrend in lower<br> - Expansion (HH + LL) Expanding pivot (increase in volatility) in lower timeframe<br> - Contraction (LH + HL) Contracting Triangle (decrease in volatility) in lower timeframe<br> - Upward Expansion (HH + FL) Support with upward leeway<br> - Upward Contraction (LH + FL) Support with selling pressure<br> - Downward Expansion (FH + LL) Resistance with downward leeway<br> - Downward Contraction (FH + HL) Resistance with buying pressure<br> - Forward Sustain (FH + FL) Sideways market<br> - Patterns can be symmetrical or asymmetrical|
|Shadow Change|Increase, Decrease, Sustain<br><br> - Upper shadow changes -> selling pressure<br> - Lower shadow changes -> buying pressure|
|Body%|Increase, Decrease, Sustain<br><br> - Changes in Body% are directly proportional to certainty<br> - Low Body% indicates uncertainty<br> - High Body% indicates certainty|
|Shadow Symmetry/Body Position|Explicit/Implicit Gaps<br><br> - Low Body Position + Low Body% indicates Uncertainty with Bearish Bias<br> - High Body Position + Low Body% indicates Uncertainty with Bullish Bias| 

<br>

### Extrinsic Forces 

This can strengthen, weaken, or change analysis of intrinsic properties 

- [Location or Trend Phase](#location-or-trend-phase)
- [Overextension](#overextension)
- [Price Barriers](#price-barriers)
- [Prior Activity](#prior-activity)

<br>

#### Location or Trend Phase 

Acceleration leads Velocity which leads Price Action 

Signal Alteration 
Length Between Extremes 
Self-Similarity


Dow Theory              Accumulation and Distribution, volume confirms price ( HH+HL divergence, etc. )
Elliot Wave Theory      Fractal 5-3 patterns 
Wychoff Method          Spring, UTAD, 


<br>

#### Overextension

The implicit forces behind price action change before price

Divergence analysis
    - Velocity divergence  ( Price HH + RSI LH >> BEARISH, Price LH + RSE HL >> BULLISH, Price HL + RSI LL >> BULLISH, Price LH + RSI HH >> BEARISH )
    - Acceleration divergence - acceleration leads velocity, 
        Signal Alternation 
        Lengths between Extremes - Longer signals leads to more significant reversals 
        Signal Self-Similarity - 
    - Volume divergence - Price and volume histogram comparison 
        Cumulative Volume Delta  - difference between bullish/bearish volume
        (Price HH + CVD LH >> buying exhaustion, Price LL + CVD HL >> selling exhaution )
        (Price HL + CVD LL >> selling absorption, Price LH + CVD HH >> buying absorption )

<br>

#### Price Barriers 

Traditional     - subjected to Test/Switch/Retest mechanics 

Containment     - highlight movements of overextension 

Geometrice      - lines, channels grounded in geometry 

Numerical       - derived from mathematical formulas (Bollinger Bands, Moving Averages)

Fractal         - Mix geometric and numerical 

Horizontal / Sloped     - don't change, based on past price geometry ( LRC )

Dynamic         - change and come from fractal and numerical  ( VWAP )

Before a barrier can be sustainable, unsustainable, or ambiguous 
    - intrinsic properties before provide context to what happens at barrier 

At the barrier 
    - intrinsic properties interacting with barrier 
    - test/switch/retest Dynamics
    - location of candles respective to barrier 

After the barrier
    - DFB and other signs price is getting away from barrier 

<br>

#### Prior Activity 

Prior activity
    - is candle in an upward or downward price movement 
    - is it same as location or trend phase 


<br> 

### Fractal Price Action Properties 

The fractal dimension (self-similarity dimension) of price varies between 1 and 2. 

Price movements with lower fractal dimension are more predictable.

Fractal dimension of price has an objective element and subjective element.

Coastline Paradox - the length of a landmass is not well defined because it's fractal, so its length depends on the size of the tool that measures

Once you choose a ruler size, you can reach an objective measurement. But the choice of the ruler size is subjective, and there is an infinite set of valid ruler sizes.

Rough Price Action - intrinsic properties of candles evolve rapidly 

Smooth Price Action - intrinsic properties of candles evolve slowly 

### Frequencies DFB and DFS 

Dynamic Frequency Breakouts / Dynamic Frequency Stops 

Frequency allows precision of lower timeframes without switching timeframes 

Dynamic Frequency lines capture numbers of shadows without breaking the line 

Downward Price Movement 
    - Lower Dynamic Frequency either gets broken to downside or stops momentarily
    - Upper Dynamic Frequency remains intact. When it breaks, there is a sign of reversal

Upward Price Movement 
    - Upper Dynamic Frequency either breaks to upside or stops momentarily 
    - Lower Dynamic Frequency remains intact. When it breaks, there is a sign of reversal 

In upward price vectors, DFB in lower frequencies point to reversal 
                         breakout of DFS in upper frequencies point to continuation
                         
In downward price vectors, DFB in upper frequencies point to reversal 
                           breakout of DFS in lower frequencies point to continuation 

### Butterfly Effect 



We want to use the small changes to take advantage of the following large changes 

Reliable DFBs occur with low to medium volatility 

### Fractal Manipulation Patterns 

2 Basic Manipulative Maneuvers 

1. Bull/Bear Trap - 

    Bull Trap - Buyers are induced to upside and price reversed 

    Bear Trap - Sellers are induced to downside and price reversed 

2. Bull/Bear Raid - 

    Bull Raid - Large traders push price to upside to induce small buyers 

    Bear Raid - Large traders push price to downside to induce small sellers 

Hybrid Fractal Manipulations - combinations of traps and raids in lower timeframes  

    - Bull Trap 
    - Bear Trap 
    - Bull Raid 
    - Bear Raid  

Raids with low/medium volatility 

All fractal candles indicate a fractal manipulation, but not all fractal manipulations indicate a fractal candle 

<br>

### Advanced Fractal Candles 

Fractal candles identification requires perception of bias and shadow changes.

    - Bullish Bias + Lower Low = Bullish Fractal Candle 

    - Bearish Bias + Higher High = Bearish Fractal Candle 

Fractal candles imply contradiction of dissonance in terms of intrinsic properties (bias and shadows)

    - Reverse Fractal Candle (RFC) - fractal candle bias disagrees with prior price action activity 
                                   - Bullish bias + LL + Downward Prior Activity 
                                   - Bearish bias + HH + Upward Prior Activity 
                              
    - Continuation Fractal Candle (CFC) - fractal candle bias agrees with prior price action activity 
                                        - Bullish bias + LL + Upward Prior Activity 
                                        - Bearish bias + HH + Downward Prior Activity 

Bullish Fractal Candles 
    - Downward Progression
    - Downward Expansion
    - Expansion 

Bearish Fractal Candles 
    - Upward Progression 
    - Upward Expansion 
    - Expansion 

Fractal Bar 
    - indicates reversal divergence in lower timeframe
    - Bearish Fractal Bar - HH than previous candle + bearish body  
    - Bullish Fractal Bar - LL than previous candle + bullish body 

 Fractal Trap 

 Fractal Butterfly 
    - expanding fractal candle with high body% 
    - volatility already picked up in lower timeframe, but not in higher timeframe 

Fractal Signal Alternation 

Integrated Fractal Candles 

1. Expanding Range Dynamics 

2. High Body% 

3. DFB & DFS Patterns 

4. Butterfly Effect 

5. Low Fractal Dimension 

6. Overextension 

7. Final Trend Phase 

8. Reaction without Penetration of Price Barrier 

9. Presence of Manipulation 

10. Unsustainable Motion 



### Linear Regression Channels 






















Financial markets are chaotic.

View markets through lens of [Nonlinear Dynamics]() and [Fractal Geometry]() to understand nature of chaos.

Chaos is not random. It's a higher degree of order that seems to be random, has simple logic at the base.


#### Nonlinear Dynamics 

Functions represent relationship among variables (dependence, )

Takes input (x, independent variable), perform action f(x), generates output (y, dependent variable)

Functions can be deterministic or stochastic 

- Deterministic:      1. output depends only on input, and 2. same input always yields same output 

- Stochastic:         1. output depends on input and on an element of chance, and 2. same input can yield different outputs 


Paradox of Chaos 

Unpredectable behavior emerges from the iteration of deterministic functions 

Chaos emerges out of order  

Iteration of functions - feed the output back into the input 


#### Atttractors & Repellers

Qualitative Dynamics - study of the long-term behavior of the orbit 

Attractors attracts the function's orbits

Repellers repel the function's orbits 

Orbit gets progressively larger ==> tends toward infinity from one which is a repeller, unstable fixed point

Orbit gets progressively smaller and smaller ==> tends toward zero which is a attractor, stable fixed point


#### Time Series Plot of Dynamical Systems

Allow us to observe how variables change over time 


#### The Geometry of Iteration 

Use Cobweb diagram to visualize functions 

Linear function f(x) = mx + b 

The slope (m) of a linear function changes the stability of the fixed point.

Positive slope: if m is > 1 OR m < -1 then unstable fixed point (Repller)

Negative slope: if 0 <= m < 1 OR -1 < m < 0 then stable fixed point (Attractor)


If slope = 1 and y-intercept = 0 then all points are fixed 

If slope = 1 and y-intercept != 0 then there are no fixed points 

If slope = -1 there is a neutral fixed point, doesn't attract or repel the orbit 

#### The Logistic Equation

Explains paradox of chaos, how iteration of deterministic functions can generate unpredictable behavior 

Exponential growth model (rabbits)

Every generation population doubles ( f(x) = 2x )

f(^n)(x0) = x02^n

f(Generation)(Seed) = x0(Rate of Reproduction)^n

f(^1)(2) = 2(2^1)
f(^1)(2) = 4

Understand the model with varying rates of reproduction (n)

Rate > 1 Population grows infinitely 
Rate = 1 Population stays same 
Rate < 1 Population decays infinitely 

Resources are limited 

A = Annihilation Parameter (Maximum Population)
P = Population 

f(P) = rP(1 - P/A)

If P becomes A, growth is zero 


Logistic Equation

f(x) = rx(1-x) 

This model allows for exponential growth and limits growth with annihalation parameter 

#### Parameter Variation 

#### Chaos Defined 

1. Rule that generates the dynamical system must be deterministic

2. Orbit must be aperiodic

3. Orbit must be bounded between two values 

4. System must have sensitive dependence on initial conditions (Butterfly Effect)

#### Butterfly Effect 

When small variations in initial conditions produce large differences in orbit after a few iterations 

#### Bifurcation Diagrams

Shows final states of orbit for various values of r 

Period-doubling route to chaos 









#### Basic Performance Appraisal 

Sharpe Ratio measures quality of trader's return 

Long-term edge 

Risk = Volatility = Standard Deviation of Returns 

The Risk-Return Tradeoff 

The market is dynamic and probabilities vary over time 

Focus more on the process than the outcome, because that's all you can do 

Must stay humble and accept the chaotic market, focus on best actions you can take 

The illusion of adaptability is when you think you can easily adapt to changing conditions of market 

Need Benchmark (SP500) to outperform 

Performance Appraisal is differentiating Skill, Chance, and Risk Tolerance 

Higher Sharpe means better efficiency in terms of risk, and therefore more sustainable 

Add question content 


#### Reversal Mechanics of Price Action in Low Fractal Dimension 

1. Spot the DFB 
2. Phase analysis 
    - dell theory 
    - elliot wave pattern (fib extension tool) 
    - wychoff 
    Analysis: Potential for bullish reversal 
3. Linear Regression Channel 
    - bullish pressure 
    - andrews pitchfork (double)
    - 2 non-obvious barriers from LRCs 
    - 2 non-obvious barriers from pitchforks 
4. RSI / CDV 
    - HL with RSI-LL signals bullish 
    - LL with RSI-HL signals bullish 
    - Velocity reversal plus DFB both do too 
5. Salient Absorption 
    - HL with CDV Lower 
6. Salient Exhaustion 
    - Lower with CDV-Higher 





#### Velocity Acceleration and Hyper Integration 



#### Fractal Dimension Filter and Temptations of a Stochastic Game 



#### Self-Similar Manipulation in Higher Fractal Dimension 



#### Unsustainable Price Feigenforks and Low Fractal Dimension 



#### When Low Fractal Dimension Falls and What To Do



#### Integration as an Ambiguity Reduction Mechanism 



#### Contronting Opposite Trade Ideas 



#### Catching the Pullback with VWAP LRCs and Forks 



#### Why Price Might Reverse in Non-Obvious Levels 



#### Hyper Integrating LRCs Pitchforks Volume Profile and VSA 























