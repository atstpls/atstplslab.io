---
title: ads, trackers, and script servers
tags: [security]
category: [protect, knowledge]
date: 2023-02-18 06:17:50

---

<br>

### ADS

Ad companies put their ads on website and in emails. When users view the ads, their device sends information that they sell.

This can be clickstream data, phone location data, or where the user was browsing.

FIX: use AdBlockers like uBlock Origin and Ghostery.


### TRACKERS


Third-party sites collect and organize information about users browsing to multiple different websites. This information shows trends on where a specific user browses and what they do at each website.

Companies that own large advertising networks like Google and Facebook can track users not only by browsing history, but also through all the services they provide (Facebook, Google Search, YouTube, Instagram, Gmail, Chrome, etc). This data can be combined and mined to make user profiles so that personalized ads can be served via ad networks (Adsense, Admob, and DoubleClick) that are embedded in millions of websites.

Here are four common methods used for tracking: Cookies, Web Bugs, Fingerprinting, Supercookies

Web bugs - small digital image files embedded in a web page or email. Additional methods include graphics, banners, buttons, and other HTML elements such as frames, styles, scripts, input links, etc.

When a user opens a page or email with a web bug, their web browser or email reader automatically downloads the resource which requires the user’s computer to send a request to the third-party server. This request contains identifying information about the user, browser, or computer which allows the host to keep track of the user.

Email marketers, spammers, and phishers use web bugs to verify that an email is read. An email containing a web bug is sent to a large list of email addresses and requests for the embedded resource identify which email addresses are valid and also that the email made it past spam filters.

Fingerprinting - used to build user profiles based only on observed patterns of characteristics associated with your browser. Fingerprinting distinguishes and recognizes individual browsers by collecting and analyzing various information that can be obtained from the browser such as:
Screen resolution, timezone, extensions, fonts, cookies, system platform/language, 

Combined, this information can create a kind of fingerprint — a signature that can be used to identify you or your computer and track your activity across multiple websites. Since this information is collected passively, tools that focus on behavioral indicators such as Privacy Badger are the most effective.

Privacy Badger looks for third parties tracking user across multiple websites.

To avoid web bugs, either read emails offline or force your reader to display emails in plain-text. The contents of plain-text email messages are not interpreted as embedded HTML code so opening them does not initiate any communication.

You can test your browser using tools like Panopticlick.
