---
title: areas of interest
tags: [security]
category: [protect, security]
date: 2023-02-18 06:17:48
---


- [Deception Technologies](#deception-technologies)
- [Detection Types](#detection-types)
- [Threat Hunting with Indicators](#threat-hunting-with-indicators)
- [Incident Response Automation](#incident-response-automation)
- [Detection and Response with SIEM](#detection-and-response-with-siem)
- [Methodologies for Cloud Environments](#methodologies-for-cloud-environments)
- [Methodologies for Post-Exploitation](#methodologies-for-post-exploitation)

<br>

#### Deception Technologies 

Deception technologies--specifically active defense tools--continue to effectively impede, decelerate, and counter adversary post-exploitation activities. They provide a unique solution that leverages automation, denial and deception, counterdeception, intelligence, counterintelligence, and offensive countermeasures to improve incident response, protection of critical data and assets, and deter intruders from further attacks.  

Vulnerability management and cyber hygiene are basic security precautions and are unlikely to stop a determined adversary.  Authorities do not currently have sufficient technology or legal avenues to successfully deter cybercriminals or bring them to justice.  organizations need legal, inexpensive options for detecting intruders and actively defending their most valuable assets once their network has been compromised.  

We provide guidance for organizations who wish to improve their detection and response times and effectiveness using low risk, low cost active defense tools and techniques. One example is the "Guide for Implementing ADHD" which provides a starting point for any organization aiming to expand their capabilities while also helping them break the routine of spending more money on security controls that offer less protection.  It explores a specific collection of active defense tools, their advantages, capabilities, and roles in countering intruders who have successfully compromised an organization's network, and the general purpose, configuration, operation, and overview of the necessary steps for implementation.

There are three general elements of active defense tools: 

- [Detecting Intrusions](#detecting-intrusions)
- [Obtaining Attribution](#obtaining-attribution)
- [Executing a Counterstrike](#executing-a-counterstrike)


##### Detecting Intrusions 


##### Obtaining Attribution 


##### Executing a Counterstrike 

This is limited to mitigative counterstrikes—those used to counter and deter active threats, rather than retributive counterstrikes which are intended to punish.  In order for organizations to remain within the legal and ethical boundaries of computer network defense, the goal of mitigative counterstrikes should be to match the level of response as closely as possible to the level of the threat.  All offensive countermeasures we recommend are restricted to obtaining only the information necessary for reporting to law enforcement for the facilitation of civil and criminal penalties.

##### ADHD 

The open source security distribution known as the Active Defense Harbinger Distribution (ADHD) is a well-documented, well-supported, and user-friendly collection of active defense tools and concepts.  Using this collection of tools provides organizations with a base knowledge of active defense tool types and uses while also eliminating potential configuration and interoperability problems that frequently arise when selecting a large number of individually supported tools for operation on the same system.

There are a number of active defense techniques that can be used to counter threats that are wholly external and do not have access to an organization’s internal network.  For example, fake employee profiles can be placed on social networking sites and associated with a fake corporate email account that is monitored for spear-phishing attempts. However, we recommend that organizations focus strictly on addressing the threat of an intruder, detected or undetected, who has successfully penetrated an organization's internal network.

There is little guidance for organizations regarding utilization of tactics such as denial and deception (D&D), counterdeception (CD), counterintelligence (CI), and offensive countermeasures (OCM) that could be used effectively to combat this threat. Active defense tools currently exist that are specifically designed to deceive, deter, and ultimately thwart intruders who have compromised a network.  These tactics and techniques can be put in place to better secure their networks using low cost, low risk tools.  

1. Improving intelligence gathering and attribution capabilities

    First, organizations must continuously improve their ability to gather intelligence and obtain attribution.  It is essential that organizations know and understand who their adversaries are, their capabilities, their intent, and the specific threats they present to the network.  The ability to identify the source and analyze the extent of a compromise is crucial to quick and effective incident response, mitigating damage, and restoring normal services.  Intelligence gained can also be used to both improve an organization’s security strategy and improve information sharing with law enforcement to ensure the parties responsible do not evade civil and criminal penalties.  Security incidents resulting in the intruder being identified and held responsible for their attack serve as excellent deterrents to potential Actors in search of an easy target.  While authorities remain both technologically and legally ill-equipped to seek justice for victims of cybercrimes, it is essential that organizations improve their ability to gather information on intruders including IP address, geographic location, mission objectives, and tactics.  The more information that can be obtained on an intruder, the greater chance they can be countered, identified, and forced to face civil and criminal penalties.

2. Improving incident response speed and accuracy

    Organizations also must improve their incident response capabilities and ensure they can successfully mitigate damage from ongoing intrusion and continue their business core processes uninterrupted.  The ability for an organization to protect their systems, critical data, and customer information is more important now than ever before.  Early detection of an incident can prevent or limit potential damage to systems and data and reduce the time and effort required to contain, eradicate, and restore normal network operation.  Rapid, focused incident response to an intruder on the network can stop an intruder from reaching his objective without affecting customer services and other business processes.  This results in better protection of systems, critical data, and customer information which is closely tied with business reputation and customer confidence in light of the many recent high-profile data breaches.

3. Developing a security strategy that is more efficient in time, cost, and effort
    
    Organizations must develop a time-, effort-, and cost-efficient security strategy for defending the network and mitigating intrusions which provides advantages to network defenders and ultimately deters intruders from continuing their mission.  The leverage of automation and open source active defense tools increases time, effort, and cost for Actors while decreasing time, effort, and cost for organizations.  Automation saves time for defenders for detection and response and must be utilized to more quickly and effectively hide and protect critical systems and data on the network.  Cheap, open source tools that leverage automation, deception, and mitigative counterstrikes should be used to increase time, effort, and cost for an attacker.  Not only will these improvements save time, effort, and cost allowing the organization to operate more efficiently, but they make it a less attractive target to intruders.

Our approach provides an assembly of guidance and reference material for organizations needing an introduction to active defense tools and techniques and the many advantages they offer.  By explaining the current threats these organizations face and the advantages these tools provide while also presenting guidelines and examples of placement, configuration, and operation, organizations can more easily utilize these valuable tools and gain the required skills and experience to maximize security and organizational performance.

Modern actors are able to operate anonymously with cheap and efficient tools, and without fear of retaliation by either the victim or various national and international legal authorities.  Without the threat of attribution, countermeasures, rising operational costs, or resulting legal action, adversaries will continue testing business technical capabilities, defeating their security controls, and achieving their objectives.

Traditional information security strategies focusing largely on vulnerability mitigation and signature-based tools contribute heavily to this problem.  Not only have they proven to be insufficient and disproportionately more expensive than the cost of carrying out cyberattacks, but they have failed to emphasize the importance and necessity of increasing cost and risk for the attacker.  An environment of cheap, offensive tools and capabilities has emerged and Actors are leveraging it successfully while the majority of organizations are not due to fear of potential legal complications, lack of knowledge, or lack of understanding.  The strategies of most organizations focus on defending and protecting by ensuring confidentiality, integrity, and availability rather than leveraging freely available tools and techniques to confuse, delay, and perform reconnaissance on an intruder.

While commercial solutions that are virtual machine-based and/or specialize in externally focused intelligence collection and analysis have seen some success, they do nothing to address the enormous gap between the cost of implementing cyberdefense, currently in the billions, and the cost of carrying out cyberattacks, which in many cases is virtually nothing.  Spending on cybersecurity alone has exploded yet large organizations continue to fall victim to evolved and determined Actors.  Increased spending on cyberdefense is counterproductive and a low cost solution is needed.  

Every organization needs a solution that deters intruders, detects their presence on the network, and provides a response that protects critical systems and data while also obtaining attribution for the attack.  State non-state actors penetrating an organization’s network must be stopped from reaching their objectives, identified as the source of the attacks they carry out, and held responsible for their actions by national and international authorities.

Too many resources have been allocated towards prevention and consequence management efforts rather than incident response and countermeasures.  Organizations that aren’t detecting or responding to an intruder in a timely and effective manner are missing opportunities to deter the intruder, obtain attribution and threat intelligence, and mitigate the effects of the intrusion.  

Traditional tools and strategies are failing to address the threat of intrusion in three primary areas: deterrence, detection, and response.

    Deterrence      - Actors are able to operate anonymously with no threat of attribution by the target organization or law enforcement. Traditional tools are not effectively increasing the cost, time, and effort required for Actors to complete their objectives. Defenders are placing little or no emphasis on deception while adversary operations are built entirely on deception.  Defenders not well-versed in deception not only encourage more attacks, but also contribute to more defeats.  

    Detection       - Once an intruder has gained a foothold on a network, signature-based tools fail to detect custom malware and legitimate account usage—two techniques frequently used in high-profile data breaches.  Lateral movement throughout the network can also be accomplished without detection by signature-based tools.  Additional problems associated with these tools are high numbers of false positive alerts which help intruders evade detection.

    Response        - Automation is not fully utilized by organizations for fear of blocking legitimate traffic.  Instead of automated, immediate responses, many organizations today rely on manual response processes which are not always as effective and take much longer to accomplish.  Traditional responses are also too passive and consist of rerouting, isolating, and even abandoning the network.  Additionally, responses are threat-generic and do not take into consideration the identity, tactics, and objectives of the intruder.  Finally, traditional responses do not utilize fail-safe techniques to adequately protect critical data and assets.

There are three primary causes to the problem of intruders having the ability to easily pivot through compromised networks undetected.  

   1. Most organizations have become comfortable with what is known and what is currently being advised by government agencies and information security industry leaders.  The private sector has been increasingly reliant on the typical traditional defenses (antivirus, IDS & IPS, firewalls, web gateways) because these tools have been used to guard against cyber threats for years.  Organizations have become good at hardening systems and checking for attack and malware signatures.  However, these tools are insufficient for detecting and responding to intruders.  They are also static, known to the attacker, easy to evade, and unnecessarily expensive.  Many organizations associate increased cost with increased protection while others want the perceived security provided by vendor support contracts and special equipment.  Finally, traditional tools do not fully utilize automation for fear of blocking legit traffic which slows response times and increases the probability of human error during a response.

   2. There is a shortage of guidance from industry leaders and government agencies on how an organization is to deal with an intruder who has penetrated their network.  Organizations lack the legal means to retaliate due to the Computer Fraud and Abuse Act (CFAA) and are therefore hesitant about implementing technologies that may result in adverse legal action.  The CFAA is interpreted differently by different courts and offensive “hack back” strategies bring with them a significant risk of civil and criminal liability.  Second, the U.S. currently does not have a cyberdeterrence policy or official legal guidance or doctrine regarding offensive countermeasures (OCM) available to organizations.  Third, the guidance that is available is extremely passive in nature.  The U.S. Department of Justice provides strictly defensive-based strategies as guidance for organizations dealing with an intrusion including rerouting traffic, isolating segments of the network, and even abandoning the network.  Finally, there is no emphasis on the benefits of intelligence gathering, denial and deception, counterdeception, and counterintelligence.  In short, organizations are encouraged to deploy inadequate tools that alert when a network is under direct attack—which is too late—and then told to “duck and cover” to avoid further damage deploying these types of tools. 

   3. The majority of the private sector is either uninformed about active defense tools, do not understand their true value to the organization, or do not know how to implement the tools in their network.  Without knowledge of alternative tools, organizations have no choice but to spend more money on the latest “next generation” firewall, IDS, and antivirus solutions.  The existence of, need for, and advantages provided by active defense tools have not yet been successfully conveyed to organizations.  Furthermore, many organizations do not understand why they should leverage these automated, inexpensive tools or how they can begin using them to counter the threat of an intruder with access to their network.

    Business Impacts

    The most significant consequence of this problem is the financial costs resulting from an intruder who successfully executes a cyber-enabled attack.  Different cyber-enabled attacks, such as cyberfraud, cyberespionage, cybersabotage, or cyberterrorism, carry with them a variety of attack-specific consequences but financial costs are common to all and generally increase as the size of the organization increases.  Depending on the business, this financial cost can result from fines due to compliance implications, loss of shareholder value, legal exposure and lawsuits, fraud, extortion, or unavailability of services. Additional business impacts include loss of intellectual property and damage to a business’s brand or reputation.

    The risk of collateral damages to an organization’s network resulting from the implementation and operation of active defense tools is very low.  Active defense tools are only in place to interact with intruders and do not affect normal network traffic and users.  The risk of false positives is also extremely low since anyone interacting with these tools by definition is a malicious user.  The risk of flawed implementation, operation, or maintenance is low and can be reduced further by enforcing the use of standard operating procedures and continuous training.

    The risk of offensive countermeasures exceeding the organization's legal right to protect its own property is the primary risk of using active defense tools.  This could result in substantial financial loss through lawsuits, penalties, or other legal fees.  In order to reduce these risks, an organization should employ warning banners, use controlled countermeasures proportionate to the threat, and ensure their legal team is consulted before and during tool deployment and operation.  The use of warning banners defining the boundaries of the organization’s network and actions that will be taken against intruders significantly reduces the organization’s legal exposure after an intrusion.  Incident response team members must ensure that all actions in response to an intruder are controlled, justified, and proportionate to the threat—restraint is key for managing legal risk.  Finally, the organization’s legal team must be involved throughout the entire process of deploying and operating these tools to provide legal analysis and recommendations in the context of the organization’s acceptable levels of risk.  This goal can be accomplished with policies and procedures designed to ensure proper planning and authorization is part of the deployment and operation processes and security group personnel are provided with specialized training, guidelines, and standard operating procedures.

    The new capabilities and tactics being used by Actors are changing every day and organizations face the enormous challenge of addressing these threats in a way that will be effective in numerous attack scenarios with completely different objectives.  Today’s actors can easily evade IDS and other signature-based controls using custom obfuscation, encryption and bypass techniques.  Reputation-based controls are ineffective against new URLs and drive-by downloads.  Application deny-lists are ineffective against encrypted binaries or legitimate apps and processes that have been hijacked.  Even when organizations detect and eradicate malware, an intruder has most likely already gained access through legitimate measures such as VPN or other remote access solutions.  Detection tools continue to be defeated or evaded and intruders remain a serious threat to organizations and their critical systems, networks, and data.  

    Without a solution, traditional security controls will continue to come up short in deterring and responding to the cyber threat.  Intruders will continue to penetrate networks and execute their mission with no additional consideration of time, risk, cost, or sophistication of attack required.  Once they gain a foothold on the network, they will face fewer obstacles, less uncertainty, and be able to move laterally within the compromised network evading detection and response.  Intruders will become faster and more efficient at locating critical systems and data and accomplishing their objectives.  After the success of the attack, those responsible will continue to evade criminal and civil legal action and serve as an inspiration for others to accomplish similar attacks.  Actors will repeat and perfect this process making it faster, cheaper, easier, and more lucrative while organizations, the ones who can survive, will be outmatched, outgunned, and eventually out of money.

    Network segmentation, role-based access control, application allow-listing and auditing systems for suspicious files are examples of basic security measures designed to prevent intruders from moving within a compromised network.  In addition, organizations should already be reviewing and maintaining up-to-date vulnerability management plans, patch management plans, and incident response plans.  Active defense tools and techniques are recommended as an augmentation to industry best practices.

	It is also assumed that the organization has the available staff, time, and budget to implement, operate, and maintain the active defense tools and techniques reviewed in this project.  Although no additional staff should be needed to maintain these tools, an organization may require additional training for new hires or additional security consulting services following changes to their systems or network architectures.

    In order to help organizations address the security gaps discussed, they must first be introduced to active defense tools and their capabilities and advantages.  The methodology, approach, and strategy focuses on educating organizations first and providing them a path forward should they choose to implement them in their network.  

    There are three general variables involved in computer attacks, commonly referred to as detection time, response time, and attack time.  The formula Dt + Rt > At states that in order for an attacker to succeed, the time required to complete the attack must be less than the time needed for detection plus the time needed for response.   This means that organizations want to decrease detection times, decrease response times, and increase the time required for an attack.

    This approach aims to provide a small suite of tools which can be used to change all three of these variables to bring about a significant improvement in overall security posture.  More in-depth approaches focusing on only one aspect of the solution do not properly arm an organization with the tools needed to succeed nor enable it to take immediate action.  With this approach, an organization can gain an understanding of the role these technologies play in reducing security risks and can begin taking steps to implement these technologies at once, without significant cost or experience.

    The approach was selected for its simplicity, scalability, and low cost.  A basic guide gives an organization an introduction to active defense tools, their role in countering intruders, and their benefits for the organization as well as the private sector as a whole.  This guide can be used by organizations of all types and all sizes as it is not tailored to specific network types or workforce skill sets.  Each tool's description, configuration, and recommended uses are provided for easy setup and implementation.  Security personnel are provided tool-specific, hands-on training that results in an even greater understanding of tool uses and configurations.  The low cost and general availability of the ADHD software is yet another reason why organizations will be more likely to take action with this approach.  All that is needed to begin using these tools on an organization's network is the guide provided, virtual software, and Internet access.

    Routine scenario-driven exercises will be needed following initial training to improve the speed and efficiency of response procedures.  Organization security and incident response teams must practice identifying, attributing, and neutralizing threats in the organization’s network under various circumstances.  Continuous assessment and evaluation of coordinated responses will reveal processes and systems that require additional training and improvement.  New developments in attacker technology and strategy will change the effectiveness of countermeasures so security teams must stay abreast of current cybersecurity defense techniques and practices.

    The actual costs to the organization are minimal, no extra manpower is needed, and the cost for deploying the tools and training team members are extremely low compared to the cost of a successful, undetected network intrusion.  Financial loss resulting from exceeding legal rights and downstream liability are a major concern and must be addressed by careful consideration from a legal perspective.  The risk of false positives is low since any user or system interaction with the ADHD system is by definition unauthorized activity. Finally, the risk of collateral damages to an organization’s network is low since no legitimate users or systems should be interacting with the ADHD system.

Risk Mitigation
    
    The low cost of the software and minimal implementation requirements of implementing the project ensures a very low risk of tool deployment issues.  Enforcing the use of standard operating procedures and continuous training will further reduce the risk of flawed implementation, operation, or maintenance.  The use of warning banners, controlled countermeasures proportionate to the threat, and legal team consultation through each phase of the project will mitigate the risk of an organization exceeding its legal rights to protect its property.  Implementing traffic control rules on the edge firewall and ensuring timely and effective responses to all tool detections reported to the security team will mitigate the risk of downstream liability.  Verifying correct tool deployment and operation will mitigate both the risk of false positives and collateral damages to other network systems.  Security awareness training for operation and maintenance teams will reduce the risk of authorized users and systems interacting with the ADHD server causing collateral damages to an organization’s network.  Additionally, security awareness training for all users will reduce the risk of false positives caused by users intentionally or unintentionally connecting to the ADHD system.
    
    If performance is unacceptable, the rollback process consists of decommissioning the virtual machine or removing it from the network environment.  This can be accomplished with no disruption to other services and systems on the network.  An alternative to the original implementation plan is to have the team members attend training first so that they could configure and deploy the tools instead of hiring a security architect.  However, this alternate plan may adversely affect management support as it would be the first installation of this type for the team members and the risk of implementation failure would increase.


#### Detection Methods

Many examples where intruders were free to move about the compromised network, sometimes for over a year. Seasoned Actors are adept at circumventing perimeter defenses, quietly surveying the environment, and carrying out their mission undetected.  Conventional tools in support of defensive and reactive strategies have failed to protect their networks and critical data from the adversaries that pose the greatest cybersecurity threat.  These companies remain attractive targets for cybercriminals and have received insufficient guidance on how to address this problem. 

Two broad methods of detection include intrusion detection in a hunting context and intrustion detection in an incident response context.

There are four types of detections:

Modeling                        - Baseline anomalies, events or groups of events that stand out from normal activity in the environment
Threat Indicators               - Files and infrastructure reported to be associated with malicious activity: IP addresses, domain names, file hashes, etc. 
Configuration Changes           - New changes on a system such as new processes, new connections, new protocols
Threat Behaviorial Analytics    - Patterns in logs and data resulting from overall tradecraft used by adversaries

Modeling and Threat Indicators are easily avoidable by a skilled or determined adversary.  Many top tier actors leverage legitimate programs, utilize approved protocols, and disguise their traffic for the sole purpose of not creating anomalies--to masqureade as legitimate applications, network traffic, and infrastructure. This means they are not the best choice in hunting contexts where the assumption is the actor is embedded and has up to that point successfully remained undetected.  However, these two types are useful in incident response contexts where now the presence of specific anomalies and indicators directly related to the current incident can quickly identify areas being targeted or controlled by the adversary.

Configuration Changes and Threat Behavior Analytics are excellent for hunting contexts as they are almost impossible for the adversary to avoid or overcome.  As the adversary attempts to manipulate and control systems, they are forced to create configuration changes such as new network connections, new processes, and new events on the system. As they use the various techniques that are required for gaining access to and moving within an environment, they create observable patterns in logs and data that experienced threat hunters can search for and detect. These two types can be combined with Modeling and Threat Indicator detections to great effect during IR/Triage contexts when changes and behaviors directly related to the current incident are now available to enhance scoping and containment efforts.


#### Explain the concept of "hunting hypotheses." How do you formulate and test hypotheses during a threat hunting exercise?

For threat hunting in general, we start with a threat model which includes the assets we seek to protect and the adversary tradecraft that will most likely be used to try to obtain them. We then collect data from the environment that we anticipate will identify the use of this tradecraft so that we can prevent it and counter it when it occurs.

We perform this in three phases: 

1. Study and pattern out adversary behaviors, quantify the tradecraft observed, and test the techniques in our environment to build detection content and determine the most effective response actions

2. Build high-fidelity detections that are both actionable and universal to ensure accurate alerts that are capable of detecting techniques used by adversaries of all skill levels

3. Leverage past investigations, research, and testing to provide key artifacts, the most useful context, and the most effective response actions to the analyst working an incident

This approach improves the quality of tools and knowledge delivered to the analyst enabling swift, effective responses that have been tested and tuned in our environment.  In the event of a detection, we have high confidence it is a true positive, it contains the context and artifacts needed for reporting and response, and allows management to make time-sensitive decisions.

To formulate and test:

We use local and crowdsourced attack data to provide insight into the TTPs that are successfully utilized by malicious actors. We research and simulate the chosen technique to better understand how its use and application.  System and component-level documentation, reports and blogs that explore and detail the different ways the technique can be used by an adversary, emulation on test systems and networks---these all facilitate a greater understanding of the technique and its use in different environments. 

developing detection content, testing it and tuning it

We then begin the process of developing detections and determine the most effective actions we can take to reduce the overall impact of incidents involving these techniques.  The searches, queries, scripts, and response actions we develop must be tested and verified as the most effective methods to use in our environment.  These are developed over days and weeks, not created as the incident unfolds.

We develop context and reference requirements needed for timely reporting and response.  We improve speed by having our research, scripts, queries, and response actions staged and ready to provide to the analyst working an incident.  This way the analyst doesn’t have to waste time researching the technique, building the same scripts and queries that have already been built, or trying to determine the best responses for the technique being used.

We improve accuracy by ensuring the correctness and completeness of the information provided to the analyst.  We also ensure consistency with this method by ensuring that all analysts get the same resources, scripts, queries, and response actions. All technical documentation and OSINT sources should be carefully selected based on their value to an incident responder.  When everyone is on the same page using the same resources and analysis techniques, the results stay consistent and are more easily used and understood. 

With these improvements in SPEED and ACCURACY, the analyst is able to quickly get a clear picture of the incident and is immediately equipped with the most effective actions to take that will reduce its overall impact.

We maintain a knowledge base--an organized, searchable platform containing information on previous hunting efforts, search syntax and performance, and references to other internal or external resources that can be used by other members of the team. We adding all work to the Knowledge Base to prevent duplicating work and resources. is transferring knowledge of adversary capabilities, infrastructure, motives, goals, and resources to partners, analysts, tools, processes to provide a more focused approach for defense.


Effective techniques will often become very popular which forces vendors to develop additional and/or enhanced detection mechanisms.  This in turn drives actors as well as security researchers to identify all the different ways the technique can still be used even when protections are in place. This requires us to revisit techniques regularly and determine if new knowledge exists that would allow the detections we've designed to be bypassed.

perform pivoting and link analysis by leveraging the APIs of multiple free and commercial online investigation tools. It is also great at giving a visualization of the data points discovered during an investigation and the relationships between them.  


#### Threat Hunting with Indicators 

Indicators are unique pieces of information that have some type of intelligence value. Their presence could indicate the possibility of a compromise or attack. Depending on how they are used, they have the ability to either enhance or impede incident detection, analysis, and response. Indicators can be Tactics, Techniques, and Procedures (TTPs), tools, artifacts, or atomic indicators.

To have value, an indicator must indicate something---an attack technique, a compromised system, a specific actor, a specific family of malware, etc. For example, an IP address can be used to indicate either a suspected compromised website, an exploit kit landing page, a C2 node, or a data exfil endpoint... we need to know which one it is in order to use it to our advantage.

Examples of indicators that might trigger a threat hunting investigation:

- [Indirect C2](#indirect-c2)
- [Post-exploitation Activity](#post-exploitation-activity)
- [Authenticated Remote Code Execution](#authenticated-remote-code-execution)
- [Evasion Techniques on Disk and the Network](#evasion-techniques-on-disk-and-the-network)

##### Indirect C2 

There are three general components involved in C2 traffic: Connection, C2 Server, Process.  With advanced C2 infrastructure, implants are provided with multiple redirectors for calling home, none of which are the actual C2 servers. This allows attack operations to continue in the event a domain/IP is discovered and blocked by defenders. If one redirector is blocked, another is used and the implants can continue to check in and receive tasks from their C2 servers.

Indirectly communicating with a C2 server requires placing some type of asset between the victim and the C2 server to obscure the identity and location of the C2 server as well as circumvent proxy restrictions.

Domain Fronting         - Uses the front end servers of CDNs such as Amazon, Google, Microsoft Azure to relay C2 traffic
Third Party Services    - Uses webmail, cloud storage, and social media platforms to relay traffic to/from the C2 server
Redirectors	            - This could be an EC2 micro instance or server at any location whose only job is to pass on traffic it receives to/from the C2 server
DNS C2                  - Hostnames from a domain an attacker controls are passed to various DNS servers to be resolved. The requests and responses contain C2 traffic
Proxy and VPN Tunnels   - Proxy tunnels (HTTP, SOCKS, etc) pass Layer 4 (TCP/UDP) traffic to/from the source and destination. An application on the source machine works with an application on the proxy host creating a tunnel that passes Layer 4 traffic between the two.

Redirectors also obfuscate the identity and location of the actual C2 servers using domain fronting, third party services, or stand-alone servers whose only function is to relay traffic between the victim and C2 server. Even if the redirectors are discovered and located, the true location of the C2 servers remains hidden.
An adversary must make a victim system initiate a connection to an external system they control called a C2 server. All connections initiated by the victim can be tracked back to a responsible process on the system and tied to a destination which is or redirects to the C2 server. 

Frequency analysis of:
  - connection type (synchronous/asynchronous), interval (short, regular/long, irregular), encryption ( encrypted/tunneled), protocol (standard/non-standard), and pattern (mode/outlier)
  - C2 server count (single/multiple, multipurpose/function-segregated), certificates (none/self-signed/valid signed), domains (newly registered/direct IP/dynamic DNS/3rd party)
  - Process integrity (native/external, mode/outlier), executable (none/self-signed/valid signed, mapped to disk/in-memory only)

Frequency Analysis is comparing different characteristics of data to identify anomalies and interesting events. This is a very effective technique when searching large sets of data and can be used to spot newly observed/registered domains, external services, and unusual port and protocol usage.

Link Analysis is using the relationships between nodes or events to locate anomalies, outliers, or related traffic.

Time Series Analysis looks at patterns of data points across time intervals such as beaconing and other unusual events or sequences.


##### Post-exploitation activity 

Collecting data from memory is the only way to get the true state of a system at the time of capture and is an excellent hunting technique. Physical memory can contain all data a system processed including files, network traffic, user input, etc. It is basically a collection of code and data used on a system over time.

Several advantages to the analyst including the ability to find:

- Malware in unpacked/unencrypted form
- Injected code that doesn't exist on disk
- Hidden and exited processes
- Closed network connections and sockets
- Cached and deleted files

Post-exploitation agents such as Meterpreter, Empire, PoshC2, and Beacon provide access to hundreds of built-in tools and the ability to run pre-built modules, pass sessions, and import custom scripts and programs from a single interface. Malware utilizing executables on disk to leveraging legitimate programs to keep all untrusted code entirely in memory. Many of these operations involve performing memory injection.

Post-expoitation Agent Behaviors

    Agent Creation          - Staged Payloads, Stageless Payloads
    Agent Evasion           - Thread Start Address, RWX Permissions, Module Stomping, Obfuscate and Sleep
    Agent Operation         - Process Migration, Post-Exploitation Jobs
    Agent Implementation    - Shellcode Injection, Reflective DLL injection, Memory Module, Process Hollowing, Module Overwriting


##### Authenticated Remote Code Execution

Once an adversary obtains valid credentials, lateral movement can be accomplished by executing just one command on a remote system. This one command is frequently a PowerShell one-liner that downloads and runs a stager on the victim via web delivery. Since an authenticated account is used to execute code, this one remote command blends in with daily system administration tasks and can be very difficult to detect.

There are many different ways to perform lateral movement, but this will focus on methods that use authenticated sessions. To track this activity, we need to understand what techniques can be used, ensure we have an accurate way to detect them when they occur, and collect the results in a way that will allow us to answer key questions during the response---such as "What accounts and systems have been compromised at this point in time?"

Authenticated RCE Behaviors

  - Windows Management Instrumentation
  - Service Control Manager
  - Windows Remoting
  - Remote Registry
  - Remote File Access
  - Task Scheduler
  - Remote Desktop
  - MMC20.Application DCOM


##### Evasion Techniques on Disk and the Network 

Evasion on Network Behaviors

    Encryption	                - Used to hide the contents of traffic
    Proxy Tunnels               - Used to hide the true destination of an application's traffic
    VPN Tunnels                 - Used to hide the true destination of all traffic
    VPN With Cloaking Device    - Used to hide the true source of the traffic

Identifying Network Evasion Techniques

- Hosts creating large amounts of traffic with little or no DNS requests
- Hosts that make the majority of connections to one or several external servers on specific ports
- Hosts that are using protocols that are unusual for the ports being used (UDP on 443 in this example)
- Hosts having little or no listening ports and services


Evasion on Disk Behaviors

    Using an Encrypted VM           - Encrypt VM hard disk or store the machine's disk image file (VDI, VMDK, VHD, HDD) and settings file (.vbox) in an encrypted container
    Using a Hidden VM               - VM files can also be stored in a hidden volume which is a volume that's created within the free space of another encrypted volume
    Booting to a Hidden OS          - Hard drive has two partitions, the first is a decoy OS and the second holds both an encrypted filesystem and a hidden OS. The second partition appears to be and functions as storage and only runs the hidden OS when provided with a specific password
    Booting to a Live OS            - Live OS's run using only the filesystem on the device (USB, CD/DVD) and the computer's RAM to operate. These can be used to make changes to the existing system as well as operate without making any changes to the system

Identifying Disk Evasion Techniques

- Encrypted VM	                Large amounts of encrypted data, presence of VM/disk encryption software
- Hidden VM	                    Large amounts of encrypted data, presence of VM/disk encryption software
- Hidden OS	                    Large encrypted partitions on hard drive or additional drives with disk encryption
- Live OS	                    Unified Extensible Firmware Interface (UEFI) Secure Boot is a successor of Basic Input Output System (BIOS). When UEFI is enabled, only signed bootloaders are allowed to run and booting from a CD/USB is not possible



How do you distinguish between normal network behavior and potential threats?


We base our analysis and detection methods on behaviors and configuration changes--the two strongest detection types--driven by processes, credentials, and network connections. Heuristic guardrails direct our focus to key observations and artifacts, along with the most effective means of validating them.

We ensure the authenticity of observations and artifacts by prioritizing high-integrity data sources such as memory collection and centralized logging. We verify the authenticity of observations with content validation, construct valideation, and abstraction.

Multiple different tools and methods provide us the ability to see how each actor behavior depends on and supports the others and to build a more complete picture of actor operations. 


KEY OBSERVATIONS 

Observing conditions and actions is the most direct, most trusted way to find and track those attempting to operate within environments. Instead of amassing technical details and checking behavior boxes, tools and processes are fine-tuned to gather and leverage the most important and most accurate information. Conditions and actions impact how behaviors are displayed as well as how they are observed

Establishing the POINT of ORIGIN, the AGENT, the CONTEXT, the ACTION and the RESULT for each behavior helps us construct the most accurate representation of reality. Which in turn allows us to formulate the most effective game plan for hunting and defense.

AUTHENTICITY

We use CONSTRUCT VALIDATION--multiple distinct measurement techniques--which combined with ABSTRACTION, work to substantiate conclusions and expose the weak links in our processes, revealing key patterns only visible from higher ground.

CONTEXT 

We can't analyze behaviors without considering the external factors that may be causing them. Different internal factors can result in the same or similar behaviors. And the completeness of analysis depends on understanding how these factors impact behaviors. 

So behaviors by themselves are not enough. We need CONTEXT and MEANING. Context in which the behaviors are observed, displayed.. And the RESULTS of behaviors, so we can abstract out key observations and patterns.


#### Incident Response Automation

Responders are required to plan and execute operations against adversaries faster than they can react.  Automation plays a key role:

- [Isolate Systems](#isolate-systems)
- [Interrogate Endpoints](#interrogate-endpoints)
- [Identify Sensitive Information](#identify-sensitive-information)
- [Evaluate File Integrity](#evaluate-file-integrity)
- [Evaluate Infrastructure Integrity](#evaluate-infrastructure-integrity)



<br>

##### Isolate a system

  - Blocking all network connections that are not used to manage the agent rather than just blocking connections to a list of domains and IP addresses
  - Scoping and containment process as they will be using the same knowledge and artifacts to determine the presence of actors in the environment.

Isolating compromised servers has been the traditional approach for years, but modern incident response requires more specific objectives such as what process or credentials does the actor control, what security context do they have, and what other processes and credentials are now available to the actor as a result of the web shell. For example, if the actor uses the web shell to read an AWS credential file, isolating the servers does nothing to contain the compromise, which would now be able to continue via the AWS environment.  


##### Interrogate an Endpoint

An initial assessment requires answering some basic questions about the system, the most common ones being:

  - What network connections is the host making?
  - What processes are currently running on the host?
  - What users are active on the system?
  - What files have been recently created, modified, or accessed?
  - Have any persistence methods have been configured on the host?


##### Identify Sensitive Information 

When sensitive information is exposed to the public, the primary goal is identifying data that can be extracted by someone who discovers it and searches through it with bad intentions. An effective response requires automating some of the manual steps involved with investigating public repositories containing sensitive information. Automation extracts out anything that could be useful to an adversary---usernames, email addresses, SSH keys, AWS credentials, URLs, domains, IP addresses, hostnames, etc. By providing management with an accurate assessment of what was available as a result of the exposure, we can increase the effectiveness of our initial response and corrective actions.

For example, when sensitive information is exposed to the public:

- Identify all code and data that can be extracted by and exploited by an adversary
- Use tools such as bulkextractor and manual searching to find usernames, email addresses, SSH keys, AWS credentials, URLs, domains, IP addresses, hostnames, etc.
- Provide management with an accurate assessment of what was available as a result of the exposure to guide initial response and corrective actions


##### Evaluate File Integrity 

Digital Signatures

On Windows 7 and later versions, all native PE files, including EXEs and DLLs, that are running in processes, device drivers, and services should be signed by Microsoft. A file signed with a valid, trusted certificate confirms authenticity and origin:

- Microsoft signs a file to prove it is authentic
- Microsoft signs a file with their private key to prove it came from Microsoft

Windows PE Digital Signatures

Windows PE files are signed in one of two ways:

    Embedded - A digital signature (Microsoft uses Authenticode) is embedded inside the PE. Embedded signatures are placed at the end of the signed file and can be verified various ways such as Get-AuthenticodeSignature, sigcheck.exe, signtool.exe, DigiCertUtil.exe, explorer.exe, and others.

    Catalog - A hash of the PE can be found in a security catalog (.CAT) file. Catalog-signed files do not have an embedded digital signature so they may not pass verification on all the tools above. Verifying them on Windows 7 hosts requires a tool such as SysInternals sigcheck.exe which looks up the Authenticode hash of the file in its associated catalog file and verifies the signature of the catalog file.

Identifying a Trojaned File

    - Hash used to verify the file's integrity Digital Signature used to verify the publisher's identity
    - Code signing does not tell you whether the file is malicious or not, it only confirms the identity of the publisher and if the code has been modified.
    - Digitally signed code is backed by a certificate issued by a trusted third party (CA)
    - Unsigned code may include publisher data but if it doesn't provide any evidence of origin or file integrity, we cannot trust that it is what it says it is
    - Time-stamp signing allows code to be trusted after a private key is compromised or the certificate is revoked. It proves the certificate was valid and trusted at the time of the timestamp.


##### Evaluate Infrastructure Integrity 

Malicious websites fall into three general categories:

    Phishing	                    User is tricked into submitting credentials, authorizing access, or giving up other private information
    Malware via Social Engineering	User is tricked into installing malware on victim host
    Malware via Exploit	            Browser or plugins are exploited to install malware on victim host


Malicious sites that host exploits can be more difficult to analyze especially when they use:

    - Encoding and encryption to hide shellcode data and script content
    - Obfuscation and anti-sandbox techniques to evade analysis and reverse engineering
    - System profiling to identify and deny/redirect repeat visitors, security analysts, and systems that cannot be exploited

If a malicious site is using one or more of these techniques, it may be difficult to obtain a sample of the payload and provide a complete analysis of what happens when a visitor's system is exploited. A tool called Thug makes this easier by mimicking the behavior of a vulnerable browser.

This enables automated identification/recovery of:

    - Cookies	        Set either by JavaScript (local storage) or by HTTP responses that include a Set-Cookie header. The browser stores the user-specific cookie which is retrieved and transmitted to third-party domains on future visits
    - Web bugs	        Files or HTML elements embedded in a web page or email that force a client application to make a request containing identifying information to a third-party server. These can be images, graphics, banners, buttons, and other HTML elements such as frames, styles, scripts, input links, etc.
    - Fingerprinting	Passively collecting and profiling browser characteristics in order to distinguish and recognize individual browsers (and users). This technique uses User agent strings, screen resolution/depth, timezones, fonts, plugins and versions, etc. to create a signature that can be used to identify and track user activity across multiple websites
    - Supercookies	    Also called zombie cookies --- any technology other than a standard HTTP Cookie that is used by a server to identify clients. Examples include Flash LSO cookies, DOM storage, HTML5 storage, and other methods of storing information in caches or etags

Automation also optimizes identification of Malware Self-Defending techniques:

  - Packers         Compress a file's original data to conceal information such as strings and imports
  - Crypters        Use obfuscation and encryption to hide information such as file paths, URLs, shellcode, etc.
  - Protectors      Use anti-analysis techniques such as anti-debugging, anti-virtualization, anti-dumping, and anti-tampering to prevent reverse engineering and analysis


#### Detection and Response with SIEM 

Information from events and logging come in many different forms.  Information discovered about actors and infrastructure in the past may not hold true today.  Analysts are continually required to not only organize the information needed, but also understand their historical context.  Nothing does this better than SIEMs.

Some examples:

- Malicious domains and IPs frequently get taken over and pointed to a sinkhole.  Although OSINT tools may report relationships between this data point and others, the infrastructure hosting the sinkhole and the other "related" domains pointing to the same sinkhole are not relevant to our investigation

- IP addresses belonging to VPNs and VPSs are constantly being shared by millions of different users every day.  Attempting to pivot to samples or infrastructure "related" to one of these IPs will almost always be a waste of time

- IP addresses that belong to hosting services and reverse proxies are a similar problem---multiple tools will find relationships between an IP and the thousands of domains it is hosting, but these are not the types of pivots that will lead to related infrastructure and samples

- Third party domain registrars like GoDaddy, Hover, and DreamHost manage reservations of domain names for many different users.  If the adversary uses one of these services, we cannot pivot to other
users of the service or other domain names registered using the service as they are in no way related to our investigation

- If Dynamic DNS is being used and a domain is pointing at multiple different IP addresses in a short period of time, pivoting on the IP addresses won't give us the information we're after.  Also, the
registrant information will belong to the DDNS providers

- Compromised legitimate sites being used for Delivery, C2, and Exfil will also have registrant information that is not related to the adversary


Heuristics without details are ineffective and details without heuristics are paralyzing.  SIEMs allow us to combine the right details with the right heuristics, allowing threat hunters to take vast amounts of information and parry it down to the small number of key observations and artifacts they need to successfully complete their mission.

SIEMs are used to collect and organize information in a way that threat hunters can more easily extract out the authentic observations needed for decision-making:

Point of origin     - used to establish the source of this information
                    - security tools, external communication, event logs, or conclusions drawn from other information

Agent               - represents someone or something initiating an action
                    - attributes representing an actor, such as an IP address, a device, a file, a process, or a set of credentials

Context             - set of circumstances and details relevant to an AGENT performing an ACTION
                    - Normal conditions, abnormal conditions, the frequency of events, the way they occur over time, or the use of evasion

Effort              - encompasses the behavior or action being performed
                    - known actor technique, an authenticated session, an authentication process, execution of code, or an exploit

Result              - outcome or payoff that comes from the EFFORT expended
                    - reconnaissance, the ability to control credentials or processes, The transfer of data or the installation of a backdoor



#### Methodologies for Cloud Environments

The traditional perimeter of an organization's network has all but disappeared. A modern organization has resources that exist both on premise and hosted on cloud provider networks spread across the world. Users are able to access these resources with multiple devices and applications by sufficiently proving their identity.

Therefore, any user or application that satisfies the proof of identity requirements can access these resources. In some cases this proof is possession of a PIV card and knowledge of its PIN. In other cases it is just a single password, API key, or access token.

In addition, a variety of tokens are used to carry out authenticated operations in most cloud environments--some are valid for up to 90 days.  Once an actor has them, they don't need a password or multi-factor authentication to use them.  Refresh tokens can be used to obtain access tokens for months.  PRT tokens can be used to generate access tokens for any application.  

The combination of universal access to applications over the Internet and the types of credentials that are issued post-password/MFA checks present a unique set of problems during IR/triage.

Because of this, we focus primarily on the following:

- [Exploiting Single Factor](#exploiting-single-factor)
- [Bypassing Initial Authentication Process](#bypassing-initial-authentication-process)
- [Bypassing Authentication Completely](#bypassing-authentication-completely)

##### Exploiting Single Factor

An organization's network boundaries are now defined by the identities that can access cloud resources. Current credentials can be searched for, discovered, and used to access accounts protected by only a single factor.

  - Users often employ weak or duplicate passwords that can be guessed or brute-forced
  - Users can be tricked into providing credentials to an adversary or running malware that steals them
  - Account recovery procedures can allow an adversary to bypass the normal authentication process
  - With XSS, the adversary is using the ability to run JavaScript in the victim's browser to send a cookie or session token to a remote server so they can create an authenticated session from there.
  - With CSRF, the adversary is taking advantage of the browser being authenticated to a target site. If the victim is logged in, any request made to the site originating from the victim's browser will be successful.

A single factor is one piece of data that can be used to access a web service in an authenticated context. They can be discovered by scanning and reconnaissance techniques, guessed if a password or key, phished with various social engineering methods, or bypassed using account recovery procedures.

Here are the most common ways an adversary will obtain a single factor in order to gain unauthorized access to a web service:

    - Find Single Factor	Current credentials can be searched for, discovered, and used to access accounts (Breaches, dumps, repos, scanning)
    - Guess Single Factor	Users often employ weak or duplicate passwords that can be guessed or brute-forced (Common, reuse, default)
    - Steal Single Factor	Users can be tricked into providing credentials to an adversary or running malware that steals them (Email, SMS, chat)
    - Bypass Single Factor	Account recovery procedures can allow an adversary to bypass the normal authentication process (Email, SMS, security questions)


Once a user performs initial authentication and receives a session credential, this cookie or token is all that is needed to access the service and can therefore also be considered a single factor. Two common ways an adversary can obtain a session credential to gain unauthorized access to a web service:

    - Steal Session Creds	With Cross Site Scripting (XSS), the adversary is using the ability to run JavaScript in the victim's browser to send a cookie or session token to a remote server so they can create an authenticated session from there
    - Forge Session Creds	With Cross Site Forgery Request (CSRF), the adversary is taking advantage of the browser being authenticated to a target site. If the victim is logged in, any request made to the site originating from the victim's browser will be successful


Best practices for both corporate and personal cloud accounts:

  - Use a long, complex password with 2FA enabled
  - Verify accurate and up-to-date account recovery info such as email, phone
  - Review recent activity, authorized/logged in devices
  - Review payments, subscriptions, 3rd party access authorizations
  - We'll begin looking at each of these techniques more closely and identify potential opportunities for an adversary to obtain credentials to internal and external services we use.

Basic Authentication	    - Base64-encoded username and password included in each request. If not included, the server sends a response with a WWW-authenticate attribute in the header
Digest Authentication	    - Username, password, and nonce is used to create hash which is sent to the server. If not included, the server sends a response with a WWW-authenticate attribute in the header and a nonce
Windows Integrated Authentication	    - Also called NTLM authentication, the server sends either a Negotiate (Kerberos) or NTLM WWW-authenticate header and a nonce. Browser returns Base64-encoded username, hostname, domain, service, and the results of the hashing functions which the server can validate with the domain controller
Form-based Authentication   - Uses code external to the HTTP protocol for authenticating the user. The application is left to deal with prompting the user for credentials, verifying them, and deciding their authenticity
Certificate Authentication  - Client application holds a certificate with a private key and the remote web application maps that certificate's public key to an account. Client sends its certificate to the application which checks the digital signature, certificate chain, expiration/activation date and validity period, and a revocation status check
    

##### Bypassing Initial Authentication Process

There are three methods we frequently see used to access web applications either after or without completing the full authentication process:

  - Cookies	        Client receives a cookie after authentication which is passed in subsequent requests
  - Session JWTs	Client receives a token after authentication which is passed in subsequent requests
  - OAuth Tokens	Client is given a token which is used to perform actions on behalf of a user

JWTs are great for authorization but if obtained by an unauthorized user or application, can be used to bypass initial authentication like in this example.

JWT Best Practices
To mitigate/prevent Token Sidejacking:

  - Avoid passing token in URL, instead pass token in Authorization header with Bearer eyJhb...
  - If logging headers, ensure tokens are removed or obscured
  - Integrate user context into tokens such as a random string generated during the authentication phase
  - Set tokens to have short lifetimes
  - Use token deny-listing to revoke compromised tokens or logged out users

Additional recommendations:

  - Encrypt JWTs to protect user-specific information it contains
  - Store in sessionStorage rather than localStorage
  - Have clients sign tokens to prevent reuse
  - Sign with complex symmetric keys to prevent dictionary attacks
  - JWTs are good for API services in which clients make frequent requests in a limited scope. They also work well for validating a user's identity via a third party such as in the OAuth protocol.

Oauth	        - Anyone presenting this token to the web application has access to the resources associated with the token

Password Authentication	Username and Password	                                - Can be guessed, cracked, sniffed, stolen, etc.
Public Key Authentication	                                                    - Digital certificate	Can be lost, copied, compromised, stolen, etc., not practical for most web apps
Multi-Factor Authentication	Password + Time-based One Time Password, SMS	    - Can be phished for a one time web session
MFA with Tamper-resistant Hardware Devices	                                    - Smart Card, U2F	Browser compromise required for an unauthorized web session
    
Tamper-resistant hardware devices are designed so that the private key never leaves the device. This ensures that someone must have possession of the device and know its PIN in order to use it.

OAuth Token Best Practices
  - Do not hard-code tokens into scripts or applications
  - Do not transmit tokens in query strings of URLs in GET requests
  - Regularly rotate and expire tokens
  - Use the most limited authorization scope required when issuing tokens

One-Time Passwords	        - One-time passwords are shared on-the-fly between two digital entities using an out-of-band (OOB) communication such as SMS, email, or application. After a server validates the username and password, it generates an OTP that can only be used once and sends it to the client via the chosen OOB method
Hardware Tokens	            - The hardware token contains an algorithm, a clock, and a seed or a unique number which is used to generate the numbers displayed for a specific time window. A user must provide the hardware token's current value along with username and password to gain access to the application
Tamper-resistant Hardware Devices	- Smart Cards and U2F devices can store X.509 certificates and private keys that can't be read or exported---all cryptographic operations are performed on the card. Physical possession of the device is required as well as knowledge of a PIN in the case of Smart Cards


##### Bypass Authentication Completely 

Storage Buckets     - S3 buckets (AWS), buckets (GCP), (Azure)
Snapshots           - captures of EBS Volumes made at a specific point in time. By default they are not shared but changing a snapshot's permissions can make it available to any AWS account.
Graph API           - An access token for Graph API can provide the bearer unlimited access to all applications in an Azure environment. It can allow an unauthorized user to interact with any application in an authenticated context without being required to pass multi-factor authentication.

Similar to how EBS snapshots have a CreateVolumePermission attribute which controls which account can access it, Amazon Machine Images (AMI) have a LaunchPermission attribute.

Misconfigured SNS Topics and SQS Queues can allow any AWS account to send and receive messages to and from clients.

Best practices for both corporate and personal cloud accounts:

    Restrict Root Account Use	    - Each AWS account has a root user account with access to all services and resources in the account. This root account should not be used for normal, everyday activity
    Prohibit Root Access Keys	    - Long term access keys provide account access using single factor authentication. That means if someone obtains the root user's secret key, they have complete control over everything in the account. For this reason, the root account should not have any access keys
    Require MFA for Console Access	- For console access, an account must have a password. When this password is set, all that's needed to log on as that user is the account name and password. Enabling MFA on the account will require the use of a second form of authentication before providing account access
    Use Roles for API Access	    - Roles use temporary credentials which do not require an AWS identity, have a limited lifetime, are generated dynamically and provided to the instance when requested
    Rotate Credentials Regularly	- All long-term credentials should be rotated regularly---that includes both passwords and access keys. Password policies should be enabled to enforce this as well as provide complexity requirements
    Attach Policies to Groups not Users	- Attaching policies to a group and then assigning the user to that group is the proper way to assign permissions. This way, users can be added to or removed from different groups according to the permissions required by their job functions
    Use AWS Managed Policies to Assign Permissions	- AWS Managed policies should be utilized before making custom managed policies in order to avoid unintentionally assigning unnecessary permissions to entities. If an AWS managed policy can't be found that is exactly right, find one that's close, copy it, and then modify it to fit your requirements
    Credential Requirements	        - Use a long, complex password with 2FA enabled
    Recovery Info	                - Verify accurate and up-to-date account recovery info such as email, phone
    Monitoring	                    - Review recent activity, authorized/logged in devices, payments, subscriptions, 3rd party access authorizations


#### Methodologies for Post-exploitation

Top tier SOCs focus on actor behaviors, their methodologies, and quick response actions that will be needed to kick actors off of the network, shut down their access, and prevent them from reaching their objectives at every turn.  This approach not only works against every tool and every vulnerability discovered so far, but also tools and vulnerabilities that do not exist or have not yet been discovered.  

Detecting exploitation is exponentially more difficult than detecting post-exploitation because vulnerabilities can be exploited in many different ways and create vastly different results and conditions while post-exploitation activity creates standardized, observable, well-known patterns and conditions that reliably indicate actor presence and activity. Instead of placing the focus on the thousands of different ways an actor can gain entry, post-exploitation in a pentest highlights a small group of techniques that are mandatory for every actor in every single compromise.  

Actors must always control either a process or a set of credentials. They must always exercise control of a process over the network.  They must always make use of processes and credentials to pivot, explore, escalate privileges, and carry out their operations.  This is a more realistic way of training responders to successfully counter actors who have gained access to an organization's network.

General Post-exploitation Steps

    Reconnaissance	    - Gather information about the network and environment
    Remote Enumeration	- Scan target system to identify ports/services/versions
    Remote Exploit	    - Gain access to the target machine
    Local Enumeration	- Search the target machine for opportunities to escalate privileges
    Local Exploit	    - Escalate privileges to gain full control of target machine
    Root Loot	        - Search the target machine with admin/root privileges
    Install Persistence	- Establish a way to maintain access to the target host
    Cover Tracks	    - Delete logs, files, and all evidence of compromise

- [Establishing C2](#establishing-c2)
- [Establishing Implant](#establishing-implant)


##### Establishing C2 

Advanced C2 infrastructure utilizes different C2 servers for hosting payloads, interactive operations, and maintaining persistence.

    Staging	Hosts       - the payloads for client-side attacks and initial callbacks
    Operations	        - Used for interactive operations, installing persistence, expanding foothold, and performing actions on objectives
    Long-haul	        - Maintains long-term access. uses low and slow callbacks, used to regain access in case a C2 server is burned, or implant fails or is terminated

A compromise may involve a payload request to badsite.com but be controlled by domain fronting C2 via HTTPS to cloudhoster.com. If analysts verify the payload was downloaded from badsite.com (Staging) and are looking for C2 traffic to badsite.com, they won't find anything. If analysts discover the C2 via domain fronting through cloudhoster.com (Operations), they still must be able to find the persistence that has been configured to call out to persistence-site.com (Long-haul).


##### Establishing Implant 

Implants are deployed in a variety of ways, most commonly using process injection.

Shellcode Injection	        - A target process is made to run malicious machine code
DLL Injection	            - A target process is made to run a malicious DLL on disk
Reflective DLL injection	- Target process is made to run a malicious DLL which loads itself into memory
Memory Module	            - Target process is made to run a malicious DLL which is loaded into memory using an injector or loader that mimics the LoadLibrary function
Process Hollowing	        - A new process is started in a suspended state, replace with malicous code, and resumed
Module Overwriting	        - A legitimate module is loaded into the target process and then overwritten with a malicious module


Windows API access is traditionally reserved for processes running on the system that were started from executables present on the filesystem. Scripting languages used for system administration like PowerShell, VBA/VBScript, and JScript can access Windows APIs using COM objects. The .NET Framework is used to run managed code and can also access the Windows API. Because of these capabilities, scripts and .NET assemblies are frequently used by malware.

Implants are made to interact with Windows API in several different ways:

Built-In Programs	        - GUI applications (explorer.exe) and command line programs (netsh.exe) are built into the OS and use Windows API functions to interact with the system
Compiled Programs	        - Custom programs can be written and compiled to interface with the Windows API
COM Objects	                - DLLs are written to interface with programs that understand the C language. Component Object Model (COM) objects were created to allow DLLs to be accessed from any programming language. Scripting languages like PowerShell, VBA/VBScript, and JScript use COM objects in order to interact with the Windows API
Dynamic .NET Assemblies	    - PowerShell can compile C# on the fly which allows the Platform Invoke (P/Invoke) service to call DLLs through .NET with the Add-Type cmdlet. PSReflect is a script created by Matt Graeber that uses .NET reflection to dynamically define methods that call Windows API functions


Advanced Post-Exploitation Methodologies

Post-Exploitation tools such as Empire and Cobalt Strike can inject .NET assemblies (PowerShell runner DLLs) into any process in memory. 

    - Created using staged or stageless payloads
    - Evade detection several ways:
        Fake thread start address
        Remove RWX Permissions
        Module Stomping
        Obfuscating and sleeping
    - Migrate to other processes
    - Execute programs/scripts with post-exploitation jobs
    - Application allow-listing Bypasses 
    - Dynamically loaded DotNet assemblies

    VBA Code	        - Office Doc uses VBA macro to create a new process and inject shellcode into it which will download and reflectively inject a DLL in memory
    PowerShell	        - Script/Doc uses a COM Object to create a PowerShell process which injects shellcode that will download the payload into memory
    .NET Assembly	    - Script/Doc uses a COM Object to run a .NET assembly in memory which injects shellcode into a created process

    Windows Script Malware

    - Batch files use cmd.exe to run commands
    - VBScript and JScript files can create hidden COM objects and use WinAPI functions by loading .NET assemblies in memory
    - VBA code can be used to create hidden COM objects and call WinAPI functions on systems where Office is installed
    - PowerShell code can run in non-traditional hosts to avoid restrictions/monitoring of traditional hosts such as powershell.exe and powershell_ise.exe


3. Privilege Escalation 

Privilege Escalation on Linux

    Misc	                    - Dylib hijacking, modify plist, startup items/launch daemons
    Discover credentials	    - User files, installation/configuration files
    Password attack	            - Guess or brute force local admin password
    Local exploit	            - OS or application
    Sudo commands	            - Using a program that can be run in an elevated context to spawn a command shell (or some other action) which is also executed in an elevated context
    SUID/SGID Permissions	    - If an executable has SUID permissions, a non-privileged user can execute it in the context of the owner of the program. If the executable has SGID permissions, a non-privileged user can execute it in the context of the group
    Wilowest-dcards	                - If a program references a wilowest-dcard (/tmp/*.sh)that the adversary has permission to write to, an executable script can be made to be included in the wilowest-dcard (/temp/script.sh) and will be executed in an elevated context along with the others that match the wilowest-dcard

Privilege Escalation on Windows

    File system	                - Path interception, DLL Hijack, modify service, new service
    Registry	                - AlwaysInstallElevated, autologons, autoruns
    Configurations	            - Modify task, new task
    Discover credentials	    - User files, installation/configuration files
    Password attack	            - Guess or brute force local admin password
    Local exploit	            - OS or application
    Unquoted Service Paths	    - When the path to a service's binary is not enclosed in quotes, the service path can be hijacked and made to run an arbitrary executable in an elevated context
    DLL Order Hijacking	        - If a program's file path has weak file or folder permissions, a low-priv user can place a malicious DLL in one of several different places where it may be found and loaded by the vulnerable program in an elevated context
    Auto-Elevation	            - If the AlwaysInstallElevated registry keys are present on the system and their value is "1", an low-priv user can install a program in an elevated context

Privilege Escalation Using Active Directory

    An adversary with a low-privilge account will typically target users, computers, and groups that have more permissions than necessary. Access to AD objects is determined by a combination of rights the account has which is made up of:
    - AD Group Membership
    - Local Group Membership
    - AD Object ACLs
    - GPO Permissions
    - User Rights Assignments
    
    Kerberoasting - Since any user can request a ticket for a service, an adversary requests a ticket for a service associated with an AD user or computer account. A portion of the ticket received is encrypted with the NTLM hash of the account's plaintext password. This ciphertext can then be fed to a tool and cracked offline avoiding failed logon attempts and AD account lockouts.

Domain Admin Methods

    Steal token/hash/ticket	            - Keylog or dump credentials from DA logins (RunAs, RDP)	Mimikatz, Windows Credential Editor
    Logon DC with other admin account	- Dump all domain credentials	Mimikatz, Task Manager, NTDS.dit
    Forge token/hash/ticket	            - Create fake/forged credentials	MS14-068
    Password attack	Offline cracking	- Kerberoast
    Discover credentials	            - Installation/configuration files	SYSVOL, GPP

Common privilege escalation scripts/programs:

    Metasploit modules	            - use post/multi/recon/local_exploit_suggester
    Windows Privesc Check	        - windows-privesc-check2.exe --audit -a -o report
    PowerUp	                        - Import-Module PowerUp.ps1; Invoke-AllChecks
    SharpUp	                        - SharpUp.exe
    Sherlock	                    - Import-Module Sherlock.ps1; Find-AllVulns
    Watson	                        - Watson.exe
    windows-exploit-suggester.py    - python windows-exploit-suggester.py -u
                                    - python windows-exploit-suggester.py -d <xls> -i systeminfo.txt
    linuxprivchecker.py	            - python linuxprivchecker.py


3. Lateral Movement

    General Lateral Movement Methodologies

    Remote session	            - Use stolen or created credentials to create session	PS Remote, PSExec, RDP, Pass-the-Hash/Pass-the-Ticket, VNC, SSH
    Remote code execution	    - Use stolen or created credentials to execute code	Invoke-Command, WMIC, Psexec, at, schtasks, sc
    Remote file copy	        - Use stolen or created credentials to copy files	scp, rsync, ftp, cifs, sftp, Logon scripts/hooks, Admin shares, shared drives, DLL preloading, shortcut hijacks
    Removable media	            - Execute code via USB, CD, other external media	Rubber Ducky/HID, autorun
    Third-party software	    - Use a tool account’s privileges to access a remote host	Nessus, Mcafee, FireEye, SCCM


    Authenticated RCE Methodologies

    Windows Management Instrumentation	    - Use WMI to execute a command on a remote system
    Service Control Manager	                - Create a service that will execute a command when started
    Windows Remoting	                    - Use Windows Remoting (WinRM) to execute command
    Remote Registry	                        - Write command to execute to a registry key
    Remote File Access	                    - Write file containing command to execute to an administrative share
    Task Scheduler	                        - Schedule a command to run at the provided time
    Remote Desktop	                        - Log in with credentials and execute code in an interactive session
    MMC20.Application DCOM	                - Instantiate a COM object remotely and call ExecuteShellCommand method


5. Collection  

    Collection Methodologies

    COM Objects	        - Internet Explorer COM objects can be used to download and run a file
    Obfuscation	Script  - Encoding can be used to obfuscate code and commands and deter code analysis
    Containers	        - VBScript/JScript can be run from containers such as Windows Scripting Files (.wsf) and HTML Applications (.hta)
    API Access	        - .NET assemblies can be embedded into JScript files using serialization and can directly access the Windows API
    VBA                 - Visual Basic for Applications is an embeddable programming environment used to automate and extend the functionality of applications. Office applications such as Word, Excel, and PowerPoint are used to host VBA code via macros. VBA is closely related to Visual Basic, a programming language and IDE used to create stand-alone windows applications.
    PowerShell          - PowerShell is a command-line shell and scripting language based on the .NET Framework which is installed by default on Windows 7/2008 R2 and later. It has become a major component of adversary tradecraft and is increasingly seen in targeted attacks as well as commodity malware


Additionally, PowerShell is utilized heavily based on its capabilities which offer adversaries the greatest tactical advantage:

    Scope of Influence	            - PowerShell is a trusted program providing an interactive command-line shell and scripting language for automating a wide range of administrative tasks
    Dynamic Code Generation	        - PowerShell has access to .NET & Windows APIs and can be used to compile and run C# code on the fly
    Process Agnostic	            - Unmanaged PowerShell allows custom and native programs to run in any process
    Memory-Only Execution	        - PowerShell can use memory modules and reflective injection to execute code in memory without ever touching disk
    Cradle Options	                - PowerShell has multiple ways to download content from remote systems to disk or to memory
    Post-Exploitation Modules	    - Multiple scripts and modules exist designed specifically to enhance and support adversary operations


7. Persistence 

    Startup Folder and Registry Keys	    - Programs in a user's Startup folder and registry Run keys will execute at user logon
    Scheduled Tasks	                        - Tasks can be created to execute a malicious program on system startup or at certain days and times
    Accessibility Features	                - Accessibility features can be abused by an attacker to maintain access to a system
    File and Folder Permissions	            - If a program uses a file or folder that has weak permissions, an attacker can overwrite a legitimate program file with a malicious one
    Logon Scripts	                        - A logon script can be configured to run whenever a user logs into a system
    Shortcuts	                            - A shortcut for a legitimate program can be modified and used to open a malicious program when accessed by a user
    Service Registry Permissions	        - If an attacker can modify registry keys, the image path value of a service can be changed to point to a different executable
    Service Permissions	                    - If an attacker can modify a service, the binary path can be changed to any executable on the system
    New Service	                            - A new service can be created and configured by an attacker to execute at startup
    Default File Associations	            - Default file associations determine which programs are used to open certain file types. These can be changed so that an arbitrary program is called when a specific file type is opened
    WMI Subscriptions                       - WMI is an administration tool that is used to query, change, and manage the components that make up the operating system. It uses classes and objects to represent different OS components such as processes, file systems, and registry keys.


#### Explain the differences between black-box, white-box, and grey-box testing. When would you recommend using each approach and why?

Confidence in your defense comes from knowing exactly what you are supposed to do in a given situation, which you get from understanding adversary tools, techniques, and methodologies. Penetration testing gives responders a path to obtain knowledge, build that knowledge into skills using practical application, and constantly refine these skills through practice and testing. This not only improves each skill individually, but also improves the ability to integrate and interface between different skill sets with speed and accuracy.

Emulating adversary tradecraft helps responders understand adversary methodologies---how and why they choose and combine individual techniques for specific scenarios and objectives---in order to better anticipate and counter the tactics and techniques that can be used against them. We can use different types of penetration testing to build on our understanding of attacks and improve our confidence in defense.


1. Black-box 

Penetration tests challenge incident responders to sufficiently address all that is "known" while also accounting for everything that is "unknown".  Responders have the unknown they must deal with ( what was actor able to accomplish, what new problems result from workarounds, how many systems have similar vulns ) which is most effectively solved by hunting and detecting post-exploitation via behaviors/MITRE techniques. 

Black-box testing strives to demonstrate to responders that the "known" will be different for each incident and is mainly a surface level problem.  First, it's not actually "known", it's what has been discovered or shared by external sources up to that point.  Second, it's a smaller, tangential problem in context of the incident as a whole. Responders learn to work through conditions where information from open-source tools is little to none.

This type of testing promotes use of observed patterns and behaviors utilizing approaches such as behavioral analytics, frequency analysis, time-series analysis, and link analysis which can be quickly used to identify users and systems that are involved in or have been impacted by the compromise.  Models like the Diamond Model and the Kill Chain help responders discover artifacts related to the compromise and provide context as they work to build a complete picture of the incident, the extent of damage, and plans for mitigation/eradication.

2. White-box 

An analysts job is defense-oriented. Responders must actively interpret events to solve problems under stress and various time constraints. This requires competence in the ability to deny adversary opportunities to attack, identify and counter the use of offensive techniques, evaluate the success and impact of adversary actions, and explain the logic and intent of adversary actions and decisions.

This requires practicing the act of gathering different pieces of information and attempting to identify relationships and correlations in order to build a complete picture of adversary operations. Training labs are effective learning tools that do this because they offer analyts different levels of available information and different levels of scenarios depending on levels of skill and experience.  This is what White-box testing aims to do.

To increase a responder's understanding of adversary approaches and attacks, testing must present realistic scenarios with different levels of visibility for logging, users, systems, analysis tools, and management tasks. White-box scenarios require combinations of commercial and open-source data, artifacts obtained from IR/triage, and DFIR skill sets to successfully complete while responders gain experience integrating a variety of tools and information to predict and anticipate adversary actions and determine how, why, and when they are most likely to occur.


3. Grey-box 

Emulation and practical application improves a responder's ability to correctly identify what things are most important and how to address them during an incident. To do this, we need a clear understanding of what offensive actions are being performed, why they are being used, and what an adversary can obtain/accomplish on a system at any given time. Adusting the amount of information visible and presenting multiple test cases for specific high-percentage techniques is the primary focus of Grey-box testing.

For example, many consider phishing to be the most effective technique for gaining a foothold on a network while memory injection is one of the best ways to remain on a compromised machine undetected. Several methods of payload delivery combine both of these techniques in order to compromise a system without writing to disk. This in turn creates the need for analysts and responders to discover and obtain memory-only artifacts in order to build a complete picture of adversary operations and successfully complete scoping, containment, and eradication phases.

Scenarios that provide analysts with different implementations of these high-percentage techniques with appropriate artifacts allow them the opportunity to become well-versed in defending against a small set of scenarios that have proven to be the most effective for adversaries. Visibility is adjusted as needed throughout testing to ensure analysts and responders are presented with different looks at the same collection of adversary tradecraft---in this case memory injection techniques, common ways that malware accesses the Windows API, and payload delivery techniques that can be used with phishing to deploy memory-only malware on a system.


Memory Injection Techniques
    - Shellcode Injection
    - Reflective DLL injection
    - Memory Module
    - Process Hollowing
    - Module Overwriting

Windows API Access
    - Using Built-In Programs
    - Using Compiled Programs
    - Using COM Objects
    - Using Dynamic DotNet Assemblies
    - Platform Invoke
    - PSReflect

Payload Delivery Techniques
    - VBA Code Injects into Created Process
    - COM Object to Injected PowerShell Process
    - COM Object to DotNet Injects into Created Process

Memory-Only Artifacts

    Residual data	            - Data from disconnected external media devices, previous boots, terminated shells, wiped event logs and browser history no longer available on disk
    Volatile data	            - Registry keys manipulated directly in memory are not written to disk and can be used to track user activity or locate malware persistence
    Network data	            - Evidence of proxy or port redirection, network share data, traffic traversing SSH/VPN tunnels, encrypted communications, connected wireless devices
    Hidden services	            - Services can be running with no traces in event log, registry, or memory of services.exe, but running process, DLL, or kernel driver is still in memory
    File data	                - Recently used files, deleted files, executable paths and timestamps allow evidence of file execution/knowledge, directory traversal
    Application data	        - Data an application received over the network, decompressed and/or decrypted instructions in memory, encryption keys
    Command History	            - Extract full console input and output, data from user/attacker sessions such as usernames, passwords, programs/files accessed
    SIDs/Privileges obtained	- User, group, and privilege information associated with user and attacker-controlled accounts and malicious processes
    Malware-created artifacts	- Parameters of the infection, C2 and exfiltration data, hidden files and processes, hooked drivers, injected code
    Anti-forensic artifacts	    - Evidence of file wiper use, programs run from removable devices, event log modifying/deleting, resetting timestamps
    Passwords	                - Plaintext passwords stored by OS/applications that may be reused on other systems/services, passwords for encrypted files and containers


#### How do you prioritize vulnerabilities discovered during a penetration test? What factors influence your decision-making process?

Vulnerabilities discovered are immediately reported to the customer so they can be prioritized according to policies and processes of the organization. Additionally, the customer can decide case by case if testing should continue or not. 

Factors that influence decision-making include application functionality and exposure, the credentials and processes targeted and resources actors would have direct access to should the vulnerability be exploited, and what steps would be necessary immediately following a successful exploit.


#### Explain the concept of "zero trust" in cybersecurity. How would you implement a zero-trust model in a network environment?

Under the Zero Trust model, trust decisions are to be validated using all relevant available information and telemetry. However, many organizations have started depending too much on the client's IP address or some derivative of it (geolocation, GPS coordinates, country). There are also those that depend on reputation services or other external intelligence sources which increase margin of error. 

High-fidelity detections that catch the full spectrum of adversary skill and effort do not rely on reputation, IP addresses, and location derivatives.  They rely on patterns of data points across time intervals combined with a deep understanding of actor methodologies. That is the purpose and goal of Zero Trust----to not trust anything that is "claimed", but to only trust observed actions and conditions.

For example, a Zero Trust model in a network environment would not place a heavy importance on collecting and analyzing the many different attributes that can be found in sign-in logs such as account names, infrastructure names, and location derivatives.  Account property fields, and especially fields that an actor can control, are not the best choice for hunting queries, detection development, or any type of access decisions.

Instead, a good Zero Trust model would place significant focus on the types and nature of system and account actions throughout the environment---specifically operation-only interaction with other systems, users, and applications. By doing this, tools and analysts are made to discover and consume information about operations in the environment utilizing the one thing actors cannot deceive responders with... their actions.