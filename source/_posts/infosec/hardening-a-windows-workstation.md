---
title: hardening a windows workstation
date: 2023-02-18 06:17:38
description: 
tags: [security]
category: [protect, knowledge]
---

<br>

Any of the following:

|Mitigation|Description|
|-|-|
|[Least Privilege](#least-privilege)|Limit access to only information and resources necessary for normal operation|
|[Multi-factor Authentication](#multi-factor-authentication)|Requiring two or more factors to confirm an identity|
|[Daily Backups](#daily-backups)|Maintaining copies of system files and data at least every day|
|[Credential Hygiene](#credential-hygiene)|Proper creation, protection, and handling of passwords and password hashes|
|[Application Whitelisting](#application-whitelisting)|Allow and deny drivers, programs, and scripts from running based on characteristics|
|[Patch Management](#patch-management)|Keep operating system and applications at latest versions|
|[Disable Untrusted Office Features](#disable-untrusted-office-features)|Disable features that can run untrusted code such as Macros, OLE Object Execution, ActiveX, DDE|
|[Disable AutoRun-AutoPlay](#disable-autorun-autoplay)|Prevent applicatons from automatically executing when a device is plugged into the computer|

<br>

## Reducing Attack Surface

- Use a password manager [Keepass](http://keepass.info/) [LastPass](https://lastpass.com/)
- Use a security/privacy-focused browser ([Tor Browser Bundle]())
- Use a VPN [NordVPN](https://go.nordvpn.net/aff_c?offer_id=38&aff_id=1764&url_id=633), [PIA](https://bit.ly/privateVPNanonymous), [ExpressVPN](https://bit.ly/exprvpnreview), [TorGuard](https://bit.ly/torguardvpn), [SlickVPN](https://www.slickvpn.com/#a_aid=570ec1e3eb913), [ProtonVPN](https://protonvpn.com/)
- Use a security/privacy-focused search engine ([DuckDuckGo](https://duckduckgo.com/), [Quant](https://www.qwant.com/), [StartPage](https://www.startpage.com/))
- Use a trusted DNS server [1.1.1.1](), [8.8.8.8]()
- Use an Ad blocker plugin [uBlock Origin](https://ublock.org), [Ghostery](https://www.ghostery.com/) or use hosts file
- Force HTTPS [HTTPS Everywhere](https://www.eff.org/https-everywhere)
- Block cookies, trackers [Privacy Badger](https://privacybadger.org/), [Decentraleyes](https://decentraleyes.org/), [Cookie AutoDelete](https://github.com/Cookie-AutoDelete)
- Block Flash [Flash Control]()
- Block WebRTC [WebRTC Blocking]()
- [Compartmentalization](https://medium.com/@privacyhivesoftware/why-compartmentalisation-is-the-key-to-protecting-your-online-privacy-b91d86482cd) - Using multiple browsers, profiles, VMs, containers, live OS, etc.

<br>

Check your browser with [Browser Leaks](https://www.browserleaks.com/) and your VPN with [WITCH](http://witch.valdikss.org.ru/)

<br>

[reference](https://gist.github.com/atcuno/3425484ac5cce5298932)

<br>

## Ads Trackers and Script Servers

- **Ads** are images, buttons, or HTML elements on a web page being served from a third party server for the purpose of advertising a product or collecting customer information 

- **Trackers** are scripts on third-party sites that collect and organize information about user browsing habits. This data is combined and mined to make user profiles so that personalized (targeted) ads can be served via ad networks (Adsense, Admob, and DoubleClick) that are embedded in millions of websites

- **Script servers** are used by third-party servers for many different purposes including tracking, serving ads, serving website content, or all of the above

<br>

## Tracking Techniques

|Technique|Description|
|-|-|
|Cookies|Set either by JavaScript (local storage) or by HTTP responses that include a Set-Cookie header. The browser stores the user-specific cookie which is retrieved and transmitted to third-party domains on future visits|
|Web bugs|Files or HTML elements embedded in a web page or email that force a client application to make a request containing identifying information to a third-party server. These can be images, graphics, banners, buttons, and other HTML elements such as frames, styles, scripts, input links, etc.|
|Fingerprinting|Passively collecting and profiling browser characteristics in order to distinguish and recognize individual browsers (and users). This technique uses User agent strings, screen resolution/depth, timezones, fonts, plugins and versions, etc. to create a signature that can be used to identify and track user activity across multiple websites|
|Supercookies|Also called *zombie cookies* --- any technology other than a standard HTTP Cookie that is used by a server to identify clients. Examples include Flash LSO cookies, DOM storage, HTML5 storage, and other methods of storing information in caches or etags|

<br>

## Blocking Ads Trackers and Script Servers

[Privacy Badger](https://www.eff.org/privacybadger), [uBlock Origin](https://www.ublock.org), [Ghostery](https://www.ghostery.com), and [NoScript]() are examples of tools that block third-party ads, trackers, and script servers and allow an analyst to control the execution of third-party scripts

<br>




## Encoding Encryption Hashing

|Term|Definition|
|-|-|
|Encoding|Transforms data into a different format using a publicly available scheme for the purpose of usability.  It is easily reversed |
|Encryption|Transforms data into a different format for the purpose of confidentiality.  A key is required to reverse |
|Hashing|Transforms a string of characters into a value of fixed length called a hash for the purpose of integrity.  One change in the string of characters will produce a different hash|

<br>

## SSL TLS HTTPS

They are all equally secure.  Secure Sockets Layer (SSL) is the old version of Transport Layer Security (TLS), essentially the same protocol which was designed to provide privacy and data integrity between two parties.  HyperText Transfer Protocol Secure (HTTPS) is a session where HTTP data is exchanged over an SSL/TLS connection.

<br>

## Salts and Slow Hashes 

These are countermeasures for password cracking:

Salts - an additional random string that is combined with a password when it's hashed to counter rainbow tables

Slow Hashes - hashing algorithms that are optimized to be slow to increase the time to crack

<br>


## Identifying a Trojaned File

Hash	used to verify the file's integrity
Digital Signature	used to verify the publisher's identity

Code signing does not tell you whether the file is malicious or not, it only confirms the identity of the publisher and if the code has been modified.

Digitally signed code is backed by a certificate issued by a trusted third party (CA)

Unsigned code may include publisher data but if it doesn't provide any evidence of origin or file integrity, we cannot trust that it is what it says it is

Time-stamp signing allows code to be trusted after a private key is compromised or the certificate is revoked.  It proves the certificate was valid and trusted at the time of the timestamp.

<br>




## Digital Signatures

On Windows 7 and later versions, all native PE files, including EXEs and DLLs, that are running in processes, device drivers, and services should be signed by Microsoft.

A file signed with a valid, trusted certificate confirms **authenticity** and **origin**:

1. Microsoft signs a file to prove it is authentic
2. Microsoft signs a file with their private key to prove it came from Microsoft

<br>

## Windows PE Digital Signatures

Windows PE files are signed in one of two ways:

1. Embedded - A digital signature (Microsoft uses Authenticode) is embedded inside the PE
2. Catalog - A hash of the PE can be found in a security catalog (.CAT) file

<br>

## Verifiying Windows PE Digital Signatures

Embedded signatures are placed at the end of the signed file and can be verified various ways such as `Get-AuthenticodeSignature`, `sigcheck.exe`, `signtool.exe`, `DigiCertUtil.exe`, `explorer.exe`, and others.

Catalog-signed files do not have an embedded digital signature so they may not pass verification on all the tools above.  Verifying them on Windows 7 hosts requires a tool such as SysInternals `sigcheck.exe` which looks up the Authenticode hash of the file in its associated catalog file and verifies the signature of the catalog file.

<br>




## Proxy and VPN Tunnels

Proxy tunnels (HTTP, SOCKS, etc) pass Layer 4 (TCP/UDP) traffic to/from the source and destination. An application on the source machine works with an application on the proxy host creating a tunnel that passes Layer 4 traffic between the two.

VPN tunnels	pass Layer 2 (ETH) traffic between two systems or networks. A network interface on one system is bridged to the network interface on another system creating a tunnel that passes Layer 2 traffic between the two.

<br>


**Single Factor Authentication**

|Method|Description|
|-|-|
|Basic Authentication|Base64-encoded username and password included in each request. If not included, the server sends a response with a `WWW-authenticate` attribute in the header|
|Digest Authentication|Username, password, and nonce is used to create hash which is sent to the server. If not included, the server sends a response with a `WWW-authenticate` attribute in the header and a nonce|
|Windows Integrated Authentication|Also called NTLM authentication, the server sends either a Negotiate (Kerberos) or NTLM `WWW-authenticate` header and a nonce.  Browser returns Base64-encoded username, hostname, domain, service, and the results of the hashing functions which the server can validate with the domain controller|
|Form-based Authentication|Uses code external to the HTTP protocol for authenticating the user. The application is left to deal with prompting the user for credentials, verifying them, and deciding their authenticity|
|Certificate Authentication|Client application holds a certificate with a private key and the remote web application maps that certificate's public key to an account. Client sends its certificate to the application which checks the digital signature, certificate chain, expiration/activation date and validity period, and a revocation status check|

<br>

**Second Factor Authentication**

|Method|Description|
|-|-|
|One-Time Passwords|One-time passwords are shared on-the-fly between two digital entities using an out-of-band (OOB) communication such as SMS, email, or application. After a server validates the username and password, it generates an OTP that can only be used once and sends it to the client via the chosen OOB method|
|Hardware Tokens|The hardware token contains an algorithm, a clock, and a seed or a unique number which is used to generate the numbers displayed for a specific time window. A user must provide the hardware token's current value along with username and password to gain access to the application|
|Tamper-resistant Hardware Devices|Smart Cards and U2F devices can store X.509 certificates and private keys that can't be read or exported---all cryptographic operations are performed on the card. Physical possession of the device is required as well as knowledge of a PIN in the case of Smart Cards|

<br>

**Session Management**

|Method|Description|
|-|-|
|Cookies|- User authenticates<br>- Server verifies and creates a session<br>- Cookie with session ID is placed in browser and stored on server<br>- For each request, session ID is included and verified in database<br>- Session is destroyed upon logout|
|Tokens|- User authenticates<br>- Server verifies and returns a signed token<br>- Token is only stored client-side<br>- All future requests include the signed token<br>- Server decodes token and if valid processes request<br>- Client destroys token on logout|

<br>

|Method|Example|Problem|
|-|-|-|
|Password Authentication|Username and Password|Can be guessed, cracked, sniffed, stolen, etc.|
|Public Key Authentication|Digital certificate|Can be lost, copied, compromised, stolen, etc., not practical for most web apps|
|Multi-Factor Authentication|Password + Time-based One Time Password, SMS|Can be phished for a one time web session|
|MFA with Tamper-resistant Hardware Devices|Smart Card, U2F|Browser compromise required for an unauthorized web session|

<br>

Tamper-resistant hardware devices are designed so that the private key never leaves the device.  This ensures that someone must have possession of the device *and* know its PIN in order to use it.

<br>


## Evasion Techniques on Network

|Technique|Description|
|-|-|
|[Encryption](#encryption)|Used to hide the contents of traffic|
|[Proxy Tunnels](#proxy-tunnels)|Used to hide the true destination of an application's traffic|
|[VPN Tunnels](#vpn-tunnels)|Used to hide the true destination of all traffic|
|[VPN With Cloaking Device](#vpn-with-cloaking-device)|Used to hide the true source of the traffic|

<br>

## Identify Network Evasion Techniques

- Hosts creating large amounts of traffic with little or no DNS requests 
- Hosts that make the majority of connections to one or several external servers on specific ports
- Hosts that are using protocols that are unusual for the ports being used (UDP on 443 in this example)
- Hosts having little or no listening ports and services

<br>

## Evasion Techniques on Disk

|Technique|Description|
|-|-|
|Using an Encrypted VM|Encrypt VM hard disk or store the machine's disk image file (VDI, VMDK, VHD, HDD) and settings file (.vbox) in an encrypted container|
|Using a Hidden VM|VM files can also be stored in a hidden volume which is a volume that's created within the free space of another encrypted volume|
|Booting to a Hidden OS|Hard drive has two partitions, the first is a decoy OS and the second holds both an encrypted filesystem and a hidden OS. The second partition appears to be and functions as storage and only runs the hidden OS when provided with a specific password|
|Booting to a Live OS|Live OS's run using only the filesystem on the device (USB, CD/DVD) and the computer's RAM to operate. These can be used to make changes to the existing system as well as operate without making any changes to the system|

<br>

## Identify Disk Evasion Techniques

|Technique|Controls|
|-|-|
|[Encrypted VM](#using-an-encrypted-vm)|Large amounts of encrypted data, presence of VM/disk encryption software|
|[Hidden VM](#using-a-hidden-vm)|Large amounts of encrypted data, presence of VM/disk encryption software|
|[Hidden OS](#booting-to-a-hidden-os)|Large encrypted partitions on hard drive or additional drives with disk encryption|
|[Live OS](#booting-to-a-live-os)|Unified Extensible Firmware Interface (UEFI) Secure Boot is a successor of Basic Input Output System (BIOS).  When UEFI is enabled, only signed bootloaders are allowed to run and booting from a CD/USB is not possible|

<br>

## Authentication Using Smart Cards and Kerberos

PKI is authentication based on digital certificates and a chain of trust. Trusted certificate authorities issue X.509 digital certificates to all entities, hosts, and services so they can authenticate each other.

Smart cards are tamper-resistant hardware devices that store X.509 certificates and private keys that can't be read or exported. A PIN is required to unlock and use the private key providing 2FA.

Active Directory contains account info such as group memberships, security identities, user details used to build TGTs determining access rights for accounts.

Keberos is a SSO mechanism which uses a third party (KDC) to authenticate clients and resources.  Once a user is verified using their digital certificate, the KDC finds the account in AD and builds a TGT containing their user and group SIDs. The TGT is then sent to the user which is used to request Service Tickets for accessing hosts and services on the network.

<br>


## Assessing the Impact of Exposed Resources 

When sensitive information is exposed to the public:

- Identify all code and data that can be extracted by and exploited by an adversary 
- Use tools such as `bulkextractor` and manual searching to find usernames, email addresses, SSH keys, AWS credentials, URLs, domains, IP addresses, hostnames, etc.
- Provide management with an accurate assessment of what was available as a result of the exposure to guide initial response and corrective actions

<br>



## Unauthorized Access to Web Service 

|Method|Description|
|-|-|
|Find Single Factor|Current credentials can be searched for, discovered, and used to access accounts|
|Guess Single Factor|Users often employ weak or duplicate passwords that can be guessed or brute-forced|
|Steal Single Factor|Users can be tricked into providing credentials to an adversary or running malware that steals them|
|Bypass Single Factor|Account recovery procedures can allow an adversary to bypass the normal authentication process|
|Steal Session Creds|With XSS, the adversary is using the ability to run JavaScript in the victim's browser to send a cookie or session token to a remote server so they can create an authenticated session from there|
|Forge Session Creds|With CSRF, the adversary is taking advantage of the browser being authenticated to a target site. If the victim is logged in, any request made to the site originating from the victim's browser will be successful|
|Exploit Application|Vulnerability is exploited to gain access|

<br>

## Cloud Account Best Practices

Best practices for both corporate and personal cloud accounts:

|Best Practice|Description|
|-|-|
|Restrict Root Account Use|Each AWS account has a root user account with access to all services and resources in the account. This root account should not be used for normal, everyday activity|
|Prohibit Root Access Keys|Long term access keys provide account access using single factor authentication. That means if someone obtains the root user's secret key, they have complete control over everything in the account. For this reason, the root account should not have any access keys|
|Require MFA for Console Access|For console access, an account must have a password. When this password is set, all that's needed to log on as that user is the account name and password. Enabling MFA on the account will require the use of a second form of authentication before providing account access|
|Use Roles for API Access|Roles use temporary credentials which do not require an AWS identity, have a limited lifetime, are generated dynamically and provided to the instance when requested|
|Rotate Credentials Regularly|All long-term credentials should be rotated regularly---that includes both passwords and access keys. Password policies should be enabled to enforce this as well as provide complexity requirements|
|Attach Policies to Groups not Users|Attaching policies to a group and then assigning the user to that group is the proper way to assign permissions. This way, users can be added to or removed from different groups according to the permissions required by their job functions|
|Use AWS Managed Policies to Assign Permissions|AWS Managed policies should be utilized before making custom managed policies in order to avoid unintentionally assigning unnecessary permissions to entities. If an AWS managed policy can't be found that is exactly right, find one that's close, copy it, and then modify it to fit your requirements|
|Credential Requirements|Use a long, complex password with 2FA enabled|
|Recovery Info|Verify accurate and up-to-date account recovery info such as email, phone|
|Monitoring|Review recent activity, authorized/logged in devices, payments, subscriptions, 3rd party access authorizations|

<br>


