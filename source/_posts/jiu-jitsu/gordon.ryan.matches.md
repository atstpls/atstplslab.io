---
title: gordon ryan matches
date: 2023-04-08 12:56:24
tags: [jiujitsu]
category: [training, tactics]

---



&emsp;[<img src="../../../../assets/img/posters/gordon-ryan-vs-jacob-couch.png" width="460" />](#gordon-ryan-vs-jacob-couch/)&emsp;[<img src="../../../../assets/img/posters/gordon-ryan-vs-felipe-pena-3.png" width="450" />](#gordon-ryan-vs-felipe-pena)&emsp;[<img src="../../../../assets/img/posters/gordon-ryan-vs-patrick-gaudio.png" width="460" />](#gordon-ryan-vs-patrick-gaudio)

&emsp;[<img src="../../../../assets/img/posters/gordon-ryan-vs-pedro-marinho.png" width="460" />](#gordon-ryan-vs-pedro-marinho)&emsp;[<img src="../../../../assets/img/posters/gordon-ryan-vs-vagner-rocha.png" width="460" />](#gordon-ryan-vs-vagner-rocha)&emsp;[<img src="../../../../assets/img/posters/gordon-ryan-vs-matheus-diniz.png" width="460" />](#gordon-ryan-vs-matheus-diniz)

&emsp;[<img src="../../../../assets/img/posters/gordon-ryan-vs-roberto-jimenez.png" width="460" />](#gordon-ryan-vs-roberto-jimenez)&emsp;[<img src="../../../../assets/img/posters/gordon-ryan-vs-andre-galvao.png" width="460" />](#gordon-ryan-vs-andre-galvao)&emsp;[<img src="../../../../assets/img/posters/gordon-ryan-vs-matheus-diniz.png" width="460" />](#gordon-ryan-vs-matheus-diniz)
