---
title: master
date: 2023-04-08 12:56:24
tags: [jiujitsu]
category: [training, tactics]

---

<br>

# Go Further Go Faster

<br>

<details>

<summary>1.1 Pin Escapes </summary>

<br>

#### 1 Insights

- Introduction 	0 - 6:56
- Escapes Overview 	6:56 - 43:37
- Defense & Escapes - General Reflections 	43:37 - 49:50
- Your First goal in Jiu Jitsu: Pin Escapes 	49:50 - 51:25
- Pin Escapes 	51:25 - 56:11
- Escape is a Responsibility 	56:11 - 57:27
- 3 Modes of Pin Escape in Jiu Jitsu 	57:27 - 1:01:46
- Minimal Escapes & Positive Escapes 	1:01:46 - 1:06:20
- The 2 Elements of Pinning in Jiu Jitsu 	1:06:20 - 1:13:31
- Single Biggest Insight Required To Escape Pins 	1:13:31 - 1:24:06
- Essential Body Movements Required For Pin Escapes 	1:24:06 - 1:25:21
- Bridging 	1:25:21 - 1:33:00
- Bridging 2 	1:33:00 - 1:37:07
- Walking Bridges 	1:37:07 - 1:38:47
- Shrimping 	1:38:47 - 1:47:20
- Back Heisting 	1:47:20 - 1:51:09
- Sitting 	1:51:09 - 1:54:33
- Shoulder Rolling 	1:54:33 - 1:57:26
- Inverted Spin 	1:57:26 - 1:59:46
- Putting Body Movements Together 	1:59:46 - 2:03:15

<br>

#### 2 Escaping Mount

- Bridging Escape From Mounted Position (UPA) 	0 - 15:02
- Bridging Escape From Mounted Position 2 	15:02 - 19:01
- Bridging Escape From Mounted Position 3 	19:01 - 22:04
- Bridging Escape From Mounted Position 4 	22:04 - 25:18
- Bridging Escape From Mounted Position 5 	25:18 - 30:01
- Elbow Escape From Mounted Position 	30:01 - 40:45
- Elbow Escape From Mounted Position 2 	40:45 - 47:34
- Elbow Escape From Mounted Position 3 	47:34 - 58:29
- Elbow Escape From Mounted Position 4 	58:29 - 1:07:24
- Elbow Escape From Mounted Position 5 	1:07:24 - 1:13:48
- Elbow Escape From Mounted Position 6 	1:13:48 - 1:18:12
- Elbow Escape From Mounted Position 7 	1:18:12 - 1:21:35

<br>

#### 3 Escaping Side Mount

- Elbow Escape From Mounted Position - Ankle Trap Method 	0 - 6:50
- Elbow Escape From Mounted Position - Ankle Trap Method 2 	6:50 - 11:14
- Elbow Escape From Mounted Position - Ankle Trap Method 3 	11:14 - 15:37
- Putting It All Together 	15:37 - 22:50
- Foot Fighting From Bottom Mount 	22:50 - 28:42
- The Goal of Foot Fighting: The Long/Short Position 	28:42 - 35:20
- Dealing With High Mounts 	35:20 - 41:21
- The Problem of Back Exposure When Escaping Mount 	41:21 - 46:54
- Elbow Escapes From Side Position Introduction 	46:54 - 56:40
- Elbow Escapes From Side Position 2 	56:40 - 1:00:49
- Elbow Escapes From Side Position 3 	1:00:49 - 1:07:10
- Elbow Escapes From Side Position 4 	1:07:10 - 1:14:57
- Elbow Escapes From Side Position 5 	1:14:57 - 1:24:10
- Elbow Escapes From Side Position 6 	1:24:10 - 1:28:37
- Elbow Escapes From Side Position 7 	1:28:37 - 1:30:59

<br>

#### Knee Escapes

- Knee Escapes From Side Position 	0 - 6:07
- Knee Escapes From Side Position 2 	6:07 - 9:23
- Knee Escapes From Side Position 3 	9:23 - 16:49
- Knee Escapes From Side Position 4 	16:49 - 21:23
- Knee Escapes From Side Position 5 	21:23 - 27:32
- Knee Escapes From Side Position 6 	27:32 - 33:52
- Knee Escapes From Side Position 7 	33:52 - 40:51
- Knee Escapes From Side Position 8 	40:51 - 44:38
- Knee Escapes From Side Position 9 	44:38 - 49:29
- Knee Escapes From Side Position 10 	49:29 - 52:49

<br>

#### 5 Framing and Space

- A General Theory of Framing and Space 	0 - 9:54
- A General Theory of Framing and Space 2 	9:54 - 15:02
- A General Theory of Framing and Space 3 	15:02 - 19:10
- 3 Most Important Frame From Bottom Side Control For Escape 	19:10 - 38:06
- Knee Escape: Single Leg Cut Back Finish 	38:06 - 42:03
- Knee Escape: Single Leg To The Back 	42:03 - 46:55
- Knee Escape: Single Leg To Half Guard 	46:55 - 52:04
- Knee Escape: Single Leg To Butterfly Guard 	52:04 - 54:44
- Knee Escape: Single Leg To Full Guard 	54:44 - 57:11

<br>

#### 6 High Leg Escape

- High Leg Escape From Side Pin 	0 - 5:31
- Details of High Leg Escape 	5:31 - 11:19
- Ankle Trap Escape From Side Pin 	11:19 - 23:44
- Shoulder Roll Escape From Side Pin 	23:44 - 32:41
- Creating Effective Frames Under Pins 	32:41 - 38:30
- The Critical Importance of Knee Entry Angle and Position 	38:30 - 47:25
- Single Most Important Tool for Escapes From Bottom Position in Jiu Jitsu 	47:25 - 51:12
- Understanding The Crucial Importance of Insertion Points 	51:12 - 57:24
- Spinning Escape From Side Control 	57:24 - 1:04:42
- Prop Escapes From Side Control 	1:04:42 - 1:13:59

<br>

#### 7 Escaping Side Pins

- Escaping From Side Pins Where Opponent Sits Out 	0 - 9:05
- Escaping From Side Pins Where Opponent Sits Out 2 	9:05 - 14:43
- Escaping From Side Pins Where Opponent Uses Reverse Sit Out 	14:43 - 28:00
- Escaping From Knee on Belly Position 	28:00 - 36:36
- Ankle Trap Escape From Knee on Belly 	36:36 - 44:36
- Escaping From North-South Position 	44:36 - 51:16
- 3 Different Kinds of North South Position 	51:16 - 56:38
- Escaping North South: Transition Escape 	56:38 - 1:02:24
- Escaping Double Over North/South 	1:02:24 - 1:06:38
- Escaping Double Under North/South 	1:06:38 - 1:16:31
- Escaping Double Under North/South Part 2 	1:16:31 - 1:22:02
- Escaping Over Under North/South 	1:22:02 - 1:27:49

<br>

#### 8 Rear Mount Escapes

- Rear Mount Escapes 	0 - 7:13
- Rear Mount Escapes Sliding Elbow Escape   7:13 - 21:58
- Rear Mount Escapes Sliding Elbow Escape 2   21:58 - 26:48
- Rear Mount Escapes Sliding Elbow Escape 3  26:48 - 30:34
- Rear Mount Escapes Sliding Elbow Escape 4  30:34 - 36:08
- Escapes From Turtle Position 	  36:08 - 46:20
- Shoulder Roll Escapes 	46:20 - 1:00:01
- Shoulder Roll Escapes 2 	1:00:01 - 1:02:36
- Countering The Seatbelt: Inside Leg Escape 	1:02:36 - 1:17:34
- Countering The Seatbelt: Inside Leg Escape 2 	1:17:34 - 1:20:33
- Countering The Seatbelt: Inside Leg Escape 3 	1:20:33 - 1:24:37

<br>

</details>


<details>

<summary>1.2 Guard Retention</summary>

<br>

#### 1 Insights

- Introduction To Guard Retention 	0 - 24:00
- Guard Retention Overview 	24:00 - 1:16:06

<br>

#### 2 Theory and Requirements

- The Big Picture - General Theory of Guard Retention 	0 - 7:37
- 2 Most Basic Requirements of Guard Retention 	7:37 - 11:23
- The Importance of Guard Retention 	11:23 - 20:13
- The Unforgivable Sin of Guard Retention 	20:13 - 26:01
- The 3 Postures of Guard Retention 	26:01 - 29:46

<br>

#### 3 Skills

- The First Skill of Guard Retention - Movement-Seated Position - Scooting 	0 - 4:04
- Rolling Inversion 	4:04 - 8:52
- Hip Heisting 	8:52 - 15:46
- Supine Position - Shrimping 	15:46 - 21:55
- Pummelling 	21:55 - 26:24
- Pendulum 	26:24 - 29:36
- Scissoring 	29:36 - 34:59
- Spinning Inversion 	34:59 - 38:06
- Rollbacks 	38:06 - 40:59
- Back Heist 	40:59 - 45:41
- Turtle Position - Shoulder Roll 	45:41 - 53:33
- Sitting 	53:33 - 58:22
- Putting All Fundamental Body Movements Together 	58:22 - 1:02:57

<br>

#### 4 Framing

- 2nd Skill of Guard Retention - Framing - Nature and Purpose Of Framing 	0 - 6:40
- Frames & Distance - Forehand Frame 	6:40 - 10:22
- Forearm Frame 	10:22 - 14:35
- Back Hand Frames 	14:35 - 20:58
- Self Framing 	20:58 - 24:07
- 3rd Skill of Guard Retention - Breakign and Negating Grips - Breaking vs Negating 	24:07 - 32:45
- Ultimate Theory of Guard Retention - Demarcation Lines 	32:45 - 48:38

<br>

#### 5 Tools

- Connection 	0 - 6:26
- 5 Things Opponent Needs to Pass Your Guard 	6:26 - 20:49
- Single Most Important Tool of Guard Retention - Scissor Guard 	20:49 - 30:59
- 2nd Most Important Tool of Guard Retention - Hip Heist 	30:59 - 36:02
- 3rd Most Important Tool of Guard Retention - Back Heist 	36:02 - 38:55
- 4th Most Important Tool of Guard Retention - Power Prop 	38:55 - 49:14
- The Theory of Cycles of Offense & Defense 	49:14 - 1:01:09
- the Theory of Cycles of Offense & Defense 2 	1:01:09 - 1:05:21
- The Crticial Importance of Head Control in Guard Retention 	1:05:21 - 1:17:15

<br>

#### 6 Double Under Pass

- The Specific Theory of Guard Retention 	0 - 4:20
- Double Under Pass 	4:20 - 13:57
- Double Under Pass Part 2 	13:57 - 18:10
- Double Under Pass Part 3 	18:10 - 22:23
- Double Under Pass Part 4 	22:23 - 25:23
- Putting It All Together 	25:23 - 28:17
- Over Under Guard Pass Retention 	28:17 - 38:47
- Over Under Guard Pass Retention 2 	38:47 - 43:28
- Over Under Guard Pass Retention 3 	43:28 - 50:13
- Over Under Guard Pass Retention 4 	50:13

<br>

#### 7 Toreando Pass

- Toreando Guard Pass Retention 	0 - 8:21
- Toreando Guard Pass Retention 2 	8:21 - 12:17
- Toreando Guard Pass Retention 3 	12:17 - 17:14
- Toreando Guard Pass Retention 4 	17:14 - 19:49
- Toreando Guard Pass Retention 5 	19:49 - 24:47
- Toreando Guard Pass Retention 6 	24:47 - 26:53
- Knee Slice Guard Pass Retention 	26:53 - 35:14
- Knee Slice Guard Pass Retention 2 	35:14 - 39:32

<br>

#### 8 Leg Drag

- Leg Drag Retention 	0 - 7:13
- Leg Drag Retention 2 	7:13 - 9:52
- Leg Drag Retention 3 	9:52 - 13:41
- Leg Drag Retention 4 	13:41 - 15:38
- Leg Drag Retention 5 	15:38 - 20:28
- Long Step Guard Pass Retention 	20:28 - 32:15
- The Cranial Shift 	32:15 - 41:21
- Back Exposure Benign and Destructive 	41:21 - 50:51
- Head Protection 	50:51 - 55:55

</details>


<details>

<summary>1.3 Half Guard</summary>

#### 1 Insights

- Overview 	0 - 7:52
- General introduction to half guard bottom 	7:52 - 1:03:47

<br>

#### 2 Paradox

- The Paradox of Half Guard 	0 - 6:01
- Half Guard Bottom - The Alignment Issue 	6:01 - 10:40
- Kuzushi From Half Guard Bottom 	10:40 - 21:11
- Half Elbow Escape Series 	21:11 - 36:42
- Grip Dominance in Half Guard 	36:42 - 47:14

<br>

#### 3 Single Elbow Escape

- Single Elbow escape series 	0 - 11:43
- Single Elbow Escape Part 2 - Shoulder Crunch Sumi Gaeshi 	11:43 - 18:10
- Single Elbow Escape Part 3 - Scoop Half Butterfly 	18:10 - 23:14
- Scoop Half Butterfly Series 	23:14 - 30:17
- Scoop Half Butterfly Series Part 2 Rotating Round Leg 	30:17 - 38:12
- Scoop Half Butterfly Series Part 3 Details of Scoop Grip 	38:12 - 42;40
- The Double Elbow Escape Series 	42:40 - 54:13
- Reverse Crossface - Reverse Elbow Escape 	54:13 - 1:01:56
- Elbow Escape vs Reverse Underhook 	1:01:56 - 1:07:39
- Summarzing Half Guard From Inferior Grips 	1:07:39 - 1:13:07

<br>

#### 4 Underhooks

- Getting to Underhooks 	0 - 7:15
- Winning From UnderhookS - The Tight Waist Series 	7:15 - 15:07
- Fundamental Reason Why the Tight Waist Series is Most effective half guar position - The Trilemma 	15:07 - 21:57
- Roll Through Sweeps 	21:57 - 30:32
- Roll Through Sweeps 2 	30:32 - 33:33
- Roll Through Sweeps 3 	33:33 - 39:53
- The Movements Behind The Moves 	39:53 - 47:26
- The Lower Leg Shift 	47:26 - 58:09

<br>

#### 5 Rules

- The 90 Degree Rule 	0 - 4:07
- The Knee Lift 	4:07 - 8:48
- The Hip Shift 	8:48 - 13:39
- The Law of The Elbow 	13:39 - 20:54
- Directionality of Force For Roll Through Sweeps 	20:54 - 28:04
- The Single Most Important Section On This Video 	28:04 - 41:30
- Taking Your Opponents Back: Duck Under 	41:30 - 46:25
- Taking The Back From Tight Waist 	46:25 - 54:45
- The Limp Arm 	54:45 - 1:00:38

<br>

#### 6 Tight Waist vs Whizzer

- Tight Waist vs Whizzer Situation 	0 - 7:44
- Tight Waist vs Whizzer Situation 2 	7:44 - 10:42
- Tight Waist vs Whizzer Situation 3 	10:42 - 14:42
- Tight Waist vs Whizzer Situation 4 	14:42 - 17:44
- Tight Waist vs Whizzer Situation 5 - Going From Half Guard to Tight Waist 	17:44 - 24:21
- Foot Fighting From Half Guard 	24:21 - 32:38
- Practical Applications of Foot Fighting 	32:38 - 35:36
- Takedowns From Tight Waist: The outside Hook - Circular Finish 	35:36 - 43:35
- Linear Finish Double Leg 	43:35 - 48:51
- Linear Finish Double Leg Part 2 	48:51 - 54:16
- Reverse Double Leg 	54:16 - 1:01:22
- Stepping Around a Whizzer to a Far Hook 	1:01:22 - 1:07:48
- Summary of Tight Waist Series 	1:07:48 - 1:14:40

<br>

#### 7 Lapel Single Legs

- Lapel Single Legs From Half Guard 	0 - 6:27
- Lapel Single Legs From Half Guard 2 	6:27 - 17:20
- Lapel Single Legs From Half Guard 3 	17:20 - 21:53
- General Consideration on Posture for Half Guard Sweeps 	21:53 - 31:28
- General Consideration on Posture for Half Guard Sweeps 2 	31:28 - 40:00
- Summary of the Lapel Single Leg Series 	40:00 - 44:57

<br>

#### 8 Knee Lever

- Knee Lever Series 	0 - 12:21
- Knee Lever Series 2 	12:21 - 18:10
- The Back Roll Sweep 	18:10 - 38:14
- Problem of Reverse Half Guards 	38:14 - 53:56
- Problem of Reverse Half Guards 2 	53:56 - 1:04:58

</details>

<details>

<summary>1.4 Open Guard</summary>

#### 1 Insights

- 0 - 3:32 	Theory - What is the value of the open guard position?
- 3:32 - 10:08 	a contradiction in Jiu-Jitsu and the fundamental problem of guard position
- 10:08 - 14:18 	Finding advantage within a neutral position
- 14:18 - 19:42 	the four main scenarios you will need to deal with when working from open guard position
- 19:42 - 28:50 	how to grip the Gi for maximum results
- 28:50 - 44:11 	The single biggest determinant of success from open guard: Kuzushi
- 44:11 - 52:38 	The single biggest determinant of success from open guard: Kuzushi Part 2
- 52:38 - 1:02:54 	Overcoming the complexity problem: The 6 principles of sweeps from open guard - Feet off the floor principle
- 1:02:54 - 1:06:53 	Double direction
- 1:06:53 - 1:10:31 	Arm trap
- 1:10:31 - 1:14:46 	Action reaction
- 1:14:46 - 1:21:20 	Wrestling reversal
- 1:21:20 - 1:27:49 	Double trouble

<br>

#### 2 Skill Sets

- 0 - 35:09 	Two crucial skill sets for open guard development - the double seated position
- 35:09 - 47:37 	Two crucial skill sets for open guard development - first contact and entry into open guard

<br>

#### 3 Principles

- 0 - 10:06 	directionality of force
- 10:06 - 17:52 	principle of ken ken
- 17:52 - 27:17 	changing direction sumi gaeshi
- 27:17 - 31:00 	hook sweep/sumi gaeshi practical application
- 31:00 - 41:18 	the role of your two legs in hook sweeps
- 41:18 - 46:58 	hook sweep/sumi gaeshi angles of attack
- 46:58 - 57:06 	look where you want them to land
- 57:06 - 1:00:57 	multiple grips for hook sweep/sumi gaeshi
- 1:00:57 - 1:23:50 	hook sweep/sumi gaeshi grip and go
- 1:23:50 - 1:28:22 	dealing with a posted hand
- 1:28:22 - 1:35:36 	double direction sasae

<br>

#### 4 Sweeps

- 0 - 10:05 	hook sweep from a supine position
- 10:05 - 14:33 	hook sweep from a supine position part 2
- 14:33 - 18:19 	leg frames from butterfly guard
- 18:19 - 27:47 	Arm trap sumi gaeshi/ hook sweep series
- 27:47 - 38:41 	Hook sweep sumi gaeshi with arm trap
- 38:41 - 45:06 	Hook sweep sumi gaeshi combined with sasae
- 45:06 - 52:20 	Supine arm traps
- 52:20 - 57:03 	Yoko sumi gaeshi
- 57:03 - 1:01:37 	Side to side arm trap sumi gaeshi
- 1:01:37 - 1:06:29 	Arm drag/arm trap hook sweep
- 1:06:29 - 1:12;27 	sumi gaeshi on a standing opponent
- 1:12:27 - 1:19:03 	spider sumi gaeshi
- 1:19:03 - 1:23:37 	hiza guruma
- 1:23:37 - 1:33:53 	hiza guruma from underhooks
- 1:33:53 - 1:39:00 	spider guard on a kneeling opponent

<br>

#### 5 Sweeps

- 0 - 7:32 	hiza guruma when opponent is on one knee
- 7:32 - 9:58 	ankle pick (kibisu gaeshi)
- 9:58 - 14:55 	shoe lace to shoe lace
- 14:55 - 19:23 	kneeling opponent transition to ashi garami
- 19:23 - 25:33 	Attacking a standing opponent - Special Study: Tomoe Nage - getting feet off the mat with tomoe nage
- 25:33 - 26:26 	tomoe nage sweep De la riva variation
- 26:26 - 38:46 	tomoe nage sweep De la riva variation (Part 2)
- 38:46 - 44:17 	tomoe nage sweep De la riva variation (part 3)
- 44:17 - 50:43 	sumi Tomoe Nage
- 50:43 - 56:36 	sumi tomoe nage Part 2
- 56:36 - 1:02:09 	Lapel and spider Tomoe Nage
- 1:02:09 - 1:09:34 	Double Footed Tomoe Nage
- 1:09:34 - 1:16:12 	Lasso Tomoe Nage
- 1:16:12 - 1:20:36 	yoko Tomoe nage as a sweep

<br>

#### 6 Attacks

- 0 - 10:10 	Attacking from cross collar cuff and bicep - Triangle
- 10:10 - 13:41 	Armbar Juji Gatame
- 13:41 - 22:44 	Omoplata
- 22:44 - 28:04 	Tripod Sweep
- 28:04 - 34:02 	Ankle Pick/ Kibisu Gaeshi
- 34;02 - 40:36 	Uke Waza/ Lapel Drag
- 40:36 - 45:28 	Tomoe Nage
- 45:28 - 51:18 	Special Study: Inside Control vs Outside Control - De La Riva Guard
- 51:18 - 1:02:22 	De La Riva Guard Part 2
- 1:02:22 - 1:05:13 	De La Riva Guard Part 3
- 1:05:13 - 1:08:13 	De La Riva Guard Part 4
- 1:08;13 - 1:13:10 	transition from outside control to inside control: Ashi Garami
- 1:13:10 - 1:19:39 	transition from outside control to inside control: Ashi Garami Part 2
- 1:19:39 - 1:22:21 	transition from outside control to inside control: Ashi Garami Part 3
- 1:22:21 - 1:26:25 	transition from outside control to inside control: Ashi Garami Part 4

<br>

#### 7 X Guard

- 0 - 11:50 	Ashi Garami standard sweep
- 11:50 - 17:31 	Double Leg Ashi Garami
- 17:31 - 23:41 	Relationship between Ashi Garami & X-Guard
- 23:41 - 29:36 	X Guard scoop Sweep
- 29:36 - 35:07 	X Guard scoop Sweep Part 2
- 35:07 - 37:58 	X Guard Near Cuff Sweep
- 37:58 - 42:08 	X Guard Far Cuff Sweep
- 42:08 - 44:57 	Double Leg X Guard Sweep
- 44:57 - 49:06 	X Guard Tomoe Nage
- 49:06 - 54:52 	X Guard Back to ashi garami

<br>

#### 8 Double Kouchi Gari

- 0 - 11:09 	Double Kouchi Gari part 2
- 11:09 - 13:53 	Double Kouchi Gari part 3
- 13:53 - 16:39 	Double Kouchi Gari part 4
- 16:39 - 19:28 	Double Kouchi Gari part 5
- 19:28 - 22:07 	Double Kouchi Gari part 6
- 22:07 - 27:54 	Attacking an opponent in split squat (headquarters) - negating the split squat (Headquarters position)
- 27:54 - 40:51 	negating the split squat (Headquarters position) Part 2
- 40:51 - 49:37 	negating the split squat (Headquarters position) Part 3
- 49:37 - 55:27 	negating the split squat (Headquarters position) Part 4
- 55:27 - 59:53 	Putting it all together

<br>

</details>

<details>

<summary>1.5 Closed Guard</summary>

#### 1 Insights

- Closed Guard Overview 	0 - 10:57
- What Is The Central Message Of This Video? 	10:57 - 20:02 
- The First Three Abilities We Need To Be Effective From Closed Guard 	20:02 - 32:54
- The Deepest Message Of This Video – The Six Vulnerabilities Inside A Closed Guard 	32:54 - 36:27
- The Six Vulnerabilities - Part 1 	36:27 - 39:40
- The Six Vulnerabilities - Part 2 	39:40 - 42:24
- The Six Vulnerabilities - Part 3 	42:24 - 46:08
- The Six Vulnerabilities - Part 4 	46:08 - 52:02
- The Six Vulnerabilities - Part 5 	52:02 - 59:44
- The Six Vulnerabilities - Part 6 	59:44 - 1:06:10

<br>

#### 2 Concepts

- Understanding A closed Guard 	0 - 7:36
- General Overview Of The Closed Guard 	7:36 - 26:37
- A Key Insight With Closed Guard: Who Is Really On Top? 	26:37 - 31:16
- Holding And Controlling A Closed Guard 	31:16 - 54:08
- The Second Key To Postural Control: The Cross Collar Grip 	54:08 - 1:02:24
- Establishing Angle From Closed Guard 	1:02:24 - 1:11:58
- The Theory Of A Strong First Move 	1:11:58 - 1:18:25

<br>

#### 3 Side Scissor

- The Side Scissor: Elbow Across The Centerline - Converting a Closed Guard to a Side Scissor 	0 - 26:02
- The Subtle Push Pull Dynamic of the Side Scissor Position 	26:02 - 30:26
- Locking Down The Side Scissor 	30:26 - 35:10
- Unlocking The Great Secret of the Side Scissor Position 	35:10 - 41:54
- The Wrist Sweep From Side Scissor 	41:54 - 51:06
- The Elbow Sweep From Side Scissor 	51:06 - 56:17
- Rolling Armbar (Juji Gatame) 	56:17 - 1:02:59
- Side Scissor into Rear Triangle (Ushiro Sankaku) 	1:02:59 - 1:08:41
- Side Scissor Position Working with Lower Head Position: Flower Sweep 	1:08:41 - 1:14:13
- Pendulum Sweep 	1:14:13 - 1:22:04
- Knee Lever sweep 	1:22:04 - 1:27:00
- Hook Sweep (Sumi Gaeshi) 	1:27:00 - 1:31:06
- Hook Sweep (Sumi Gaeshi) Part 2 	1:31:06 - 1:35:39
- Troubleshooting The Transition From Closed Guard To Side Scissor: Shoulder Posting 	1:35:39 - 1:44:22
- Troubleshooting The Transition From Closed Guard To Side Scissor: Shoulder Posting 2 	1:44:22 - 1:47:20
- Troubleshooting The Transition From Closed Guard To Side Scissor: Shoulder Posting 3- Overcoming Biggest Problem with Side Scissor: Hip Sweep 	1:47:20 - 1:53:51
- Overview of the Side Scissor Series 	1:53:51 - 2:04:19

<br>

#### 4 Arm Bar

- The Top Lock/Armbar - The Top Lock 	0 - 8:52
- The Number One Grip 	8:52 - 20:27
- Attacking With the Arm Lock (Juji Gatame) From Top Lock Position 	20:27 - 30:40
- The 45 Inside Position 	30:40 - 44:01
- Arm Bar (Juji Gatame) From Closed Guard 	44:01 - 54:33

<br>

#### 5 Sweeps

- Knees/Hip On Floor - Flower Sweep 	0 - 11;06
- The Hip Sweep 	11:06 - 23:21
- Hip Sweep Setup - Misdirection 	23:21 - 28:01
- Hip Sweep Setup - Cross Elbow Post 	28:01 - 31:04
- Hip Sweep Setup - Grip Break Method 	31:04 = 34:37
- The Golden Rule of The Hip Sweep 	34:37 - 39:56
- Breaking Your Opponent’s Posture - Flower Sweep to Hip Sweep 	39:56 - 46:23
- Breaking Your Opponent’s Posture - Collar Tie Method 	46:23 - 49:48
- Breaking Your Opponent’s Posture - Scoop Sweep to Hip Sweep 	49:48 - 55:45
- Breaking Your Opponent’s Posture When Your Opponent Posts One Leg: Scoop Sweep to Hip Sweep 	55:45 - 59:21
- Cross Cuff Hip Sweep 	59:21 - 1:08:18
- The Hip Sweep: Putting It All Together 	1:08:18 - 1:12:04

<br>

#### 6 Sweeps

- Scissor Sweep 	0 - 14:41
- Scissor Sweep Double Sleeve Grips 	14:41 - 19:56
- 2 On 1 Grip Scissor Sweep 	19:56 - 23:00
- Scissor Sweep On a Posted Leg 	23:00 - 26:37
- The Pendulum Sweep 	26:37 - 43:31
- Pendulum Sweep With Cuff Grip 	43:31 - 48:31
- The Pendulum Sweep as a Learning Device 	48:31 - 52:17
- The Strongest Pendulum Sweep: Trapping The Arm 	52:17 - 59:09
- The Versatility of The Arm Trap 	59:09 - 1:02:27
- The Versatility of The Arm Trap Part 2 	1:02:27 - 1:06:03
- The Versatility of The Arm Trap Part 3 	1:06:03 - 1:10:57

<br>

#### 7 Clamp

- The Clamp/Hand On Floor - Transition From Closed Guard to The Clamp 	0 - 6:56
- Attacking From The Clamp 	6:56 - 30:13
- Trap Triangle/Inside Wrist - Inside Wrist Grip 	30:13 - 51:05
- Overhead Sweep 	51:05 - 1:00:07

<br>

#### 8 Standing

- Attacking A Standing Opponent - applying The Principle of Opportunity: The Scooping Sweep 	0 - 12:40
- applying The Principle of Opportunity: The Handstand Sweep 	12:40 - 18:27
- applying The Principle of Opportunity: The Leg Trap Sweep 	18:27 - 22:23
- Omoplata Sweeps on a Standing Opponent 	22:23 - 39:49
- The Double Ankle Sweep 	39:49 - 50:25

<br>

</details>

<details>

<summary>1.6 Guard Passing</summary>

#### 1 Insights

- Introduction to Passing the Guard 	0 - 3:40
- Philosophy of Opening a Closed Guard 	3:40 - 12:11
- Top Position: Opening a closed Guard 	12:11 - 30:31
- Opening Closed Guard 2 	30:31 - 37:41
- Opening closed guard 3 	37:41 - 44:26
- Opening Closed guard 4 	44:26 - 53:02
- Opening closed guard 5 	53:02 - 56:05
- Opening closed guard 6 	56:05 - 1:00:39
- Opening closed guard 7 	1:00:39 - 1:05:40
- Opening closed guard 8 	1:05:40 - 1:07:20
- Opening closed guard 9 	1:07:20 - 1:11:48
- Opening closed guard 10 	1:11:48 - 1:19:03
- Opening closed guard 11 	1:19:03 - 1:29:41

<br>

#### 2 Standard Method

- Overview of Standard Method Of Opening Closed Guard 	0 - 8:29
- Opening Closed guard: Knee Post Method 	8:29 - 16:12
- Knee Post Method 2 	16:12 - 21:40
- Knee Post Method 3 	21:40 - 23:53
- knee post method 4 	23;53 - 27:33
- knee post method 5 	27:33 - 29:30
- knee post method 6 	29:30 - 34:07
- Opening Closed Guard / Lifting Method 	34:07 - 41:16
- Overview of 3 methods 	41:16 - 47:10
- Problem of Procrastination 	47:10 - 52:50
- the Most Important Message Of This Video: The 5 Steps Of Guard Passing - Step 1 	52:50 - 1:00:13
- Step 2 	1:00:13 - 1:06:25
- Step 3 	1:06:25 - 1:10:52
- Step 4 	1:10:52 - 1:17:44
- Step 5 	1:17:44 - 1:27:10
- Negate Advantage Completion Guard Passing Model - NAC Open Guard Passing Method 	1:27:10 - 1:31:35
- Split Squat as Ultimate Staging Platform 	1:31:35 - 1:36:29
- Single Biggest Insight into Guard Passing: Side to Side Pressure 	1:36:29 - 1:43:50
- Mechanical and Tactical Advantage 	1:43:50 - 1:53:22

<br>

#### 3 High Percentage Methods

- First Requirement Of A Guard Passing Program: Technical Proficiency With The Most High Percentage Methods 	0 - 1:34
- Double Under Pass 	1:34 - 26:12
- Over Under Pass 	26:12 - 46:17
- Knee Cut Pass 	46:17 - 1:26:06

<br>

#### 4 Toreando

- Toreando Pass 	0 - 1:05:33

<br>

#### 5 Leg Drag

- Leg Drag 	0 - 35;06
- Long Step Guard Pass 	35:06 - 1:09:15
- The Smash Pass 	1:09:15 - 1:18:14

<br>

#### 6 Breaking Connections

- Second Requirement Of A Guard Passing Program: The Ability To Break Connections And Shut Down a Dangerous Guard 	0 - 7:26
- Cross Collar Cuff And Bicep Guard 	7:26 - 28:36
- De La Riva Guard 	28:36 - 42:37
- Reverse De la Riva 	42:37 - 47:13
- Lasso Guard 	47:13 - 52:56
- Spider And Lasso Guard 	52:56 - 1:00:18
- Double Spider Guard 	1:00:18 - 1:07:27
- Emergency Measures 	1:07:27 - 1:14:13
- Ashi Garami 	1:14:13 - 1:21:37
- The 3rd Requirement Of A Guard Passing Program: The Ability To Maintain Top Position 	1:21:37 - 1:23:40
- The Ancient Law Of Push When Pulled And Pull When Pushed 	1:23:40 - 1:31:26
- Recovering From A Fall 	1:31:26 - 1:44:38

<br>

#### 7 Staging Positions

- The Fourth Requirement of a Guard Passing Program: Using Staging Positions To Pass A Dangerous Guard - the four best staging positions - 1) The Split Squat Position (Headquarters) 	0 - 12:47
- Second Great Virtue of the Split Squat Position: hip Control and Head Control 	12:47 - 16:30
- Negating Both Feet From Split Squat 	16:30 - 21:45
- 2) The Knee Drop 	21:45 - 26:13
- 3) Double Knee Position 	26:13 - 29:56
- 4) Outside Advantage Position 	29:56 - 39:07
- Putting Skills & Theory Into Combative Context 	39:07 - 51:47
- Flattening an Opponent 	51:47 - 1:00:45
- Negating An Opponent’s Guard 	1:00:45 - 1:07:46
- The Fifth Requirement of a guard passing program: finding advantage within a neutral position - creating advantage while passing 	1:07:46 - 1:26:01

<br>

#### 8 Breaking Through Frames

- Guard Passing From The Shoulder Line 	0 - 8:35
- sixth requirement of a guard passing program: breaking through defensive frames and getting to your pin - Beating Frames - Distance 	8:35 - 13:48
- Angle Change 	13:48 - 16:04
- Direction 	16:04 - 20:39
- Directly Attacking The Frames 	20:39 - 24:11
- Side To Side Pressure 	24:11 - 29:35
- Beating Inversion 	29:35 - 33:40
- Turning In To Turtle 	33:40 - 38:24
- Turning Out To Turtle 	38:24 - 42:33
- Propping 	42:33 - 46:20

<br>

</details>

<details>

<summary>1.7 Half Guard Passing & Pins</summary>

#### 1 Insights

- Half Guard Passing Introduction 	0
- Overview of Half Guard Passing: The 4.6.3 System 	04:49
- 1st Skill: Forcing Half Guard 	26:54
- Pinning the Knee to the Mat 	33:29
- Inside Knee Position 	37:58
- Using Standard Guard Passes to Attain Half Guard 	43:05
- Double Knee Position 	48:53
- Clearing the Knee Shield 	59:12
- Clearing a Scorpion (Lockdown) 	01:18:39
- Clearing a Deep Half Guard 	01:30:35
- Negating Your Opponents Attacks 	01:41:02
- The Fundamental Gripping Stance 	01:45:16
- The Fundamental Starting Position (FSP) 	01:54:33

<br>

#### 2 Skills

- The Skill of Crossfacing & Underhooking 	0
- Shoelace Method 	22:10
- The Ball of Foot Method 	29:16
- The Hand Post & Elbow Post Method 	36:25
- Square Tripod Hand Post Method 	43:42
- The Shaking Method 	47:57
- Zig Zag Knee Direction 	51:35
- Pass from 3/4 Side Sit-Out 	55:05
- Pass from 3/4 Side 	01:07:38
- Reverse 3/4 Side 	01:12:55
- 3/4 Mount 	01:20:11
- Moving Between the 4 Dominant Passing Positions 	01:40:29
- Half Guard Passing Direction & The Law of Perpendicularity 	01:46:24

<br>

#### Passing

- The Single Most Important Reason Why I Put So Much Emphasis On Half Guard Passing 	0
- Different Upper Body Configurations for Half Guard Passing 	11:06
- A Crucial Skill of Half Guard Passing: Setting a Crossface 	18:19
- A Crucial Skill of Half Guard Passing: Setting a Crossface 2 	25:30
- Dealing with an Opponent's Underhook 	38:33

<br>

#### 4 Passing

- Configuring Your Free Leg 	0
- Configuring Your Free Leg 2 	05:23
- Configuring Your Free Leg 3 	08:01
- Passing Half Guard 1: Half Kata Gatame 	12:43
- Passing Half Guard 2: Lapel Cross Face 	21:04
- Passing Half Guard 3: Top Head and Arm 	29:06
- Passing Half Guard 4: Head Block Half Guard Pass 	37:49
- Passing Half Guard 5: Double Underhook Half Guard Passing 	42:30
- Near Side & Far Side Knee Wedges from Chest to Chest 	47:36
- Getting the Inside Knee To the Floor 	52:08
- Overview of the Half Guard Passing System 'The 4.6.3 Passing System' 	57:07

<br>

#### 5 Passing Half Butterfly

- Passing Half Butterfly Guard 	0
- The Golden Rule of Half Butterfly Guard Passing 	04:30
- Upper Body Gripping 	10:11
- Lower Body Leg Positioning 1 	22:51
- Lower Body Leg Positioning 2 	43:49
- Passing Half Butterfly Guard: Putting It All Together 	01:00:19

<br>

#### 6 Dynamic Pinning

- Introduction to Dynamic Pinning 	0
- Definition of a Pin 	09:02
- Location of Wedges 	12:34
- Base of Support 	17:50
- No Pin is Perfect But There is Perfect Pinning 	20:38
- The Main Directive of Dynamic Pinning 	25:22
- The 3 Golden Rules of Dynamic Pinning 	33:55
- Understanding The Scoring Criteria for Pins in Jiu Jitsu 	40:47
- The Battle for Inside Position 	57:23
- A Theory of Wedges, Inside Position, & Base 	01:05:30
- Static Pinning 	01:21:01
- Pushing 	01:31:15
- Shrimping 	01:40:50
- Bridging 	01:46:10
- Knee Escapes 	01:51:34

<br>

#### 7 Pins

- The 1st Skill 	0
- North South Pin (Variations) 	19:30
- Mounted Position (Variations) 	29:43
- Rear Mount (Variations) 	47:46
- Transioning Between Pins 	56:42
- Kneeling Step Over 	01:14:44
- A Sit 	01:26:46
- Side to Side 	01:35:16
- Side to Knee on Belly 	01:46:23
- Knee on Belly to Knee on Belly 	01:53:40
- Mount to Back (Reactive) 	02:00:31
- Mount to Back (Proactive) 	02:11:16
- Mount to Back (Leg Entry) 	02:23:09

<br>

#### 8 Mount

- Transitioning Betweeb Pins (cont.) 	0
- Rear Mount to Mount 	07:39
- Side to Rear Mount 	12:10
- Side to Rear Mount (Reactive) 	20:21
- Extracting a foot 	28:51
- Maintaining the Mounted Position: Practical Applications 	38:45
- Maintaining the Side Position: Practical Applications 	50:43
- Retaining the Rear Mount 	01:01:04
- The Minimum Requirement of Back Control: Diagonal Control 	01:13:27
- Lower Body Game 	01:18:52
- The Three Most Important Moves to Maintain Lower Body Control 	01:24:45

<br>

</details>

<details>

<summary>1.8 Strangles & Turtle Breakdowns</summary>

#### 1 Insights

- 5:48 	the major theme of this video: the 3x3 approach to gi strangles
- 9:33 	Understanding The Gi and Strangles
- 15:35 	Sliding Collar Strangle - Demarcation Line
- 23:44 	Wrist flick
- 28:02:00 	How Much is Too Much?
- 32:01:00 	Hands In Unison
- 35:36:00 	Comparing Gi & No Gi Strangulation
- 39:23:00 	3 Alternatives For Control Hand
- 46:06:00 	Single Greatest Advantage of Lapel Strangles
- 55:07:00 	strenghthening the Strangle Hand - 3 Finger Grip
- 1:04:36 	Role of the Thumb on the Strangle Hand
- 1:16:44 	Short & Long Leg Strangles
- 1:23:37 	the Hidden hand
- 1:29:32 	The Strangle Shift and Cross back position

<br>

#### 2 Sliding Collar

- 1:07 	Sliding Collar Strangle System Step 2 Establishing The Strangle Hand
- 5:16 	Sliding Collar Strangle System Step 3 Establishing Control Hand
- 9:05 	Sliding Collar Strangle System Step 4: Maximizing Mechanical Power 4 Key Ingredients
- 11:46 	Full Mastery of the sliding collar strangle
- 20:03 	System Modification
- 34:51:00 	System Modification 2
- 44:06:00 	System Modification 3
- 49:29:00 	Strangling Without Hooks - Clock Strangle
- 54:10:00 	Clock Strangle - Foot Work
- 1:08:04 	Clock Strangle - Throwing Weight Onto Your Hands
- 1:13:15 	Clock Strangle - Walking The Circle
- 1:20:15 	Clock Strangle - Unified Hands are Opposing Hands
- 1:23:55 	Clock Strangle - Power of Sprawling Finish
- 1:27:02 	Clock Stranlge - Critical Role of Head Position
- 1:29:44 	Clock Strangle - Nullifying Defense
- 1:31:54 	Clock Strangle - Nullifying Defense 2
- 1:34:38 	Clock Strangle - Alternative Grips

<br>

#### 3 Cross Collar

- 2:25 	Cross Collar Strangles - Strangle Hand & Finishing Hand
- 5:28 	Cross Collar Strangles - Setting The Lapels
- 8:46 	Cross Collar Strangles - Setting Strangle Hand
- 12:44 	Cross Collar Strangles - Biting Method
- 17:40 	Cross Collar Strangle - Setting Finishing Hand
- 21:46 	Cross Collar Strangle - Head Position
- 26:15:00 	Cross Collar Strangle - Directionality of Force
- 32:28:00 	Juji Jime - A dilemma based attack
- 39:56:00 	Juji Jime - Bottom Position Kuzushi Based Method
- 45:55:00 	Juji Jime Bottom Position Fighting Through The Hands - High Elbow / Low Elbow
- 53:58:00 	Juji Jime From Mounted Position
- 1:03:58 	Mounted Juji Jime - Setting The Finishing Hand
- 1:13:51 	Troubleshooting Mounted Juji Jime
- 1:26:47 	Troubleshooting Mounted Juji Jime 2
- 1:33:25 	Variations of Juji Jime - Kata Juji Jime

<br>

#### 4 Jime

- 4:38 	Kata Juji Jime from Front Side Top Turtle Position
- 8:34 	Using your Legs in Kata Juji Jime
- 12:23 	Spinning Juji Jime
- 23:17 	Spinning Juji Jime 2
- 32:09:00 	Sprawling Juji Jime
- 45:17:00 	Troubleshooting Sprawling Juji Jime
- 50:16:00 	Juji Jime - Cross Collar Strangle From Open Guard
- 55:52:00 	Low Lapel Juji Jime

<br>

#### 5 Ezekiel

- 3:03 	Combat Applications of the Back Ezekiel
- 17:32 	The Front Ezekiel Strangle
- 24:28:00 	The Single Biggest Problem Associated with Front Ezekiel and solution
- 35:35:00 	rear strangle system order of operations
- 47:47:00 	Bonus: Low Lapel Strangle

<br>

#### 6 Turtle Breakdowns

- 3:16 	The Fundamental Dilemma Upon Which Our Turtle Breakdowns Are Based
- 9:59 	The 2 Central Problems Of Turtle Breakdowns - First Problem: Escapes and Counters
- 14:44 	Second Problem: The Elbow Knee Connection
- 22:27 	Four Stances From Which To Attack Turtle Position
- 25:39:00 	The Three Main Targets Of Your Turtle Attacks - target #1: The Hips
- 43:24:00 	Target #2: The Head
- 58:17:00 	Target #3: The Wrists
- 1:05:50 	Which Hook Should I Put In First: Near Side Or Far Side?

<br>

#### 7 Clock Theory

- 6:16 	The Theory Of The Clock
- 14:56 	The Easiest And Most Practical Way To Use Clock Theory To Your Advantage
- 22:24 	Scoring From The Hip Breakdown
- 32:22:00 	Scoring From The Seated Breakdown
- 48:04:00 	Scoring From The Shoulder Breakdown

<br>

#### 8 Seat Belt

- 18:28 	Seat Belt Roll Method
- 23:14 	Back Crucifix Method
- 32:10:00 	Kimura Method

<br>

</details>

<br>

# Enter The System

<br>

<details>

<summary>2.1 Back Attacks</summary>

#### 1 Overview

- Back Attack System Overview          	0 - 7:20
- Introduction 	7:20 - 8:20
- The Back As Ultimate Position 	8:20 - 9:35
- Limitations of Straitjacket System 	9:35 - 11:56
- ntroduction to Straightjacket System 	11:56 - 13:32
- General Reflections on the Back 	13:32 - 15:00
- Left/Right Control 	15;00 - 22:28
- Strangle Hand/Control Hand Dichotomy 	22:28 - 24:54
- Fundamental Insight of Offense & Defense on The Back: The head trap 	24:54 - 30:25
- Alignment and Deficit Problem 	30:25 - 37:03
- Solving Alignment Problem - Understanding 	37:03 - 46:06
- Protecting The Bottom Hook 	46:06 - 56:49
- Through Movement 	56:49 - 1:04:36
- Through Movement - 2 	1:04:36 - 1:11:41
- Switching Arms 	1:11:41 - 1:16:01
- Solving The Deficit Problem 	1:16:01 - 1:25:58
- SOLVING THE PINNED ARM PROBLEM 	1:25:58 - 1:34:24

<br>

#### 2 Principles

- Neutrality Principle 	0 - 9:57
- Primacy Principle 	9:57 - 12:38
- Percentage Principle 	12:38 - 15:12
- Top Position 	15:12 - 19:06
- Hidden Hand Principle 	19:06 - 23:55
- Specific Grips Principle 	23:55 - 28:04
- Default Principle 	28:04 - 32:23
- Heirarchy Principle 	32:23 - 38:40
- Concentration/Diffusion Principle 	38:40 - 41:48
- Closed Wedges Principle 	41:48 - 48:53

<br>

#### 3 Grips

- Underarm Side 	0 -14:40
- Double Straight Direct Grip 	14:40 - 19:11
- Single Cross Grip 	19:11 - 25:45
- Double Cross / Indirect Grip 	25:45 - 30:40
- Double Cross / Direct Grips 	30:40 - 36:42
- Neck Penetration System 	36:42 - 46:55
- New Theory of Strangling Mechanics 	46:55 - 57:36

<br>

#### 4 Strangles

- One Handed Strangle 	0 - 4:24
- Technical Notes on Rear Naked Strangle 	4:24 - 12:38
- Mandible Strangles 	12:38 - 20:16
- Intro Overarm Side 	20:16 - 32:07
- Overarm Side / Wedging Method 	32:07 - 39:04
- Overarm Side / Side Switching Method 	39:04 - 46:13
- Working of Straitjacket System Conclusion 	46:13 - 50:33

<br>

#### 5 Auxiliary Systems

- Auxiliary Systems Introduction 	0 - 3:33
- Rear Triangle System 	3:33 - 12:49
- Diversifying Attacks 	12:49 - 22:00
- Troubleshooting 	22:00 - 30:34
- Reverse Top Lock System 	30:34 - 44:28
- Cross Body Ride 	44:28 - 53:49
- Leg Attacks 	53:49 - 1:06:41

<br>

#### 6 Transitions

- Leg Attacks 2 	0 - 10:17
- Arm Attack/Hammer Lock 	10:17 - 19:10
- Twister 	19:10 - 25:27
- Transitions 	25:27 - 32:56
- Back Crucifix 	32:56 - 43:00
- Strangles 	43:00 - 53:30
- Arm Locks 	53:30 - 1:10:29

<br>

#### 7 Hooks

- establishing hooks and rear mounts Introduction 	0 - 7:08
- focus Points 	7:08 - 18:05
- Foot and Knee Entries 	18:05 - 31:23
- Near Side Entries 01 	31:23 - 39:54
- Near Side Entries 02 	39:54 - 46:32
- Near Side Entries 03 	46:32 - 53:16

<br>

#### 8 Entries

- Near Side Entries 04 	0 - 7:23
- Near Side Entries 05 	7:23 - 18:10
- Rolling Entries 01 	18:10 - 29:45
- Rolling Entries 02 	29:45 - 41:58
- FOUR Point Position 	41:58 - 53:08
- Conclusion 	53:08 - 54:51

<br>

</details>

<details>

<summary>2.2 Triangles</summary>

#### 1 Mechanics

- 0 - 20:09 	multiplicity of triangles
- 20:09 - 22:48 	The 2 stage approach to triangles
- 22:48 - 24:56 	Mechanics
- 24:56 - 27:24 	Mindset
- 27:24 - 29:02 	Your body and the triangle strangle
- 20:02 - 37:54 	solo pummeling drills for triangle development
- 37:54 - 42:36 	solo body folding drills
- 42:36 - 47:11 	Partner Pummeling Drills
- 47:11 - 51:53 	Partner Inversion and Rotational Drill
- 51:53 - 56:19 	Turning Off The Shoulder
- 56:19 - 58:50 	Introduction to the 5 Triangles
- 58:50 - 1:08:47 	rotating ARound Head and Shoulders with Triangles
- 1:08:47 - 1:14:49 	Fundamental Transition From Trap Triangle to Figure 4 Triangle.
- 1:14:49 - 1:20:04 	Triangles Out Of Scrambles
- 1:20:04 - 1:26:22 	Pros and cons pulling down head w ith hands
- 1:26:22 - 1:34:18 	Pros and Cons elbow acrossw chest
- 1:34:18 - 1:39:22 	upper body to lower body

<br>

#### 2 Mecahnics

- 0 - 13:45 	Front Triangle Mechanics 2
- 13:45 - 18:06 	Front Triangle Mechanics 3
- 18:06 - 21:04 	Managing Transition from Trap Triangle to Figure Four Triangle
- 21:04 - 25:50 	Managing Transition from Trap Triangle to Figure Four Triangle 2
- 25:50 - 31:11 	Putting It All Together - Front Triangle
- 31:11 - 39:39 	Unified Legs
- 39:39 - 43:21 	Getting Lower Back Off Groud and Onto Points of Shoulders
- 43:21 - 47:26 	Story of Every Triangle is the Same
- 47:26 - 52:36 	Entries to Front Triangle - Seated Position
- 52:36 - 1:00:09 	Magic of 2 on 1 Gripping
- 1:00:09 - 1:07:54 	triangle vs single leg
- 1:07:54 - 1:12:33 	triangle vs single leg 2
- 1:12:33 - 1:16:04 	triangle vs single leg 3
- 1:16:04 - 1:19:26 	triangle vs single leg 4
- 1:19:26 - 1:23:12 	triangle vs single leg 5

<br>

#### 3 Setups

- 0 - 5:03 	Collar Tie Setups
- 5:03 - 7:58 	Over Hook Grip To Triangle
- 7:58 - 12:30 	Underhooks to Triangle
- 12:30 - 22:39 	Inside Wrist Grip
- 22:39 - 29:126 	Kuzushi Based Entires into Front Triangle
- 29:16 - 36:52 	Foot Penetration V Knee Penetration
- 36:52 - 41:41 	The Clamp
- 41:41 - 49:47 	Entering Clamp Guard
- 49:47 - 56:15 	Wrist Controls to Triangles
- 56:15 - 1:01:03 	Wrist Controls to Triangles 2
- 1:01:03 - 1:06:28 	Monoplata to Front Triangle
- 1:06:28 - 1:11:58 	fake hip sweep to front triangle

<br>

#### 4 Front Triangles

- 0 - 7:42 	Front Triangles Top Position Passing Guard
- 7:42 - 12:23 	Front Triangles Top Position Passing Guard 2
- 12:23 - 16:55 	Front Triangle Side Position
- 16:55 - 25:37 	Front Triangle Side Position 2
- 25:37 - 33:21 	Front Triangle Side Position 3
- 33:21 - 37:49 	Front Triangle Side Position 4
- 37:49 - 42:30 	Front Triangle Mounted Position
- 42:30 - 47:24 	Triangle From Tight waist vs whizzer
- 47:24 - 50:28 	Relationship between front triangle and other submissions
- 50:28 - 58:22 	Relationship between front triangle and other submissions 2
- 58:22 - 1:03:01 	Relationship between front triangle and other submissions 3
- 1:03:01 - 1:07:03 	Relationship between front triangle and other submissions 4
- 1:07:03 - 1:11:05 	transition to inside heel hook

<br>

#### 5 Opposite Triangles

- 0 - 11:52 	Front Triangle, Opposite Triangle and Elbow Position
- 11:52 - 22:13 	Sweeping and Off Balancing with Opposite Triangle
- 22:13 - 30:01 	transition from opposite triangle to joint locks

<br>

#### 6 Rear Triangles

- 0 - 6:30 	Mechanics of Rear Triangle
- 6:30 - 11:22 	Entries Into Rear Triangle Back Position
- 11:22 - 15:37 	Rear Triangle From Back 2
- 15:37 - 17:59 	Rear Triangle From Back 3
- 17:59 - 20:29 	Rear Triangle From Side Positions
- 20:29 - 25:59 	Rear Triangle From Side Positions 2
- 25:59 - 32:01 	Rear Triangle from Mounted Position
- 32:01 - 38:30 	Relationship Between Rear Triangle and Front Triangle
- 38:30 - 42:52 	Rear Triangle to Opposite Triangle
- 42:52 - 45:47 	Rear Triangle Out of Armlock
- 45:47 - 48:38 	Rear Triangle From Guard Passing
- 48:38 - 51:42 	Rear Triangle From Referees Position
- 51:42 - 55:58 	Rear Triangle Transitioning From Strangle to Joint Locks

<br>

#### 7 Side Triangle

- 0 - 15:30 	Attacking Turtle with Side Triangle
- 15:30 - 24:09 	Attacking Turtle with Side Triangle 2
- 24:09 - 31:02 	Attacking Turtle with Side Triangle 3
- 31:02 - 34:29 	Linking side triangle and arm bar
- 34:29 - 41:05 	side Triangle While Guard Passing
- 41:05 - 43:52 	Side Triangle Switching To Arm Locks

<br>

#### 8 Reverse Triangle

- 0 - 5:34 	reverse triangle Gyaku Sankaku
- 5:34 - 9:52 	Entering Reverse Triangle
- 9:52 - 16:58 	Entering Reverse Triangle 2
- 16:58 - 21:10 	Entering Reverse Triangle 3
- 21:10 - 25:43 	Reverse Triangle to Armlocks

<br>

</details>

<details>

<summary>2.3 Armbars</summary>

#### 1 Overview

- Overview 	0 - 5:00
- Speed Based Juji Gatame 	5:00 - 26:59
- Overview 1st Phase - Entry 	26:59 - 34:44
- overview 2st Phase - Control 	34:44 - 40:13
- overview 3rd Phase - Orientation 	40:13 - 43:56
- Overview 4th phase Configuration Phase 	43:56 - 48:34
- overview 5th phase Separation Phase 	48:34 - 55:52
- overview 6th Phase Breaking 	55:52 - 1:02:35
- Central Problems of Juji Gatame 	1:02:35 - 1:17:41
- 3 Central Problem Opponent on Back 	1:17:41 - 1:24:53
- 3 Central Problem Attacking From Back. 	1:24:53 - 1:34:44
- Core Principles 	1:34:44 - 1:43:58

<br>

#### 2 Mechanics

- Mechanics of Control 	0 - 10:08
- Generating Movement from Juji Position 	10:08 - 15:11
- Crossed Feet 	15:11 - 20:47
- Crossed Feet For Juji Gatame Pros and Cons 	20:47 - 32:49
- Elbow Propping Gripping the Legs 	32:49 - 37:09
- Putting Our Control Mechanics Together 	37:09 - 39:27
- Finishing Juji Through 360 degrees 	39:27 - 49:07
- Mechanics of the Position 	49:07 - 59:37
- Mechanics of Breaking 	59:37 - 1:07:13
- Mechanics of Breaking 2 	1:07:13 - 1:15:57
- Mechanics of Breaking 3 	1:15:57 - 1:23:45
- Breaking Mechanics - Opposing Forces 	1:23:45 - 1:29:37

<br>

#### 3 Separation

- Mechanics of Separation 	0 - 17:56
- Mechanics of Separation 2 	17:56 - 25:00
- Mechanics of Separation 3 	25:00 - 30:02
- Mechanics of Separation 4 	30:02 - 35:13
- understanding mechanical advantage of cross chest position 	35:13 - 39:45
- 5 Progressions of Hand Position 	39:45 - 47:36
- 4 Phases of Juji Gatame 	47:36 - 52:18
- Multiplicity 	52:18 - 1:12:56
- Entering Into Juji Gatame 	1:12:56 - 1:29:41
- Entering Into Juji Gatame 2 	1:29:41 - 1:37:59
- Entering Into Juji Gatame 3 	1:37:59 - 1:45:26
- Entering Into Juji Gatame 4 	1:45:26 - 1:49:55
- 2 General Approaches To Armbars - Speed Based & Control Based Methods 	1:49:55 - 1:55:37

<br>

#### 4 Top Lock

- Reflexions on The Top Lock 	0 - 8:06
- Armbar From Guard - Critical Role of Head & Elbow Position 	8:06 - 12:54
- 2 Step Approach to Bottom Juji Gatame 	12:54 - 19:47
- Troubleshooting the Top Lock 	19:47 - 25:10
- Armbar Bottom Position - TopLock vs Shoulder Pivot. 	25:10 - 30:02
- Shoulder to Shoulder Transfer 	30:02 - 42:05
- Sweeping w Armbars - Base, Direction of Force & Direction of Feet 	42:05 - 52:34
- Human Body and Juji Gatame 	52:34 - 1:02:04
- CLASSROOM Bottom Armbar: Going From Top Lock To Juji 	1:02:04 - 1:07:38
- single Step Juji Gatame Attacks (widecam) 13:45 	1:07:38 - 1:21:17
- Bottom Position - Armbar Critical Law & Revealing Test 	1:21:17 - 1:29:42
- Figure Four Top Lock 	1:29:42 - 1:40:01
- Alternative Bottom Juji - Overwrap Grip 	1:40:01 - 1:45:50
- Juji Gatame on Standing Opponent 	1:45:50 - 1:56:19

<br>

#### 5 Scoops and Entries

- Inside Scoop and Outside Scoop 	0 - 5:03
- Iron Rule of Head and Hips 	5:03 - 14:52
- Iron Rule of Head and Hips 2 	14:52 - 17:57
- 3 Quarter Juji Gatame From Bottom 	17:57 - 24:31
- Going across Center Line With Juji Gatame 	24:31 - 30:50
- Belly Down Juji From Bottom 	30:50 - 34:43
- Seated Juji Entries From Bottom 	34:43 - 44:17
- Seated Juji Entries From Bottom 2 	44:17 - 48:28
- Seated Juji Entries From Bottom 3 	48:28 - 53:33
- Seated Juji Entries From Bottom 4 	53:33 - 57:53
- Seated Juji Entries From Bottom 5 	57:53 - 1:00:02
- Seated Juji Entries From Bottom 6 	1:00:02 - 1:02:17
- Seated Juji Entries From Bottom 7 	1:02:17 - 1:04:18

<br>

#### 6 Juji Gatame

- Introduction to Top Position Juji 	0 - 3:47
- The Ratchet 	3:47 - 21:08
- Iron Law of Elbow and Shoulder Line 	21:08 - 26:14
- Applying Juji Gatame From Mount 	26:14 - 39:42
- Key to Mounted Juji - Shoulder Wedge 	39:42 - 44:30
- Skillful Use of Cross Face Leg 	44:30 - 51:04
- Putting it All Together 	51:04 - 58:42

<br>

#### 7 Juji Gatame

- Underhook Juji From Mount 	0 - 5:10
- Classroom: Mounted Arm Bar - Underhook Setup 	5:10 - 8:21
- Cross Wrist Juji Attack From Mount 	8:21 - 12:06
- Arm Wraps and Mounted Jujis 	12:06 - 18:38
- Classroom: Key Insight - Biomechanical Pins vs Gravity Pins 	18:38 - 24:49
- Single Most Important Drill for Top Juji Mastery 	24:49 - 35:01
- Top Position Juji From Side Pins 	35:01 - 52:08
- Quarter Juji Gatame as Entry Method 	52:08 - 56:37
- Spinning Juji 	56:37 - 1:07:00
- Reverse Underhook Juji 	1:07:00 - 1:14:10
- Near Side Juji Gatame Cross Step Method 	1:14:10 - 1:18:46
- Near Side Juji Overwrap Grip 	1:18:46 - 1:23:26

<br>

#### 8 Juji Gatame Lower Body

- Juji Gatame and Lower Body Attacks 	0 - 11:39
- juji Gatame & Lower Body Attacks 2 	11:39 - 16:06
- Juji Gatame & Lower Body Attacks 3 	16:06 - 17:44
- Juji Gatame & Lower Body Attacks 4 	17:44 - 21:58
- Introduction to Juji Gatame From Chest To Back Situations 	21:58 - 25:41
- Rolling Your Opponent into Juji 	25:41 - 40:08
- Rolling Your Opponent into Juji 2 	40:08 - 43:50
- Rolling Your Opponent into Juji 3 	43:50 - 47:12
- Rolling Your Opponent into Juji 4 	47:12 - 53:21
- Rolling Your Opponent into Juji 5 	53:21 - 1:01:39
- juji Gatame From Rear Mount 	1:01:39 - 1:11:05

<br>

</details>

<details>

<summary>2.4 Kimuras</summary>

#### 1 Overview

- System Overview 	0 - 15:48
- Trinity of Power 	15:48 - 21:15
- Thumb or no Thumb 	21:15 - 26:19
- Essential Nature of Kimura - A Twisting Lock 	26:19 - 30:23
- Hand and Arm Positioning 	30:23 - 37:47
- Bending A Straightened Arm 	37:47 - 43:21
- Diagonol Power Line 	43:21 - 49:30
- Pull Dominant Kimuras vs Push Dominant Kimuras 	49:30 - 52:11
- Beating The Shoulder and Trapping Head 	52:11 - 56:35
- Shoulder Line 	56:35 - 59:51
- A Vexing Problem 	56:35 - 1:04:07
- Essential Skill of Kimura System - Roll Through Kimura Scrambles 	1:04:07 - 1:11:54

<br>

#### 2 Dorsal Kimura

- Introduction to Dorsal Kimura 	0 - 7:53
- Unlocking Reinforced Hands 	7:53 - 17:45
- Using Legs For Kimura 	17:45 - 34:04
- Switching to Alternative Submission 	34:04 - 39:18
- Switching to Different Position / System 	39:18 - 48:56

<br>

#### 3 T Kimura

- Introduction to T Kimura 	0 - 10:58
- Top and Bottom Knee 	10:58 - 14:15
- Bottom Knee to Back 	14:15 - 17:41
- Bottom Knee to Rear Triangle 	17:41 - 20:01
- Bottom Knee to Arm Bar 	20:01 - 22:10
- Bottom Knee to Side Triangle 	22:10 - 24:22
- Bottom Knee Penetration to Russian Arm 	24:22 - 29:44
- Bottom Knee into Cross Ashi Garami 	29:44 - 32:50
- Bottom Knee Losing Russian Arm 	32:50 - 38:49
- Top Knee Attacks 	38:49 - 46:05
- Relationship Between Kimura and Bicep Slicer 	46:05 - 54:03

<br>

#### 4 Side Kimura

- Introduction to Applications 	0 - 3:43
- Side Kimura Applications 	3:43 - 13:19
- Side Kimura Switching Elbows 	13:19 - 17:13
- Side Kimura Supinated Grip 	17:13 - 21:03
- Side Kimura Pronated Grip 	21:03 - 26:13
- Half Guard Top 	26:13 - 34:09
- Half Guard to T Kimura 	34:09 - 39:12
- Half Guard Attacking Power Prop 	39:12 - 44:11
- Classroom Commentary #1 	44:11 - 45:57
- Classroom Commentary #2 	45:57 - 49:46
- Classroom Commentary #3 	49:46 - 52:38

<br>

#### 5 Kimura vs Defense

- Butterfly Guard 	0 - 4:27
- kimura vs Butterfly Guard Moving Head to Setup Kimura 	4:27 - 7:50
- Kimura vs Butterfly Guard Standing 	7:50 - 11:24
- Kimura vs Butterfly Guard Standing Opposite Side 	11:24 - 16:06
- Kimura vs Butterfly Guard Working From Outside Position 	16:06 - 19:34
- Kimura's Kimura 	19:34 - 31:53
- Turtle Position Olympic Bar to Kimura 	31:53 - 35:39
- Kimura vs Turtle Side Triangle 	35:39 - 39:26
- Kimura vs Single Leg on Mat 	39:26 - 47:58
- Kimura vs Single Leg on Mat Drill 	47:58 - 55:45
- Classroom Commentary #4 	55:45 - 59:28
- Classroom Commentary #5 	59:28 - 1:02:43

<br>

#### 6 Kimura From Guards

- Kimura vs Single Leg Countering the Peak Out 	0 - 4:59
- Kesa-Gatame 	4:59 - 14:43
- Closed Guard Kimura - The Great Mystery of Closed Guard Kimura 	14:43 - 38:15
- Kimura Closed Guard Wrist Control and Power Prop 	38:15 - 46:20
- Closed Guard Kimura Head Pass Setup 	46:20 - 51:46
- Kimura From Bottom Half Guard 	51:46 - 1:02:05
- Bottom Half Guard Kimura Converting to Full Guard 	1:02:05 - 1:05:46
- Half Guard Kimura Sumi Gaeshi Variations 	1:05:46 - 1:09:08
- Classroom Commentary #6 	1:09:08 - 1:11:06
- Classroom Commentary #7 	1:11:06 - 1:12:39

<br>

#### 7 Kimura vs Defense

- Bottom Half Guard To T Kimura 	0 - 6:21
- Half Guard Kimura Vs Arm Bar 	6:21 - 14:44
- Half Guard Kimura To Front Triangle 	14:44 - 24:44
- Kimura Bottom Turtle Position 	24:44 - 31:32
- Kimura Bottom Turtle Inside Arm Roll Staying Above the Hips 	31:32 - 37:54
- Kimura Bottom Turtle Inside Arm Roll to Kimura Finish 	37:54 - 40:33
- Kimura Bottom Turtle vs Seat Belt 	40:33 - 45:07
- Kimura Bottom Turtle Outside Arm Roll 	45:07 - 49:56
- Kimura Bottom Turtle Outside Arm Variation 	49:56 - 55:21
- Standing Position 	55:21 - 1:01:47
- Kimura vs Standing Single Leg Head Outside 	1:01:47 - 1:04:34
- Kimura vs Standing Single Moving The Head 	1:04:34 - 1:08:05
- Kimura Vs Single Leg Kneeling Opponent 	1:08:05 - 1:12:33
- Kimura Vs Double Leg 	1:12:33 - 1:17:00
- Kimura vs Double Leg (Cont.) 	1:17:00 - 1:19:02
- Classroom Commentary #8 	1:19:02 - 1:21:18

<br>

#### 8 Kimura Variations

- Legs Kimura / Reverse Omoplata 	0 - 14:29
- TRIMURA 	14:29 - 33:53
- Multiple Kimuras 	33:53 - 45:14
- Timing the Roll Through 	45:14 - 51:33
- Elbow Wedge for Kimura 	51:33 - 54:22
- Head and Arm Kimura 	54:22 - 1:00:03
- Classroom Commentary #9 	1:00:03 - 1:05:39

<br>

</details>

<details>

<summary>2.5 Front Headlocks</summary>

#### 1 Insights

- System Overview 	0 - 18:05
- Introduction Front Headlock 	18:05 - 24:52
- Control vs Submission 	24:52 - 27:42
- 4 Main Scenarios of Front Headlock System 	27:42 - 30:46
- 4 Points of Control 	30:46 - 37:48
- Vulnerabilities of Front Headlock 	37:48 - 45:06
- Helpful Terminology and Concepts 	45:06 - 49:52
- 4 Step System 	49:52 - 54:25

<br>

#### 2 Entries

- Reactive entries 	0 - 6:17
- Reactive entries single 	6:17 - 11:11
- Reactive entries down blocks 	11:11 - 16:09
- Proactive entries snap downs 	16:09 - 23:20
- Inside tie straight snap 	23:20 - 27:51
- Inside tie side to side snap downs 	27:51 - 30:04
- Mutual collar ties snap down 	30:04 - 34:39
- Inside tie cross snap 	34:39 - 39:17
- Over tie snap down - outside Control 	39:17 - 46:42
- Half overtie snap 	46:42 - 50:39
- 2 on 1 snap down 	50:39 - 58:38
- Over hook and under hook snap downs 	58:38 - 1:03:09
- Fake shot snap down 	1:03:09 - 1:10:14
- Taking standing front headlock to Mat - circling method 	1:10:14 - 1:13:22
- Taking stand front headlock to Mat sumi-gaeshi 	1:13:22 - 1:17:11

<br>

#### 3 Variations

- introduction To Kneeling Fronbt Headlocks 	0 - 2:54
- high wrist series 	2:54- 14:47
- centerline shift 	14:47 - 17:54
- high elbow guillotine first impressions 	17:54 - 23:12
- low elbow guillotine high wrist position 	23:12 - 27:30
- arm in guillotine high wrist position 	27:30 - 32:48
- high wrist position one handed guillotine 	32:48 - 36:56
- high wrist power guillotine 	36:56 - 41:40
- Kneeling front headlock katagatame series 	41:40 - 47:19
- front katagatame anaconda strangle 	47:19 - 55:32

<br>

#### 4 Anaconda

- Front kata gatame anaconda strangle - Olympic roll 	0 - 8:32
- Anaconda strangle leg assist 	8:32 - 14:58
- Anaconda strangle critical details 	14:58 - 20:05
- Anaconda strangle elbow to elbow method 	20:05 - 32:41
- Darce strangle / inverted kata gatame 	32:41 - 54:30

<br>

#### 5 Darce

- Darce strangle / inverted kata gatame elbow to elbow method 	0 - 3:28
- Seated kata gatame 	3:28 - 10:23
- Seated kata gatame - alternative finish 	10:23 - 13:40
- Cradle series 	13:40 - 19:35
- Cradle Leg Assist 	19:35 - 28:36
- Knee block go behind - mixing submission and position 	28:36 - 38:06
- Hand block go behind 	38:06 - 41:03
- Arm drag go behind 	43:33 - 47:52
- Putting it all together 	47:52 - 53:24

<br>

#### 6 Seated Guard

- Introduction To Seated Front Headlocks 	0 - 4:47
- Seated guard snap downs 	4:47 - 10:11
- Seated guard snap down leg assist 	10:11 - 11:57
- Forward shift snap down 	11:57 - 13:54
- High elbow guillotine in detail 	13:54 - 28:47
- Low elbow guillotine in detail 	28:47 - 36:45
- Arm in guillotine in detail 	36:45 - 46:41

<br>

#### 7 Hip Heists

- Hip heist 	0 - 6:44
- Collar tie hip heist 	6:44 - 12:07
- Shoulder post hip heist 	12:07 - 16:40
- Forward shift and hip heist 	16:40 - 18:30
- Overhook hip heist 	18:30 - 21:35
- Protecting the knee line with hip heist 	21:35 - 26:19
- Seated front headlock positional game 	26:19 - 32:58
- Yoko Sumi gaeshi 	32:58 - 40:48

<br>

#### 8 Front Headlock vs Guards

- Introduction to Guard Passing 	0 - 8:10
- Conventional guillotines vs seated guard 	8:10 - 11:45
- Front headlock guard pass 	11:45 - 14:55
- Front headlock inverted North South 	14:55 - 20:00
- Front headlock vs half guard 	20:00 - 28:16
- Front headlock vs half guard 2 	28:16 - 33:31
- Front headlock vs half guard 3 	33:31 - 38:25
- Front headlock vs half guard 4 	38:25 - 42:02
- Thumb to sternum the principle of closure 	42:02 - 47:10

<br>

</details>

<details>

<summary>2.6 Leglocks</summary>

#### 1 Overview

- Introduction 	0 - 1:54
- Breakdown 	1:54 - 9:05
- Explication 	9:05 - 10:56
- Independant Thinking 	10:56 - 12:15
- Overview 	12:15 - 15:20
- Manifestations of Leg Lock System 	15:20 - 18:41
- Grip and Inside Position 	18:41 - 22:38
- Gripping 	22:38 - 26:43
- 2 on 1 Gripping 	26:43 - 33:25
- Immovable ElbCHAPTER TITLE
- START TIMEow 	33:25 - 39:00
- Push Pull Dynamic 	39:00 - 42:38
- Torso Controls 	42:38 - 46:22
- Hand Over Shoulders 	46:22 - 52:23
- 2 on 1 Leg 	52:23 - 56:49
- shin To Shin 	56:49 - 1:00:51
- Standing Opponent 	1:00:51 - 1:05:33
- scoop gripping 	1:05:33 - 1:11:16
- Ankle Gripping 	1:11:16 - 1:15:09

<br>

#### 2 Inside Position

- Attaining Inside 	0 - 5:00
- Standing Opponent 	5:00 - 7:49
- De La Riva 	7:49 - 12:17
- Pummelling Legs Top Position 	12:17 - 18:40
- Movement Inside 	18:40 - 25:07
- Supine Opponent 	25:07 - 32:09
- Hand Post - Leg Pummeling 	32:09 - 39:53
- Irimi Ashi 	39:53 - 48:48
- Knee and Knee Line 	48:48 - 53:54
- Connection Drills 	53:54 - 58:05
- Penetration Drills 	58:05 - 1:04:13
- Gravity Drills 	1:04:13 - 1:09:08
- Gravity Drills 2 	1:09:08 - 1:14:20
- Gravity Drills 3 Rolling 	1:14:20 - 1:17:43
- Recovering Knee Line 	1:17:43 - 1:22:41

<br>

#### 3 Ashi Garami

- Introduction 	0 - 1:43
- Single Leg Ashi 	1:43 - 7:07
- double Leg Ashi 	7:07 - 10:19
- Inside Leg Breakdowns 	10:19 - 16:12
- Cross Ashi 	16:12 - 19:03
- Introduction 	19:03 - 22:14
- Lifting Method 	22:14 - 28:27
- Lifting Method Cross Ashi 	28:27 - 30:59
- centerline Method 	30:59 - 33:24
- Tilting Method 	33:24 - 35:39
- negating Resistance Intro 	35:39 - 44:16
- General Theory Intro 	44:16 - 45:45
- General Theory 	45:45 - 54:05
- Follow Through 	54:05 - 59:04
- Slack Removal 	59:04 - 1:04:30
- Gripping 	1:04:30 - 1:11:25
- Outside Heel Hook 	1:11:25 - 1:18:42
- Inside Heel Hook 	1:18:42 - 1:25:49

<br>

#### 4 Ashi Variations

- Intro 	0 - 2:26
- Shared Spiral Problem 	2:26 - 7:12
- Single Ashi Solution 	7:12 - 11:43
- Multiple Ashi Solution 	11:43 - 16:14
- Exposing The Heel 	16:14 - 19:37
- Negating Resistance 	19:37 - 26:52
- Breaking 	26:52 - 36:43
- Cycles of Inside Outside Dominance 	36:43 - 43:04
- Stopping The Spiral 	43:04 - 48:46
- CONTROL SPEED OF THE SPIRAL 	48:46 - 1:11:46

<br>

#### 5 Straight and Cross Ashi

- introduction 	0 - 14:11
- similarities & differences of straight nd cross ashi 	14:11 - 18:04
- primary & secondary leg 	18:04 - 19:38
- double & single Leg approaches 	19:38 - 21:55
- turnout problem 	21:55 - 26:28
- double leg approach - ankle lace ashi 	26:28 - 30:18
- release problem 	30:18 - 32:09
- managing the release 	32:09 - 36:46
- alternative achilles grip 	36:46 - 41:38
- double heel hook attack 	41:38 - 45:06
- lace leg (creating dilemma) 	45:06 - 47:59
- splitter hand 	47:59 - 53:50
- elbow slicer 	53:50 - 57:17
- over under method 	57:17 - 1:02:01
- double elbow 	1:02:01 - 1:04:21
- double cross method 	1:04:21 - 1:07:46
- controlling shared spiral 	1:07:46 - 1:15:47

<br>

#### 6 Central Problems

- intro 	0 - 2:21
- Central Problems 	2:21 - 4:59
- Untying Legs 	4:49 - 10:18
- Double Leg Roll 	10:18 - 15:47
- Figure Four Toe Hold 	15:47 - 19:26
- Dilemma Attack 	19:26 - 26:37
- Figure Four toe Hold Primary Leg 	26:37 - 35:06
- Figure Four toe Hold Non Optimal Conditions 	35:06 - 42:25
- Transition to Outside Ashi Outside Heel Hook 	42:25 - 48:17
- Transition to Cross Ashi Inside Heel Hook 	48:17 - 53:22
- Intro 	53:22 - 55:00
- Head Position 	55:00 - 1:01:10
- Opponent Spins Leg Crossover 	1:01:10 - 1:05:46
- Static Crossover 	1:05:46 - 1:09:48
- Spin Leg To Cross Ashi 	1:09:48 - 1:13:34
- Spin Leg To Reverse Ashi 	1:13:34 - 1:16:56
- Rolling Transition From straight to Cross Ashi 	1:16:56 - 1:22:27

<br>

#### 7 Entries

- Sliding Ashi 	0 - 4:58
- Colloring Elbow 	4:58 - 8:31
- Double Leg Entry 	8:31 - 11:46
- Single Leg Entry 	11:46 - 16:07
- Front Headlock 	16:07 - 18:25
- Arm Drag 	18:25 - 21:32
- 4 Point Breakdown 	21:32 - 24:02
- Intro Ground position 	24:02 - 28:35
- Bottom Single Elevator 	28:35 - 32:57
- Crossed Feet Single Elevator 	32:57 - 36:47
- Inversion 	36:47 - 41:22
- Knee Shield Inversion 	41:22 - 45:39
- Knee Shield to X Guard 	45:39 - 49:43
- Hook Sweep to Reverse X Guard 	49:43 - 54:32
- Arm Drag 	54:32 - 58:07
- Collar and Heel Entry 	58:07 - 1:02:17
- Shoelace Breakdown 	1:02:17 - 1:07:41

<br>

#### 8 Entries

- Sitback 	0 - 6:56
- Inside Out Entry 	6:56 - 9:50
- Outside In Entry 	9:50 - 13:04
- Outside Step In 	13:04 - 17:23
- Half Guard Entries 	17:23 - 26:58
- Kani Basami 	26:58 - 30:24
- Double Seated Position 	30:24 - 45:24

<br>

</details>

<details>


<summary>2.7 Submission Escapes</summary>

#### 1 Overview

- INTRODUCTION 	0 - 1:35
- Introduction: The Importance of studying submission escapes 	1:35 - 5:26
- Training Methodology of escapes 	5:26 - 7:26
- Philosophy of training escapes 	7:26 - 11:27
- How to Solve problems on defense 	11:27 - 17:44
- The 4 Stages of Defense 	17:44 - 24:04
- ARMBAR - Introduction 	24:04 - 24:52
- Prevention From Top 	24:52 - 31:57
- Prevention From Bottom 	31:57 - 36:33
- Full Juji Turning Escape 	36:33 - 42:08
- Full Juji Stack Escape 	42:08 - 47:45
- Crossed Full Juji 	47:45 - 51:52
- Combination Escape 	51:52 - 56:07
- ¾ Juji 	56:07 - 1:00:23
- The Top Lock Escape 	1:00:23 - 1:04:41
- ¼ Juji 	1:04:41 - 1:07:39

<br>

#### 2 KIMURA

- KIMURA - Introduction	0 - 2:31
- Topside Prevention	2:31 - 7:34
- Bottom Side Prevention	7:34 - 11:32
- Full Guard Escape	11:32 - 15:06
- Side Control Top	15:06 - 18:48
- Turtle	18:48 - 21:40
- Half Guard Top	21:40 - 24:43
- Half Guard Bottom	24:43 - 28:53
- T Kimura The Late Escape	28:53 - 33:55
- T Kimura Leg Frame	33:55 - 37:34
- Side Control Bottom	37:34 - 39:36
- Single Leg	39:36 - 41:59

<br>

#### 3 GUILLOTINE

- Overview 	0 - 1:16
- Low Elbow Guillotine 	1:16 - 5:47
- Late Escape High Elbow 	5:47 - 7:56
- Arm-In Guillotine 	7:56 - 11:52
- High Elbow Guillotine 	11:52 - 14:31
- Figure 4 Guillotine 	14:31 - 16:40
- Arm Spin Escape 	16:40 - 20:07
- Half Guard Bottom 	20:07 - 22:53

<br>

#### 4 TRIANGLES

- Overview	0 - 1:53
- Prevention: Front and Reverse Triangle	1:53 - 5:48
- Prevention: Part 2 Intercepting Legs	5:48 - 8:51
- Prevention: Part 3 The Shoulder Turn	8:51 - 11:24
- Front Triangle Early Escape	11:24 - 14:35
- Front Triangle Late Escape	14:35 - 18:18
- Back Triangle Prevention	18:18 - 22:42
- Back Triangles	22:42 - 28:14
- Reverse Triangles	28:14 - 31:31
- Side Triangle - Yoko Sankaku	31:31 - 34:52
- Inverted Triangle Escapes	34:52 - 37:45

<br>

#### 5 KATA GATAME

- kata gatame (darce anaconda) - Introduction 	0 - 2:35
- Front Headlock 	2:35 - 5:42
- Inverted Kata gatame/Darce 	5:42 - 11:06
- Standard Kata Gatame / Anaconda Strangle 	11:06 - 14:27
- Mounted Kata Gatame 	14:27 - 18:51
- BACK ESCAPES - Overview 	18:51 - 23:35
- Figure 4 Body Lock Escapes 	23:35 - 27:07

<br>

#### 6 HEEL HOOKS

- heel hooks - Intro 	0 - 4:44
- irimi / Standard Ashi Garami 	4:44 - 8:28
- Outside Ashi Garami 	8:28 - 13:05
- 50 / 50 	13:05 - 18:31
- Inside Sankaku / Inside Heel Hook 	18:31 - 23:53
- ACHILLES LOCKS - Overview 	23:53 - 28:04
- Sit-Out Escape 	28:04 - 31:05
- Rotational Escape 	31:05 - 35:16

<br>

#### 7 TOE HOLDS

- toe holds - Overview 	0 - 3:25
- Single Leg Toehold Escapes 	3:25 - 6:47
- Double Trouble Toehold 	6:47 - 10:29
- KNEE BARS - Overview 	10:29 - 12:46
- Single Leg Knee Bar 	12:46 - 16:39
- Double Leg Lat Knee Bar 	16:39 - 20:43
- INSIDE POSITION DRILL 	20:43 - 32:02

<br>

#### 8 OMOPLATA 

- omoplata - Overview 	0 - 2:43
- Knee Wedge Escape 	2:43 - 4:13
- Step Over Escape 	4:13 - 6:06
- The Roll Through Late Escape 	6:06 - 8:59
- LIVE TRAINING 	8:59 - 10:13
- Mounted 	10:13 - 14:11
- Front Headlock 	14:11 - 17:43
- 50 / 50 	17:43 - 21:33
- Juji Gatame 	21:33 - 25:14
- Trap Triangle 	25:14 - 28:43
- Back Attack 	28:43 - 32:33
- Full Guard 	32:33 - 36:40
- OUTRO 	36:40 - 38:48

<br>

</details>

<details>

<summary>2.8 Takedowns & Back Control</summary>

#### 1 Overview

- Introduction 	0 - 0:28
- Slide by 	0:28 - 3:26
- Slide by to Mat Return 	3:26 - 5:42
- S Motion Snapdown 	5:42 - 9:47
- Seated Guard to Half Guard 	9:47 - 13:49
- Knee Lever Back Take 	13:49 - 20:00

<br>

#### 2 Arm Drags

- High Tension Arm Drag 	0 - 5:04
- High Tension Arm Drag Single Leg 	5:04 - 9:37
- Arm Drag Swing Single 	9:37 - 15:00
- Knee Pull Single 	15:00 - 21:29
- Misdirection Go Behind 	21:29 - 25:15
- Outside Ankle Pick 	25:16 - 31:59
- Inside Ankle Pick 	31:59 - 34:36
- Nearside Underhook Passing 	37:06 - 59:55
- Reverse Underhook Turning Pass 	59:55 - 1:04:57
- Turning Pass Arm Trap 	1:04:57

<br>

#### 3 

- High Crotch 	0 - 3:42
- Nicky Rod Dump 	3:42 - 10:29
- Single Leg Defense Kick Through 	10:29 - 13:46
- Kick Through Arm Drag 	13:46 - 19:13
- Defensive Ankle Pick 	19:13 - 22:10
- Shin Whizzer 	22:10 - 28:35

<br>

#### 4

- Pancake 	0 - 3:23
- Re Shot Low Outside Single 	3:23 - 6:32
- Nicky Rod Cartwheel Pass 	6:32 - 9:50
- Chest Wrap Roll Through 	9:50 - 13:58
- How I Improved in BJJ 	13:58 - 17:29
- Competing with higher belts 	17:29 - 19:51
- Pop the top 	19:51 - 20:49

<br>

</details>

<br>

# New Wave 

<details>

<summary>3.1 Submission Escapes</summary>

#### 1 Standing Guillotine

- Introduction 	0
- Escaping the Guillotine - Standing Guillotine Defense - Hip Post Method 	7:25
- Ude Gaeshi 	16:24
- Tradition 	28:49
- Centerline Theory 	34:56
- Beating the High Elbow 	50:19
- The Spinning Escape 	55:07
- Defending the Arm-In Guillotine - Sit-Out Wrist Control 	1:03:14
- Arm Spin to Centerline 	1:17:32
- First reactions: Traditional VS Modern 	1:21:32
- Knee Posting 	1:30:58
- Hand Fighting and Guillotines 	1:37:50

<br>

#### 2 Guillotine

- Defending the Guillotine in Closed Guard - Head Slipping 	0
- Hip Slip 	11:57
- Intro to the Von Flue 	22:49
- Mechanics of the Von Flue 	27:45
- First Application: Closed Guard 	34:02
- Second Application: Side Control 	42:24
- Third Application: Guard Passing 	47:53
- A Summary of Our Guillotine Defense Philosophy 	55:24

<br>

#### 3 Arm Bar

- Arm Bar (Juji Gatame) Defense - Important Considerations 	0
- Variations in Arm Configuration 	10:18
- Counter Offense From Juji Gatame 	15:27
- Elbow Escape From Juji Gatame 	19:53
- The Turning Escape 	28:23
- The Passover Escape 	49:36
- The Cross Face Escape 	1:01:21
- The Misdirectional Escape 	1:08:12
- Defending Juji Gatame From Guard 	1:20:10
- Defending Juji Gatame From Rear Mount 	1:30:43
- Escape Juji Gatame From Turtle Position 	1:37:51

<br>

#### 4 Triangle

- Triangle Defense - General Considerations on the Triangle 	0
- The Thumb Post Escape 	15:13
- Cross Ankle Defense 	27:40

<br>

#### 5 Kimura Defense

- Kimura Defense - Understanding Kimura So That We Can Escape Kimura 	0
- Keeping Your Hand in Front Of You 	9:50
- Denying Wrist Control 	28:28
- Elbow Slipping 	35:41
- Freeing Your Head 	45:06
- The Best Kimura Defense Drill You Can use 	52:02
- Trap Kimura 	1:02:49
- Escaping T Kimura 	1:10:10
- Standing Kimuras 	1:21:57
- Layers of Defense to Kimura - 1st Layer of Defense - Wrists 	1:30:47
- 2nd Layer of Defense - The Elbow 	1:34:26
- 3rd Layer of Defense - Head Mobility/Body Mobility 	1:36:14
- Counter Attacking vs Kimura 	1:41:42

<br>

#### 6 Heel Hook

- Heel Hook Defense - The Law of Mutual Exposure 	0
- The Most Basic Distinction in Ashi Garami: Inside Position and Outside Position 	9:53
- Traditional Approaches to Heel Hook Defense in Jiu Jitsu 	14:46
- A New Philosophy of Heel Hook Defense 	24:27
- The Art of Heel Slipping 	38:10
- The Art of Heel Slipping 2 	55:43
- Hiding the Heel: The Law of Knee and Toes 	1:02:45
- The Art of Knee Slipping 	1:09:56
- The 4 Fundamental Defensive Positions for Inside heel Hooks 	1:21:28
- Ankle Control 	1:44:54

<br>

#### 7 Cross Ashi Garami

- Cross Ashi Garami Defense - Defending Inside Foot Position - Hip Scoot Escape 	0
- The Misdirectional Escape 	6:52
- Separation Escape 	15:45
- Countering a Lace Leg (Double Trouble) 	21:27
- Defending Outside Foot Position - Contrasting Inside and Outside Foot Position 	36:06
- Differences in Defending 50/50 vs Inside Sankaku 	44:39
- Heel Slipping in 50/50 	51:50
- True 50/50 	58:49
- Countering the Inverted Knee/Inside Shoulder 	1:12:18
- His Preference/Your Preference 	1:21:12
- Small Considerations That Make Big Differences 	1:27:41
- When to Turn/When Not to Turn 	1:38:33
- When to Turn/When Not to Turn part 2 	1:49:46

<br>

#### 8 50/50

- The 3 Fundamental Defensive Positions in 50/50 	0
- Backside Applications 	0.9097222222
- Defending Backside Part 2 	29:00
- Straight Ashi Garami Defense 	33:32
- Heel Slipping From Straight Ashi 	38:05
- Defending Irimi Ashi Garami Part 1 	47:06
- Defending Irimi Ashi Garami Part 2 	53:35
- Defending the Reap to Inside Ashi Garami 	57:27
- Defending Outside Ashi Garami 	1:05:21
- Defending Outside Ashi Garami Part 2 	

<br>

</details>

<details>

<summary>3.2 Positional Escapes</summary>

#### 1 

- Introduction 	0
- Escape Skills Are The Basis of Your Confidence in Jiu Jitsu 	8:05
- The Traditional Approach to Escape in Jiu Jitsu 	15:59
- The 2 Defensive Skills of Jiu Jitsu 	20:47
- Positional Escapes - What is a Pin? 	22:32
- Solving the Problem of Pins 	27:13
- A New Philosophy of Pin Escapes: Satisficing vs Maximizing 	33:03
- The Principle of Sunk Costs 	42:00
- The Game Plan for Our New Escape Philosophy 	50:41
- The 5 Pins of Jiu Jitsu 	58:57

<br>

#### 2 Escaping Mount

- Escaping the Mounted Position: Maximizing Philosophy - The Kipping Escape - The Lateral Kipping Escape 	0
- The Misdirectional Kipping Escape 	25:21
- The Overhead Kipping Escape 	38:34
- The Elbow Escape - Inside and Outside Variations 	55:04
- Further Reflections on the Outside Elbow Escape 	1:05:36

<br>

#### 3 Escaping Rear Mount

- Escaping the Rear Mount Position - The First Battle of Back Escapes: The Hand Fight 	0
- The Second Battle of Back Escapes: The Head Fight 	12:23
- Back Escapes Part 2 	23:16
- Escaping The Body Triangle - Body Triangle Preliminaries 	32:42
- The Theory of the 4 Triangles 	39:54
- The Fifth Triangle 	49:46
- The Theory of the Upper Body Connection: The Spinning Escape 	59:14
- The Headlock Escape 	1:15:16

<br>

#### 4 Belly Escapes

- Knee on Belly Escapes - Part 1 	0
- Knee on Belly Escapes - Part 2 	13:01
- Side Pin Escapes - Side Elbow Escape 	20:38
- Side Elbow Escape Part 2 	33:06
- The Role of the Head in Side Escapes 	42:42
- Different Defensive Frame Options 	48:30
- Frame Options With the Outside Arm 	54:45
- The Central Problem of Crossface Side Pins: Near Hip/Far Hip Connection 	58:17
- The Central Problem of Reverse Cross Face Side Pins: Near Shoulder/Far Shoulder Connection 	1:03:51
- Understanding Variations in Side Pins 	1:06:59

<br>

#### 5 Kipping

- The Role of Kipping in Side Elbow Escapes 	0
- The Role of Reverse Shrimping in Side Elbow Escapes 	4:48
- Side Elbow Escape: Putting it All Together 	10:20
- A Special Study: The Clamp 	15:31
- Side Elbow Escape Into Leg Locks 	34:23
- The High Leg Escape 	49:00
- The High Leg Escape Part 2 	1:01:27
- The Knee Escape 	1:12:39
- The Knee Escape Part 2 	1:23:47
- The Knee Escape Part 3 	1:29:18

<br>

#### 6 Ankle Trap Escape

- Ankle Trap Escape 	0
- Tricep Post Escape 	8:31
- The Back Door Escape 	24:08
- Escaping Sit-Out Pins 	41:38
- Escaping North South Pins - Unique Elements of North/South Pins 	57:03
- The High Leg Escape 	1:05:31
- Variations of North/South Require Variations in Escape 	1:14:16
- Escaping Other Variations of North/South Pins 	1:18:54

<br>

#### 7 Escaping Turtle

- Escaping Turtle Position - Preliminaries 	0
- Escaping the Tight Waist - Makikomi 	5:37
- Shoulder Roll 	15:52
- Escaping Body Locks 	27:25
- Reach-Back Sumi Gaeshi 	35:49
- Escaping Seat Belt 	40:46
- Peek-Out 	51:41
- General Hand Fighting From Turtle 	54:28

<br>

#### 8 Summary

- Summarizing Our Philosophy and Method of Positional Escape: The 5 Step Method - Defensive Responsibility 	0
- Kuzushi 	4:44
- The First Frame 	9:48
- Primary Entry Point 	14:45
- Majority Inside Control 	21:34
- The Sixth Step - Your Choice 	25:46

<br>

</details>

<details>

<summary>3.3 Open Guard 1</summary>


#### 1 Intro 

- Introduction 	0
- Understanding New Wave Guard Play: 3 Key Insights 	8:52
- Connection & Grip - The First Action From Guard is Grip 	14:07
- Four Limbed Gripping 	40:33:00
- Inside & Outside Position/Control 	44:40:00
- 2 On 1 Gripping 	52:59:00
- The Theory of Handles 	1:14:14
- Gripping Tempo 	1:25:02
- Gripping With Direction 	1:33:01

<br>

#### 2 Laws

- The Secret to Successful Guard Play: Dynamic Energy Theory 	0
- The Law of Shoulders & Hips (Push Pull Dynamic Knees) 	5:33
- The Law of Heel & Toes (Push Pull Dynamic Standing) 	14:11
- The Law of Head Under/Head Over (Under Over Dynamic) 	21:53
- The First Skill of Guard Play: Retention 	7:54
- The Knowledge/Denial Model of Guard Retention 	11:37
- The Six Elements of Guard Passing - Distance 	13:53
- Grip 	16:10
- Angle 	18:09
- Level 	43:46:00
- Penetration 	45:57:00
- Pin 	48:37:00

<br>

#### 3 Guard Retention

- 3 Critical Elements of Guard Retention - The Theory of Demarcation 	0
- Essential Body Movements For Guard Retention 	10:52
- Foot Pummeling 	12:30
- Crossovers 	17:15
- High Leg 	19:59
- The Inverted Spin 	2:26
- The Scissor and Sit 	8:14
- The Pendulum 	13:44
- Scissor and Roll 	44:00:00
- Shrimping 	50:51:00
- Reverse Shrimping 	55:18:00
- Scooting 	57:40:00
- Framing 	1:01:02
- Framing High Framing Low 	1:20:02
- Framing & The Law of Feet & Hips 	1:23:03

<br>

#### 4 Applications

- Applications - Mobility Based Passes 	0
- Pressure Based Passes: Stack Passing, Body Lock Passing & Knee Cut Passing 	7:39
- Going Beyond the Knowledge/Denial: A Maximizing Theory of Guard Retention 	7:13
- The Golden Rule of Counter Offense from Guard Retention 	20:27
- Putting it all together 	3:55

<br>

#### 5 Guard Play

- The Second Skill of Guard Play: Constant Threat Theory - Behavior is Determined by Belief 	0
- The One Target That Your Opponent Can Never Hide From You 	4:57
- Direct Attacks on the Legs From Guard Position - 2 on 1 Ankle Entries 	14:03
- Partial Inversions From Distance 	15:51
- Shin to shin entry 	50:44:00

<br>

#### 6 Entries

- Single Leg Entry 	0
- Double Leg Entry 	2:30
- Reverse Double Leg Entry 	8:59
- Irimi Ashi Cross Catch 	15:48
- Cross Catch to Hip Pin 	49:00:00

<br>

#### 7 X Guard

- X-Guard Triple Attack 	0
- X-Guard Momentum Entry 	17:21
- Leg Entries From Reverse De La Riva 	2:58
- Leg Entries From Reverse De La Riva Part 2 	15:06
- Leg Entries From Reverse De La Riva Part 3 	43:10:00
- Leg Entries From Reverse De La Riva Part 4 	48:45:00
- Leg Entries vs Split Squat (Headquarters) 	56:54:00
- Leg Entries vs Split Squat (Headquarters) part 2 	1:10:34
- Leg Entries vs Split Squat (Headquarters) part 3 	1:15:54

<br>

#### 8 Entries

- De La Riva Entries 	0
- De La Riva Entries Part 2 	8:59
- Closed Guard Entries 	15:27
- Closed Guard Entries Part 2 	30:36:00
- Putting it all together 	40:54:00

<br>

</details>

<details>

<summary>3.4 Open Guard 2</summary>

#### Insights

- Introduction 	0
- Understanding New Wave Guard Play: 3 Key Insights 	11:04
- General Reflections on the Difference Between Gi and No Gi Guard Play 	40:18
- Winning Strategies From Guard - The Distance Dilemma 	1:11:28
- Seated Shots vs Standing Shots 	1:18:31
- The Fundamental 3 Step Pattern of Wrestling Reversals 	1:33:24

<br>

#### 2 Reversals

- Wrestling reversals - Ankle Picks 	0
- Knee Pull 	7:45
- The Switching Knee Pull 	12:16
- Double Leg 	16:19
- Arm Drag 	26:21
- Ashi Waza (Foot Sweeps) 	32:36
- Wrestling Reversals on a Kneeling Opponent 	39:15
- 2 Phase Reversals 	52:00
- A Thought Experiment That will Change Your Outlook On No-Gi Reversals and Sweeps 	56:13
- Tripod Sweep Part 1 	1:11:49
- Part 2: Reverse Tripod Sweep 	1:22:06
- Part 3: Outside Tripod Sweep 	1:28:41
- Double Shin Sweep 	1:33:16
- Irimi Ashi Garami Sweep 	1:38:11
- Double Leg Ashi 	1:44:54

<br>

#### 3 X Guard

- X Guard 	0
- X Guard Heist Reversal 	10:41
- Shoelace Heist Reversal 	22:33
- X Guard Scissor 	30:44
- Double Leg X Guard 	41:03
- Overclasp X Guard 	52:41
- Double Kouchi Gari 	1:03:19
- Shoelace Double Kouchi 	1:12:19
- De Ashi Harai 	1:19:14
- Taking Opponents Forward Rather Than Backwards While Scrimmaging 	1:23:23
- Controlling the Scramble From 2 Phase Reversals/Scrimmages 	1:33:05

<br>

#### 4 Sumi Gaeshi

- Sumi Gaeshi - Sumi Gaeshi as the Principal Attack On a Keeling Opponent 	0
- The Mechanics of Sumi Gaeshi 	3:48
- Crucial Insight Regarding Your Shoulder for Sumi Gaeshi 	21:21
- Winning the Scramble After Sumi Gaeshi 	26:19
- Collar and Elbow Sumi Gaeshi 	32:01
- Double Triceps Sumi Gaeshi 	41:48
- Double Overhook Sumi Gaeshi 	49:32
- Over Under Sumi Gaseshi 	1:00:46
- Important Note on Butterfly Sumi Gaeshi: Double Directionality 	1:10:30
- Arm Trap Sumi Gaeshi 	1:17:30
- Shoulder Crunch Sumi Gaeshi 	1:25:36
- Forward Shift and Sumi Gaeshi 	1:40:33
- Body Lock Butterfly Sweep 	1:46:01

<br>

#### 5 Upper Body

- Upper Body Submissions - Triangle, Arm Bar and Guillotine 	0
- Triangle Entries - 2 On One Elbow 	4:29
- Hand to Hand Triangle Setups 	13:39
- Lat Pinch Triangle 	20:00
- Overties Traingles 	23:42
- Forearm and Elbow 	27:34
- Overhook and Wrist Triangles 	30:10
- Clamp - Special Study: The Clamp 	35:05
- Rolling Triangle From the Clamp 	41:25
- Clamp to Inverted Juji Gatame 	45:02
- Arm Bar (Juji Gatame) 	49:29

<br>

#### 6 Guillotines

- Guillotines - Football Snap Guillotine 	0
- The High Wrist Guillotine 	9:12
- Arm In Guillotine 	14:37
- Hip Heisting - The Most Neglected Skill in Guard Play 	24:19
- Hip Heist Snap Downs 	33:37

<br>

#### 7 Go Behinds

- The Skill of Go Behinds - Knee Block Go Behind 	0
- Hand Block Go Behind 	08:23
- Arm Drag Go Behind 	14:48
- Misdirectional Go Behind 	21:57
- Front Body Lock Go Behind 	26:27
- Hip Heist Guillotine 	33:13
- Roll Through Guillotines 	40:00
- Making the Hip Heist a Central Feature of Your Guard Play 	49:11
- Hip Heisting From the Whizzer 	54:53
- Hip Heisting From the Underhook 	1:01:28

<br>

#### 8 Guard Dilemmas

- 1. The Push/Pull Dilemma 	0
- 2. Under/Over Dilemma 	17:13
- 3. Contraction/Extension Dilemma 	23:09
- 4. Lower Body/Position Dilemma 	29:00
- 5. Upper Body/Lower Body Dilemma 	36:08
- Guard Play: Putting it All Together 	42:31

<br>

</details>

<details>

<summary>3.5 Half Guard</summary> 

#### 1 Insights

- Introduction 	0
- Understanding New Wave Guard Play: 3 Key Insights 	6:39
- The 3 Reasons Why You Must Have a Strong Half Guard - Half guard is an excellent means of direct attack - From Bottom Position That Confers Some Advantages Over Conventional Guards 	35:54
- Half Guard Offers the Possiblity of Greater Connection So You Can Impose Bottom Position 	37:38
- Half Guard Allows Stronger Kuzushi Potential Because You Are Underneath Your Opponent's Center of Gravity 	41:51
- Half Guard is a True 3 Directional Guard 	44:46
- Half Guard is Halfway to Your Opponent's Back and Halfway to a Takedown 	48:58
- Half Guard is Well Adapted to a Transition to Mixed Martial Arts 	51:24
- Half Guard is Closely Related to the Skill of Pin Escapes 	54:31
- Half Guard Addresses the Fundamental Reality of Guard Play 	57:48
- Understanding Half Guard Position: The Fundamental Paradox of Half Guard 	1:01:32
- Resolving the Paradox: The Law of Outside Shoulder and Hip 	1:03:25

<br>

#### 2 Fundamentals

- The Path to Heaven: Making Half Guard Work for You - Knee Shield 	0
- Half Elbow Escape 	6:37
- Arm Frames 	12:44
- Kuzushi (Off Balancing) From Half Guard 	23:24
- Half Scissor Kuzushi 	26:10
- Half Butterfly Bump Kuzushi 	28:18
- Scoop Half Kuzushi 	30:13
- Scorpion Elevator Kuzushi 	35:31
- Half Butterfly Elevator Kuzushi 	40:02
- Knee Lever Kuzushi 	43:02
- Tight Waist Tilt Kuzushi 	46:33
- Windmill Kuzushi 	48:48

#### 3 Tight Waist

- Tight Waist Series - Getting to the Position 	0
- The Lower Leg Shift 	4:53
- The Tight Waist Roll Through 	11:05
- Ankle Pick 	21:27
- Pass Off Ankle Pick 	25:43
- Duck Under 	29:16
- Half Guard Body Lock - Body Lock Suck Back 	34:28
- Coming Up To Your Feet With a Body Lock 	41:00
- Outside Knee Pick 	1:00:04
- Inside Scoop Sweep (Navy) 	1:04:02
- Limp Arm 	1:11:21
- Cross Body Ride 	1:20:41
- Kosoto Circle 	1:28:40

<br>

#### 4 Whizzer

- Tight Waist VS Whizzer Series - Understanding the Whizzer 	0
- The Circle Body Lock 	13:53
- Uki Goshi 	16:38
- Far Side Walk 	19:01
- Half Nelson Pin 	23:02
- Outside Scoop 	26:20
- Single Leg Series - The Differences Between Single Leg and Tight Waist 	29:49
- Understanding a Back Side Single Leg Finish 	36:37
- Back Side Single Leg From Half Guard 	43:56
- Back Side Single Leg Limp Arm 	49:03
- Forward Heist Mechanics 	51:53
- Front Side Single Leg Finishes From Half Guard 	1:00:06
- Half Guard Single Leg Coming Up To Your Feet 	1:12:11
- Hiza Gatame Series 	1:20:35
- The Rolling Hiza Gatame 	1:39:24
- Lower Leg Shift and Hiza Gatame 	1:42:20

<br>

#### 5 Knee Lever

- The Knee Lever Series - Mechanics 	0
- Knee Lever: Head Position 	4:23
- 2 on 1 Wrist Knee Lever 	8:23
- Knee Lever 1 on 1 Grips 	12:20
- 1 on 1 Scoop 	15:08
- The Knee Lever Creates Scrambles That You Always Win 	17:39
- Knee Lever with a Lat Post 	23:28
- Putting Your Opponent's Knee on the Mat 	30:16
- Elbow Lever 	33:18
- Forward Heist to Single Leg From Failed Knee Lever 	36:08
- Arm Drag From Failed Knee Lever 	41:12
- 2 on 1 Elbow Post Knee Lever to the Back 	44:29
- Overhook Knee Levers - Overhook Sweep 	49:36
- Knee Lever as Entry to Tight Waist 	55:55
- Knee Lever to Butterfly Hook 	1:00:53

<br>

#### 6 Elbow Escape

- The Elbow Escape Series - Mechanics of Elbow Escape 	0
- Special Study: Ude Gatame 	16:03
- Mechanics 	19:28
- Attacking Both Sides with Ude Gatame 	31:05
- The Relationship Between Ude Gatame and the Shoulder Crunch Sumi Gaeshi 	35:59
- Ude Gatame to Triangle 	40:24
- Ude Gatame Into Kimura 	45:43
- Ude Gatame to Juji Gatame 	50:34
- Ude Gatame Combined with Forward Shift 	58:29
- Scoop Scorpion Series 	1:08:06
- Scorpion Leg Pass 	1:20:48
- Scorpion Walk Around 	1:25:25
- Scorpion Hip Heist 	1:32:52

<br>

#### 7 Entries

- Knee Shield Half Guard Entries - The Scoop Elevator 	0
- Different Variations of Attack 	13:25
- Attacking the Far Leg 	18:48
- Controlling Movement From the Scoop Grip 	23:05
- Spinning Around The Leg 	29:34
- Kani Basami 	40:07
- Butterfly Insertion 	50:33
- Ankle Pick Heel Hook Series 	57:20
- Ankle Pick to Irimi Ashi Garami 	59:44
- Ankle Pick Kani Basami 	1:06:28
- Half Butterfly Guard Series - Outside Elevator 	1:13:53
- Half Butterfly Guard Into Single Leg Cross Ashi Garami 	1:25:50

<br>

#### 8 Deep Half

- Deep Half Guard Into Leg Locks - Inside Sankaku 	0
- 50/50 From Deep Half Guard 	7:10
- Knee Lever Leg Lock Entries - Knee Lever and Outside Spin (Best Entry For Less Athletic/Older Athletes) 	12:51
- Knee Lever into Leg Locks from Disadvantageous Positions 	25:50
- Leg Locks From Half Guard - Why Leg Locks Should be the Centerpiece of Your Half Guard Game 	36:28
- Countering Half Guard Sit-out Situation - Thumb Post 	41:30
- Double Knee Lever 	50:38
- Butterfly Hook Counter 	58:20
- Reverse Underhook Counter 	1:10:04

<br>

</details>

<details>

<summary>3.6 Closed Guard</summary>

#### 1 Insights

- Introduction 	0
- Advantages and Disadvantages of Closed Guard 	3:26
- The Biggest Theme of this Video: The Battle of Closed Guard is Always a Battle for Posture 	20:16
- How to Win the Battle for Posture & 4 Infallible Signs that You Are Winning 	25:03
- The Side Scissor Series - Mechanics of the Side Scissor 	48:38
- Getting to a Side Scissor from Closed Guard - Elbow Post Method 	59:24
- Arm Drag Method 	1:11:22
- The Underhook Method 	1:19:12
- The Misdirectional Method 	1:25:09

<br>

#### 2 Side Scissor

- The Crucial Tactical Elements of the Side Scissor Position - Battle for Elbow, Height & Angle 	0
- The Side Scissor Trilemma 	8:36
- The Best Moves From Side Scissor Position When You Win The Battle for Height - Taking the Back 	15:25
- The Wrist Sweep 	22:05
- The Elbow Sweep 	26:37
- Elbow Sweep: Part 2 	29:28
- Armbar: Juji Gatame 	34:16
- The Rear Triangle (Ushiro Sankaku) 	38:57
- Best Moves from Side Scissor when you lose the battle for height - The Flower Sweep 	45:19
- The Pendulum Sweep 	50:43
- Lat Armbar (Lat Juji Gatame) 	57:11
- Ude Gatame 	1:03:27
- Shoulder Crunch Sumi Gaeshi 	1:12:48

<br>

#### 3 Clamp Series

- Losing the Battle for the Elbow: The Clamp Series - The Clamp 	0
- Triangle 	10:11
- Inverted Arm Bar Juji Gatame 	17:40
- Trimura 	21:19
- Omoplata Roll Over 	26:42
- When the Clamp Fails: Lat & Wrist Triangle 	33:18
- Side Clamp vs Vertical Clamp 	39:29
- Deepening Your Understanding of the Side Scissor 	47:40

<br>

#### 4 Double Underhook

- The Double Underhook Series 	0
- Getting to Double Underhooks - Elbow Post Method 	4:01
- Wrist Control Method 	7:05
- Flower Method 	9:14
- Collar & Scoop Method 	11:18
- The Single Most Important Move From Double Underhooks: The Slide By 	13:06
- Troubleshooting the Slide By 	22:38
- Special Study: How Short Athletes Can Use Closed Guard to Advantage: The Two Stage Philosophy of Attack from Closed Guard 	32:37
- Hiza Guruma 	39:49
- Sumi Gaeshi 	44:45
- Bear Hug Sweep 	47:42
- When the Bear Hug Fails 	52:59
- Convert to Half Guard 	56:08
- Double Overhooks to Sumi Gaeshi 	1:04:13

<br>

#### 5 High Cross Series

- The High Cross Series - Getting to the High Cross 	0
- High Cross Juji Gatame Armbar 	6:29
- High Cross to Triangle (Hantai Sankaku) 	10:48
- High Cross to Trimura 	21:22
- High Cross Omoplata to Heel Hook 	25:04
- High Cross: Putting it all together 	33:12
- Side Sit Up Series - Getting to the Side Sit Up 	38:11
- My Favorite Entry to The Side Sit Up 	45:42
- Two Arm Positions for the Side Sit Up 	53:06
- The Most Important Move From the Side Sit Up: the Hip Sweep 	56:03
- Two Directions of Force for the Hip Sweep 	1:02:54
- Trapping Your Opponents Arm 	1:07:17
- Working from a Failed Hip Sweep 	1:13:33
- Hip Sweeping from the Elbow 	1:18:00
- The Hip Sweep: Putting it All Together 	1:21:00

<br>

#### 6 Sweeps

- Scoop Handstand Series - Mechanics of the Scoop Handstand Sweep 	0
- Attacks From Scoop Sweep - Hip Sweep 	8:05
- Scoop Sweep to Triangle 	13:41
- Scoop Sweep to Arm Bar (Juji Gatame) 	17:44
- Tying Together the Scoop Sweep with the Rest of Your Closed Guard Game - Side Scissor 	21:57
- High Cross 	28:23
- Attacking from closed guard without the benefit of angle or height - The top lock - The top lock arm bar juji gatame 	31:24
- Overtie Top Lock 	41:52
- Top Lock on a Standing Opponent 	47:04
- Dealing with Opponents Pulling Out of Top Lock 	54:38
- Figure 4 Top Lock 	1:02:38

<br>

#### 7 Triangles

- Trap Triangle - Collar and Wrist Dilemma 	0
- Transition from Trap Triangle to Figure 4 Triangle 	8:01
- Trap Triangle For Short Legged Athletes 	13:09
- Elbow Overhook - Kneeling Opponent 	19:37
- Standing Opponent 	27:27
- Losing the Battle for Posture But Winning The Game - The Golden Rule of the Battle for Posture 	36:06
- How to Transition from Closed Guard to Open Guard Safely - Go First 	42:03
- Stopping Leg Locks with Inside Position 	49:45
- Stopping Guard Passes with Entanglements 	53:24
- Your Minimum Goal when Your Opponent Opens Your Guard 	57:59

<br>

#### 8 Leg Attacks

- Attacking the Legs from Closed Guard - Spin Around Legs Into Heel Hooks 	0
- Transition to X-Guard and Heel Hook 	6:49
- Transition to Ashi Garami & Heel Hook 	9:44
- Sweeps: The Magic Formula for Strong Knockdowns from Guard Position 	16:10
- Overview: Creating a Complete System for Your Closed Guard Game - Step 1: Break Opponents Posture 	30:50
- Step 2: Work Towards a Dominant Position 	36:08
- Step 3: If The Postural Game Fails Attack the Legs 	49:34
- Alternative Strategy for Shorter Athletes: The Two Stage Strategy 	54:50

<br>

</details>

<details>

<summary>3.7 Side Pin Attacks</summary>

#### 1 Insights

- Introduction 	0
- Building a Devestating Side Control System - Advantages and Disadvantages of the Side Control Position 	3:50
- The Central Problem of Side Control and it's Solution 	13:39
- The Main Message of This Video 	20:13
- The Relationship Between Side Control and the Mount Position 	27:48
- 2 Philosophies of Moving From Side to Mount - 1st Philosophy: Opponent Denies the Mount - Crossface Knee Slide Method 	34:19
- Step Over Method 	42:26
- Double Underhooks Method 	46:30
- Hip Step Method 	51:36
- 2nd Philosophy: Opponent Denies Submission - Speed Based Method 	59:25
- Tripod Method 	1:07:41
- Side Control Different Variations - Arm Configurations 	1:14:59
- Leg Configurations 	1:24:15

<br>

#### 2 Escapes

- The 2 Most Common Escapes to Side Control and How to Negate Them - Countering the Elbow Escape 	0
- Negating a Knee Escape 	11:50
- The 4 Step Side Control System - Step 1: Elbow Penetration - 1st Option: Underhook - The 10 Finger Method 	27:57
- Spontaneous Pommel Method 	33:28
- Fake American Method 	39:53
- 2nd Option: Reverse Overhook - The Underclasp Method 	47:53
- The Walk Around Method 	54:41
- The Side to Side method 	1:00:54
- Step 2: Elbow Separation - Reverse Overhook/Rotational Method 	1:06:52
- The Underhook Forearm Lever Method 	1:24:20
- Special Study: Developing Devastating Pinning Pressure by Attacking Your Oponent's Breathing 	1:28:54

<br>

#### 3 Control Positions

- Step 3: Control Positions: The 4 Major Control Positions of the Side System - Seated Head and Arm 	0
- Dorsal Pin 	15:15
- Side Crucifix 	29:28
- The Elbow Trap - Hip Entry 	38:08
- Quarter Nelson Entry 	48:08

<br>

#### 4 Submissions

- Step 4: Submissions - The 4 Major Submission of the Side Control System 	0
- Seated Head and Arm Series 	3:53
- Seated Head and Arm to Rear Strangle 	27:59
- Seated Head and Arm to Mount 	42:09
- Seated Head and Arm to Arm Bar 	53:18

<br>

#### 5 Kimura

- Dorsal Pin Series - Kimura (Hands) 	0
- Kimura (Legs) 	15:21
- Yoko Sankaku (Side Triangle) 	24:30
- Transition to Juji Gatame (Arm Bar) 	42:47
- Transition to the Back 	53:16

<br>

#### 6 Side Crucifix

- Side Crucifix Series - Scissor Side Crucifix - Entries 	0
- Reverse Triangle 	6:43
- Reverse Triangle Supine Finish 	14:16
- Reverse Triangle Into Side Traingle 	20:35
- Reverse Triangle Side Triangle Kimura 	26:17
- Reverse Triangle Into American Lock 	32:56

<br>

#### 7 Shin Pin

- Shin Pin Side Crucifix - Entering and Controlling the Shin Pin 	0
- A Deep Point About Posture and Pinning 	4:15
- Shin Pin to Arm Bar (Juji Gatame) 	9:43
- Shin Pin to Kimura 	23:13
- Shoulder Drop Triangle 	31:30
- Shoulder Drop To Ude Gatame 	42:06
- Shoulder Drop Trimura 	47:08
- Shin Pin to Mounted Head and Arm Triangle 	55:39
- Shin Pin to Mounted Head and Arm Supine Finish 	1:06:15
- Mounted Head and Arm Into Arm Locks 	1:13:24

<br>

#### 8 Elbow Trap

- The Elbow Trap Series - Step Over Triangle 	0
- Knee Slide Triangle 	9:23
- 3/4 Juji Gatame 	24:05
- Trimura 	29:20
- When the System Fails: Leg Locks From Side Control - Pins and Leg Locks: My Philosophy 	36:30
- Outside Entry Into Legs 	39:14
- Step Through Entry 	58:23

<br>

</details>

<details>

<summary>3.8 Guard Passing</summary>

#### 1 

- Introduction to Passing the Guard 	0 - 3:40
- Philosophy of Opening a Closed Guard 	3:40 - 12:11
- Top Position: Opening a closed Guard 	12:11 - 30:31
- Opening Closed Guard 2 	30:31 - 37:41
- opening closed guard 3 	37:41 - 44:26
- Opening Closed guard 4 	44:26 - 53:02
- opening closed guard 5 	53:02 - 56:05
- opening closed guard 6 	56:05 - 1:00:39
- opening closed guard 7 	1:00:39 - 1:05:40
- opening closed guard 8 	1:05:40 - 1:07:20
- opening closed guard 9 	1:07:20 - 1:11:48
- opening closed guard 10 	1:11:48 - 1:19:03
- opening closed guard 11 	1:19:03 - 1:29:41

<br>

#### 2

- Overview of Standard Method Of Opening Closed Guard 	0 - 8:29
- Opening Closed guard: Knee Post Method 	8:29 - 16:12
- Knee Post Method 2 	16:12 - 21:40
- Knee Post Method 3 	21:40 - 23:53
- knee post method 4 	23;53 - 27:33
- knee post method 5 	27:33 - 29:30
- knee post method 6 	29:30 - 34:07
- Opening Closed Guard / Lifting Method 	34:07 - 41:16
- Overview of 3 methods 	41:16 - 47:10
- Problem of Procrastination 	47:10 - 52:50
- the Most Important Message Of This Video: The 5 Steps Of Guard Passing - Step 1 	52:50 - 1:00:13
- Step 2 	1:00:13 - 1:06:25
- Step 3 	1:06:25 - 1:10:52
- Step 4 	1:10:52 - 1:17:44
- Step 5 	1:17:44 - 1:27:10
- Negate Advantage Completion Guard Passing Model - NAC Open Guard Passing Method 	1:27:10 - 1:31:35
- Split Squat as Ultimate Staging Platform 	1:31:35 - 1:36:29
- Single Biggest Insight into Guard Passing: Side to Side Pressure 	1:36:29 - 1:43:50
- Mechanical and Tactical Advantage 	1:43:50 - 1:53:22

<br>

#### 3

- First Requirement Of A Guard Passing Program: Technical Proficiency With The Most High Percentage Methods 	0 - 1:34
- Double Under Pass 	1:34 - 26:12
- Over Under Pass 	26:12 - 46:17
- Knee Cut Pass 	46:17 - 1:26:06

<br>

#### 4

- Toreando Pass 	0 - 1:05:33

<br>

#### 5

- Leg Drag 	0 - 35;06
- Long Step Guard Pass 	35:06 - 1:09:15
- The Smash Pass 	1:09:15 - 1:18:14

<br>

#### 6

- Second Requirement Of A Guard Passing Program: The Ability To Break Connections And Shut Down a Dangerous Guard 	0 - 7:26
- Cross Collar Cuff And Bicep Guard 	7:26 - 28:36
- De La Riva Guard 	28:36 - 42:37
- Reverse De la Riva 	42:37 - 47:13
- Lasso Guard 	47:13 - 52:56
- Spider And Lasso Guard 	52:56 - 1:00:18
- Double Spider Guard 	1:00:18 - 1:07:27
- Emergency Measures 	1:07:27 - 1:14:13
- Ashi Garami 	1:14:13 - 1:21:37
- The 3rd Requirement Of A Guard Passing Program: The Ability To Maintain Top Position 	1:21:37 - 1:23:40
- The Ancient Law Of Push When Pulled And Pull When Pushed 	1:23:40 - 1:31:26
- Recovering From A Fall 	1:31:26 - 1:44:38

<br>

#### 7

- The Fourth Requirement of a Guard Passing Program: Using Staging Positions To Pass A Dangerous Guard - 1) The Split Squat Position (Headquarters) 	0 - 12:47
- Second Great Virtue of the Split Squat Position: hip Control and Head Control 	12:47 - 16:30
- Negating Both Feet From Split Squat 	16:30 - 21:45
- 2) The Knee Drop 	21:45 - 26:13
- 3) Double Knee Position 	26:13 - 29:56
- 4) Outside Advantage Position 	29:56 - 39:07
- Putting Skills & Theory Into Combative Context 	39:07 - 51:47
- Flattening an Opponent 	51:47 - 1:00:45
- Negating An Opponent’s Guard 	1:00:45 - 1:07:46
- The Fifth Requirement of a guard passing program: finding advantage within a neutral position - creating advantage while passing 	1:07:46 - 1:26:01

<br>

#### 8

- Guard Passing From The Shoulder Line 	0 - 8:35
- sixth requirement of a guard passing program: breaking through defensive frames and getting to your pin - Beating Frames - Distance 	8:35 - 13:48
- Angle Change 	13:48 - 16:04
- Direction 	16:04 - 20:39
- Directly Attacking The Frames 	20:39 - 24:11
- Side To Side Pressure 	24:11 - 29:35
- Beating Inversion 	29:35 - 33:40
- Turning In To Turtle 	33:40 - 38:24
- Turning Out To Turtle 	38:24 - 42:33
- Propping 	42:33 - 46:20

<br>

</details>

<details>

<summary>3.9 Mounted Pin Attacks</summary>

##### 1 Insights

Introduction 	0
The Central Problem of Pinning in Submission Grappling - The 5 Pins of Jiu-Jitsu 	6:59
The Mechanics of Pinning 	19:56
The 2 Big Themes of New Wave Pinning 	29:13
Converting Pins to Submissions - The 2 Keys of Conversion 	40:38
Elbow Penetration 	46:04
My Favorite Philosophy of Pinning: Legs vs Upper Body 	51:18
The Mounted Position - Reflections on the Mounted Position for Grappling 	56:24
The Single Greatest Advantage of the Mounted Position Over Other Pins 	1:02:58
The First Skill of the Mounted Position: Getting Mounted - The Central Problems of the Mounted Position 	1:11:50

##### 2 Getting Mounted

Getting Mounted - The Knee Drive Method 	0
Knee Drive Method Part 2 	17:17
Stepover Method 	25:15
Double Underhooks Method 	38:18
The Second Skill of Getting Mounted: Mount Retention - Multiple mounts Outperform Any Single Mount 	48:01
The Crucial Relationship Between Half Guard Passing and Mounted Position 	1:21:21
The Third Skill of Mounted Position: Winning the Batte of the Elbow - In Jiu Jitsu: Extension is Weakness 	1:37:56

##### 3 4x4 Mount System

The 4x4 Mount System: A 4 Step Program to Win From Mounted Position - Step 1: Getting to an Underhook - The Cross Wrist method 	0
The Half Hand Grip 	8:33
Establish Underhook From Half Guard 	18:07
Establish the Underhook as You Mount From Side Control 	23:18
Give a Little to Gain a Lot 	29:52
Penetrate Opposite Elbow 	38:35
Midirectional Underhook 	43:44
Wrist vs Elbow Method 	46:59
Step 2. Elbow Separation Over the Shoulder Line: The Ratchet Method 	55:11
Step 3. Establishing the 4 Control Grips of the Mounted Position - The Single Chest Wrap 	1:09:48
The Far Trap Underhook 	1:23:28
Double Chest Wrap 	1:28:41
Arm Wrap/Figure 4 Arm Wrap 	1:35:17

##### 4 4x4 Mount System

Step 4: The 4 Submissions of the 4x4 System - Kata Gatame (Arm Triangle) - Begin With Kata Gatame 	0
Entering Kata Gatame From the Single Chest Wrap 	3:19
The Mechanics of Kata gatame 	10:38
Improving Your Kata Gatame Mechanics With the Pump Flex (Lifting the Head to Attack) 	29:51
Bulldozing Through Your opponent's Defenses 	39:17
Recovery From a Failed Kata Gatame 	50:21
Timing the Dismount From Mount to Side 	58:27

##### 5 Rear Strangle

Rear Strangle - Far Trap Underhook Transition to Rear Mount 	0
The Importance of Lifting the Head When Tranisitioning to Rear Mount 	12:06
Lower Body Controls From rear Mount 	17:47
Upper Body Controls 	28:28
The Number One Back Submission Sequence 	37:15
Strangling With the Legs From the Back 	52:16
Front Strangle/Rear Strangle Dilemma 	1:07:23
Juji Gatame (Arm Bar) - The Double Arm Wrap is the Best Setup for Mounted Juji Gatame 	1:17:00
Lifting the Head for Better Entries 	1:30:08
Special Study of Juji Gatame 	1:37:05
The Right Right Right Rule 	1:50:34

##### 6 Magnum Finishes

Magnum Finishes - Pushing Finish 	0
Lat Grip 	4:58
Multiple Juji Gatame Configurations 	8:07
Juji Through 360 Degrees 	26:56
Mounted Triangle - General Reflections on the Mounted Triangle 	37:06
The Single Chest Wrap is the Best Setup for Mounted Triangles 	43:15
The Great Dichotomy in Mounted Triangle Attacks: Stepover Methods vs Knee Slide Methods 	50:48

##### 7 Mounted Triangle

The 4 Keys to a Strong Mounted Triangle - 1. Hand Assist 	0
2. Forward Bias 	4:30
3. Get Your Opponent's Head Off the Mat - Twice 	7:46
4. The Rules of Risk 	10:37
Push Down Stepover Method 	21:49
Push Out Stepover Method 	35:15
Knee Slide Shoulder Drop Method 	46:12
Pivot Knee Slide Method (My Favorite) 	59:02
Mounted Head and Arm 	1:09:58
Mounted Head and Arm Part 2 	1:18:32
Placido's Revenge 	1:27:21
Arm Wrap to Mounted Triangle 	1:31:33
Lat 3/4 Juji Gatame (Arm Bar) 	1:38:10

##### 8 Overcoming Failure

Overcoming Failure in the 4 Step System - First Failure: You Can't Get the Elbow Over the Shoulder Line 	0
Second Form of Failure: You Can't Get an Underhook 	11:45
Final Overview of the 4x4 Mount System 	22:16
The Deepest Theme of This Video Which Will Change Your Outlook on Grappling Pins 	34:21




Men have Advantages

Why no women joining men's leagues??



The doctors in gender clinics transition virtually every child who walks through their doors

This isn't a medical industry. It's a cult.



Newly obtained documents confirm yet again Fauci lied about COVID. Fauci’s NIH lab was a partner with Wuhan on a proposal to engineer a highly transmissible coronavirus in 2018. But he wasn’t alone, 15 government agencies knew about it and said nothing. Americans deserve answers.

https://www.hsgac.senate.gov/media/reps/dr-paul-sends-letters-to-fifteen-federal-agencies-after-discovering-their-knowledge-of-risky-defuse-project/


|||
|-|-|
|National Institutes of Health|[Letter to the Director of the National Institutes of Health](https://www.hsgac.senate.gov/wp-content/uploads/2024.04.09_SRP-letter-to-NIH.pdf)|
|National Institute of Allergy and Infectious Diseases|[Letter to the Director of the National Institute of Allergy and Infectious Diseases](https://www.hsgac.senate.gov/wp-content/uploads/2024.04.09_SRP-Letter-to-NIAID.pdf)|
|Department of Health and Human Services|[Letter to the Secretary of the Department of Health and Human Services](https://www.hsgac.senate.gov/wp-content/uploads/2024.04.09_SRP-Letter-to-HHS.pdf)|
|Defense Advanced Research Projects Agency|[Letter to the Director of the Defense Advanced Research Projects Agency](https://www.hsgac.senate.gov/wp-content/uploads/2024.04.09_SRP-Letter-to-DARPA_Final.pdf)|
|U.S. Agency for International Development (USAID)|[Letter to the Administrator of the U.S. Agency for International Development](https://www.hsgac.senate.gov/wp-content/uploads/2024.04.09_SRP-Letter-to-USAID.pdf)|
|U.S. Department of Agriculture (USDA)|[Letter to the Secretary of the U.S. Department of Agriculture](https://www.hsgac.senate.gov/wp-content/uploads/2024.04.09_SRP-Letter-to-USDA.pdf)|
|U.S. Department of Homeland Security (DHS)|[Letter to the Secretary of the U.S. Department of Homeland Security](https://www.hsgac.senate.gov/wp-content/uploads/2024.04.09_SRP-Letter-to-DHS.pdf)|
|U.S Navy (USN)|[Letter to the Secretary of the Navy](https://www.hsgac.senate.gov/wp-content/uploads/2024.04.09_SRP-Letter-to-Navy.pdf)|
|Centers for Disease Control and Prevention (CDC)|[Letter to the Director of the Centers for Disease Control and Prevention](https://www.hsgac.senate.gov/wp-content/uploads/2024.04.09_SRP-Letter-to-CDC.pdf)|
|U.S Army|[Letter to the Secretary of the Army](https://www.hsgac.senate.gov/wp-content/uploads/2024.04.09_SRP-Letter-to-Army.pdf)|
|Assistant Secretary for Preparedness and Response (ASPR)|[Letter to the Assistant Secretary for Preparedness and Response](https://www.hsgac.senate.gov/wp-content/uploads/2024.04.09_SRP-Letter-to-ASPR.pdf)|
|U.S. Army Medical Research Institute of Infectious Diseases (USAMRIID)|[Letter to the Commander of the U.S. Army Medical Research Institute of Infectious Diseases](https://www.hsgac.senate.gov/wp-content/uploads/2024.04.09_SRP-Letter-to-USAMRIID.pdf)|
|National Institute of Food and Agriculture (NIFA)|[Letter to the Director of the National Institute of Food and Agriculture](https://www.hsgac.senate.gov/wp-content/uploads/2024.04.09_SRP-Letter-to-NIFA2.pdf)|
|Military Operational Medicine Research Program (MOMRP)|[Letter to the Director of the Military Operational Medicine Research Program](https://www.hsgac.senate.gov/wp-content/uploads/2024.04.09_SRP-Letter-to-MOMRP.pdf)|
|Defense Health Agency (DHA)|[Letter to the Director of the Defense Health Agency](https://www.hsgac.senate.gov/wp-content/uploads/2024.04.09_SRP-Letter-to-DHA2.pdf)|

<br>

### 5.1 FEET TO FLOOR 1

##### 1 Takedown Selection

4:20 	The Criteria for Takedown Selection for Jiu Jitsu
11:35 	Creating an Overall Game plan for Jiu Jitsu Students in the Standing Position
40:15:00 	The First Precursor Skill of the Standing Position: Stance
1:00:35 	The Most Important Insight You Can Gain in the Standing Position: Left & Right Stances
1:16:09 	Why The Distinction Between Kenka-Yotsu & Ai-Yotsu Is So Important For Your Standing Game
1:28:44 	The Second Precursor Skill of Standing Position: Fighting For A Grip - Understanding Power Hand and Control Hand
1:40:55 	Gripping for a Purpose
1:49:16 	The Reverse Power Hand
1:56:03 	Turning the Most Common Grip Scenario to Your Advantage
2:09:06 	Grip Fighting in Kenka-Yotsu to Throw An Ai-Yotsu
2:17:42 	Dealing with Failure to Get Inside Position in Kenka-Yotsu Position

##### 2 Breaking Grip

17:27 	The Second Skill of Gripping: Breaking a Grip
39:18:00 	Grip Fighting Strategy - Ai-Yotsu Strategy - Strategy 1
1:15:45 	Strategy 2
1:20:44 	Strategy 3
1:28:25 	Strategy 4
1:33:36 	Kenka-Yotsu Strategy - Strategy 1
1:40:27 	Strategy 2
1:44 	Strategy 3
1:50 	Strategy 4

##### 3 Grip and Move

14:31 	The Grip and Move Principle
19:52 	The Fourth Precursor Skill of Standing Position: Kuzushi/Breaking Stance
24:30:00 	The Four Golden Opportunities of Kuzushi
36:47:00 	Fifth Precursor Skill Of Standing: Position
42:18:00 	The 5 Minimum Requirements As You Begin Your Journey In The Standing Position - #1 Prerequisite Skills
56:53:00 	#2 Front Takedowns
1:06:25 	#3 Rear Takedowns
1:09:09 	#4 Pulling Guard
1:11:22 	#5 Countering a Guard Pull

##### 4 Collar Drag

15:37 	Power Hand and Control Hand for Collar Drag
27:46:00 	The Leg Action of the Collar Drag: Uki Waza or Single Slide
32:40:00 	Heavy Hand on the Cross Lapel
37:27:00 	High Head
43:53:00 	The Forward Rock
47:08:00 	Body Position & Angle for the Collar Drag
51:47:00 	The 3 Targets of the Collar Drag
57:12:00 	The Greatest Disadvantage of the Collar Drag - Predictability
1:11:32 	Attacking Off The Grip
1:14:50 	Working From Grip Fight - Straight Cuff
1:20:57 	Movement As a Distraction
1:24:53 	Collar Drag from Kenka-Yotsu: Cuff Method
1:26:46 	Collar Drag from Kenka-Yotsu: Cross Grip
1:30:03 	Combining the Collar Drag with Other Attacks - Half Sasae into Collar Drag
1:33:07 	Seoi Snap into Collar Drag
1:36:06 	Two on One Cross Collar Snap Into Collar Drag
1:39:32 	Failed Cross Osoto-Gari into Collar Drag
1:47:12 	Failed Kouchi-Gari into Collar Drag
1:52:34 	Failed Ankle Pick into Collar Drag
1:57:06 	Double Drag
2:00:26 	Working the Interface Between Feet & Floor
2:03:39 	A Big Advantage of the Collar Drag Over Other Takedown Methods
2:08:26 	The Great Relationship Between the Collar Drag, the Single Leg, & The Back

##### 5 Ankle Picks

5:35 	Cuff Ankle Picks
6:53 	Inside Ankle Pick
8:55 	Shoulder Grip Cross Ankle Pick
10:44 	Combining Outside Pick & Inside Pick
12:24 	Kibisu Gaeshi Floor Post
14:32 	Why I strongly Favor The Ankle Pick For Jiu Jitsu - Overlap With Ground Grappling
22:08 	Low Risk/High Reward
26:49:00 	Ankle Pick Is Very Well Suited To Common Jiu-Jitsu Stance
32:14:00 	Ankle Pick Combines Extremely Well With Guard Pulls
36:56:00 	Ankle Pick Utilizes The Long End Of The Lever
40:03:00 	What Do We Need To Develop A High Level Ankle Pick - There Are 2 Main Kinds Of Ankle Picks
43:50:00 	Understanding Shot Hand/Shot Leg Correlation
47:46:00 	Ability To Get Our Opponent To Take A Step
53:14:00 	Ability To Get Opponent’s Head Over The Ankle You Are Attacking
8:06:00 	Ability To Keep Opponent’s Head Same Height As Yours
1:02:38 	Ability To Go From One Ankle To Another
1:05:49 	Ability To Use Shot Hand In A Linear Fashion
1:09:07 	Ability To Create A Drive Leg And Finish With A Knee Slide
1:11:27 	Ankle Picking From Kenka-Yotsu
1:19:01 	The Relationship Between Snap-Downs And Ankle Picks
1:28:08 	Double Ankle Pick
1:34:47 	High Finishes To An Ankle Pick
1:38:36 	Troubleshooting The Ankle Pick - Dealing With A Stiff Arm
1:46:21 	Opponent Controls Our Shot Hand
1:58:09 	Opponent Plays Upright Stance
2:05:22 	The Special Relationship Between Ankle Picking And Knee Picking
2:12:36 	Double Leg Takedown (Morote Gari) from Ai-Yotsu - Open Position
2:19:22 	Straight Cuff Flank
2:23:01 	Cross Cuff Grip
2:24:28 	Cross Grip
2:26:10 	Armdrag
2:27:31 	Using a Grip Fight to Your Advantage
2:29:01 	Sleeve Lapel
2:31:16 	Double Leg Takedown (Morote Gari) from Kenka-Yotsu - Cross Grip
2:34:51 	Grip Break Method
2:36:46 	Lapel Grip
2:39:09 	Single Leg Takedown - The difference between single legs Gi & No-Gi
2:41:46 	Ai-Yotsu - Lapel Single Leg
2:47:20 	Dump
2:50:45 	Sleeve Cuff Single
2:52:44 	Double Sleeves
2:54:43 	Kenka-Yotsu - One Handed Gripping
2:57:16 	Knee Pick Kuchiki-Daoshi
2:59:46 	Kenka-Yotsu Lapel Grip
3:07:17 	Grip Fighting
3:09:50 	Kuchiki-Daoshi combined with Tani-O’toshi

##### 6 Snap Downs

6:19 	Two on One/Half Tai-Otoshi Kenka-Yotsu Situation
9:37 	Cross Grip Snap Down Ai-Yotsu
13:31 	Arm Trap Snap Down Kenka-Yotsu
16:01 	Ude Gaeshi - Standard Supine Version
18:59 	Standard Supine to an Arm Pin
28:53:00 	Ude Gaeshi Cross Grip
33:03:00 	Seoi Snap
35:41:00 	Straight Lapel Version
44:33:00 	Double Knee Drop vs Single Knee Drop
48:10:00 	Troubleshooting Seoi Snap
53:10:00 	Rear Takedowns
57:21:00 	The Special Relationship Between Front Takedowns and Rear Takedowns
1:04:13 	Controlling The Rear Body Lock
1:08:43 	Kazushi From The Standing Rear Body Lock
1:18:18 	Forward Takedowns From The Rear - Reverse Kouchi-Gari
1:24:02 	Reverse Kosoto
1:34:27 	Reverse De Ashi Harai
1:43:43 	Reverse Ankle Pick
1:52:07 	Rear Takedowns Backwards Direction - Tani-Otoshi
2:01:30 	Reverse Tai Otoshi
2:14:58 	Reverse Sumi-Gaeshi

##### 7 Takedown Defense

5:47 	The 3 Functions of Takedowns In Self Defense
10:32 	Not All Self Defense Scenarios Are The Same
26:57:00 	My Golden Rules For Self Defense Takedowns - #1 Don’t Go to Your Knees
29:13:00 	#2 Favor Takedowns That Make It Difficult For An Opponent To Strike You Before, During, And After A Takedown
32:40:00 	#3 Favor Low Amplitude Takedowns
38:37:00 	#4 Favor Takedowns That Result In Your Opponent Going Down To The Ground And You Remaining On Your Feet
44:05:00 	#5 Have A Couple Of High Amplitude Takedowns That Involve Crashing Your Opponent Into The Ground With Velocity
48:16:00 	#6 Favor Takedowns From Behind Your Opponent
51:36:00 	#7 Favor Takedowns that Don’t Rely on Clothing
54:44:00 	#8 Favor Takedowns Where You Remain On Two Feet
59:58:00 	#9 Favor Takedowns That Do Not Require Large Amounts of Space for Their Application
1:03:16 	The Three Best Takedowns For Self Defense Applications - High Single Leg Ankle Block And Classic Tai Otoshi

##### 8 Takedown Defense

4:45 	Phase 2: Capture the Leg
17:04:00 	Phase 3: Transfer to the High Leg
18:55:00 	Head Position
24:59:00 	The Instant Off Balance
28:26:00 	Application From the Back
31:19:00 	The Arm Drag as the Best Method of Getting Behind an Opponent
33:43:00 	Special Topic: The Impact of Clothing on Self Defense Takedowns - The Relevance of Hockey Fighting to Self Defense
41:25:00 	Neutral Attack Positon Screen Only Heading
46:12:00 	Kenka-Yotsu Neutral Grip
52:29:00 	Safety Grips Transition to Dominance
56:53:00 	Advantage Front Position /Advantage Back Position/ Advantage Blind
1:01:12 	Clothing, Takedowns & Self-Defense
1:06:35 	Special Topic: SVG Theory
1:17:00 	Understanding the Nuances of SVG
1:47:01 	Final Reflections on Takedowns for Self-Defense

### 4.2 FEET TO FLOOR 2

##### 1 Gripping

Introduction to Feet to Floor Volume 2 	0
A Simplified Theory Of Gripping 	0.2236111111
Small Ashi Waza - Kouchi Gari - The Mechanics Of Kouchi Gari - Sole Of The Foot 	23:15
The Ideal Arm Action For Kouchi 	27:33
Split The Legs And Pull 	31:49
Sweeping A Foot vs Blocking A Foot 	34:54
Committing Body Weight 	38:27
Back Step And Side Step For Kouchi Gari 	42:39
Creating The Best Opportunities For Kouchi Gari 	45:16
Kouchi Gari Ai-Yotsu Situation 	51:24
Kouchi Gari Kenka-Yotsu Situation 	55:01
Alternative Grips For Kouchi Gari 	58:42
Hand Assist Kouchi Gari 	1:03:40
The Immense Tactical Value Of Kouchi Gari 	1:07:39
Kosoto Gari - Mechanics 	1:11:41
Weighting And Unweighting The Foot 	1:16:38
The Hook And Hop Method 	1:21:13
Plant And Sweep Method 	1:25:19
Sticky Foot Method 	1:29:40
Hand Assist Method 	1:35:35
The Immense Tactical Value Of Kosoto Gari 	1:40:54

##### 2 Throwing

Sasae Tsuri Komi Ashi - The Concept Of Sasae 	0
Mechanics Of Sasae - Rotation With Arms 	4:04
Hips Forward/Head Back 	10:02
Center Step, Side Step, Cross Step 	12:07
Two Directions Of Throwing 	14:33
An Easy Way To Increase Your Throwing Power 	17:40
Look Where You Want Them To Land 	21:17
Hopping To Finish Sasae 	23:54
Sasae Tactics 	26:59
Grips For Sasae - Arms Length Gripping 	31:34
One Handed Gripping 	35:47
Half Trap Grip 	39:32
Waist Gripping 	44:50
The Great Value Of The Cross Step 	48:53
De Ashi Harai - The Basic Concept 	53:05
Mechanics Of De Ashi Harai 	1:01:19
Applications Of De Ashi Harai - Off The Grip 	1:08:12
The Pulling Man 	1:14:15
The Lateral Burst 	1:18:20
The Circular Pull 	1:24:16
Cross Cuff And Scapula 	1:27:02

##### 3 Picks

Big Ashi Waza - Ai-Yotsu Ouchi Gari 	0
Kenka-Yotsu Ouchi Gari 	13:10
Hand Assist Ouchi Gari - Cross Ankle Pick 	20:35
Knee Pick Ouchi 	23:38
Double Ouchi 	26:37
Cross Cuff Ouchi 	28:35
Osoto Gari - Classic Osoto Gari 	32:17
Weakening Our Opponent’s Defenses 	38:13
Hook And Hop 	45:26
Backstep Entry And Side Step Entry 	51:15
Head Position 	57:15
Knee Drop Osoto Gari 	1:01:46
Grips - Sleeve Lapel 	1:11:14
Cross Grip And Sleeve 	1:14:14
Scapula And Sleeve 	1:16:27
Ippon Seoi Grip 	1:24:18
Kenka-Yotsu Osoto Gari 	1:42:38
Osoto TvS Position 	1:50:39

##### 4 Tomoe Nage

Sutemi Waza - Tomoe Nage - Why Tomoe Nage Is The Ideal Takedown For Jiu Jitsu 	0
The Central Problem Of Tomoe Nage 	5:49
Tomoe Nage Comes In Two Forms 	12:25
The Mechanics Of Side (Yoko) Tomoe Nage 	17:06
Placement Of The Foot Of The Throwing Leg 	31:08
Creating The Hollow Body 	34:48
A Specialized Head Position For Yoko Tomoe Nage 	39:02
Footwork For Tomoe Nage - The Falling Step 	45:05
The Burst Step 	56:55
The Cross Step 	1:01:05
The Single Most Important Grip For Yoko Tomoe Nage 	1:05:17
The Special Case Of Kenka-Yotsu Side Tomoe Nage 	1:12:26

##### 5 Yoko Tomoe Nage

Alternate Grips For Yoko Tomoe Nage - Overwrap Grip 	0
Side Step Tomoe Nage 	10:54
Double Tricep Grip 	17:02
An Excellent Alternative Kenka-Yotsu Grip: Tricep And Cuff 	23:40
Cross Body Finishes For Tomoe Nage 	31:02
Turning Failure With Tomoe Nage Into Success 	37:18
Two Stage Tomoe Nage 	44:21

##### 6 Front Tomoe Nage

Front Tomoe Nage - Understanding The Mechanics Of Front Tomoe Nage 	0
The Sumi Gaeshi Hybrid 	12:24
Opening Elbows And Closing Elbows 	21:23
The Backwards Shuffle 	26:39
Tomoe Nage: Landing On Top For Jiu Jitsu 	30:37

##### 7 Sumi Gaeshi

Sumi Gaeshi - Why Sumi Gaeshi Is The BestThrow For Jiu Jitsu 	0
The 3 Major Forms Of Sumi Gaeshi - Side (Yoko) Sumi Gaeshi 	6:20
Front Sumi Gaeshi 	10:41
Landing On Top With Front Sumi Gaeshi 	21:50
Butterfly Sumi Gaeshi 	28:43
The Mechanics Of Sumi Gaeshi - Directionality For Perfect Throws 	37:29
Penetration 	42:31
Connection To The Floor With a Drive Leg 	46:15
Positioning Of Elevator Hook And Knee 	49:27
The Right Right Right Rule 	54:51
Sumi Gaeshi Is A Rotational Throw 	58:33

##### 8 Sumi Gaeshi Variations

The Most Important Variations Of Sumi Gaeshi - Arm Trap Sumi Gaeshi 	0
The Half Trap 	7:49
Over Back Sumi Gaeshi 	13:44
Tricep And Cuff Sumi Gaeshi 	20:57
The Single Biggest Problem With Sumi Gaeshi: Predictability 	24:56
The Special Relationship Between Uchi Mata And Sumi Gaeshi 	31:45
Uki Waza - Why Uki Waza Is An Excellent Throw For Jiu Jitsu 	37:14
Cross Cuff And Scapula Grip/Reverse Scapula Grip Uki Waza 	40:26
Arm Trap Uki Waza 	45:39
Half Trap Uki Waza 	50:25
Overhook And Pant Grip Uki Waza 	55:02
Tricep And Cuff Grip Uki Waza 	58:57
Overwrap Uki Waza 	1:05:00
Hybrid Uki Waza 	1:07:30

### 4.3 FEET TO FLOOR 3

##### 1 Knee Bar

Introduction 	0
Rolling Knee Bar 	15:03
Grips for Rolling Knee Bar - Front Belt Grip 	26:22
Lapel Grip 	32:56
Scapula Grip 	38:52
Arm Trap Grip 	41:38
Pulling Guard - A New Philosophy of Guard Pulling 	45:21

##### 2 Pulling to Sweep

Pulling to a Sweep (PTS Guard Pulls) - Pulling to Ashi Garami Sweep 	0
Double Ashi Garami Pull 	22:54
Pulling Ashi Garami Kenka-Yotsu Situation 	30:34
Pulling X-Guard 	35:33
More Sweeps From X-Guard Position 	51:57
Double Kouchi 	1:03:12
Pulling Half Guard/Knee Lever 	1:14:31
Pull to Half Guard Tight Waist 	1:21:27
Pulling to Butterfly Sumi Gaeshi 	1:39:47
Pulling to Butterfly Sumi Gaeshi 2 	1:52:00
Fake Guard Pull to Ankle Pick 	1:55:59
Pulling to a Tripod Sweep 	2:01:58
Reflections on Guard pulling 	2:07:11

##### 3 Pulling to Advantage

Pulling to Advantage (PTA Guard Pulls) - Pulling to Advantage 	0
Single Elevator 	8:36
PTA to Ude Gatame 	11:51
PTA to Shoulder Crunch Sumi Gaeshi 	14:46
PTA to Kata Gatame 	23:50
PTA to Pinch Headlock 	33:27
Slide-by 	45:48
Slide-by to Katagatame 	52:05
Pinch Headlock to Shoulder Crunch Sumi Gaeshi 	56:52
Pinch Headlock to Sumi Gaeshi 	1:00:43
Pinch headlock to Hiza Guruma 	1:09:55
The relationship between Hiza Guruma & Sumi Gaeshi 	1:18:36

##### 4 Pulling to Position

Pull to Position (PTP) 	0
Pull Counters - A Philosophy Of Guard Pull Counters: Winning the Guard Pull 	20:43
General Concepts For Pull Counters 	28:52
Passing Off the Pull (POP) 	36:43
Winning The Guard Pull Via Hand Post 	48:55
Jump Pull Counter 	59:30
O Soto Pass vs Seated Pull 	1:07:41
Post & Jump Pass vs Seated Pull 	1:12:59
Long Step vs Seated Pull 	1:17:08

##### 5 Submission Off the Pull

Submission Off the Pull (SOP) - Achilles Lock 	0
Countering Seated Pull with Knee Bar 	18:32
Countering Seated Pull with Flying Triangle / 3/4 Juji Gatame 	24:31
Guard Pull Negation: Knee Drop Method 	30:51
Cheap Shot Takedowns Off the Guard Pull 	40:00
Pull to Submission - Sitting Armbar (Juji Gatame) 	45:37
Sitting Juji Gatame: Ai-Yotsu 	56:47
Sitting Juji Gatame: Ai-Yotsu Part 2: Prone Finish 	1:06:46
Sitting Juji Gatame: Kenka-Yotsu Situation 	1:19:09
Sitting Juji Gatame: Kenka-Yotsu Situation: Tricep Grip 	1:25:39
Flying Arm Bar: Scapula Grip 	1:32:49
Arm Trap Entry To Arm Bar 	1:38:54
Flying Triangle 	1:47:29
The Achilles Lock 	2:03:31

##### 6 Takedown Counters

Takedown Counters - A philosophy of takedown counters 	0
Understanding the Rules of Jiu Jitsu with Regards to Takedown Counters 	4:32
Countering Upper Body Takedowns - Hip Check 	11:10
Hip check part 2 	18:11
Hip check to strangle hold 	23:57
Hip check to Armbar / juji gatame 	28:05
Hip check to rear Ezekiel 	33:59
Hip check into arm trap guard pull 	39:33

##### 7 Counters

Step around counters - Outside step around 	0
Countering foot techniques with a step out 	6:46
Lower body takedown defense - Legal concerns 	13:22
General consideration on defending lower body takedowns 	19:58
Sprawl vs double leg 	29:26
Pant reversal vs double leg 	35:33
Countering single leg with Harai Goshi 	42:31
Sumi gaeshi vs single leg 	46:05
Yoko sankaku vs single leg 	50:14
Risk and Reward: More Takedowns 	56:25
Modified drop Seoi-Nage 	59:25
Modified drop Seoi-Nage Part II 	1:06:22
Drop Tai-Otoshi 	1:10:09
Modified Seoi Otoshi 	1:18:19
Ogoshi 	1:24:36

##### 8 Mat Returns

Special Topic: Mat Returns in Jiu Jitsu 	0
Special Topic: Recovering from Failed Takedowns in Jiu Jitsu 	12:45


### 4.4 STANDING TO GROUND 1 

##### 1 Insights

Intro 	0
The 4 Most Important Ideas of This Video - Wherever Possible, Avoid Carrying Your Opponent's Weight 	7:30
Focus on the Easier Takedowns First 	33:31
Hand Fight With Direction 	38:28
In Jiu Jitsu, the Aftermath of the Takedown is as Important as the Takedown Itself 	42:51
Standing Preliminaries - Stance: Defensive Responsibility 	52:06
Stance: The Beginnings of Offense 	1:01:16
Stance: Vision 	1:11:12
Same Side and Opposite Side Stances 	1:14:32
Motion 	1:18:43
Contact and Grip - First Contact 	1:24:52
Angle Within Your Opponent's Elbows 	1:35:46

##### 2 Hand Fighting

9 Important Principles of Hand Fighting - Head Position 	0
The Theory of Indirect Gripping 	12:53
Inside VS Outside Control 	19:54
Grip - Contract - Move 	27:47
Push Pull Principles 	34:08
The Law of Elbow Extension 	42:45
Avoid Symmetrical Grips 	51:34
The Reset Principle 	1:00:41
Hand Fighting With Purpose 	1:06:58
Breaking Your Opponent's Stance: The 3 Paths to Dominance in the Standing Position 	1:14:55
A Theory of Dominant Position on the Feet 	1:20:57
Preliminaries - Putting it All Together 	1:35:10

##### 3 Snap Down

Special study: The Snap Down - The Jab of Standing Grappling - The Purpose of Snapdowns 	0
Mechanics of the Snapdown 	11:07
The Most Important Snapdowns for You to Learn - Rear Hand Snap 	24:54
Lead Hand Snap 	26:26
Multi Snap 	27:20
Head Pass Snap 	28:23
Collar and Tricep Snap 	29:28
Collar and Forearm Snap 	31:36
Overtie Snap 	33:14
Single and Double Tricep Snap 	35:29
Pump Snap 	38:02
Underhook Snap 	40:18
2 on 1 Snap 	42:15
Kouchi Snap 	44:43
Takedowns - Front Takedowns and Rear Takedowns 	48:09
Front Takedowns - The 4 Step Pattern of Front Leg Takedowns - Phase 1: Setup 	49:51
Phase 2: Catch 	53:26
Phase 3: Building Tighter Connection and Structure 	1:01:59
Phase 4: Completion 	1:09:37

##### 4 Double Leg

Double Leg Takedown - Advantages and Disadvantages of Double Leg Takedowns 	0
The Way Double Legs are Typically Taught is Wrong Headed 	3:52
The First Double Legs to Learn - Reverse Overback Double Leg 	6:52
Front Headlock Double Leg 	11:22
Short Double Leg 	17:49
The Second Double Legs to Learn - Pull Double Leg 	27:15
Snap Double 	33:23
Post Double 	37:45
Arm Drag Double 	44:58
Misdirection Double 	49:49
The Third Double Legs to Learn - Shooting From Distance 	53:45
The Number One Setup For Outside Shots: Fake Reach 	1:05:42
Double Knee Slide Method 	1:09:51

##### 5 Double Leg

Elbow Deep Double Leg: Georges St. Pierre - Mechanics of the Elbow Deep Double Leg 	0
Setups 	9:16
Corkscrew Finish 	13:37
Flare Across Finish 	16:17
Ear to Back Finish 	18:59
Opponent Underhooks/Te Guruma 	21:21
High Double Leg 	26:36
One Handed Snatch 	33:44
Single Leg Takedown - Mechanics of Single Leg 	38:25
High Single Leg - Mechanics 	46:52
Finishing Strategies for Single Legs 	50:38
Inside Reach/Outside Reach 	54:29
Attacking the Back Leg and Attacking the Front Leg 	57:49

##### 6 Single Leg

Single Leg Finishes With One Leg - Kneeling Cutback 	0
Standing Cutback 	5:16
Tight Waist Cutback 	8:31
Running the Pipe/Dump 	14:27
Thigh Pry 	21:00
Foot Trip 	24:11
Back Side Finish 	28:05
Single Leg to Double Leg - Far Side Finish 	34:59
Near Side Finish 	40:16
Barzegar Finish 	43:45
Drop Double 	49:50
Back Side Double 	53:25

##### 7 High Leg Finishes

High Leg Finishes - De Ashi Harai 	0
Knee Pressure 	5:13
Harai Goshi 	10:47
Tree Topping 	13:07
Thigh Pry 	18:15
Outside High Leg 	21:13
Troubleshooting Single Leg - First Problem: Limp Leg 	26:01
First Problem: Limp Leg - Part 2 	33:03
Problem 2: Base 	36:27
Problem 3: Maintaining Stance 	41:21

##### 8 Body Lock Takedowns

Body Lock takedowns - My Philosophy of Body Lock Takedowns 	0
The Number 1 Method of Getting to Body Locks in Jiu-Jitsu & Submission Grappling 	5:17
Mechanics of Body Locking 	9:03
First Attack: Hip in Front - Power Twist (Uki Goshi) 	16:16
Power Twist - Part 2 	23:32
Second Attack: Hips Behind - Three Step Finish 	27:15
The two Step method 	32:55
Rotational Method 	40:49
Lifting Method 	45:49
Placido and Giancarlo Drill Body Locks 	50:23
Knee Sweep Method 	58:21
Third Attack: Taing the Back - Slide By 	1:02:04

##### 9 Knee Pick

Knee Pick (Kuchiki Daoshi) - The Relationship Between Leg Tackles, Body Locks and Knee Picks 	0
Mechanics of the Knee Pick 	7:36
Failed Leg Tackle to Knee Pick 	19:54
Direct attacks From an Underhook 	24:05
Ankle Pick (Kibisu Gaeshi) - Mechanics and Tactics of Ankle Picking 	31:53
Front Headlock to Ankle Pick 	42:03
Front Headlock Cross Ankle Pick 	45:17
Double Pits Ankle Pick 	48:19
Collar Tie Ankle Pick 	56:59
Overtie Ankle Pick 	1:04:51
Underhook Ankle Pick 	1:07:03
Placido and Giancarlo Drill the Underhook Ankle Pick 	1:14:17
Floor Post Ankle Pick 	1:19:01

##### 10 Takedowns

Takedowns That Employ Your Feet and Legs (Ashi-Waza) - Kouchi Gari - Arm Drag Kouchi 	0
Kouchi Gari Hand Assist 	9:51
Kouchi From Overhook and Underhook 	16:02
Ouchi gari - Mechanics 	21:33
Double Triceps Ouchi Gari 	25:44
Collar and Elbow Ouchi Gari 	35:10
Placido and Giancarlo Drill Ouchi Gari 	37:37
De Ashi Harai - Mechanics 	46:04
De Ashi Off the Grip 	52:30
Collar Tie De Ashi 	57:06
Overhook De Ashi Harai 	1:01:27
Two On One De Ashi Harai 	1:10:12
Sesae Tsuri Komi Ashi - Mechanics 	1:23:17
Underhook Sasae 	1:29:58
Collartie 	1:34:06
Scoring Under Jiu-Jitsu Rule Set 	1:37:23
Scoring in a Competitive Jiu Jitsu Situation 	1:46:02
Takedowns Breakdowns and Scores 	1:54:10

### 4.5 STANDING TO GROUND 2

##### 1 Insights

Intro 	0
The Main Theme of This Video 	5:27
1st Theme: Positional Hierarchy Applies to the Standing Position 	8:02
2nd Theme: Reshots Are Easier Than Shots 	10:58
3rd Theme: Submission are the Joker in the Pack 	12:47
4th Theme: Scoring Criteria in Jiu Jitsu is Different From Wrestling 	16:46
5th Theme: Takedown Defense as 2 Distinct Phases 	22:49
Chapter 1: Positional Takedowns - The Rear Body Lock - Understanding the Rear Body Lock 	26:37
Getting to a Standing Rear Body Lock From Neutral - Armdrag - Mechanics of the Armdrag 	34:45
Head Inside Arm Drag 	43:33
Head Over Arm Drag 	51:40
Sliding Arm Drag 	56:26
Arm Drag Drill 	1:03:15
Sasae Arm Drag 	1:06:51

##### 2 Throw By

Throw By - Underhook Throw By 	0
Front head Lock 	8:08
Slide By - Collar Tie Slide By 	12:00
Collar Tie Slide By II 	17:51
Shrug Slide By (with a V-post) 	21:47
Body Lock Slide By 	29:44
Power Twist (Uke Goshi) 	37:30 
Duck Under - Collar Tie Elbow Post 	43:30
Collar Tie Snap Duck 	49:23
Underhook Duck Under 	53:29
Wrist Ducks 	57:38 

##### 3 2 on 1 Position

Special Study: The 2 on 1 - A Superb Backtake Method - Getting to a 2 on 1 	0
Controlling the 2 on 1 Position 	8:52
2 on 1 Figure 4 Snap 	13:24
Figure 4 2 on 1 Snap Down Go Behind 	19:24
2 on 1 arm drag 	23:23
Climbing the Arm from a 2 on 1 	27:24
Uchi Mata Go Behind 	32:27
Takedowns From The Standing Rear Body Lock - Reverse kosoto Gake 	36:18
Reverse Deashi Harai (Walking Variation) 	42:24
Reverse Deashi harai (static version) 	48:26
Reverse Half Sasae 	52:53
Knee Sweep 	57:56
Reverse Kouchi Gari 	1:03:27
Reverse Tai Otoshi 	1:08:46
Reverse Double Leg 	1:13:33
Turning Double Leg 	1:17:29
The Side Lift 	1:22:09
Hip Crank 	1:27:32

##### 4 Short Offense

Short Offense - What is Short Offense? 	0
Getting Into Short Offense - Proactive Method: Snapdowns 	7:20
Snapdown Drill 	15:24
Reactive Entries Into Short Offense 	20:41
The 3 Major Grips from Short Offense 	25:05
Double Pits Arm Drag Go Behind 	35:35
Front Head Lock Arm Drag Go Behind 	47:08
Arm Drag Go Behind Into Takedowns 	51:59
Short Offense Arm Drag Go Behind Drill 	57:07
Chapter 2: Reshots - Why Reshots Are Easier Than Shots 	59:24
Classic Reshot Drill 	1:03:21
Classic Reshot Drill Featuring Placido Santos 	1:05:38
Ankle Pick Reshots 	1:07:30

##### 5 Scoring Positions

Chapter 3: The Theory and Practice of Scoring Takedowns in Jiu Jitsu - Scoring Criteria for Jiu Jitsu Takedowns 	0
Understanding the Problem: 3 Critical Scoring Positions 	8:22
Scoring on Turtle 	12:08
Scoring on Referees Position 	33:58
A Great All Around Method Of Stopping an Opponent Standing Up: Crab Ride 	44:28
Scoring on 4 Point Position 	51:14
A Common Problem From 4 Point Position and a Simple Solution 	1:01:17
A Very Important Insight for Jiu Jitsu Players: The Golden Rule of the Arms 	1:04:26
Breakdowns From 4 Point Position - Back Foot Shoelace Breakdown 	1:08:40
Front Foot Shoelace Breakdown 	1:15:45
4 Point Breakdown Drill 	1:19:33
Kosoto Breakdown 	1:22:36
Spiral Breakdown 	1:25:37
Gut Wrench (Daki Wakare) 	1:30:06
Back Cradle Breakdown 	1:39:31
Hamstring stretch Breakdown 	1:43:41
Seatbelt Trip Back 	1:50:51

##### 6 After Takedown

Chapter 4: The Key to Winning the Jiu Jitsu Takedown Game: Controlling the Immediate Aftermath of a Takedown -Stepping Over The Legs 	0
Cover the Hips 	10:40
Head Over Head 	14:25
Chapter 5: Takedown Defense - Defending Leg Tackles: The Golden Rule 	19:33
Defending the Single Leg Takedown: Belly Whizzer 	23:18
Defending the Single Leg Takedown: Hip Tilt 	32:41
Defending Head Inside Single: Sumi Gaeshi 	35:43
Countering a High Single Leg 	39:17
How to Sprawl More Effectively: 4 Don'ts and a Do 	45:10

##### 7 Takedown Defense

Special Topic: The Limp Leg Takedown Defense - The Turning Limp Leg 	0
Turning Limp Leg vs High Crotch 	6:12
Turning Limp Leg vs Double Leg 	7:46
A High Turning Limp Leg 	9:29
The Front Limp Leg 	12:10
The Low Limp Leg 	15:43
Defending a Double Leg Takedown 	18:25
Hip Heist 	22:41
Roll Through Counter 	28:13
Defending the High Crotch 	32:15
Chapter 6: Positional Defense - A Current Weakness in Jiu Jitsu 	37:26
Standing Escapes Contrasting Styles 	40:27
Kimura Roll 	50:27
Cross Body Granby 	53:08
Granby 	56:38
Standing Granby 	58:51
Elbow Penetration 	1:01:31
Maki Komi 	1:03:56
Switch 	1:06:46

##### 8 Standing Submissions

Chapter 7: Standing Submissions - The Crucial Role of Submissions in the Standing Jiu Jitsu Game 	0
High Wrist Guillotine 	6:57
Arm In Guillotine 	12:39
Learn to Fight the Takedown First and Apply the Submission Second 	23:13
Darce Strangle 	26:58
Anaconda Strangle 	35:54
Side Triangle (Yoko Sankaku) 	39:25
Kimura vs Single Leg 	43:55
Kimura Part II 	50:17
Kimura Part III 	54:40
Hand Kani Basami to Heel Hook 	57:40
Forward Roll To Heel Hook 	1:07:35


### 4.6 STANDING TO GROUND 3

##### 1 Upper Body Takedown Concepts

Introduction 	0
Upper Body Takedown Concepts - The Central Problem of Takedowns in Jiu Jitsu 	4:30
Advantages of Upper body Takedowns 	14:52
Disadvantages of Upper Body Takedowns 	19:32
Minimum Athletic Requirements to Perform the Moves in This Video 	25:44
Entering Into Upper Body Grips: The 3 Main Methods - The Legs as a Gateway to the Upper Body 	31:16
Defending a Shot 	33:39
Upper Body Grip Fighting 	35:22
The First Great Training Method to Develop Upper Body Skills: Pummelling 	40:56
The Second Great Training Method to Develop Upper Body Skills: Hip Jousting 	50:49

##### 2 Cow Catcher

Special Study: The Cow Catcher - Mechanics 	0
Two Approaches to the Cow Catcher: One Chin Strap First Two Tight Waist First 	13:01
Go Behind Setups to the Cow Catcher 	18:20
Reactions Based Off Elbow Position 	24:19
The Perfect Compliment to the Cow Catcher: The Roll Through 	26:57
Opponent's 2nd Line of Defense to Cow Catcher: Standing Up - Yoko Sumi Gaeshi 	33:47
Ouchi Gari 	41:53
A Great Setup For the Cow Catcher: Stopping a Shot 	48:04
An Alternative Way of Entering the Cow Catcher: Waist First Chin Strap Second 	55:26
Driilling Scene Featuring Placido Santos 	1:03:08

##### 3 Uki Waza

Special Study: Uki Waza - Arm Drag and Hip Variation 	0
Special Note on Arm Drag and Hip Variation 	11:40
2 on 1 Opponent Headlocks 	14:28
Front Headlock Uki Waza 	18:27
Overhook Uki Waza: The Lateral Drop 	22:32
My Favorite Setup Overhook Uke Waza (Lateral Drop) 	34:11
2 on 1 Uke Waza For the Strongest Lateral Drop 	37:03

##### 4 Sumi Gaeshi

Special Study: Sumi Gaeshi - Whizzer and Wrist Vs Single Leg 	0
Lat and Wrist Vs Single Leg 	10:40
Chest Wrap Sumi Gaeshi Vs Double Leg 	15:07
Butterfly Sumi Gaeshi Vs Double Leg 	21:05
Sumi Gaeshi Vs High Crotch 	25:17
Yoko Sumi Gaeshi on the Mat 	29:27
2 on 1 Yoko Sumi Gaeshi 	34:12
Arm Drag Sumi Gaeshi 	39:03
Special Study: Ude Gaeshi - Mechanics 	44:09
Application 	50:28
Setups 	54:38
Mistakes To Avoid 	59:16

##### 5 Throwing from Overhook

Throwing From the Overhook - Uki Goshi (Half Hip Throw) 	0
Tai Otoshi 	6:17
One Handed Tai Otoshi 	9:21
Uchi Mata 	11:57:00
Harai Goshi 	22:01:00
Uki Waza 	25:48:00
Kosoto Gake 	30:23:00
Throwing from the Over/Under Tie Up - Uke Goshi (Half Hip Throw) 	36:25
Single Knee Drop Variation 	42:33
Double Knee Drop Variation 	46:42
Uki Waza 	50:28
Ouchi Gari (Inside Trip) 	56:45
Step Around Kosoto Gake 	1:02:04
Kosoto Gake Calf to Calf Variation 	1:10:21

##### 6 Double Overhooks

Double Overhooks - Understanding Double Overhooks 	0
Ouchi Gari (Inside Trip) 	5:07
Step Around Kosoto Gake 	10:00
Calf to Calf Kosoto Gake (Outside Trip) 	14:53
Uki Waza Lateral Drop 	20:55
Double Underhooks - Understanding Double Underhooks 	24:21:00
Step Around Kosoto Gake (Outside Trip) 	33:16:00
Calf to Calf Kosoto Gake (Outside Trip) 	39:33:00
Back Bend Takedown 	19:19:00
Ken Ken Uchi Mata 	22:32:00
Uki Goshi (Hip Throw) 	53:08
Uki Goshi High Lock 	56:03
Tai Otoshi High Lock 	1:02:19
Sasae Foot Sweep 	1:05:44

##### 7 Throwing from Underhook

Throwing From an Underhook - Uki Goshi (Half Hip Throw) 	0
Ogoshi (Full Hip Throw) 	6:45
Tai Otoshi 	11:26
Uchi Mata 	17:14
Kouchi Trip (Small Inside Trip) 	22:59:00
Placido Asks a Question 	30:26:00
Pinch Headlock - Uki Goshi 	44:36:00
Kosoto and Kouchi From Pinch Headlock 	53:49
Reverse Lat Grip - Osoto Gari 	57:03
Harai Goshi 	1:08:05
Special Topic: Georges St. Pierre Hybrid Throw 	1:12:26

##### 8 Older Athletes

Upper Body Takedowns for Older/Less Athletic Athletes - What is it That Makes Upper Body Takedowns Difficult for Older and Less Athletic People 	0
Tai Otoshi From Arm Drag and Lat Grip 	4:20
The Uki Goshi Hustle 	8:46
Arm Drag and Hip Kouchi Trip 	11:41:00
The Lateral Drop Uke Waza 	13:55:00
Uki Goshi (Half Hip Throw) 	20:11
Kouchi Trip 	23:45:00





### GUARD PASSING 

Introduction to Passing the Guard 	0 - 3:40
Philosophy of Opening a Closed Guard 	3:40 - 12:11
Top Position: Opening a closed Guard 	12:11 - 30:31
Opening Closed Guard 2 	30:31 - 37:41
opening closed guard 3 	37:41 - 44:26
Opening Closed guard 4 	44:26 - 53:02
opening closed guard 5 	53:02 - 56:05
opening closed guard 6 	56:05 - 1:00:39
opening closed guard 7 	1:00:39 - 1:05:40
opening closed guard 8 	1:05:40 - 1:07:20
opening closed guard 9 	1:07:20 - 1:11:48
opening closed guard 10 	1:11:48 - 1:19:03
opening closed guard 11 	1:19:03 - 1:29:41
Volume 2
CHAPTER TITLE
START TIME
Overview of Standard Method Of Opening Closed Guard 	0 - 8:29
Opening Closed guard: Knee Post Method 	8:29 - 16:12
Knee Post Method 2 	16:12 - 21:40
Knee Post Method 3 	21:40 - 23:53
knee post method 4 	23;53 - 27:33
knee post method 5 	27:33 - 29:30
knee post method 6 	29:30 - 34:07
Opening Closed Guard / Lifting Method 	34:07 - 41:16
Overview of 3 methods 	41:16 - 47:10
Problem of Procrastination 	47:10 - 52:50
the Most Important Message Of This Video: The 5 Steps Of Guard Passing - Step 1 	52:50 - 1:00:13
Step 2 	1:00:13 - 1:06:25
Step 3 	1:06:25 - 1:10:52
Step 4 	1:10:52 - 1:17:44
Step 5 	1:17:44 - 1:27:10
Negate Advantage Completion Guard Passing Model - NAC Open Guard Passing Method 	1:27:10 - 1:31:35
Split Squat as Ultimate Staging Platform 	1:31:35 - 1:36:29
Single Biggest Insight into Guard Passing: Side to Side Pressure 	1:36:29 - 1:43:50
Mechanical and Tactical Advantage 	1:43:50 - 1:53:22
Volume 3
CHAPTER TITLE
START TIME
First Requirement Of A Guard Passing Program: Technical Proficiency With The Most High Percentage Methods 	0 - 1:34
Double Under Pass 	1:34 - 26:12
Over Under Pass 	26:12 - 46:17
Knee Cut Pass 	46:17 - 1:26:06
Volume 4
CHAPTER TITLE
START TIME
Toreando Pass 	0 - 1:05:33
Volume 5
CHAPTER TITLE
START TIME
Leg Drag 	0 - 35;06
Long Step Guard Pass 	35:06 - 1:09:15
The Smash Pass 	1:09:15 - 1:18:14
Volume 6
CHAPTER TITLE
START TIME
Second Requirement Of A Guard Passing Program: The Ability To Break Connections And Shut Down a Dangerous Guard 	0 - 7:26
Cross Collar Cuff And Bicep Guard 	7:26 - 28:36
De La Riva Guard 	28:36 - 42:37
Reverse De la Riva 	42:37 - 47:13
Lasso Guard 	47:13 - 52:56
Spider And Lasso Guard 	52:56 - 1:00:18
Double Spider Guard 	1:00:18 - 1:07:27
Emergency Measures 	1:07:27 - 1:14:13
Ashi Garami 	1:14:13 - 1:21:37
The 3rd Requirement Of A Guard Passing Program: The Ability To Maintain Top Position 	1:21:37 - 1:23:40
The Ancient Law Of Push When Pulled And Pull When Pushed 	1:23:40 - 1:31:26
Recovering From A Fall 	1:31:26 - 1:44:38
Volume 7
CHAPTER TITLE
START TIME
The Fourth Requirement of a Guard Passing Program: Using Staging Positions To Pass A Dangerous Guard - the four best staging positions - 1) The Split Squat Position (Headquarters) 	0 - 12:47
Second Great Virtue of the Split Squat Position: hip Control and Head Control 	12:47 - 16:30
Negating Both Feet From Split Squat 	16:30 - 21:45
2) The Knee Drop 	21:45 - 26:13
3) Double Knee Position 	26:13 - 29:56
4) Outside Advantage Position 	29:56 - 39:07
Putting Skills & Theory Into Combative Context 	39:07 - 51:47
Flattening an Opponent 	51:47 - 1:00:45
Negating An Opponent’s Guard 	1:00:45 - 1:07:46
The Fifth Requirement of a guard passing program: finding advantage within a neutral position - creating advantage while passing 	1:07:46 - 1:26:01
Volume 8
CHAPTER TITLE
START TIME
Guard Passing From The Shoulder Line 	0 - 8:35
sixth requirement of a guard passing program: breaking through defensive frames and getting to your pin - Beating Frames - Distance 	8:35 - 13:48
Angle Change 	13:48 - 16:04
Direction 	16:04 - 20:39
Directly Attacking The Frames 	20:39 - 24:11
Side To Side Pressure 	24:11 - 29:35
Beating Inversion 	29:35 - 33:40
Turning In To Turtle 	33:40 - 38:24
Turning Out To Turtle 	38:24 - 42:33
Propping 	42:33 - 46:20

### How To Take The Back

Intro 	0
Overview 	4:41
Turtle Situation - Seat Belt or Rear Body Lock 	6:53
Seat Belt Roll 	12:42
Chest Lock Roll 	21:23
2 on 1 Tilt 	31:49
Tight Waist Achilles 	43:05
Claw Tilt 	50:01
Bottom Hook First 	56:27
Figure 4 	1:02:21
Crucifix 	1:08:42
Volume 02
CHAPTER TITLE
START TIME
Front Headlock - Go Behinds 	0
Cross Body Ride 	15:16
Hand Fighting 	24:59
Seated Kata Gatame 	34:41
Volume 03
CHAPTER TITLE
START TIME
4 Point - Spiral Ride 	0
Asymmetrical Hooks 	7:35
Hamstring Lock 	18:43
Claw 	25:52
Sweep the Leg 	37:26
Back Arch 	43:30
Cross Body Ride 	52:03
Volume 04
CHAPTER TITLE
START TIME
Rear Body Lock - Ashi Otoshi 	0
Hands to the Mat 	6:22
Knees and Hips to the Mat - Reverse Tai Otoshi 	15:06
Mat Returns 	19:56
De Ashi 	24:57
Reverse Kouchi 	32:42
Kick Out 	35:37
Knee Hooks 	38:29
Volume 05
CHAPTER TITLE
START TIME
Winning the Scramble - Follow the Hips 	0
Crab Ride 	5:56
Cross Body Ride Transition 	12:39
1 Hook Conversion 	23:10
The Hot Seat 	34:56
Shoveling 	45:11
Guard Recovery 	49:33
Following Grandbys 	55:12
Maki Komi Defense 	1:02:03
Protect Your Legs 	1:06:28
Volume 06
CHAPTER TITLE
START TIME
Simple Systems - System 1 	0
System 2 	5:22
System 3 	8:24
System 4 	11:23
System 5 	13:59
System 6 	15:23
Heisting - Failed Submissions 	17:16
Guard Recovery 	24:08
Volume 07
CHAPTER TITLE
START TIME
Overcoming The Whizzer - Standing Position 	0
Dogfight 	9:42
Half Guard 	21:02
Side Body Lock 	38:18
Using The Whizzer 	46:26
Volume 08
CHAPTER TITLE
START TIME
Shooting For The Legs 	0
Front Headlock 	11:59
Pinch Headlock/Body Lock 	19:36
Failed Sumi Gaeshi 	25:24
Armdrag 	30:47
Supine 	43:40
Re-Drag 	56:14
Volume 09
CHAPTER TITLE
START TIME
Ashi Bolo 	0
Stack Position 	16:45
Passing 	29:17
Kimura - T Kimura 	39:33
Kimura Entries 	45:31
Volume 10
CHAPTER TITLE
START TIME
Submissions - Armlocks 	0
Omoplata 	10:07
Kata Gatame 	19:43
Over Under 	29:33
Duck Under 	35:45
Closed Guard 	43:31
Reverse Overback 	1:00:07

#### THE FASTEST WAY TO INCREASE SUBMISSION PERCENTAGE 

### Volume 1

Introduction	0:00 - 3:12
Nothing Beats The Back - How To Increase Your Submission Rate: The 4 Key Factors	3:12 - 10:37
The 3 Gateways To The Back: Training Your Mind To See Opportunities	10:37 - 22:33
The Rear Strangle Hold Is The Most High Percentage Submission In Jiu Jitsu	22:33 - 35:39
Building A Program To Maximize Your Submission Progress In Minimum Time - Getting To The Back From Everywhere - Getting To The Back From Neutral Position - Front Headlock - Chin Strap Go Behind	35:39 - 41:28
Wrist Block Go Behind	41:28 - 46:04
Arm Drag Go Behind	46:04 - 51:24
Straight Knee Block Go Behind	51:24 - 58:11
Cross Knee Block Go Behind	58:11 - 1:01:00
Head Block Go Behind	1:01:00 - 1:06:40
Misdirection Go Behind	1:06:40 - 1:11:14
Going Behind When Your Opponent Changes To 4 Point Stance - Kosoto Go Behind	1:11:14 - 1:15:21
4 Point Misdirection	1:15:21 - 1:18:26
4 Point Breakdown	1:18:26 - 1:21:47
Clearing A Leg	1:21:47


### Volume 2

Open Guard - Rising Arm Drag	0:00 - 3:00
Rising Drag To Go Behind	3:00 - 5:05
Scooting Arm Drag	5:05 - 10:02
Far Side Arm Drag	10:02 - 16:44
Hiza Guruma Arm Drag	16:44 - 23:00
Weave Drags	23:00 - 27:31
Sumi Gaeshi Arm Drag	27:31 - 30:56
A Different Phiosophy Of Finishing Arm Drags: The Post And Pull Method	30:56 - 35:55
Elevator Drag	35:55 - 39:22
Arm Drag On Standing Opponent	39:22 - 45:41
Elbow Post	45:41 - 50:23
Snapdowns - Heist And Snap	50:23 - 54:03
Snap And Catch To The Back	54:03 - 58:45
Front Headlock Sumi Gaeshi Go Behind	58:45 - 1:02:57
Sumi Gaeshi To Go Behind	1:02:57 - 1:07:07
Reverse De La Riva Inversions	1:07:07

### Volume 3

Taking The Back From Half Guard - Knee Lever Elbow Post	0:00 - 5:35
Duck Under	5:35 - 13:57
Limp Arm	13:57 - 23:29
Walk Around	23:29 - 28:27
The Pull Through	28:27 - 32:17
Far Side Hook	32:17 - 36:13
Deep Half Guard Back Take	36:13 - 41:40
Closed Guard Back Takes - Closed Guard Elbow Post	41:40 - 51:23
Double Underhooks Back Take	51:23


### Volume 4

Taking The Back From Dominant Position - Getting The Back From The Mount - Single Arm Chest Wrap To The Back	0:00 - 10:09
Mount Bolo	10:09 - 16:31
Side Mount To Rear Mount - Elbow Block Go Behind	16:31 - 22:24
Seated Entry To Rear Mount	22:24 - 29:08
Getting To The Back From Inferior Position - Elbow Escape To The Back	29:08 - 35:20
Elbow Escape To The Back Part 2	35:20 - 39:21
Kipping Escape To Take The Back	39:21

### Volume 5

Taking The Back From Side Control Bottom - Coming Up With A Tight Waist	0:00 - 10:06
Coming Up To Single Leg	10:06 - 15:54
Coming Up To Front Headlock From The Back	15:54 - 21:01
Chest Lock Sitout To The Back	21:01 - 30:07
Taking The Back From Leg Entanglements - Taking The Back From Ashi Garami	30:07 - 39:14
Taking The Back From Cross Ashi Garami (Turning Away)	39:14 - 44:23
Taking The Back From Cross Ashi Garami (Turning Towards)	44:23 - 50:31
Taking The Back From 50/50 Entanglements	50:31 - 57:47
Back Side 50/50 To The Back	57:47 - 1:02:48
The Side Bolo	1:02:48


### Volume 6

Maintaining The Back - Upper Body Control - Seatbelt	0:00 - 10:28
Reverse Seatbelt	10:28 - 15:08
Rear Body Lock	15:08 - 20:40
Tight Waist	20:40 - 23:54
Thigh Pry	23:54 - 28:18
Claw Grip	28:18 - 32:47
Lower Body Control - The Most Important Lower Body Controls	32:47 - 49:40
Integrating Upper Body And Lower Body Control - Seatbelt Sag	49:40 - 58:45
2 On 1 Leg Method	58:45 - 1:11:54
Breakdown Method	1:11:54 - 1:21:26
Far Side First Method	1:21:26 - 1:27:37
Near Side Hook First Seatbelt Second Method	1:27:37 - 1:35:55
Shoulder Pressure Method	1:35:55


### Volume 7

Two Crucial Principles Of Back Control - Diagonal Control	0:00 - 4:00
The Principle Of Knees And Elbows	4:00 - 9:32
Classic Back Retention Drill	9:32 - 19:03
Finishing Mechanics Of Rear Strangle	19:03 - 24:13
One Handed Strangle	24:13 - 27:58
Linear Mandible Strangle	27:58 - 33:02
Turning Mandible Strangle	33:02 - 35:51
Palm To Palm Strangle	35:51 - 44:19
Understanding The Problem That You're Trying To Solve	44:19 - 52:41
Winning The Hand Fight - Straight Jacket Method	52:41 - 1:04:01
Understanding The Straight Jacket System In Minimum Time	1:04:01 - 1:12:20
Strangling Without An Arm Trap - Top Hand Position Method	1:12:20 - 1:25:03
The Hidden Hand Method	1:25:03 - 1:32:16
The Gravity Drop	1:32:16 - 1:45:05
Strangle First Methods	1:45:05


### Volume 8

Building A System Of Strangles Beyond The Rear Strangle - Kata Gatame	0:00 - 6:37
Reverse Kata Gatame	6:37 - 11:08
Back Kata Gatame - Bottom Elbow Variation	11:08 - 23:24
Turn Away Bottom Elbow	23:24 - 29:14
Prone Variation	29:14 - 33:46
North South Strangle	33:46 - 40:32
Ushiro Sankaku (Rear Triangle)	40:32


### PILLARS OF DEFENSE - BACK ESCAPES 

##### Volume 1

Intro To Escaping The Back	0:00 - 5:07
Offensive Goals	5:07 - 9:28
Defensive Goals	9:28 - 13:28
Overview	13:28 - 16:38
Intro To Defensive Hand Fighting	16:38 - 25:16
Sticky Grips	25:16 - 29:05
Secondary Hand Interception	29:05 - 36:15
Countering A Garrote - Primary Hand Reach Back	36:15 - 39:43
Countering A Garrote - Secondary Hand Reach Back	39:43 - 45:03
Thumb Posting Out	45:03 - 47:55
Thumb Post Vs. Locked Strangle (Strangle Arm)	47:55 - 50:26
Thumb Post Vs. Locked Strangle (Support Arm)	50:26 - 52:47
End Game	52:47 - 54:02
Negating An Attempted Arm Trap	54:02 - 56:35
Freeing A Trapped Arm (Kipping)	56:35 - 59:49
Freeing A Trapped Arm (Limp Arm)	59:49 - 1:03:59
Opponent Counter Limp Arm Step Over Bottom Hook	1:03:59 - 1:08:19
Countering A Locked Strangle (Locked Legs)	1:08:19 - 1:11:30
Countering A Locked Strangle (Unlocked Legs)	1:11:30 - 1:14:20
Closing Overhook Side, Hand Fighting	1:14:20

##### Volume 2

Intro To Upper Body Escapes (OverHook Side)	0:00 - 2:43
Double Over Conversion	2:43 - 5:37
Origin Of The Headlock Escape	5:37 - 9:53
Basic Headlock Escape	9:53 - 14:17
Headlock Escape - Enter The Legs	14:17 - 20:21
Avoiding Mount	20:21 - 23:45
Basic Two On One Pass	23:45 - 30:46
Two On One Pass/Headlock Combo	30:46 - 32:29
Countering A Half Claw Recovery	32:29 - 36:02
Closing Out Upper Body Escapes (OverHook Side)	36:02

##### Volume 3

Intro To Lower Body Escapes (OverHook Side)	0:00 - 3:39
Stepping Over The Bottom Hook	3:39 - 9:10
Countering A Long Hook	9:10 - 13:51
Countering A Deep Hook	13:51 - 16:57
Countering A Butterfly Hook	16:57 - 21:25
Closing Out Lower Body Escapes (OverHook Side)	21:25 - 22:49
Introduction To Body Triangle Escapes (OverHook Side)	22:49 - 27:56
4 Body Triangle Scenarios	27:56 - 29:51
Pass Offs	29:51 - 30:58
Basic Escape To Top Side Triangle	30:58 - 34:18
Elbow Wedge	34:18 - 37:37
Failed Trap Switch Sides	37:37 - 40:15
Bottom Side Body Triangle	40:15 - 45:15
Closing Out Body Triangle Escapes (Overhook Side)	45:15 - 46:44
Intro To High Ball Ride (Overhook Side)	46:44 - 48:40
Understanding Why Upper Body Escapes Don't Work	48:40 - 50:44
Overview	50:44 - 51:58
Knee Off The Hip Turn To Turtle	51:58 - 57:17
Baiting Turtle To Step Over The Bottom Hook	57:17 - 1:00:02
2 Options When Opponent Covers The Hip	1:00:02 - 1:05:38
Closing Out High Ball Ride (Overhook Side)	1:05:38

##### Volume 4

Intro To Cross Body Escapes (OverHook Side)	0:00 - 3:06
Overview	3:06 - 4:53
Escaping - No Upper Body Control	4:53 - 7:24
Escaping - Arm Control	7:24 - 13:10
Escaping - Upper Body Control	13:10 - 18:23
Closing Out Overhook Side Escapes	18:23 - 19:30
Introduction To Escaping The Underhooks	19:30 - 22:35
Differences In Hand Fighting	22:35 - 25:32
Countering Cross Wrist Grips	25:32 - 32:03
Freeing A Trapped Arm	32:03 - 34:41
Strangle Escapes	34:41 - 42:38
Introduction To Upper Body Escapes (Underhook Side)	42:38 - 43:46
Converting To Double Overs	43:46 - 45:58
Converting To Double Unders	45:58 - 48:50
Elbow Cutting Vs Headlocks	48:50 - 53:55
Basic Elbow Cut	53:55 - 58:02
Differences In Avoiding Mount/Entering Ashi	58:02 - 1:05:57
2 On 1 Pass To Elbow Cut	1:05:57 - 1:08:38
2 On 1 Pass Convert To Overhook Side	1:08:38 - 1:11:55
Closing Out Underhook Side Upper Body Escape	1:11:55

##### Volume 5

Intro To Lower Body Escapes (Underhook Side)	0:00 - 1:54
Clearing The Bottom Hook/Entering Ashi/Avoiding Mount	1:54 - 3:38
Countering A Long Hook	3:38 - 8:31
Countering A Deep Hook	8:31 - 9:51
Countering A Butterfly Hook	9:51 - 13:19
Hand Assist Clear The Top Hook	13:19 - 16:36
Closing Out Lower Body Escapes (Underhook Side)	16:36 - 18:12
Intro To Body Triangle Escapes (Underhook Sides)	18:12 - 19:37
Topside Body Triangle Escapes (Underhook Side)	19:37 - 27:16
Bottom Side Body Triangle Escapes (Underhook Side)	27:16 - 30:46
Touching On Post Rear Mount	30:46 - 34:55
Closing Out Body Triangle Escapes (Underhook Side)	34:55

##### Volume 6


Happy Fathers Day Papa John!!

Looks like T-Rex Arms doesn't do gift cards yet so here's one you can use!!!

https://www.trex-arms.com

Love
Drew, Tracey and Dub


Introduction To High Ball Ride Escapes (Underhook Side)	0:00 - 0:46
Escaping High Ball Ride (Underhook Side)	0:46 - 5:42
Introduction To Escaping Cross Body Ride (Underhook Side)	5:42 - 7:07
Escaping Cross Body Ride (Underhook Side)	7:07 - 11:36
Intro To Double Overhook Escapes	11:36 - 13:24
Converting Double Overs To Under Over	13:24 - 16:23
2 On 1 Pass	16:23 - 20:07
Using Secondary Hands To Convert To A 2 On 1	20:07 - 23:14
Immediately Picking A Side	23:14 - 24:42
Countering A Strangle	24:42 - 28:20
Closing Double Overhooks	28:20 - 29:22
Intro To Double Underhooks	29:22 - 31:39
Connecting The Dots	31:39 - 34:12
Closing Out Double Underhook Escapes	34:12 - 36:06
Turning Escapes	36:06 - 41:42
Outro	41:42

##### Volume 7

Rolling - Christos	0:00 - 5:13
Rolling - Alex	5:13 - 10:15
Rolling - Drew	10:15 - 15:17
Rolling - Christos	15:17 - 20:19
Rolling - Alex	20:19

##### Volume 8

Rolling Commentary - Christos	0:00 - 10:58
Rolling Commentary - Alex	10:58 - 20:26
Rolling Commentary - Drew	20:26 - 29:24
Rolling Commentary - Christos	29:24 - 37:31
Rolling Commentary - Alex	37:31


### PILLARS OF DEFENSE - PIN ESCAPES 

##### Volume 1

Intro to Pin Escapes	0:00
Carrying Body Weight - Torso Vs Frames	3.41.00
Extension Vs Contraction Concave Vs Convex Shoulders	6.42.00
Cranium Control, Side Control and Mount Vs North South	11.01.00
Escaping - Static Vs Dynamic Pins	14.46.00
Overview	17.15.00
Escaping Knee on Belly	20.08.00
Escaping Knee on Belly Shin Mount Hybrid	27.26.00
Intro to Escaping Side Control	33.58.00
Side Guard Wins the game	36.20.00
Insertion Points	37.49.00
Inserting the Far Arm	43.09.00
Inserting Near Side Elbow	49.19.00
Escaping Vs a Naive Opponent	57.01.00

##### Volume 2

Actively Fighting the Far Elbow to Inside Position	0:00
Knee to Hip Contact	3.58.00
Side Guard	6.38.00
Basic Reguard From Side Guard	13.08.00
Recovering From Side Guard Into Clamp	17.39.00
Side Guard to Te Gatame	23.10.00
Te Gatame Into the Legs	27.20.00
Inserting a Far Side Underhook	33.08.00
Conservative Recovery	37.38.00
Underhook to Pinch Headlock/Shoulder Crunch	41.19.00
Underhook to Half Guard	45.41.00
Underhook to Ashi Garami	49.28.00
Underhook to Wrestling Scenarios	53.35.00

##### Volume 3

Leg Grab/Reguard Dilemma	0:00
Escaping With a Near Side Underhook	2.02.00
Opponent Crowds the Hips	11.11.00
Recovering With a Tricep Post	14.28.00
Tricep Post to Side Guard	18.40.00
Tricep Post to Overback Series	23.25.00
Countering a Reachback Crossface	29.17.00
Side Kipping	39.50.00
Side Kip Vs Posted Crossface Hand	42.30.00
Side Kip Vs Posted Underhook Hand	46.31.00

##### Volume 4

Intro to Double Underhooks	0:00
Escaping on the Pommel	2.25.00
Pommeling Back Inside	6.18.00
Intro to Opponent Hip Switching	14.39.00
Inserting Frames	16.57.00
Escaping With a High Leg	20.24.00
Pendulum/Side Guard Dilemma	24.31.00
Intro to Kesa Gatame	28.52.00
Escaping as Opponent Pommels	30.59.00
Escaping Fully Locked Kesa Gatame	33.56.00
Addressing Other Upper Body Grips - Body Lock	40.49.00
Tight Waist and Cross Face	47.50.00
Tight Waist and Near Side Underhook	56.53.00

##### Volume 5

Segway to North South	0:00
Escaping on the Transition Crossface Hand First	1.32.00
Escaping on the Transition Underhook Hand First	4.44.00
Escaping on the Transition Elbows Inside Shoulders	8.19.00
North South Body Positions	13.18.00
Opponent's Hand Positions	15.34.00
The Threat of Forcing Side Control	16.43.00
Opponent Has Knees Down Pommeling Hands Inside	20.05.00
Escaping With Knee Elbow Connection	27.45.00
High Tripod Pommeling Hands Inside	34.02.00
Forcing Side Control - Knees Up Vs Knees Down	41.59.00
Escaping as Opponent Moves From North South to Side Control	54.16.00
Side Control to Mount - Upper Body Positions	58.35.00
Escaping On the Transition -Top Head and Arm	1.00.36.00
Escaping On the Transition - Double Underhooks	1.09.15.00
Escaping On the Transition - Crossface	1.13.41.00

##### Volume 6

Mount Escape Overview	0:00
Opponent's Leg Positions	1.28.00
Bridging and Shrimping	2.55.00
Basic Knee Elbow Escape	6.33.00
Inside Elbow Escape	11.02.00
Clearing Grapevines and Preventing and Underhook	13.57.00
Separating Crossed Feet	18.00.00
Knee Elbow Escape/Inside Knee Elbow Escape Vs Crossface	21.03.00
Intro to Kipping	26.16.00
Solo Kipping Drill	28.10.00
Types of Kipping	31.26.00
Kipping/Knee Elbow Dilemma	33.49.00
Basic Overhead Kip	38.07.00
Kipping Over a Shoulder	43.13.00
Side to Side Kipping	46.43.00
Side Kipping When Opponent Counters Initial Kip	52.07.00
Kipping to Counter a Long Hook	55.22.00
Kipping to Counter One Knee Up	58.33.00

##### Volume 7

A Deeper Dive Into Arm Positions	0:00
Clearing an Underhook	4.18.00
Kipping to Free a Trapped Arm	6.52.00
Side to Side Kipping Vs Underhook	12.15.00
Same Concept When Opponent Switches to a Knee Up/Long Hook	15.40.00
Addressing Double Underhooks	18.47.00
Addressing a High Mount	23.33.00
Stripping a Figure Four Back Head and Arm	30.42.00
Preventing a Dismount to Side Control	36.40.00
Outro	40.20.00

##### Volume 8

Intro/Roll 1 - Alex	0:00
Roll 2 - Drew	28.24.00
Roll 3 - Cristos	40.41.00
Roll 4 - Alex	53.30.00
Roll 5 - Drew	1.10.05.00

##### Volume 9

Roll 1 - Alex	0:00
Roll 2 - Drew	5.15.00
Roll 3 - Cristos	10.21.00
Roll 4 - Alex	15.25.00
Roll 5 - Drew	20.28.00



### FASTEST WAY - UNPASSABLE GUARD 

### Volume 01

Introduction	0:00 - 2:56
12 Essential Body Movements for Guard Retention	2:56

### Volume 02

Understanding HOW An Opponent Passes Your Guard is the Key to Stop it From Happening - The 5 Steps of Guard Passing	0:00 - 11:50
The 3 Most Important Danger Zones of Guard Passing	11:50 - 16:25
Stance and Posture for Guard retention	16:25 - 23:40
The 3 Most Important Guard Passing Methodologies That You'll Be Working Against	23:40 - 27:15
Getting a Solid Start to Guard Retention With a Strong Stance and a Simple Game Plan	27:15

### Volume 03

Guard Retention Against Distance Passing/Toreando	0:00 - 8:28
High Leg Retention Method	8:28 - 15:07
Inside Leg Retention Method	15:07 - 19:07
Inside Foot or Knee Guard Retention Method	19:07 - 36:32
Outside Shoulder Roll Guard Retention Method	36:32 - 41:59
Elbow Prop Method	41:59 - 45:47
Turning Away From the Pass	45:47

### Volume 04

Countering Head Control - Elbow Post	0:00 - 4:22
Inside Position Leg Scissor	4:22 - 9:18
Guard Retention Vs North/South Toreando Passing - Leg Frames to Prevent North/South Transition	9:18 - 17:06
Situp Recovery	17:06 - 22:00
Framing From North/South	22:00

### Volume 05

Guard Retention Vs Hips Centered Passing - Negating Body Lock Guard Passing	0:00 - 21:20
The 3 Great Safety Positions Against a Body Lock	21:20 - 27:48
Late Defense Against Body Lock Passes - Chin Post and Shrimp	27:48 - 33:55
Outside Shoulder Roll	33:55 - 40:05
The Hip Slip	40:05

### Volume 06

Defending Against Double Scoop Grips - Working Against Locked Hands	0:00 - 2:23
Working Against Unlocked Hands	2:23 - 4:05
Leg Pommeling Vs Double Scoops	4:05 - 7:02
Wrist Control Vs Double Scoops	7:02 - 9:31
Guard Retention Vs Knee Cut - Knee Prop/Shield Method	9:31 - 17:32
Elbow Prop Vs Knee Cut	17:32 - 20:59
Knee Shield to Outdside Shoulder Roll	20:59

### Volume 07

Guard Retention Vs Head and Shoulder Control - Stopping Half Guard Passes - Top Head and Arm - Inside Knee/Elbow Escape	0:00 - 9:07
Outside Knee Shield Method	9:07 - 15:39
Knee Lever Guard Retention	15:39 - 20:51
Half Butterfly Retention Method	20:51 - 27:21
Guard Retention Against a Near Side Underhook	27:21 - 35:34
Guard Retention Against Double Underhooks	35:34

### Volume 08

Sit-Out Half Guard Variations - Guard Retention Against Sit-Out Crossface	0:00 - 13:56
Guard Retention Against Sit-Out Reverse Crossface	13:56 - 20:53
Guard Retention Against Reverse Underhook Sit-Out	20:53


## UNIFYING THE SYSTEM 

### Volume 1

Introduction	0:00 - 5:25
Goals	5:25 - 8:32
Leglocks Rotation: -Outside Heel Hook	8:32 - 14:36
Toehold	14:36 - 19:33
Knee-bar	19:33 - 28:16:00
Spinning the Leg	28:16:00 - 34:12:00
Achilles Lock	34:12:00 - 41:18:00
Switching: -Forward Facing	41:18:00 - 49:24:00
Rear Facing	49:24:00
Hip To Hip	56::05
Legs to Back: -Forward Facing	1:00:12 - 1:05:18
Rear Facing	1:05:18 - 1:10:08
Legs To Kimura - Forward Facing	1:10:08 - 1:14:45
Legs To Kimura - rear Facing	1:14:45 - 1:17:33
Legs To Front Head	1:17:33 - 1:21:48
Legs to Armbar & Triangle	1:21:48

### Volume 2

Back Attack Cross Body Ride	0:00 - 8:40
Back to Juji Gatame: -Juji Transition 1	8:40 - 12:44
Lat Juji	12:44 - 17:41
Juji Transition 2	17:41 - 19:36
Back Triangle	19:36 - 26:22:00
Back to Kimura	26:22:00 - 31:08:00
Crucifix	31:08:00 - 38:42:00
Back to Front Head Lock 1	38:42:00 - 43:50:00
Back to Front Head Lock 2	43:50:00 - 46:26:00
Back to Leg Locks	46:26:00

### Volume 3

Kimura Enhancing the Kimura	0:00 - 5:45
Kimura to Front Head Lock: -Turtle	5:45 - 13:10
Dorsa	13:10 - 21:33
T-Kimura/Crucifix	21:33 - 25:36:00
Bottom Position	25:36:00 - 28:43:00
Kimura to Juji -Dorsal	28:43:00 - 34:54:00
Crucifix/T-Kimura	34:54:00 - 39:30:00
Kimura to Legs -The Scoop	39:30:00 - 45:07:00
Dorsal	45:07:00 - 50:13:00
Elevating	50:13:00

### Volume 4

Kimura to the Back: -T-Kimura (Top)	0:00 - 4:42
T-Kimura (Bottom)	4:42 - 8:51
T-Kimura (Turtle)	8:51 - 13:07
Climbing to the back	13:07 - 16:14
Cross Body Ride	16:14 - 21:40
Dorsal	21:40 - 26:59:00
Kimura to Triangle -Dorsal	26:59:00 - 37:35:00
T-Kimura/Crucifix	37:35:00 - 40:52:00
Bottom Position	40:52:00 - 44:36:00
Omoplata	44:36:00 - 52:39:00
Rotation	52:39:00

### Volume 5

Front Head Lock Katagatame & Guillotine	0:00 - 9:08
Following	9:08 - 15:21
Front Head Lock to Legs -Elevating	15:21 - 21:41
Dorsal	21:41 - 25:59:00
Cradle	25:59:00 - 29:19:00
Front head lock to kimura -Bottom	29:19:00 - 32:01:00
Dorsal	32:01:00 - 35:56:00
Front Head Lock to Arm Locks -Bottom	35:56:00 - 40:42:00
Dorsal	40:42:00 - 44:25:00
Mount	44:25:00 - 49:08:00
Front Head Lock to Triangle -Bottom	49:08:00 - 54:38:00
Dorsal	54:38:00 - 59:45:00
Single leg	59:45:00 - 1:02:43
Seated Guard	1:02:43 - 1:05:12
Front Head Lock to the Back -Go Behinds	1:05:12 - 1:08:30
Cross Body Ride	1:08:30 - 1:15:01
Seated Guard	1:15:01

### Volume 6

Navigating the system -Armbar top	0:00 - 7:10
Armbar Bottom	7:10 - 10:52
Arm locks to legs -top	10:52 - 17:15
Bottom	17:15 - 26:39:00
Spin-Through	26:39:00 - 28:58:00
Arm Locks to the Back -Side Scissor	28:58:00 - 36:42:00
S Mount	36:42:00 - 41:23:00
Arm Locks to Kimura/Omoplata -Bottom	41:23:00 - 46:34:00
Top	46:34:00 - 51:27:00
Arm locks to triangle -ude gatame	51:27:00 - 56:36:00
3/4 Juji Gatame	56:36:00 - 1:03:40
Pulling Out	1:03:40 - 1:07:46
Connected Hands	1:07:46

### Volume 7

Triangles Navigating the System: -Forward Facing	0:00 - 7:04
Rear Facing	7:04 - 11:11
Triangle To Legs -Forward Facing	11:11 - 18:48
Rear Facing	18:48 - 23:06
Triangle to Armlock/Kimura -Omote Sankaku	23:06 - 27:16:00
Reverse Triangle	27:16:00 - 30:06:00
Ankle Sankaku	30:06:00 - 35:34:00
Back Triangle	35:34:00 - 38:32:00
Rear Facing Triangles	38:32:00 - 41:54:00
Te Gatame & Omoplata	41:54:00

### Volume 8

Attacking in 3’s & 4’s Introduction	0:00 - 2:22
Leg Locks	2:22 - 5:23
Back Attack	5:23 - 7:44
Kimura	7:44 - 10:37
Front Head Lock	10:37 - 13:45
Arm Lock	13:45 - 16:54
Triangle	16:54 - 19:08
Training Tips	19:08 - 20:53
Outtro	20:53

##### FASTEST WAY - STANDING 

### Volume 1

Introduction	0:00 - 2:27
Your First Standing Skill: Stance Motion Contact - Finding Your Stance	2:27 - 13:37
Making First Contact - Forehead to Forehead Method	13:37 - 19:34
The Rear Hand Reach Method	19:34 - 22:16
Front Hand Reach Method	22:16 - 24:47
Head Post Method	24:47 - 29:02
Getting 2 Hands on Your Opponent	29:02 - 34:49
The All Important Push Pull Dynamic	34:49 - 39:27
Making Your Opponent Step Where you Want	39:27 - 42:56
Head Position	42:56 - 47:37
Asymmetric Gripping	47:37 - 57:17
Closed Stance (Ai-Yotsu) and Open Stance (Kenka-Yotsu)	57:17 - 1:04:20
Inside Control Vs Outside Control and the First 2 Grips You Must Master	1:04:20

### Volume 2

The 3 Basic Goals of Hand Fighting	0:00 - 4:48
The 2 Goals of Foot Fighting	4:48 - 10:39
The High Single Leg Takedown - Procedure and Mechanics	10:39 - 26:29
Outside Step / Outside Reach	26:29 - 31:40
Outside Step / Inside Reach	31:40 - 36:15
Inside Outside Step	36:15 - 41:14
2 Hands to a Leg	41:14

### Volume 3

Setups For the High Single Leg - Collar and Elbow Setup	0:00 - 7:14
Tricep and Elbow Setup	7:14 - 10:20
Wrist and Elbow Setup	10:20 - 13:16
Cross Wrist Setup	13:16 - 16:57
Elbow Post	16:57 - 20:38
High/Low Setup	20:38 - 25:39
Arm Drag Setup	25:39 - 30:25
Head Post Setup	30:25 - 33:18
Overtie With Inside Elbow	33:18 - 36:32
Underhook Setup	36:32 - 39:05
Shuck Setup	39:05 - 41:18
2 on 1 Setups	41:18

### Volume 4

Single Leg Finishes - The 3 Finishing Positions	0:00 - 3:18
Finishing Low Inside - Dump	3:18 - 10:37
Low Inside (Te-Guruma)	10:37 - 14:28
High Inside - De Ashi Harai	14:28 - 22:05
Modified Dump	22:05 - 24:58
High Outside - De Ashi Harai	24:58 - 27:48
Double Leg Finishes - Head Inside Double Leg	27:48 - 32:33
Head Outside Double Leg	32:33 - 37:20
Sweep Double Leg	37:20 - 40:49
Wrap Around Double Leg	40:49 - 43:01
Understanding the Crucial Importance of the Foot to Foot Dilemma in Our Standing Program	43:01

### Volume 5

Going to Your Knees on a Single Leg - Pros, Cons and Posture	0:00 - 9:26
Getting to a Leg on 2 Knees	9:26 - 13:21
Dealing with the 2 Most Common Problems with Single Legs in Jiu-Jitsu - Dealing with a LImp Leg	13:21 - 20:26
Dealing with a Guillotine	20:26 - 23:25
When the High Single Leg Fails: Switching to the Waist or Underhook - Getting to the Waist or Underhook	23:25 - 26:04
Knee Pick	26:04 - 31:34
Body Lock Uki Goshi	31:34 - 35:24
High Lock Uki Goshi	35:24 - 37:37
Body Lock Kosoto Gake	37:37 - 46:47
Pinch Headlock	46:47

### Volume 6

Positional Attacks to the Back - Snapdown - Ear to Ear Snap	0:00 - 6:32
Forehead to Forehead Snap	6:32 - 9:21
Overtie Snap	9:21 - 16:11
Duck Unders - Wrist Drop Duck Under	16:11 - 19:41
Misdirection Duck Under	19:41 - 22:39
Arm Drag - Wrist to Inside Drag	22:39 - 28:03
Wrist and Overdrag	28:03 - 30:08
Snap and Drag	30:08 - 34:33
Throw By - Underhook Throw By	34:33 - 39:48
Double Underhook Throw By	39:48 - 41:50
Sliding Past a Whizzer	41:50 - 45:11
Control and Takedowns from the Back	45:11

### Volume 7

Defensive Skills - Defensive Stance and Hand Fighting	0:00 - 5:40
Down Blocking	5:40 - 10:14
Sprawling on a Single Leg	10:14 - 16:45
Sprawling on a Double Leg	16:45 - 22:54
Defensive Whizzer: Keeping Your Opponent in Front of You	22:54 - 27:52
Whizzer Vs Single Leg	27:52 - 32:25
Summary of the Skills and Procedures of this Video: A Short Guide to Effectiveness in the Standing Position - 4 Key Skills in Standing Position - 1. Controlling the Initial Contact	32:25 - 41:11
2. Single Leg Takedown as Foundational Skill	41:11 - 46:36
3. Positional Attacks	46:36 - 50:02
4. Defensive Skills	50:02

### Volume 8

Demonstration of Procedure - Step 1. Control the Initial Contact	0:00 - 4:36
Getting to Grip Asymmetry	4:36 - 8:10
Hand Fighting With Purpose	8:10 - 10:58
Generate Foot to Foot Positioning With a Connection Hand	10:58 - 15:55
Step 2. Single Leg Game - Posture is as Important as Position	15:55 - 19:43
Single Leg to High Leg (Inside/Outside)	19:43 - 22:46
Transition to Double Leg	22:46 - 25:07
Transition to Waist or Underhook	25:07 - 28:54
Step 3. When Foot to Foot Positioning is Impossible, Play the Positional Game - When the Feet are Back: Snaps and Drags. When the Feet are Forward: Ducks and Throw-bys. Too Much Resistance? Back to Single Leg	28:54 - 32:06
Scoring From Rear Bodylock and 4 Point	32:06 - 34:40
Step 4. Defense and Defensive Scoring - Imposing Barriers	34:40 - 36:14
Down Block	36:14 - 37:38
Sprawl	37:38 - 39:38
Whizzer - Whizzer for Back Defense	39:38

##### FASTEST WAY - GUARD PASSING 

### Volume 1

Building A Conceptual Understanding Of Guard Passing; Some Key Ideas - Guard Passing, Theory Vs Practice	0:00 - 6:02
The Single Greatest Advantage The Guard Passer Possesses	6:02 - 17:04
Knowing What You're Fighting Against Is Half Of The Battle	17:04 - 22:02
Passing A Supine Guard - Forcing A Seated Opponent Into A Supine Position	22:02 - 28:00
Grip Denial Vs A Supine Opponent	28:00 - 34:48
Approaching A Supine Guard And Seizing The Initiative; Stance Contact Angle	34:48 - 40:48
Understanding The Value Of Angle And The Theory Of Camping	40:48

### Volume 2

Toreando Passing - Hip And Knee Method	0:00 - 7:42
North South Camping	7:42 - 12:11
Side To Side Toreando	12:11 - 17:52
Side To Side With Reverse Scoop Pass	17:52 - 23:19
Toreando Misdirections And Feints	23:19 - 29:08
Cross Over Step	29:08 - 37:23
Crescent Step	37:23 - 40:42
Ankle And Knee Post	40:42 - 43:13
Reverse Toreando	43:13 - 50:15
Reverse Toreando To Reverse Scoop Pass	50:15 - 53:39
Combining Toreando With Knee Cut Pass	53:39

### Volume 3

Hand Block Pass - Key Conceptual Ideas To The Hand Block Pass	0:00 - 3:53
Using The Hand Block Guard Pass	3:53 - 8:21
Combining A Hand Block With Reverse Scoop Pass	8:21 - 10:55
Ankle Control Variation	10:55 - 13:15
High Leg Guard Pass - Classic High Leg Pass	13:15 - 17:28
Combining Knee Cut Pass With High Leg	17:28 - 20:04
High Leg With Side Body Lock Finish	20:04 - 25:52
Camping Inside A Guard; Tripod Method	25:52 - 30:27
Butterfly Shuck Into Side Body Lock Pass	30:27 - 33:47
One In One Out Into Side Body Lock	33:47

### Volume 4

Leg Drag Passing - Understanding The Value Of Leg Drags	0:00 - 2:11
The Problem With No Gi Leg Drags	2:11 - 4:19
Solutions	4:19 - 12:28
Passing With No Gi Leg Drag	12:28 - 16:34
Scoop Passing / Reverse Scoop Passing - When It Works And When It Doesn't	16:34 - 23:08
Passing From The Plow Position	23:08 - 26:29
Passing From Calf Control	26:29 - 31:33
Reverse Scoop Pass	31:33

### Volume 5

Passing A Seated Guard - Approaching A Seated Guard - Grip Denial	0:00 - 11:20
Attacking The Head	11:20 - 14:07
Knee Cut Passes - Hand To Hand Method	14:07 - 17:40
Back Collar Tie Method	17:40 - 20:08
Front Hand Collar Tie Method	20:08 - 22:29
Kneeling Knee Cut Method	22:29 - 25:22
Body Lock Guard Passing - Building A Strong Bodylock Stance	25:22 - 30:29
Understanding The 6 Most Advantageous Positions For Bodylocking	30:29 - 50:04
Working The 2 On 1 Bodylock	50:04 - 55:07
Working The Cross Shin Pin	55:07 - 1:00:17
Working The Inside Knee Pin	1:00:17 - 1:03:14
Working The Side Body Lock	1:03:14 - 1:07:38
Working The Scissor Legs	1:07:38 - 1:17:53
Working The Inside Shoe Lace	1:17:53 - 1:22:52
Training Body Lock Skills Ft Placido	1:22:52

### Volume 6

Tripod Passing - Essential Features Of Tripod Pass	0:00 - 8:48
The Vaulting Pass	8:48 - 12:34
Tripod Knee Cut Pass	12:34 - 17:44
Tripod Step Over	17:44 - 23:15
Tripod Switching Step Over Pass	23:15 - 27:28
The Pop Up Pass - Hip And Knee Pop Up	27:28 - 34:32
Collar And Knee Pop Up	34:32 - 36:49
2 Knees To Side Body Lock	36:49 - 39:55
Half Guard Passing - Half Guard Passing Is The Single Best Method Of Passing Guard	39:55 - 52:25
Best Entries Into Half Guard - The Tight Waist Method	52:25 - 57:17
Body Lock Method	57:17 - 1:03:48
The High Step Method	1:03:48 - 1:08:29
Toreando Method	1:08:29

### Volume 7

Overcoming A Knee Shield	0:00 - 10:04
Shutting Down Your Opponents Game From Bottom Half Guard	10:04 - 20:24
Understanding Half Guard Control; Ankle Vs Knee	20:24 - 27:06
Passing With Far Side Underhook - Passing With Top Head And Arm	27:06 - 32:24
Near Side Head Block	32:24 - 39:09
Near Side Underhook	39:09 - 46:04
Double Underhook	46:04 - 51:09
Passing With No Underhooks	51:09

### Volume 8

Sit Out Half Guard Passing - Building A Strong Sit Out Stance	0:00 - 14:03
Passing From The Sit Out	14:03 - 24:53
Passing Half Butterfly Guard - Setting A Strong Stance In Your Opponents Half Butterfly Guard	24:53 - 31:43
Passing Half Butterfly Guard; Near Side Underhook	31:43 - 35:32
Adding Your Own Butterfly Hook	35:32 - 38:30
Passing Half Butterfly With Top Head And Arm	38:30 - 41:57
Passing Half Butterfly Guard With A Crossface	41:57


## Gordon Ryan - Attacking From Half Guard

### Volume 1
Overview of the Half Guard Series	4:52 - 7:23
Advantages and Disadvantages to Butterfly Half Guard	7:23 - 11:36
Advantages and Disadvantages to (Low/High) Knee Shield	11:36 - 16:19
The General Battle for Inside Position	16:19 - 23:06
Denying Head Control	23:06 - 27:24
Framing Effectively	27:24 - 31:37
The Centerline Shift Dilemma	31:37 - 37:59
Moving From a Cross Shoulder Post to an Underhook	37:59 - 42:57
Basic Switch to Sumi Gaeshi	42:57 - 50:18
Entering the Near Leg Irimi Ashi	50:18 - 57:23
Importance of Switching Legs	57:23 - 1:04:49
Underhook to Far Leg Irimi Ashi	1:04:49 - 1:08:47
Underhook to Cross Ashi	1:08:47 - 1:15:31
Introduction to a Scoop Grip	1:15:31 - 1:20:07
Using a Scoop Grip to Enter X Guard	1:20:07 - 1:23:22
Using a Scoop to Enter Near Leg Cross Ashi	1:23:22 - 1:27:45
Using a Scoop to Spin to Far Leg Cross Ashi	1:27:45 +

### Volume 2

Pinch Headlock to Shoulder Crunch	6:14 - 11:04
Pinch Headlock Entering the Legs	11:04 - 13:36
An Overhook Sumi Gaeshi	13:36 - 16:32
Failed Overhook Sumi Gaeshi Enter the Legs	16:32 - 23:33
A Special Gift	23:33 - 25:08
Stalling From a Butterfly Half Guard	25:08 - 32:07
Butterfly Half to Arm Drag	32:07 - 35:59
Failed Arm Drag To Knee Lever	35:59 - 43:51
Failed Knee Lever Switch to Sumi Gaeshi	43:51 - 51:56
Failed Knee Lever Re-drag	51:56 - 55:59
Failed Arm Drag Enter the Legs	55:59 - 59:39
Entering Kimuras From Butterfly Half Guard	59:39 - 1:03:20
Putting it Behind the Back	1:03:20 - 1:11:17
Counter to a Hip Switch	1:11:17 - 1:16:02
Separating the Hands	1:16:02 - 1:21:40
Failed Kimura Enter the Legs	1:21:40 - 1:27:33
Closing Out the Kimura Series (Butterfly Half)	1:27:33 +

### Volume 3

Introduction to the Lower Leg Shift	6:36 - 16:08
Transitioning From Half Guard to a Lower Leg Shift	16:08 - 19:06
Basic Sweep From Lower Leg Shift	19:06 - 23:42
Failed Sweep Back Take	23:42 - 27:19
Coming Up From a Lower Leg Shift	27:19 - 31:31
Options From Underhook Versus Whizzer	31:31 - 34:40
Countering the Whizzer with the Roll	34:40 - 40:04
Partner Counters The Roll, Take the Back	40:04 - 43:53
Opponent Wins the Battle for Knee Position Enter Hiza Gatame	43:53 - 46:02
Countering a Backstep From a Lower Leg Shift	46:02 - 55:31
Reiterating Pinch Headlocks and Moving Back to a Butterfly Half	55:31 - 1:00:15
Opponent Gets an Underhook Switch Back to Butterfly Half	1:00:15 - 1:02:16
Opponent Gets Underhook Move to Triangle	1:02:16 - 1:10:53
Opponent Crushes Knees Together Re-guard With High Knee Shield	1:10:53 +

### Volume 4

Key Differences Between Locking a Kimura (Butterfly Half) Versus a Knee Shield	6:57 - 16:20
Far Arm Drag	16:20 - 22:37
Opponent Counters Far Arm Drag with Darce, Enter Knee Lever	22:37 - 27:19
Partner Postures Out of Arm Drag Entering Far Leg Outside Ashi	27:19 - 33:24
Extreme Staller Pumping the Legs to Create Opportunity	33:24 +

### Volume 5

Opponent Puts Far Knee Down Enter the Legs	8:21 - 14:39
Opponent Puts Near Knee Down, Force the Far Knee Down	14:39 - 16:47
Opponent Puts Near Knee Down, Enter the Near Leg	16:47 - 21:07
Near Knee Down Enter Ashi on the Far Leg	21:07 - 25:10
Opponent Puts Near Knee Down, Exit Through the Back	25:10 - 28:55
Failed Back Attack, Chase the Legs	28:55 - 32:25
Countering a Backstep	32:25 - 37:47
Sweeping an Opponent Who Steps Over the head	37:47 - 41:51
Entering Deep Half Guard From Outside Foot Position (Far Knee Down)	41:51 +

### Volume 6

Heisting From a Scorpion	4:55 - 8:56
Using a Scorpion to Pass the Leg Across	8:56 - 13:46
Using a Scoop Grip to Sweep Backwards	13:46 - 18:10
Countering Opponent’s Underhook	18:10 - 24:39
Using an Overwrap to Sweep Forward	24:39 - 29:38
The Scorpion/Lower Leg Shift Dilemma	29:38 - 35:10
Locking a Scorpion When Opponent Puts Near Knee Down	35:10 - 37:28
Returning Opponent’s Far Leg to the Floor	37:28 - 41:14
Countering a Knee Slip with Hiza Gatame	41:14 - 46:23
Countering a Far Hip Knee Slip With an Inside Elbow	46:23 - 49:54
The Waiter Sweep Dilemma	49:54 - 59:17
Taking the Back When Opponent Steps Over the Head	59:17 - 1:08:56
Back Take/Sweep Dilemma	1:08:56 - 1:12:48
Pommeling a Leg Back Inside From Outside foot Position	1:12:48
Outro	1:16:0 +

### Volume 7

Half Guard Rolling - Drew	5:03 - 10:02
Half Guard Rolling - Alex	10:02 - 15:03
Half Guard Rolling - Sean	15:03 +

### Volume 8

Rolling Commentary - Drew	29:54 - 59:34
Rolling Commentary - Alex	59:34 - 1:23:55
Rolling Commentary - Sean	1:23:55 +


## SHOOT TO KILL - Wrestle, Scramble, submitAction

### Volume 1

Introduction	0:00 - 3:13
Basic Concepts of Posture & Momentum	3:13 - 7:45
Pushing, Pulling & Circular Momentum	7:45 - 10:44
Creating Opportunity- Snapping, Dragging, & Faking	10:44 - 18:32
Harassing the Legs	18:32 - 21:05
Inside and Outside Position	21:05 - 25:31
Getting Behind & Attacking The Back - Intro	25:31 - 26:56
Arm drags	26:56 - 36:45
Duck Unders	36:45 - 45:28

### Volume 2

Single leg - Kosoto limp arm	0:00 - 4:09
Kosoto Step Around	4:09 - 6:33
Peak Out	6:33 - 8:28
High single leg	8:28 - 11:04
Canuto Variation	11:04 - 13:42
Down Blocking	13:42 - 15:43
Throw bys	15:43 - 22:21
Russians	22:21 - 27:12
Double Leg - Turning the corner	27:12 - 28:12
Kosoto	28:12 - 29:52
Front Head Lock Defense	29:52 - 32:34
Rear Body Lock Offense & Mat Returns	32:34 - 34:43
De Ashi Barai	34:43 - 37:28
The Momentum Breakdown	37:28 - 39:38
Cross body ride	39:38 - 43:58
Ashi Otoshi	43:58 - 47:16
50/50	47:16 - 49:30
Tripod Offense	49:30 - 55:31

### Volume 3

"Attacking Legs - Ashi Garami Pulls - Intro"	0:00 - 2:00
"Front Headlock"	2:00 - 3:20
Front Headlock To Cradle	3:20 - 5:24
"Defense Single Leg"	5:24 - 7:02
Rear Body Lock	7:02 - 9:12
Double Leg Ashi	9:12 - 11:08
"Basic Single Leg"	11:08 - 13:36
"High Single Leg Roll"	13:36 - 15:51
Single To Double	15:51 - 18:32
Overhook	18:32 - 21:15
"Underhook Entry"	21:15 - 23:44
"Defensive Pull"	23:44 - 26:16
Low Single	26:16 - 31:43
"Ashi Slide"	31:43 - 33:56
"Cross Ashi Slide"	33:56 - 35:38
"Imanari Rolls"	35:38 - 39:57
Cross Imanari To 50/50	39:57 - 41:54

### Volume 4

"Victor’s Roll"	0:00 - 3:07
"Collar Tie"	3:07 - 4:55
Overhook	4:55 - 6:38
Single Leg Defense	6:38 - 8:13
Double Leg Defense	8:13 - 10:54
Flanking	10:54 - 13:04
Yoko Sumi Gaeshi - Single Leg Defense (Head Inside)	13:04 - 15:24
Rear Body Lock Defense	15:24 - 18:17
Front Headlock	18:17 - 19:45
Single Leg Defense (Head Outside)	19:45 - 21:38
"Kani Basami - Basics"	21:38 - 25:46
Overtie	25:46 - 27:01
Double Leg Kani Basami	27:01 - 28:42
Underhook	28:42 - 29:22
Front Headlock	29:22 - 30:56
Cutback	30:56 - 33:05
Half Kani	33:05 - 35:25
Ankle Pick	35:25 - 37:57

### Volume 5

Attacking Upper Body - Front Headlock - Setups	0:00 - 7:47
Standing Guillotines	7:47 - 16:46
Cradle	16:46 - 20:40
4 Point	20:40 - 22:55
Sumi Gaeshi	22:55 - 27:39
"Tawara Gaeshi"	27:39 - 30:29
Front Headlock To Back	30:29 - 37:41
Falling Juji Gatame	37:41 - 40:59
Flying Triangles	40:59 - 44:57

### Volume 6

Kata Gatame - Cradle	0:00 - 3:02
4 Point	3:02 - 4:58
Kimura - Head Inside Single Leg	4:58 - 9:26
Head Outside Single Leg	9:26 - 11:51
Rear Body Lock	11:51 - 15:35
Tai Otoshi	15:35 - 17:36
"Soto Gari"	17:36 - 20:23
Knees	20:23 - 22:49
Triangles - Yoko Sankaku	22:49 - 29:13
Far Wrist	29:13 - 32:40
Head Inside SIngle Leg	32:40 - 35:28

### Volume 7

"Scrambles - Introduction"	0:00 - 1:32
"Grandby Roll Rolling Mechanics"	1:32 - 8:54
From The Knees	8:54 - 13:42
4 Point	13:42 - 16:38
Standing	16:38 - 19:22
"Maki Komi Roll Knees"	19:22 - 22:53
4 Point	22:53 - 24:45
Standing	24:45 - 28:04
"Funk Roll - Head Inside Single Leg Standing"	28:04 - 30:37
Double Leg	30:37 - 32:22
Head Inside Single Leg Knees	32:22 - 34:41
Double Leg Knees	34:41 - 36:42
Floor	36:42 - 40:52
No Spooning Allowed	40:52 - 45:30

### Volume 8

Live Drills / Rolling - Intro	0:00 - 3:56
Front, Head, And Arm	3:56 - 11:20
Head Inside Single	11:20 - 17:05
Turtle Position	17:05 - 22:34
Outro	22:34 - 24:59












##### 1 Insights

Introduction 	0
The Central Problem of Guard Passing 	6:36
Four Conceptual Solutions: The Push Pull Asymmetry 	12:43
The Push/Curl Asymmetry 	19:38 
The Flexion/Extension Asymmetry 	24:14
Leg vs Leg Passing 	27:34
The Four Scenarios You Must Be Effective In 	29:58
Five Key Insights That Make a Real Difference to Your Passing Performance - The Three Nemesis of Guard Passing 	32:46
Penetrating to the Torso 	44:48
Completing a Pass: Hips First/Head Second Principle 	51:54
Be Ready to Take the Back at all Times 	59:27
The Foundation of All Guard Play: Control Resides Between The Knees 	1:12:26

##### 2 Opening Closed Guard

Opening a Closed Guard - Negation First 	0
Opening a Closed Guard: Squad Philosophy 	13:16
Analysis of the Squad Philosophy 	23:08
The 3 Best Ways to Open a Closed Guard Nogi - The Reach Back Method 	30:49
Two on One Knee Method 	36:23
Knee Post 	40:19
Dealing with Lower Body Grips: Double Outside Grips 	48:22
Dealing with Lower Body Grips Part 2 	59:24
Dealing with Lower Body Grips: Single Inside Grip 	1:06:13
Opening a Closed Guard: Putting It All Together 	1:17:49

##### 3 Passing Supine Guard

Passing a Supine Guard - Toreando Pass Series - Footwork Skills 	0
crescent step 	1:29
side step 	3:03
cross step 	4:13
front step 	6:44
back step 	8:31
jab step 	9:50
fake step 	11:35
Putting it all together 	13:04
The First Stage of Toreando Passing: Approaching Your Opponent - Reading Your Opponents Body 	14:08
Getting a Strong Start in Toreando Passing: Grip Asymmetry 	20:01
Essential Grip Fighting - Double down V Grips 	29:23
Double Sole Grip 	32:10
Double Up V Grip 	34:32
Knee and Shin 	38:01
Inside Knee Post 	41:09
Inverted V Grip 	43:52
Inverted Cross Grip 	46:38
Two on One Knee and Ankle 	48:34
The King of All Nogi Toreando Passing Positons: The Hip & Knee Post 	51:10
The Queen of All Nogi Toreando Passing Positions: Toes to Mat (AKA The Plow) 	59:18
Getting Down to Business with Toreando Passing - Toreando Throw By 	1:02:46
Misdirectional Throw By 	1:08:21
Toreando Throw By To a Hip & Knee Post 	1:10:47
Toreando Throw By to Toes to Floor 	1:12:59
Toreando Throw By to Leg Drag 	1:17:25

##### 4 Hip Knee Post

Hip & Knee Post Series - Running the Circle 	0
Running the Circle Part 2 	8:31
Step Back Knee Cut 	13:30
Step Back Inside Knee 	20:17
Spinning Toreando - Mechanics 	27:06
Tight Waist Finish 	31:38
Hip Block Method 	36:11
Hip Block Method Part 2 	39:22
Reverse Grip Method 	43:30
Spinning Toreando to the Back 	47:21
Spinning Toreando to Leg Drag 	51:08
Side to Side Pressure and Toreando Passing - Arm Swim 	58:47
The Foot Step 	1:04:52
Cross Shin Pin - Hip & Knee Post 	1:12:42
Front Step/Back Step 	1:18:07
Overcoming the Problem of Entanglement 	1:23:18
The Relationship Between Toreando & Knee Cut 	1:29:16

##### 5 Leg Pommel

The Floating Leg Pommel Series - Why Pommel Passing? 	0
Getting to the Start Position 	5:54
Inside Pommel Pass 	9:55
Outside Pommel Pass 	14:59
Cross Knee Pommel 	20:17
Cross Foot Pommel 	22:48
Front Pommel 	25:14
Pommel to knee cut 	27:52
Finishing a Pommel Pass: Alternate Method 	34:27
Body Lock Guard Passing - Why Body Locks? 	37:49
Front Body Locks vs Side Body Locks 	43:05
Getting to a Front Body Lock - Creating Waist Exposure 	45:18
Inside & Outside Penetration 	52:54
Head Position & Pull 	1:00:29
Arm Position/Hand Grip 	1:08:38
2 Philosophies of Body Lock Guard Passing 	1:16:57
The Hierarchy of Body Lock Positions 	1:26:00
The Two Most Important Body Lock Passing Methods: The Step Over Method 	1:33:30
The Knee Drive Method 	1:44:19

##### 6 Body Lock Passing

Passing with a front body lock: Double Shin Method 	0
Leg Scissor Method 	5:48
Shoelace Method 	14:05
Knee Cut Method 	20:50
Kick Out Method 	27:48
High Hip Scissor Method 	34:52
High Hip Scissor From Double Shin 	45:00
Maximizing the Potential of Your Body Lock Passes 	53:34
The Single Most Common Error in Body Lock Guard Passing and How to Avoid It 	1:00:57
My Favorite Body Lock Passing Method: Lumbar Lock To High Lock 	1:11:26
Countering a Forward Shift 	1:22:50
The Relationship Between a Tight Waist and a Bodylock 	1:29:03
Switching Tight Waist 	1:39:42
Side Body Lock: Entering a Side Body Lock 	1:45:51
Side Body Lock: Leg Riding 	1:54:31
Passing with a Side Body Lock 	2:06:09

##### 7 Half Guard Passing

Half Guard Passing - Why Half Guard Passing? 	0
First Problem of Half Guard: Clearing a Knee Shield - Shifting to Centerline 	8:35
Front Step/Back Step Method 	17:25
Entries Into Half Guard Passing - Tight Waist Entry 	25:29
Body Lock Entry 	29:43
Floating Leg Pommel Entry 	33:31
Hip & Knee Post Entry 	35:43
Toes to Mat Entry 	38:20
The Fundamental Start Position for Half Guard Passing 	46:01
The Two King Knee Positions 	59:38
The Four Step Half Guard Passing Sequence 	1:06:12

##### 8 Half Guard Passing

Half Guard Passing Methods: Top Head & Arm 	0
Far Underhook & Head Block 	6:14
Double Underhook Passing 	12:50
Cross Face & Reverse Cross Face 	21:52
Half Katagatame 	31:28
Countering a Power Prop From Half Guard 	36:13
Passing Half Butterfly Guard: Near Side Underhook: Grape Vine Method 	41:04
Near Side Underhook: Hand Post Method 	47:43
Near Side Underhook: Butterfly Hook Method 	50:58
Cross Face Half Butterfly Guard Pass 	54:46

