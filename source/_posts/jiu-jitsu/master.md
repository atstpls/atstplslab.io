---
title: master
date: 2023-04-08 12:56:24
tags: [jiujitsu]
category: [training, tactics]

---

<br>

##### OVERVIEW

```mermaid
flowchart LR

    subgraph DEFENDING [ <a href='#DEFEND' style='font-weight:bold;color:#fff'>DEFEND</a> ]
        subgraph TRAPPED [ ]
            RMD(<a style='color: #d61303' href='#REAR-MOUNTED'>REAR\nMOUNTED</a>)
            MD(<a style='color: #d61303' href='#MOUNTED'>MOUNTED</a>)
            SMD(<a style='color: #d61303' href='#SIDE-MOUNTED'>SIDE\nMOUNTED</a>)
            IG(<a style='color: #d61303' href='#IN-GUARD'>IN GUARD</a>)
        end
    end
    classDef Defend stroke:#d61303,fill:#d61303,opacity:0.15,stroke-width:5px
    class DEFENDING Defend
    classDef TrapPos fill:#34495e,opacity:0.2
    class TRAPPED TrapPos

    subgraph ESCAPES [  ]
        subgraph MOVES [ ]
            EP{<a style='color: #e36107' href='#ESCAPE-PROTOCOL'>ESCAPE\nPROTOCOL</a>}
            SS{<a style='color: #e36107' href='#CAT-WEDGE'>C.A.T.\nWEDGE</a>}
            KP{<a style='color: #e36107' href='#KNEE-POST'>KNEE\nPOST</a>}
            LA{<a style='color: #e36107' href='#LIFT-ACTION'>LIFT\nACTION</a>}
        end
    end
     style ESCAPES stroke:#e36107,fill:#e36107,opacity:0.15,stroke-width:5px,stroke-dasharray 5 5
     style MOVES fill:#34495e,opacity:0.2

    NC[ \n \n \nNEGATE\nCONNECTIONS \n \n \n ]


    subgraph NEGATE [ <a href='#NEGATE' style='font-weight:bold;color:#fff'>NEGATE</a> ]
        subgraph STAGING [  ]
           DK[<a style='color:#6b6a1b' href='#DOUBLE-KNEE'> \n \nDOUBLE\nKNEE \n \n</a>]    
           HQP[<a style='color:#6b6a1b' href='#HQ-POSITION'> \n \nHQ\nPOSITION \n \n</a>]
           KD[<a style='color:#6b6a1b' href='#KNEE-DROP'> \n \nKNEE\nDROP \n \n</a>]
        end
    end
     style NEGATE stroke:#e0dc01,fill:#e0dc01,opacity:0.15,stroke-width:5px,stroke-dasharray 5 5
     style STAGING fill:#34495e,opacity:0.2

    subgraph ADVANTAGE [ <a href='#ADVANTAGE' style='font-weight:bold;color:#fff'>ADVANTAGE</a> ]
        subgraph P1[ ]
            DHP(<a style='color: #02fde2' href='#DOUBLE-HEEL-PASS'>DOUBLE HEEL</a>)
            DUP(<a style='color: #02fde2' href='#DOUBLE-UNDER-PASS'>DOUBLE UNDER</a>)
        end
        subgraph P2[ ]
            KCP(<a style='color: #02fde2' href='#KNEE-CUT-PASS'>KNEE CUT</a>)
            OUP(<a style='color: #02fde2' href='#OVER-UNDER-PASS'>OVER UNDER</a>)
            LSP(<a style='color: #02fde2' href='#LONG-STEP-PASS'>LONG STEP</a>)
            SMP(<a style='color: #02fde2' href='#SMASH-PASS'>SMASH</a>)
        end
        subgraph P3[ ]
            LDP(<a style='color: #02fde2' href='#LEG-DRAG-PASS'>LEG DRAG</a>)
            TP(<a style='color: #02fde2' href='#TOREANDO-PASS'>TOREANDO</a>)
        end
    end
     style ADVANTAGE stroke:#02fde2,fill:#02ede2,opacity:0.1,stroke-width:5px,stroke-dasharray: 5 5
     style P1 fill:#34495e,opacity:0.2
     style P2 fill:#34495e,opacity:0.2
     style P3 fill:#34495e,opacity:0.2

    subgraph STRONG [ <a href='#CONTROL' style='font-weight:bold;color:#fff'>COMPLETION</a> ]
        subgraph CP [ ]
            RM(<a href='#REAR-MOUNT'>REAR MOUNT</a>)
            M(<a href='#MOUNT'>MOUNT</a>)
            SM(<a href='#SIDE-MOUNT'>SIDE MOUNT</a>)
            CG(<a href='#CLOSED-GUARD'>CLOSED GUARD</a>)
        end
    end
     style STRONG stroke:#0387e8,fill:#0387e8,opacity:0.2,stroke-width:5px,stroke-dasharray 5 5
     style CP fill:#34495e,opacity:0.2

    MD -.-> LA
    SMD -.-> KP
    IG -.-> SS
    RMD -.-> EP
    EP ---> NC
    SS ---> NC
    KP ---> NC
    LA ---> NC
    NC -.-> HQP
    NC -.-> KD
    NC -.-> DK
    HQP -.-> KCP
    HQP -.-> OUP
    HQP -.-> LSP
    HQP -.-> SMP
    KD -.-> LDP
    KD -.-> TP
    DK -.-> DHP
    DK -.-> DUP

    LN[ \n \n \nLEGS\nNEGATED\n \n \n \n ]
     style LN stroke:#0387e8,stroke-width:5px,stroke-dasharray: 5 5


    classDef opponentPosition stroke:#d61303,stroke-width:2px
    class RMD,MD,SMD,IG opponentPosition

    classDef escapeAction stroke:#e36107,stroke-width:2px
    class LA,KP,SS,EP escapeAction

    DHP --> LN
    DUP --> LN
    KCP --> LN
    OUP --> LN
    LSP --> LN
    SMP --> LN
    LDP --> LN
    TP  --> LN
    LN -.-> RM
    LN -.-> M 
    LN -.-> SM 
    LN -.-> CG

     style NC stroke:#6b6a1b,stroke-width:5px,stroke-dasharray: 5 5
     style HQP stroke:#6b6a1b,stroke-width:3px
     style DK stroke:#6b6a1b,stroke-width:3px
     style KD stroke:#6b6a1b,stroke-width:3px
     style DHP stroke:#02fde2,stroke-width:2px
     style DUP stroke:#02fde2,stroke-width:2px
     style KCP stroke:#02fde2,stroke-width:2px
     style OUP stroke:#02fde2,stroke-width:2px
     style LSP stroke:#02fde2,stroke-width:2px
     style SMP stroke:#02fde2,stroke-width:2px
     style LDP stroke:#02fde2,stroke-width:2px
     style TP stroke:#02fde2,stroke-width:2px

    linkStyle 0,1,2,3,4,5,6,7 stroke:orange
    linkStyle 8,9,10 stroke:yellow
    linkStyle 11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26 stroke:#02fde2
    linkStyle 27,28,29,30 stroke:blue



```


<br><br>

<br><br>

##### GUARD PASSING


```mermaid
flowchart LR
    subgraph INIT [ ]
        subgraph OPPPOS [ OPP POSITION ]
            SUP[<a style='color:#e0dc01'>SUPINE</a>] 
            SUPNA[<a style='color:#e0dc01'>SUPINE NO ANGLE</a>] 
            SEA[<a style='color:#e0dc01'>SEATED</a>] 
        end

        subgraph OPPPOS2 [ OPP POSITION ]
            DRG(<a style="color:#f78702" href='#DE-LA-RIVA'>DE LA RIVA GUARDS</a>)
            SPG(<a style="color:#f78702" href='#SPIDER'>SPIDER GUARDS</a>)
            LAG(<a style="color:#f78702" href='#LASSO'>LASSO GUARDS</a>)
            ASHG(<a style="color:#f78702" href='#ASHI-GARAMI'>ASHI GARAMI</a>)
        end
    end

    SUP --> TVP
    SUPNA --> FPP
    SEA --> BLP

    subgraph NEGATE [ <a href='#NEGATE' style='font-weight:bold;color:#fff'>NEGATE</a> ]

        subgraph STAGING [  ]
            HGP[<a style='color:#6b6a1b' href='#HALF-GUARD-POSITION'>HALF\nGUARD</a>] 
            HQP[<a style='color:#6b6a1b' href='#HQ-POSITION'>HQ\nPOSITION</a>]
            DK[<a style='color:#6b6a1b' href='#DOUBLE-KNEE'>DOUBLE\nKNEE</a>]    
            KD[<a style='color:#6b6a1b' href='#KNEE-DROP'>KNEE\nDROP</a>]
            OAP[<a style='color:#6b6a1b' href='#OUTSIDE-ADVANTAGE'>OUTSIDE\nADVANTAGE</a>]
        end
        
        subgraph HGS [ ]
            TVP[<a style='color:#6b6a1b' href='#DOUBLE-KNEE'>TOREANDO PASSES</a>] 
            FPP[<a style='color:#6b6a1b' href='#DOUBLE-KNEE'>FLOATING PASSES</a>] 
            BLP[<a style='color:#6b6a1b' href='#DOUBLE-KNEE'>BODYLOCK PASSES</a>] 
        end
        
        subgraph ADV[ ]
            KI[<a style='color:#6b6a1b' href='#KNEE-INSIDE'>KNEE INSIDE</a>]
            LP[<a style='color:#6b6a1b' href='#LEG-POST'>LEG POST</a>]
            BS[<a style='color:#6b6a1b' href='#BACKSTEP'>BACKSTEP</a>]
            KDR(<a style='color:#6b6a1b' href='#KNEE-DRIVE'>KNEE DRIVE</a>)
            THRBY(<a style='color:#6b6a1b' href='#THROW-BY'>THROW-BY</a>)
        end            
    end
    style NEGATE stroke:#e0dc01,fill:#e0dc01,opacity:0.15,stroke-width:1px,stroke-dasharray 5 5
    style STAGING fill:#34495e,opacity:0.2

    classDef NegateAction stroke:#e0dc01,stroke-width:1px,stroke-dasharray: 5 5
    class TVP,FPP,BLP,KI,LP,BS,KDR,THRBY NegateAction

    classDef NegatePosition stroke:#e0dc01,stroke-width:1px
    class HGP,HQP,DK,KD,OAP NegatePosition


    HQP --> KCP(<a style="color:#34495e" href='#KNEE-CUT-PASS'>KNEE-CUT PASS</a>)
    HQP --> OUP(<a style="color:#34495e" href='#OVER-UNDER-PASS'>OVER-UNDER PASS</a>)
    HQP --> LSP(<a style="color:#34495e" href='#LONG-STEP-PASS'>LONG-STEP PASS</a>)
    HQP --> SMP(<a style="color:#34495e" href='#SMASH-PASS'>SMASH PASS</a>)

    subgraph ADVANTAGE [ <a href='#ADVANTAGE' style='font-weight:bold;color:#fff'>ADVANTAGE</a> ]
        PLOW[<a href='#PLOW-POSITION'>PLOW</a>] 
        HGADV[<a href='#HALF-GUARD-PASSING'>HALF\nGUARD</a>] 
        subgraph P1[ ]
            DHP(<a style='color: #02fde2' href='#DOUBLE-HEEL-PASS'>DOUBLE HEEL</a>)
            DUP(<a style='color: #02fde2' href='#DOUBLE-UNDER-PASS'>DOUBLE UNDER</a>)
        end
        subgraph P2[ ]
            KCP(<a style='color: #02fde2' href='#KNEE-CUT-PASS'>KNEE CUT</a>)
            OUP(<a style='color: #02fde2' href='#OVER-UNDER-PASS'>OVER UNDER</a>)
            LSP(<a style='color: #02fde2' href='#LONG-STEP-PASS'>LONG STEP</a>)
            SMP(<a style='color: #02fde2' href='#SMASH-PASS'>SMASH</a>)
        end
        subgraph P3[ ]
            LDP(<a style='color: #02fde2' href='#LEG-DRAG-PASS'>LEG DRAG</a>)
            TP(<a style='color: #02fde2' href='#TOREANDO-PASS'>TOREANDO</a>)
        end

    end
    HGP --> HGADV
    HGADV --> LN


     style ADVANTAGE stroke:#02fde2,fill:#02ede2,opacity:0.1,stroke-width:1px,stroke-dasharray: 5 5
     style P1 fill:#34495e,opacity:0.2
     style P2 fill:#34495e,opacity:0.2
     style P3 fill:#34495e,opacity:0.2

    SPG -.-> KI
    SPG -.-> THRBY
    DRG -.-> LP
    DRG -.-> KDR
    LAG -.-> KI
    LAG -.-> THRBY
    ASHG -.-> BS

    subgraph STRONG [ <a href='#CONTROL' style='font-weight:bold;color:#fff'>COMPLETION</a> ]
        LN[<a href='#LEGS-NEGATED'> \n \n \nLEGS\nNEGATED\n \n \n \n</a>]
        subgraph CP [ ]
            RM(<a href='#REAR-MOUNT'>REAR MOUNT</a>)
            M(<a href='#MOUNT'>MOUNT</a>)
            SM(<a href='#SIDE-MOUNT'>SIDE MOUNT</a>)
            CG(<a href='#CLOSED-GUARD'>CLOSED GUARD</a>)
        end
    end
     style STRONG stroke:#0387e8,fill:#0387e8,opacity:0.2,stroke-width:1px,stroke-dasharray 5 5
     style CP fill:#34495e,opacity:0.2



    LP -.-> HQP
    KDR -.-> HQP
    BS -.-> HQP
    KI -.-> HQP
    THRBY --> HQP
    KCP ==> LN
    OUP ==> LN
    LSP ==> LN
    SMP ==> LN

    TVP ---> HGP
    FPP ---> HGP
    BLP ---> HGP


     style KI stroke:#02fde2,stroke-dasharray: 5 5
     style LP stroke:#02fde2,stroke-dasharray: 5 5
     style BS stroke:#02fde2,stroke-dasharray: 5 5
     style THRBY stroke:#02fde2
     style KDR stroke:#02fde2

    KD ---> LDP
    KD ---> TP
    DK ---> DHP
    DK ---> DUP

    LN[ \n \n \nLEGS\nNEGATED\n \n \n \n ]
     style LN stroke:#0387e8,stroke-width:5px,stroke-dasharray: 5 5


    DHP ==> LN
    DUP ==> LN
    LDP ==> LN
    TP  ==> LN
    LN -.-> RM
    LN -.-> M 
    LN -.-> SM 
    LN -.-> CG

    RM ==> SJ(<a style="color:#53ff03" href='/jiujitsu/straitjacket'>STRAITJACKET</a>)
    RM ==> A(<a style="color:#53ff03" href='/jiujitsu/straitjacket'>AUXILIARY\nSYSTEMS</a>) 

    classDef container fill:#34495e,opacity:0.2,stroke: #000000
    class OPPPOS,OPPPOS2,INIT,EARACT,LATACT,ACTRES,HQA,P2,P3,P4,HGS,ADV container

    classDef advantageAction stroke:#02fde2,stroke-width:1px
    class LSP,OUP,SMP,KCP,DHP,DUP,LDP,TP,PLOW,HGADV advantageAction

    classDef controlPosition color:#0387e8,stroke:#0387e8,stroke-width:2px
    class M,SM,RM,CG controlPosition

    classDef submitAction color:#53ff03,stroke:#53ff03,stroke-width:1px
    class SJ,A submitAction
    
    classDef opponentAction color: #f78702,stroke: #f78702 
    class DRG,SPG,LAG,ASHG opponentAction

    classDef opponentPosition stroke:#e0dc01
    class SUP,SUPNA,SEA opponentPosition

```

<br><br>
<br><br>

##### SUPINE PASSING

```mermaid
flowchart LR

    subgraph DEFENDING [ <a href='#DEFEND' style='font-weight:bold;color:#fff'>DEFEND</a> ]
        subgraph TRAPPED [ ]
            RMD(<a style='color: #d61303' href='#REAR-MOUNTED'>REAR\nMOUNTED</a>)
            MD(<a style='color: #d61303' href='#MOUNTED'>MOUNTED</a>)
            SMD(<a style='color: #d61303' href='#SIDE-MOUNTED'>SIDE\nMOUNTED</a>)
            IG(<a style='color: #d61303' href='#IN-GUARD'>IN GUARD</a>)
        end
        KZ[ \n \n \n<a style='color:#e36107' href='#ESCAPES'>ESCAPES</a>\n \n<span style='color:#e36107' >SWEEPS</span>\n \n<a style='color:#e36107' href='#SUBMISSIONS'>SUBS</a>\n \n \n]
    end
    classDef Defend stroke:#d61303,fill:#d61303,opacity:0.15,stroke-width:5px
    class DEFENDING Defend
    classDef TrapPos fill:#34495e,opacity:0.2
    class TRAPPED TrapPos

    RMD -.-> KZ 
    MD -.-> KZ
    SMD -.-> KZ
    IG -.-> KZ 

    subgraph NEGATE [ <a href='#NEGATE' style='font-weight:bold;color:#fff'>NEGATE</a> ]
        subgraph STAGING [  ]
            NC[ \n \n \n<a style='color:#e0dc01' href='#NEGATE'>NEGATE\nCONNECTIONS</a>\n \n<span style='color:#e0dc01' >&</span>\n \n<a style='color:#5ffc02' href='#INSIDE-POSITION'>INSIDE\nPOSITION</a>\n \n \n]
        end
    end
    classDef Negates stroke:#e0dc01,fill:#e0dc01,opacity:0.15,stroke-width:5px,stroke-dasharray: 5 5
    class NEGATE Negates
    classDef Stagings fill:#34495e,opacity:0.2
    class STAGING Stagings


    classDef Advantages stroke:#02fde2,fill:#02ede2,opacity:0.1,stroke-width:5px,stroke-dasharray: 5 5
    class ADVANTAGE Advantages
    classDef Advances fill:#34495e,opacity:0.2
    class ADVANCE Advances

    subgraph STRONG [ <a href='#CONTROL' style='font-weight:bold;color:#fff'>COMPLETION</a> ]
        LN[ \n \n \n<a style='color:#02fde2' href='#ADVANTAGES'>ADVANTAGES</a>\n \n<span style='color:#02fde2' >PASSES</span>\n \n<a style='color:#02fde2' href='#DILEMMAS'>DILEMMAS</a>\n \n \n]
        subgraph CP [ ]
            RM(<a href='#REAR-MOUNT'>REAR/nMOUNT</a>)
            M(<a href='#MOUNT'>MOUNT</a>)
            SM(<a href='#SIDE-MOUNT'>SIDE\nMOUNT</a>)
            CG(<a href='#CLOSED-GUARD'>CLOSED\nGUARD</a>)
        end
    end
    classDef Control stroke:#0387e8,fill:#0387e8,opacity:0.2,stroke-width:5px,stroke-dasharray: 5 5
    class STRONG Control
    classDef control2 fill:#34495e,opacity:0.2
    class CP control2

    KZ ---> NC
    NC -.-> LN
    LN ==> RM
    LN ==> M 
    LN ==> SM 
    LN ==> CG



     classDef ControlPositions stroke:#0387e8,stroke-width:2px
     class SM,RM,CG,M ControlPositions

     classDef TrapPositions stroke:#d61303,stroke-width:2px
     class SMD,RMD,IG,MD TrapPositions

     classDef EscapeTypes stroke:#e36107,stroke-dasharray: 5 5
     class ESC,SWE,SUB EscapeTypes

     classDef ContPos stroke:#02fde2,stroke-width:3px,stroke-dasharray: 5 5
     class LN ContPos

     classDef NegPos stroke:#e0dc01,stroke-width:3px,stroke-dasharray: 5 5
     class NC NegPos

     classDef Kuzushi stroke:#e36107,stroke-width:2px,stroke-dasharray: 5 5
     class KZ Kuzushi

     classDef AdvanceMoves stroke:#02fde2,stroke-width:2px,stroke-dasharray: 5 5
     class ADV,PAS,SUBS AdvanceMoves



    classDef container fill:#34495e,opacity:0.2,stroke: #000000
   
    linkStyle default fill: none, stroke: green;


```

















### BREAKING GUARD 


```mermaid
flowchart LR

    subgraph OA1 [ OPP ACTIONS ]
        WGR(<a style="color:red">WRIST GRIPS</a>)
        GA(<a style="color:red">SITS UP</a>)
        UHOH(<a style="color:red">UNDER-OVERHOOK</a>)
        ANG(<a style="color:red">ANGLE CHANGE</a>)
        TOPLOCK(<a style="color:red">TOPLOCK</a>)
        CTG(<a style="color:red">COLLAR TIE\nGRIP</a>)
        HG(<a style="color:red">HIGH GUARD</a>)
    end
    classDef OpponentAction stroke-width:2px,stroke-dasharray: 5 5
    class WGR,GA,UHOH,ANG,TOPLOCK,CTG,HG OpponentAction

    subgraph EA1 [ EARLY ACTIONS ]
        BRG(BREAK GRIPS)
        HP(HEAD POST)
        RA(REMOVE ARM)
        HUA(HOP UP ADJUST)
        PUAT(POSTURE UP & TURN)
        HIN(HANDS INSIDE)
        WHU(WALK HANDS UP)
    end
    classDef EarlyAction stroke:#02fde2,stroke-dasharray: 5 5,stroke-width:3px
    class BRG,HP,RA,HUA,PUAT,HIN,WHU EarlyAction

    subgraph ACTRES [ ACT RESULT ]
        DBG(<a href='#DOUBLE-BICEPS-GRIP' style='color:#0387e8'>\n \n \n \nDOUBLE\nBICEPS\nGRIP\n \n \n \n </a>)
    end
    classDef ActionResult stroke:#0387e8,stroke-width:4px
    class DBG ActionResult

    subgraph ADVPOS [ <a style='color:#000000' >ADV POSITION</a> ]
        VP(<a href='#VERTICAL-POSTURE' style='color:#e0dc01'>VERTICAL\nPOSTURE</a>)
        NVP(<a href='#NON-VERTICAL-POSTURE' style='color:#e0dc01'>NON-VERTICAL\nPOSTURE</a>)
    end
    classDef AdvancePosition stroke:#e0dc01,stroke-width:2px,stroke-dasharray: 5 5
    class VP,NVP AdvancePosition

    subgraph OPPACT [  OPPONENT ACTION ]
        INSCP[INSIDE SCOOP]
    end
    class INSCP OpponentAction

    subgraph EA2 [ EARLY ACTION ]
        RBM(REACHBACK\nMETHOD)
        TOOK(TWO-ON-ONE\nKNEE POST)
        KPM(KNEE POST\nMETHOD)
        TKI(TURN KNEE IN)
    end
    class RBM,TOOK,KPM,TKI EarlyAction

    subgraph ADVRES [  ]
        GB[<a href='#GUARD-BROKEN' style='color:#0387e8'>\n \n \n \nGUARD\nBROKEN\n \n \n \n </a>]
    end
    classDef AdvanceResult stroke:#0387e8,stroke-width:4px
    class GB AdvanceResult

    WGR --> BRG
    GA --> HP
    UHOH --> RA
    ANG --> HUA
    TOPLOCK --> PUAT
    CTG --> HIN
    HG --> WHU

    BRG --> DBG
    HP --> DBG
    RA --> DBG
    HUA --> DBG
    PUAT --> DBG
    HIN --> DBG
    WHU --> DBG

    DBG --> VP
    DBG --> NVP

    NVP --> KPM
    VP --> RBM
    VP --> TOOK
    VP --> INSCP
    INSCP --> TKI

    TKI --> GB
    RBM --> GB
    TOOK --> GB
    KPM --> GB 

    classDef container fill:#34495e,opacity:0.2,stroke: #000000
    class OA1,EA1,ACTRES,ADVPOS,EA2,ADVRES,OPPACT container
    
    linkStyle default fill: none, stroke: green;
```


```mermaid
flowchart LR

    subgraph OPPPOS [ OPP POSITION ]
        KAH[<a style='color:#e0dc01'> \n \n \n \nKNEES\nABOVE\nHIP LINE\n \n \n \n </a>] 
        KBH[<a style='color:#e0dc01'> \n \n \n \nKNEES\nBELOW\nHIP LINE\n \n \n \n </a>] 
    end
    subgraph EARACT [ EARLY ACTION ]
        TTB[ \n \nTOREANDO\nTHROW BY\n \n \n ]
        SKP[ \n \nSHIN & KNEE\nPOST\n \n \n ]
        FBL[ \n \nFRONT\nBODYLOCK\n \n \n ]
        FPP[ \n \nFLOAT\nPOMMEL\n \n \n ]
    end
    subgraph LATACT [ LATE ACTION ]
        FS(FOOT STEP)
        CSP(CROSS SHIN PIN)
        FBS(FRONT BACK STEP)
        TLD(TOREANDO LEG DRAG) 
        TTM(TOES TO MAT) 
        TOP(TWO-ON-ONE POST)
        SOM(STEP-OVER METHOD)
        KDM(KNEE-DRIVE METHOD)
        FWDSHFT(FORWARD SHIFT)
        WSTEXP(WAIST EXPOSURE)
        HHS(HIGH-HIP SCISSOR)
        IPP(INSIDE POMMEL)
        OPP(OUTSIDE POMMEL)
        CKP(CROSS-KNEE POMMEL)
        CFP(CROSS-FOOT POMMEL)
    end
    subgraph ACTRES [ ACT RESULT ]
        BLOL[ \n \n \nBODYLOCK\nONE LEG\n \n \n \n ]
        SBL[ \n \n \nSIDE\nBODYLOCK\n \n \n \n ]
        HKP[ \n \n \nHIP\nKNEE\nPOST\n \n \n \n ]
        KOB[ \n \n \nKNEE\nON\nBELLY\n \n \n \n ]
    end

    KAH --> SKP
    KAH --> FBL
    FBL --> SOM
    FBL --> KDM
    FBL --> HHS
    TTB --> WSTEXP
    FBL --> FWDSHFT
    FWDSHFT --> SBL
    WSTEXP --> SBL
    HHS --> HIPCTRL

    SKP --> CSP
    SKP --> FBS
    SKP --> FS
    KBH --> TTB
    TTB --> TLD
    TTB --> TOP
    TTB --> TTM
    TLD --> HKP
    TOP --> HKP
    TTM --> HKP
    SOM --> BLOL
    KDM --> BLOL
    KAH --> FPP
    FPP --> IPP
    FPP --> CKP
    FPP --> CFP
    FPP --> OPP

    subgraph EARADV [ EARLY ADVANTAGE ]
        RUNCIRC(RUN THE\nCIRCLE)
        KLM(KNEE LIFT\nMETHOD)
        KPM(KNEE PUSH\nMETHOD)
        SBKC(STEPBACK\nKNEE CUT)
        SBIK(STEPBACK\nINSIDE KNEE)
        DSM(DOUBLE-SHIN)
        LSM(LEG\nSCISSOR)
        SLM(SHOELACE)
        KCM(KNEE-CUT)
        KOM(KICK-OUT)
        HLM(HIGH-LOCK)
        LR(LEG-RIDING)
    end

    subgraph LATADV [ LATE ADVANTAGE ]
        HOC(HAND ON CHEST)
        SOA(SWIM OVER ARM)
        INCTRL(INSIDE CONTROL)
        OUTCTRL(OUTSIDE CONTROL)
    end

    HIPCTRL( \n \n \nHIP\nCONTROL\n \n \n \n )
    HDCTRL( \n \n \nHEAD\nCONTROL\n \n \n \n )

    FS ---> HKP
    CSP --> HKP
    FBS --> HKP
    HKP ---> RUNCIRC
    HKP ---> SBKC
    HKP ---> SBIK
    RUNCIRC ---> KLM
    RUNCIRC ---> KPM
    KLM ---> HIPCTRL
    KPM ---> HIPCTRL
    HIPCTRL ---> HOC
    HIPCTRL ---> SOA
    HOC ---> INCTRL
    SOA ---> OUTCTRL
    INCTRL ---> HDCTRL
    OUTCTRL ---> HDCTRL
    SBIK ---> HIPCTRL
    SBKC ---> HIPCTRL

    BLOL --> DSM
    BLOL --> LSM
    BLOL --> SLM
    BLOL --> KCM
    BLOL --> KOM
    BLOL --> HLM
    SBL --> LR
    LR --> HIPCTRL

    DSM --> HIPCTRL
    LSM --> HIPCTRL
    SLM --> HIPCTRL
    KCM --> HIPCTRL
    KOM --> HIPCTRL

    IPP --> KOB
    OPP --> KOB
    CKP --> KOB
    CFP --> KOB
    KOB --> HDCTRL
    HLM --> HDCTRL

    classDef TargetStaging stroke:#e0dc01,stroke-width:3px,stroke-dasharray: 5 5
    class KBH,KAH TargetStaging

    classDef TargetAction stroke:#f78702,stroke-width:2px,stroke-dasharray: 5 5
    class FWDSHFT,WSTEXP TargetAction

    classDef AdvantageStaging stroke:#23eacf,stroke-width:3px,stroke-dasharray: 5 5
    class SKP,FBL,FPP,TTB AdvantageStaging

    classDef AdvantageResult stroke:#23eacf,stroke-width:2px
    class FS,CSP,FBS,TLD,TTM,TOP,IPP,OPP,CKP,CFP,SOM,KDM,HHS AdvantageResult

    classDef ControlStaging stroke:#0387e8,stroke-width:3px,stroke-dasharray: 5 5
    class HKP,KOB,BLOL,SBL ControlStaging

    classDef ControlResult stroke:#0387e8,stroke-width:4px
    class HIPCTRL,HDCTRL ControlResult

    classDef container fill:#34495e,opacity:0.2,stroke: #000000
    class OPPPOS,EARACT,LATACT,ACTRES,EARADV,LATADV container

    %% linkStyle 1,2,5 fill:none, stroke:#e0dc01
    linkStyle default fill: none, stroke:#255401

```

