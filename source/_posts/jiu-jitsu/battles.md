---
title: battles
date: 2023-04-08 12:56:24
tags: [jiujitsu]
category: [training, tactics]

---

# FLOW

<img src="../../../assets/img/posters/flow.png">


<br><br>
<br><br>

### SCARECROW

<br>

<video id="scarecrow-x" class="tut"  width=90% controls="controls" poster="../../../assets/img/posters/scarecrow-x.png" src="../../../assets/videos/jiu-jitsu/battles/Scarecrow-X.mp4" type="video/mp4"></video>

<br>


### LION

<br>

<video id="lion-x" class="tut"  width=90% controls="controls" poster="../../../assets/img/posters/lion-x.png" src="../../../assets/videos/jiu-jitsu/battles/Lion-X.mp4" type="video/mp4"></video>

<br>

### TIGER

<br>

<video id="tiger-x" class="tut"  width=90% controls="controls" poster="../../../assets/img/posters/tiger-x.png" src="../../../assets/videos/jiu-jitsu/battles/Tiger-X.mp4" type="video/mp4"></video>

<br>

### BEAR

<br>

<video id="bear-x" class="tut"  width=90% controls="controls" poster="../../../assets/img/posters/bear-x.png" src="../../../assets/videos/jiu-jitsu/battles/Bear-X.mp4" type="video/mp4"></video>

<br>

### BADGER

<br>

<video id="badger-x" class="tut"  width=90% controls="controls" poster="../../../assets/img/posters/badger-x.png" src="../../../assets/videos/jiu-jitsu/battles/Badger-X.mp4" type="video/mp4"></video>

<br>

### CROC

<br>

<video id="croc-x" class="tut"  width=90% controls="controls" poster="../../../assets/img/posters/croc-x.png" src="../../../assets/videos/jiu-jitsu/battles/Croc-X.mp4" type="video/mp4"></video>

<br>

### DOG

<br>

<video id="dog-x" class="tut"  width=90% controls="controls" poster="../../../assets/img/posters/dog-x.png" src="../../../assets/videos/jiu-jitsu/battles/Dog-X.mp4" type="video/mp4"></video>

<br>

### CHIMP

<br>

<video id="chimp-x" class="tut"  width=90% controls="controls" poster="../../../assets/img/posters/chimp-x.png" src="../../../assets/videos/jiu-jitsu/battles/Chimp-X.mp4" type="video/mp4"></video>

<br>

### LEOPARD

<br>

<video id="leopard-x" class="tut"  width=90% controls="controls" poster="../../../assets/img/posters/leopard-x.png" src="../../../assets/videos/jiu-jitsu/battles/Leopard-X.mp4" type="video/mp4"></video>

<br>

### JAGUAR

<br>

<video id="jaguar-x" class="tut"  width=90% controls="controls" poster="../../../assets/img/posters/jaguar-x.png" src="../../../assets/videos/jiu-jitsu/battles/Jaguar-X.mp4" type="video/mp4"></video>

<br>

### CONDA

<br>

<video id="conda-x" class="tut"  width=90% controls="controls" poster="../../../assets/img/posters/conda-x.png" src="../../../assets/videos/jiu-jitsu/battles/Conda-X.mp4" type="video/mp4"></video>

<br>

