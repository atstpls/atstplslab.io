---
title: mission
date: 2023-04-08 12:56:24
tags: [jiujitsu]
category: [training, tactics]
---

## FLOW

<br>

```mermaid
flowchart LR

    subgraph GI
        TOO[<a href='#THROWS' ><span style='min-width: 480px; min-height: 260px; display: block;'><img src='../../assets/img/posters/throw.png' /><span></a>\n\n\n<a href='#PULLS' ><span style='min-width: 480px; min-height: 260px; display: block;'><img src='../../assets/img/posters/pulls.png' /><span></a>]
    end
    
    subgraph NOGI
        CLS[<a href='#UNDERHOOK' ><span style='min-width: 480px; min-height: 260px; display: block;'><img src='../../assets/img/posters/underhook.png' /><span></a>\n\n\n<a href='#TWO-ON-ONE' ><span style='min-width: 480px; min-height: 260px; display: block;'><img src='../../assets/img/posters/two-on-one.png' /><span></a>]
    end

    RBL[<a href='#REAR-BODYLOCK' ><span style='min-width: 480px; min-height: 260px; display: block;'><img src='../../assets/img/posters/rear-bodylock.png' /><span></a>\n\n<a href='#SHORT-OFFENSE' ><span style='min-width: 480px; min-height: 260px; display: block;'><img src='../../assets/img/posters/short-offense.png' /><span></a>\n\n<a href='#CLOSED-GUARD' ><span style='min-width: 480px; min-height: 260px; display: block;'><img src='../../assets/img/posters/closed-guard.png' /><span></a>]
    
  
   subgraph DEFENDING [  ]
        AMB[<a href='#CLAMP' ><span style='min-width: 480px; min-height: 260px; display: block;'><img src='../../assets/img/posters/the-clamp.png' /><span></a>\n\n<a href='#STRAITJACKET' ><span style='min-width: 480px; min-height: 260px; display: block;'><img src='../../assets/img/posters/ambush-straitjacket.png' /><span></a>\n\n<a href='#CAT-WEDGE' ><span style='min-width: 480px; min-height: 260px; display: block;'><img src='../../assets/img/posters/cat-wedge.png' /><span></a>]
    end
    
    subgraph ESCAPING [ ]
        ESC[<a href='#TRIANGLE-COUNTER' ><span style='min-width: 480px; min-height: 260px; display: block;'><img src='../../assets/img/posters/triangle-counter.png' /><span></a>\n\n<a href='#ARMBAR-COUNTER' ><span style='min-width: 480px; min-height: 260px; display: block;'><img src='../../assets/img/posters/armbar-counter.png' /><span></a>\n\n<a href='#GUILLOTINE-COUNTER' ><span style='min-width: 480px; min-height: 260px; display: block;'><img src='../../assets/img/posters/guillotine-counter.png' /><span></a>\n\n<a href='#KIMURA-COUNTER' ><span style='min-width: 480px; min-height: 260px; display: block;'><img src='../../assets/img/posters/kimura-counter.png' /><span></a>]
    end

    classDef Defend stroke:#d61303,fill:#d61303,opacity:0.15,stroke-width:5px
    class DEFENDING Defend


    classDef Escape stroke:purple,fill:purple,opacity:0.15,stroke-width:5px
    class ESCAPING Escape

    NOGI --> RBL 

    GI --> RBL 

    RBL --- DEFENDING

    DEFENDING --- ESCAPING

    classDef Yell stroke:#808000,stroke-width:1px,fill:#808000,opacity:0.3
    class NOGI Yell

    classDef YellLine stroke:#808000,stroke-width:3px
    class CLS YellLine

    classDef Red stroke:red,stroke-width:1px
    class AMB Red

    classDef Green stroke:green,stroke-width:6px
    class HG Green

    classDef StrongHoldEntry stroke:#e0dc01,fill:#e0dc01,opacity:0.15,stroke-width:1px
    class SE StrongHoldEntry

    classDef Purp stroke:purple,stroke-width:3px,fill:purple,opacity:0.3
    class GI Purp

    classDef PurpLine stroke:purple,stroke-width:3px
    class TOO PurpLine

    classDef Advantage stroke:#02fde2,fill:#0200f2,opacity:0.9,stroke-width:1px
    class RBL Advantage

    linkStyle 0,1 stroke:blue,stroke-width:6px
    linkStyle 2,3 stroke:darkred,stroke-width:3px;opacity:0.5


```

<br></br>
<br></br>

### UNDERHOOK

<video width="1440" height="920"  controls="controls" poster="../../assets/img/posters/underhook.png" src="../../assets/videos/jiu-jitsu/hand-fighting/Underhook-Series.mp4" type="video/mp4"></video>

<br>

### TWO ON ONE

<video width="1440" height="920"  controls="controls" poster="../../assets/img/posters/two-on-one.png" src="../../assets/videos/jiu-jitsu/hand-fighting/Two-On-One-Series.mp4" type="video/mp4"></video>

<br>

### THROWS

<video width="1440" height="920"  controls="controls" poster="../../assets/img/posters/throw.png" src="../../assets/videos/jiu-jitsu/hand-fighting/Flank-Drop-Trap.mp4" type="video/mp4"></video>

<br>

### PULLS

<video width="1440" height="920"  controls="controls" poster="../../assets/img/posters/pulls.png" src="../../assets/videos/jiu-jitsu/hand-fighting/Pulls-And-Passes.mp4" type="video/mp4"></video>

<br>

### REAR BODYLOCK 

<video width="1440" height="920"  controls="controls" poster="../../assets/img/posters/rear-bodylock.png" src="../../assets/videos/jiu-jitsu/rear-bodylock/Rear-Bodylock-Series.mp4" type="video/mp4"></video>

<br>

### SHORT OFFENSE

<video width="1440" height="920"  controls="controls" poster="../../assets/img/posters/short-offense.png" src="../../assets/videos/jiu-jitsu/short-offense/Short-Offense-Series.mp4" type="video/mp4"></video>

<br>

### CLOSED GUARD

<video width="1440" height="920"  controls="controls" poster="../../assets/img/posters/closed-guard.png" src="../../assets/videos/jiu-jitsu/closed-guard/Robber-Otter.mp4" type="video/mp4"></video>

<br>

### CLAMP

<video width="1440" height="920"  controls="controls" poster="../../assets/img/posters/the-clamp.png" src="../../assets/videos/jiu-jitsu/ambush/Side-Ambush.mp4" type="video/mp4"></video>

<br>

### STRAITJACKET

<video width="1440" height="920"  controls="controls" poster="../../assets/img/posters/ambush-straitjacket.png" src="../../assets/videos/jiu-jitsu/ambush/Ambush-Straitjacket.mp4" type="video/mp4"></video>

<br>

### CAT WEDGE

<video width="1440" height="920"  controls="controls" poster="../../assets/img/posters/cat-wedge.png" src="../../assets/videos/jiu-jitsu/ambush/Ambush-In-Guard.mp4" type="video/mp4"></video>

<br>

### TRIANGLE COUNTER

<video width="1440" height="920"  controls="controls" poster="../../assets/img/posters/triangle-counter.png" src="../../assets/videos/jiu-jitsu/triangles/Triangle-Counters.mp4" type="video/mp4"></video>

<br>

### ARMBAR COUNTER

<video width="1440" height="920" controls="controls" poster="../../assets/img/posters/armbar-counter.png" src="../../assets/videos/jiu-jitsu/armbars/Armbar-Counters.mp4" type="video/mp4"></video>

<br>

### GUILLOTINE COUNTER

<video width="1440" height="920"  controls="controls" poster="../../assets/img/posters/guillotine-counter.png" src="../../assets/videos/jiu-jitsu/short-offense/Fhl-Counters.mp4" type="video/mp4"></video>

<br>

### KIMURA COUNTER

<video width="1440" height="920"  controls="controls" poster="../../assets/img/posters/kimura-counter.png" src="../../assets/videos/jiu-jitsu/kimuras/Kimura-Counter.mp4" type="video/mp4"></video>


<br>





Underhand side - Keep hands on sternum, beat head, beat knee, elbow to ground 
Overhand side - Keep hands on sternum, look at opponent, beat hook, turn inside using v grip on hands, elbow to ground, cross ashi 
Overhand body triangle top - Convert to outside cross, turn inside, ashi garami 
Overhand body triangle bottom - Turn in and kip legs to spin on top, OR separate strangle hands, roll once then super cut 


outside body triangle - 
inside body triangle - 
inside cross - 
outside cross - 

Convert all triangles to outside cross, the weakest of the 4

Handfighting
- same stance: leg tackles 
- opposite stance: foot sweeps




##### Counters 



##### Major Flow


```mermaid
flowchart TD

    HF[ <video width="275" height="185"  controls="controls" poster="../../assets/img/posters/hand-fighting.png" src="../../assets/videos/jiu-jitsu/dilemmas/standing/Hand-Fighting-NoGi.mp4" type="video/mp4"></video>\n<a style="font-size:22px;color:#fff" href=#Hand-Fighting>HAND FIGHTING</a> ]
    POP[ <video width="200" height="110"  controls="controls" poster="../../assets/img/posters/pass-on-pull.png" src="../../assets/videos/jiu-jitsu/dilemmas/standing/Pass-On-Pull-Dilemma-Gi.mp4" type="video/mp4"></video>\n<a style="font-size:22px;color:#fff" href=#Pass-On-Pull>PASS ON PULL</a> ]

    SD[<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/snapdown.png" src="../../assets/videos/jiu-jitsu/dilemmas/standing/snapdown/Snapdown-Variations-NoGi.mp4" type="video/mp4"></video>]     
    SOFH[ <video width="245" height="165"  controls="controls" poster="../../assets/img/posters/short-offense.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/FHL-Control-Mechanics.mp4" type="video/mp4"></video>\n<video width="245" height="165"  controls="controls" poster="../../assets/img/posters/front-headlock.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/FHL-Control-Mechanics.mp4" type="video/mp4"></video>\n<a style="font-size:22px;color:#fff" href=#Front-Headlock>FRONT HEADLOCK</a>  ]

    GB[<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/go-behind.png" src="../../assets/videos/jiu-jitsu/dilemmas/standing/go-behind/Arm-Drags-Throw-Bys-Slide-Bys.mp4" type="video/mp4">]
    RBL[<video width="275" height="185" controls="controls" poster="../../assets/img/posters/rear-bodylock.png" src="../../assets/videos/jiu-jitsu/rear-bodylock/Rear-Bodylock-Advances.mp4" type="video/mp4"></video]    
    RM[<video width="275" height="185" controls="controls" poster="../../assets/img/posters/rear-mount.png" src="../../assets/videos/jiu-jitsu/dilemmas/rear-mount/rear-mount.mp4" type="video/mp4"></video>\n<a style="font-size:22px;color:#fff" href=#Rear-Mount>REAR MOUNT</a>]    

    TD[<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/takedown.png" src="../../assets/videos/jiu-jitsu/dilemmas/standing/takedown/Double-Variations-NoGi.mp4" type="video/mp4"></video>] 
    GP[<video width="275" height="185" controls="controls" poster="../../assets/img/posters/guard-passing.png" src="../../assets/videos/jiu-jitsu/dilemmas/guard-passing/Guard-Passing-1.mp4" type="video/mp4"></video>\n<a style="font-size:22px;color:#fff" href=#Guard-Passing>GUARD PASSING</a>] 
    ETK[<video width="275" height="185" controls="controls" poster="../../assets/img/posters/elbow-to-knee.png" src="../../assets/videos/jiu-jitsu/dilemmas/guard-passing/Guard-Passing-1.mp4" type="video/mp4"></video>\n<a style="font-size:22px;color:#fff" href=#Elbow-To-Knee>ELBOW TO KNEE</a>] 

    SCBT[<video width="275" height="185" controls="controls" poster="../../assets/img/posters/elbow-to-knee.png" src="../../assets/videos/jiu-jitsu/dilemmas/guard-passing/Guard-Passing-1.mp4" type="video/mp4"></video>\n<a style="font-size:22px;color:#fff" href=#Elbow-To-Knee>ELBOW TO KNEE</a>] 


    HF --> SD
    HF --> GB
    HF --> TD

    SD --> SOFH
    GB --> RBL
    TD --> GP

    SOFH --> ETK
    RBL --> ETK
    GP --> ETK

    ETK --> RM
           
    HF --> POP
    POP --> TD

    classDef Cont stroke:#0387e8,stroke-width:1px,fill:#29f
    class SOFH,RBL,ETK,RM Cont

    classDef Cont2 stroke:#0099ff,stroke-width:2px
    class RM Cont2

    classDef Oran stroke:#F59C11,stroke-width:1px,fill:darkorange
    class SD,GB,TD Oran

    classDef Pass stroke:#33cc33,stroke-width:1px,fill:#33cc33
    class GP,POP Pass

    classDef Yell stroke:#808000,stroke-width:1px,fill:#808000
    class HF Yell

    classDef Redd stroke:red,stroke-width:1px
    class SUB Redd

    classDef Box fill:#34495e,opacity:0.3
    class SDS,TDS,GBS Box


    linkStyle 0,1,2 stroke:yellow,stroke-width:3px
    linkStyle 3,4,5 stroke:orange,stroke-width:3px
    linkStyle 7,6,8,9 stroke:#0099ff,stroke-width:4px
    linkStyle 10,11 stroke:green,stroke-width:4px,stroke-dasharray: 5 5

```

<br><br>



##### Front Headlock 

```mermaid
flowchart LR

    FHP[ <video width="275" height="185"  controls="controls" poster="../../assets/img/posters/front-headlock.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/FHL-Control-Mechanics.mp4" type="video/mp4"></video> ]

   
    AN[<video width="275" height="185" controls="controls" poster="../../assets/img/posters/anaconda.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/anaconda.mp4" type="video/mp4"></video>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/seated-kata-gatame.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/seated-kata-gatame.mp4" type="video/mp4"></video>]
    
    PTP[<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/palm-to-palm.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/palm-to-palm.mp4" type="video/mp4"></video>]     
    HWP[<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/high-wrist.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/High-Wrist-Position-Mechanics.mp4" type="video/mp4"></video>] 
    DA[<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/darce.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/darce.mp4" type="video/mp4">]

    FHP --> PTP
    FHP --> HWP
    FHP --> DA

    HEG[<video width="275" height="185" controls="controls" poster="../../assets/img/posters/high-elbow-guillotine.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/high-wrist/high-elbow-guillotine.mp4" type="video/mp4"></video>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/low-elbow-guillotine.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/high-wrist/low-elbow-guillotine.mp4" type="video/mp4"></video><br><video width="275" height="185"  controls="controls" poster="../../assets/img/posters/arm-in-guillotine.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/high-wrist/arm-in-guillotine.mp4" type="video/mp4"></video>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/power-guillotine.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/high-wrist/power-guillotine.mp4" type="video/mp4">] 


    PTP --> AN
    HWP --> HEG  

    style FHP stroke:#0387e8,stroke-width:5px,stroke-dasharray: 5 5


    classDef anaconda stroke:#B308E7,stroke-width:4px
    class PTP,AN anaconda

    classDef darce stroke:#F59C11,stroke-width:4px
    class DA darce

    classDef highwrist stroke:#8FCE00,stroke-width:3px
    class HWP,HEG highwrist

    linkStyle 1,4 stroke:green,stroke-width:3px,color:green,fill:green
    linkStyle 0,3 stroke:purple,stroke-width:3px
    linkStyle 2 stroke:orange,stroke-width:3px

```

<br><br>



##### Rear Bodylock 

```mermaid
flowchart LR

    FHP[ <video width="275" height="185"  controls="controls" poster="../../assets/img/posters/front-headlock.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/FHL-Control-Mechanics.mp4" type="video/mp4"></video> ]

   
    AN[<video width="275" height="185" controls="controls" poster="../../assets/img/posters/anaconda.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/anaconda.mp4" type="video/mp4"></video>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/seated-kata-gatame.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/seated-kata-gatame.mp4" type="video/mp4"></video>]
    
    PTP[<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/palm-to-palm.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/palm-to-palm.mp4" type="video/mp4"></video>]     
    HWP[<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/high-wrist.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/High-Wrist-Position-Mechanics.mp4" type="video/mp4"></video>] 
    DA[<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/darce.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/darce.mp4" type="video/mp4">]

    FHP --> PTP
    FHP --> HWP
    FHP --> DA

    HEG[<video width="275" height="185" controls="controls" poster="../../assets/img/posters/high-elbow-guillotine.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/high-wrist/high-elbow-guillotine.mp4" type="video/mp4"></video>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/low-elbow-guillotine.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/high-wrist/low-elbow-guillotine.mp4" type="video/mp4"></video><br><video width="275" height="185"  controls="controls" poster="../../assets/img/posters/arm-in-guillotine.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/high-wrist/arm-in-guillotine.mp4" type="video/mp4"></video>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/power-guillotine.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/high-wrist/power-guillotine.mp4" type="video/mp4">] 


    PTP --> AN
    HWP --> HEG  

    style FHP stroke:#0387e8,stroke-width:5px,stroke-dasharray: 5 5


    classDef anaconda stroke:#B308E7,stroke-width:4px
    class PTP,AN anaconda

    classDef darce stroke:#F59C11,stroke-width:4px
    class DA darce

    classDef highwrist stroke:#8FCE00,stroke-width:3px
    class HWP,HEG highwrist

    linkStyle 1,4 stroke:green,stroke-width:3px,color:green,fill:green
    linkStyle 0,3 stroke:purple,stroke-width:3px
    linkStyle 2 stroke:orange,stroke-width:3px

```

<br><br>


##### Guard Passing


```mermaid
flowchart LR

    TOR[ <video width="210" height="140"  controls="controls" poster="../../assets/img/posters/toreando.png" src="../../assets/videos/jiu-jitsu/dilemmas/standing/Hand-Fighting-NoGi.mp4" type="video/mp4"></video>]
    POM[ <video width="210" height="140"  controls="controls" poster="../../assets/img/posters/pommeling.png" src="../../assets/videos/jiu-jitsu/dilemmas/standing/Hand-Fighting-NoGi.mp4" type="video/mp4"></video>]
    BLP[ <video width="210" height="140"  controls="controls" poster="../../assets/img/posters/bodylocks.png" src="../../assets/videos/jiu-jitsu/dilemmas/standing/Hand-Fighting-NoGi.mp4" type="video/mp4"></video>]
    HSP[ <video width="210" height="140"  controls="controls" poster="../../assets/img/posters/high-step.png" src="../../assets/videos/jiu-jitsu/dilemmas/standing/Hand-Fighting-NoGi.mp4" type="video/mp4"></video>]
    HGP[ <video width="210" height="140"  controls="controls" poster="../../assets/img/posters/half-guard-passing.png" src="../../assets/videos/jiu-jitsu/dilemmas/standing/Hand-Fighting-NoGi.mp4" type="video/mp4"></video>]

    HKP[<video width="210" height="140"  controls="controls" poster="../../assets/img/posters/hip-knee-post.png" src="../../assets/videos/jiu-jitsu/dilemmas/standing/snapdown/Snapdown-Variations-NoGi.mp4" type="video/mp4"></video>]     
    TTM[ <video width="210" height="140"  controls="controls" poster="../../assets/img/posters/toes-to-mat.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/FHL-Control-Mechanics.mp4" type="video/mp4"></video> ]

    TOR --> HKP
    TOR --> TTM
   
    classDef Cont stroke:#0387e8,stroke-width:1px,fill:#29f
    class TTM,HKP Cont

    classDef Cont2 stroke:#0099ff,stroke-width:2px
    class RM Cont2

    classDef Oran stroke:#F59C11,stroke-width:1px,fill:darkorange
    class SD,GB,TD Oran

    classDef Pass stroke:#33cc33,stroke-width:1px,fill:#33cc33
    class TOR,POM,BLP,HSP Pass

    classDef Yell stroke:#808000,stroke-width:1px,fill:#808000
    class HF Yell

    classDef Redd stroke:red,stroke-width:1px
    class SUB Redd

    classDef Box fill:#34495e,opacity:0.3
    class SDS,TDS,GBS Box



```

<br><br>

When using the hip and knee post to camp at the J-Point, the  bottom player is going to try to maintain a knee elbow connection in the near side to prevent the guard pass. The bottom player is going to be trying to strip the grip on their near leg. If the top player is having trouble keeping their grip on their knee, they can always grip the ankle. Now when the bottom player tries to strip the grip on their ankle, they need to extend their near leg. This will allow the top player to go back to the knee. In this way the top player can effectively go back and forth between gripping the ankle and the knee.




```mermaid
flowchart LR
    subgraph INIT [ ]
        subgraph OPPPOS [ OPP POSITION ]
            SUP[<a style='color:#e0dc01'>SUPINE</a>] 
            SUPNA[<a style='color:#e0dc01'>SUPINE NO ANGLE</a>] 
            SEA[<a style='color:#e0dc01'>SEATED</a>] 
        end

        subgraph OPPPOS2 [ OPP POSITION ]
            DRG(<a style="color:#f78702" href='#DE-LA-RIVA'>DE LA RIVA GUARDS</a>)
            SPG(<a style="color:#f78702" href='#SPIDER'>SPIDER GUARDS</a>)
            LAG(<a style="color:#f78702" href='#LASSO'>LASSO GUARDS</a>)
            ASHG(<a style="color:#f78702" href='#ASHI-GARAMI'>ASHI GARAMI</a>)
        end
    end

    SUP --> TVP
    SUPNA --> FPP
    SEA --> BLP

    subgraph NEGATE [ <a href='#NEGATE' style='font-weight:bold;color:#fff'>NEGATE</a> ]

        subgraph STAGING [  ]
            HGP[<a style='color:#6b6a1b' href='#HALF-GUARD-POSITION'>HALF\nGUARD</a>] 
            HQP[<a style='color:#6b6a1b' href='#HQ-POSITION'>HQ\nPOSITION</a>]
            DK[<a style='color:#6b6a1b' href='#DOUBLE-KNEE'>DOUBLE\nKNEE</a>]    
            KD[<a style='color:#6b6a1b' href='#KNEE-DROP'>KNEE\nDROP</a>]
            OAP[<a style='color:#6b6a1b' href='#OUTSIDE-ADVANTAGE'>OUTSIDE\nADVANTAGE</a>]
        end
        
        subgraph HGS [ ]
            TVP[<a style='color:#6b6a1b' href='#DOUBLE-KNEE'>TOREANDO PASSES</a>] 
            FPP[<a style='color:#6b6a1b' href='#DOUBLE-KNEE'>FLOATING PASSES</a>] 
            BLP[<a style='color:#6b6a1b' href='#DOUBLE-KNEE'>BODYLOCK PASSES</a>] 
        end
        
        subgraph ADV[ ]
            KI[<a style='color:#6b6a1b' href='#KNEE-INSIDE'>KNEE INSIDE</a>]
            LP[<a style='color:#6b6a1b' href='#LEG-POST'>LEG POST</a>]
            BS[<a style='color:#6b6a1b' href='#BACKSTEP'>BACKSTEP</a>]
            KDR(<a style='color:#6b6a1b' href='#KNEE-DRIVE'>KNEE DRIVE</a>)
            THRBY(<a style='color:#6b6a1b' href='#THROW-BY'>THROW-BY</a>)
        end            
    end
    style NEGATE stroke:#e0dc01,fill:#e0dc01,opacity:0.15,stroke-width:1px,stroke-dasharray 5 5
    style STAGING fill:#34495e,opacity:0.2

    classDef NegateAction stroke:#e0dc01,stroke-width:1px,stroke-dasharray: 5 5
    class TVP,FPP,BLP,KI,LP,BS,KDR,THRBY NegateAction

    classDef NegatePosition stroke:#e0dc01,stroke-width:1px
    class HGP,HQP,DK,KD,OAP NegatePosition


    HQP --> KCP(<a style="color:#34495e" href='#KNEE-CUT-PASS'>KNEE-CUT PASS</a>)
    HQP --> OUP(<a style="color:#34495e" href='#OVER-UNDER-PASS'>OVER-UNDER PASS</a>)
    HQP --> LSP(<a style="color:#34495e" href='#LONG-STEP-PASS'>LONG-STEP PASS</a>)
    HQP --> SMP(<a style="color:#34495e" href='#SMASH-PASS'>SMASH PASS</a>)

    subgraph ADVANTAGE [ <a href='#ADVANTAGE' style='font-weight:bold;color:#fff'>ADVANTAGE</a> ]
        PLOW[<a href='#PLOW-POSITION'>PLOW</a>] 
        HGADV[<a href='#HALF-GUARD-PASSING'>HALF\nGUARD</a>] 
        subgraph P1[ ]
            DHP(<a style='color: #02fde2' href='#DOUBLE-HEEL-PASS'>DOUBLE HEEL</a>)
            DUP(<a style='color: #02fde2' href='#DOUBLE-UNDER-PASS'>DOUBLE UNDER</a>)
        end
        subgraph P2[ ]
            KCP(<a style='color: #02fde2' href='#KNEE-CUT-PASS'>KNEE CUT</a>)
            OUP(<a style='color: #02fde2' href='#OVER-UNDER-PASS'>OVER UNDER</a>)
            LSP(<a style='color: #02fde2' href='#LONG-STEP-PASS'>LONG STEP</a>)
            SMP(<a style='color: #02fde2' href='#SMASH-PASS'>SMASH</a>)
        end
        subgraph P3[ ]
            LDP(<a style='color: #02fde2' href='#LEG-DRAG-PASS'>LEG DRAG</a>)
            TP(<a style='color: #02fde2' href='#TOREANDO-PASS'>TOREANDO</a>)
        end

    end
    HGP --> HGADV
    HGADV --> LN


     style ADVANTAGE stroke:#02fde2,fill:#02ede2,opacity:0.1,stroke-width:1px,stroke-dasharray: 5 5
     style P1 fill:#34495e,opacity:0.2
     style P2 fill:#34495e,opacity:0.2
     style P3 fill:#34495e,opacity:0.2

    SPG -.-> KI
    SPG -.-> THRBY
    DRG -.-> LP
    DRG -.-> KDR
    LAG -.-> KI
    LAG -.-> THRBY
    ASHG -.-> BS

    subgraph STRONG [ <a href='#CONTROL' style='font-weight:bold;color:#fff'>COMPLETION</a> ]
        LN[<a href='#LEGS-NEGATED'> \n \n \nLEGS\nNEGATED\n \n \n \n</a>]
        subgraph CP [ ]
            RM(<a href='#REAR-MOUNT'>REAR MOUNT</a>)
            M(<a href='#MOUNT'>MOUNT</a>)
            SM(<a href='#SIDE-MOUNT'>SIDE MOUNT</a>)
            CG(<a href='#CLOSED-GUARD'>CLOSED GUARD</a>)
        end
    end
     style STRONG stroke:#0387e8,fill:#0387e8,opacity:0.2,stroke-width:1px,stroke-dasharray 5 5
     style CP fill:#34495e,opacity:0.2



    LP -.-> HQP
    KDR -.-> HQP
    BS -.-> HQP
    KI -.-> HQP
    THRBY --> HQP
    KCP ==> LN
    OUP ==> LN
    LSP ==> LN
    SMP ==> LN

    TVP ---> HGP
    FPP ---> HGP
    BLP ---> HGP


     style KI stroke:#02fde2,stroke-dasharray: 5 5
     style LP stroke:#02fde2,stroke-dasharray: 5 5
     style BS stroke:#02fde2,stroke-dasharray: 5 5
     style THRBY stroke:#02fde2
     style KDR stroke:#02fde2

    KD ---> LDP
    KD ---> TP
    DK ---> DHP
    DK ---> DUP

    LN[ \n \n \nLEGS\nNEGATED\n \n \n \n ]
     style LN stroke:#0387e8,stroke-width:5px,stroke-dasharray: 5 5


    DHP ==> LN
    DUP ==> LN
    LDP ==> LN
    TP  ==> LN
    LN -.-> RM
    LN -.-> M 
    LN -.-> SM 
    LN -.-> CG

    RM ==> SJ(<a style="color:#53ff03" href='/jiujitsu/straitjacket'>STRAITJACKET</a>)
    RM ==> A(<a style="color:#53ff03" href='/jiujitsu/straitjacket'>AUXILIARY\nSYSTEMS</a>) 

    classDef container fill:#34495e,opacity:0.2,stroke: #000000
    class OPPPOS,OPPPOS2,INIT,EARACT,LATACT,ACTRES,HQA,P2,P3,P4,HGS,ADV container

    classDef advantageAction stroke:#02fde2,stroke-width:1px
    class LSP,OUP,SMP,KCP,DHP,DUP,LDP,TP,PLOW,HGADV advantageAction

    classDef controlPosition color:#0387e8,stroke:#0387e8,stroke-width:2px
    class M,SM,RM,CG controlPosition

    classDef submitAction color:#53ff03,stroke:#53ff03,stroke-width:1px
    class SJ,A submitAction
    
    classDef opponentAction color: #f78702,stroke: #f78702 
    class DRG,SPG,LAG,ASHG opponentAction

    classDef opponentPosition stroke:#e0dc01,fill:#e0ccff
    class SUP,SUPNA,SEA opponentPosition

```

staggered stance, flanking position, off centerline, 

<br><br>
<br><br>

##### Rear Mount

```mermaid
flowchart LR

    RM[<video width="275" height="185" controls="controls" poster="../../assets/img/posters/rear-mount.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/anaconda.mp4" type="video/mp4"></video]
    TL[<video width="275" height="185" controls="controls" poster="../../assets/img/posters/rear-mount-trilemma.png" src="../../assets/videos/jiu-jitsu/dilemmas/rear-mount/rear-mount-strangle-trilemma.mp4" type="video/mp4"></video]

    RNC[<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/rear-naked-choke.png" src="../../assets/videos/jiu-jitsu/dilemmas/rear-mount/rear-naked-choke.mp4" type="video/mp4"></video>]     
    KG[<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/kata-gatame.png" src="../../assets/videos/jiu-jitsu/dilemmas/rear-mount/kata-gatame.mp4" type="video/mp4"></video>] 
    BKG[<video width="275" height="185"  controls="controls" poster="../../assets/img/posters/back-kata-gatame.png" src="../../assets/videos/jiu-jitsu/dilemmas/rear-mount/back-kata-gatame.mp4" type="video/mp4">]

    RM --> TL
    TL --> RNC
    TL --> KG
    TL --> BKG

    classDef Cont stroke:#0387e8,stroke-width:5px,stroke-dasharray: 5 5
    class RM Cont

    classDef Redd stroke:red,stroke-width:3px
    class TL,RNC,KG,BKG Redd

    linkStyle 0,1,2,3 stroke:red,stroke-width:3px

```

<br><br>


##### Breaking In-Guard

<br>

```mermaid
flowchart LR

    BIG[<video width="275" height="185" controls="controls" poster="../../assets/img/posters/inguard-trilemma-gi.png" src="../../assets/videos/jiu-jitsu/dilemmas/in-guard/In-Guard-Trilemma-Gi.mp4" type="video/mp4"></video>]
    BING[<video width="275" height="185" controls="controls" poster="../../assets/img/posters/high-elbow-guillotine.png" src="../../assets/videos/jiu-jitsu/dilemmas/front-headlock/high-wrist/high-elbow-guillotine.mp4" type="video/mp4"></video>]

```

<br><br>


<br><br>

<br><br>


##### Supine Passing

```mermaid
flowchart LR

    subgraph DEFENDING [ <a href='#DEFEND' style='font-weight:bold;color:#fff'>DEFEND</a> ]
        subgraph TRAPPED [ ]
            RMD(<a style='color: #d61303' href='#REAR-MOUNTED'>REAR\nMOUNTED</a>)
            MD(<a style='color: #d61303' href='#MOUNTED'>MOUNTED</a>)
            SMD(<a style='color: #d61303' href='#SIDE-MOUNTED'>SIDE\nMOUNTED</a>)
            IG(<a style='color: #d61303' href='#IN-GUARD'>IN GUARD</a>)
        end
        KZ[ \n \n \n<a style='color:#e36107' href='#ESCAPES'>ESCAPES</a>\n \n<span style='color:#e36107' >SWEEPS</span>\n \n<a style='color:#e36107' href='#SUBMISSIONS'>SUBS</a>\n \n \n]
    end
    classDef Defend stroke:#d61303,fill:#d61303,opacity:0.15,stroke-width:5px
    class DEFENDING Defend
    classDef TrapPos fill:#34495e,opacity:0.2
    class TRAPPED TrapPos

    RMD -.-> KZ 
    MD -.-> KZ
    SMD -.-> KZ
    IG -.-> KZ 

    subgraph NEGATE [ <a href='#NEGATE' style='font-weight:bold;color:#fff'>NEGATE</a> ]
        subgraph STAGING [  ]
            NC[ \n \n \n<a style='color:#e0dc01' href='#NEGATE'>NEGATE\nCONNECTIONS</a>\n \n<span style='color:#e0dc01' >&</span>\n \n<a style='color:#5ffc02' href='#INSIDE-POSITION'>INSIDE\nPOSITION</a>\n \n \n]
        end
    end
    classDef Negates stroke:#e0dc01,fill:#e0dc01,opacity:0.15,stroke-width:5px,stroke-dasharray: 5 5
    class NEGATE Negates
    classDef Stagings fill:#34495e,opacity:0.2
    class STAGING Stagings


    classDef Advantages stroke:#02fde2,fill:#02ede2,opacity:0.1,stroke-width:5px,stroke-dasharray: 5 5
    class ADVANTAGE Advantages
    classDef Advances fill:#34495e,opacity:0.2
    class ADVANCE Advances

    subgraph STRONG [ <a href='#CONTROL' style='font-weight:bold;color:#fff'>COMPLETION</a> ]
        LN[ \n \n \n<a style='color:#02fde2' href='#ADVANTAGES'>ADVANTAGES</a>\n \n<span style='color:#02fde2' >PASSES</span>\n \n<a style='color:#02fde2' href='#DILEMMAS'>DILEMMAS</a>\n \n \n]
        subgraph CP [ ]
            RM(<a href='#REAR-MOUNT'>REAR/nMOUNT</a>)
            M(<a href='#MOUNT'>MOUNT</a>)
            SM(<a href='#SIDE-MOUNT'>SIDE\nMOUNT</a>)
            CG(<a href='#CLOSED-GUARD'>CLOSED\nGUARD</a>)
        end
    end
    classDef Control stroke:#0387e8,fill:#0387e8,opacity:0.2,stroke-width:5px,stroke-dasharray: 5 5
    class STRONG Control
    classDef control2 fill:#34495e,opacity:0.2
    class CP control2

    KZ ---> NC
    NC -.-> LN
    LN ==> RM
    LN ==> M 
    LN ==> SM 
    LN ==> CG



     classDef ControlPositions stroke:#0387e8,stroke-width:2px
     class SM,RM,CG,M ControlPositions

     classDef TrapPositions stroke:#d61303,stroke-width:2px
     class SMD,RMD,IG,MD TrapPositions

     classDef EscapeTypes stroke:#e36107,stroke-dasharray: 5 5
     class ESC,SWE,SUB EscapeTypes

     classDef ContPos stroke:#02fde2,stroke-width:3px,stroke-dasharray: 5 5
     class LN ContPos

     classDef NegPos stroke:#e0dc01,stroke-width:3px,stroke-dasharray: 5 5
     class NC NegPos

     classDef Kuzushi stroke:#e36107,stroke-width:2px,stroke-dasharray: 5 5
     class KZ Kuzushi

     classDef AdvanceMoves stroke:#02fde2,stroke-width:2px,stroke-dasharray: 5 5
     class ADV,PAS,SUBS AdvanceMoves



    classDef container fill:#34495e,opacity:0.2,stroke: #000000
   
    linkStyle default fill: none, stroke: green;


```

















##### Breaking Guard


```mermaid
flowchart LR

    subgraph OA1 [ OPP ACTIONS ]
        WGR(<a style="color:red">WRIST GRIPS</a>)
        GA(<a style="color:red">SITS UP</a>)
        UHOH(<a style="color:red">UNDER-OVERHOOK</a>)
        ANG(<a style="color:red">ANGLE CHANGE</a>)
        TOPLOCK(<a style="color:red">TOPLOCK</a>)
        CTG(<a style="color:red">COLLAR TIE\nGRIP</a>)
        HG(<a style="color:red">HIGH GUARD</a>)
    end
    classDef OpponentAction stroke-width:2px,stroke-dasharray: 5 5
    class WGR,GA,UHOH,ANG,TOPLOCK,CTG,HG OpponentAction

    subgraph EA1 [ EARLY ACTIONS ]
        BRG(BREAK GRIPS)
        HP(HEAD POST)
        RA(REMOVE ARM)
        HUA(HOP UP ADJUST)
        PUAT(POSTURE UP & TURN)
        HIN(HANDS INSIDE)
        WHU(WALK HANDS UP)
    end
    classDef EarlyAction stroke:#02fde2,stroke-dasharray: 5 5,stroke-width:3px
    class BRG,HP,RA,HUA,PUAT,HIN,WHU EarlyAction

    subgraph ACTRES [ ACT RESULT ]
        DBG(<a href='#DOUBLE-BICEPS-GRIP' style='color:#0387e8'>\n \n \n \nDOUBLE\nBICEPS\nGRIP\n \n \n \n </a>)
    end
    classDef ActionResult stroke:#0387e8,stroke-width:4px
    class DBG ActionResult

    subgraph ADVPOS [ <a style='color:#000000' >ADV POSITION</a> ]
        VP(<a href='#VERTICAL-POSTURE' style='color:#e0dc01'>VERTICAL\nPOSTURE</a>)
        NVP(<a href='#NON-VERTICAL-POSTURE' style='color:#e0dc01'>NON-VERTICAL\nPOSTURE</a>)
    end
    classDef AdvancePosition stroke:#e0dc01,stroke-width:2px,stroke-dasharray: 5 5
    class VP,NVP AdvancePosition

    subgraph OPPACT [  OPPONENT ACTION ]
        INSCP[INSIDE SCOOP]
    end
    class INSCP OpponentAction

    subgraph EA2 [ EARLY ACTION ]
        RBM(REACHBACK\nMETHOD)
        TOOK(TWO-ON-ONE\nKNEE POST)
        KPM(KNEE POST\nMETHOD)
        TKI(TURN KNEE IN)
    end
    class RBM,TOOK,KPM,TKI EarlyAction

    subgraph ADVRES [  ]
        GB[<a href='#GUARD-BROKEN' style='color:#0387e8'>\n \n \n \nGUARD\nBROKEN\n \n \n \n </a>]
    end
    classDef AdvanceResult stroke:#0387e8,stroke-width:4px
    class GB AdvanceResult

    WGR --> BRG
    GA --> HP
    UHOH --> RA
    ANG --> HUA
    TOPLOCK --> PUAT
    CTG --> HIN
    HG --> WHU

    BRG --> DBG
    HP --> DBG
    RA --> DBG
    HUA --> DBG
    PUAT --> DBG
    HIN --> DBG
    WHU --> DBG

    DBG --> VP
    DBG --> NVP

    NVP --> KPM
    VP --> RBM
    VP --> TOOK
    VP --> INSCP
    INSCP --> TKI

    TKI --> GB
    RBM --> GB
    TOOK --> GB
    KPM --> GB 

    classDef container fill:#34495e,opacity:0.2,stroke: #000000
    class OA1,EA1,ACTRES,ADVPOS,EA2,ADVRES,OPPACT container
    
    linkStyle default fill: none, stroke: green;
```


```mermaid
flowchart LR

    subgraph OPPPOS [ OPP POSITION ]
        KAH[<a style='color:#e0dc01'> \n \n \n \nKNEES\nABOVE\nHIP LINE\n \n \n \n </a>] 
        KBH[<a style='color:#e0dc01'> \n \n \n \nKNEES\nBELOW\nHIP LINE\n \n \n \n </a>] 
    end
    subgraph EARACT [ EARLY ACTION ]
        TTB[ \n \nTOREANDO\nTHROW BY\n \n \n ]
        SKP[ \n \nSHIN & KNEE\nPOST\n \n \n ]
        FBL[ \n \nFRONT\nBODYLOCK\n \n \n ]
        FPP[ \n \nFLOAT\nPOMMEL\n \n \n ]
    end
    subgraph LATACT [ LATE ACTION ]
        FS(FOOT STEP)
        CSP(CROSS SHIN PIN)
        FBS(FRONT BACK STEP)
        TLD(TOREANDO LEG DRAG) 
        TTM(TOES TO MAT) 
        TOP(TWO-ON-ONE POST)
        SOM(STEP-OVER METHOD)
        KDM(KNEE-DRIVE METHOD)
        FWDSHFT(FORWARD SHIFT)
        WSTEXP(WAIST EXPOSURE)
        HHS(HIGH-HIP SCISSOR)
        IPP(INSIDE POMMEL)
        OPP(OUTSIDE POMMEL)
        CKP(CROSS-KNEE POMMEL)
        CFP(CROSS-FOOT POMMEL)
    end
    subgraph ACTRES [ ACT RESULT ]
        BLOL[ \n \n \nBODYLOCK\nONE LEG\n \n \n \n ]
        SBL[ \n \n \nSIDE\nBODYLOCK\n \n \n \n ]
        HKP[ \n \n \nHIP\nKNEE\nPOST\n \n \n \n ]
        KOB[ \n \n \nKNEE\nON\nBELLY\n \n \n \n ]
    end

    KAH --> SKP
    KAH --> FBL
    FBL --> SOM
    FBL --> KDM
    FBL --> HHS
    TTB --> WSTEXP
    FBL --> FWDSHFT
    FWDSHFT --> SBL
    WSTEXP --> SBL
    HHS --> HIPCTRL

    SKP --> CSP
    SKP --> FBS
    SKP --> FS
    KBH --> TTB
    TTB --> TLD
    TTB --> TOP
    TTB --> TTM
    TLD --> HKP
    TOP --> HKP
    TTM --> HKP
    SOM --> BLOL
    KDM --> BLOL
    KAH --> FPP
    FPP --> IPP
    FPP --> CKP
    FPP --> CFP
    FPP --> OPP

    subgraph EARADV [ EARLY ADVANTAGE ]
        RUNCIRC(RUN THE\nCIRCLE)
        KLM(KNEE LIFT\nMETHOD)
        KPM(KNEE PUSH\nMETHOD)
        SBKC(STEPBACK\nKNEE CUT)
        SBIK(STEPBACK\nINSIDE KNEE)
        DSM(DOUBLE-SHIN)
        LSM(LEG\nSCISSOR)
        SLM(SHOELACE)
        KCM(KNEE-CUT)
        KOM(KICK-OUT)
        HLM(HIGH-LOCK)
        LR(LEG-RIDING)
    end

    subgraph LATADV [ LATE ADVANTAGE ]
        HOC(HAND ON CHEST)
        SOA(SWIM OVER ARM)
        INCTRL(INSIDE CONTROL)
        OUTCTRL(OUTSIDE CONTROL)
    end

    HIPCTRL( \n \n \nHIP\nCONTROL\n \n \n \n )
    HDCTRL( \n \n \nHEAD\nCONTROL\n \n \n \n )

    FS ---> HKP
    CSP --> HKP
    FBS --> HKP
    HKP ---> RUNCIRC
    HKP ---> SBKC
    HKP ---> SBIK
    RUNCIRC ---> KLM
    RUNCIRC ---> KPM
    KLM ---> HIPCTRL
    KPM ---> HIPCTRL
    HIPCTRL ---> HOC
    HIPCTRL ---> SOA
    HOC ---> INCTRL
    SOA ---> OUTCTRL
    INCTRL ---> HDCTRL
    OUTCTRL ---> HDCTRL
    SBIK ---> HIPCTRL
    SBKC ---> HIPCTRL

    BLOL --> DSM
    BLOL --> LSM
    BLOL --> SLM
    BLOL --> KCM
    BLOL --> KOM
    BLOL --> HLM
    SBL --> LR
    LR --> HIPCTRL

    DSM --> HIPCTRL
    LSM --> HIPCTRL
    SLM --> HIPCTRL
    KCM --> HIPCTRL
    KOM --> HIPCTRL

    IPP --> KOB
    OPP --> KOB
    CKP --> KOB
    CFP --> KOB
    KOB --> HDCTRL
    HLM --> HDCTRL

    classDef TargetStaging stroke:#e0dc01,stroke-width:3px,stroke-dasharray: 5 5
    class KBH,KAH TargetStaging

    classDef TargetAction stroke:#f78702,stroke-width:2px,stroke-dasharray: 5 5
    class FWDSHFT,WSTEXP TargetAction

    classDef AdvantageStaging stroke:#23eacf,stroke-width:3px,stroke-dasharray: 5 5
    class SKP,FBL,FPP,TTB AdvantageStaging

    classDef AdvantageResult stroke:#23eacf,stroke-width:2px
    class FS,CSP,FBS,TLD,TTM,TOP,IPP,OPP,CKP,CFP,SOM,KDM,HHS AdvantageResult

    classDef ControlStaging stroke:#0387e8,stroke-width:3px,stroke-dasharray: 5 5
    class HKP,KOB,BLOL,SBL ControlStaging

    classDef ControlResult stroke:#0387e8,stroke-width:4px
    class HIPCTRL,HDCTRL ControlResult

    classDef container fill:#34495e,opacity:0.2,stroke: #000000
    class OPPPOS,EARACT,LATACT,ACTRES,EARADV,LATADV container

    %% linkStyle 1,2,5 fill:none, stroke:#e0dc01
    linkStyle default fill: none, stroke:#255401

```





<br>

##### Hand Fighting

<video id="rear-mount-dilemma" class="tut"  width="100%" height="100%" controls="controls" poster="../../assets/img/posters/rear-mount-dilemma.png" src="../../assets/videos/jiu-jitsu/dilemmas/Rear-Mount-Dilemma.mp4" type="video/mp4"></video>

<br>


##### Rear Mount

<video id="rear-mount-dilemma" class="tut"  width="100%" height="100%" controls="controls" poster="../../assets/img/posters/rear-mount-dilemma.png" src="../../assets/videos/jiu-jitsu/dilemmas/Rear-Mount-Dilemma.mp4" type="video/mp4"></video>

<br>

##### Pass On Pull

<video id="pass-on-pull-dilemma-gi" class="tut"  width="50%" height="50%" controls="controls" poster="../../assets/img/posters/pass-on-pull.png" src="../../assets/videos/jiu-jitsu/dilemmas/standing/Pass-On-Pull-Dilemma-Gi.mp4" type="video/mp4"></video>

<br>

##### Elbow To Knee

<video id="pass-on-pull-dilemma-gi" class="tut"  width="50%" height="50%" controls="controls" poster="../../assets/img/posters/pass-on-pull.png" src="../../assets/videos/jiu-jitsu/dilemmas/standing/Pass-On-Pull-Dilemma-Gi.mp4" type="video/mp4"></video>

<br>