---
title: positions
date: 2023-04-08 12:56:16
tags: [jiujitsu]
category: [ecosystem, concepts] 
---


# OVERVIEW

<br><br>

[STANDING VIDEOS](#STANDING-VIDEOS)
[IN-GUARD VIDEOS](#IN-GUARD-VIDEOS)
[MOUNT VIDEOS](#MOUNT-VIDEOS)
[MOUNTED VIDEOS](#MOUNTED-VIDEOS)
[SIDE MOUNT VIDEOS](#SIDE-MOUNT-VIDEOS)
[SIDE MOUNTED VIDEOS](#SIDE-MOUNTED-VIDEOS)

<br>


<br><br>
<br><br>
<br><br>
<br><br>
<br><br>

# IN GUARD

[IN GUARD VIDEOS](#IN-GUARD-VIDEOS)


### GUARD PASSING

[GUARD PASSING VIDEOS](#GUARD-PASSING-VIDEOS)


Gordon passing 

Toreando 
- Straight to Side Mount 
- Knee on Belly
- Cross shin pin 

If knees and elbows stay together:
- Side to side 


```mermaid
flowchart LR
    subgraph PFA [ POINT FEET AWAY ]
        SS[SHUFFLE STEP]
        SWS[SWITCH STEP]
    end



    subgraph PFA [ POINT FEET AWAY ]
        SS[SHUFFLE STEP]
        SWS[SWITCH STEP]
    end
    subgraph PFA [ POINT FEET AWAY ]
        SS[SHUFFLE STEP]
        SWS[SWITCH STEP]
    end
    HKP[HIP KNEE POS]
    JPC[J POINT CAMP]

    subgraph PKA [ POINT KNEES AWAY ]
        PKT[PUSH KNEE THROUGH]
    end
    SS ---> HKP
    SWS ---> HKP
    HKP ---> JPC
    JPC ---> PKT
```






### HALF GUARD PASSING

[HALF-GUARD PASSING VIDEOS](#HALF-GUARD-PASSING-VIDEOS)


```mermaid
flowchart LR
    FBL[FRONT BODYLOCK]
    
    subgraph DIL1 [ DILEMMA ]
         SOM[STEP-OVER METHOD]
         KDM[KNEE-DRIVE METHOD]
    end

    FBL ---> SOM
    FBL ---> KDM

    SOM ---> LN[LEGS NEGATED]
    KDM ---> LN

    OTA[OPP TURNS AWAY] ---> TPL[TURTLE PIN LEG]
    OSU[OPP SITS UP] ---> SBL[SIDE BODYLOCK]
    OTI[OPP TURNS IN] ---> EKT[ELBOW-KNEE-TAILBONE]

    LN ---> OTA
    LN ---> OSU
    LN ---> OTI

    TTM[TOES TO MAT ENTRY]
    BLP[BODYLOCK ENTRY]
    FPE[FLOAT POMMEL ENTRY]
    HKPE[HIP KNEE-POST ENTRY]

    KSH[KNEE-SHIELD] -.-> FBS[FRONT-STEP BACK-STEP]
    
    subgraph HGT [ HALF GUARD THREATS ]
        KLV[KNEE-LEVER] -.-> KOM[KNEE OFF MAT]
        EE[ELBOW ESCAPE] -.-> KBI[KNEE BLOCK INSIDE]
        IBR[INSIDE BRIDGE] -.-> FSA[FEET SPREAD APART]
    end
    FSP[FUNDAMENTAL STARTING POSITION\n- Inside knee off mat and pointed out\n- Outside knee pins hip\n- Lower legs at 90 deg, toes on mat]

    IKOF[INSIDE KNEE ON FLOOR]
    OKOF[OUTSIDE KNEE ON FLOOR]

    FSM[FOUR STEP METHOD]

    THA[TOP HEAD & ARM]

    CF[ CROSSFACE ]
    RCF[ REVERSE CROSSFACE ]

    HKG[ HALF KATA GATAME ]


```




<br><br>

- [DOUBLE HEEL PASS]()
- [DOUBLE UNDER PASS]()
- [KNEE CUT PASS]()
- [OVER UNDER PASS]() 
- [LONG STEP PASS]()
- [SMASH PASS]()
- [LEG DRAG PASS]()
- [TOREANDO PASS]()

# NEGATE

 SPIDER

CROSS COLLAR, SLEEVE, HIP, BICEP

1. Use knee to steal inside position, move into HQ 

2. Else, Knee and elbow together, Knee drop back, get inside position with on other leg, come back forward 

3. Else, Sit to Ashi garami, cross grip, stand up and trip 

 DE LA RIVA 

OUTSIDE CONTROL OF LEAD LEG WITH FOOT AND HAND, CROSS COLLAR GRIP, DISTANCE FOOT ON FAR HIP 

1. Hips down and head up, Break collar grip, push 45 deg out, at end of lever (wrist)

2. Post on shin and pop hook off

3. Grip pants or ankle on opposite foot, push to floor, move to HQ 


If lead leg is controlled by pants grip:

- Don't break it, negate it

- Pommel under that leg, work to HQ

- Grab both legs, straighten arms and legs to disengage, toreando to side of grip 


 REVERSE DE LA RIVA 

1. Pop collar grip off in direction of thumb 

2. Grip inside leg, push it down while extending leg to break hook, slide into HQ 


 LASSO


 LASSO SPIDER



 DOUBLE SPIDER

- Throw by 

- battle of legs 



 STAGING POSITIONS

Negate ability to be attacked, focus on own attacks

<br>

HQ POSITION

- Negates soles of soles of feet 
- Grip Collar and pants, Rotate towards outside leg
- Drive leg inside and turn knee in other direction 
- Equal access to head control and hip control

<br>




KNEE DROP 

= go to double under pass  
- go to toreando pass 

<br>

 DOUBLE KNEE

- body lock pass
- 

<br>

OUTSIDE ADVANTAGE

<br>

 ESCAPES

<br>

 CAT WEDGE

<br><br>


 SHOULDERS TO MAT 

<br>

- [COLLAR AND SLEEVE]()
- [DOUBLE ANKLE LIFT]()
- [DOUBLE SHOULDER PUSH]() 


# FOUR SCENARIOS

- [OPEN CLOSED GUARD]()
- [SEATED OPPONENT]()
- [SUPINE OPPONENT]()
- [HALF GUARD]()


INSIGHTS 

PASSING PROBLEMS

||||
|-|-|-|
|Inside Leg|Knee-Elbow Connection|Fight with correct leverage| 
| Outside Leg|Recover Guard|Get inside position|
| Upper Body|Frames|Always go away from frames, towards the hips, turn shoulders away|
| Hips | Turns towards you | Elbow Block Go-Behind |
| Hips | Turns away from you|Anchor Tight-waist Go-Behind|
| Hips | Half turn towards you and gets knee inside|Punch Under Bolo Action|


<br>

PASSING SOLUTIONS

- [CHEST PENETRATION]()
- [KNEE PENETRATION]()
- [HIPS FIRST HEAD SECOND]()
- [IMMOBOLIZE THE HIPS]()

Knee to tailbone, elbow to knee 


OUTSIDE KNEE EXPOSURE

PASSING SUPINE GUARD


 FOOTWORK 

- [CRESCENT STEP]()
- [SIDE STEP]() 
- [CROSS STEP]()
- [FRONT STEP]()
- [BACK STEP]()
- [JAB STEP]()
- [FAKE STEP]() 

NEGATE CLOSED GUARD

DANGERS

1. Arm across centerline
2. Angle 
3. Toplocks
4. Sitting up attacks (Guillotine, hip sweeps) 

<br>

READING OPPONENT 

Feet higher than knees, control from top, double up V grip

Feet lower than knees, control from bottom, double down V grip 

Knees above hip line, knee and shin, turn opponent 

Knees below hip line,  


APPROACH

Come in behind your hands 

- [DOUBLE SOLE GRIP]() 
- [DOUBLE UP V GRIP]()
- [DOUBLE DOWN V GRIP]()
- [KNEE AND SHIN GRIP]()
- [INSIDE KNEE POST]()
- [INVERTED V GRIP]()
- [INVERTED CROSS GRIP]()
- [TWO ON ONE KNEE AND ANKLE]()


## PASSING POSITIONS

- [HIP AND KNEE POST]()
- [THE PLOW]()


Multiplicity of grips and footwork patterns, reading opponents to get to one position... the hip and knee post 

Create movement and opportunity until the far hip is exposed.

Only grip that shuts down outside leg while also splitting apart knee and elbow CONNECTION

# ONE THING FOR EVERYTHING

Toreando  ---- Push far knee to mat 
Mount escape  ----- Knee inside legs 
Throw by ------ Pin the leg you threw to the mat 
Leg drag/Flank  ----- Capture the knee 

# PASSING 

Head is the goal, but don't skip steps 

1. Break dangerous connections
2. Make feet point away  ( up, down, or out )
3. Seek mechanical/tactical advantage ( adv positions )
4. Make knees face away ( point them out, or switch sides )
5. First kill the hips, then transition to head 


Create threats on one side
Make opponent form defensive frames on that side
Switch to other side 

Focus on small subset of passes that work 90-95% all divisions, all levels
Get good at combining passes together 



Advantage positions 

- Inside knee position --- over / under or double under pass 
- Flank position - leg drag position 
- Plow position
- Toreando with no foot connections
- HQ / Split squat 
- Drop Knee
- Chest between knees --- Long step pass 
- Cross knee cut --- cross knee on floor 


HQ SERIES 

- First stuff the leg to shut down offense 
- Block knee shield by getting elbow and knee inside and together 
- Farside Underhook is #1 choice, #2 is power hand on farside collar, #3 power hand nearside with elbow going across shoulder, #4 is power hand nearside  
- If knee shield gets through, take your knee under his, walk in front, then walk his knees away
- chase hips, then head 

Toreando SERIES
- Get angle, drive into them, and push far knee to the mat
- Enter with knee, Push near knee back and chase the hips 
- 2 options... enter from Knee on Belly stance, or put knee on tailbone and enter with smash pass 

- Also can shift to side, enter in low to ground and get to Drop Knee position
- Get grips, use recoil action of popping up to start toreando , flank/smash pass

- Also a great way to transition into other passes 
- Always take the knee to the centerline 


Examples

1. Toreando to HQ to Over Under Pass
2. Toreando to Cross Hamstring to Knee Cut Pass or Cross Knee Slide
3. Toreando to Chest Between Legs to Double Under Pass to Flank 
4. Toreando to Leg Drag/Flank
5. Toreando to HQ to Long Step Pass 


LOGIC CHART 

HQ --> ( ELBOW INSIDE )  -->   

   --> ( ELBOW OUTSIDE ) -->  LONG STEP 


<img src="../../../assets/img/posters/bw.doubleunder.png" height="240" width="360"></img>&nbsp;<img src="../../../assets/img/posters/bw.overunder.png" height="240" width="360"></img>&nbsp;<img src="../../../assets/img/posters/bw.kneecut.png" height="240" width="360"></img>&nbsp;<img src="../../../assets/img/posters/bw.plow.png" height="240" width="360"></img>
<img src="../../../assets/img/posters/bw.flank.png" height="240" width="360"></img>&nbsp;<img src="../../../assets/img/posters/bw.toreando.png" height="240" width="360"></img>&nbsp;<img src="../../../assets/img/posters/bw.hq.png" height="240" width="360"></img>&nbsp;<img src="../../../assets/img/posters/bw.longstep.png" height="240" width="360"></img>
<img src="../../../assets/img/posters/bw.toreando.png" height="240" width="360"></img>&nbsp;


PASSING POSITIONS

1. Double Under Pass

Transition to PLOW position
Push opposite knee to mat, Bring leg on shoulder to centerline, get hand under lower back, touch toes to mat, bring head to outside position, drive head to opponent far hip, 

2. Over Under Pass 

HQ position, then shoot inside elbow under far knee, hold belt or gi with that hand, other arm goes over the knee 
Drive head to far hip, connect chin with your thumb, come up off knees, one hip low, one hip high, 
Drive to and capture knee, drive back to centerline, pass over leg, chase hips 

3. Knee Cut Pass

HQ position -- move to cross hamstring, keep your hip against his hip entire time, do not go down to knee with outside foot
Walk to side to prevent Reverse DRL, underhook on farside, head down on opponent chest ( or as cross face ) to prevent knee Shield
Take tricep grip, lean over oppoents other side, free trapped foot 
Block hips with knee, then follow the hips as they move away 

If knee shield happens, take collar and sleeve grip, shift to other side so you can connect your knee and elbow, then come back 


4. Leg Drag Pass 

Come straight on, two hands on two knees, push to side and go forward into the Flank position 
The elbow on side of his legs grabs straight collar grip and pinches top leg against own knee 
Other arm on back of gi collar (nape grip), hold head down and forward 
IF opponent stays put, use knee to shuck his hips over and pass 
IF opponent turns away, ride hamstring to let you get first hook in

Second way is to take outside pants grip, whip knee to centerline, come forward and post opposite knee to mat

If you lose the knee, use body to push knee back, or grab it and jerk it across 
Also you can put a drive leg up with Flank position


5. Long Step Pass 

From HQ/Cross Hamstring, grip nearside collar and farside gi pants at ankle 
Put free knee above opponent's low knee blocking it, keep head down and scapula grip 
Backstep trapped knee out and back 






Advantage POSITIONS

```mermaid
flowchart LR
    GUL[GET UNDERNEATH LEGS]
    PLOW[PLOW]

    CKC[CROSS KNEE CUT]
    CNOF[CROSS KNEE ON FLOOR]    

    TFC[TOREANDO FEET CLEARED]
    TP[TOREANDO PASS]

    CBK[CHEST BETWEEN KNEES]
    LSP[LONG STEP PASS]

    KIEO[KNEE INSIDE ELBOW OUT]
    DUP[ DOUBLE UNDER PASS ]

    FLK[FLANK]
    LDP[LEG DRAG]

    HQP[ HQ ]

    DKP[DROP KNEE]



    GUL --> PLOW
    CKC --> CNOF
    TFC --> TP
    CBK --> LSP 

```



Toreando passing

```mermaid
flowchart LR
    PE[PIN ESCAPES]
    POSE[POSITION ESCAPES]
    SUBE[SUBMISSION ESCAPES]
    STG[STAGING POSITIONS]
    CTRL[CONTROL POSITIONS]
    ADV[ADVANTAGE POSITIONS]
    NEU[NEUTRAL POSITIONS]

    SUBE --> POSE
    POSE --> STG
    STG --> ADV
    ADV --> 


```

<br><br><br><br>



# VIDEOS

### REAR-MOUNTED VIDEOS



### MOUNTED VIDEOS

<video id="mounted-dilemma" class="tut"  width="900" height="600"  controls><source src="../../../assets/videos/jiu-jitsu/dilemmas/Mounted-Dilemma.mp4" type="video/mp4"></video><span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><video id="mounted-dilemma-gi" class="tut"  width="900" height="600"   controls><source src="../../../assets/videos/jiu-jitsu/dilemmas/Mounted-Dilemma-Gi.mp4" type="video/mp4"></video>


### SIDE MOUNTED VIDEOS 

<video id="side-mounted-dilemma" class="tut"  width="900" height="600"  controls><source src="../../../assets/videos/jiu-jitsu/dilemmas/Side-Mounted-Dilemma.mp4" type="video/mp4"></video><span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><video id="side-mounted-dilemma-gi" class="tut"  width="900" height="600"   controls><source src="../../../assets/videos/jiu-jitsu/dilemmas/Side-Mounted-Dilemma-Gi.mp4" type="video/mp4"></video>


### IN GUARD VIDEOS

<video id="in-guard-trilemma" class="tut"  width="900" height="600"  controls><source src="../../../assets/videos/jiu-jitsu/dilemmas/In-Guard-Trilemma.mp4" type="video/mp4"></video><span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><video id="in-guard-trilemma-gi" class="tut"  width="900" height="600"   controls><source src="../../../assets/videos/jiu-jitsu/dilemmas/In-Guard-Trilemma-Gi.mp4" type="video/mp4"></video>

```mermaid

flowchart LR
    CATW[CAT WEDGE] ---> VERT[VERTICAL]
    VERT ---> SHAM(STRAIGHT HAMSTRING)
    CATW -.-> KPM(KNEE POST)
    KPM ---> SHAM
    CATW ---> LIFT(LIFT ACTION)
    LIFT ---> SHAM

```
### STANDING VIDEOS

<video id="standing-trilemma" class="tut"  width="900" height="600"  controls><source src="../../../assets/videos/jiu-jitsu/dilemmas/Standing-Trilemma.mp4" type="video/mp4"></video><span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><video id="standing-trilemma-gi" class="tut"  width="900" height="600"   controls><source src="../../../assets/videos/jiu-jitsu/dilemmas/Standing-Trilemma-Gi.mp4" type="video/mp4"></video>

### GUARD PASSING VIDEOS 


### HALF GUARD PASSING VIDEOS


### CLOSED GUARD VIDEOS 


### SIDE MOUNT VIDEOS 

<video id="side-mount-dilemma" class="tut"  width="900" height="600"  controls><source src="../../../assets/videos/jiu-jitsu/dilemmas/Side-Mount-Dilemma.mp4" type="video/mp4"></video><span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><video id="side-mount-dilemma-gi" class="tut"  width="900" height="600"   controls><source src="../../../assets/videos/jiu-jitsu/dilemmas/Side-Mount-Dilemma-Gi.mp4" type="video/mp4"></video>

### MOUNT VIDEOS

<video id="mount-dilemma" class="tut"  width="900" height="600"  controls><source src="../../../assets/videos/jiu-jitsu/dilemmas/Mount-Dilemma.mp4" type="video/mp4"></video><span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><video id="mount-dilemma-gi" class="tut"  width="900" height="600"   controls><source src="../../../assets/videos/jiu-jitsu/dilemmas/Mount-Dilemma-Gi.mp4" type="video/mp4"></video>


### REAR-MOUNT VIDEOS






