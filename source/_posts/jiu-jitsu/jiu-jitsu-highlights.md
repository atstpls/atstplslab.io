---
title: jiu-jitsu highlights
date: 2023-04-08 12:56:24
tags: [jiujitsu]
category: [training, tactics]

---

# top

&emsp;[<img src="../../../../assets/img/posters/natural-rebel.png" width="460" />](#natural-rebel)&emsp;[<img src="../../../../assets/img/posters/garry-tonon.png" width="460" />](#garry-tonon)&emsp;[<img src="../../../../assets/img/posters/dern.png" width="460" />](#mackenzie-dern)


&emsp;[<img src="../../../../assets/img/posters/nicky-rod.png" width="460" />](#nicky-rod)&emsp;[<img src="../../../../assets/img/posters/mask-off.png" width="460" />](#back-attacks)&emsp;[<img src="../../../../assets/img/posters/gordon-ryan.png" width="460" />](#gordon-ryan)

<br><br><br><br>
<br><br><br><br>
<br><br><br><br>
<br><br><br><br>
<br><br><br><br>
<br><br><br><br>
<br><br><br><br>


### natural rebel

<br>

<video id="natural-rebel" class="tut"   width=90% controls="controls" poster="../../../../assets/img/posters/natural-rebel.png" src="../../../../assets/videos/jiu-jitsu/highlights/Natural-Rebel.mp4" type="video/mp4"></video>

<br>


### garry tonon

<video id="garry-tonon" class="tut"   width=90% controls="controls" poster="../../../../assets/img/posters/garry-tonon.png" src="../../../../assets/videos/jiu-jitsu/highlights/Garry-Tonon.mp4" type="video/mp4"></video>

<br>

### mackenzie dern

<video id="mackenzie-dern" class="tut"   width=90% controls="controls" poster="../../../../assets/img/posters/dern.png" src="../../../../assets/videos/jiu-jitsu/highlights/Mackenzie-Dern.mp4" type="video/mp4"></video>

<br>

### nicky rod

<video id="nicky-rod" class="tut"   width=90% controls="controls" poster="../../../../assets/img/posters/nicky-rod.png" src="../../../../assets/videos/jiu-jitsu/highlights/Nicky-Rod.mp4" type="video/mp4"></video>

<br>


### back attacks

<video id="back-attacks" class="tut"   width=90% controls="controls" poster="../../../../assets/img/posters/mask-off.png" src="../../../../assets/videos/jiu-jitsu/highlights/Mask-Off.mp4" type="video/mp4"></video>

<br>

### gordon ryan

<video id="gordon-ryan" class="tut"   width=90% controls="controls" poster="../../../../assets/img/posters/gordon-ryan.png" src="../../../../assets/videos/jiu-jitsu/highlights/Gordon-Ryan-Top-Pin-Attacks.mp4" type="video/mp4"></video>

<br>