---
title: untitled
date: 2023-10-18 01:00:42
tags: [resiliency, government]
category: [ecosystem, resources]
---

"You don't become confident by shouting affirmations in the mirror, but by having a stack of undeniable proof that you are who you say you are."
- Alex Hormozi



List of needs:


Oxygen, safety, temperature, hydration, vitamins, sleep, energy, macronutrients, comfort, privacy, rest, trust, enjoyment, social interaction, stimulation, authenticity 

Knee on Belly Escapes

Forward Leg Method 
- Post hands underneath posted leg thigh
- Head should quickly follow hands
- Spin to put knee in front of his knee
- Keep one hand on far leg thigh
- Tighten wedge with leg and attack heel
- Pass foot through hole, lock triangle
- If backstep, roll that same way, lock shoestrings on his hip, protect head, hands, and elbow

Rear Leg Method
- Knee on side of hip, Bump opponent forward
- Elbow around his knee, start rotation
- Come around and draw in leg to hip
- Head to mat, swing leg over and through hole
- Make good contact with far leg
- Roll through to Double 50 



Garry Tonon Instructionals

Habits for Highly Effective Jiu-Jitsu 
Unifying the System 
How to take the back 
Protect Your Back 
Breaking Legs and Breaking Hearts 
Shoot To Kill




The greatest story ever told was about God who created the world and Jesus who saved it. We were made imperfect, but with free will. 



1. Past Legs  > 
2. Past Hips  > 
3. Past Shoulders  >
4. Control head  > 

FOCUS
* Escape bad positions/submissions
* Positions of mechanical leverage 
* Inside position for everything 


Stand Up Tactics 

- Ability to move head forward and back
- Power hand moves head
- Control hand moves opponent power hand

1. Stay outside opponent’s elbows
2. 

Gordon 

1. Outside CT > Flare out
2. Inside CT > Rotate Elbow in
3. Opp Adv > Push off reset 
4. Opp Rush > Use CT elbow for distance 

1. CT + WC   > Move Opp far hand to near side > Push Opp to far side > Move cross-grip even more while tripping far leg 
2. Snap down to hook to body lock
3. Shoulder Roll 



1. Lack of sacrifice = no proof of work 
2. Learn humility, respect, and how to handle yourself. Faster that happens, faster you grow up and get your shit together 
3. I don’t argue because I need to be right, but because I need to be proven wrong





Gordon Instructionals

Attacking Legs
Attacking Armbar
Attacking Half Guard
Attacking Triangles
Attacking from Open Guard Supine
Attacking Guard Body Locks
Attacking Open Guard Seated
Attacking Back
Attacking from Top Pins: Mount
Attacking from Top Pins: Side Control & North South 
Attacking Turtle Position 
Attacking from Half Guard
Attacking from Closed Guard
They Shall Not Pass
Attacking Guard 2.0
Pillars of Escapes: Pin Escapes 
Pillars of Escapes: Upper Body Joint Locks
Pillars of Escapes: Leg locks to Back Takes
Pillars of Escapes: Leg lock Escapes and Counters 
Pillars of Escapes: Standing Escapes 
Pillars of Escapes: Back Escapes 
Pillars of Escapes: Leglocks to Guard Passing
Pillars of Escapes: Turtle & Front Headlock
My Evolution Your Revolution 2017,2019,2022




Feeding people poision, managing their sickness

Media normalizes eating garbage and getting sick

Mandated by fiat to anchor your expectations of what is normal in ways that help them sell profitable cheap garbage by keeping you hungry, addicted, and sick.


Insulin-resistance is driver of majority of modern diseases.

Processed foods spike insulin and develop insulin resistance.

1. Polyunsaturated and hydrogenated vegetable and seed oils (canola, rapeseed, soybean, corn, safflower, sunflower)
2. Processed corn
3. Soy
4. Low-fat foods
5. Refined flour and sugar

80 percent of food and calories in a modern supermarket are made from these poisons.


Medical Industrial Complex 



Short-term thinking vs long-term thinking 




```mermaid
%%{init: {'theme':'dark'}}%%

erDiagram
          CUSTOMER }|..|{ DELIVERY-ADDRESS : has
          CUSTOMER ||--o{ ORDER : places
          CUSTOMER ||--o{ INVOICE : "liable for"
          DELIVERY-ADDRESS ||--o{ ORDER : receives
          INVOICE ||--|{ ORDER : covers
          ORDER ||--|{ ORDER-ITEM : includes
          PRODUCT-CATEGORY ||--|{ PRODUCT : contains
          PRODUCT ||--o{ ORDER-ITEM : "ordered in"
```


```mermaid
stateDiagram
    direction LR
    [*] --> A
    A --> B
    B --> C
    state B {
      direction LR
      a --> b
    }
    B --> D
```

```mermaid
flowchart TB
    c1:::sub-->a2:::pos
    subgraph one
    a1-->a2
    end
    subgraph two
    b1-->b2
    end
    subgraph three
    c1-->c2
    end
    one --> two
    three --> two
    two --> c2
    classDef sub fill:#f96
    classDef targ stroke:#f00,stroke-width:4px
    classDef pos stroke:00f,stroke-width:4px
```


```mermaid
flowchart LR
  subgraph TOP
    direction TB
    subgraph B1
        direction RL
        i1 -->f1
    end
    subgraph B2
        direction BT
        i2 -->f2
    end
  end
  A --> TOP --> B
  B1 --> B2
```


```mermaid


classDiagram

    class Point_of_Origin {
        Truth
        Inequality 
        Frames
    }

    class Agent {
        individual
    }
    Agent : Thoughts()
    Agent : Words()
    Agent : Actions()

    class Context {
        Viewpoints
        Conditions
        Details
    }
    Context : Inform()
    

    class Effort {
        
    }
    Effort : BuildUp()
    Effort : ForgiveForget()
    Effort : Lead()
    Effort : Abstract()
    Effort : OwnIt()

    class Result {

    }
    Result : SharedVisions
    Result : UnifyingStoriesGoals
    Result : IntegratedSkillsAbilities

```



```mermaid
flowchart LR

    subgraph DEFENDING [ <a href='#DEFEND' style='font-weight:bold;color:#fff'>DEFEND</a> ]
        subgraph TRAPPED [ ]
            RMD(<a style='color: #d61303' href='#REAR-MOUNTED'>REAR\nMOUNTED</a>)
            MD(<a style='color: #d61303' href='#MOUNTED'>MOUNTED</a>)
            SMD(<a style='color: #d61303' href='#SIDE-MOUNTED'>SIDE\nMOUNTED</a>)
            IG(<a style='color: #d61303' href='#IN-GUARD'>IN GUARD</a>)
        end
    end
    classDef Defend stroke:#d61303,fill:#d61303,opacity:0.15,stroke-width:5px
    class DEFENDING Defend
    classDef TrapPos fill:#34495e,opacity:0.2
    class TRAPPED TrapPos

    subgraph ESCAPES [ PERCEIVE ]
        subgraph MOVES [ ]
            EP{<a style='color: #e36107' href='#ESCAPE-PROTOCOL'>ESCAPE\nPROTOCOL</a>}
            SS{<a style='color: #e36107' href='#CAT-WEDGE'>C.A.T.\nWEDGE</a>}
            KP{<a style='color: #e36107' href='#KNEE-POST'>KNEE\nPOST</a>}
            LA{<a style='color: #e36107' href='#LIFT-ACTION'>LIFT\nACTION</a>}
        end
    end
     style ESCAPES stroke:#e36107,fill:#e36107,opacity:0.15,stroke-width:5px,stroke-dasharray 5 5
     style MOVES fill:#34495e,opacity:0.2

    NC[ \n \n \nNEGATE\nCONNECTIONS \n \n \n ]


    subgraph NEGATE [ <a href='#NEGATE' style='font-weight:bold;color:#fff'>NEGATE</a> ]
        subgraph STAGING [  ]
           DK[<a style='color:#6b6a1b' href='#DOUBLE-KNEE'> \n \nDOUBLE\nKNEE \n \n</a>]    
           HQP[<a style='color:#6b6a1b' href='#HQ-POSITION'> \n \nHQ\nPOSITION \n \n</a>]
           KD[<a style='color:#6b6a1b' href='#KNEE-DROP'> \n \nKNEE\nDROP \n \n</a>]
        end
    end
     style NEGATE stroke:#e0dc01,fill:#e0dc01,opacity:0.15,stroke-width:5px,stroke-dasharray 5 5
     style STAGING fill:#34495e,opacity:0.2

    subgraph ADVANTAGE [ <a href='#ADVANTAGE' style='font-weight:bold;color:#fff'>ADVANTAGE</a> ]
        subgraph P1[ ]
            DHP(<a style='color: #02fde2' href='#DOUBLE-HEEL-PASS'>DOUBLE HEEL</a>)
            DUP(<a style='color: #02fde2' href='#DOUBLE-UNDER-PASS'>DOUBLE UNDER</a>)
        end
        subgraph P2[ ]
            KCP(<a style='color: #02fde2' href='#KNEE-CUT-PASS'>KNEE CUT</a>)
            OUP(<a style='color: #02fde2' href='#OVER-UNDER-PASS'>OVER UNDER</a>)
            LSP(<a style='color: #02fde2' href='#LONG-STEP-PASS'>LONG STEP</a>)
            SMP(<a style='color: #02fde2' href='#SMASH-PASS'>SMASH</a>)
        end
        subgraph P3[ ]
            LDP(<a style='color: #02fde2' href='#LEG-DRAG-PASS'>LEG DRAG</a>)
            TP(<a style='color: #02fde2' href='#TOREANDO-PASS'>TOREANDO</a>)
        end
    end
     style ADVANTAGE stroke:#02fde2,fill:#02ede2,opacity:0.1,stroke-width:5px,stroke-dasharray: 5 5
     style P1 fill:#34495e,opacity:0.2
     style P2 fill:#34495e,opacity:0.2
     style P3 fill:#34495e,opacity:0.2

    subgraph STRONG [ <a href='#CONTROL' style='font-weight:bold;color:#fff'>COMPLETION</a> ]
        subgraph CP [ ]
            RM(<a href='#REAR-MOUNT'>REAR MOUNT</a>)
            M(<a href='#MOUNT'>MOUNT</a>)
            SM(<a href='#SIDE-MOUNT'>SIDE MOUNT</a>)
            CG(<a href='#CLOSED-GUARD'>CLOSED GUARD</a>)
        end
    end
     style STRONG stroke:#0387e8,fill:#0387e8,opacity:0.2,stroke-width:5px,stroke-dasharray 5 5
     style CP fill:#34495e,opacity:0.2

    MD -.-> LA
    SMD -.-> KP
    IG -.-> SS
    RMD -.-> EP
    EP ---> NC
    SS ---> NC
    KP ---> NC
    LA ---> NC
    NC -.-> HQP
    NC -.-> KD
    NC -.-> DK
    HQP -.-> KCP
    HQP -.-> OUP
    HQP -.-> LSP
    HQP -.-> SMP
    KD -.-> LDP
    KD -.-> TP
    DK -.-> DHP
    DK -.-> DUP

    LN[ \n \n \nLEGS\nNEGATED\n \n \n \n ]
     style LN stroke:#0387e8,stroke-width:5px,stroke-dasharray: 5 5


    classDef opponentPosition stroke:#d61303,stroke-width:2px
    class RMD,MD,SMD,IG opponentPosition

    classDef escapeAction stroke:#e36107,stroke-width:2px
    class LA,KP,SS,EP escapeAction

    DHP --> LN
    DUP --> LN
    KCP --> LN
    OUP --> LN
    LSP --> LN
    SMP --> LN
    LDP --> LN
    TP  --> LN
    LN -.-> RM
    LN -.-> M 
    LN -.-> SM 
    LN -.-> CG

     style NC stroke:#6b6a1b,stroke-width:5px,stroke-dasharray: 5 5
     style HQP stroke:#6b6a1b,stroke-width:3px
     style DK stroke:#6b6a1b,stroke-width:3px
     style KD stroke:#6b6a1b,stroke-width:3px
     style DHP stroke:#02fde2,stroke-width:2px
     style DUP stroke:#02fde2,stroke-width:2px
     style KCP stroke:#02fde2,stroke-width:2px
     style OUP stroke:#02fde2,stroke-width:2px
     style LSP stroke:#02fde2,stroke-width:2px
     style SMP stroke:#02fde2,stroke-width:2px
     style LDP stroke:#02fde2,stroke-width:2px
     style TP stroke:#02fde2,stroke-width:2px

    linkStyle 0,1,2,3,4,5,6,7 stroke:orange
    linkStyle 8,9,10 stroke:yellow
    linkStyle 11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26 stroke:#02fde2
    linkStyle 27,28,29,30 stroke:blue



```


```mermaid
flowchart LR
    S0 [PERCEIVE]
    S1 [THINK]
    S2 [VALUE]
    S3 [ACTIONS] 
    S4 [WORDS]

    S0 --> S1 
    S1 --> S2
    S2 --> S3
    S2 --> S4

```






```mermaid
    stateDiagram-v2
        State1: SERVICE TO OTHERS
        State2: HONOR TO SELF 
        State3: ACTION IS TRUTH 
        State4: TEAMS ARE PROOF 
        note right of State1
            Jesus taught us 
            Our gut tells us 
            History has shown us

            Sacrifice
            Putting others first
            Produces the best outcomes
            for everyone

            It's the deep meaning
            We need to sustain us through 
            The inherent suffering of life
        end note
        note right of State2
            Sensation and intuition
            Form our individual perspectives
            As we connect ideas and solve problems 

            Maintaining and exceeding
            Standards of universal human judgement 
            Is the most optimal way to serve others
        end note
        note right of State3
            Thoughts and words
            Can name or describe truth
            But they can never be truth

            Engaging with the world
            In the most clear, rational way
            Requires analysis and decision-making
            Based heavily on actions
        end note
        note right of State4
            Teams have shared visions
            Unifying stories and goals
            Integrating unique skills and abilities
            Functioning in a subsidiary and hierarchical manner 

            Being a member of a team
            Is the single best way to demonstrate
            You value cooperation, harmony, and the future
            More than your immediate base desires
        end note
        State1 --> State2
        State2 --> State3
        State3 --> State4
        State4 --> State1
```



```mermaid
    mindmap LR
    root((mindmap))
    CB(<a href="government/banks" >Central Banks</a>)
    MP(<a href="government/fiat" >Monetary Policies</a>) 
    WHY (and are causing [inflation](government/inflation), )

``` 




The lens is not one of power, 

It's difficult for people to withstand weaponized guilt 

- The nature of the universe is knowable
- There is an ethical good 
- Pursuit of truth is an ethical good 





```mermaid
mindmap
  root((mindmap))
    SERVICE TO OTHERS
        Jesus taught us 
        Our gut tells us 
        History has shown us

        Sacrifice
        Putting others first
        Produces the best outcomes
        for everyone

        It's the deep meaning
        We need to sustain us through 
        The inherent suffering of life
    HONOR TO SELF
            Sensation and intuition
            Form our individual perspectives
            As we connect ideas and solve problems 

            Maintaining and exceeding
            Standards of universal human judgement 
            Is the most optimal way to serve others
    ACTION IS TRUTH
            Thoughts and words
            Can name or describe truth
            But they can never be truth

            Engaging with the world
            In the most clear, rational way
            Requires analysis and decision-making
            Based heavily on actions
    TEAMS ARE PROOF
            Teams have shared visions
            Unifying stories and goals
            Integrating unique skills and abilities
            Functioning in a subsidiary and hierarchical manner 

            Being a member of a team
            Is the single best way to demonstrate
            You value cooperation, harmony, and the future
            More than your immediate base desires

```



```mermaid
    stateDiagram-v2
        State1: SERVICE TO OTHERS
        State2: HONOR TO SELF 
        State3: ACTION IS TRUTH 
        State4: TEAMS ARE PROOF 
        note right of State1
            Jesus taught us 
            Our gut tells us 
            History has shown us

            Sacrifice
            Putting others first
            Produces the best outcomes
            for everyone

            It's the deep meaning
            We need to sustain us through 
            The inherent suffering of life
        end note
        note right of State2
            Sensation and intuition
            Form our individual perspectives
            As we connect ideas and solve problems 

            Maintaining and exceeding
            Standards of universal human judgement 
            Is the most optimal way to serve others
        end note
        note right of State3
            Thoughts and words
            Can name or describe truth
            But they can never be truth

            Engaging with the world
            In the most clear, rational way
            Requires analysis and decision-making
            Based heavily on actions
        end note
        note right of State4
            Teams have shared visions
            Unifying stories and goals
            Integrating unique skills and abilities
            Functioning in a subsidiary and hierarchical manner 

            Being a member of a team
            Is the single best way to demonstrate
            You value cooperation, harmony, and the future
            More than your immediate base desires
        end note
        State1 --> State2
        State2 --> State3
        State3 --> State4
        State4 --> State1
```





Temptations of chaos, hopelessness, self-grandisement, aims to power...





While navigating through temptations of chaos, hopelessness, aims to power

NOW, hedonism, eliminates time, and others and focuses on the individual, now, base desires 
power aims aare not good either 

we have an insticnt for high investment reproduction strategies, mentoring 


individual perspective and personal choices determine how happy (or miserable) we humans are in this life

Heaven is not a place of comfort. As Len says, there’s joy there, but also a great deal of work on the journey.

We cannot remain who we are... we can’t keep the corrupt parts of ourselves, the selfishness, the self-focus, and enter into the joy of Heaven.

As C.S. Lewis wrote in The Problem of Pain, "We are therefore at liberty... to think of [a] bad man’s perdition not as a sentence imposed on him but as the mere fact of being what he is."


##### Mechanics

Parkour maps meaning into the world, you see a wall which means a place you can't go in.

Now you see a thing you can run up, jump or flip off of, wall becomes a source of reward, increases in value 

You're acting out the hero archetype, you approach jumps which are challenging.  They have promise but also potential danger.

Play with and recognize body dealing with fear.

<br>

##### Analysis

Temptations of chaos, hopelessness, self-grandisement, aims to power...

The arc of evolution is this:

<b>Service to others is</b>



Ideological possession is a pathology.

Transformation of corporate-owned distributors of information






Study math to understand physics

Study physics to understand chemistry

Study chemistry to understand biology

Study biology to understand psychology

Study psychology to understand economics and philosophy

Study economics and philosophy to be free



Math is the language of physics > chemistry, biology... nature

Psychology is the language of action....human behavior









Dub and Tilly chess - everything based on goals 

Forgiveness - JP prisoner dilemma, mistakes get corrected for progress 





##### EXECUTION

Establish talking points for each industry example of corruption, censorship, and erosion of individual sovereignty.

Read the best books, write about them, organize your thoughts, aim up not down, act in the service of the greater good

POINTS OF ORIGIN: individuals who aren't pressured to push narrative ( rogan, brand, peterson )

New breed of reporters/journalists ( Greenberg, Tiabbi, Shellenberger, Weiss )

EFFORTS: Guard against the effects of monetary policy and middle man economics. Use apex assets to preserve and grow long-term wealth.

Safeguard your Wealth

Using strong money with highest form of property rights

Transfer its value into the future using durable, incorruptible assets

Reap the rewards of free and sound money, and is emerging life-changing technologies

- postponing consumption allows us to provide for and improve our future


<br>

##### ADMIN / LOGISTICS








Morality vs practical atlas shrugged 







Physics - the problem of the observer 

Complexity theory - the problem of emergence 

All these things are bringing materialism to an end 

Old view is mind and matter, when we study consciousness and meaning, we get stopped 

Evolutionary thinking is trying to interpret the interpreter 

Problem of emergence, problem of quality, problem of attention 

Manifesting itself in many fields.. the world is too complex, infinite in its complexity 

In everything there is unity, and we can percieve that.

We can't account for the quality of things, account for identity of things using descriptive 

Once we have the identity, we can describe it.

Identity seems to come from somewhere else, it brings  multiplicity into unity 

Difficult to understand ancient thinking 

The world must have patterns. For things to exist, there have to be patterns.

The patterns stack up at different levels of reality.  They are fractal. And inevitable.

I encounter the world as potential that is brought into patterns (actualities) that I need to exist 

Not mathematical patterns, but teleological ( purposes ) which can be regained from evolutionary thinking 

This shows we need purposes in order to encounter the world 

Recognizing a cup means you recognize its purpose to you 

Evolution Theory worked over the substrate of genes ( materials )

Relevance, realization, and predictive processing

2 interlocking 

1.  Predictive Processing - The more I can anticipate in the world, the better ( watch out for tiger ) - adaptive anticipation - self-organizing process of what to anticipate, on many different levels (local/global) it's trying to minimize surprise, but inevitable trade-off relationships.  At the core of any prediction is the problem of sampling bias: Bias-variance problem.  

Variance is you overfit to your sample, you predict properties in the population that are not there.

or 

Bias is there is property you're missing in your sample that does generalize to the population.

Unsolvable in a perfect manner.  Whenever you do something to reduce one, you increase probability of the other.  

Try to pick up on more patterns in sample, to reduce bias, more likely to overfit 

Try to reduce overfitting data, more susceptible to bias 

Dynamically recursive self-organizing system, uses opponent processing to address trade-off relationship 

Autonomic system
One system is biased to energize you, one is biased to relax you, continuously self-correcting for each other, there is no final state 

What are you gonna care about?  It depends on environment (stable, erratic).  It's dynamically coupled to the environment in a way that optimizes 

parallels evolutionary process: Mind wandering introduces variation, Task focus kills it off, you're constantly evolving, that's the theory of relevance realization 

The two theories are interlocked because

The deeper I try to anticipate the world, the more the problem of relevance realization goes up exponentially, so the more factors and more kinds of factors to account for (quantitative and qualitative complexity), the more relevance realization I am required to do 


Relevance realization needs predictive processing 

Needs to operate on a machinery of reproduction 

Predictive processing is what allows you to reproduce your sensory motor behavior and that will get opponent processing that is relevance realization that will 

Evolve the optimization of your predictive processing so you're getting the ability to 

Anticipate more and more relevant features of the environment across time, space, and modality

Very much like Mandilian genetics and Darwinian natural selection go together in the Grand Synthesis 

Transcendental:  The true, the Good, and the Beautiful 

Meaning in life and morality: you can't reduce one to the other 

Just because you are a highly moral person, doesn't guarantee meaningful life 
Just because you live a meaningful life, doesn't guarantee you're a highly moral person


People are told to save for their future selves. They imagine their future selves and 6 months later none of them have saved.

Then tell them, Imagine your future self as beloved family member you've always cared for and have tremendous compassion and concern for

6 months later, they're saving, and how vividly they do the practice predicts how much they save

The imaginal is necessary for the rational, the rational doesn't mean the inferential, because the inferences were there the first time

The point isn't that it's an accurate prediction of the future self, the point is this ratio properly proportioned orientation towards the future 

Formulate a form of escatology that is not fortune telling gives us a solution 

Imaginal orientation aimed to future but leaves space for uncertainty


Two meta problems you have to solve to solve any Problem

1. Relevance realization ( what's important now, near future, far future )

2. Predictive processing ( anticipate rather than react )

Mode of orientation where you're trying to properly orienting yourself to the future, but not be caught in a predictive frame 


If you place your hope in yourself, others, government, you will lose 

Placing your hope in the highest good, the infinite, is the best stance 

What's the best stance to have in the worst circumstances?

Imaginal 

To walk across the floor, you need to imagine it first.  Sensory motor loop is too slow. Top down processing is predicting, imagining most of the floor.  Gives the capacity to walk quickly.  Imaginal is in the very guts of your contact with reality.

We are not all-seeing, all-knowing creatures  ( objective facts don't exist )

Best we can do with our knowledge is observe its functionality and improve it when it fails  ( mental model )
    Human behavior is goal-directed, and this determines how the world manifests itself to each of us.

A long, long time ago, we realized that time exists and always moves forward. We began describing things not as they were in one moment, but as they were across many moments. Over days, years, and lifetimes. 

Stories became how we communicated the deepest and most profound observations—about the world and about ourselves. 
We spoke them with words, acted them out in plays, drew them in pictures, wrote them in books, sang them in songs, and made them into movies. 



A spirit is an animating principle or a set of animating principles 
A universal spirit is the same set of animating principles animating in many people simultaneously 
Biblical corpus lays out a sequence of narratives, each stress a different ultimate unity


Vol 1 - Media: The Unvaccinated are Scum https://www.bitchute.com/video/kbS9zMQPS262/
Vol 2 - Experts: Kids Are Going to Thrive in Facemasks! https://grabien.com/file?id=1930991
Vol 3 - Reality is a Conspiracy https://www.bitchute.com/video/vkdX7EZ9Bal6/
Vol 4 - Anthony Fauci, America’s Covid Disinformation Agent https://www.bitchute.com/video/SdjIXQT35728/
Vol 5 - Rules Are For The Lab Rats https://www.bitchute.com/video/OA4hR0ZGC5VH/







