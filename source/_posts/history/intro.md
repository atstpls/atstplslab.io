---
title: intro
date: 2023-10-18 01:00:42
tags: [resiliency, government]
category: [ecosystem, resources]
---


<br>

We are born into a world of incomplete and asymmetric information.  We must discover what gives us the most accurate and coherent version of reality using the information available.

It's a complex system of multi-person games which can be deceptive without the right frame... sometimes you see what look like good results from bad actions and bad results from good actions. Over time, service to others (skills and character) has proven the best path to survival and success. 

How do we deal with suffering and malevolence?  Self-transcendence is proven to be the best way.  Finding meaning and/or a higher cause.

God is the meaning/higher cause.  He created the world and it was good.  Aiming up, long-term, embodying truth is the best path to survival and success.

Substitutes for God like materialism are not viable worldviews

<br>

[Unity](../concepts/unity/) and multiplicity make up the world we live in. 

Consequences have consequences which have consequencs.  It's impossible to compute your way through life. Humans orient ourselves through life using mechanisms we really don't understand. 

The structures that motivate ourselves are alive.  We embody spirits, personalities

Modern Homo sapiens arose 200,000 years ago, the product of 3.5 billion years of adaptive evolution.

Humans learned to use [Sacrifice](../concepts/sacrifice/) to survive in the [Physical World](../concepts/physical-world/) and searched for [Meaning](../concepts/meaning/) in the [Spiritual World](../concepts/spiritual-world/) 

[Inequality](../concepts/inequality/) enabled teamwork and group survival. Humans learned [Reciprocity](../concepts/reciprocity/) through [Play](../concepts/play/) and developed [Religion](../concepts/religion/), [Morality](../concepts/morality/) and [Ethics](../concepts/ethics/)

[Symbols](../concepts/symbols/) were used to communicate [Truth](../concepts/truth/) as well as [Identity](../concepts/identity/). [Myths](../concepts/myths/) contained valuable human knowledge and experience.

It was discovered that [Value](../concepts/value/) is derived from utility and scarcity and [Money](../concepts/money/) can be used to represent value. [Economics](../concepts/economics/) facilitated trade and growth. [Ownership](../concepts/ownership/) and [Free Speech](../concepts/free-speech/) paved the way for democracy.

Humans developed [Philosophy](../concepts/philosophy/) and then [Political Philosophy](../concepts/politcal-philosophy/) and learned to [Debate](../concepts/debate/) ideas and identify [Fallacies](../concepts/fallacies/).

Humans worked through many [Social Issues](../concepts/social-issues/) while dealing with problematic components of human nature such as [Luciferian Intellect](../concepts/luciferian-intellect/), [Spirit of Cain](../concepts/spirit-of-cain/), and [Dark Tetrad](../concepts/dark-tetrad/).




[Quantum Mechanics](../concepts/quantum-mechanics/)


[Materialism](../concepts/materialism/) claims there is only the Physical World.

[Inflation](../concepts/inflation/) resulted in [Government Overreach](../concepts/government-overreach/) and [Government Corruption](../concepts/government-corruption/)

[Kayfabe](../concepts/kayfabe/) was used to hide certain truths.

[Coopetition](../concepts/coopetition/) and [Wokecraft](../concepts/wokecraft/) used to achieve goals 

[Critical Theory](../concepts/critical-theory/) claimed life was a power game 

[Censorship Industrial Complex](../censorship-industrial-complex/)

[Censorship](../concepts/censorship/) and [Disinformation Campaigns](../concepts/disinformation-campaigns/) were used to hide truth 



- [2008 Recession](../events/2008-recession/)
- [Biden Laptop](../events/biden-laptop/)
- [COVID](../events/covid/)
- [Declaration of Independence](../events/declaration-of-independence/)
- [Financial Crisis](../events/financial-crisis/)

- [January 6th](../events/january-6/)
- [Laser Incident](../events/laser-incident/)
- [Twitter Files](../events/twitter-files/)
- [Analysis of America](../events/analysis-of-america/)
- [Analysis of Bears](../events/analysis-of-bears/)
- [Analysis of Marxism](../analysis-of-marxism/)
- [Analysis of Media](../analysis-of-media/)

 
### NOW 

We no longer rely on communities and deep understanding of the terrain 

- dangerous supermarket products 
- healthcare system focused on symptoms and profits 
- disintegrating social safety net 
- social issues aa excuses for violence and anarchy 


Our species' pace of change now outstrips our ability to adapt.  We're generating new problems at a new and accelerating rate and it's making us sick--physically, psychologically, socially, and environmentally. It's the problem of accelerating novelty.

