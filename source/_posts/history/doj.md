---
title: doj
date: 2023-01-14 14:00:42
tags: [debate]
category: [reference]
---

<br>

A mutual exchange of support, emotional investment, care, and love.


##### Processes

|||
|-|-|
|[Collusion with Big Pharma](#collusion-with-big-pharma)|Hiding origin/purpose of virus, coordinated response and health policies with for-profit vaccine stakeholders|
|[Collusion with Big Media](#collusion-with-legacy-media)|Suppressed authentic virus info and treatments, pushed vaccine as a safe cure, FBI Cover-Up of Hunter laptop | 
|[Mismanagement of Monetary Policy](#mismanagement-of-monetary-policy)|Printed money to profit hedge funds and take more money from citizens without raising taxes|
|[Unconstitutional Mandates](#unconstitutional-mandates)|COVID Vaccine Mandates, lockdowns, masks, testing, treatment|
|[Weaponize Government Agencies](#weaponize-government-agencies)| Intimidating whistleblowers, pressuring social media companies to police domestic speech on their platforms|
|[Illegal Funding](#illegal-funding)|Dark money funding, money laundering through ACTBLUE|

<br>

##### Objectives

|||
|-|-|
|[Diversity Equity Inclusion](#diversity-equity-inclusion)|DEI hires people based on their race, sex, ethnicity which is discrimination.  It is not merit-based |
|[Climate Change Policies](#climage-change-policies)|Flawed evidence supporting causes and solutions of climate change |
|[Censorship Policies](#censorship-policies)|Used to shape and censor opinions, propagate lies, and hide the truth|
|[Gender Affirmation Care](#gender-affirmation-care)|In reality this is the sterilization and mutilation of children below the age of consent |
|[Critical Race Theory](#critical-race-theory)|An effort to change history and seek retribution using false claims of systemic racism | 
|[Social Emotional Learning](#social-emotional-learning)|Used in public schools to indoctrinate children with ideologies of critical theory |



### Collusion with Big Pharma 

|||
|-|=|
90 % of USDA funded by Big Pharma 

social emotional learning and critical consciousness 

### Collusion with Big Media 


shaping and censor opinions rather than report them 

2. same stories, same narratives, same sources, same phrases 

3. media literacy is the new thing, trying to be the official expert, but they aren't 


### Unconstitutional Mandates 

<br>

|||
|-|-|
|Nov 2021|Biden Vaccine Mandae Unconstitutional Infringement on Fundamental Rights of American Citizen|

<br>

### Weaponize Government Agencies 

<br>

|June 2024|Indicted Whistleblower Eithan Haim Sterilization Surgeries on Children |
|Jan 2024|DHS Weaponization of Taxpayer Dollars to Leftist Academics to Create Propaganda Under Guise of Media Literacy|
|Dec 2023|FBI Targeting of Traditionalist Catholics|
|Nov 2023|DHS Reassigning Hundreds of Agents Assigned to Child Exploitation to Menial Tasks on Border|
|Apr 2023|DOJ Targeting and Arrests of Catholics|
|Mar 2023|DOJ Aggressive Measures to Arrest Pro-Life Activist Mark Houck Despite Offers To Cooperate|
|Aug 2022|AG Merrick Garland on Overruling Operators to Raid Mar-a-Lago|
|Nov 2021|DOJ Using Counterterrorism Division to Investigate Parents Speaking Out at School Board Meetings|
|2016 - 2017|Steele Dossier, ODNI Report, Mueller Report to Implicate Trump-Russia Collusion|

<br>

### Obstruction of Justice 

<br>

|||
|-|-|
|Feb 2024|AG Merrick Garland held in Contempt for Refusing Release Audio of Biden Classified Doc case | 
|Jan 2023|DHS Partisan Double Standard in Biden Classified Docs Probe |
|Oct 2020|FBI Cover-up of Hunter Laptop, Suppression and Disinformation Campaign|

<br>

### Illegal Funding 

<br>

|||
|-|-|
|May 2024|Ignore Illegal Dark Money Funding of Coordinated Antisemtic College Protests|

<br>

### Efforts to Subvert Voting Process

<br>

|||
|-|-|
|Mar 2023|DHS Markets New Phone App CBP One Allowing Unauthorized Immigrants to Cross Border Easily|
|Apr 2022|DHS Rollback of Title 42 Allowing Immigrants To Cross Border|

<br>



|Jun 2022|DHS Disinformation Governance Board Plans with Big Tech |
|Feb 2022|DOJ Lenient Sentence for BLM Rioter Montez Terriel Lee|



- Biden Unconstitutional Vaccine Mandates 
    - Vaccines Cause Harm 

- Weaponization of Agencies to Support Gender Ideology
    - Gender Ideology Causes Harm 

- Failure to Secure Border/Immigration
    - Unmanaged Immigration Causes Harm

- Government Collusion with Censorship Industrial Complex 
    - Collusion and Censorship Causes Harm 



- 2016 Election Interference 
    - Dec 2016 - Steele Dossier paid for by Hillary Campaign and DNC ( Trump colluded with Russia )
        - Read Steele Dossier ? 
    - Jan 2017 - ODNI (NSA/CIA/FBI) Report  ( Putin favored Trump )
        - Read report ?
    - Mueller Report 
        - Read Mueller Report ? 



### Social Emotional Learning 


Zero Tolerance is a discipline policy with clear expectations and predetermined consequences, including expulsions, suspensions, and referrals to law enforcement. Offenses such as possession of weapons, drug violations, or violent behaviors were subject to zero-tolerance consequences regardless of race or identity.

Restorative Justice is a discipline policy which removes predetermined consequences and replaces them with a relationship-building approach that focuses heavily on social issues, particularly race. Asserts undesirable behaviors from marginalized individuals are not the fault of the offender, but rather the result of our racist and oppressive society.

When disruptive behavior occurs, hold entire claass accountable, conduct "community circles" (Maoist struggle sessions)
Use mental health interventions to further brainwash students into believing an oppressive America is responsible for undesired behaviors of marginalized individuals 
Continually evaluate if students are demonstrating empathy towards those exhibiting violent behaviors and acknowledgeing their role in racism by examining their own unconscious biases 
Students who misbehave wiht consistent outbursts, including violent episodes are left in the classroom, which creates chaos, interruptions, anxiety and fear 

Social Emotional Learning uses ongoing lessons, discussions, and counseling to reinforce the belief that society is responsible for the undesired behaviors of marginalized individuals. These interventions aim to reshape students' perspectives and foster a critical race theory, anti-American viewpoint.

Replacing zero-tolerance policies with Restorative Justice was an intentional assault on dismantling our government K-12 system. Instead of creating safer, more high-achieving school environments, RJ has led to increased violence, chaos, and a decline in overall school safety. 


|||
|-|-|
|2014|Zero-Tolerance discipline policies were determined racist and oppressive which led to disproportionate discplinary results for students of colr. Department of Education responded by issuing guidance replacing zero-tolerance policies with Restoratative Justice (RJ)|
|2016|Schools in all 50 states began adopting Restorative Justice policies because this practice is a key component of Social Emotional Learning (SEL)|
|

