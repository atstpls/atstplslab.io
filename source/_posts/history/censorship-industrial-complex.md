---
title: censorship industrial complex
date: 2023-01-19 18:05:42
tags: [fiat, crisis]
category: [ecosystem, events]
---

<br>


The <b>Censorship Industrial Complex</b> is a network of ideologically-aligned governmental, NGO, and academic institutions that discovered over the last few years the power of censorship to protect their own interests against the volatility and risks of the democratic process.

They do not publicly engage with their opponents in an open exchange of ideas.

Rather, they create blacklists of disfavored people and then pressuring, cajoling, and demanding that social media platforms censor, deamplify, and
even ban the people on these blacklists.

Run influence operations aimed at discrediting factual information under the guise of "fact-checking"

Push social media platforms to become more traditional news media companies, whether newspapers or TV networks, which the national security state in the past has been much better
able to control.

In fact, with Section 230 protection, the censorship industrial complex may exert significantly more control over social media platforms, with their strict legal liability, than over news organizations.

They are not "defending democracy" as they claim. Rather they are defending their own policy and pecuniary interests against democracy.

<br>

##### Mechanics

|||
|-|-|
| <b>US Government</b> |- Funded organizations that pressure advertisers to boycott news media organizations and social media platforms that refuse to censor and/or spread disinformation|
| <b>Foreign Influence Task Force</b><br>(FITF), a cyber-regulatory agency comprised of members of the FBI, DHS, and ODNI|
|<b>Global Engagement Center</b><br>(GEC)|An analytical division of the U.S. State Department which systematically launders domestic censorship by working through "counter-disinformation" NGOs and foreign firms|
|<b>Cybersecurity and Infrastructure Security Agency</b><br>(CISA)| Congress created CISA in November 2018 to defend the U.S. from cybersecurity threats from hostile foreign actors. Began viewing "election infrastructure" as "critical infrastructure," opening up CISA’s mission to censoring alleged "disinformation"|
|<b>Stanford Internet Observatory</b><br>(SIO)|Government-funded organization that mMonitors social media and promotes Internet censorship|
|<b>University of Washington</b><br>(UoW)|Government-funded organization that mMonitors social media and promotes Internet censorship|
|<b>Atlantic Council Digital Forensic Research Lab</b><br>(DFR)|<br>Government-funded full-time censorship institution and research lab that monitors social media and promotes Internet censorship<br>- Created the foreign-facing DisinfoPortal in June 2018, working directly with the National Endowment for Democracy (NED) and 23 organizations to censor election narratives leading up to the 2019 elections in Europe<br>- In 2018, Facebook named Atlantic Council, an official partner in "countering disinformation" worldwide
<br>- US taxpayer funding to the Atlantic Council comes from the Defense Department, the US Marines, the US Air Force, the US Navy, the State Department, USAID, the National Endowment for Democracy, as well as energy companies and weapons manufacturers|
|<b>Graphika</b>|A private network analysis firm, published a report for the Senate Intelligence Committee in December 2018, which claimed Russia's interference in 2016 U.S. presidential election. DoD Minerva Initiative which focuses on psychological warfare, and DARPA, both gave grants to Graphika. In 2021, the Pentagon awarded nearly 5 million in grants and nearly 2 million in contracts to the organization|
|<b>Election Integrity Partnership</b><br>(EIP)|CISA’s deputized domestic disinformation flagger now known as Virality Project composed of:<br>- Stanford Internet Observatory<br>- Washington University Center for an Informed Public<br>- The Atlantic Council's Digital Forensics Research Lab<br>- Graphika<br><br>It was created in June 2019 by director Alex Stamos and research manager Renee DiResta<br>Partnership with CISA, SIO had 50 "misinformation" analysts assigned to monitor social media<br>In 2020, began demanding censorship on COVID-related information|
|<b>Hamilton 68</b>|A dashboard created with U.S. government funding and the support of New Knowledge claiming to reveal Russian bots on Twitter but was mocked by Twitter staff because all or almost all belonged to American citizens|
|<b>Internet Research Agency</b><br>(IRA)|The infamous Russian "troll farm" headed by "Putin’s chef", Yevgheny Prigozhin|
|Moonshot CVE|Private firm to redirect right-wing people online away from radicalism27 but was found to have pushed right-wing people toward an
anarchist leader. "They sent people who were already looking for violence to a convicted felon with anarchist and anti-Semitic views," Rep. Morgan Griffith (RVa.) said to Google’s CEO. "Who is vetting the vetters? We continue to need more transparency and accountability." Moonshot includes Elizabeth Neumann, former DHS Asst. Sec. for Counter Terrorism|
|<b>Graham Brookie</b><br>Leader of the Atlantic Council’s DFR Lab|Brookie served in the Obama White House on the National Security Council|
|<b>Renee DiResta</b><br>Stanford Internet Observatory|DiResta was the research director for the organization caught creating bot accounts and spreading disinformation about Alabama Republican Senate Candidate Roy Moore. In her 2018 Senate testimony DiResta advocated "legislation that defines and criminalizes foreign propaganda" and allowing law enforcement to "prosecute foreign propaganda." According to recorded remarks by DiResta’s supervisor at Stanford, Alex Stamos, she had previously "worked for the CIA."|
|<b>Jen Easterly</b><br>CISA Director|A former military intelligence officer and the National Security Agency (NSA) deputy director for counterterrorism. "One could argue we’re in the business of critical infrastructure," said Easterly in November 2021, "and the most critical infrastructure is our cognitive infrastructure, so building that resilience to misinformation and disinformation, I think, is incredibly important."39 The month before, Easterly said during a CISA summit that Chris Krebs's construction of a "counter-misinformation" complex with the private sector was a high priority for DHS.40 A U.S. District Court ruled in October of last year that Easterly could be deposed because of her "first-hand knowledge" of the CISA "nerve center" around disinformation.|
|<b>Chris Krebs</b><br>CISA Director (2018 to 2020)|Chair of Aspen Institute "Commission on Information Disorder," helped organize DHS’s "wholeof-society" approach to censorship. Krebs administered the federal side of the 2020 election after DHS effectively nationalized election security on January 6, 2017, via the declaration of elections as "critical infrastructure." Krebs then declared that "misinformation" was an attack on election security. Krebs said in April 2022 that the Hunter Biden laptop still looked like Russian  isinformation and that what mattered was that news media did not cover the laptop during the 2020 election cycle. Advocated for censoring critics of government COVID-19 protocols44 and said "misinformation" is the largest threat to election security.|
|<b>Ben Nimmo</b><br>Head of Global Threat Intelligence for Facebook|Nimmo was the technical lead for censorship at the Atlantic Council's Digital Forensics Research Lab, was employed by Graphika in the fall of 2020 46, and worked in NATO information operations.47 In 2018, Nimmo publicly reported an anonymous Twitter account, "Ian56", as a Russian disinformation bot account because it expressed left-of-center populist anti-war views when in reality Ian56 was a real person.48 After Nimmo's report, "Ian56" was reported to the UK government|
|<b>Kate Starbird</b><br>UoW Disinformation Lab|Runs the University of Washington disinformation lab, has for years been funded primarily by U.S. government agencies to do social media narrative analytics of political groups, or insurgency movements, of interest or concern to U.S. military intelligence or diplomatic equities. Starbird acknowledged that the censorship focus of CISA and EIP moved from "foreign, inauthentic" social media users to "domestic, authentic" social media users between 2016 to 2020. Starbird is now the head of CISA’s censorship advisory subcommittee.|
|<b>Alex Stamos</b><br>Former senior leader at EIP and VP| Served as deputized domestic "disinformation" flagger for DHS via Chris Krebs’ CISA. Stamos in 2020 proposed that DHS  centralize government censorship. Stamos was the Chief Security Officer of Facebook and led Facebook's response to alleged Russian disinformation after the 2016 election. Stamos left Facebook, now Meta, in 2018, after reportedly conflicting with other Facebook executives over how much to censor. Stamos says he favors moving away from a free and open Internet toward a more controlled "cable news network" model. A huge part of the problem is "large influencers," said Stamos|
|<b>Claire Wardle</b><br>Cofounder/Director, First Draft News|Founded and ran First Draft News, a nonprofit coalition, in June 2015, to build the censorship complex. "In September 2016, our original coalition expanded to become an international Partner Network of newsrooms, universities, platforms and civil society organizations." In 2017, while at the Shorenstein Center for Media, Politics and Public Policy at Harvard’s Kennedy School, Wardle helped develop the "Information Disorder Lab," a framing that Aspen Institute would embrace. In June 2022, First Draft closed, but its work lives on at the Information Futures Lab at Brown University’s School of Public Health.

<br>

##### Analysis

The Censorship Industrial Complex is not "defending democracy" as they claim. Rather they are defending their own policy and pecuniary interests against democracy.

Censorship by social media platforms at the behest of government officials, government contractors, and ostensibly nonpartisan scientists and experts. Under pressure from governmental and nongovernmental actors, Facebook, Twitter, and other social media platforms have censored true or potentially true information


1. Relentless demand for censorship (Twitter, Facebook)
2. Directing government and philanthropic money toward research and advocacy for greater censorship of social media platforms (News Guard, Disinformation Index)
3. seek legislation that would give increasing control over the content moderation of social media platforms to establishment experts and elites
4. seek to direct advertiser revenue away from disfavored news media corporations and toward favored news organizations
5. deplatform disfavored individuals, labeling them "superspreaders" of disinformation.
6. increase public comfort with growing censorship with videos, podcasts, reports, and opeds in newspapers

<br>

- Biden Administration & EIP/Virality Project pressured Twitter and Facebook to censor accurate COVID information to reduce vaccine hesitancy and discredit doctors and other experts who disagreed
- Aspen Institute Workshop Trains Top Journalists To Pre-Bunk "Hack and Leak"
- Masks do not work, have bad effects on people/kids
- White House and Congress regularly threatened to revoke Section 230 of the Communications Decency Act, which indemnifies social media platforms from liability for content posted by users. The social media platforms consider the possible repeal of Section 230 an existential threat. 
- Facebook, Twitter suppressed Hunter Biden laptop story 2 weeks before election
- Free speech enables free markets , discuss products openly
- few restrictions:  can't lie to steal through fraud, can't incite violence
- strongest anywhere in the world

<br>

Cognitive Security Collaborative and Adversarial Misinformation and Influence Tactics and Techniques. These are online platforms for describing and coordinating disinformation attacks.