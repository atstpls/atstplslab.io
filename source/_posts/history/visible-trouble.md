---
title: visible trouble
date: 2023-01-18 03:00:42
tags: [trading]
category: [ecosystem, concepts] 
---

<br>


### Motte and Bailey

Motte is a morally sound, highly defensive tower from which they throw racist stones and dump fascist oil on anyone not in favor of the tower 

"Trans people exist and deserve human dignity"

Bailey is indefensible activist playground built on morally reprehensible or perverted ground 

"Intergenerational grooming, rapes, trans women in women prisons, binders, puberty blockers, etc"


### Framing 

Choose the right word or words that evoke the proper values and you make argument against them nearly impossible. 

“What do you mean, you’re against Inclusion!?”


embedded within are ulterior motives and deeper meaning.

f magic is the “art of influencing events and producing marvels using hidden natural forces”.

Framing, at the highest level is akin to the circumference of a Wizard’s Circle

When you’re in the Wizard’s Circle, you are in an alternate reality. You cannot see outside of it. The world whirls around you and it’s funky looking because you’re in an altered reality. Your very consciousness is Affected.

### German Idealism

Hegel's Great Men exercising Nietzche's Will to Power 

The Fascist and Communist ideologies are twins, like Romulus and Remus. One bad, one badder. Who’s who? That depends on your Patriotism: you like open borders or strong nations? Commies are international collectivists, who know no national borders, Fascists are national collectives who know no borders.

ll of us are wrapped up in Hegel’s Polar Dialectic (which doesn’t exist) seeing CCP Commies on one side and Nazi Fascists on the other, when in reality they’re one and the same, just different flavors - Nation first, build the Internationale or International Proletariat Becoming the Global Citizen. There isn’t even a choice as to the end result they are the same damn thing.

### Marx and Hegels Hermetic Wizardry

I think this Spell is being cast all across the planet. The UN, WEF, EU, US Government, NATO, the US Military and the coming Amazing and Mesmerizing Circularly-Sustainable Stakeholder Capitalist economy where you vill eat zi bugz and you vill be happy. This framework is a religion and it’s everywhere.

In order to build your Critical Consciousness you gotta dance with those 7 Deadly Sins. And then you ruminate on them. And then you Act upon those Ruminations. That is the gnostic meditation of Critical Consciousness.

People going through DEI training and all people being “educated” are being indoctrinated into a Gnostic Hermetic Cult with the express goal of overthrowing Western Liberalism and paving the way for Socialist Man and for Humanity’s Becoming of the Species-Being.

### Transfaction 

Transfaction is the intentional or unintentional swapping of Features for Properties. This creates a fallacy that is often used as dialectical leverage to move your mind and change your thoughts.

The Individual and the Self are different expressions for a human being. The Individual holds the Properties of the Human Being and the Self holds the Features of the human being. The Individual has immutable characteristics, the Self has aspects.

Properties: Properties are essential characteristics that are immutable and defining of the object or idea.
Features: Features are inessential aspects or additions to an object that are mutable and may or may not be inherently relevant to the object or idea.

|Constrains (Has Properties)|Affords (Has Features)|
|-|-|
|Individual|Self|
|Sex|Gender|
|Government|State|
|Business|Brand|
|Education|SEL|
|Hospital|Healthcare Community|
|Military|Defense Community|
|CIA|Intelligence Community|
|Liberty|Freedom|
|News|Narrative|
|Money|Wealth|
|Norms & Mores|Culture|
|City|Community|

<br>

### Four Arch Angels 

ESG - Environment, Sustainability, Governance 
DEI - Diversity, Inclusion, Equity 
SEL - Social Emotional Learning 
SDG - Social Development Goals 

https://visibletrouble.substack.com/p/the-17-q-solved-stakeholder-gnosticism?utm_source=profile&utm_medium=reader2


### Social Media 

A steady stream of information, a constant churn of highly personal and emotionally charged information designed to shape and intercept behavior hammers your mind.

My Digital Self, and remains hidden behind the attention grabbing creativity and ideation perpetually shooting emotively charged posts, arguments, and news stories at us — moving minds and shifting moods and peer groups in a single moment, post, or headline. This is the magic of the SoME medium. 

VisibleTrouble’s mission is disclosing the affordances of mental manipulation so that people can have some chance of making good decisions based upon logic as well as emotion. The Hidden Ground of SoME is a mindfield, by design mind you, of emotional programming, hijacking, and manipulation.

We can be mesmerized by media or participate in it’s construction. During his time, 1950s-70s TV was inferior picture and quality, it didn’t mesmerize people like the big screen. But what it did do was ask the viewer to construct the picture. Our participation in media is important to it’s value and impact on us. 

The other notable idea from TV and Film by McLuhan is the idea that the film is a reflected surface that we watch - we look out there into the perceptual field, at  the screen, like normal reality. But with the TV we field the message. It comes to us we are fielding a signal - it’s magic, lighting up and shooting the image at us. We’re IN the TV. We don’t look around at the screen in front of us, we’re in it - from multiple perspectives. 

The External Locus of Control of what gets put in your head is so bad we can’t even choose the order of display. Algorithm chooses it, we scroll it. Ban/Block/Mute, etc? Doesn’t matter… you can’t unring that bell. 

There is a -20 pt impatt on IQ from strong in-group identification. Relevance and identity of your SoME, your superpowered avatar to the outside world is completely dependent upon continued group affirmation. That’s a perverse incentive and a very useful tool.

The effects of this constant barrage of emotionally charged content is exhausting. It does have effects on your actual Self, and has ended more than a few friendships and familial relationships over the last few years.

Narrative insertion, dialectic opportunities, and behavioral intercepts create a situation where we’re starting to see some potential mass manipulation

e’re Becoming the Product. We’re being made, constructed, through our lack of Awareness and our inability to mind our Attention and to attend to our mind. We’re being shaped to accept, to act, and to believe

### Grounding the Dialectic

transfactions create fallacies that are often used as dialectical leverage to move minds and change thoughts

Hegel’s Dialectical Spring™. It’s a model that discloses how the dialectic can be used to radically alter the intellectual landscape. This model depicts an instrumental application of the dialectic, the kind of application intersectional marxists, reactionaries, and other collectivists employ

The dialectic as depicted here has the well reasoned arguments happening in the center of the spring. Normal people think we’re having an argument across this spring, after all that is the thesis and antithesis. But what normal, charitable Westerners don’t know is that the real goal of the dialectician is to get to the top of the sprin

here are 7 moves that intersectional marxists have, and 3 of them are transfactions. We’ll be sticking to the Transfactions, but here’s their whole bag of tricks:

Marrying a Truth to a Lie

DARVO - Deny/Attack/Reverse/Victim & Offender

Motte & Bailey

Transfaction of Knowledge

Transfaction of Meaning

Transfaction of Experience

Redefinition of Terms


o understand something explicitly you have to know the properties. Explicit knowledge is required for intentionality, recall, strategy and tactics, and troubleshooting.

Explicit knowledge requires thinking. It’s slower than direct perception of reality. Experienced Affordance use will, more often than not, outpace thinking. And if you’re thinking that they’re actually thinking… it’s not gonna go well for you - you’re gonna get a read and throw a punch and they’re not gonna be there. They’re not thinking, they’re reading the environment and playing by feel.

hen your theory of knowledge runs into trouble in the actual world, in your lived experience, cognitive dissonance arises. This is uncomfortable. Humans like reality to match what we know and we’ll go through all kinds of mental and emotional gyrations to ensure that this is the case.

As an epistemology, a theory of knowledge, or as the extent of what we know, this is completely insufficient. It’s naval gazing. Lived Experience is context dependent, and because it happened in the actual world (complex domain), we can’t predict it will hold true in other contexts until after the fact. To assert that it will is to attempt to control reality.

Manipulating inner significance shapes perception of external reality, influencing belief in internal truths.

Shaping external events aligns with inner significance, reinforcing belief in perceived truths.

Rather than events/entities having some innate, immutable Being or essence, the wizard's transfactive abilities reduce them to functional affordances - opportunities to project or fabricate meanings, instrumentally, in service of ulterior motives.

The ontological dimension flattens out, stripped of depth and substance. Events and occurrences are transfigured into plastic utility surfaces for wizard circles and confusion spells.

Given this prioritizing of affordances over any intrinsic properties or essences, the world is turned into a machine only bearing the facade that dialectical agents like the wizard have grafted upon it.

So in essence, the transfaction of Meaning creates a world where Being and meaning itself are just a fungible resources, perpetually liquefied and recycled through predatory dialectics.

Transfactions of Experience provide the illusion of profundity and are often terribly difficult to suss out. This illusion of profundity, artificially transcends the argument by compressing the spring the distance between the spiraling levels decreases. Compress it hard enough and the coils touch. This is artificial transcendence.

### Hegel Spring of Progress 

Hegel envisions history and reality itself as following a dialectical process of thesis > antithesis > synthesis in an upwardly spiraling pattern. Each syntheses becomes a new thesis that gets challenged by its antithesis, allowing further progression.

This resonates with the ouroboros symbolism, where the snake continually consumes itself and re-creates itself anew in a circular, self-feeding cycle. Representing the affirmative, progressive unfolding at work in the dynamic of thesis > antithesis > higher-level synthesis.

Hegel's primary driving force of the dialectic was the unfolding of self-consciousness and philosophical ideas over time. His was an idealist dialectic focused on the realm of thought, spirit and mindedness.  It is an abstract, top-down model disconnected from the material realities of the oppressed working class.

Marx famously turned Hegel "right side up" by grounding the dialectic in the material conditions and relations of production. For Marx, it was the tensions and contradictions within economic systems and class struggles that provided the true locomotive force of historical progress. Marx's dialectic centered on lived experiences, labor, and the standpoint of the proletariat. It was grounded in the harsh material world, not the lofty realm of abstracted philosophical thought.

Hegel builds to the Absolute, Marx strips to the Ground. 

### Alchemical Transmutation 

Stages of the alchemical transmutation of the Enlightenment from Sovereign Individual into an Idealistic Collective

Stage 1     Descartes 
Stage 2     Kant
Stage 3     Hegel
Stage 4     Marx


#### Stage 1 

Descartes trashed Understanding - he made the arbiter of reality the mind alone. His doubt is more powerful than the sun. Anything could be doubted and nothing could be proven. This was never meant to function as a system of knowledge. This was a direct broadside on Objective Reality and Common Sense and the opening move of an alchemical project to create a Plastic and Fungible Reality.

#### Stage 2 

This paved the way for Kant to "solve it" with ethics and morals, which would be the next thing to go, as they were "not up to the job". Kant pulls a Copernican Revolution by putting the Self at the center of Meaning and smuggles in the idea of Relative Realism in the process. The ethical juggling and inordinate focus upon ethics and morality create perverse incentives for moral relativism and idealistic capture. This creates Ideology, the appearance of thinking, the roots of Pragmatism, and an internal dialectic made softer and more granular via phenomena & noumena.

#### Stage 3

next we get the reality chewing alchemical Dialectic from Hegel which allows for a fungible history and epistemic and ontological control, the State as Divinity, and a goal to reach the “End of History”. Hegel is universally recognized as an impenetrable read, and I’ve found that to be true. 

#### Stage 4 

Marx’s materialist ontology means that Understanding, limits of and and the structure of being are “Socio Economic Structure”. This creates a perpetual Grounding of the dialectic, the conjunction of experiencing and creative intent is brought down to earth and is touchable and moldable,”I did this, I am like God.”



The Seal of Laudato Si

The "Seal of Online Reactance" diagram appears to be a comprehensive map designed to disclose and critique the structure of online interactions and debates, revealing how certain narratives and goals are driven by various methods. The seal integrates metaphysical, ontological, epistemological, and practical dimensions, represented through various components such as the Hive Mind, Digital Avatar, Attention Economy, and Group Identity. This complex diagram likely serves to illustrate the manipulative mechanisms at play in online discourse and suggests methods to counteract them.

The Seal of the NPC 

This iteration of the NPC, focused on the Flexible Targeting System, illustrates a figure that is constantly adjusting its focus, methods, and narratives to maintain ideological control. It weaponizes current events, uses linguistic perversion, and redefines history to ensure that its flexible, adaptive system remains dominant. The NPC’s ability to shift targets, narratives, and values keeps it relevant and in control, while its pragmatic approach ensures it can always justify its actions in the name of future vindication.

This construction symbolizes the modern ideological state, where truth is fluid, and language, science, and history are continually reframed to suit the needs of the moment.

### Synthetic Negation 

From the lens of Western esotericism, Shiva and Zurvan embody the alchemical and Hermetic principle that all destruction leads to transformation, and all opposites eventually dissolve into unity. Synthetic negation aligns with the constructive dismantling of forms and cycles of time necessary for new levels of reality to emerge. These gods show that to become whole, one must first dissolve, and that true mastery lies not in avoiding opposites but in working through them—in engagement with the polar dance that generates reality itself.

Both Shiva and Zurvan stand as archetypes of esoteric praxis, revealing that the true path of transformation lies in embracing negation as part of a continuous, cosmic synthesis—a dialectic in action, where resolution emerges not from stasis, but from the ever-evolving dance of opposites.

### Chain of Being 

This framework explores the dynamic interplay between the Ideal and the Actual realms of existence, conceptualized through the Chain of Being. It identifies eight distinct "moves" that represent the processes by which knowledge, values, and beliefs transition between these realms. These moves are categorized under two overarching themes:

Illumination: Gnosis from Above (Ideal to Actual)

Creativity: Gnosis from Below (Actual to Ideal)

Understanding these moves provides insight into how we develop, apply, and evolve our metaphysical beliefs, methods, values, and ethics in response to our experiences and understanding of reality.

The Chain of Being is a conceptual framework that represents different levels of reality, ranging from the most abstract (Ideal) to the most concrete (Actual):

|||
|-|-|
|Metaphysics (Belief & Meaning)|The realm of abstract beliefs, meanings, and foundational principles|
|Right Methods (Process)|The processes and methods derived from or leading to metaphysical beliefs|
|System of Values|The values and principles that guide behavior, derived from or informing metaphysical beliefs and methods|
|Practical Ethos (Ideal Ground of Being)|The ethical principles and mores that shape our way of life|
|Ontology (Limits & Structure)|The understanding of reality's fundamental nature, limits, and structures|
|Expected Results (Ontical Ground)|The concrete outcomes and manifestations in the actual world|

<br>

### Magnum Opus 

The Magnum Opus, or "Great Work," in alchemy refers to the process of working with the prima materia to create the philosopher's stone, symbolizing spiritual transformation and enlightenment. This process is traditionally divided into four main stages:

Nigredo (Blackening): Decomposition and dissolution of existing structures.

Albedo (Whitening): Purification and washing away of impurities.

Citrinitas (Yellowing): Spiritual awakening and dawning of new understanding.

Rubedo (Reddening): Completion, wholeness, and the attainment of enlightenment.

1. Nigredo 

Nigredo represents the initial stage of breakdown and dissolution, where existing forms are deconstructed to their basic elements.

René Descartes initiated a profound philosophical shift by applying radical doubt to all prior knowledge, effectively "blackening" or deconstructing established understandings.

Trashing Understanding: Descartes questioned the reliability of sensory experience and traditional authorities, reducing knowledge to a fundamental certainty: "Cogito, ergo sum" ("I think, therefore I am").

Duality: He introduced Cartesian Dualism, the strict separation of mind (res cogitans) and body (res extensa), fundamentally altering the conception of reality and self.

Interpretation: Descartes' method parallels Nigredo by dissolving existing knowledge structures, setting the stage for a new foundation in philosophy.

2. Albedo 

Albedo signifies purification, where the material is cleansed of impurities, leading to clarity and illumination.

Immanuel Kant sought to resolve the issues raised by empiricism and rationalism, critically examining the limits and capacities of human reason.

Trashing Meaning: Kant argued that we cannot know things-in-themselves (noumena) but only things as they appear to us (phenomena), mediated by the structures of our mind.

Synthesis as Fact: He introduced the idea that the mind actively synthesizes experiences using innate categories, effectively "purifying" knowledge by acknowledging the mind's role in shaping reality.

Interpretation: Kant's work corresponds to Albedo by cleansing philosophy of misconceptions about objective knowledge, illuminating the processes by which we construct meaning.

3. Citrinitas 

Citrinitas represents spiritual awakening and the manifestation of enlightenment, often associated with the dawning of a new consciousness.

Georg Wilhelm Friedrich Hegel developed a dialectical method where ideas progress through a dynamic process of thesis, antithesis, and synthesis.

New Telos (Purpose): Hegel proposed that history is the unfolding of the World Spirit realizing itself, moving towards greater self-awareness and freedom.

Collective Self and Becoming: He emphasized the importance of the collective in achieving self-consciousness, viewing individuals as part of a larger historical and social process of Becoming.

Interpretation: Hegel's philosophy aligns with Citrinitas by heralding a new era of collective understanding and self-realization, awakening a higher purpose in the development of human consciousness.

4. Rubedo 

Rubedo is the culmination of the alchemical process, symbolizing transformation, unification, and the realization of the philosopher's stone.

Karl Marx took Hegel's dialectical framework and applied it to material conditions, formulating dialectical materialism.

Results: Marx focused on tangible societal change, aiming to resolve the contradictions between the proletariat and bourgeoisie.

Becoming: He envisioned a classless, communist society as the end goal of historical development—a realization of humanity's potential.

Interpretation: Marx represents Rubedo by seeking to actualize the transformative insights of previous philosophical developments into concrete social outcomes, achieving the "reddening" or maturation of human society.

**Conclusion**

"Plastic Reality" suggests a malleable or moldable conception of reality, shaped by human thought and action.

Descartes initiates the deconstruction of old paradigms, questioning the very foundations of knowledge.

Kant purifies this understanding by exploring how the mind structures experience, redefining the relationship between the observer and the observed.

Hegel introduces a dynamic, collective process, where reality is continually shaped and reshaped through historical dialectics.

Marx seeks to solidify these transformations into material reality, emphasizing praxis and the actualization of philosophical ideals.

Overall Interpretation:

This progression mirrors the alchemical transformation, with each philosopher contributing to the evolution of thought and society.

The stages represent not only individual philosophical contributions but also the collective advancement towards a redefined reality shaped by human agency.





### Deductive Nomological Model 

Scientific model uses it, You make a hypothesis, deduce to make predictions, then compare with reality to see if they are expected 

Social science tried to use it, but couldn't explain why Hitler / Mao / Stalin did what they did 

Requires an Understanding model rather than an Explanation Model 

### Understanding Model 

Show how social ontology (money, private property, etc) is consistent with physics and chemistry and is a natural consequence

Universe is physical particles, big things made out of little things,  some are organized into systems (collection of particles with boundaries)

Carbon-based organisms evolved to have neurons, synapses, receptors, which produce conscisousness. 

Intentionality is directedness of the mind, beliefs, desires, emotions, they're about something 

Speech acts must have intentionality. Society is created by linguistic representation.

Distinguish the type of content and the type of speech act 

Assert, Command, Request, Promise

Belief, Hope, Fear, Worry 

Intentional state is a representation of its condition(s) of satisfaction (desires fulfilled or not, commands obeyed or not, beliefs true or false)

Sometimes a speech act presuppose a fit (apologies, condolences, congratulations) you take for granted something happened 


- Intentional action comprised of conscious experience of acting causing a bodily movement (mental actions included)


- Collective intentionality 


Freedom of will only applies to action, not perception 

Cooperating with others is built into our biology 

You have reasons for actions (beliefs, desires, etc) which lead to the formation of a prior intention (decision) which leads to formation of an intention in action (mental component of action, trying) which causes bodily movement.

You can have an action with no prior intention, 

1. causal relation - 
2. constituted relation - common where social human behavior is concerned (raising hand constitutes voting, it doesn't cause it) 


Collective Intentionality - not on your own, but by way of cooperating with others 
    - have cooperative forms of causes (voting for president)
    - operation of society and social movements 

You know you're cooperating, but you don't know what others intentionality
Individual makes individual contribution to the team effort 

Cognition tells you how the world is (sensory system)
Volition where you plan/act to change the world (motor system) 

Background determines how you see your behavior and behavior of your 


Collective acceptance of a certain status 

Taken for granted presuppositions.

--------------

- Reasons for actions (beliefs, desires, etc)
- Reasons lead to formation of prior intention (decision)
- Decision leads to formation of an intention in action (mental component of action, trying)
- Intention in Action leads to body movement 

There are different levels of description of an action which describe different conditions of satisfactions 

Collective intentionality is irreducible and real 


Sensory and Motor Systems
- You should only get mind-to-world direction of fit only from world-to-mind direction of fit 
- You should only get world-to-mind direction of fit only from mind-to-world direction of fit


Words describe ideas.  All institutions are built on speech acts. 

The mind is an inventory of different variations of intentional states. 

Things common to all cultures are deep background, then we have additional cultural-specific things which is local background 


Collective belief, a mutual belief unites us together














