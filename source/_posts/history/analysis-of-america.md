---
title: analysis of america
date: 2023-09-16 16:00:42
tags: [fiat, crisis]
category: [ecosystem, events]
---

<br>


##### Spectrum of Ideologies


things/ideas - mostly men
people/aesthetics - mostly women

|||
|-|-|
|Economic Inequality        |money is a product of the market not the state|
|Racial Inequality          |culture based on merit works better than basing everything on group identity|
|Gender Inequality          |identity crisis ignores biology and does irrepairable harm|
|Nature & Ecosystem         |more research is needed, not more action |
|Gun Control                |criminals cause problems, not guns--our most effective tool for protection |

<br>

###### Economic Inequality

Main function of money is more than just allowing exchange of goods, It's making sure that every member of economy is accountable to the real wealth that's being produced.

The traditional business cycle is natural result of manipulating the interest rate, distorting the amount of available capital, and incentivizing investors to invest when they shouldn't be investing.

<br>

###### Racial Inequality


###### Gender Inequality

[Gender identity](gender-identity) is a created term claiming sex is socially constructed, mutable, and completely detached from underlying sex of the body

<br>

###### Nature and Ecosystem

###### Gun Control


<br>

##### Ideological Capture

Civic duties/responsibilities abdicated will be assumed by pathologicals and used against you

- university administrations, then college students
- faculty of education, then school boards, then students
- government agencies, then industry regulators
- medical boards, then medical staff, doctors can't say what they think
- pharmaceutical boards, 

<br>

##### Capture Symptoms

- protect healthcare workers instead of patients
- protect teachers instead of students
- protect 
- using kids to promote/support ideology they can't understand ( climate change, gun control, gender identity, critical race theory )
- demoralization of boys/masculinity ( energy, exploration, competition is bad, suppress with drugs )


##### Capture Tactics

Censorship Industrial Complex - clustering of govt agencies and NGOs funding think tanks/universities to censor, disparage, discredit 


#### Biden Award 

pass legislation that improves civil rights of Americans 
- affirmative action, economic equity, living standards
- hate crimes legislation
- expand voting rights 
- reduce gun violence, violence against women 

- in the face of hateful rhetoric or devisive legislation, we cannot remain silent 

