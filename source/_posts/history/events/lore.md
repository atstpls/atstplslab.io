---
title: lore
date: 2023-01-04 19:00:42
tags: [analysis, media]
category: [ecosystem, actors]
---

<br>


##### Beginning

Year of silence after Blurryface album 
Glitched out URL appeared on top of Vessel store page to website called DMAORG: http://dmaorg.info/found/15398642_14/clancy.html 
Link showed letter from clancy
Substituting letters like EASTISUP into URL led to another page containing journals and images
Detailing story of Clancy

##### Clancy 

Clancy is the character played by Tyler. He is a resident of Dema who is famous for writing letters detailing his life in the city. He is also famous as a rebel and member of the Banditos who makes rebellious music. He spent an amount of time a prisoner of Dema being forced to make propaganda, but he has since escaped and is back to being a rebel. He is the only non-bishop who can use psychokinesis. He was last seen on Vøldsoy

Lives in walls of theocratic city ruled by organization made up of 9 bishops called Sacred Municipality of Dema 
People of Dema live under autocratic religion called Vialism
Vialism requires suicide to get to so-called Paradise 

Clancy trapped for 9 years, begins to question Vialism and rule of bishops 
Tries to escape multiple times but Nico, head bishop (Blurryface), tracks him down and brings him back 

One time Clancy convinces Nico to leave city and then destroys car they were travelling in (Heavy Dirty Soul)
Clancy then gets lost in the wilderness of the world surrounding Dema called Trench 

Within 5 days Nico captures Clancy again, uses smearing and black marks appear on neck and hands 
But during time in Trench, Clancy meets Banditos, a legendary rebel group that reside in Trench (Jumpsuit)

Attempts escape again and meets with Banditos
Banditos help him escape during Annual Assemblage of the Glorified, a ritual where most upstanding citizens die 
Makes it to Bandito camp but Nico finds him again and brings him back 

Trench also introduces Ned (Chlorine), small creature whose species grow antlers that can be removed 
Person who gets antlers has power of psychokinesis, a ritual bishops use to possess the dead bodies of the Glorious Gone 
Glorious Gone are those who have committed to killing themselves in the name of Vialism 

On April 5th DMAORG was deleted

Following a year of silence, then led into Scaled and Icy era 



##### Bishops 

Andre, Lisden, Keons, Nico, Reisdro, Sacarver, Nills, Vetomo and Listo 
Blurryface is both Nico and also all of the bishops (and the city) at the same time. Same concept as God being the father and the trinity in Christianity. Clancy theorises they may be immortal beings using psychokinese to swap from body to body. By extension this means Keons may return.


##### Dema

Dema is a city on the continent of Trench. 

It is surrounded by a large circular wall and has nine towers in the middle. It is also split into nine districts with each district being ruled by its own Bishop. The outside of the city is a necropolis, full of neon gravestones. The city is a very depressive place. The album ‘Blurryface’ details life in Dema. Citizens are not allowed to leave the city. Dema also has VIP Citizens who receive special treatment from the Bishops. It is sometimes referred to as the ‘Sacred Municipality of Dema’.

##### Trench

Trench is the ‘inbetween’ it’s the land outside of Dema. Parts of it resemble Iceland. It is where the Banditos have a camp/base. There may be a part of it that is a desert, that also may or may not have a population of cheetahs.

##### Voldsøy:

An island off the shore of Trench. Has a large population of Neds. The island is a reference to the 'island of violence' in the song migraine.

##### Paladin Strait:

The bit of ocean between Voldsoy and Trench.

Port Vial:

A port on the shore of Trench, controlled by Dema.


##### The Torchbearer

This is Josh’s character. He is a member of the Banditos, and even perhaps their leader. His character is very mysterious. He is currently on the Island of Vøldsoy with Clancy.

##### Nico

Nico, full name Nicholas Bourbaki (sometimes also known as Blurryface) is the leader of the nine bishops. He is the scariest.


##### Keons

Keons was Clancys bishop. He is described as more kind and caring than the other bishops. He conspired with Ned, the Torchbearer and Clancy to betray Dema and use Trash to free Clancy and get him to Voldsoy. He was killed by the other bishops before being possessed via Psychokinesis by Clancy and starting a fire in dema.

##### Trash

Trash the dragon is a sea dragon that is speculated to have been long dead. His body was temporarily possessed via Psychokinesis by Keons to attack the submarine carrying Clancy. He represents creativity.

##### Ned

Ned the individual is a cute little creature living somewhere in a cozy cabin on the continent of Trench. His species (who are each individually also known as Ned) as a whole largely resides on Voldsoy. Their antlers can grow and be removed. Once removed they give Clancy the ability to do psychokinesis. Ned stands for ‘Neural Expansion Device’


##### The Banditos

The group of rebels who aim to escape/overthrow Dema. They wear yellow.


##### Vialism

The religion celebrated in Dema. The end goal of the religion is for one to take their own life.


##### The Glorious Gone

Those who have committed to vialism and ended their lives. They each get a neon gravestone around the city. Their bodies are referred to as ‘available vessels’ for psychokinesis.

##### Psychokinesis

An art/ritual the bishops use to possess dead bodies (usually of the glorious gone) others (but likely just Clancy) can use it via antlers from Neds. We don’t know for sure, but Clancy theorises that perhaps the bishops can use it on living beings, and that it had been used on him at points.

##### Smearing

This is what happens when an individual is captured by a bishop. It causes black marks on the persons neck and hands. Could potentially just be another form of psychokinesis.

##### The Annual Assembly of the Glorified

This is an annual meeting of all of the VIPs of Dema. One year it was held on a submarine outside of the city.

##### The Feature Performance Event

This was a live event performed by Clancy while he was a prisoner and making Dema propaganda. The event was hosted by two bishops possessing dead bodies using Pyschokinesis. The bodies slowly deteriorated over the course of the event. Clancy played one song (Choker) and spent the whole time daydreaming himself performing other music with the Torchbearer. This is also an example of an FPE (initials often re-used and referred to in twenty one pilots lore).

##### Failed Perimeter Escape

This is what people who try, and fail, to escape Dema are categorised as. Another example of FPE. Other uses of FPE in the lore include 'The Few, The Proud, The Emotional'(in Fairly Local) and 'Fuel, Petrol, Etc.' (from the Livestream).

##### Dmaorg.info

A website Clancy used to spread letters and media about Dema. The bishops found the site and took it down for a while, but Clancy has since got it back up following his escape.

'East is Up':

A code phrase used amongst Banditos to help identify each other. This phrase is used because the way out of Dema is in the east side of the wall. This is represented by the 'pitchfork' symbol seen on the Trench album cover. It was later repurposed by Clancy into the Scaled And Icy logo, also on that albums cover.

##### Sahlo Folina

What the Banditos cry out when they are in need of help.

##### Vultures

There is a large population of Vultures in Dema. For the Banditos, Vultures are a symbol of their commitment to turn death into life and to continue living. The Vulture on the cover of Trench is named Clifford!

##### Map of Trench

The first symbol on the Trench album cover (looks like a flower kind of) is a map of all of the canyons in Trench. There is also a full map of the continent of Trench on the Dmaorg.info website.

##### ||-//

A symbol of the Banditos. It is rebellious as it is an upside down neon gravestone. The gravestones resemble /-|. The extra lines two around the symbol are symbolic of a 'jumpsuit' something covering us and giving us support.

##### Good Day Dema

A live form TV show hosted by two bishops to spread Dema propaganda. It hosted the feature performance event.

##### Timeline

Clancy spends 9 years in Dema. It is not explained how old he is or where he came from, and even he does not seem to be sure. Clancys bishop is Keons.

Clancy hears rumours of ‘the old world’ and ‘the outside’ and is inspired to discover what is beyond the walls of Dema.

During the annual assembly of the glorified Clancy and a group of like minded rebels attempt to escape at night with the hopes of meeting Banditos outside of the walls. Clancy is the only one to escape and he does not meet any Banditos.

Clancy is eventually caught. Upon returning to Dema he tries to escape again, this time by somehow tricking Nico into driving him out of the City. He causes a distraction while they are on the road and escapes into Trench. (This is the Heavydirtysoul MV)

Clancy explores Trench for an unexplained amount of time and eventually runs into the Banditos as he walks down a river. He is suddenly approached by Nico, who ‘smears’ him. Clancy is inspired by the Banditos (Including Josh’s Character ‘The Torchbearer’) showering him with petals to resist the smear and escapes its grasp. Clancy is recaptured and dragged down the river for a while but eventually escapes again on his own. Clancy dissociates throughout this entire event and views it ‘out of body’ and does not recognise until later that it actually happened to him. (These are the events of the Jumpsuit MV)

Clancy journeys Trench for another 5 days, not running into any more Banditos. Keons finds him one morning and takes him back to Dema.

Clancy spend a few weeks in Dema before coordinating with the Banditos to escape again while the Bishops are busy with a vialism ceremony. He successfully escapes and the people of the Dema are inspired by the sight of the Banditos. (The events of the NATN MV)

Clancy travels to a Bandito camp and makes rebellious music against Dema with the Banditos (This is the album ‘Trench’) The camp is attacked and Clancy is recaptured again. (The events of the Levitate MV)

Clancy spends a few years in prison. During this time he becomes a bit of a legend in Dema for his letters and music.

The Bishops harness his fame and force Clancy to write propaganda for them. This becomes ‘Scaled And Icy’. Clancy sneaks a hidden message, ‘Clancy Is Dead’ into the album title.

Clancy performs in the Feature Performance Event on Good Day Dema but spends most of it daydreaming. The show is hosted by two bishops who use Psychokinesis to control the vessels of two corpses of the glorious gone which slowly rot and fall apart as the show goes on.

Clancy performs at the annual assembly of the glorified on a submarine outside of the city for the VIPs of Dema. He is guarded by fake bishops to intimidate him. Unbeknownst to the other Bishops they have been betrayed and The Torchbearer has been snuck into the submarine too. An escape plan is underway.

Keons betrays the Bishops and uses Psychokinesis to posses the sea dragon Trash to destroy the submarine and free Clancy and The Torchbearer. (The events of the Saturday MV)

The other bishops kill Keons. Clancy and The Torchbearer wash up on Voldsoy and discover creatures (Ned’s) that give Clancy the ability to use Psychokinesis. He uses this to control the vessel of Keons corpse and starts a fire in Dema. (The events of The Outside MV)

Clancy and The Torchbearer wait on the island for rescue, intending to finally use this new power to truly fight back against Dema. We know at this stage anywhere from 5 to 10 years have passed since the NATN MV as we see the same kids who appear in that MV all grown up as Banditos. Clancy reveals that the Torchbearer had this all planned out the whole time. We know from Ned’s cozy cabin that he had been communicating with the Neds. Clancy making propaganda may have just been a long term plan coordinated by Keons and The Torchbearer to get him to island so he can learn psychokinesis.

Clancy is now a weapon to be used in the upcoming final war. He gets ready to cross Paladin Strait to return to Dema, which he has know filled with embers of inspiration.

Clancy and the Torchbearer return to Dema across the Paladin Strait on boat. While on the boat Clancy uses psychokineses to possess a glorious gone and teach anti-dema lessons inside the city. A rebellion starts to brew within the city. These rebels wear Red Tape. Clancy leaves the body and returns to his own as him and the Torchbearer arrive on the shores of Trench. (This is the Overcompensate MV).

Recommended Watching/Listening Order

##### Albums

- Vessel (apparently - ask Tyler lol)
- Blurryface
- Trench
- Scaled And Icy
- Clancy

Music Videos:

Heavydirtysoul

Jumpsuit

Nico and the Niners

Levitate

The Livestream (not a music video, but canon)

Saturday

The Outside

Overcompensate

Extra 'non-canon' Music Videos:

These music videos aren't part of the canon but still give us ideas about the lore.

Heathens (could be seen as Clancy in a Dema prison)

My Blood (features a vulture, and the idea of dissociation and seeing someone else living events that are actually happening to you)

Chlorine (introduces Ned)

The Hype (a 'behind the scenes' MV acting as a metaphor for the bands career leading to the creation of Trench. Features Ned)

Shy Away (could be seen as a propaganda video made by Clancy. Foreshadows Trash rescuing Clancy)

Choker (the story of this music video is essentially a microcosm/representation of the Dema universe as a whole)

Next Semester (contains the Clancy mask!)

And that’s where Clancys story ends for now. Everything is set up for the final push, and I cannot wait to see where the story leads. I will update this post with more details (and sources!!!!) once the next era is over and we have all of the story.



