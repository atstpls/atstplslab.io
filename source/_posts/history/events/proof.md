---
title: proof
tags: [analysis]
category: [reference]

---

<br>

- [Growing Up](#growing-up)
    - [Family](#family)
    - [Sports](#sports)
    - [Music](#music)
- [Young Man](#young-man)
    - [Jackson](#jackson)
    - [Fitness](#fitness)
    - [Navy](#navy)
- [Meeting You](#meeting-you)
- [Marriage](#marriage)
    - [The Best of You](#the-best-of-you)
    - [The Man Job](#the-man-job)   problem-solving 
    - [Self Improvement](#self-improvement)   when i improved, you noticed 
    - [Love Languages](#love-languages)    i need to notice more 
    - [Different for a Reason](#different-for-a-reason)   we make each other better 
- [Parenthood](#parenthood)
    - [The Best of Us](#the-best-of-us)  everything I love about you 
    - [Protect at all Costs](#protect-at-all-costs)    safety, guns, knowledge, finances, resilience, hope 
    - [Lead By Example](#lead-by-example)   be the man, request don't command, speak all five, keep hope alive 
    - [Own It and End It](#own-it-and-end-it)  yes maam/sir and one question  Jocko 
    - [Forty Six and Two](#forty-six-and-two)   evolve 
    - [World of Crypto](#world-of-crypto)     break out of the rat race 
    - [Investigation of Truth](#investigation-of-truth)  jordan peterson, ja morant, money, philosophy, politics, gathering validating evidence, root cause analysis 
    - [Testing Ideas](#testing-ideas)   podcasts, twitter, mental models w/ sean, coaching Dub teams, subscriptions 
    - [Ductile Bear Development](#ductile-bear-development)  hundres of notes, thoughts, results, artifacts, reasons 
    - [Bitcoin and Proof of Work](#bitcoin-and-proof-of-work)  books, courses, video series, research 
    - [PACER Heuristic](#pacer-heuristic)  organize information, validate authenticity 
- [Playbooks](#playbooks)
    - [Weaksauce Playbook](#weaksauce-playbook)   to expose what bullies do 
    - [B Plan Playbook](#b-plan-playbook)  to guide dealing with bullies
    - [Jiu Jitsu](#jiu-jitsu)  the ability to deal with bullies, videos, models, analysis, flograppling 
    - [Bible Knowledge](#bible-knowledge)  mormons, sean, bible series, models, concepts 
    - [Jordan Knowledge](#jordan-knowledge)   
    - [Pinocchio Progression](#pinocchio-progression)
    - [Victim Playbook](#victim-playbook)  to expose what crybullies do, victim logic and learned helplessness 
    - [Buck Progression](#buck-progression)    to guide dealing with Pinocchios 


<br>


## Growing Up

### Family

### Sports

### Music

## Young Man

### Jackson

### Fitness


### Navy


## Meeting You

## Marriage


### The Best of You


### The Man Job 

### Self Improvement


### Love Languages


### Different for a Reason


## Parenthood


### The Best of Us


### Protect at all Costs

- I try my best to protect our family
- learned about guns, bought them, carry them, practice with them, teach Dub about them
- learned about Jiu Jitsu, signed us up for lessons, signed up for FloGrappling to get into Jiu Jitsu, followed the top Jiu Jitsu athletes
- Watched thousands of videos, made plans and strategies, organized the moves, gave them unique names, practice with him whenever he wants
- Bought and downloaded self defense videos from Sheepdog Response, watch them with y'all, bought airsoft guns to practice, make shooting games
- Go through countless scenarios so we can be more prepared for dangerous/emergency situations
- Carry tourniquets for us, bought and carry choking rescue device, first aid kids, prescribed medicine kits, safe vehicles
- Stress safety constantly, made up emergency words like pinch-point
- Constantly point out dangerous and unsafe situations to our family and encourage critical thinking about how to handle them, make the house secure with doors/locks/cameras/alarms

<br>

### Lead By Example

- I lead by example, never telling y'all to do things but showing y'all how they can be done, even made myself the phrase:  Be the man, request don't command, speak all five, keep hope alive, I keep myself in good shape, I work hard at everything I do, I constantly bring things to y'all that I think y'all might be interested in or that would support something y'all want to do, I practice literally anything y'all want to practice, I practice things y'all don't want to practice, I eat things y'all don't want to eat, do things y'all don't want to do, to be an example that we can do anything we set our minds to. I stress to do the hard things, I overcome setbacks, I work through my injuries, I work out when I don't want to, I give myself challenges like Hero WOD a day for a year, and then I do them. I show different strategies of how to get things done, I constantly come up with ideas for problem-solving, and I've made myself available to help with every single problem y'all have faced in the last decade. Every single one I have been there ready to help. 

<br>

### Own It and End It

- I came up with Own It and End It, yes maam/sir and one question, and several other mottos that helped Dub, you, and me be better... did that after listening to thousands of hours of Jocko Podcast for the sole purpose of becoming a better man. Not because it was fun, not because it made me money, but to be better, for y'all. 

<br>

### Forty Six and Two

I have made it my focus for the last 6-7 years to evolve, to constantly be better, I made the sign 46&2 to remind me of my duty to evolve and be the best I could be for you and for Dub. I've helped Dub get better at mental jiu jitsu, point out its use whenever I see it, 

<br>

### World of Crypto

- I studied and became knowledgeable in the fundamental theories and practice of money, crypto, economics, media analysis, international politics, Biblical stories, philosophy, social issues, the woke mind virus, critical theory, gender ideology, censorship, disinformation, government overreach, corporate and government corruption and I have thousands of notes in my computer as a result of thinking about how to navigate these in our life.  Not for me, I did it for us.  I researched thousands of hours on the history of economics like the 2008 recession, and financial tools, strategies to keep us financially resilient. I listened to hundreds and hundreds of podcasts, ordered and read books, completed online courses, thousands and thousands of hours of online research, thousands of hours of understanding these things, trying to figure out how we can learn how they affect us, creating mental models to help us to be better and to navigate this world better. 

<br>

### Investigation of Truth

- I took on dozens of online investigations of real world events like ja morant, brett favre, COVID shots, twitter files, claims about Trump, War in Gaza, etc, etc learned about gathering and validating evidence, doing root cause analysis online, so that we could know the truth about things and understand how to find the truth so that we could be safer and healthier and have better lives. I worked thousands of hours researching different ways to validate authenticity of information and developed the PACER heuristic, a mental model that works so well I presented it at my work to the teams I work with.

<br>

### Testing Ideas



### Ductile Bear Development

- I created my own company for the sole purpose of developing people called Ductile Bear Development, because that was what I was spending almost every free hour I had doing--researching ways to develop people--kids, adults, teams, organizations, I made thousands of notes, thoughts, results, artifacts, reasons and continually applied them to various development efforts such as coaching Dub's teams, helping Dub get various skills and abilities, helping neighborhood kids develop, made many games over the last 6-7 years intended to help kids learn and understand concepts, find ways to overcome obstacles, organize what I've learned from books, courses, video series, research online


<br>

### Bitcoin and Proof of Work


### PACER Heuristic


## Playbooks 

- I studied, researched, learned about human behavior, immersed myself in books/podcasts/videos/online discussions from Jocko, Jordan Peterson, Jonathan Pageau, Douglas Murray, David Goggins, all these experts out there that are trying to help people like us
- I took huge chunks of their knowledge and worked hard to understand it, condense it, and translate it into something that could help Dub, help you, help us
- I developed multiple mental models to explain human behavior and guide us in understanding what people are doing and why
- I made the Weaksauce Playbook to expose what crybullies do, the B-Plan playbook on how to deal with bullies and crybullies
- I made the Pinocchio Progression to show the traps and temptations of not prioritizing truth and responsibility
- I made the Victim Playbook to show how the Woke Mind Virus works and how victim logic and learned helplessness is used to deceive good honest people
- I made the BUFFALO INCEPTION model to show the importance of things and how they all fit together
- I made the BUCK playbook to show how to successfully prioritize truth and responsibility, and many many others 

<br>

### Weaksauce Playbook


### B Plan Playbook


### Jiu Jitsu


### Bible Knowledge

- I studied, researched, read books, took courses on love languages, personality traits, marriage, parenthood, reciprocity, ethics, religion, logical fallacies, etc so I could communicate better with you and Dub, so we could communicate better with others, so we could make better decisions in life and have better outcomes.  I talked to random strangers on the Internet about God, initiated our discussions with the Mormons, spent thousands of hours talking to Sean about God and life and all the other problems in his life, discuss God with you and Dub for however long you want anytime every time, created my own mental models about stories in the Bible and regularly try to apply them to scenarios in real life, in our lives, and make myself available to discuss them for however long anyone would need to understand them.  

<br>

### Jordan Knowledge


### Pinocchio Progression

ACTOR

- Quote Mining 
- Frame Mining 
- Two-Tier Justice 

LIAR 

- 


VICTIM 

- Someone wronged 
- Someone blamed 
- Someone shamed 

JACKASS 

- Don't understand 
- Can't explain 
- My truth 







### Victim Playbook


### Buck Progression



<br>



















