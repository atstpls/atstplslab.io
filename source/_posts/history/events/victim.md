---
title: victim
tags: [analysis]
category: [reference]

---

<div data-person="b">
    <p></p>
</div>
<div data-person="a">
    <p></p>
</div>
<div data-person="b">
    <p></p>
</div>
<div data-person="a">
    <p></p>
</div>
<div data-person="b">
    <p></p>
</div>


<br>
<br>
<br>
<br>



<div data-person="a">
    <p>Please don't let our son in your office while you have me shamed on your board. That is one million percent unacceptable</p>
</div>
<div data-person="b">
    <p>Tell me it's not true</p>
    <p>Trying to shame me by saying I'm shaming you lol.  You're the one who said it!</p>
    <p>If him seeing it on the board is so bad, what if he hears you saying it</p>
    <p>Bandaids</p>
    <p>The problem is the thing</p>
</div>
<div data-person="a">
    <p>I'm sad you find this "lol" worthy. I'm not your dog nor your child. You will not break me by shaming me. But you do lose some of my respect. Not that you seem to care. Goodnight.</p>
</div>
<div data-person="b">
    <p>I'm leaving it. Our son will hear you say it eventually</p>
    <p>You sure love drama</p>
    <p>16 years married and you tell me day before Valentines Day that I enjoy making you feel stupid</p>
    <p>Imagine the reverse</p>
    <p>That's why the LOL... This is ridiculous</p>
    <p>If I enjoy it so much, when's the last time it happened?</p>
    <p>What are the things I love making fun of you about</p>
    <p>I really don't know</p>
    <p>Please no drama tomorrow</p>
    <p>I didn't lose any respect for your. I'm still trying to find out what you are thinking</p>
    <p>Must be nice</p>
    <p>Sorry I made it worse</p>
</div>
<div data-person="a">
    <p>I'm sorry I'm so difficult. When I get my feelings hurt, I don't think straight and I lash out. I know it's wrong. You may not believe this, but I actually do try to curb it. I'm sorry, I shouldn't have said that you enjoy making me feel stupid.Of course, I know you don't actually enjoy it. I shouldn't have said it.</p>
</div>



<br>
<br>
<br>
<br>


<div data-person="b">
    <p>Our lives together, everything I've said and done, coaching, parenting, pushing to solve life's toughest problems. None of it matters, your interpretation of what happened last night makes me a monster</p>
    <p>Interpretation</p>
    <p>Which you refuse to balance with your spouse's interpretation</p>
    <p>Your teammate</p>
    <p>And I am failing to help you understand that</p>
    <p>I'm failing you</p>
    <p>And I'm sorry</p>
    <p>But I will never give up</p>
</div>
<div data-person="a">
    <p>Who paints who the monster? Your idea of having a teammate is to continually say that I'm acting like a victim. As if I am not allowed to have an interpretation that varies from yours. Because yours is the correct one. You have decided that you are more intelligenet and have more emotional growth than anyone else; therefore we all are just wrong. I don't know how you expect me to respond to this. I'm not the type of woman to just roll over. I have a voice, too.</p>
    <p>You are the best man I've ever known. Does that mean you are perfect? No. Does that mean you aren't misguided sometime? No. The reason God gave us to each other is to balance each other.
</div>
<div data-person="b">
    <p>Then let's balance and not shut down each other's versions of what happened. Deal?</p>
    <p>I promise to always get your perspective first and try very hard to see the best in what you did before decideing for myself when you do things I don't understand or agree with</p>
    <p>Starting right now. I haven't done that well but I'm going to from here on out</p>
    <p>Even if you don't I will. This is a promise from me to you and I believe it will have a huge positive impact on our life together.
</div>
<div data-person="a">
    <p>I will try very hard to do this. I may slip, but I promise to try.</p>
</div>


<br>
<br>
<br>
<br>
<br>
<br>



<div data-person="a">
    <p>I told you yesterday that I was going to work on communication with you. I'm very upset at you right now. You have now in the last 3 days undercut me and insinuated that I am not on your level of thinking-behaving. If he is ever going to respect m, you will need to curb that. Please.</p>
    <p>Also, if you truly feel as if you have surpassed me, we will need to address that within our relationship, too.
</div>
<div data-person="b">
    <p>He respects you. And he listens to the reasons you give for things. Your reasoning was flawed in this case, that's all that's oing on.  The problem is that you have chosen to play victim instead of address it. It's not just you who does this, it's everyone. It's a world problem. I make it a point to correct my flawed thinking and pass on what I have learned to you and Dub. I'm not after respect, I just want to give Dub the truth and the best advice possible.</p>
</div>
<div data-person="a">
    <p>Noted. Thank you for your explanation.</p>
</div>
<div data-person="b">
    <p>Buffalo Inception pic</p>
    <p>I think about what we need to live the best life every single day</p>
</div>
<div data-person="a">
    <p>Well, thinking about it and using it to put people down, are two different things</p>
</div>
<div data-person="b">
    <p>God has given me everything and I'm going to give everything</p>
    <p>Victim mentality</p>
    <p>Come be a problem solver with me baby</p>
</div>
<div data-person="a">
    <p>Yes, we know, as you love to point out, that I am flawed and use victim mentality. What, pray tell, do you ever do wrong?</p>
</div>
<div data-person="b">
    <p>I failed to be water for many years... still fail, but I'm improving. I've failed to have thick skin, failed to admit my mistakes, failed to correct what I knew needed correcting. I could go on forever on that. But I won't. Because that time could be used problem solving and helping people.</p>
    <p>We have one life, we gotta use it the best way we can I believe</p>
</div>
<div data-person="a">
    <p>And you believe that you're using it best putting me in place? I'm just trying to make sure I am following what you're saying, because you know I'm subpar in the thought department</p>
</div>
<div data-person="b">
    <p>Notice my language is about helping others, progressing, making the best out of things. Yours is the exact opposite. It's focused on you, your flaws, and placing blame on others (me in this case) I don't take pleasure in hurting you, I love you and you're my best friend. I've always been that way and I always will be.</p>
</div>



<br>
<br>
<br>
<br>


<div data-person="b">
    <p>We're not victims</p>
</div>
<div data-person="a">
    <p>I didn't say we were</p>
</div>
<div data-person="b">
    <p>"They don't like us / come around / it's because of me / my abrasive manner"</p>
</div>
<div data-person="a">
    <p>You took my statement out of context. I said that I hoped I didn't keep your younger family from coming around you and Du. You asked why I thought that. I answered with an honest answer. Please don't come at me with the we are not victims line.</p>
</div>
<div data-person="b">
    <p>"Don't come at me" < victim<br>"You took out of context" < victim<br><br>Responsibility is the opposite of victimhood.</p>
    <p>Try these<br><br>"I'm going to be better"<br>"I may have got my signals crossed with them"<br>"I'll think of a solution"</p>
</div>
<div data-person="a">
    <p>Sometimes you need to remember that I'm your wife. Not your pupil.</p>
</div>
<div data-person="b">
    <p>How could I communicate that better? How can I improve?</p>
</div>
<div data-person="a">
    <p>When your wife, whom you love, approaches you emotionally vulnerable, you could atempt to curb your inclination to immediately chastise her for showing emotions. You tend to see things in black and white. You have been fixated on pointing out people's weakness and you especially hate "victims". But you are going to have to let the reins go some with me. I'm not Sean. I'm not Rhema. I'm not someone you can just come at full force with your righteousness. I'm your partner. You don't cut the jugular of your partner when they show weakness.</p>
    <p>Next time, because I'm sure there will be a next time, (I am flawed) please at least wait until I'm not showing you my weakness before you jump to correct my behavior</p>
</div>
<div data-person="b">
    <p>I didn't chastise you for showing emotions. I pointed out the victim logic you were using. To help you.<br><br>I know you are not those people. I amd the most honest with you. To help you.<br><br>Victimhood is not a weakness, it's a choice. You don't have to choose it. When you do I will point it out. To help you.<br><br>Correcting your behavior would be telling you how to behave. I'm not. I offered suggestions that you are free to take or ignore.<br><br>Full force? Think about that choice of words... is there anything... ANYTHING I have done in years that is full force?<br><br>Serious question</p>
    <p>Oh yeah, I don't hate anyone. I've told you again and again, people run victim plays.  They are all equally valuable in the eyes of God.</p>
</div>
<div data-person="a">
    <p>Please don't "help" me so hard. And you know what I mean by that, so please don't pick my words apart.<br><br>I'm working on my Bible study. Be home later.</p>
</div>

<br>
<br>
<br>
<br>
<br>

<div data-person="b">
    <p>The following is not how I "feel".  These are universal truths in this world.  They've been proven over and over and over and over, for years and years and years.</p>
    <p>Victimhood<br>
        &emsp;- is the opposite of gratefulness, duty, and responsibility<br>
        &emsp;- works to seek redress, not to move forward<br>
        &emsp;- deprives people of their agency, producing helplessness<br>
        &emsp;- prevents taking personal responsibility--problems are always "out there", "in them", but never "in me"<br>
        &emsp;- feeds on emotions and outrage, achieves nothing<br>
    </p>
<p>Truth #1<br>
&emsp;- It's not fun to be around people who play the victim.  Remember back when I used to say that something you did hurt my feelings?  And things couldn't move forward until you made it right?  I was running victim plays.  It wasn't very fun to be around me was it?   
</p>
<p>
Truth #2 <br>
&emsp;- It makes you weaker when you play the victim.  Remember when you talked about getting the FAFO tattoo?  And how it was about being strong, overcoming evil and doing hard things?  That's the opposite of being a victim.  That's saying that you refuse to become a victim.  You cannot grow stronger and run victim plays at the same time.  You can either blame others or make yourself better, you cannot do both.  
</p>
<p>
Truth #3 <br>
&emsp;- Everything I tell you is to make you better.  Every single thing I tell you, I've already told myself hundreds of times.  I'm leading from the front, I'm doing everything that I can to be better and to make y'all better.  There is literally nothing else I think about.  If you don't believe my words, then watch my actions.  But even if you still don't see it, even after we spend 50 years together, I'm still not going to blame you.... I'm just going to get better.  
</p>
</div>
<div data-person="a">

<p>I appreciate that you are always trying to make me see the error of my ways and correct my behavior. You are my partner. Thats what you are supposed to do- help me. And you do that very well. 
</p>
<p>
Do you find it ironic that the way you teach me the right way is that you distance yourself from me and then send me an email later explaining all the ways I am in need of changing myself? I am flawed. I am always trying to get better. I am also different from you. I am not you. You are not me. You are very quick to point out my failures. 
</p>
<p>
I pray that one day I may eventually be worthy. 
</p>
</div>
<div data-person="b">
    <p>&emsp;1. I'm not trying to correct you, I'm telling you what works and what doesn't work (UNIVERSAL TRUTHS)<br>
    &emsp;2. I'm not distancing myself, what you are experiencing is what everyone experiences when they play the victim--distance from reality.  You're not a victim, and acting like one brings you away from reality, away from everyone else<br>
    &emsp;3. I'm not explaining how you should change yourself, I'm explaining how victimhood works.  Please point out where I said you should change (QUOTE ME, BACK UP YOUR CLAIMS)<br>
    &emsp;4. I'm doing it via email because it's more effective communication that speaking with you (DO YOU DISAGREE?)<br>
    &emsp;5. I'm not pointing out failures, I'm teaching you about victimhood (QUOTE ME, BACK UP YOUR CLAIMS)
    </p>
</div>
<div data-person="a">
    <p>You keep walking away. You keep telling me that I'm not in touch with reality. Why? Because I disagree with you about an event that we both witnessed yesterday. You keep trying to force me to agree with you that I'm not in touch with reality. I do NOT agree with you. You do not control me. You do not control my thoughts. You do not control my actions. You should not WANT to control me. Maybe YOU should ask yourself why you do. </p>
</div>
<div data-person="b">
<p>I walked away because you wouldn't stop talking over me </p>

<p>It's great that we disagreed about the event—I said so yesterday. I thought I saw a revolver, you and Dub saw a Glock type gun. I laughed, I thought it was cool and interesting. I told John that, I told yall that. Because it's true.</p>

<p>The claim that I was trying to downplay the event and make you look stupid is not true. And I told you it wasn't true. And when you believe something that isn't true, in that moment you are not aligned with reality.</p>

<p>I gave examples of times when I'm not aligned with reality. I'll give you a list of many more, I do it all the time. </p>

<p>"Ask myself why I want to control you" that's another claim that's not true and you can't back it up. </p>

<p>I love you. I can back that up. I like troubleshooting hard problems. I can back that up. I own things I've done and things I haven't done. I can back that up. </p>

<p>Your turn to back your claims up. </p> 

</div>
<div data-person="a">
    <p>Here is the problem with your "logic". If I'm not aligned with reality, how can I back up any claims? This is how you get to walk away feeling superior and right. You need that for some reason. </p>
</div>
<div data-person="b">
    <p>Try backing up this claim:</p>

<p>Drew tried to make Tracey look stupid (out of the blue) using a dangerous incident that happened to them earlier that morning.</p>

<p>Good luck</p> 

</div>

<div data-person="a">

<p>This is an easy one. More than once in the truck you said, "wow. You're really on high alert now." I said, "yes, of course I am." You said, "that was then. This is now. I'm getting us back into now. You're focused on the past." </p>

<p>Maybe next time you could try to show our safety instead of trying to make me appear ridiculous for being shook up.</p>

<p>At T4L you kept trying to downplay the incident and find all of these other reasons for what could have transpired - while simultaneously trying to point out that I was not admitting that other things could have been taking place- other "realities", as you like to put it. And you did this in front of your mother, step-dad, and our son. 
</p>
</div>

<div data-person="b">
<p>You used another claim to back up your claim. </p>

<p>I'm telling you (again) I don't try to make you look stupid. </p>

<p>Believe me or don't. My actions are true and I stand by them. </p>

<p>You have been wrong before. All I can do is tell you you're wrong about this. </p>

</div>
<div data-person="a">

<p>It's funny how I'm always the one who is wrong- I'm always the one who doesn't understand. I'm using claims to back claims.. </p>

<p>Again I ask, what in the world can you possibly gain by being with me? Out of touch with reality, make claims with claims.. what is the value? How do you cope with always having to tell me exactly what to think and then explain why I shouldn't feel anything?</p>

<p>You are focused on being right. You enjoy arguing. You show other people how they are doing it wrong but you're doing it right. This isn't healthy either. I'm an emotional person, yes. But there isn't anything wrong with caring. Please stop trying me to be an unfeeling robot. </p>

</div> 

<div data-person="b">

<p>You can't back up your claims. That doesn't bother you?</p>

<p>If you can't back it up, then there's a possibility it could be untrue. </p>

<p>Doesn't that matter to you? </p>

</div>
<div data-person="a">
<p>You're the one saying I can't back up claims. You're the one saying I'm not in touch with reality. It seems like you are one who is bothered… even though you consistently claim that you can't get upset or have your feelings hurt.
</p>
</div> 
<div data-person="b">

<p>Ok but what about you?</p>

<p>Can you answer the question honestly?</p>

</div>
<div data-person="a">

<p>Of course it matters to me. Unsupported claims aren't healthy either. Telling me that I'm out of touch with reality is a lie. You want to try to force me to concede that I am out of touch with reality- that's not going to happen. You asked me about my FAFO tat, yes, I'm not weak and I'm not a victim. I absolutely will not let you gaslight me into believing that I'm crazy. I disagree with your perception of the events yesterday. That does NOT make me crazy or out of touch. It simply means that I think differently than you do. 
</p>

</div> 
<div data-person="b">
<p>Good, then we can have an honest discussion about the truth. </p>

<p>The truth is I like investigating events and how they are perceived differently. I do that as a job and also have been researching that in media and politics for years and can back that up with all the notes I have on my computer, I can send you the link. Years and years of comparing people's perceptions of events and trying to discover what might it be immediately obvious to people who are trying to determine what happened somewhere with only limited information. 
</p>
<p>
Yesterday that was us, we couldn't explain much and so I was attempting to brainstorm possibilities. This is what I do every single day. It is the truth. I've told you it was the truth. I have a history of telling you the truth about everything. EVERYTHING. 
</p>
<p>
So are you willing to admit that it's possible that you could be wrong about that?
</p>
</div>
<div data-person="a">
<p>
Aaaaannnnnddddd we are back to me admitting that im wrong. You have the experience. You have the credentials. I'm out of touch with reality. I'm wrong. Honestly, I may as well just tell you that I'm wrong so that this will end. You are right. I'm wrong. I'm ignorant of the ways of investigation. I'm ignorant of handling my own opinions. Please, by all means, let's discuss what I should be thinking- I know you're ready to tell me what it should be. 
</p>
<p>
Not gonna lie, this offense you are running truly wears down the mental acuity. You may be in the wrong profession. Maybe you should be a politician. Or in the media. 
</p>
</div>

<div data-person="b">

<p>The question is can you admit there's a possibility that you COULD be wrong.</p> 

<p>I can do it. I might be wrong and you might be right. </p>

<p>Can you reciprocate?</p>

</div>
<div data-person="a">

<p>Of course I can admit that I could be wrong. Have you not heard me say MANY times today, that I am wrong?? You're so focused on getting in a jab that you aren't listening/seeing me. You want to show me over and over that you are the only one around here who is trying to fix things. But that's not truth, and we all know how much you love truth. Can YOU admit that that is untrue? Can you bring yourself to admit that I'm not some raving crazy person?? You accused me earlier of making everything about me. Well, that's because I'm my only advocate. I HAVE to defend myself around you. You would probably try to have me committed. Lord knows you have told me enough that I'm out of touch with reality. It's shocking that you entrust me to drive around your vehicles, live in your house, and take care of our son. 
</p>
</div>
<div data-person="b">

<p>Ok, so we agree it's possible I was not trying to make you feel stupid while discussing the event. </p>

<p>What would I need to do to prove to you that I was not trying to make you feel stupid and I was only discussing it honestly trying to think of possible scenarios we may have not thought of?</p>

</div>
<div data-person="a">

<p>If you want to show that you are not trying to make it seem as if I'm stupid, then you should be more careful with your words. </p>

<p>Stop telling me what I should think. Stop telling me how I should think.</p> 

<p>And definitely don't tell me that because I disagree with you, that I'm not in touch with reality. That is beyond fucked up. </p>

</div>
<div data-person="b">
    <p>The truth matters to both of us. I'm asking how can I prove I didn't do what you think I did. Can you please tell me how?</p>
</div>
<div data-person="a">
    <p>I feel like I answered that question.</p>

<p>I stand by what I have always said about you. You are a great man. You make me feel loved. You make this life wonderful. You are the absolute BEST father.</p>

<p>You are not perfect. I'm most definitely not perfect. We are going to have misunderstandings. We are going to disagree. We don't have to let them turn into this sort of thing. It doesn't seem fair to come at me with being out of touch with reality simply because I don't agree with you. </p>
</div>
<div data-person="b">
    <p>What do I need to do?</p>

<p>Please help me out here?  You think I did something I didn't. The truth is important to both of us. </p>

<p>What would it take for me to convince you I didn't do what you think I did ?</p>

<p>If you've told me please tell me again, just to be clear. </p>
</div>
<div data-person="a">

<p>Why are you trying so hard to make me change my perception of this disagreement? Why can't we just agree that we disagree and move on? Why does it have to come to this?</p> 

<p>I have acknowledged that you are a great man. If you need more, I acknowledge that you do not INTENTIONALLY hurt me or my feelings. I don't believe that you act nefariously against me. </p>

<p>You said what you said. You said I'm not in touch with reality.</p>

<p>I will not play this game of entrapment with words. I'm not your latest X troll. I'm your wife. Please stop trying to beat this dead horse. I'm ready and willing to move on from this disagreement. Are you? </p>
</div>
<div data-person="b">
    <p>You're ok with me admitting to something I know in my heart I didn't do?</p>

<p>Or are you interested in the truth?</p>

<p>We're not forgetting about it because we both agreed it's important to both of us. </p>
</div>
<div data-person="a">
    <p>You don't have to admit to anything. The MORE IMPORTANT thing is that we learn from the exchange and not repeat it. As I previously stated, if you don't tell me what and how to think and then cap it off by telling me that I'm not in touch with reality, then I won't be able to make the assumption that you're trying to make me feel stupid. And if I will stop trying to figure what in the world you're thinking, then you won't get your defenses up. I know you don't like to admit that you have feelings, but we both know you do in fact have both feelings and opinions. </p>

</div>
<div data-person="b">

<p>Of course I have feelings </p>

<p>Im talking about how this started. I was discussing the event and you've stated you felt I was trying to make you feel stupid with my ideas. And the truth is I wasn't. </p>

<p>You said it's possible I wasn't. What would you need to hear to believe me?</p>

<p>This is important to our marriage. I need to know what makes you believe me and what doesn't. </p>

<p>Please tell me what would it take to believe my story about what I was thinking and doing versus what you thought I was thinking/doing?</p>
</div>
<div data-person="a">
<p>Also, you are clearly still not happy with me, because when I mentioned that you needed to start the medicine, you promptly told me that you had it. </p>

<p>Please. Can we end this stand off? What do I need to do? </p></div>
<div data-person="b">
    <p>Answer my question. That's it. </p>
</div>
<div data-person="a">
    <p>Please talk to me- in person</p>
</div>
<div data-person="b">
    <p>That's why talking in person doesn't work as well as email </p>

<p>You don't believe me when I tell you I don't intentionally hurt you with words</p>

<p>That is the issue </p>
</div>
<div data-person="a">
    <p>Maybe telling me that I'm lying isn't productive. How can you stand to be married to a person who lies and isn't in touch with reality? This is stupid. You are dragging this out needlessly. It's cruel. </p>
</div>
<div data-person="b">
    <p>Just to recap, you "know" that I enjoy "coming at you" because I do it all the time to everyone and I told you I like doing it to everyone. </p>

<p>Ridiculous.</p>
</div>
<div data-person="a">
    <p>Enjoy this separation. It seems to be what you're after. </p>
</div>
<div data-person="b">

<p>Says the person who can't trust what their spouse would swear to them under God</p>

<p>I want to be close to you. But I won't let you live a lie </p>

</div>
<div data-person="a">
    <p>So you're saving me from my lies? By pushing me away, telling me that I'm telling lies, running victim plays, and not in touch with reality? It's crystal clear now, thanks. Meanwhile, you are doing something extremely dangerous and reckless- climbing a tall ladder with a bum shoulder and an empty belly. By all means, please tell me more about how I'm the problem. </p>
</div>
<div data-person="b">
    <p>It's really simple. If you say you didn't do something on purpose, I'll believe you. </p>

<p>You can't reciprocate. </p>

<p>You're not the problem, but we need to agree on what is best for our marriage. </p>

<p>I believe it's best to trust each other when we tell each other things. Do you agree?</p>

</div>
<div data-person="a">
    <p>Yes, I agree. </p>
</div>
<div data-person="b">
    <p>So will you do it then?</p>
</div>
<div data-person="a">
    <p>Yes</p>
</div>
<div data-person="b">
    <p>Next time, If you just believe me, that I was not trying to make you feel stupid, and I don't "enjoy coming at you" we can avoid this entire situation. </p>

<p>We've found a solution but it's not something I can do. I can help by earning your trust but you have to ultimately trust me. </p>

<p>Have I given you ANY reason to not trust me?  Over the last 20 years??? Anything?</p>

<p>Serious question. I need to know if there's anything I need to make right. </p>

</div>
<div data-person="a">
    <p>No</p>
</div>
<div data-person="b">
    <p>Is there anything I can do to be more trustworthy?</p>
</div>
<div data-person="a">
    <p>No. </p>

<p>I have answered your questions. Will you please answer mine?</p>
</div>                      
<div data-person="b">
    <p>Sure. What's your question?</p>
</div>
<div data-person="a">
    <p>Changed my mind. I don't have any questions for you. I'm going to bed. 
</p>
</div>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<div data-person="b">
    <p>If you doubt that I love you, care about you, want the best for you, and continually work every single day to make things better for us, read this:</p>
    <p>It is proof... it my ACTIONS and RESULTS over a long span of time.</p>
    <p>- I try my best to protect our family, learned about guns, bought them, carry them, practice with them, teach Dub about them, learned about Jiu Jitsu, signed us up for lessons, signed up for FloGrappling to get into Jiu Jitsu, followed the top Jiu Jitsu athletes, watched thousands of videos, made plans and strategies, organized the moves, gave them unique names, practice with him whenever he wants, bought and downloaded self defense videos from Sheepdog Response, watch them with y'all, bought airsoft guns to practice, make shooting games, go through countless scenarios so we can be more prepared for dangerous/emergency situations, carry tourniquets for us, bought and carry choking rescue device, first aid kids, prescribed medicine kits, safe vehicles, stress safety constantly, made up emergency words like pinch-point, constantly point out dangerous and unsafe situations to our family and encourage critical thinking about how to handle them, make the house secure with doors/locks/cameras/alarms</p>
    <p>- I lead by example, never telling y'all to do things but showing y'all how they can be done, even made myself the phrase:  Be the man, request don't command, speak all five, keep hope alive, I keep myself in good shape, I work hard at everything I do, I constantly bring things to y'all that I think y'all might be interested in or that would support something y'all want to do, I practice literally anything y'all want to practice, I practice things y'all don't want to practice, I eat things y'all don't want to eat, do things y'all don't want to do, to be an example that we can do anything we set our minds to. I stress to do the hard things, I overcome setbacks, I work through my injuries, I work out when I don't want to, I give myself challenges like Hero WOD a day for a year, and then I do them. I show different strategies of how to get things done, I constantly come up with ideas for problem-solving, and I've made myself available to help with every single problem y'all have faced in the last decade. Every single one I have been there ready to help.</p>
    <p>- I studied, researched, learned about human behavior, immersed myself in books/podcasts/videos/online discussions from Jocko, Jordan Peterson, Jonathan Pageau, Douglas Murray, David Goggins, all these experts out there that are trying to help people like us. I took huge chunks of their knowledge and worked hard to understand it, condense it, and translate it into something that could help Dub, help you, help us. I developed multiple mental models to explain human behavior and guide us in understanding what people are doing and why. I made the Weaksauce Playbook to expose what crybullies do, the B-Plan playbook on how to deal with bullies and crybullies, the Pinocchio Progression to show the traps and temptations of not prioritizing truth and responsibility, the Victim Playbook to show how the Woke Mind Virus works and how victim logic and learned helplessness is used to deceive good honest people, the BUFFALO INCEPTION model to show the importance of things and how they all fit together, the BUCK playbook to show how to successfully prioritize truth and responsibility, and many many others</p>
    <p>- I came up with Own It and End It, yes maam/sir and one question, and several other mottos that helped Dub, you, and me be better... did that after listening to thousands of hours of Jocko Podcast for the sole purpose of becoming a better man. Not because it was fun, not because it made me money, but to be better, for y'all. I have made it my focus for the last 6-7 years to evolve, to constantly be better, I made the sign 46&2 to remind me of my duty to evolve and be the best I could be for you and for Dub. I've helped Dub get better at mental jiu jitsu, point out its use whenever I see it,</p>
    <p>- I studied and became knowledgeable in the fundamental theories and practice of money, crypto, economics, media analysis, international politics, Biblical stories, philosophy, social issues, the woke mind virus, critical theory, gender ideology, censorship, disinformation, government overreach, corporate and government corruption and I have thousands of notes in my computer as a result of thinking about how to navigate these in our life.  Not for me, I did it for us.  I researched thousands of hours on the history of economics like the 2008 recession, and financial tools, strategies to keep us financially resilient. I listened to hundreds and hundreds of podcasts, ordered and read books, completed online courses, thousands and thousands of hours of online research, thousands of hours of understanding these things, trying to figure out how we can learn how they affect us, creating mental models to help us to be better and to navigate this world better.</p>
    <p>- I studied, researched, read books, took courses on love languages, personality traits, marriage, parenthood, reciprocity, ethics, religion, logical fallacies, etc so I could communicate better with you and Dub, so we could communicate better with others, so we could make better decisions in life and have better outcomes.  I talked to random strangers on the Internet about God, initiated our discussions with the Mormons, spent thousands of hours talking to Sean about God and life and all the other problems in his life, discuss God with you and Dub for however long you want anytime every time, created my own mental models about stories in the Bible and regularly try to apply them to scenarios in real life, in our lives, and make myself available to discuss them for however long anyone would need to understand them.  </p>
    <p>- I took on dozens of online investigations of real world events like ja morant, brett favre, COVID shots, twitter files, claims about Trump, War in Gaza, etc, etc learned about gathering and validating evidence, doing root cause analysis online, so that we could know the truth about things and understand how to find the truth so that we could be safer and healthier and have better lives. I worked thousands of hours researching different ways to validate authenticity of information and developed the PACER heuristic, a mental model that works so well I presented it at my work to the teams I work with.</p>
    <p>- I created my own company for the sole purpose of developing people called Ductile Bear Development, because that was what I was spending almost every free hour I had doing--researching ways to develop people--kids, adults, teams, organizations, I made thousands of notes, thoughts, results, artifacts, reasons and continually applied them to various development efforts such as coaching Dub's teams, helping Dub get various skills and abilities, helping neighborhood kids develop, made many games over the last 6-7 years intended to help kids learn and understand concepts, find ways to overcome obstacles, organize what I've learned from books, courses, video series, research online</p>
    <p>I've got plenty--PLENTY more where that came from, and I am going to keep adding to this list every day</p>
    <p>And you of all people should know that I will because I do what I say I'm going to do.  Disagree?  Then PROVE it.</p>
</div>
<div data-person="a">
    <p>With all of that studying, you would think you would learn how to talk to me. I don’t appreciate the way you speak to me. I have told you this. Seems to me that judging from your extensive list, that you are willing to listen to almost anyone else, except me.</p>
    <p>While I know you love me, you still need to work on how you speak to me. I can’t speak on anything other than benign topics (food, house, clothes, etc) without you telling me what I should be thinking. It seems as if you cannot wait to jump in and tell me how wrong I am. </p>
</div>
<div data-person="b">
    <p>When you make claims, you must be able to back them up for people to take them seriously.</p>
    <p>So what proof do you have for any of your claims?</p>
    <p>I made claims and provided proof.  Will you reciprocate?</p>
</div>
<div data-person="a">
    <p>What claims?</p>
</div>
<div data-person="b">
    <p>The claims from your last email.</p>
    <p>Let's start with these:</p>
    <p>1. I'm willing to listen to almost anyone else except you</p>
    <p>2. You can't speak on anything other than benign topics (food, house, clothes) without me telling you what you should be thinking</p>
    <p>I can't wait to jump in and tell you how wrong you are</p>
</div>
<div data-person="a">
    <p>1. Your email gave a very detailed list of people you listen to. When I tried to ask you questions the other night about a concern I had, you started yelling at me that I had no idea what I was talking about.</p>
    <p>2. For the last month or so, there have been several instances of when you have told me that what I think and feel is victim speak and how wrong I am.</p>
    <p>3. You have consistently jumped quickly to show me that I am wrong.</p>
    <p>None of this is a surprise. You can’t keep speaking to me like this and keep expecting me to just be ok with it. </p>
</div>
<div data-person="b">
    <p>Read it again.  Those are people I studied and learned from.  Big difference. </p>
    <p>It is true you had no idea what you were talking about. If you disagree, prove it by explaining to me how AT&T can see our internet traffic ? ?</p>
    <p>But... you can't.</p>
    <p>It is true that you run victim plays.  I tell you this because I love you and you need to know that it doesn't work. </p>
    <p>In the example above, at some point while I was answering your question, you began believing</p>
    <p>1) someone was wronged and</p>
    <p>2) I was the one doing it (the problem)</p>
    <p>Those are plays #1 and #2 in the victim playbook.  You would know this already if you honestly tried to learn WITH ME as I've been discussing what I've learned with you for months.</p>
    <p>Showing someone they are wrong is not bad.  It's how humans help each other learn.  Yes, we help each other learn.  That's how marriage works.</p>
</div>
<div data-person="a">
    <p>You are right. I am wrong. You are everything I am not. Thank you for always being willing to help me. </p>
</p>
</div>
<div data-person="b">
    <p>Being sarcastic and giving up will not make you better</p>
    <p>But you know that</p>
</div>
<div data-person="a">
    <p>I am not being sarcastic. I simply don’t want to fight. You are going to keep telling me how and when I’m wrong, so there is no point in me saying anything else about it.</p>
</div>
<div data-person="b">
    <p>You're giving up is what you're doing.</p>
    <p>Go ahead, finish the discussion.   How does AT&T see our traffic?    OR do you admit you had no idea what you were talking about, and didn't want to learn from me?</p>
</div>
<div data-person="a">
    <p>You still don’t understand that I wasn’t arguing with you about whether or not they could. I wanted you to explain it to me and assure me it wasn’t going to happen. But you wanted to yell at me and tell me how little I know. </p>
</div>
<div data-person="b">
    <p>Oh so you now admit you didn’t know what you were talking about?</p>
</div>
<div data-person="a">
    <p></p>
</div>
<div data-person="b">
    <p></p>
</div>
<div data-person="a">
    <p></p>
</div>
<div data-person="b">
    <p></p>
</div>
<div data-person="a">
    <p></p>
</div>
<div data-person="b">
    <p></p>
</div>
<div data-person="a">
    <p></p>
</div>
<div data-person="b">
    <p></p>
</div>
<div data-person="a">
    <p></p>
</div>
<div data-person="b">
    <p></p>
</div>
<div data-person="a">
    <p></p>
</div>
<div data-person="b">
    <p></p>
</div>
<div data-person="a">
    <p></p>
</div>
<div data-person="b">
    <p></p>
</div>
<div data-person="a">
    <p></p>
</div>
<div data-person="b">
    <p></p>
</div>