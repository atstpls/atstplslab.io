---
title: twitter files
date: 2023-01-04 19:00:42
tags: [analysis, media]
category: [ecosystem, actors]
---
<br>

Elon invites Bari Weiss, Matt Tiabbi, and Michael Shellenberger... all independent outlets, corporate media couldn't be trusted.

Summary: Power of a handful of people at a private company can influence public discourse and democracy.


The Twitter Files Index.

|-|-|-|
|[Part 1](https://twitter.com/mtaibbi/status/1598822959866683394)|2022-12-02|Jack Dorsey, Vijaya Gadde, Yoel Roth|
|[Part 2](https://twitter.com/bariweiss/status/1601007575633305600)|2022-12-08|TWITTER'S SECRET BLACKLISTS|
|[Part 3](https://twitter.com/mtaibbi/status/1601352083617505281)|2022-12-09|THE REMOVAL OF DONALD TRUMP, Jan 6 (Patrick Conlon)|
|[Part 4](https://twitter.com/ShellenbergerMD/status/1601720455005511680)|2022-12-10|The Removal of Donald Trump: January 7|
|[Part 5](https://twitter.com/bariweiss/status/1602364197194432515)|2022-12-12|THE REMOVAL OF TRUMP FROM TWITTER|
|[Part 6](https://twitter.com/mtaibbi/status/1603857534737072128)|2022-12-16|Twitter, the FBI Subsidiary|
|[Part 6 Supplemental](https://twitter.com/mtaibbi/status/1604613292491538432)|2022-12-18|TWITTER AND THE FEDERAL TASK FORCE|
|[Part 7](https://twitter.com/ShellenbergerMD/status/1604871630613753856)|2022-12-19|The FBI and the Hunter Biden Laptop|
|[Part 8](https://twitter.com/lhfang/status/1605292454261182464)|2022-12-20|How Twitter Quietly Aided the Pentagon's Covert Online PsyOp Campaign|
|[Part 9](https://twitter.com/davidzweig/status/1607378386338340867)|2022-12-26|HOW TWITTER RIGGED THE COVID DEBATE|
|[Part 10](https://twitter.com/mtaibbi/status/1610372352872783872)|2023-01-03|How Twitter Let the Intelligence Community In|
|[Part 11](https://twitter.com/mtaibbi/status/1610394197730725889)|2023-01-03|Twitter and the FBI "Belly Button"|
|[Part 12](https://twitter.com/AGAndrewBailey/status/1611568518612254722)|2023-01-06|When I took office, I swore that I would protect the Constitution. Here's why|
|[Part 13](https://twitter.com/AlexBerenson/status/1612526697038897167)|2023-01-09|How a top pfizer board member @scottgottliebmd used the same twitter lobbyist as the white house to suppress debate on covid vaccines, including from a fellow head of @us_fda|

|[Part 14](https://twitter.com/mtaibbi/status/1613589031773769739)|2023-01-12|THE RUSSIAGATE LIES|
|[The Twitter Files Supplemental](https://twitter.com/mtaibbi/status/1613932017716195329)|2023-01-13|More Adam Schiff Ban Requests, and "Deamplification"|
|[Part 15](https://twitter.com/lhfang/status/1615008625575202818)|2023-01-16|How the pharmaceutical industry lobbied social media to shape content around vaccine policy|
|[Part 15](https://twitter.com/mtaibbi/status/1619029772977455105)|2023-01-27|MOVE OVER, JAYSON BLAIR: TWITTER FILES EXPOSE NEXT GREAT MEDIA FRAUD|
|[Part 16](https://twitter.com/mtaibbi/status/1627098945359867904)|2023-02-18|Comic Interlude: A Media Experiment|
|[Part 17](https://twitter.com/mtaibbi/status/1631338650901389322)|2023-03-02|New Knowledge, the Global Engagement Center, and State-Sponsored Blacklists|
|[Part 18](https://twitter.com/mtaibbi/status/1633830002742657027)|2023-03-09|Statement to Congress THE CENSORSHIP-INDUSTRIAL COMPLEX|
|[Part 19](https://twitter.com/mtaibbi/status/1636729166631432195)|2023-03-17|The Great Covid-19 Lie Machine. Stanford, the Virality Project, and the Censorship of "True Stories"|
|[Part 20](https://twitter.com/NAffects/status/1650954036009398277)|2023-04-25|The Information Cartel|
|[Part 20 Extra](https://twitter.com/NAffects/status/1661104541004079107)|2023-05-23|The Covid Censorship Requests of Australia's Department of Home Affairs (DHA)|

<br>







|Part|Contents|
|-|-|
|1|- system based on contacts, used by both sides. More channels on left than right.<br>- October 14, 2020 - New York Post published BIDEN SECRET EMAILS<br>- Twitter removed links, posted unsafe warnings, blocked transmission via DM, locked out users<br>- Used excuse that it was hacked|
|2|- deny lists prevented disfavored tweets from trending, actively limiting visibility<br>- Prevented trending for Stanford PhD Jay Bhattacharya argues Covid lockdowns would harm children<br>- Prevented searching visibility for Dan Bongino talk show host<br>- Suppressed conservative activist Charlie Kirk<br>- Twitter denied it did that, claimed they don't shadow ban (visibility filtering)<br>- carried out by Strategic Response Team - Global Escalation Team (SRT-GET)<br>- Overseen by Site Integrity Policy, Policy Escalation Support Team (SIP-PES)<br>- banned libsoftiktok 6 times for hateful conduct knowing they did not violate Hateful Conduct Policy<br>- Justified internally by claiming posts encouraged online harassment|
|3|- Twitter execs liasing with federal enforcement and intelligence agencies about moderation of election-related content<br>- Met weekly with FBI/DHS/DNI who would tell Twitter which posts are false/misinformation<br>- Standard became misleading claims that could cause confusion, then intent, orientation, and reception<br>- Go after James Woods for calling out Twitter for suppressing Trump tweet<br>- deployed bots to attempt impossible task of making moderation decisions on various articles/posts/speech<br>- Twitter had come to believe it was their responsibility to control what people talked about, how often, and with whom|
|4|- Ban on Trump based on how the tweets are being received and interpreted<br>- Twitter admits it did not attempt to determine all potential interpretations of content and intent<br>- Employees who spoke up were overruled|
|5|- Open letter from 300 Twitter employees to ban Trump printed in Washington Post<br>- interpreted American Patriots as rioters so they could ban Trump for incitement of violence|
|6|- FTIF continuously communicated with Twitter to identify alleged foreign influence and election tampering<br>- Twitter received many requests with lists of names and they would look for reasons to suspend accounts<br>- EIP, Atlantic Council's DFR Lab, and UofWashington CIP are government-affiliated think tanks that mass reviews content and request action|
|7|- December 2019 Delaware computer store owner reports Biden laptop to FBI who issues subpoena and confiscates<br>- October 2020 owner emails Rudy Giuliani under FBI surveillance<br> - Giuliani gives to New York Post who runs story on Oct 14th.<br>- FBI sends docs through Teleporter ( one way app between FBI and Twitter )<br>- When story runs, it's censored on Twitter and FBI says to handle it like a hack-and-leak<br>- Aspen Institute tabletop exercise in September 2020 for hack-and-leak of Hunter Biden to shape way media covered it<br>- attendees were media, social media, FBI<br>- Baker former FBI ensures laptop materials are faked, hacked, or both and violate policy<br>- Twitter execs bought the influence operation from FBI and that it was related to Russia<br>- operation successfully censored and discredited the Hunter Biden laptop story<br>- FBI was paying Twitter staff under a reimbursement program for legal process response|
|8|- US CENTCOM sent 52 Arab language accounts to be allow-listed (exempt from spam/abuse flags) and used for amplifications<br>- accounts were used to deploy disinformation about middle east US actions<br>- Another 157 in 2020, pushed narrative against Russia, China, Iran, and others<br>- Fake accounts used an AI-created deep fake image<br>- when exposed, Twitter tried to play the hero|
|9|- Government began calling Twitter, Facebook out for being used as tool for foreign influence<br>- Twitter formed Russia Task Force to perform internal investigation<br>- Congress threatened costly legislation, baad press fueled by comittees
|10|Twitter rigged COVID debate by censoring info that was true but inconvenient to U.S. govt. policy, by discrediting doctors and other experts who disagreed, and by suppressing ordinary users, including some sharing the CDC’s *own data*|

<br>

First run didn't reveal much but it was discovered the FBI turned Twitter employee was sabotaging the efforts and was fired.

Content moderation decisions in run-up to 2020 elections.

Began to see emails and notations like "Flagged by FBI".  "Forwarded by DHS"

Shadow banning, controlling visibility at certain levels. 

In 2018, Twitter says they don't shadow ban. But in reality they have a complex toolbox capable of amplifying tweets to everyone or supressing it so no one sees it.  Different categories of blacklists.

Federal government regularly communicated requests.

- moderation requests from states went through DHS
- moderation requests from federal government went through FBI

Industry meetings with FBI, DHS, DNI, intelligence agencies, HHS, DOT, Pintrest, Google, smaller tech companies, anti-disinformation outlets.

bigger than 1st amendment, bigger than anti-trust, government gives twitter lists of accounts that would be suspended.

Global Engagement Center - multi-agency task force appears everywhere, sending reports on groups they consider foreign group actors.

Center for Countering Digital Hate - compiled list of people they called "disinformation dozen", regarding covid

<br>

##### Pattern

Law enforcement agencies, civil society organizations, news media, tech companies were all communicating and on the same page about censoring and modifying speech.

If Twitter wouldn't comply, requestor would forward to NYT, Washington Post, who would call Twitter.  That didn't work, call Law Enforcement agencies who would also call Twitter.  Eventually would get done 

Powerful institutions in politics.  Press and NGOs stopped acting as checks and balances, and began working together like cartel controlling narratives and mass movements.

Infodemics, information disorder, anti-vax, foreign interference, collusion were terms used, banned together to battle these politically dangerous ideas.

Bellingcat... leaks ok things that is favorable to the government.

Higgins thinks traditional news outlets have moved from rivalry to collaboration.

Hamilton 68 fiasco, Trump russia stories, Hunter biden laptop, you ever have to face your critics, they can change their minds at drop of a hat.

Stanford's morality project, created to fight disinformation, asked Twitter to supress bad effects from vaccines.

Big corporate media starting to report bad effects of vaccines, 

Censorship Industrial Complex - group of institutions that should be policing each other, but have banded together and decided collectively what should be seen and what should not.

Media should be check on govt, govt should be separate from industry, industry....

High profile journalists attended table top exercises on real world events that happened months later.

<br>

##### Shellenberger

##### Part 1

Twitter first built tools to combat spam and fraudsters.  
Began to find more and more uses for these tools.
Outsiders petitioned to manipulate speech.
By 2020, requests were routine
celebrities, unknowns, anyone
mostly requests from democrats
NY POST printed story on Biden emails Hunter
Twitter suppressed story and removed links saying unsafe
Blocked transmission via DM
Locked people out for tweeting about it

<br>

##### Part 2

Users were deamplified, not just tweets
employees build blacklists, prevent tweets from trending, all in secret without informing users
Dr. Jay Bhattacharya said Covid lockdowns would harm children
He was placed on a blacklist and his tweets couldn't trend
others got search blacklists, do not amplify lists,

<br>

##### Part 3

Twitter execs googling to make moderation decisions 

<br>

##### Part 4

Twitter execs created justifications to ban Trump
Wan

