---
title: financial crisis
date: 2023-01-16 18:00:42
tags: [fiat, crisis]
category: [ecosystem, events]
---

<br>

|||
|-|-|
|1907| - corporate trusts became targeted by Theodore Roosevelt<br> - earthquake in 1906, interest rates rose<br> - failed stock market manipulation, confidence tanked<br> - run on banks, banks ran out of money|
|1929| - Federal Reserve founded in 1913 as lender of last resort<br> - roaring twenties and investor speculation rose<br> - investment trusts were popularized, used high leveraging<br> - margin calls failed, stocks liquidated|
|1989| - portfolio insurance created to hedge against crashes<br> - designed to not fall below a certain line<br> - dollar weakening, interest rates were increased<br> - US attacks Iran, portfolio insurance sales began <br> - cascading sell orders crashed market|
|2008| - housing turned from shelter into an investment<br> - Govt lowered requirements to purchase home<br> - More buyers > higher demand > prices rose<br> - Sub-prime mortgages were bundled and sold as MBS<br> - CDOs used for diversification, CDS for handling defaults<br> - Banks now owned depreciating assets<br> -House prices fell > people couldn't pay their mortgage > foreclosures<br> - Banks went bankrupt|
|2020| - lowered interest rate<br> -  QE<br> - open to buy corporate debt<br> - economic stimmulus bill 2trillion to individuals & small businesses|


##### Mechanics

2008
 - Fed Lowered interest rates, didn't work 
  - Fed started Quantitative Easing (QE)
    - Ben Bernake
    - Inject money into financial system, lower long-term interest rates
    - Create new money, use it to buy mortgage backed securities and government debt from banks and other institutions
    - lower rates should cause more spending and borrowing 
    - get more credit to citizens
    - flood the economy, bring back confidence in the market 
    - bonds (low long-term interest) became less attractive, stocks became more attractive 

<br>

Feds tools work through Wall Street
Wall Street used its middle man position to its advantage
It wouldn't loan the money, it just sat on it 
trickle down solution 
excuse:  giving people jobs and getting their wages up 
keeping rates low didn't raise growth, it raised markets 
Businesses used to borrow, invest in themselves 
Now they borrow, and buy back their own stocks to increase stock value 
Over $6 trillion stock buybacks in decade after the 2008 crisis 
It doesn't create real wealth, 

The Great Recession was a period of marked general decline observed in national economies globally, i.e. a recession, that occurred from late 2007 to 2009. The scale and timing of the recession varied from country to country (see map).[1][2] At the time, the International Monetary Fund (IMF) concluded that it was the most severe economic and financial meltdown since the Great Depression. One result was a serious disruption of normal international relations.

The causes of the Great Recession include a combination of vulnerabilities that developed in the financial system, along with a series of triggering events that began with the bursting of the United States housing bubble in 2005–2012.[3][4] When housing prices fell and homeowners began to abandon their mortgages, the value of mortgage-backed securities held by investment banks declined in 2007–2008, causing several to collapse or be bailed out in September 2008. This 2007–2008 phase was called the subprime mortgage crisis. The combination of banks unable to provide funds to businesses, and homeowners paying down debt rather than borrowing and spending, resulted in the Great Recession that began in the U.S. officially in December 2007 and lasted until June 2009, thus extending over 19 months.[5][6] As with most other recessions, it appears that no known formal theoretical or empirical model was able to accurately predict the advance of this recession, except for minor signals in the sudden rise of forecast probabilities, which were still well under 50%

<br>

When the currency collapses
- the bonds linked to that currency 
- companies that generate cash flows in that currency
- cash is worthless
- every equity in that currency collapses 
- buy something tangible that won't lose 80% of its value overnight (boat, land, gold)
- Asset valued based on expected cash flows based on a collapsing currency wont help
- Owning every business in venezuela doesn't help you if that country's currency collapses
- Mainstream economists are using flawed mental models... like say 7% inflation is actually 2% and it's good for everyone  
- Height of COVID saw massive pile of low interest debt (4 trillion)
- Now interest rates are higher and investors are dumping their old low interest debt.
- As they dump, the resale price of the old debt goes down.
- The more it declines, the more investors want to dump. Panic is born.
- March 2020, Trump signed 2.2 trillion economic stimulus bill CARES act, 
- March 2021 Biden signedd Aamerican Rescue Plan Act 2 trillion 
- April 2021 1 trillion Consolidated Appropriations Act 
- Everyone received public money.  Provider Relief Fund spen 178 bilion tohealthcare system
- Then lockdowns strangled economy, inflation
- Fed said inflation is transitory, 