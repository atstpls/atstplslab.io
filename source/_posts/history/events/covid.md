---
title: covid
date: 2023-01-04 19:00:42
tags: [analysis, media]
category: [ecosystem, actors]
---
<br>

The Severe Acute Respiratory Syndrome Coronavirus (SARS-Cov-2) pandemic is the largest human disaster in U.S. history and caused unprecedented injuries, disabilities, and deaths.

<br>

### NEWEST ANALYSIS 

We now have a proper conspiracy.

2017: Fauci, Collin’s overturn the US moratorium on funding GoF research of concern.

2018: Daszak, Shi ZhengLi, Baric et al write a grant that serves as a blueprint for SARS-CoV-2

2019: All the authors of the grant, who never wrote another paper before nor since, were on an NIAID call discussing SARSr-CoVs

2020-Present:

-Andersen writes Holmes that he believes SARS-CoV-2 may be from a lab, they wrote Fauci

-Fauci tells Hugh Auchincloss to answer his phone, attached one of Baric’s papers, says he needs to wait for a call due to important work ahead

-Fauci brings GOFROC lobbyists to a call to berate and challenge Andersen’s thesis.

Eddie Holmes on the call: “Big Ask!” 
Andersen: “Destroy the world with sequence data, yay or nay?”

-Andersen, Holmes et al. begin ghostwriting a paper claiming a lab origin is implausible, prompted, edited, and supported by funders of Peter Daszak (Collins/Fauci through NIH/NIAID, Farrar through WellcomeTrust —> CEPI —> GVP). Privately, authors make fun of Daszak, say he couldn’t “PREDICT” a virus coming from his own lab, confess a lab origin is “so friggin likely”.

-Fauci meets with Baric to discuss Baric’s GOF work on CoVs, work Baric did with Baric’s former student Shi ZhengLi.

- Andersen et al. published, Fauci advertises their paper on international television in his capacity as NIAID director briefing Americans on COVID. Farrar advertises paper. None mention their involvement.

- Farrar, Daszak et al. write paper claiming lab origin theories are “conspiracy theories” and conspire with Daszak, Baric, and other blueprint PI’s to not sign it. Baric, Linfa Want didn’t sign it, but Daszak did without disclosing his COIs

- Fauci lied under oath, saying (1) he did not find GOF research in Wuhan and (2) he didn’t know Ralph Baric. FOIAs obtained today show Fauci’s recognition that Baric’s work (with Wuhan scientists) was GoF/P3O work and show Fauci knew Baric.

- NIAID FOIA lady hides everything throughout COVID, but Fauci’s deputy Morens cracks and admits the FOIA lady can “make emails disappear”. Morens was close friends and a confidante of Daszak, said Fauci knew their reputations are tied. Morens demonstrably violated federal records act by using his private gmail for official NIAID business, and FOI lady Marge Moore has plead the fifth.

What does this all mean?

Anthony Fauci overturned the moratorium on GOFROC, funded the group that wrote the blueprint for SARS-CoV-2, and then used his position as NIAID director to cast doubt on the lab origin theory by

(i) pressuring authors to ghostwrite manuscripts claiming a lab origin is implausible
(ii) giving funding to those authors
(iii) advertising their work during official NIAID duties like briefing the American people
(iv) sending the paper in (i) to DoS COVID origins investigators who requested all info on NIAID funded work in Wuhan in 2019
(v) pushing the US to censor a lab origin as “disinformation”
(vi) lying under other about NIAID funding labs in Wuhan, and demonstrating a knowledge of this lie’s consequences by also lying about his connection with Ralph Baric.

Currently, we see a clear pattern of NIAID officials violating federal records laws, misleading to DoS investigators, lying to congress, and hiding their knowledge of risky research behind a thin veil of expertise that an expert like me can confidently see through. Why the lies, ghostwriting, FOIA abuses, perjury, and more?

On a more societal note, why is the media letting Fauci get away with this?

We need full transparency from NIAID to either rule out their involvement in the lab origin of COVID (even if possibly revealing a conspiracy to defraud the US government by Fauci et al), or learn of a lab origin due to two bureaucrats’ terrible decision in 2017 and put all the blame on them, letting there be truth and fair trials and justice as our constitution permits.


##### Mechanics

Major Mistakes:

- We had answers and went a different direction
- We incentivized to treat with only one medicine, the wrong medicine, wrong standards of care
- We denied medications that were effective and life-saving
- Intubation, remdesivir, vaccination, ventilation were pushed before diagnosis
- bad policy and protocols
- Nigeria has less deaths than Rhode Island, India has less death than US
- Instead of enhancing treatment options, we restricted them
- isolation, sedation, ventilation were only way, never evaluated these techniques, only one strategy was adopted
- HOW, WHO, WHY

<br>


- [Agency Capture](#agency-capture)    USG agencies were infiltrated by the industry corporations they are tasked to regulate
- [Lead up](#lead-up)                  USG agencies organized and prepared for a global pandemic response
- [Origin](#origin)                    USG agencies lied and worked to hide the creator and origin of the virus from the public
- [Purpose](#purpose)                  USG agencies lied and worked to hide the true purpose of the virus
- [Response](#response)                USG agencies solutions (masks, shutdowns, and vaccines) failed miserably and caused injury and deaths
  - [Lockdowns](#lockdowns)            USG agencies administered vaccine program with zero experience and zero safety mechanisms
  - [Masks](#masks)                    USG agencies administered vaccine program with zero experience and zero safety mechanisms
  - [Testing](#testing)                USG agencies administered vaccine program with zero experience and zero safety mechanisms
  - [Treatment](#treatment)            USG agencies administered vaccine program with zero experience and zero safety mechanisms
- [Vaccine Damage](#vaccine-damage)    USG agencies solutions (masks, shutdowns, and vaccines) failed miserably and caused injury and deaths
  - [Cardiovascular](#cardiovascular)  USG agencies administered vaccine program with zero experience and zero safety mechanisms
  - [Neurologic](#neurologic)          USG agencies administered vaccine program with zero experience and zero safety mechanisms
  - [Blood Clot](#blood-clot)          USG agencies administered vaccine program with zero experience and zero safety mechanisms
  - [Immunological](#immunilogical)    USG agencies administered vaccine program with zero experience and zero safety mechanisms
- [Transparency](#transparency)        USG agencies deployed disinformation campaigns to mislead public
                                       USG collaborated with big tech, coordinated health policy with for-profit vaccine stakeholders

<br>

##### Agency Capture

First part of

Academy of Pediatric - masks for 2 year olds and older

<br>

##### Lead Up

October 2019 - Event 201 Tabletop Exercise on global pandemic funded by BMGF, WHO, WEF, Johns Hopkins (funded by NIH & Gates), Deputy Director CIA

The Wellcome Trust's Jeremy Ferrar, Fauci, COllins, began using burner phones after the outbreak to avoid trails of their conversations

Chinese knew virus escaped September 12, 2019, military

Progaganda always involves censorship because it doesn't tolerate argument, it doesn't try to persuade with reason, it's neurological manipulation

<br>

##### Origin

U.S. was planning for and anticipating possibility of a SARS-COV-2 pandemic since 2012 when DARPA's P3 program designed to end pandemics in 60 days.

NIH (Collins) and NIAID (Fauci) funded the creation of SARS-COV-2 virus at Wuhan Lab through NGO EcoHealth Alliance, even after Obama's ban on GoF research.

Fauci National Institute of Allergy and Infectious Diseases (NIAID) and Collins (NIH) held teleconference in January 2020 to create a deception campaign to lead academic and public communities that virus arose out of nature.

The official U.S. narrative for 3 years was that the virus spontaneously arose out of nature.

USG agencies FBI, CDC, DOE, NSA all admit it came from a lab now, same agencies that had oversight of what was going on.

<br>



##### Purpose

U.S. researchers followed the work of University of North Carolina Ralph Baric who was senior author in a 2015 Nature paper.

Dr. Ralph Baric led researchers in collaboration with  with Harvard, Swiss Institute, EcoHealth Alliance.  Done in biosecurity level 3 conditions.

Nature 2015 A Sars like cluster of circulating bat corona viruses shows
National Academy of Sciences - SARS like Wuhan Institute Virology poised for human emergence

Thanks NIH, NIAID, China

Not like a natural virus, made everything more difficult

Elderly, obesity, heart lung, cancer, diabetes are the most at risk

2/3 deaths could have been prevented with proper treatment, oxygen, home nursing,

Hospitals focused almost completely on health worker protection,

ASSUMPTIONS (incorrect)

- Virus spread asymptomatically, anyone could get anyone, finally debunked in 2020
- Only people with symptoms could spread
- Debunk meant no lockdowns, no social distancing, no other changes
- Great Barrington suggested focus on risk population and that's all
- White House focused exclusively on lockdowns, masks, testing
- Nothing they did  helped anyone
- End of 2020 we figured out treatment that worked
- CDC chose to rely on grouping of hospitals, incomplete data
- Early treatment was only thing in reducing symptoms
- HHS incentivized hospitals financially to hospitalize people and use ventilators
- Hospitals capitalized, took on more patients, more ventilators
- Not a single hospital in the country opened an outpatient center
- Not a single HHS committee discussed keeping people out of the hospital
- Relied on a single test, should have been multiple testing methods
- Testing should have been on acutely sick as a diagnostic tool
- FDA said only acutely sick and as a diagnostic aid, never said weekly, school, work, travel, etc.
- WHO (2021) finally said stop asymptomatic testing, but US kept testing
- CDC (2022) stop asymptomatic testing, but US continued
- Asymptomatic testing had no benefits, was a giant waste of time

<br>

Natural Immunity / Recovery

- In 2020, Pfizer, Moderna, J&J clinical trials revealed that COVID-recovered patients could not benefit from vaccine or could be harmed, they knew and they excluded these patients from receiving the vaccine.
- US did not advise that people who already had it should not need vaccines.  Instead, no one kept track.  Sports, military, schools, did not keep track.
- CDC initiatives to analyze sewer water for COVID.  It's useless, waste of time.
- US still enforcing masking and testing beyond CDC guidelines. Nursing homes were the only ones that may have needed special measures, reverse ventilations, protections, etc.  But US hyperfocused on hospitals.   85% of transmission occurred at home because it takes hours of close contact.
- Public masking had no impact on COVID-19. Viracidal nasal washes and sprays, iodine, peroxide, cylitol, coloital silver, with nasal spray and gargles, significantly reduces pcr positivity within a day or two.  those at risk, twice a day preventively, every four hours for infected.  Everything should have been focused on early treatmenet, because that's the only thing that can prevent hospitalization and death.
- If contagious we had to distance, but to test we had to do deep swabs
- Respiratory or contact-spread?  after 2 months, we knew it was respiratory, hazmat suits, isolation structures, all waste of time, overreaction.

<br>

2020 four pillars of pandemic response

1. Reduce spread of illness, viral nasal sprays washes
2. Early treatment, multi-drug protocols, with forward facing outpatient clinics and home care, not playing defense and waiting for people to come to hospital
3. Doing better with hospital, no one asking for data, no innovative approaches, no monitoring
4. Vaccination, only if there was a safe and effective, and nursing homes would be the focus

<br>

2005 CARES Act

- POTUS and HHS Director authorize
- Emergency allows countermeasures with no liability
- NIH guidelines stated no treatment until they are in hospital and are not getting oxygen
- Our government has no say as to what doesn't work.  They set boundaries and missed things like effectiveness of steroids
- Doctor advocacy group abolished ivermectin,
- Hospitals denied care over and over again, people died, and they are protected by the CARE act
- Hospitals were following guidelines like they were rules...

<br>

##### Response

- Drugs doctors found effective became the community standard of care.
- Government agencies have no business interfering with it.  
- Medical boards threatened doctors license
- Pharmacy boards refused to dispense certain medications
- Role is licensing requirements not standard of care.
- Boards and government agencies worked against doctors and nurses that were helping and saving patients.

<br>

###### Lockdowns

Lockdowns were marketed as life-saving public health measures, but wreaked avoc and did not work to stop spread.

<br>

###### Masks

- Fauci, WHO initially said they are ineffective.
- Then they flipped and mandated it, but didn't follow it themselves.
- Reinnoculation is a big risk of masks,

<br>

##### Testing

- CR testing was ineffective, but CDC manipulated
- PCR test amplifies the amount of agent you're looking for
- Creator of PCR says it doesn't test anything, it finds what you want it to find
- Data shows low correlation between PCR and disease...

<br>

###### Treatment

- There's no way the government can know the treatment for every patient.  Top-down approach cannot work.
- Symptoms were inflammation and blood clotting. Patients clearly dying of undertreatment.

<br>

McCullough protocol

- Open windows, fresh air, reduce viral density in the aerosol of the room
- Indian medical society for doctors mandated hydroxychloroclin, zinc, vitamin D, vitamin C, curciten
- July 2020, hydroxychloroquine studies showed 50% reduced hospitalization and 75% reduced mortality
- Fauci says "the data shows remdesivir has a clear cut significant positive effect in diminishing the time to recovery"
- No NIH/FDA/CDC review of older proven repurposed drugs, it's all novel/expensive pharmaceutically engineered drugs (remdesivir, vaccines)
- October 2020, trials with Ivermectin are successful
- Pierre Kory testimony December 2020
- January 2020, Tess Laurie report on Ivermectin as effective treatment
- Andrew Hill worked for WHO, worked on Ivermectin, reported in a way that made it look risky
- CNN / MSNBC lies about ivermectin, saying horse dewormer, nobel prize winning drug 2015, but it's cheap and interferes with EUA requirements  
  

<br>

##### Vaccines

- Moderna had co-written a patent with NIH years before and had a vaccine ready 3 days after U.S. declared national emergency. Pfizer and others (total of 12 on market) followed soon after.
- FDA and CDC began administrating the program without any experience, without any safety protections for U.S. citizens, and in spite of enormous conflicts of interest.
- Vaccine Adverse Event Reporting System (VAERS) showed 182 deaths after first few weeks. 
Vaccines cause long term effects
- Campaign had no independent data safety monitoring board of experts reviewing data on monthly basis, program should have been paused.
- Fauci collaborated with Facebook, Google, Microsoft (makes vaccine passports) who all invested in vaccines, huge conflict of interest taking input and aligning his message with big tech

<br>

1. mRNA induced autoimmune response (e.g. myocarditis)
2. Spike circulation/cytotoxicity 
3. Vaccine circulates to all organs
4. Narrow immunity
5. IV injection 
6. Batch variation/contamination 
7. Persistent mRNA transcript
8. LNP toxicity and accumulation 

<br>

##### Transparency

Vaccine development happened in concert with this, done in bio-security level 4 lab

- Original Pfizer and moderna vaccines (monovail) have been withdrawn from market, never were fully licensed by FDA
- Jansen removed
- Novavax
- FDA is a watchdog for drug safety, CDC is for outbreak investigation, data analytics, vitrol diagnostics
- Pfizer, Moderna, and others kept records for 90 days by regulation.  Didn't release to public but released to FDA.
- FDA wanted to block information for 55 years.  Evidence they are involved in a drug safety cover-up
- After court order, Pfizer forced to release data that FDA knew about
- That dossier shows Pfizer knew about 1,223 deaths resulting from vaccine in first 90 days, exactly matched early data in VAERS
- 3400 papers on vaccine injuries

<br>


  1) [Cardiovascular](#cardiovascular)
  2) [Neurologic](#neurologic)
  3) [Blood Clot](blood-clot)
  4) [Immunilogical](#immunilogical)

<br>

###### Cardiovascular

- Myocarditis, vaccines cause heart damage, FDA and regulatory agencies agrees vaccines cause myocarditis (heart inflammation )
- Vaccine install code for spike protein, spike protein damages the heart, more blood flows through heart the more vaccine is deposited
- Standard care for myocarditis pre-COVID, no sports due to a surge of adrenaline or stress horomones could cause cardiac arrest
- US NCAA, Big Ten League, US military were screening for myocarditis because it could be caused by COVID.  No hospitalizations, no deaths, stopped screening for myocarditis
- After vaccines, which FDA agrees cause myocarditis, no screening resumed
- Unprecedented deaths of athletes.  Average cardiac arrests in pro/semi-pro before covid 29 per year. After vaccines 283 sudden cardiac deaths.
- Death of unknown cause.  2 events when adrenaline is highest: sports and between 3-6 AM.
- Conclusive evidence that vaccines cause types of cardiac arrest, acceleration of cardiovascular disease, heart attack, stroke

<br>

###### Neurologic

- Giamberet syndrome, aascending paralysis, acute psychosis, progression of parkinsons and other neurologically debilitating diseases, small fiber therafopy
- Seizers, blindness, hearing loss, 

<br>

###### Blood Clot

- All vaccines unequivocally cause blood clots, 11% are fatal
- Resistant to blood thinners, large and rubberized and won't dissolve
- Blood clots in arteries and veins of eyes
- Woo paper from FDA is definitive, clots diagnosed by ultrasound or phenography

<br>

###### Immunilogical

- VITT blood disorder, clotting and bleeding, 1/3 die in hospital
- Multi-System Inflammatory Disorder (MSID) - typically in younger people, organs damaged,

<br>

Denmark clearly shows batches of vaccines are not the same
- 3 risk groups of batches
- 1st batch 1/3 people got, no side effects
- 2nd batch 2/3 mild to moderate side effects
- 3rd batch side effects throught the roof
- product manufacturing problem, possible hyperconcentration of mRNA
- FDA/CDC got analysis with request to analyze, reply was they disagree, no variability problem
- 15% of people that took vaccine have problems (Zagbee), 7% (VSAFE) have to go to hospital for acute problem
- Multiple reporting systems worldwide show same thing ( Zagbee survey, CDC VSAFE data, VEARS, yellow card, viggie safe, utris system )

<br>

WHO
- June 22 issued a recall for vaccines on market
- Dec 22 McColloguh, Senate Panel concludes we should withdraw all vaccines for unacceptable safety and harm to Americans
- No response from regulatory agencies, no change in stance, they continue to support vaccines with illegal and unlawful promotion
- HHS, FDA, CDC, Vaccine companies cannot advertise vaccines on tv, media
- No one should mandate vaccines that are experimental, emergency use authorized, and causing permanent serious effects

mRNA - a genetic material that teaches cells how to make a proteins — to trigger an immune response in the body

<br>

Treatments
- nanokinates, Chinese have used for years, mild blood thinner, Japanese showed it can dissolve spike protein
- clots are not affected by human enzymes
- curcumin was positive
- bromoline
- anocenalsistine
- hypobaric oxygen

<br>

mRNA through foods
- Chinese proved you can immunize mice with mRNA-laced milk
- USDA has array of vaccines they are in code development with companies to vaccinate food supply, vegetables and livestocks
- Self replicated RNA vaccines are used in pork to prevent disease, but experts believe not needed, traditional vaccines are fine for livestock
- Drug companies driving it, proposed for cattle
- Americans need transparency if genetic vaccines are used to protect animals
- USDA vaccinations against hepatitis, infectious diahreahs, etc.

<br>

Unvaccinated have best overall covid outcomes anywhere there's a fair assessment
- US data not reliable, CDC not releasing data
- Majority of hospitalizations and deaths were the vaccinated ( good data in UK, Australia, south africa, )
- Unvaccinated have lowest risk of recurring omicron infection, every additional shot the risk of omnicron increases
- Breastfeeding women study showed mRNA in breast milk
- American Asociation of Clinical BLood Banking and Red Cross about concerns of blood contamination
- ARC refused to add checkbox to identify vaccinated blood, but are currently reordering all their data entry forms to include terminology for gender/sexual preferences
- mRNA shown to circulate in blood stream atleast 28 days
- Those that have gone through Delta or Omnicron, zero risk of hospitalization and death
- Never take a shot for an illness we've already had
- No benefit, never been a prospective reanimized double blind placebo controlled showing the vaccine reduces hospitalization and death
- does not reduce severity of disease
- does not stop or reduce spread or transmission (CDC)
- no meaningful benefit

<br>

Australia pre-vaccinated entire country, 99% who were hospitalized and died were fully vaccinated
- CDC in May 2021 overwhelmed by fully vaccinated Americans hospitalized and died, announced they cannot track breakthrough infections the vaccines failed so badly
- Unvaccinated people died of COVID because of lack of treatment
- Monoclonal antibodies consistently worked, but only 15% got them in emergency care

<br>

Why was it created
- NGO go between Eco Health Alliance
- study thank Wuhan, NIH, Eco, and acknowledge it's GoF research
- started before Obama banned it, grandfathered in, outsourced it to Wuhan Institute of Virology
- Berrick paper shows they made a chimeric virus, made a monoclonal antibody, and a killed vaccine
- DARPA and BARDA division of NIH think new types of warfare are biological warfare
- Bio labs are working on enhanced biological threats and countermeasures (monoclonal antibodies, vaccines, therapudics)
- We have countermeasures for anthrax, small pox, monkey pox)

<br>

3 papers Subermania, Kampf and Beedee
- show least vaccinated countries had lowest amount of covid and lowest number of deaths
- U.S. leads world in COVID deaths, we're only 1/6th of the worlds population

<br>

Pharmecutical and biological product advertising laws 
- there must be fair balance on claims on what it does as well as side effects
- Vaccines are promoted without fairly telling them about side effects and not licensed

<br>

- Dozens of randomized trials, masks are ineffective
- CDC is now back to normal recommendations
- 3 years, CDC hyperfocused on masks while people were dying
- No focus on treatment of high risk people

<br>
Helrick

1) deductive, inductive, independent thinking in science is declining
       influenced by agribusiness, big pharma, government, we abolished free thinking
       truth is ...   supreme court justice can't answer the question, what's difference between man and woman

2) lack of free speech
       takes away ability to resolve problems, and all other rights like science, good practices
       gives control to agencies not interested in truth
       inalienable rights were taken away, couldn't protect our bodies, religion, movements, speech

<br>

Matriana

- covid was politicized, media covered government, shamed unvaccinated, inconsistent rules, rulemakers broke rules
- rigging of covid deaths, coroners association asked DoH where numbers came from, fabricating numbers to keep counties shut down
- lack of science sending sick back to long term care facilities, plagues spread in these homes, killing these because older were most vulnerable
- hypocracy, duplicity, politicization of the virus

Attorney Thomas Renz

- There are manufacturing discrepancies and lot variability
- Lawsuit against EcoHealth Alliance who facilitated this virus, funded by Fauci & USG
- Was created under and with the knowledge of US government
- Concept of ventilators came from China, even after we discovered those were killing people, they kept doing it to protect health workers, no one told the families ( up to 90% put on ventilator died )
- No Ivermectin, but yes to ventilators?
- Why keep doing it with 90% fatality rate

All US Government public health agency budgets are heavily dependent on fee-for-service research work contracted directly by the pharmaceutical industry in exchange for “user fees.”

Half FDA's budget is from industry user fees.

75% of approval testing program budget is paid for by pharmaceutical companies.

n addition, government scientists are allowed to own patents derived from the research they do for private corporations. Government scientists can receive royalties of up to 150,000 per patent on top of their salaries. [13] For example, Anthony Fauci, director of the National Institute of Allergy and Infectious Diseases (NIAID) and Chief Medical Advisor to the President, co-owns six HIV related patents. [14] This sort of direct financial entanglement constitutes a very dangerous conflict of interest. 

the young have very little to fear from this disease, while the very old face very real risks. Policy should have reflected these facts, but it has not.

 The pandemic has seen record surges in fatal drug overdoses and homicide. The CDC found a 28 percent increase in drug overdose deaths from April 2020 to April 2021. [55] While the homicide rate increased by 30 percent. [56] Bizarrely, traffic deaths went up by 7 percent in 2020, even as the total number of miles driven declined by 13 percent. [57]

  loss of employer-provided health insurance as people lost their jobs — have had the unintended effect of delaying care for some of our sickest patients.” [58] The authors reported, “sizable decreases in new cancer diagnoses (45 percent) and reports of heart attacks (38 percent) and strokes (30 percent). Visits to hospital emergency departments are down by as much as 40 percent, but measures of how sick emergency department patients are have risen by 20 percent

How can you say my body my choice and also be for mandated vaccines and lockdowns?

Mosquitos can be used to vaccinate certain populations

Looking at alternatives to vaccinate through food, drink, mosquitos

If you're not doing genetic transmission, you don't need to do anything.  If you interfere with my health, my genome, I have the right to know. Doesn't add burden to anyone, unless they're modifying people medically

- Ralph Berrick through EcoHealth Alliance took advanced biotech to create a biological weapon
- Transferred it to a lab that is known to be controlled by the CCP who have stated they want to use bio weapons against the U.S.
- Did DoD and CIA have knowledge?   Who else knew ?  Flubbed intel operation ?
- Fauci and CDC is actively involved in biological weapons

Steve Kirsch
- All vaccines are hurting kids
- No study of vax/no-vax study of kids

pharmaceutical companies have insisted on total protection from vaccine related lawsuits. As The Financial Times explained: “Before deals could be agreed, Pfizer demanded countries change national laws to protect vaccine makers from lawsuits, which many western jurisdictions already had. From Lebanon to the Philippines, national governments changed laws to guarantee their supply of vaccines.

We do not have vaccine on market that is licensed and FDA approved
We no longer have emergency pandemic
We still have mandates across the country for the vaccine

<br>

##### Analysis

1. SARS-CoV-2 is related to bat SARS-like coronaviruses from southern China and northern Laos

2. SARS-CoV-2 entere humans in or near Wuhan in August-November 2019

3. SARS-CoV-2 entered either through natural accident or research accident

4. Wuhan is located 1000 km from nearest wild bats with SARS-CoV-2-like coronaviruses

5. Wuhan has labs that conducted world's largest research program on bat SARS-like coronaviruses and possessed world's only sample of SARS-CoV-2

6. In 2016-2018 Wuhan Institute of Virology constructed series of coronaviruses that combined spike gene of one virus with rest of genetic information of another virus, and identified they could infect and replicate efficiently in human airway cells that had 10,000x enhanced viral growth and 4x enhanced lethality in mice engineered to desplay human receptors on cells (bsically built a human pandemic pathogen)

7. In 2018, Wuhan Institute of Virology tuned it for human receptors to increase transmissibility

8. In 2016-2019, Wuhan Institute of Virology constructed and characterized coronaviruses at biosafety level 2, a level inadequate for work with potential pandemic pathogens and inadequate to contain virus having high transmissibility 

9. Wuhan has withheld information, misrepresented facts, and obstructed investigation.

10. Preponderance of evidence scientific (1-3), documentary (4-9), indicates SAR-CoV-2 likely entered humans through lab accident

2021
- Made people choose between jabs and jobs
- Threatened to withhold medical care
- demonized and censored anyone who stood up to them
- lied as the shots failed
- 

ivermectin 

claimed it was a horse dewormer, said it was ineffective

FDA claims it's dangerous horse drug

CDC warns against taking this medicine

Doctors prosecuted for using safe, generic, life-saving measures.

Twitter suspends accounts Robert Malone inventor of mRNA vaccine technology

Physicians were labeled as right wing, investigated

response was built on global vaccination policy

US policies are written by big pharma, design trials to fail to disprove use of cheap medicine, make them appear that they don't work

Manipulate outcomes of clinical studies, academics don't get further research grants if they go against narrative

2/3 of world's non-commercial biological research is funded by Wellcome Trust in UK, NIH (NIDIA under Fauci), BMGF

Pattern of misleading public by withholding information using modern technology, media, censorship 

safest and effective, wide variety of treatments, anti-inflammation, anti-viral, and anti-tumor properties

stops replication, 

1986  Actgave  pharmaceutical  companies  immunity from liability for injuries caused by mostof their vaccines andinstead made vaccine safety the responsibility  of HHS


"They only people you can blame, and this isn't shaming, maybe they should be shamed, are the unvaccinated" - Don Lemon, CNN
"We have to start doing things for the greater good of society, and not for idiots who think they can do their own research"  -Don Lemon, CNN
"Oh you can't shame them, you can't call the stupid, you can't call them silly... yes, they are"  -Don Lemon, CNN

"It is the unvaccinated who are the problem, period, end of story" - Mika Brzezinski, MSNBC
"...pointing back to the unvaccinated, who are really creating a problem in this country. Every death that we are seeing from COVID could have been prevented" - Mika Brzezinski, MSNBC

"Anyone you came into contact with will blame you, as will the rest of us who have done the right thing by getting vaccinated" - Jonathan Capehart, MSNBC
"Don't get me started on the lunatics who won't take any of the COVID vaccines" - Jonathan Capehart, MSNBC

"You're basically punishing the vaccinated for the sins of the unvaccinated" - Erin Burnett, CNN
"Rogan telling his 13M Instagram followers that he was treated and he included Ivermectin a drug used on livestock" - Erin Burnett, CNN

"The anti-vaxxers, they seem to have a thing for death and home remedies" - Joy Reid, MSNBC

"Life is too short to be an ass, life is way too short to be ignorant to the promise of something that is helping people worldwide" - Neil Cavuto, FOX News

"Those who are not vaccinated will end up paying the price" - POTUS Joe Biden

"Literally, the only people dying are the unvaccinated. And for those of you spreading misinformation, shame on you" - Chuck Todd, MSNBC



----------------------------------------------

Mario
- Why can't we see the data now?  Why hide it?

Liza
- It takes a long time

Brian Tyson
- Where's data?

McCollough
- refused to release data
- when they were forced, 90 day revealed 1200 deaths 
- post-marketing data must be transparent

Liza
- people who sell supplements should be held accountable too
- thrombalitics are dangerous, pharma standard should be across the board 

Brian Tyson
- they sell a new technology with limited liability

Jason Howe 
- individuals driven by profit, greater opportunity for abuse, must be debate, transparency to get trust 

Liza
- there wasn't fair public debate

Mario
- Whenever crisis, no room to debate, survival mode, 

Brian Tyson
- Priority should have been front line doctors, instead effective early treatments were suppressed
- discussions with actual physicians would have saved thousands, even hundred thousands

Liza
- losing scientific debate over politicizing 
- doctors are reluctant to have public discussions because of legal recourse

Mario
- Why would they be in legal trouble if they're not lying

Sabine
- if they're afraid to talk, they shouldn't be leading 
- lack of transparency
- lies about ivermectin, hydrocholoroquine, 

Liza
- hydrocholoroquine overdoses can give you blindness
- potential for harm outweighs benefits

Brian Tyson
- those are highly irregular, 100 percent wrong 

James Thornton
- Pregnant women were 

Tirah Att
- unfortunate that medical institutions are being delegitimized
- people don't trust experts
- Hotez shouldn't need to, it would be messy

Brian Tyson
- Next pandemic, work with local public health department
- They didn't want any front line treatment data

Avery
- patients read Google, so much information, we're constantly battered by this and used to mistrust
- we just reassure them, tell them what we know, how we do 
- we're not responsible for getting word out to masses

Sulaiman
- What about holistic approach, during a pandemic

Avery
- That's fine, if 40 percent don't believe it, too bad 

Brian
- During pandemic, there's no need for studies
- it was polarized because people didn't listen to successful early treatments

Sabine
- Shut down treatement, it's like trying to figure out what hose you're using when house is burning down
- Why can't doctors trust data? Nothing like front lines, no QT interval seen like data says
- You listen to those who achieve success, government didn't do that
- Courageous doctors that were brave enough to prescribe drugs
- Instead we  stopped treatment and made doctors fearful for using off label meds 
- 300 clinical trials, Fake paper in the Lancet, fake data about high QT interval
- Bill Gates funding trials, how was that allowed, why stop treatment off label
- Why 400 papers retracted
- Why were doctors bullied and censored
- Why did we vaccinate people who already had COVID

Heather Ges
- Debate needs to happen
- Pre-licensing clinical trials, vaccines on CDC schedule were never tested against placebo
- Derogatory terms used and told to listen to the science
- Saying discussion will do "More harm than good" is censorship


Heather & Peter McCollough
-  CDC vaccines did not have placebo 

Liza
- the placebo is saline
- you can't blind

Peter McCollough
- interesting studies of unvaccinated having better outcomes than those that had full schedule
- 

Sabine
- Hcq given to millions of arthritis patients
- Ivm is safe and millions use it

1986 Vaccine Injury Liability Act
- broad protections for vaccine manufacturers
- with "unavoidable harm", companies need liability protection 
- all measures to monitor safety and periodic review are non-existent

Aaron Siri
- National Childhood Vaccine Injury
- up to 86, only 3   MMr, DTP, 
- DTP was facing 200x liability than revenue it was bringing in
- Congress decided to do it different that any other industry and eliminated liability they had for unsafe products
- Not just a one time thing, it was from now on...
- Gave manufacturers they had a guaranteed market (most states mandated them), no liability, free advertising

Liza
- Medical malpractice suits in U.S. are because the tort system allows medical litigation

Amish
- Liability law said ok we'll have injuries, but we'll have compensation fund
- Medical malpractice litigation system is broken
- Compensation fund is broken system

Aaron
- Pharma companies should make money for their shareholders
- We incentivize companies to self-regulate safety by ensuring bottom line hurts when their products are unsafe
- Pre-licence clinical trials for top 4 lasted years and were tested against placebo
- Covid had much shorter trials and tested against experimental vaccine
- With drugs, pharma have an incentive to know safety profile because they'll be liable
- With vaccines, they won't be liable so they take quickest path to shelf 
- They hyperfocus on efficacy
- billions of guaranteed revenue 


- When you mandate vaccine as safe and effective, it is individual and civil right to know 

Liza
- If you want pharma to make drugs and vaccines, can't litigate

Aaron
- Drugs that are harmful come off the market

Liza
- you can have science-free litigation


Brian Tyson
- 3 main variants of COVID: ALPHA, DELTA are both deadly
- Pfizer vaccines were effective against these early in pandemic, those who had one vaccine did well
- Now those with serious illness have had atleast 3 COVID vaccines, Disease has progressed to outweigh benefits of vaccine

Todd
- Why can't lead expert on pro vaccine staff have debate
- Trust the experts they say, why won't they talk about autism
- We need to talk about why autism went from 1/10K to 1 in 25
- Doctors and physicians should understand vaccine ingredients, possible side effects, reactions

Liza
- Why has medical schools put pharmacology on back burner (Washington University) has short class on pharmacology

Todd
- Common people do not trust medical, because they talk out of both sides of their mouth
- One side saying safe, other side have people dying two days after taking vaccine
- There's a problem, we can't discuss, licenses suspended for speaking about ivermectin

Mollie
- Hotez chose to be public in criticsm of people, calling out misinformation, 
- In the realm of criticism, but unwilling to debate

Sabine
- Freedom of choice, religious reasons whatever, shouldn't be forced to take something
- part of 10 year trial that failed, this comes out and approved in 8 months and hurts people

Mario
- Stop using pro-vax and anti-vax, puts people in box

Gounesh
- Vaccines saved lives, should have been mandated

Mollie James
- The problem that made this pandemic was the spike protein
- Shots instruct the body to make the same spike protein that virus uses, complete failure
- Everyone who is at low risk for COVID doesn't need a shot
- Everyone who is at high risk for COVID is at high risk for the shots because it increases spike protein 1000x worse than virus
- adverse event after adverse event, people say long covid
- it's not long covid, people who are suffering now are those who trusted their doctors/media, and got vaccines and boosters
- No one will help them, spending massive amounts to try to get answers, with zero opportunities for improvement
- Pharma is profiting from this and have no incentive to help
- People are forced to find doctors that can actually treat COVID and willing to speak out against the narrative

Liza
- Bad disease for significant number of people, pandemic, tragedy 
- We made mandates on the fly, which we weren't willing to walk back from
- vaccine is incredible feat of science
- suppressing discussion created doubt 

Mollie
- Doctors knew and did nothing


Liza
- There's not a big conspiracy

Mollie
- I was fired from 3 jobs from trying to treat patients in ICU
- people don't want to go against the grain because they don't want to get fired
- Fauci never been challenged, that's a problem

Brian Tyson
- Doctors were having significant impact, but everyone closed their doors
- people were saying there was no outpatient, wait 10 days before we can treat you, they show up to hospital in terrible condition
- Patients have best chance with early treatment, can't fix someone who waited 14-21 days without any sort of treatment
- if they listened to what we were telling them, they would not have died

Mollie
- agree with Brian, huge success using his protocol, pulled people out of home
- hospital were severely undertreating

Bryan
- for lower risk population, evaluate, call in 3 days if no improvement, if so start treatment 
- pharmacists would not dispense medications, ERs wouldn't dispense, pharmacies wouldn't dispense
- But every one that got that medication lived

Anish
- there's equipoise, whole group of doctors that think this doesn't work 
- if these treatments were so effective, you could prove it with a small population


Brian Tyson
- State of CA was in clinic for 1.5 years    gave 10k data
- only interested in demographics, comorbidities, age, and sex
- asked why not interested in reinfection rate and treatment data?
- they said they're not collecting that on purpose
- no one would publish that data


Anish
- Fine they messed up, but you can still do the test now to show efficacy

Defeat the Mandates
- Even when we knew no benefit for college age, colleges kept pushing mandates, medical community didn't speak out
- false equivalency, no one fired for taking the vaccine, no one deplatformed for taking the vaccine
- no reprecussions for people promoting the vaccine
- Can't just say that's unfortunate, we need fundamental change
- Very expensive public health system did not get message to very expensive education system




----------------------------------------------

