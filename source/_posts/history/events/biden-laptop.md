---
title: biden-laptop
date: 2023-01-19 18:05:42
tags: [fiat, crisis]
category: [ecosystem, events]
---

<br>


##### Hunter Biden Laptop

Initial experts falsely claim laptop is fake
Media jumps on board instead of digging deeper
Washington elites point at the media and say "See? The laptop is fake!"

<br>


Ex-FBI Andrew McCabe fired for leaking classified documents, CNN hires him to give judgement against Trump.

Ex-DNI James Clapper is one of 51 people who used their position to tell public laptop was Russia collusion instead of revealing

<b>An organized effort by intelligence community (IC) to discredit leaked information about Hunter Biden before and after it was published.</b>

- Hunter was given tens of millions of dollars for work he didn't do from foreign businesses
- His laptop was discovered to have criminal evidence by computer shop owner who called Giuliani who was under FBI surveillance
- Giuliani called New York Post who ran story on October 14, 2020
- FBI took possession of laptop in December 2019
- FBI began convincing journalists, social media executives, and general public that laptop was a Russian hack-and-leak operation
- The Stanford Internet Observatory published a report urging news media to focus on the perpetrators of the hack and leak, rather than the contents of the leak.
- Aspen Institute hosted tabletop exercise (pre-bunking operation) to shape reporting around a potential Biden hack-and-leak
- Facebook, Twitter, reporters at the New York Times, Washington Post, and CNN, in the summer of 2020, months before the October 14 publication.
- During all of 2020, the FBI and other law enforcement agencies repeatedly primed Twitter laptop was Russian hack-and-leak operation.
- Ex-FBI Jim Baker and Dawn Burton joined Twitter, were ones who initiated investigating Trump

<br>
