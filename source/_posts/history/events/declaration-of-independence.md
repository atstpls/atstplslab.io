---
title: centralization
tags: [hierarchy, government]
category: [ecosystem, concepts]
---

<br>

Jefferson derived this from sentiment from John Locke

John Locke said humans have inalienable rights by virtue of being human among them life, liberty, and property  

property enables your pursuit of happiness, not only have it but use it however you want


Jefferson - property is part of a broader category of inalienable rights, with other things you can use to build happiness, prosperity

We are endowed by natural law with these rights

No one has authority to take our life, our liberty, and our property

Laws made to reflect these rights, when you violate them, you can be held accountable by the law for your actions

Law is changeable by man, but it is not man

If you have a legitimate claim on ownership, 

we are individuals, are own abilities, we're equal in our capacity to use our faculties and operate in the world around us 

If you are controlled, you are now subject to those that control you

This applies to belief, thought, speech, life, property, freedoms, actions
