---
date: 2023-03-05 07:06:30
title: analysis of media
description: We are the star and the papparazi
tags: ["analysis"]
category: [protect, tactics]

---


90% of US media are owned by just 6 corporations ('96 Telecom Act). Much of what is fed to us is propaganda.

I previously worked for the press, and cringed every time Trump said 'fake news.' But through COVID they all strangely parroted the same lies.


<br>

The digital world is full of actors who manipulate information.  Your capacity for critical thinking and online research is essential for navigating this environment.

The challenge is not just identifying false information, but also countering the emotional pull of false information.

<br>


##### Mechanics

Validate information before using it

- To extract accurate, credible information from messages broadcasted by individuals from all around the world, we first must decide this:
- Are we going to validate information ourselves or do we let someone else do it for us?
- If you choose to outsource it, you still must validate the individual or group you are trusting to perform it correctly.
- Outsourcing makes you less knowledgable, less capable, and a more attractive target for those untrustworthy actors and groups you need to avoid.

<br>

These skills are confirming [AUTHENTICITY](#authenticity) and evaluating [CONTEXT](#context) and [PATTERNS](#patterns):

[AUTHENTICITY](#authenticity) - challenge actor authenticity

[CONTEXT](#context) - challenge actor conditions, motives, audience

[PATTERNS](#patterns) - challenge actor tactics and history

<br>

Verification is a process. It is rare that all of these checks provide clear answers.

<br>

###### AUTHENTICITY

Behind every message is an actor who may be biased, incorrect, or untrustworthy

First verify the authenticity of the actor

  - Actors are writers, reporters, editors, producers, bloggers, citizens
  - Sources are companies, citizens, groups, governments 
  - Messages are news, stories, publications, programs, magazines, tweets, posts, websites
  - Interpreters are readers, listeners, viewers, users, followers, fans, citizens

  <br>
  
- Actors use information from sources to build messages that they broadcast to interpreters
- identify all possible digital traces of that person, and the piece of content. (For example, we examined other social media accounts, mentions by media articles, information about university, affiliations, etc.). The search was aimed at determining if the person was a reliable source, and if there was a trace of the information they provided elsewhere online.
second step was to use the information collected to build a profile of the person, as well as a profile of the content they provided. For each of the 5Ws - who, what, when, where and why administrators had to carefully determine what they could prove, and what they could not

<br>

1. Source: Who uploaded the content?

- Researching the history of an uploader 
- Maltego
- pipl.com webmii.com and linked in for cross referencing names, usernames, etc.
- Are we familiar with this account? Has the account holder’s content and report age been reliable in the past?

<br>


###### CONTEXT

- All messages have distinct bias, priorities, ideals, interests, and embedded values that shape and influence our worldviews.
- As humans, we all have our own spin on things, we all make mistakes and some of us should not be trusted.

<br>

<b>Conditions</b>

- Did it originate from an eye witness or someone who received and broadcasted.

<br>

<b>Motives</b>

- It’s much easier to beat someone when you know what they want to do.  
- Context can allow you to negate and then capitalize on an actor's intentions.
- Trying to increase size and reach audiences and broadcast to smaller geographical areas

<br>

<b>Audience</b>

- Is this someone trying to meet the demands of an ongoing stream of gossip, rumor, and opinion
- Is this a Docudramas and staged disagreements, fremium business models, paywalls,  

<br>

Identifying [media tactics](social-media):

- false connection - headlines/visuals/captions don't support content 
- false context - genuine info is shared with false context 
- manipulated context - genuine info is changed 
- misleading content - frames an issue or individual 
- imposter content - genuine sources are impersonated 
- fabricated content - completely false content 
- Fake News and Feel Good stories 
- Hegemony 
- Spiral of Silence
- Cognitive Dissonance 

<br>

They require proven processes, trustworthy tools, and tried and true techniques. 

 - Challenge actor messages 
 - Triangulate what they provide with other credible sources and verify what is true
 - Identify what is false or not adequately verified.

<br>

###### PATTERNS

Online profiles leave a digital footprint that allows us to examine history, activity, and digital footprint.

Connected accounts, friends/followers, online social circles, location, reliability, bias, agenda...

Do they have a history of:

- censoring content in a manner that undermines media freedom
- sowing confusion and discontent 
- discrediting individuals or organizations 
- ignoring professional standards and ethics
- spreading disinformation that reduces public trust
- remaining reluctant to accept responsibility for publishing oversights
- More controversial issues where people are likely to hold vastly different and often polarised belief structures
- More to come... computational propoganda AI image and video manipulation, 

<br>

It's not helpful because it clouds the vision of what we actually know and do not know 


<b>LACK OF INFORMATION</b>
  - What app was used?          IG LIVE
  - What rules were broken?     NBA's CBA
  - Who first reported this?    ATHLETIC, IG viewers, twitter

<br>

<b>LACK OF EMPATHY</b>
  - wouldn't make the same decision, but I can find many reasons why he might
  - acknowledge his everyday life is different, his priorities are different
  
<br>

<b>TRUTH DECAY</b>
- increasing disagreement about facts and data
- declinint trust in respected sources
- increasing relative volume of opinion over fact
- a blurring of the line between opinion and fact 

<br>

Polarization - we're tribal and democracy is difficult 

- 2009 added fuel 
- like button, retweet button, algorithmicize everything created an outrage machine
- social media runs on advertising dollars, designers keep us engaged
- incentivized to present information that draws us in and keeps us, most engaging content is emotion, moral outrage
- Tribal emotions
- Supernormal stimulus has been manufactured to 

<br>

CANDACE OWENS LAWSUIT

- USA Today prints false fact check so that Facebook would use it in its factcheck
- Then all users when presented a factcheck box, will click on the USA Today article
- Redirecting traffic for financial gain ( clisks and views that increase advertising revenue )
- Also demonetizes user because potential sharer gets message: Notice will be added to your post if you share
- Block and obstruct viewability of user posts and undermine the content within
- Posts and sharing are controlled by false information warnings

