---
title: analysis of bears
date: 2023-04-07 13:58:48
tags: ["security", "defense", "tools", "tactics"]
description: Bears
permalink: /bears/


---
<br>

##### CHARACTERISTICS

- Survival depends on understanding seasons ahead
- Spends harsh winter months underground
- Adapt to changing seasons
- All bears are omnivores, but each has preferred diet  
- non-retractable claws, shaggy fur, excellent sense of smell, short tails
- solitary, except for mothers with cubs, generally diurnal
- long-living with life spans of 25 years in wild, 50 in captivity
- when food is scarce, effective survival solution is sleep

##### LIFE CYCLE

- As hibernation begins, bear prepares
- Days get shorter, heart rate slows, body temperature drops
- Falls into a deep sleep
- Give birth in and out of sleep
- Cubs eat and sleep in den until spring time
- More mouths to feed takes toll on body
- Fat cells shrink, energy stores used up producing milk
- Lose a quarter of body weight
- Temperature below zero
- Spring comes, sunlight felt on skin, sensed by eyes, temperature tells bear to wake
- Emerge from den, long journey to find food
- Cubs survive on mother's milk and grass
- Mama bear needs nutrition
- Bear smell 2000 times more powerful than ours
- Finds soft shell clams underneath the mud
- Can pry them apart with teeth
- Finally make it to river's edge, salmon is their first real meal in almost 8 months
- Salmon will allow them to make it through winter


##### TYPES OF BEARS

- polar bears are carnivores and prefer seals, also walruses, seabirds, fish, builds up fat stores during winter
- american black bears prefere berries and insect larvae , winter in dens but don't hibernate, how they survive is a mystery
- asiatic black bear plant eaters, fruit nuts insect cattle sheep goats mostly vegetarians but will also eat small animals, main predator is siberian tiger
- giant pandas eat only bamboo, massive jaws and chewing power
- sloth bears prefer ants and termites , also leaves honey flowers fruit
- strong shoulders and back for digging out dens and searching for food, use sounds, movement, and sounds to communicate, brown bear, up to 1320 lbs, fruit nuts, roots, seaweed - - - grasses moss bulbs insects fish small vertebrates, carrin mice squirels, great plains, desert, woodlands, forests, alpine meadows, and prairies
- Like areas along rivers and streams
- - harsh weather and little food, hibernation minimized energy, sprint at 35 mph, live to around 25 years old
