---
title: 5GW
date: 2023-01-14 14:00:42
tags: [debate]
category: [reference]
---

<br>


While some theorists in the xGW framework still
 use the term generation, the elements of the taxonomy are now
 generally known as gradients. The gradients of war, like gradients
 we see in other elements of social organization (wealth, height, skin
 color, and so on) flow indefinitely into each other, and their
 emergence pre-dates written history. The first 0GW occurred
 thousands of years ago. According to the xGW framework, the first
 5GW was fought and lost before the dawn of time.

|||
|-|-|
|0GW|genocide and a holocaust.  The entire able population fights. No difference between civilians and soldiers Ant colonies regularly engage in 0GWs. The Great Sioux Uprising that temporarily removed all whites from what is now South  Dakota, for instance, was rapidly followed by the removal of most Sioux Indians onto reservations.|
|1GW|Characterized by concentration of labor, or the attempt to win by selecting those most able to fight and concentrating them in one place for battle. Chimpanzees are capable of 1GW, with rival troops forming insertion teams, engaging in pitch battles, and other civilized behaviors. Many European conflicts around the time of Napoleon were also 1GWs|
|2GW|In response to 1GW rank-and-file formations, fires (i.e.,  explosive and kinetic energy delivered by a variety of means, such  as artillery) are massed to shatter their cohesion. Concentration of firepower. 2GW allows victory through producing deadly goods and aiming them at the same place at the same time. More of the fighting society is now involved in essentially economic or technical roles, taking men away from the front lines while keeping them in the fight.|
|3GW|Massed fires are countered by maneuverability.3GW comes from better minds. The most famous 3GW was the German Blitzkrieg against France in 1940. The French had better fortifications, better equipment, and even better tanks. owever, the Germans had a better trained officer corps that knew how to create uncertainty by maneuvering inside and out of the French lines, preventing any meaningful counterattack and paralyzing the French. The “Shock and Awe” defeat of Saddam Hussein's Iraq in 2003 relied heavily on 3GW, as did the 100-hour campaign during the first Gulf War.|
|4GW| Maneuver forces are proved inadequate in the face of an asymmetric adversary who exploits the full breadth of the maneuver space (not only military but also civil) by denying sanctuary to 3GW units.
4GW war is so peaceful that the warfighter becomes a criminal. The wars America has lost, including Vietnam, Lebanon, and Somalia, have all been 4GWs (Hammes 2004). Defeat in Vietnam so broke America's will to fight that America's institutional knowledge of occupation warfare was eliminated, and would not be restored until years into the Iraq War. COIN in 4GW often involves either degrading the opponent into an earlier generation of warfare that can be defeated conventionally, or else providing incentives for some part of the 4GW force to turn on their comrades. Proponents of 4GW, such as Paul of Tarsus in the Christian revolt against the Senate and People of Rome and the Chinese Communists in their wars against Japan and the KMT (Mao 1961), accept a more violent path to power than in 5GW as the cost of not being able to blind the enemy to the existence of a war.|
|5GW| Moral and cultural warfare is fought through manipulating
 perceptions and altering the context by which the world is perceived. Violence is so dispersed that the losing side may never realize that it has been conquered. The very secrecy of 5GW makes it the hardest generation of war to study. Because 5GW attacks occur below the threshold of observation, COIN in 5GW is the preemptive, system-wide, automatic degeneration of 5GW forces into more primitive gradients of warfare. The 5GW warfighter hides in the static, and the most successful 5GWs are those that are never identified|

<br>


5GW is the deliberate manipulation of an observer's context in order to achieve a desired outcome. 

"Making the enemy do our will is essential to war. An act of force is not."

"Politics is the division of power between cultural desires"  What power there is, therefore, must be divided  between a clamoring multitude of cultural desires. 

War is a mixture of two forms of strategic power: violence and  influence:

Violence is the strategic form of power used to physically deny the  enemy the power to resist our efforts to satisfy our desires. Influence is the strategic form of power used to shape the enemy’s knowledge in ways that help us satisfy our desires. Influence takes many forms: diplomacy, propaganda, subversion, commerce, agitation, intelligence, education. However, the most elemental form of influence in war is deception, the strategic form of  power used to distort enemy perceptions in ways that help us satisfy our desires. 

most of the violence it incites will be committed by third parties whose observational context is manipulated through a war of influence. 

Energy is how much power it takes to make the enemy conform to
 our political desires while visibility is how easy it is for the enemy to
 gather knowledge we don't want them to have. There's a trade-off
 between visibility and energy. The more energy that's concentrated
 into a form of strategic power, the more visible it is. The smaller the
 amount of energy that's concentrated into a form of strategic power,
 the less visible a form of strategic power is. Influence has low
 visibility and low energy while violence has high visibility and high
 energy. It’s hard to hide the energy of an atom bomb and it's hard to
 light a city with invisible ink

5GW must become a
 true war of influence. It must influence others to expend energy on
 its behalf rather than expending energy itself. Any visibility caused by
 energy expenditure is death to its political desires. To wage a 5GW
 campaign, 5GW politics must have low energy and low visibility. It
 must become invisible politics. 

 It forces us to see war as a spectrum that is
 constantly shifting between extremes of energy and visibility. It
 forces us to develop a definition of war that is broader than the
 definitions found in most contemporary general theories of war. The
 problem with using a narrower definition of war is that the enemy
 may fight you with a form of war that you not only can’t see, but,
 even worse, don’t even believe in. If the enemy uses a broader
 definition of war than you, any attack on that portion of the spectrum
 where you are defenseless may inflict a decisive defeat. The worst
 part of such a defeat may be that:
 ·      
·      
·      
·      
·      
You never knew you were at war.
 You never saw what hit you.
 You never knew there was a chance for victory.
 You never knew that you were defeated.
 You don’t believe in any of the above


 Misperception and Demoralization as Insurgent Goals

As stated above, the ultimate aim of 4GW is the demoralization of a stronger state adversary and the erosion of its moral will to continue its policy goals. To achieve demoralization, 4GW insurgents selectively use violence not to defeat state forces in pitched battles, but instead to prevent such forces from identifying insurgents and separating them from the population. In doing so, 4GW insurgents seek to provoke indiscriminate state repression that aids in the mobilization of the indigenous population.

The Sunni insurgency based in Iraq’s al-Anbar Province provides a contemporary example of 4GW insurgent strategy. Following the  downfall of Saddam Hussein’s Ba’athist regime in March 2003, American military forces occupied Iraq and took up positions in major towns and cities. In Fallujah, a conservative city in the heart of al Anbar, American forces set up a base in a local school, prompting protests from aggrieved civilians. At one such protest on April 28, American forces reported insurgents firing small arms from within a
crowd of mostly unarmed civilians. In response, American forces fired back and killed several Iraqis (Hashim 2006, 23). 

In another
example during late April, an American convoy confronted with
 protesters on Highway 10 fired into another crowd of Iraqis
 protesting their presence (Ricks 2006, 140). Because American
 forces were only trained in conventional warfare and had no
 experience operating amidst a potentially hostile indigenous
 population, troops often employed repressive violence due to the
 uncertainty presented by their surrounding social environment. The
 events of April 28-30 would serve to be the spark of the Sunni
 insurgency, and American forces regularly came under attack in
 following months. In response, American forces sought to capture or
 kill 
insurgents by surrounding suspected villages, forcefully
 searching indigenous homes, and occasionally detaining the family
 members of suspected insurgents. These “cordon-and-sweep”
 tactics and the lack of self-restraint by American forces only served
 to create greater hostility between indigenous Iraqis and American
 soldiers (Ricks 2006, 241-251). American security practices—
 breaking down doors, searching rooms occupied by women, and
 roughly handling male heads of households in front of family
 members-–confirmed Iraqis’ worst suspicions of Americans.
 Because Iraqi cultural norms stress the maintenance of one’s honor
 and the importance of revenge to redress grievances related to
 public humiliation, these repressive security practices accelerated
 insurgent mobilization.
 The demoralizing effects of the Sunni insurgency were felt both
 among American forces in Iraq and in the United States. Fighting an
 enemy that never presented itself to American forces caused
 psychological strain among many soldiers and contributed to
 instances of abuse and intimidation of the indigenous population
 (Ricks 2006, 272-276). Tactically and strategically, this led to a
 greater emphasis on force protection among American forces. Since
 they could not identify and isolate insurgents from the indigenous
 population, US troops pulled back to large forward-operating bases
 outside of major population centers. This was also supported by the
 belief that the indigenous population had a fundamental hatred of the
 American military, whose social presence created a perception of
occupation. This belief was accepted by many American
 commanders including Gen. John Abizaid of Central Command
 (West 2008). As the insurgency dragged on, many felt that there was
 no way that American forces could ever defeat the insurgency,
 despite adjustments in tactics and strategy. In the United States, the
 loss of faith in the war effort was evident as support for the Iraq War
 steadily declined and contributed to the Republican Party’s defeat in
 the 2006 midterm elections. Thus, the pursuit of American policy
 goals in the Iraq War ultimately left President Bush politically weaker
 and alienated from the American people.

Into 2006, American forces in al-Anbar began to experiment with a
 population-centric counterinsurgency strategy that capitalized on the
 excesses of insurgent control over the indigenous population. The
 most violent insurgent groups, like Al Qaeda in Iraq, often used fear
 and intimidation to coerce local Sunnis to submit to their strict
 religious rule. This included repressive actions such as extortion,
 infringement on traditional Sunni smuggling routes, and attempts to
 “marry into” the Sunni tribal hierarchy (Kilcullen 2007). Although
 some Sunni tribes attempted to resist Al Qaeda, they were ultimately
 outnumbered and defeated as Al Qaeda mobilized allied tribes
 against these dissidents.

 Because Al Qaeda created insecurity for local Iraqis, American
 forces were presented with a strategic opportunity to turn the tribes
 of Anbar against it. Beginning in 2005 and into 2006, along the towns
 and cities of the Euphrates River, United States Marines embedded
 themselves along with Iraqi security forces in major population
 centers along the Euphrates River (Malkasian 2008). Compared with
 the previous force protection strategy that sought separation
 between soldiers and the indigenous population, the new population
centric strategy was certainly more risky, as it required greater
 interaction with a potentially hostile population while employing self
restraint unless the insurgent threat could be positively identified.
This new set of security practices involved hunting down insurgents,
 yet in a culturally appropriate way. For example, American forces
 refrained from peering inside houses and conducted searches only
 with partnered Iraqi forces. Arrests were rarely made in front of
 relatives, as a means of preserving the dignity and honor of male
 family members. Mounted American patrols shared roadways with
 Iraqi drivers instead of demanding right of way (West 2008, 128).
 These 
security-providing 
practices 
demonstrated American
 recognition of Iraqi norms and values, and served to contradict
 assumptions of hostility held by ordinary Iraqis.
 By summer 2006, a population-centric counterinsurgency strategy
 was applied to the provincial capital and insurgent stronghold of
 Ramadi, where American forces had only controlled the Government
 Center that came under daily fire. The First Brigade of the US Army’s
 First Armored Division spread out across the city and performed
 daily patrols, actively conveying to the population an interest in their
 protection and security (McFarland and Smith 2008). Over time, this
 led to interdependence: while residents of Ramadi depended on
 American forces security, those same American forces depended on
 local residents for intelligence about the identity and location of the
 insurgents. As American forces came under insurgent attack and
 residents endured Al Qaeda’s repressive intimidation, both groups
 experienced a common fate and perceived a common threat from
 the most extreme insurgents. Finally, as both Americans and Iraqis
 came to fight side-by-side against Al Qaeda, they began to perceive
 themselves as being functionally similar, or homogenous. These
 events set the stage for the birth of the Awakening, or Sahwa, in
 September 2006. Under the leadership of Sheik Abdul Sattar
 Buzaigh al-Rishawi, the Awakening sought to rally the Sunni tribes of
 Anbar against Al Qaeda in cooperation with American forces, who
 paid each Awakening volunteer (sponsored by tribal leaders)
 approximately $300 for manning checkpoints and participating in the
 provision of security (West 2008, 209-212). The US-Awakening
 alliance would prove itself at the battle of Sufia, where American and
 Sunni tribal forces drove out Al Qaeda after it attacked the Albu
Soda tribe. Once the tribes observed that American forces could be
 reliable partners in creating security, they joined the Awakening en
 masse and in defiance of Al Qaeda’s intimidation (McFarland and
 Smith 2008, 49-50). By the end of the year, Al Qaeda was in decline
 and losing its hold in Anbar.
 This brief account of insurgency and counterinsurgency in Anbar is
 intended to highlight how the process of identity construction was
 central to defeating Al Qaeda in Iraq and reconciling with al-Anbar’s
 Sunni Arabs, some of whom had previously been insurgents
 themselves. During the opening phase of the war, American forces
 operated under the assumptions of conventional warfare without
 considering the local cultural context. Their attempts to cope with the
 disgruntled Sunni population backfired against them, as their lack of
 self-restraint confirmed to the civilian population the hostile identity
 altercasted upon American forces by Sunni insurgents. Thus, early
 American counterinsurgency operations completed a self-fulfilling
 prophecy of enmity that aided insurgent mobilization and created the
 perception that no strategic or tactical innovation could defeat the
 insurgency. Once American forces adopt a population-centric
 strategy and actively perform self-restraint, they break the logic of
 enmity that presumes the Other has hostile intentions by
 demonstrating an interest in cooperation. Security is provided on the
 cultural terms of the indigenous population, which can only be
 discovered by repeated interaction and communication. Once this
 first variable is activated, continued counterinsurgency operations
 activate the remaining three (interdependence, common fate, and
 homogeneity) and lead to the emergence of a shared identity
 between American forces and the indigenous population. Fully
 consistent with 5GW, the US military prevented indigenous Iraqis
 from perceiving them as an enemy, and then co-opted them in
 pursuit of American policy.


The history of the Iraq war in al-Anbar
 testifies to how identity manipulation is of central importance to the
 opposing objectives of demoralization and empowerment. The
 history of the Iraq War is not yet complete, and further research is
 needed given the recent arrest of Awakening leaders in Baghdad
 and accusations of American betrayal. However, the development of
 counterinsurgency by the American military, once confronted with
 4GW insurgency, suggests the arrival of 5GW and another turn in
 the long history of organized warfare


1GW shifted, in my mind, from the GMW description of
 the tactics of the line and column to the organization and
 concentration of strength to move toward or from key points
 on the field of battle.
·         
2GW shifted from the industrialized fire and movement
 of attrition battles in GMW to the destruction of an
 opponent’s strength in order to weaken the opponent to the
 point that resistance is impossible.
 ·         
3GW, which involved the Blitzkrieg ideal of infiltration
 to bypass strength in GMW, became the dislocation of the
 strength of the opponent by attacking and defending critical
 vulnerabilities.
 ·         
4GW, not too far from the fourth generation definition
 by Hammes, involved the operative action of disruptive
 attacks, or threat of disruptive attacks, to cause the
 perception of an unwinnable situation in an opponent,
 resulting in a loss of morale or will until the opponent was
 rendered incapacitated.
 ·         
5GW, in line with my earlier definition, involved
 manipulation, influence, and co-optation in order to define
 and shape outcomes and effects.

|||
|-|-|
|First Gradient – Cooperative Warfare – 1GW|Cooperative warfare doctrines are based upon the principle of
 creating organizations that require the individual to surrender control
 to the group in order to project Force to accomplish goals that are
 necessary to the survival of the group.|
|Second Gradient – Attrition Warfare – 2GW|The principle behind attrition warfare describes doctrines that use
 the strength of the attacker to target the strength of the opponent.
|Third Gradient – Maneuver Warfare – 3GW|Maneuver warfare doctrines are based upon the principle of
 avoiding the strength of the opponent in order to attack the critical
 vulnerability of the opponent.|
|Fourth Gradient – Moral Warfare – 4GW|Fourth gradient doctrines are based upon the principle of the
 attainment of a functional invulnerability that prevents the opponent
 from being able to orient upon a threat and creates a perception that
 saps the ability of the opponent to function effectively.
|Fifth Gradient – Contextual Warfare – 5GW|Fifth gradient doctrines are based upon the principle of the
 manipulation of the context of the observations of actors in a conflict
 or confrontation in order to affect a specific positional change or
 achieve a specific effect.|

 <br>

 The key to the xGW framework, and specifically 5GW, is the
 distinction between conflict and confrontation. As globalization
 spreads connectivity and rule-sets to less-developed parts of the
 world, states will increasingly be forced to deal with the nonkinetic
 political, social, economic, and military confrontations that have the
 potential to cross over into violent kinetic conflicts. 


 Fifth gradient war (5GW) takes this to the next level. It moves from
 violent crime to manipulation of information and identity at a level
 where the practitioners are recognized neither as soldiers or
 criminals. Instead, we are back to individuals and small groups,
 empowered by technologies and often removed from one another,
 who act to shape their environment—particularly the nonphysical
 environment—in ways that are not clear. The conflict is not to
 conquer the state, or to divide the state, but to undermine the state.
 It is not so much to rule as it is to make certain nobody else can.

 Break Loops
 Feedback loops are created when a cause effects more of itself.
 Often, feedback loops will create an S-curve jump in some variable;
 for example, the size of a population or the efficiency of computer
 processors. The positive feedback loop gives more and more,
 exhausting the implicit capacity for people or processing power of
 forest fire, and becomes a negative feedback loop that stabilizes,
 giving less and less. Sometimes positive feedback loops are not
 merely exponential, they are hyperbolic, approaching an asymptote
 where the variable approaches infinity. By definition, infinity can't be
 reached, so what happens in practice is that the feedback loop
 breaks the system it originated from.
 One example of a break loop happened in finance in 2008. The
 global financial system had become caught up in a practice called
 "regulatory arbitrage." Banks are required to hold some fraction of
 their deposits as being available at all times; they can only make so
 many loans. A meeting of bank regulators in Basel, Switzerland,
 changed that. Banks were suddenly able to sell loans as securities,
 freeing up reserves for yet more loans. A huge boom in securities for
 all manner of debt exploded in the early years of the twentieth
 century, with electronic networks enabling efficient transactions in
 unregulated contracts. The credit default swap contract become the
 de rigueur instrument for banks to insure these securities against
 loss, which made buyers of those securities feel so secure that they
 borrowed new money into existence to buy tons of debt securities.
 One adjustment to the rules led to a hyperbolic expansion of money
supply and accompanying debt, leading to the rapid deterioration of
 the global financial system's integrity when infinity dollars failed to be
 created, despite the Federal Reserve's best attempts. The Basel
 regulations could be considered part of an effective 5GW strategy to
 create a financial crisis, demanding a cure in the form of a global
 currency. The strategy involves instigating a model-breaking
 feedback loop, and then wielding it as a weapon

  The onset of each generation of war was instigated by a break
 loop that rendered all established models useless. The proliferation
 of global communication networks was one such loop; it could be
 argued that fifth-generation warfare has attained some degree of
 self-awareness, as far as wars go, in that it hinges on setting in
 motion feedback loops that destroy the context of an opponents'
 strength. There is another level above effecting break loops:
 effecting a feedback loop in self-modifying organization that
 precludes all organizations that could effect break loops. Such an
 organization could be software that can rewrite its own code, an
 organic brain utilizing computer networks via electrodes, a chemical
 substance operated on by nanomachines and computing in 3D, a
 collective of computers being used over the Web by a mediating AI,
 or a human equipped with an invasive brain-computer interface.
 Once this feedback loop of intelligence escalates past an inflection
 point, it could have enough capacity to pre-calculate all 5GW
 machinations, so that any movement toward initiating a break loop
 would be easily countered, ignored, or assimilated into a larger
 design. Any attempts to wage 5GW would be doomed by default
 without the perpetrators having any concept of being pawns in the
 game of a profoundly more vast intelligence


|||
|-|-|
| 0GW | struggles represent survival overcoming destruction| 
|1GW |represents emotional resolve overcoming self preservation|
|2GW| represents logistics supporting emotional attrition|
|3GW| represents socioeconomic capacities being applied through clever maneuvers|
|4GW| represents a will-to-power rallying the moral support of a civilization to erode the established opponent's base of maneuvers|
|5GW| represents the manipulation of guerilla pressures to effect an occult feedback loop and alter the context of society|
|6GW| involves the systematic knowledge tapped by 5GW, but is leveraged by the capacity of a mind to self-modify|

<br>

If 5GW is contextual warfare then 6GW is design warfare, a contest of systemic resilience



States will either successfully adapt
 or they will fail. Many will fail, lacking sufficient political resilience to
 weather protracted civil conflict or an economic base from which to
 wage it. Of those that manage to adapt, they will be most likely to do
 so by either:
a)              
Adjusting their response to complex
 decentralized insurgencies down to a very granular level of
 society with intelligence, COIN, information operations and
 economic development, states in essence becoming more
 complex themselves, or:
 b)              
Savagely ratcheting back the systemic level of
 complexity by a sustained application of extreme violence
 to disrupt the social fabric and simplify it by atomizing social
 networks deemed to be enemies of the state.
 The first option involves counterinsurgency warfare and skillfully
 selective political and economic concessions by the state to separate
 the people from insurgents and to strengthen the legitimacy of the
 state in their eyes by displaying competence in providing physical
 security, desired public goods, civic engagement, and appropriate
 reactions to insurgency attacks. This is a sophisticated and
 exceptionally difficult policy to carry out and requires governmental
 elites to consider long-term national interest over their own
 immediate interests. This usually proves to be the sticking point.


 The French lost in Indochina and Algeria politically, due to deeply
 exploitative and punitive colonial regimes that they could not bring
 themselves to reform, long before they lost on the battlefield. The
 United States, in turn, never succeeded in convincing the Saigon
 governments to reduce corruption or to enact meaningful reforms
 that might appeal to South Vietnam’s rural peasantry, even when the
 regime was facing collapse. This contrasts with the more positive
 COIN experiences of the Malayan emergency, El Salvador, and,
 most recently, Iraq, where a more nuanced and concessionary
 approach coupled with more precise uses of force, enlisted the
 population as allies (or as armed paramilitaries) against the
 insurgency. Even El Salvador, where COIN was far more “kinetic”
 than today, involved major political concessions by the “Forty
 Families” oligarchy in establishing genuine democratic government.
 Unfortunately, because of the difficulty of finding or persuading
 sufficiently enlightened elites to reform in their own self interest, and
the challenges of navigating old-fashioned Maoist insurgency (to say
 nothing of today’s 4GW environments), most efforts at prosecuting
 COIN warfare have failed (Richards 2008). We can expect that in the
 future, while some will succeed brilliantly, many states will likewise
 fail—especially those without a great power patron, like the recent
 case of the deposed Royal government of Nepal. These kinds of
 states—unpopular, authoritarian, relatively backward, corrupt, and
 isolated—are exceedingly poor candidates for bootstrapping a COIN
 strategy on their own. Or even with considerable outside help.

Stalin 

The Stalinist Soviet Union has, since the publication of Conquest’s
 The Great Terror: A Reassessment (1991), been one of the major
 examples of democide that is comparable to the great ethnoracial
sectarian genocides of European Jewry by the Nazis or of the
 Armenians by the Ottoman Turks. What is less well understood
 about Stalin’s crimes is that the apparently random terror, with
 quotas for arrests issued to branches of the secret police in every
 Soviet oblast, that swept up millions of Soviet citizens in the 1930’s,
 contained a far more targeted campaign against specific and readily
 identifiable networks that Stalin considered especially problematic
 potential enemies.

 A frequent Stalinist purge technique was to liquidate not only the
 holder of an important post in an organization, but his immediate
 replacement as well (and not infrequently, the replacement’s
 replacement), thus not only atomizing existing social networks, but
 terminating institutional memory in the bargain as the documentary
 records were purged with the same severity as the staff. This
 permitted a complete reshaping of organizations in any fashion the
 dictator desired as Stalin could be sure the ”new blood” was
 completely loyal to him and untainted by previous “enemies.” Soviet
 society had been so thoroughly terrorized by the end of the
 Yezhovschina that no effective opposition of any kind existed to
 Stalin’s will. Neither the Soviet government nor the Communist Party
 nor the general staff of the Red Army retained any independent
 functionality after 1938, and after 1948 the politburo itself fell into
 gradual disuse under Stalin’s paranoid eye as he arrested the wives
 and families of his closest collaborators.

 Cambodia
 Despite being a secretive, almost cultlike, ultra-Maoist movement,
 Cambodia’s Khmer Rouge leadership aped Stalin’s bureaucratic
 totalitarian regime in conducting a two-tiered auto-genocide
 designed to exterminate specific networks even as it is
 deconstructed Cambodian society as a whole. Submerged within the
 most radical and terrifying democidal expression of Marxist-Leninism
 in history was a sinister racial and religious subtext that would have
 warmed the heart of Heinrich Himmler; and like Stalin’s Great Terror,
Pol Pot’s “Year Zero” left Cambodian society completely prostrate
 and incapable of even conceiving of resistance. “They treated us like
 dogs; we dared not protest,” recalled one ethnic Chinese Cambodian
 peasant who was doubly suspect for having converted to Protestant
 Christianity (Biernan The Pol Pot Regime). As Khmer Rouge cadres
 would say, “To keep you is no gain; to kill you is no loss” (p. 294).

  The Khmer Rouge ideologically idealized a peasant Communist
 utopia and followed revolutionary tradition in targeting “bourgeoisie,”
 a category the Khmer Rouge radically expanded to embrace all
 urban dwellers or with education, famously killing those who wore
 eyeglasses on the presumption that they could read. Like other
 Marxists, the Khmer Rouge sought an atheistic state and targeted
 the Buddhist clergy for liquidation along with those Cambodians who
 had been contaminated by converting to foreign religions like Islam
 or Christianity. But the Khmer Rouge leadership also had deep
 pseudoracialist antipathy for Muslim Chams, ethnic Vietnamese and
 ethnic Chinese, all of whom as non-Khmers were slated for
 destruction (though to appease Beijing’s sensibilities, ethnic Chinese
 were always classed as “bourgeois” and not, specifically, killed for
 their ethnicity, unlike the Vietnamese minority).
 As with Stalin’s purges of the CPSU, Polish and Ukranian
 Communist Parties, the Khmer Rouge achieved a chilling
 thoroughness in their elimination of leadership networks in
 “traitorous” or “enemy” groups. Of the Islamic leaders in Cambodia
 categorized as “community leaders,” “deputies,” “Haji,” and
 “teachers,” the death toll was approximately 90%. The primary
 political vehicle of the Chams, the Islamic Central Organization, was
 killed off to almost the last man (Biernan at. 271). Islam and the
 Cham language were banned.
 An innovation in genocide, if it can be called that, instituted by the
 Khmer Rouge and later perfected by the Interahamwe militias of
 Rwanda, was the devolution from elite to a granular social level of
 state-sanctioned mass murder. Unlike the Nazi Gestapo and special
 Totenkopf SS division that ran Hitler’s death camps, or Stalin’s
 NKVD, which executed political prisoners in secret or in faraway
gulags, Pol Pot ordered that village officials, ordinary soldiers,
 peasants, or even children be enlisted to execute enemies, hacking
 them to death with farm implements in order to save bullets. One
 former Khmer Rouge official confessed to personally killing 5,000
 people by wielding a pickaxe (Power 2002).
 This downward dissemination of responsibility for genocide
 created situations where victims were frequently compelled to
 become perpetrators, demonstrating their “loyalty” by slaughtering
 neighbors, friends, spouses, parents, or children. These survivors
 under the Khmer Rouge regime were left with their social relations
 atomized, unable to reconstruct new social networks as forming
 bonds of trust was impossible so long as the rule of Pol Pot endured.


 Rwanda
 The most “granular” genocide in history occurred in Rwanda in
 1994, where between 800,000 and 1,000,000 Tutsis and “moderate”
 Hutus were systematically murdered over the course of just 100
 days by radical Hutu mobs mobilized and directed by Interahamwe
 and Impuzamugambi militiamen and the Rwandan government,
 possibly abetted by French military intelligence officers (France
 accused in Rwanda Genocide 2008). One of the most publicized
 genocides in recorded history, the Rwandan genocide is notable for
 the recruitment of enormous numbers of participants, where every
 Hutu citizen was expected to play the role of an enthusiastic SS
 man, and for the failure of the genocide to affect the military
 capabilities of the Tutsi rebel Rwandan Patriotic Front, which
 ultimately overthrew the Hutu government in Kigali.

As with the genocide of Communist regimes, the radical Hutu state
 was targeting latent social networks of potential opposition in trying
 to destroy the Tutsi population, but unlike Stalin or Pol Pot, Hutu
 generals also faced an active military opponent in the Rwandan
 Patriotic Front (RPF) with which they were locked in a civil war:

 The genocide failed to stabilize the radical Hutu government and,
 instead, led directly to its overthrow by its Tutsi rebel enemies. The
 RPF rebels were based in Uganda and, unlike most insurgents, their
 military effectiveness in the field was not impaired by the Hutu
 destruction of their civilian Tutsi “base.” By contrast, Rwandan
 society and governmental machinery were severely disrupted by the
 genocide, both by the loss of Tutsi personnel throughout the private
 and public sector and by the mobilization of the Hutu population and
 prioritization of genocidal killings over their normal activities. The
 regime was less able to field effective military resistance to the RPF
during the genocide than had been the case prior to the Arsuha
 Accords, and it collapsed in July of 1994.


 Analysis
 These historical case studies point not only to the persistence of
 genocide as a historical tragedy, but to its perceived utility as a tool
 of statecraft by regimes of a paranoid character that consider
 themselves surrounded by enemies, real or imagined. The siege
 mentality that is an inherent characteristic of governmental elites in
 states like Burma, Algeria, North Korea, Zimbabwe, and Sudan are
 like gasoline waiting to be ignited by the spark of 4GW into a
 monstrous overreaction.
 4GW entities like Hezbollah or complex decentralized insurgencies
 seen in Iraq or the narco-insurgency raging in Mexico, operate at
 what strategist John Boyd referred to as the mental and moral levels
 of war, seeking to erode the legitimacy of the state and win over the
 primary loyalty of the population, or a segment of it, to itself. It would
 be hard to conceive of a more antagonizing type of opponent for a
 paranoid, statist, elite than a 4GW group whose existence and
 successes tend to inflame the worst kind of conspiracy theorizing.
 For elites of this kind, a democidal response to the challenge or the
 potential of 4GW conflict offers pragmatic and psychological
 benefits.
 The pragmatic benefit is that genocide is often, though not always,
 effective at eliminating the capacity of a targeted population to resist
 while terrorizing observers within the society into passivity or even
 active complicity with the regime. Algeria in the 1990s, Iraq in the
 1980s, Guatemala in the 1970s, and Indonesia in the 1960s all
 successfully used “death squads” on a massive scale and in
 conjunction with regular military and security forces to brutally put
 down Islamist terrorists, Communist guerillas or restive minority
 populations. Nor does genocide require the sophisticated and
 expensive state security apparatus fielded by the Nazis to carry out.
 As Rwanda and Cambodia demonstrated, political mobilization and
 recruitment of a “perpetrator population” is enough; Rwandan Hutu
militiamen actually murdered more efficiently with their machetes
 than the SS did with Auschwitz.
 Psychologically, a regime that opts for so extreme a policy as
 genocide to crush an insurgency is akin to Cortez burning his boats
 before assaulting the Aztec empire. The state backs itself into a
 moral corner where the only sure path for safety for its high-level
 apparatchiks is to prevail and retain power indefinitely. The bonds
 between members of the regime are tightened by mutual guilt and a
 common enemy (or perhaps the enmity of the whole civilized world)
 and frequently, an increasingly distorted worldview as the need to
 rationalize or minimize the genocide as justifiable becomes a critical
 imperative when the genocide is “discovered” by other states


 Case STudy  

 I am going to draw heavily on Columbia University
 sociology professor Sudhir Venkatesh’s three volumes on life in
 inner-city Chicago to describe a 5GW counterinsurgency operation
 that was conducted by the Chicago Housing Authority (CHA), the
 City of Chicago, and the Federal government against a second
generation gang known as the Black Kings (BKs), who operated out
 of the Robert Taylor public housing project in the late 1980s and
 early 1990s. I am going to argue that the CHA et al. adopted a 5GW
 strategy because the BKs had become so embedded within the
 community that it was necessary to change the whole community—
 or, to put it another way, shape the battlespace—in order to defeat
 the BKs


 In 
a 
4GW, caring is important. In a battle of ideas
 (capitalism/communism, Jihadism/Liberal Democracy) the fighter—
 especially the insurgent—is generally a passionate advocate for their
 position. A 5GW is, like a 4GW, typically an insurgency that pits a
 smaller force against a much stronger opponent, but unlike 4GW
 there is no ideology involved. 5GW fighters don’t care about ideology
 —and they hope their opponent does not care that there is a battle
 going on:
Every other form of modern-warfare requires people to care. The aggressor
 needs to be able to morally and physically support his military forces for over a
 period of time—often a long time. The defender, once he realizes he is being
 attacked, will care about his own survival and fight back.(Abbott, Dreaming 5th
 generation war 2009)
 In many ways getting an enemy to not care is the essence of what
 happened in the Robert Taylor between the mid 1980s and mid
 1990s as the BKs rose to prominence. The gangs needed the City of
 Chicago to not care that they were operating. This was no Maoist
 insurgency; the BKs were not really looking for converts or
 comrades; they just needed enough space to operate freely. Much of
 what the BKs did, from paying off local elites to tamping down
 violence at the behest of the police, was designed to make potential
 troublemakers not care just enough to decide that taking on the gang
 was more trouble than going along. On the flip side, anyone inside
 any level of government that really wanted to fight the gangs was
 fighting a battle to get someone to care: get the FBI to care about the
 racketeering, get the City to care about the conditions inside the
 projects, and get the police to care more about a strong rule of law
 than a hassle-free peace. And once this was accomplished, once the
 government started caring enough to dump resources into solving
 the problem, the war was won.
 Once the authorities cared, they set off a series of developments
 that substantially weakened the Black Kings. To explain how that
 happened, I shall steal another concept from Abbott’s (Dreaming 5th
 generation war 2009) post on 5GW: waterfall development. In a
 waterfall development model:

  In the 5GW I am describing, the insurgents, like all insurgents,
 draw strength from their environment. Not unlike the way the Viet
 Cong hid in the jungle and used the natural landscape of Vietnam as
 a weapon against American soldiers and marines, the Black Kings
 used their immense store of local knowledge and ability to blend into
 the environment of Robert Taylor as their primary defense. So the
 CHA et al. defeated them by launching a 5GW against the
 environment itself. In their grand strategy to destroy the gangs of
 Chicago, the government turned construction workers, real estate
 developers, and nonprofit organizations into unknowing soldiers in a
 massive counterinsurgency campaign. And when they were finished,
 the insurgents found the environment had been so radically altered
 that they were unable to reorient themselves, and many wound up
 walking away from insurgency all together

The Robert Taylor housing project was built with the best of
 intentions. When they opened in 1962, the 4,500 apartments in
 Robert Taylor Homes were to be a mixed-income public housing
 project that would serve as a kind of stepping-stone between poverty
 and entry into the middle class for the primarily black, low-income
residents on Chicago’s South Side (Venkatesh, American project
 2002). Venkatesh describes Robert Taylor as a large-scale “social
 engineering project” (p. 15) because every aspect of Robert Taylor
 seemed somewhat experimental: the high-rise design with copious
 amounts of open spaces was all the rage in Europe (p. 16); the CHA
 would purposefully place poor and working class families side by
 side, to reduce the “isolation” felt by the poor (p. 20); and the CHA
 would also maintain social and educational services nearby, to help
 the residents find a way out of poverty (p. 23). Ultimately, Robert
 Taylor was envisioned as a sort of mini-city unto itself, designed
 especially for those who the authorities felt did not quite fit in the rest
 of Chicago.
 The rise of the Local Advisory Council
 The grand visions of the CHA had one major weakness: in order
 for the experiment to work a lot of things had to go right, a number of
 city bureaucracies had to learn how to work together, and budgets
 would have to be maintained at levels which fully funded the
 educational and social programs or the goal of helping the tenants
 move up and out from public housing would be difficult to realize,
 especially crucial in the late 1960s as opportunities in manufacturing
 disappeared (p. 45). Also, law enforcement would have to be
 committed to policing the local area and the private sector would
 have to be willing to invest in the kinds of businesses any thriving
 community needs, including stores and places of employment.
 Robert Taylor began accepting residents in the mid-1960s and it did
 not take long before the various forces that would have to align for
 the project to work began to show signs of stress.

 Very quickly the formal institutions that had official jurisdiction over
 Robert Taylor began to demonstrate that they were not ready for the
 challenges at hand. As early as 1965, residents began complaining
 that it was hard to get police protection in Robert Taylor. The CHA
 was also increasingly and conspicuously absent from Robert Taylor
 as the physical infrastructure quickly deteriorated to the point where
 several children died in accidents as a result of a faulty fence (p. 49).
The failings of local law enforcement and the CHA led residents of
 Robert Taylor to begin look to closer to home, to less formal
 solutions and organizations, to deal with their problems.
 One local solution that would become part of the landscape of
 Robert Taylor was the Local Advisory Council. The LAC (p. 60) was
 an elected body made up of and voted on by the residents of each
 building that would work with the CHA to address tenant issues. The
 LAC became very important both because it empowered local
 residents, to an extent, and also because the LAC members became
 the first, but not the last, local organization who would use extortion
 to make money off the growing underground economy in Robert
 Taylor (p. 90). LAC members would often take bribes to look the
 other way while people ran various scams and businesses out of
 their apartments, including bars and brothels

  The Black Kings take over
 By the early 1980s the CHA was dubbed the worst public housing
 authority in America (p. 112), and there was plenty of evidence to
 support that assertion. Although part of the CHA’s professed goal
 had been to help tenants find legitimate work and move out of public
 housing, after 2 years in operation 90 percent of the residents the
 CHA was responsible for were unemployed (p. 115). Murder rates in
 and around Robert Taylor were about 100 out of 100,000 (Levvit and
 Venkatesh 2001), which was approximately ten times the national
 average. To make matters worse, the buildings maintained by CHA
 were falling apart and overcrowded; the CHA was a billion dollars
 shy of the funds that would have been needed to bring the buildings
 up to code and they had twenty four thousand people on the waiting
 list to even get an apartment (Venkatesh, American project 2002).
 From these statistics we can get picture of an enclave of inner-city
 Chicago that was poor, violent, badly neglected by the government
 agencies charged with its care, and broadly disconnected from the
 larger American society.
 Although the Black Kings had existed since the 1970s, it was not
 until the arrival of crack cocaine and the potential for large profits
that the BKs developed as a serious organization, led by college
educated adults and operated for the express purpose of generating
 revenue. The BKs were so organized that when University of
 Chicago economics professor Steven Levitt first got a look at some
 of Venkatesh’s original notes from his field research, it struck him
 that the organization chart for a crack gang looked a lot like the
 organization chart for the McDonald’s fast food chain (Dubner and
 Levitt 2005).
 In the early 1990s the BKs effectively “took over” several buildings
 in Robert Taylor. It was during this time period that the BKs began to
 become a local institution; the local community center established a
 “community court” where gang members would listen to residents’
 complaints; the BKs made a sometimes-tempestuous peace with the
 LAC and started doing “community service” around Robert Taylor
 designed to persuade the residents that the gang’s crack business
 was actually a benefit to the community. (Among the more
 interesting community projects, an LAC leader had the gang take
 pictures of the crumbling infrastructure around Robert Taylor.)

 Understanding the way money flowed from the population (through
 drug sales and extortion) up to key members of the BKs is important
 to understanding how the Black Kings operated, because some
 amount of that money was “recycled” by the leader of the BKs back
 into the hands of key community leaders who played a role in
 helping the BKs maintain control over their drug territory. For
 example, after 1990 the BKs had established a rule that only gang
 officers should pay bribes to the LAC (Venkatesh, American project
 2002), who in turn helped keep the BKs abreast of goings-on in
 Robert Taylor and helped gang members keep a lookout for police.
 Another example was an NGO called No More Wars, which was
 ostensibly interested in helping rival gangs solve their problems
 without resorting to violence but was also funded largely with gang
 money and was believed by many residents to be interested in
 keeping violence to a minimum only because violence was bad for
 the drug trade. With key community members, including officially
 recognized building authorities like the LAC on their payroll, the BKs
were able to keep the potential friction they faced from the local
 population to a minimum even though many residents were less than
 enthusiastic about the BKs’ activities 

 he Chicago police were less than enthusiastic about enforcing
 the law within the Robert Taylor homes. In the 1970s the police had
 given up regular patrols in favor of coordinating police activity with
 the LAC, both because they believed the LAC was viewed more
 favorably by local residents and because the physical layout of the
 high-rise buildings was ready-made for suspects to hide from or
 perhaps even ambush approaching officers. There were reported
 incidents of mass violence against police entering the buildings in
 the early 1970s. In the late 1980s the Chicago Police and CHA
 attempted to reintroduce a police presence in the project but
 probably made matters worse by utilizing “sweep and clear”
 operations, in which officers would enter buildings fast and hard,
 kicking in doors, searching apartments on the least suspicion, and
 taking large numbers of residents suspected of having some link to
 illegal activity downtown for questioning. This technique occasionally
 netted drugs or gang members, but also served to further isolate the
 local residents from law enforcement and push them closer to
 accepting the authority of the BKs. By the early 1990s, Venkatesh
 was told by residents that, whatever misgivings the residents had
 about the BKs, many would still rather call the Kings than call the
 police (Venkatesh 2008).
 It helps to think of the power relationships within the area as a
 series of 4 flows, analogous to the 4 flows of globalization (Barnett,
 The Pentagon's new map: War and peace in the 21st century 2004).
 There is a flow of legitimacy, meaning the people who are
 recognized by both the residents and by city officials as community
 leaders; this often included members of LAC (because they were
 duly elected to represent the residents) and officials from the local
 church and community center. The BKs harnessed the goodwill that
 these local institutions had by paying off and providing services for
 the leaders. The LAC, community center and church, in turn, by
accepting the BKs’ money and using them to provide services
 essentially—for lack of a better term—blessed the BKs and made
 them a legitimate local institution.

 In Robert Taylor wealth might
 come from lawful economic activity (i.e., having a job in the regular
 economy), but was more likely to come from “hustling” (which could
 include prostitution, drug dealing, gambling, or running an unlicensed
 small business such as an auto-repair business) or wealth transfers,
 which include welfare from federal, state and city governments. The
 BKs managed to capture the flow of wealth both by “taxing” (aka
 running a protection rackets on) local hustlers and also by selling
 drugs to the residents. The BKs would then transfer part of that
 wealth to the local elites (such as donations to the local church or the
 aforementioned Grace Center (Venkatesh 2008)). Besides buying
 the allegiance of local leaders, the BKs were also empowering LAC
 officials to provide more services to the local residents, which
 improved the standing of the LAC but also made their power position
 increasingly dependent upon the goodwill of the BKs and the
 success of the BKs in various illicit business ventures.
 The flow of security is the local monopoly of violence. In most
 American communities the uniformed police fill this role, but in
 Robert Taylor it was often filled by the BKs. By not pushing for a
 greater presence, and by essentially acquiescing to the BKs’ control
 of Robert Taylor, the Chicago Police ceded their role as local
 Leviathan and that vacuum was quickly filled by the BKs. As an
 example of the police ceding their authority, Venkatesh reports
 witnessing a meeting where police and the leader of the local
 community center met with two rival gang leaders and negotiated
 cessation of hostilities in a gang war in exchange for one gang
 gaining exclusive rights to sell crack in the local park (Venkatesh,
 Gang leader for a day: A rogue sociologist takes to the streets 2008).
 The problem, of course, was that the same monopoly of violence
 that the BKs used to beat up those accused of domestic violence
 and warn away strangers could also be utilized to threaten any
 resident who refused to play by the BKs’ rules.
Finally, we see a flow of social services. In most communities the
 government, often with the help of local charities, provides social
 services. The government sent a fair amount of welfare to Robert
 Taylor (recall that about 90 percent were on public assistance), but
 many local services were performed by the LAC with gang money;
 among them were local basketball leagues, car services to take
 elderly people on errands, and a program to buy school supplies for
 local kids. Occasionally the BKs even helped clean up the apartment
 lobby.
 This is the tangled web the BKs wove which allowed them the
 space to operate with impunity. By the early 1990s, with the LAC
 (and by extension the preferred contact between the police and local
 residents) in their pocket and with residents increasingly recognizing
 the BKs as the local security service provider, the Kings had created
 a TAZ—Temporary Autonomous Zone—an area of the city that they
 effectively controlled, and they were able to leverage that control to
 extract greater and greater amounts of wealth out of the local
 population

 As murder rates spiraled upward, long
 mandatory prison sentences became the norm and police
 departments across the country were given the money they needed
 to add officers. To put it another way, the BKs were losing the battle
 of who could care less; the authorities were starting to care.
 Federal law enforcement cared enough to bring RICO
 prosecutions against the leadership of all Chicago area gangs and
 the distributors they used to bring drugs in from Mexico. While being
 prosecuted for possession or trafficking had always been a risk of
 gang life, federal prosecutors were now applying a full court press,
taking down the leaders of Chicago area gangs and prosecuting
 them for being involved in organized crime. With each successive
 grade of war kinetics becomes more and more focused; as the 5GW
 campaign against the BKs kicked off the kinetics were focused on a
 list of high-value individuals who had been key in the BKs’ C2
 structure.
 The prosecutions were affecting life in Robert Taylor in at least
 three ways. First, and most obviously, taking gang leaders off the
 street prevented them from making deals and handing out bribes,
 although it must be noted that most gang leaders were able to exert
 some influence from prison, but they would never have the day-to
day ability to get a “feel” for the ever-changing situation on the street
 that they might have when they were free. Second, Chicago police,
 who had been fairly amiable to gang control of the area for many
 years, had to become much more careful about even the slightest
 hint that they allowed gang activity to proceed unimpeded in their
 jurisdiction. Finally, the prosecutions were making gang members
 more and more paranoid about other members cutting deals to
 become witnesses in exchange for leniency in the face of
 prosecution. Upper-level gang members suspected their underlings
 and the underlings assumed that, when push came to shove, the
 higher-ups on the board of directors would cut deals at the expense
 of their junior officers. As fear began to grip the BKs their level of
 cohesion decreased; key members started looking for a way out of
 the gang and recruitment was being hindered by another phase of
 the 5GW war being waged against the BKs

 The second front in the 5GW involved a federal project known as
 HOPE VI, which sought to replace all high-rise public housing
 projects in the US with mixed-income single-family and multifamily
 structures. This is where the concept of waterfall development
 comes into play. Remaking Robert Taylor was no easy task and
 involved agencies at the local, state and national levels as well as
 private sector firms and NGOs (Levy and Gallagher 2007) working
 with at least half a billion dollars in funding (Micheals Development
 Company 2003). All of these resources, as well as the political will to
use them, had to be in place before Robert Taylor could be torn
 down, but once those resources were put in place and the process
 was begun it would move forward with its own inertia, because by
 that time there were simply too many interested parties to stop the
 process

 In each previous generation of war, fighters had to care. Many of
 the people who fought the war against the Black Kings probably
 never even heard of the local gang. The private sector investors
 really did not care about the BKs; they were interested in prime real
 estate. The construction workers were interested in fulfilling their
 duties to operate machinery, pour concrete, and collect a paycheck
 on Friday; they had no idea they were fighting a COIN campaign with
 each old structure they destroyed.

 As the HOPE VI construction got under way the BKs began to feel
 the pressure of losing their resources. Residents were being
 relocated to other projects or given vouchers to move into private
 apartments. Efforts to spread the gang were met with little success
 (Venkatesh, Gang leader for a day: A rogue sociologist takes to the
 streets 2008), partially because Chicago had decided to attack all
 the projects in the city with the HOPE VI program simultaneously
 (and the Feds were going after all the gangs at the same time as
 well), and partially because the BKs’ success in Robert Taylor had
 been heavily correlated with the unique conditions within the
 projects; a large number of people living outside the law (easy prey
for extortion), a large number of customers (drug users) living in
 close proximity, and a large potential labor pool (all the unemployed
 young men), when combined with a city that generally did not care
 what happened inside Robert Taylor, had allowed the BKs to flourish
 for several years. To make matters worse, as the residents were
 moved out and construction workers moved in, the Chicago police
 began increasing their patrols to protect the workers.

The reduction in power for the BKs was also reducing the power of
 other local elites. The LAC, which had been working hand-in-hand
 with the BKs, was losing their power both because the money was
 drying up and because the residents were forced to rely directly
 upon the CHA relocation assistance; as much power as the LAC had
 developed within the projects they were relatively powerless outside
 Robert Taylor. All of these changes worked to isolate the BKs from
 all the flows they had previously employed to stay in power.

  The BKs were destroyed in two distinct phases. First, the big bang
 of the federal indictment, which can be thought of as extremely
 focused kinetics, disrupted the OODA of the BKs’ leadership. Then
 the second phase sought to remake the BKs’ environment right out
 from under them by kicking off a waterfall development remaking of
 the battlespace the BKs had learned not only to operate in, but to
 dominate. In the end, the 5GW campaign ended with the area of
 Chicago that had been Robert Taylor—now renamed Legends South
 —gang-free. And J.T., who had been a key leader of the BKs and at
 his height had commanded hundreds of foot soldiers and dozens of
 officers, was reduced to working in the dry cleaning business. (The
 once powerful gang leader also attempted a career as a barber.
 Luckily, he had enough money left over from his dealing that he did
 not have to work.)

  The BKs were destroyed in two distinct phases. First, the big bang
 of the federal indictment, which can be thought of as extremely
 focused kinetics, disrupted the OODA of the BKs’ leadership. Then
 the second phase sought to remake the BKs’ environment right out
 from under them by kicking off a waterfall development remaking of
 the battlespace the BKs had learned not only to operate in, but to
 dominate. In the end, the 5GW campaign ended with the area of
 Chicago that had been Robert Taylor—now renamed Legends South
 —gang-free. And J.T., who had been a key leader of the BKs and at
 his height had commanded hundreds of foot soldiers and dozens of
 officers, was reduced to working in the dry cleaning business. (The
 once powerful gang leader also attempted a career as a barber.
 Luckily, he had enough money left over from his dealing that he did
 not have to work.)

 Conclusion
 Historian Mark Safranski has pointed out that genocide could be a
 form of 5GW used to put down a 4GW insurgency that was
 supported by an ethnic minority (5GW: Into the heart of darkness
 2009). My example shows almost the exact same thing occurring,
but with a much lower level of physical violence. If the Robert Taylor
 Homes are viewed objectively, one can see a unique culture with its
 own norms, values and institutions. Residents, feeling underserved
 by America’s procedural justice system, devised their own honor
 based justice system that included lists of crimes and punishments
 up to and including a death penalty for severe infractions. They
 developed their own economic system that mixed entrepreneurial
 capitalism with a barter system. And they developed their own
 security forces whose activities undermined the authority of the
 political elites.
 When the government forces decided to destroy the BKs, they
 recognized that they had to destroy the entire culture within which
 the BKs operated. Just as in Safranski’s example the Rwandan
 government inspired the Hutu majority to take up arms against the
 Tutsi minority in 1994, the government in Chicago wanted to
 empower a decentralized coalition of actors, including investors,
 NGOs, and real estate developers to act against the culture that
 supported the BKs. In America, those actors would be armed not
 with machetes but with zoning permits, but the result was similar: the
 removal of a local population and the destruction of a culture that
 had supported an insurgency

 What my analogy is
 designed to illustrate are the essential elements of 5GW that could
 be observed in both my and Mark Safranski’s examples: a central
 authority empowering groups of actors to work ostensibly for
 individual gain while also affecting an outcome desired by the central
 authority, and also a focus on getting those actors to affect the
 human terrain of a given geographic area with the goal of robbing an
 insurgency of its power base.

Next examle 

 The nearly 20-year-old conflict in Somalia is the perfect example
 of 5GW in Africa. Persistent political and humanitarian crises, and a
 disastrous early US intervention, gave rise to seething and spreading
 anti-Americanism, escalating economic warfare by way of sea piracy
 and a campaign of secretive US intervention whose benefits, and
 costs, are unclear. The conditions were ripe for exploitation by a
 subtle actor aiming to overturn US designs for Somalia.
 Washington fought to keep Islamists out of Somalia, in the interest
 of preventing terrorists from taking root in the country. But the
 Islamists hijacked muddled US efforts and strengthened their cause.
 After years of fighting that left hundreds of thousands dead, in
 February 2009 Islamists took advantage of the escalating chaos, and
 growing frustration in Washington, to reassert control of the country,
 essentially inflicting an indirect battlefield defeat on America

  In 2009, thousands of Somali pirates employed by sophisticated
 criminal enterprises threaten some two million square miles of
 ocean, including all of the Gulf of Aden and vast swaths of the Indian
 Ocean. In 2008 pirates seized more than 40 large vessels headed to
 and from the Suez Canal, ransoming them for an average price of
 more than $1 million.
 With access to GPS, satellite phones, and commercial satellite
 imagery—not to mention small arms and rockets readily available
 across Africa—pirates have managed to capture large cargo ships
 and even supertankers. Pirates' surprising success has had the
 effect of driving up insurance rates for shippers and forcing some
 companies to abandon the Suez Canal route between Europe and
 Asia, in turn raising consumer prices at a time when most consumers
 have less to spend.

Around 20 warships from a dozen navies have deployed to
 combat piracy, but they can only hope to mitigate the threat. "I don't
 think we'll ever stop pirates," said U.S. Navy Rear Admiral James
 McKnight. "We will do our best to bring the numbers down" (Federal
 News Service 2009).
 That's because ending piracy requires law and order and a
 measure of prosperity on land, at the source of the problem.
 Fundamentally, Somali pirates are aggrieved fishermen whose
 livelihoods suffered from Somalia's collapse in 1991, McKnight
 explained. In the absence of any Somali authority, "for very many
 years...countries were coming in and fishing in their international
 waters, stealing their fish. And so what they did is they started
 pirating some of these fishing vessels and they figured out that, hey,
 we can go for bigger fish. And so they went for bigger vessels."
 Piracy "is beyond a military solution," said Roger Middleton, from
 the International Institute of Strategic Studies in London. Pirates'
 continued success demonstrates the power of the individual and the
 impotence of old-fashioned military force in this age of powerful,
 accessible consumer technology. A few thousand plugged-in pirates
 have rendered ineffective the combined might of the world's navies,
 and undermined the notion that the traditional state can protect its
 interests with displays of force.
 That's not to say they did it on purpose. "We just want the money,"
 Sugule Ali, a pirate spokesman, told the New York Times. Indeed,
 pirates have no clear or stated political aims. Their intent is only to
 get rich, but their effect is to rattle the very foundation of the state. In
 this way, pirates are a 5G threat. Somalia's Islamists are the subtle
 third-party actors benefiting from the havoc pirates wreak.
 For Islamists seeking to rewrite the global order, one country at a
 time, piracy might represent a form of economic warfare that, in
 World Wars I and II, was executed by submarines targeting
 merchant ships. Piracy has a similar effect, albeit less severe, and at
 much lower cost and risk to both the attackers (the pirates) and
 those hoping to benefit from the attacks (the subtle actor)


When civil war toppled dictator Siad Barre's regime in 1991, clans
 began fighting for dominance in Somalia. The fighting disrupted food
 distribution and threatened millions with starvation. It was this dark
 prospect that prompted the first major US military-humanitarian
 intervention of the post-Cold War era. In 1992, US Marines stormed
 ashore near Mogadishu, launching a three-year peacekeeping
 operation, coordinated with the UN, that grew to include 40,000
 troops from 25 countries.
 Operation Restore Hope helped end the starvation crisis, but this
 success was overshadowed by the deaths of 18 US troops in a raid
 targeting a Mogadishu warlord accused of hijacking food shipments.
The American deaths led to a rapid and ignominious end to the US
 and UN intervention, despite the absence of a widely recognized
 Somali government and the high probability of another famine.
 What followed was a decade during which Somalia was almost
 entirely on its own, ungoverned, hungry and ignored. "The great ship
 of international good will has sailed," wrote Mark Bowden in his
 seminal book Blackhawk Down. Somalis had "effectively written
 themselves off the map.

  the first
 pirates were Somali fishermen demanding unofficial fees from
 foreign trawlers illegally operating in Somali waters. From there,
 piracy quickly evolved into Mafia-style organized crime. And it could
 only have happened in the absence of a widely accepted Somali
 government, an effective international peacekeeping force or, more
 broadly, substantial economic assistance to desperate fishermen.
 Somalia's isolation and neglect also proved a perfect breeding
 ground for militant Islamists. Promising peace, rallying desperate
 thousands around the banner of anti-Westernism, Somali Islamists
 emerged in the early 2000s and quickly organized across clan lines.
 The Islamists' rapid spread began to pull together Somalia's
 fractured landscape of warlord enclaves. While good for Somalis, the
 prospect of an Islamified Somalia terrified Washington, even more
 than pirates did, at first

 Somalia would make a dramatic reappearance on the "map" 10
years after the Marines stormed Mogadishu's white beaches. The
terrorist attacks on September 11, 2001, organized from
Afghanistan, awoke the United States to the dangers posed by
militant Islamists—especially those Islamists based in ungoverned or
undergoverned spaces. Suddenly Washington thought it could no
longer ignore Somalia. The anarchic country seemed to perfectly
match Afghanistan's profile.
So in October 2002, a force of 800 US Marines landed in Djibouti,
north of Somalia, aiming to "coerce others to get rid of their terrorist
problem," in the cryptic words of Army General Tommy Franks. The
resulting "Joint Task Force Horn of Africa" grew to 2,000 people.
Gunships and drones flying from the task force's base launched
several air raids on suspected Al Qaeda enclaves inside Somalia.
But the air strikes didn't stem the "Islamification" of Somalia or the
spread of piracy. In fact, American attacks may very well have
accelerated both processes, by fueling Somali suspicion of the
Christian West and its allies, and thereby boosting popular support
for the Islamists and for pirates, many of whom had branded
themselves as do-it-yourself Somali "coast guards."

In 2006, an alliance of Islamists calling itself the Islamic Courts
Union defeated Somalia's entrenched warlords and gained control of
Mogadishu. They imposed a moderate form of Sharia law,
suppressed banditry and opened up Somalia to foreign investment.
The BBC called the Courts' meteoric ascent a "popular uprising." In
Mogadishu, residents bristled under the harsher aspects of Sharia,
such as the prohibition of cinemas, but the same resident welcomed
the law and order the Courts enforced. Piracy waned during the
Courts' rule.
But the US State Department had branded the Courts' armed
wing, Al Shabab, a terrorist organization owing to its purported Al
Qaeda ties, so Washington never accepted the Courts as Somalia's
legitimate government, even if most Somalis did. Washington, the
UN, the African Union, and Ethiopia all backed the unpopular, clanbased "Transitional Federal Government" (TFG), formed in Kenya
and headquartered in Baidoa, a small town outside Mogadishu.
In 2006, at the peak of the Courts' rise, Ethiopia—a landlocked
Christian nation and a longtime rival of Somalia with its exquisite
deepwater ports—reflexively launched a 3GW, blitzkrieg-style
invasion of Somalia, with Washington providing key support in the
form of aircraft and Special Forces operating out of Djibouti. In a
matter of weeks, the Courts had been routed. Al Shabab melted into
the countryside and into Mogadishu's teeming slums.
Soon, Al Shabab would reemerge to challenge the roughly 50,000
occupying Ethiopian troops, turning the Somalia conflict into an Iraqstyle insurgency. What followed was two years of urban bloodshed,
punctuated by periodic US air strikes on suspected terrorists in the
countryside. By 2007, Mogadishu residents seethed at the mere
mention of America. "You Americans. You'll destroy an entire city to
get three people," scolded one professor in Mogadishu

 Not
coincidentally, it was the period of Ethiopian occupation in 2007 and
2008 that saw piracy escalate, from a regional nuisance to a global
economic threat.
With tens of thousands dead on all sides, by 2009 the Ethiopians
had had enough. And with Somalia now threatening world trade, the
US State Department had had enough, too. Where before,
Washington had preferred anarchy to Islamic government for
Somalia, now the State Department just wanted order—any order. "If
you want help ensure regional stability and prevent the criminality
that has taken place around Somalia for the last decade and a half,
you must have a state capable of securing its borders," a State
Department source said. "That’s our over-riding perspective.”
Even if that meant the Islamists return? Yes, the source said. "It's
not up to us to decide who has the most legitimacy among the
Somali people."
For Islamists, State's change of heart represented a subtle victory.
The chaos unleashed first by the Somali civil war in 1991, and anew
with the 2006 destruction of the Islamic Courts, had resulted in
seemingly intractable economic warfare that seemed to have
convinced Washington that maybe Islamists weren't so bad, after all.
That's model 5GW. The Islamists had only to let the pirates do what
pirates do best. The pirates, for their part, probably weren't even
aware that they were helping the Islamists wage a winning war.

The Ethiopians' withdrawal in January sparked a dramatic
sequence of events. The TFG fled to Djibouti, where the unpopular
body promptly signed a peace deal with a coalition of moderate
Islamic groups, then elected the former head of the Islamic Courts as
the country's new president. President Shariff Sheikh Ahmed
installed his new government in Mogadishu, restored Sharia, and
began offering truces to holdout Islamists. Only Al Shabab resisted
the olive branch, and the fighting finally subsided. "I'm cautiously
optimistic," one State Department official said. "It’s fragile," he said of
Somalia's new, more inclusive government, "but all new beginnings
are."
As far as piracy is concerned, the return of Sharia law offers hope
of a long-term solution. Sea banditry is incompatible with Islamists'
obsession with lawfulness. If peace holds and Sheikh Ahmed's
government lasts, pirates might finally face real justice in their own
enclaves. But the price, for Washington, is the firm establishment of
a government led by a man formerly associated with a "terror group."

Somalia's recent history is marked by cycles of US engagement
and withdrawal. A major peacekeeping deployment collapsed,
followed by nearly a decade of total neglect, at the end of which
events drew America back to Somalia, but in fits and starts marked
more by strategic failure than by success. US involvement since
2002 failed to stop the Islamification of Somalia, and had the sad
effect of alienating everyday Somalis by prolonging the Courts'
bloody, and inevitable, ascent.
The confusion is, in part, endemic to Africa. But it's also the result
of Washington having two incompatible goals. In Somalia, the United
States wants the kind of law and order that trumps piracy. But these
days that kind of order is best enforced by Islamic groups with broad
popular support—and preventing the rise of such groups is
Washington's other goal. The US can have a stable Somalia, or it
can have a Somalia without formal Islamic leadership, but it can't
have both. Trying to have both means having neither. Insurgency,
piracy and rising anti-American extremism are the immediate results.
The long-term risk is further damage to US interests inflicted by 5G
subtle actors hijacking Somalia's chaos for their own purposes.

 In Africa, in an age of 5GW, the only meaningful security
is individuals' security, and that's a condition that's rarely improved
by the imprecise application of massive firepower.
To prevent 5GW, the US must prevent subtle actors from
"socializing their problem," to borrow Barnett's phrase. That means
preventing the kinds of widespread desperation that makes
individuals and populations vulnerable to exploitation by subtle
actors. The unconscious collective actions of individuals seeking
security—the mass movement of refugees, for instance, or spiraling
tensions over food, water and firewood—are potentially more
destructive, globally and in the long term, than most imaginable
conventional military conflicts between African states. Desperation is
the 5G actor's favorite state—for others.
It's for those reasons that some military planners have begun to
reconceptualize US national security as a facet of world security,
which is itself anchored in human security. "What we're going to see
in the future is that security is not going to be based as much on
state-centric models," said Major Shannon Beebe, the US Army's top
intelligence officer for Africa. "[Security] is not going to be based as
much on state-versus-state type of engagement, but the insecurities
and the conditions of human beings that create these insecurities
across state borders."

In other words, "security" no longer means detente between
superpowers. In the age of instantaneous communications and
empowered individuals, security is a person's freedom from fear,
disease, and hunger. People who feel secure are peaceful, or so the
thinking goes. By that line of reasoning, insecure people pose a
threat to secure people, for the insecure might seek to destroy, with
gestures big and small, the global systems that they believe have
failed them. 5G actors wait in the wings to pick up the pieces, and
rebuild the world according to their designs.
Somali piracy, for one, "is a typical case in which failed states and
a poor representation of marginalized sectors of the population can
become a threat to the security of others around the world," Gonzalo
Peña, a Utah-based consultant with a humanitarian background. The
5G effect of that threat was to help change US policy to embrace the
very people Washington had once branded an enemy.
In some circles, that's called "defeat."

The Observe-Orient-Decide-Act (OODA) loop of John Boyd is not
only a model of human cognition. It is also useful in aligning the generations of modern war within
the framework of human cognition

The arrows of attack fall earlier and earlier in the OODA loop. 
    1GW targets Act
    2GW targets Decide/Act
    3GW targets Orient/Decide 
    4GW targets Orient 
    5GW targets Observe

In contrast to “hearts and minds,” 5GW focuses on the enemy’s
“fingertips and gut.” “Fingertip feeling,” what the Germans called
Fingerspitzengefuhl, is the ability to know without thinking. This is
what Americans call “gut feeling.” To a certain extent, it means a
commander trusting his intuition. It is critical in 5GW because
fingertip feelings, or “hunches,” will be the only way for the enemy to
sense the fighter.
To rephrase these points, in 5GW:
· The people do not have to want to be on the fighter’s side
· Forces the fighter is using do not have to want to be on the
fighter’s side
· Your enemy must not feel that he is not on your side\

arrange the enemy’s OODA loop, so his
thoughts never flow into the orient-decide-act power-line relative to
you, your plan, or your organization

 In limited 5GWs, removing the enemy’s “capacity for
independent action” is the goal. Specifically, the fighter tries to
entangle the enemy into a web of obligations that effectively
reharmonize the enemy, without the enemy knowing that he has
“conditionally surrendered.”

Commentary: In a successful 5GW, the enemy’s attention won’t so
much need to be “diverted” away from a focus but “misdirected” from
ever attaining that focus.

In a successful 5GW, the events the enemy “must”
react to are an “unknown unknown.” The enemy doesn’t know what
they are, and doesn’t even know that he needs to know what they
are.

