---
title: debate
date: 2023-01-14 14:00:42
tags: [debate]
category: [reference]
---

<br>

The normal academic response to an opposing argument is to engage with it, testing the strengths and weaknesses of the differing views, in the expectations that the truth will emerge through a process of <a href="#debate">debate</a>.

<br>

##### Mechanics

Debate requires that both parties obey certain ground rules, such as a willingness to look at the evidence as a whole, to reject deliberate distortions and to accept principles of logic.

A meaningful discourse is impossible when one party rejects these rules. Yet it would be wrong to prevent the denialists having a voice. Instead, we argue, it is necessary to shift the debate from the subject under consideration, instead exposing to public scrutiny the tactics they employ and identifying them publicly for what they are.

An understanding of the five tactics provides a useful framework for doing so:

<br>

###### Conspiracy Theory & Inversionism

[Conspiracy Theory & Inversionism](https://www.industrydocuments.ucsf.edu/tobacco/docs/#id=pyfy0144)

[europepmc](https://europepmc.org/article/med/13891004)

[google](https://www.google.com/books/edition/Smoking_and_Health/yPtqAAAAMAAJ?hl=en&gbpv=1&pg=PR1&printsec=frontcover)

[Fake Experts](https://www.sciencedirect.com/science/article/abs/pii/S0140673605664744) 

[apha](https://ajph.aphapublications.org/doi/full/10.2105/AJPH.2004.061507)

[Selectivity](https://www.bmj.com/content/326/7398/1057?query=rft.jtitle%25253DAmerican%252BJournal%252Bof%252BEpidemiology%2526rft.stitle%25253DAm%252BJ%252BEpidemiol%2526rft.issn%25253D0002-9262%2526rft.aulast%25253DSchaefer%2526rft.auinit1%25253DC.%2526rft.volume%25253D141%2526rft.issue%25253D12%2526rft.spage%25253D1142%2526rft.epage%25253D1152%2526rft.atitle%25253DMortality%252BFollowing%252BConjugal%252BBereavement%252Band%252Bthe%252BEffects%252Bof%252Ba%252BShared%252BEnvironment%2526rft_id%25253Dinfo%25253Apmid%25252F7771452%2526rft.genre%25253Darticle%2526rft_val_fmt%25253Dinfo%25253Aofi%25252Ffmt%25253Akev%25253Amtx%25253Ajournal%2526ctx_ver%25253DZ39.88-2004%2526url_ver%25253DZ39.88-2004%2526url_ctx_fmt%25253Dinfo%25253Aofi%25252Ffmt%25253Akev%25253Amtx%25253Actx)
             
[tobaccocontrol](https://tobaccocontrol.bmj.com/content/14/2/118.short)

[Impossible Expectations]()

[Misrepresentation & Logical Fallacies](https://tobaccocontrol.bmj.com/content/17/5/291.short)

<br>

##### GOALS

- Have the courage to test the ideas you believe in against someone who disagree
- Have the humility to listen, discuss, and learn from others
- Have the willingness and determination to change past beliefs and behaviors in order to be better

- Do not place a high value on compliments, concessions, apologies, excuses and bumper sticker wisdom.
- Do not waste time, thoughts, and ideas with those incapable of honest discourse
- Do not compromise values in the name of convenience, social acceptance, or to avoid uncomfortable questions and awkward moments

<br>

Reasons are called premises.  The claim they back is called conclusion.
A simple argument is one conclusion supported by one reason.
Extended argument is main conclusion supported by reasons that are also conclusions of other reasons

Evaluating argument
   1. Are reasons true or likely to be true
   2. Are reasons sufficient for us to accept conclusion

Two forms of argument
   1. deductive - uses logic (categorical and propositional)
   2. inductive - probabilistic generalizations, causal arguemnts, analogical arguments

A cogent argument has
  - a valid deductive argument with acceptable premises
  - inductive argument with acceptable premises in which reasoning is legitimate and sensible

Cogent argument
   - premises are acceptable
   - conclusion is sufficiently grounded by the premises

Normative is prescriptive is what OUGHT    Bill ought not put cat in microwave
Descriptive is what IS                     Bill put cat in microwave

Critical thinking is meta-philosophy, tools and methods philosophers use

Autonomous means we hold beliefs that are justified, not those thrust upon us

Argument - providing reasons for a belief

Aristotle showed 3 basic activities of mind:
 1. representing         draw moon
 2. judging              saying it is big
 3. inferring            saying it's getting closer

Kant said inferences are indirect judgements

New definition

An argument is the presentation of reasons offered in support of a claim.  A conclusion backed by reason(s).

An opinion is a conclusion without supporting reason(s).

Make other person in conversation a partner, not an adversary.

Messengers espouse beliefs and assume their audience will listen and ultimately embrace their conclusions.

Conversations are exchanges.

Dedictive reasoning uses logical principles rather than observation and evidence.

Conceptual 

we appeal to logic and the meaning of words and concepts to arrive at the answers without engaging in experiments or observations.

 Evaluative questions are questions which explicitly or implicitly invoke values and norms. These questions relate to value judgments about moral correctness or aesthetic values.


<a style="font-weight:bold">Argument</a> is how you establish something is true.


All arguments are composed of premises and conclusions, which are both types of statements. The premises of the argument provide a reason for thinking that the conclusion is true.

A standard way of capturing the structure of an argument is by numbering the premises and conclusion:

1. It is morally wrong to take the life of an innocent human being
2. A fetus is an innocent human being
3. Therefore, abortion is morally wrong

<br>

<a style="font-weight:bold">Validity</a> relates to how well the premises support the conclusion, and it is the golden standard that
every argument should aim for. 

A valid argument is an argument whose conclusion cannot possibly be false, assuming that the premises are true.

A valid argument is an argument in which if the premises are true, the conclusion <i>must</i> be true.

<br>

##### Counterexample

A <a style="font-weight:bold" href="#counterexample">counterexample</a> is simply a description of a scenario in which the premises of the argument are all
true while the conclusion of the argument is false. If you can construct a counterexample to an argument, the argument is invalid.

A sound argument is a valid argument that has all true premises. That means that the conclusion of a sound argument will always be true.

The only difference between a valid argument and a sound argument is that a sound argument has all true premises.

The concepts of validity and soundness that we have introduced apply only to the class of what are called “deductive arguments”. A <a style="font-weight:bold">deductive argument</a> is an argument whose conclusion is supposed to follow from its premises with absolute certainty, thus leaving no possibility that the conclusion doesn’t follow from the premises.

<br>

##### Inductive Arguments

An <a style="font-weight:bold" href="#inductive-arguments">inductive argument</a> is an argument whose conclusion is supposed to follow from its premises with a high level of probability, which means that although it is possible that the conclusion doesn’t follow from its premises, it is unlikely that this is the case.

The conclusion of this argument is a normative statement—a statement about whether something ought to be true, relative to some standard of evaluation. Normative statements can be contrasted with descriptive statements, which are simply factual claims about what is true.

An important idea within philosophy, which is often traced back to the Scottish philosopher David Hume (1711-1776), is that statements about what ought to be the case (i.e., normative statements) can never be derived from statements about what is the case (i.e., descriptive statements). This is known within philosophy as the is-ought gap. The problem with the above argument is that it attempts to infer a normative statement from a purely descriptive statement, violating the is-ought gap.

As an empirical science, psychology attempts to describe and explain the way things are, in this case, the processes that lead us to believe or act as we do. Logic, in contrast, is not an empirical science. Logic is not trying to tell us how we do think, but what good thinking is and, thus, how we ought think. The study of logic is the study of the nature of arguments and, importantly, of what distinguishes a good argument from a bad one.



