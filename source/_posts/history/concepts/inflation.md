---
title: inflation
date: 2023-01-16 19:00:42
tags: [security, defense, tools, tactics]
category: [ecosystem, events]

---

<br>

<b>Inflation</b> is an expansion of the money supply.  This causes rising prices and recessions which get blamed on market conditions and world events (COVID, supply chain issues, Russia, built-in, etc.)

<br>

##### Mechanics

<b>Price</b> is a function of the ratio of money available to goods available.  If money twice as available, things cost twice as much.

This creates higher prices and the illusion of growth.  Assets don't grow, money loses value.

Inflation reduces the wealth of workers and employers while also increasing the price of market goods they want to purchase.

<br>

##### Analysis

Economic growth is the growth of goods and services, not the growth of money.  It is a result of saving and investing in productivity.

Money growth leads to more consumption, less savings, and stagnation or regression of living standards due to the reduction of capital investment.


Government bonds ( pension funds, treasuries ) all went down in value 

Government sold assets that it massively devalued, wiping out every institution that trusted it.

ultra-loose fiscal, monetary, and credit policies

Years of QE kept cost of borrowing near zero
Created insolvent "zombies" that were propped up by low interest rates
Bailouts rescued many that would have gone under 

Now central banks are forced to increase interest rates which does 3 things
  1. Sharp increase in debt-servicing costs
  2. Inflation eroding real household income 
  3. Inflation reducing value of household assets (homes & stocks)

<br>

  1. Rising borrowing costs
  2. Falling incomes and revenue
  3. Falling asset values

<br>

- prices go up and down because of supply and demand of resources
- shouldnt go up and down because of money supply
- The government grades its own success ( cpi )
- USD became reserve because we were the biggest creditor and had huge trade surpluses.
- Today we are biggest debtor with huge trade deficiencies.
- Unprecedented sanctions Biden put on Russia, highlights tremendous risk every soveriegn nation assumes when beholden to the dollar.
- federal reserve notes are a promise to pay something.  They used to mean pay in gold.  No longer mean anything.
- gold was good, then third party was involved (govt, banks), then everything got manipulated and abused.
- With stock you own equity, with bonds you only get the principal and interest.
- The creditworthiness of the borrower
- As interest rate rises, existing bond prices fall (because of their now lower interest rate)

