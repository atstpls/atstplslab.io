---
title: spiritual world
date: 2023-01-16 18:00:42
tags: [fiat, crisis]
category: [ecosystem, events]
---

<br>

{% blockquote %}

    *"On one hand, we may interpret reality exclusively with mechanical causality, 
    where everything is devoid of meaning and higher purpose. 
    On the other hand, we may interpret reality exclusively through a cosmic language, 
    where facts and events are the hosts of spiritual meaning. 
    Neither of these worldviews is complete without the other."*
{% endblockquote %}

{% blockquote %}

    *"A purely material perspective knows how the universe works but perceives no higher reason for its existence,
     while a purely spiritual perspective knows the meaning of the universe without understanding how it technically works. 
     Ideally, we should be able to adopt both perspectives without having to sacrifice one or the other."*
{% endblockquote %}


{% blockquote %}
    The spiritual world is higher meaning, manifestations of truth. What does it mean? What truth does it embody?
    A philosophical or moral ideal does not exist in one person, but is an abstract concept that manifests itself in human form.
    Humans continually encounter the unknown, identify patterns, and categorize the experience in order to improve future experience by viewing it through the lens of the past. 
    We extract representations of metaphysical principles or patterns of being.  
      

    Induction - making an inference of an abstract principle based on concrete instances 
A philosophical or moral ideal manifests itself first as a concrete pattern of behaviors that's characteristic of a single individual

Then a set of individuals, then an abstraction of that set 

We need abstracted ideals we can act out so that we can understand what they mean

{% endblockquote %}


{% blockquote %}
    The physical world is matter, energy, and mechanical causality. How does it work? What is it made of?
    All objects are entities bounded by their contextual affective relationship to a goal.  We do not try to collect objective facts for their own sake, but by their contextual affective relationship to a goal. Without context, there are infinite ways to perceive objects and a near-infinite number of objects to perceive. Instead, only objects related to our goals are perceived, and are evaluated with a value system.
{% endblockquote %}






<br>


---
title: metamaps
date: 2023-01-18 03:00:42
tags: [trading]
category: [ecosystem, concepts] 
---

<br>


Everyone has hidden assumptions that are implicit, unconsicous, axiomatic, taken on faith as absolute truth assumptions about the way reality really is.  This is the fallacy of reification (hardened, made real)

Critical thinking is process of unpacking and challenging hidden assumptions that exist in our own action, thought, or perception, testing them with alternatives, and possibly practicing until it's made implicit, re-abstracted into unconscious habit, but this time chosen consciously.

## World of Meaning 

Humans began to make sense of this chaotic world by representing it in primitive forms of abstraction. At first, non-linguistically (e.g., mimicry, image, ritual). Later, by telling stories about it. These symbols and myths functioned as pragmatic guides to action.

## World of Objects

Plato said that the changing natural world knowable by the senses—what he called the realm of Becoming—was too unstable to provide real knowledge. To overcome this problem, he proposed the existence of an unchanging (permanent; timeless) realm of Being, ostensibly filled with the universal categories of thought. These universals comprised a hierarchy of eternal Ideas said to be directly accessible by the mind. For example: the Idea of the Bird, the Idea of the Chair, the Idea of Justice, etc. Plato said that the actual objects and particulars here in the earthly realm of Becoming are only lesser, shadowy imitations of the real Ideas that reside in the otherworldly realm of Being. The absolute, abstract Ideas are what give particular things their conceptual identity, and thus to mentally apprehend the Ideas themselves is to know the real. 

Aristotle (384-322 BC), Plato's #1 student, preferred the concrete, everyday realm of Becoming over the non-empirical realm of Being, but he still tried to account for both.

During the Middle Ages (~476-1450 AD), Aristotle's theory of teleology was incorporated into Christianity and prevailed as a psychologically acceptable way of guaranteeing the objectivity of values. When applied to humans, a telos provides a certain way of "being in the world": goals or values toward which we understand we must strive. In other words, teleology provides an uncontroversial, collectively accepted "built-in" human ought, i.e., an answer to the question of meaning and purpose. That is, when people really, truly believe a teleological explanation, there is no separate problem of value (no is-ought problem, no fact-value problem).

During the Renaissance (~1450-1650), Scientific Revolution (~1400-1690), and Enlightenment (~1601-1800), the spirit of the times changed: intellectuals began to believe that during the Middle Ages they had been falsely interpreting reality through an artificial Aristotelian teleological overlay. Now they had stripped that illusion off, and discovered the "real facts", the bottom of everything — the mechanistic, mathematical basis of nature — the worldview of science, or what Peterson calls the "world of objects". Formal & final causes were dispensed with, and material & efficient causes became the sole basis for explanation (theorizing).

The problem of value thus arose as a separate problem. Teleological explanation was no longer universally, psychologically accepted. It was therefore no longer clear where values came from, how they were justified, or if they even existed. The humanities (studying the world of meaning or value) split from the sciences (studying the world of objects). Fact was split from value, data from theory, is from ought.

René Descartes (1596-1650) reified this philosophical & psychological schism in his theory of metaphysical dualism. He claimed that reality literally consists of two entirely separate & different realms: res cogitans (mind; the mental) and res extensa (body & world; the physical). This was the origin of the modern mind-body problem.

the objective realm is considered the real, "higher" reality from which we get true knowledge — that is, unchanging, fundamental, and certain scientific knowledge that goes beyond opinion or mere belief. The objective realm is value- and meaning-free, i.e., context-free: objects are objects precisely because they remain the same regardless of their surrounding context. Put another way: the external world is thought to have an intrinsic, pre-existing structure independent of anyone's subjective interpretation of it.
by contrast, the subjective realm consists of our free will, personal opinions, feelings, memories, imagination, biases, identities, etc. In other words, this realm is about value, meaning, interpretation, and context. The subjective is considered "lower" than the objective. It is too changeable & idiosyncratic to be a source of true knowledge, instead functioning mainly as a wellspring of errors & biases that distort our perceptions of the objective realm. The scientific method is therefore how we overcome the subjective in order to more accurately know (represent) the objective.

Enlightenment epistemology (theory of what & how we can know about the real) split along the forks of Cartesian dualism, in many ways recapitulating the difference between Plato (proto-rationalist) and Aristotle (proto-empiricist):

the empiricists focused on the world or object side of the dualism. They said that we know the world primarily through our objective sensory perception/observation of it. In other words, the true source of knowledge is the object causally impinging on the subject. The mind's contribution is only secondary to that. Empiricists tend to reason from the bottom up (particular → general; part → whole; data → theory).
the rationalists, on the other hand, focused on the mind side of the dualism. They said that the true source of knowledge is rooted in our objective thought or reason, e.g. in innate knowledge or innate concepts. Rationalists often emphasize, against empiricists, that the world of sensory perception (appearances) is too deceiving, changeable, illusory, or otherwise unfit to be the true source of knowledge. Rationalists tend to reason from the top down (general → particular; whole → part; theory → data).
In short, empiricism beat rationalism. Because empiricism is the primary epistemology of science, it's safe to say that it's the primary epistemology of the West. We now generally believe that to know most fundamentally means to scientifically know objective reality. And we usually conceive of this reality as really existing, entirely separate from ourselves.

Nietzsche (1844-1900) would later describe the loss of value, cemented during the Enlightenment, as the "Death of God". He predicted that nihilism and mass social upheaval would necessarily follow. 

Today, we commonly reify the following assumptions (often entirely unconsciously), which are pretty much straight out of Enlightenment thought:

metaphysical realism: a "higher", knower-independent real reality or "true world" really exists, completely separate from all humans. In subject-object dualism specifically, the nature of this reality is physical or material, i.e. the objective realm. However, the distinguishing feature of realism is not the specific nature of the real, but rather its knower-independence. So, for example, Plato also counts as a realist, even though the nature of Being (i.e., the Ideas) was mental rather than physical.
subject-object dualism: the world is really, actually divided into the subjective and objective "realms". These realms are entirely independent of one another (i.e. fully understandable in isolation from one another) and they literally "exist". Therefore, the problem of skepticism (how the subject "in here" comes to know the object "out there", e.g., by representing it) is a genuine problem in need of a theoretical solution. Moreover, because the subject stands completely outside of the objects he studies, he has a privileged epistemological position from which he can know objects "as they really are".
objectivism: the objects that reside in the "real reality" or objective realm have pre-determined, fixed structures and properties. In other words, the way in which they are perceived or observed has absolutely nothing to do with their intrinsic essence (being) and qualities.
representationalism: the world is a picture to be observed by the subject, and thoughts are mirrors of that objective reality. The knower's immediate object of perception is therefore not the world itself ('reality'), but rather an idea or mental representation of the world (an 'appearance'), which intermediates or stands in between knower and world. In this way, the subject is theorized to be radically separated from the object, standing "over and above" it, striving to internally represent it as it really is, by using a combination of his ostensibly value-free observation and value-free reason. The subject more clearly observes reality in direct proportion to his disinterest and distance from it. Scientific knowing is therefore thought to be the general model of knowing. Humans fundamentally know the world in the manner of scientists, even if we don't always live up to the scientific ideal in our everyday lives. A.K.A. 'copy theory', 'spectator theory', 'indirect realism', the 'discoverer' (as opposed to 'inventor') perspective, etc. Also similar to the 'sense-data' theory of perception.
propositional (knowing-that) theory of knowledge: real knowledge is specifically restricted to knowing-that knowledge, i.e. inner propositional/theoretical conceptual representations of objective reality (e.g., justified true belief). Scientific knowledge is thus sharply distinguished from & privileged over pedestrian knowing-how (practical habit or skill), as well as from "mere belief" and opinion.
correspondence theory of truth: we know the truth when our inner conceptual representations accurately and literally reflect the state of affairs out there in objective reality. Peterson calls this "Newtonian truth".
disembodied mind: mind is completely unconstrained by the nature of our bodies & brains, and its essential workings do not depend in any way on the body's physical surroundings. Reason is transcendent (an objective feature of the universe that human minds participate in). Reason is unrelated to or even at war with bodily or subjective functions such as emotion, imagination, values, etc. When reason is operating correctly, it escapes the influence of subjectivity and operates according to objective, precise, logical, even mathematical rules.
dyadic theory of meaning: since the objective & subjective realms are completely separate and different from one another, meaning also has separate objective and subjective components. Objective meaning expresses a representational (correspondence) truth relationship between our internal concepts/propositions and external reality, or between our internal concepts. What a representation objectively "means" is therefore what it refers or corresponds to. This is the only real kind of meaning. On the other hand, subjective (personal) meaning is though to be completely arbitrary & idiosyncratic and therefore not really real (i.e. epiphenomenal). In both cases, meaning exists in a dyadic semiotic system, in which meaning is "all in the head" and is purely a matter of mental interpretation occurring inside a disembodied, isolated mind.
faculty psychology: we are made of discrete, nonoverlapping "faculties" that work together like a mini-society or even a factory assembly line. For example, sensation comes first, then perception, then cognition, then understanding, and finally action. All of these faculties are entirely self-contained & separate from one another, both psychologically and neurologically.
naturalism: the categories of natural science (informally, physical matter in the objective realm) are sufficient as opposed to merely necessary conditions to understand the world, including first-person subjective experience. Therefore, the epistemology of the hard sciences (empiricism) is also the correct epistemology for psychology.
mechanism: following from naturalism, the universe is fundamentally a Newtonian "billiard ball" physical system that operates purely on material & efficient causes. Formal & final causes are subjective overlays and therefore not really real. This would seem to logically imply determinism (necessary effects of necessary causes, essentially the opposite of teleology and free will), and therefore nihilism (there is no meaning and so nothing really matters).
reductionism: wholes are nothing but the sum of their parts, which means they can be fully understood by decomposing them and studying their parts in isolation. For example, you can understand situations, events, or activities by reducing them to their constituent facts or objects, and studying those parts independently.
atomism: the divisions between things are more real and objective than the relations between things. Relations are merely secondary, subjective add-ons to the objective.
the classical theory of categories: categories (kinds) are metaphorically conceptualized as abstract thing-like containers, which contain abstract thing-like objects, and therefore operate according to mechanistic spatial logic. They have sharp borders defined by necessary & sufficient conditions that strip away surrounding context, which means that things are either firmly inside or outside a category. They occur "inside" (after the fact of) experience. Mind, which is itself conceptualized as a container, contains thing-like representations (concepts) of the categories of objects that objectively exist out there in the external world. Science is therefore the process of studying these categories and their contents.

### Abstractionism

The hidden meta-assumption that things are most real when they're divorced from their surrounding contexts---that self-sufficient abstractions are more real than the concrete experience that they're abstracted from, and can therefore provide a metaphysical ground for explanation. 

Western philosophical tradition as: a search for certain, unchanging, universally correct ideas (abstractions) that represent or mirror the objective state of affairs out there in the "real reality", which ideally span all times and places (abstracted from, or independent of, all contexts), discovered by a knower with a fixed, disembodied (abstracted) self and mind, radically separated (abstracted) from the reality that is to be known.

### Problems with Abstractionism

- No philosopher has ever proved metaphysical realism, because there is no way to prove it even in principle. Realism posits an unparsimonious conceptual doubling or copying of reality. Just because we can construct conceptual systems that work "as if" an external reality exists, does not mean that it actually, literally does.
- No philosopher has ever proved that representations represent, i.e., that phenomena are bona fide appearances, because there is no way, even in principle, to prove it. There is no conceivable property of mental representations that could ever prove that they actually represent an external reality.
Subject-object dualism fails, because if metaphysical realism fails, then there can be no objective realm that "really exists" independent of all knowers. And if the object side fails, so does the subject side: no self-sufficient objects necessarily implies no self-sufficient subjects internally representing those objects.
- No philosopher has ever successfully explained how facts & reason alone get us from is to ought in an objective (i.e. value-free) way. David Hume's is-ought problem said that the facts do not tell you what to do with the facts, and it remains undefeated. All known arguments in some way presuppose what they purport to prove. Some prior value, which does not itself come from the facts, inevitably gets smuggled into the argument.
- The classical theory of categories is based on handed-down, implicit, reified philosophical assumptions — not empirical evidence. What if there were a way to turn the tables and make categories themselves the subject of empirical study?
- Life is fundamentally lived concretely and subjectively, not abstractly and objectively. Like it or not, first-person experience (the only kind anyone has ever had) is about contextualized subjectivity, knowing-how, action, meaning, and value — precisely what traditional philosophy excludes. In this way, philosophy becomes detached from life.
- Abstractionist assumptions leave the role of the subject's internal category/interpretive/perceptual/value structure hidden (unconscious) — and therefore projected into the black boxes of "objects" and/or "reason".

## Frame Problem 

In the 1980s, cognitivists ran into the frame problem

for a computer to add "subjective meaning" (i.e. a context) onto a stored representation that contains only "objective meaning" (i.e. a descriptive model of some external physical object or event) — a rule (algorithm) is required. However, to put that contextualized representation into context, an additional rule is required...and so on...forever. This leads to combinatorial explosion and an infinite regress of contexts. It turns out there's simply no way to computationally add subjective context (meaning; relevance; significance) to inherently meaningless objects. The number of potential contexts you have to account for is unlimited. No final stopping rule is possible.

AI/robotics researchers building artificial vision systems soon realized that the problem was even more serious: the frame problem doesn't merely occur when trying to add subjective context onto independent objects, but rather, perceiving objects in the first place already presents the frame problem! That is, when an artificial vision system tries to discriminate objects from their surroundings based on objective sensory properties, it's immediately faced with the question of which similarities & differences in the visual data are the currently relevant ones (a value judgment, which requires a specifying context). This merely pushes the frame problem one step backward. Importantly, this wasn't just a philosophical (conceptual) problem — it was a bona fide scientific (empirical) problem.

Events are simple and distinct only insofar as their relevant features are framed, a priori, by the constraints of an operative [subjective] context."


## Kant 

Around end of Enlightment, Immanuel Kant had tried to incorporate the strengths of empiricism into rationalism, while overcoming the weaknesses of each. Whereas virtually all previous philosophers had put the universal categories of thought (e.g., ideas, essences) into the object, Kant moved them into the subject. He said that the mind comes equipped with universal "a priori" categories of understanding, such as unity, plurality, possibility, necessity, etc. These internal categories shape incoming sensory data from the start and thereby make the world intelligible to us. Against Descartes, Kant said we don't, and can't, experience the independent object "as it truly is" (what Kant called the "thing-in-itself" or the noumenon), even for a moment. The only reality we can ever know — the totality of our phenomenal experience — is inescapably knower-dependent. This was Kant's rationalist "Copernican revolution".

Kant called his philosophy "transcendental idealism". This was an investigation into what must be the case in order for human experience to be the way it is. In other words, transcendental investigation was the philosophical method of attempting to make implicit (hidden) assumptions explicit (that is: the very same thing we are doing in this document).

However, Kant made a key abstractionist error: he conceived of the a priori categories as decontextualized (self-sufficient, disembodied, ahistorical, etc). That is, for Kant the nature of the body & brain (including meaning, value, emotions) and all other meaningful human contexts, played no determinative role in the construction of phenomenal experience. His original a priori categories were overly intellectualized, contrived, biologically/neuropsychologically implausible abstractions (again: plurality, necessity, etc). He also thought, at least on some readings, that the noumenal "realm" really, literally existed apart from all human subjects.

Post-Enlightenment philosophers expanded on Kant's Copernican revolution in various ways. For example, the later German idealists focused on the social & historical dimensions of experience. Nietzsche had the loosely related theory of perspectivism (rejection of a single, objective, knower-independent god's-eye view of reality). But the biggest leap happened in the 20th century when philosophers (e.g., pragmatists, phenomenologists, existentialists) and psychologists (e.g., Jung, Piaget) added meaning, embodiment, motivation, emotion, and function back into the picture.

## Peterson 

 The commonalities between these fields provides the basis for a new philosophical meta-assumption. This is the perspective that underpins Maps of Meaning, and is an alternative to abstractionism:

contextualism: the meta-assumption that what is most real are not "things" themselves (i.e., self-sufficient abstractions like objects, facts, essences, etc), but rather their contextual meanings.

Contextualism can be considered a return to the world of meaning, but 'on a higher level.' Whereas the abstractionist paradigm eventually narrowed down Aristotle's four causes to only material & efficient causes (the two types unrelated to meaning), the contextualist paradigm brings back formal & final causes. And it does this through the construct of contexts or wholes, to which the Kantian a priori categories were the precursor.

Importantly, contextualism does not simply discard or reject the abstractionist worldview. Rather, the abstractionist worldview is theorized to be nested inside (a part of) the contextualist worldview (the whole). Peterson (1999a) puts it this way:

"The world can be validly construed as forum for action, or as place of things . . . No complete world-picture can be generated without use of both modes of construal."

pragmatism (Peirce, James, Dewey): The key idea in pragmatism is that all thinking and ideas ultimately only matter in terms of their practical effect (i.e., their meaning when cashed out in action). In other words: "you will know them by their fruits". Pragmatism is a nondualist philosophy focused on re-conceptualizing traditional dualisms as contingent, instrumental abstractions rather than reified ontological dichotomies. It is Darwinian (evolutionary), in that it denies a radical gap between animal and human cognition, and in that it views all cognition (including scientific inquiry) as embodied and fundamentally devoted to practical problem-solving (adaptation) rather than objective representation for its own sake.
phenomenology (Heidegger, Merleau-Ponty): Phenomenology is the philosophy of first-person experience & meaning. Essentially, it dispenses with the Kantian noumenon and focuses only on the phenomenon. It therefore inverts the traditional focus on the objective, instead starting from the premise that life is fundamentally lived subjectively (although phenomenologists tend to reject the term "subjective" because they wish to undermine the entire subject-object dichotomy). Like pragmatism, phenomenology is skeptical of explanatory abstractions and instead redirects attention to concrete action & perception. Heidegger's phenomenology, ontological hermeneutics, specifically emphasized that the deepest ground of existence is a contextual/interpreted relational matrix of meanings, not a world of physical matter. He said objects are more like "equipment in order to", i.e., tools. Merleau-Ponty was known as the phenomenologist of embodiment. He rejected the Cartesian cogito ("I think therefore I am"), and instead suggested that human beings are characterized by something much more like 'I am because I can' — a focus on action over detached cognition.
existentialism (Kierkegaard, Nietzsche, Heidegger): Existentialism aimed to close the gap between philosophy and life. It critiqued the traditional fixation on the abstract and unchanging (e.g., the realm of Being, essences, subject-object dualism, substances, mechanisms), instead pointing out that the allegedly "lesser", changing realm of Becoming (existence) is actually the fundamental setting in which we live. Philosophy should therefore not be an attempt to mentally escape into a hypothetical "true world", but rather an investigation into concrete existence to help us live more meaningful lives. Existentialism emphasizes particularity over universality, and shares with phenomenology a general emphasis on first-person experience.
philosophy of language (Wittgenstein): In the first half of his career, Wittgenstein advocated an extreme abstractionist, dyadic picture theory of meaning: words get their meaning through direct mappings onto empirically observed reality. But in the second half of his career, he renounced this view and instead said that the meaning of words is their use. In other words, to understand the meaning of words, you have to look at the everyday practices or contexts in which they're used.
analytical psychology (Jung): Carl Jung hypothesized that there is a relationship between meaning and psychological health. He studied the history of collective (cultural) symbolic productions (dreams, religion, art, fantasy, ideas, etc., and especially myths), all of which are expressions of what humans historically found to be valuable. In other words, Jung studied unconscious fantasies of value, cross-culturally, over time, in an attempt to find the highest value (the highest meaning) common to all cultures & times. The central symbol that emerged from Jung's research was the hero myth.
cybernetics (Wiener, Miller, Luria, Sokolov, Vinogradova, Gray, etc): the cybernetic hypothesis is that "the fundamental building block of the nervous system is the feedback loop" (i.e., learning from error, or as Peterson (2013b) puts it, "the assumption that goal-directed, self-regulatory systems constantly compare what is to what should be, while attempting to reduce mismatch").
narrative psychology (Sarbin, Bruner): The narrative psychologists argued that our fundamental kind of internal representation is not propositional or objective, but rather story. Further, stories aren't descriptions of reality that occur after the fact of experience, but rather they constitute experience, from the start.
ecological psychology & enactivism (Gibson, Rosch): J.J. Gibson was an ecological psychologist influenced by the pragmatists and the phenomenologists. His key insight was that our perceptual systems are evolutionarily adapted not to the micro-level described by the categories of physics, but rather the middle ("basic") level defined by body size and features. This level is made of action opportunities (affordances; tools; meanings; objects viewed relative to subjects' embodiment and desires). He argued that perception is not primarily for objective representation but rather for adaptive action, and therefore that perception & action are tightly coupled. The enactivists had similar ideas, but started more from the perspective of the subject/animal rather than the object/environment.
process philosophy (Whitehead): Process philosophy, like existentialism, views becoming (change) as the default state of affairs, and being (permanence) as only provisionally established within that broader matrix of change. Process, change, events, and relationships are its preferred categories of analysis, rather than structure, substance, essence, object, etc.
4E cognitive science (Rosch, Lakoff, Johnson etc): As we've seen, first-generation cognitivism was based on abstractionist assumptions that ran straight into the frame problem. Second-generation "4E" (embodied, embedded, enacted, extended) cognitive science brings the body, action, meaning, and the environment back into the scope of study.

Key contextualist assumptions
Here are the key assumptions of Peterson's contextualism (derived from the above-mentioned fields), contrasted with their abstractionist foils. Note that many of these assumptions overlap one another.

constructivism (an alternative to metaphysical realism): what appears to be a pre-given, independent reality is actually already a co-creation of the knower. In other words, the only reality we can ever hope to know is the raw phenomenal experience that we construct or interpret — and it is precisely our participation in this construction that allows us to know. The terms "subjective" and "objective" therefore no longer imply an absolute ontological Cartesian dichotomy. Rather, they become shorthand ways to focus attention on (provisionally abstract out) the relative contributions of one side or the other. Because as soon as you talk about one side, you are also necessarily talking about the other — and to an ultimately unknown degree. A.K.A. the 'inventor' (as opposed to 'discoverer') perspective.
embodied mind (an alternative to disembodied mind and faculty psychology): Descartes reified mind as a disembodied inner realm, but mind—body dualism is false. Mind is evolutionary, embodied, cultural, historical, enactive, emotional, & motivated. The way we think, and the categories we have, are shaped by our bodies and the meaningful contexts we inhabit. In other words, abstract thought is built on sensorimotor and perceptual capacities. Essentially, mind is more abstract body, not a separate and different ontological kind. Nor is mind made of discrete "modules". For example, sensorimotor abilities and perception are not two completely separate and different "faculties". Rather, they overlap one another, and are processed in the same areas of the brain.
goal-directedness: whereas the abstractionist viewpoint portrays us as primarily occupying a detached, disinterested stance in which we collect objective facts for their own sake, contextualism conceives of us as always perceptually oriented toward some motivated goal. Peterson says: "It is an accepted axiom of neo-psychoanalytic, cybernetic, behavioral, cognitive, psychobiological, narrative and social-psychological theories that human behavior is goal-directed, rather than simply driven." In other words, we are always already applying some way of 'being in the world'.
pragmatic (knowing-how) theory of knowledge (an alternative to the propositional theory of knowledge): since contextualism rejects metaphysical realism in favor of constructivism, knowing is no longer a matter of an isolated and self-contained subject passively observing and propositionally 'mirroring' a pre-formed and independent object. Instead, our most fundamental relationship with the world is theorized to be nondual and action-oriented. (Very similar to Heidegger’s “ready-to-hand”, a mode of being in which we are at one with our environment, and interact with the objects around us not as targets of detached, scientific observation, but rather as tools available for fluid use in the activities & practices we are involved in, as if they were natural extensions of our bodies). Knowing is therefore a practice or activity of adaptive problem solving. We know when we can successfully act to transform our experience in desired ways, and correctly anticipate the meanings of things for future experience. "Knowing" thus means an ongoing mutual reconstruction of knower and world (i.e., an activity or process). The problem of skepticism in Enlightenment thought — how the subject "in here" comes to know the world "out there", and how that gets guaranteed through rational epistemological arguments (e.g., foundationalism) — was never a genuine problem in the first place.
instrumentalism/anti-representationalism (an alternative to representationalism): ideas are abstract tools rather than mirrors of reality. The referents of abstract concepts are not primarily objects, but rather actions & perceptions. The purpose of abstraction is therefore not to produce objective representations as ends in themselves, but to produce desired, adaptive transformations of experience. In other words, the purpose of abstraction is ultimately the same as the purpose of action.
pragmatic theory of truth: (an alternative to the correspondence theory of truth): truth is not primarily a matter of how well a proposition or other internal representation corresponds to the objective state of affairs in the external world, but rather how well it works to achieve a desired goal (predicting & controlling). In other words, an idea is true to the degree that it functionally 'fits' the world (in the same sense as Darwinian 'survival of the fittest'), as opposed to how well it 'matches' in a picture-like manner (Von Glasersfeld, 1984). The typical concern with a pragmatic theory of truth is that it's nothing but a fancy way of saying "ends justify the means". We will discuss later why this is not necessarily the case.
action—abstraction "dualism" (an alternative to subject-object dualism): given the primacy of knowing-how in contextualism, the fundamental "dualism" of interest is no longer subject—object, but rather action—abstraction (that is, action—perception/cognition). Dualism is in scare quotes to emphasize that these are just relative and provisional poles on a continuum of experience, not separate, literal realms. And this is not a static dualism, but it is a dynamic cybernetic loop: action is the application of current beliefs in pursuit of goals, whereas abstraction is a pause when error is encountered to rework the presuppositions that guide action. This is Piagetian assimilation & accommodation. (The same pattern is also evident in Dewey's need-search-satisfaction, Heidegger's ready-to-hand → unready-to-hand → present-at-hand → ready-to-hand, Merleau-Ponty's intentional arc and maximal grip, J.J. Gibson's direct perception, etc.)
triadic theory of meaning (an alternative to the dyadic theory of meaning): in contextualism, meaning is not fundamentally linguistic, cognitive, theoretical, representational, nor "all in the head". Any of those things can be meaningful, but they are not what meaning most fundamentally is. Rather, meaning is a property of a triadic semiotic system, which means that it cuts across subject & object (that is, it's a property of experience as a whole, not just of a disembodied inner mind) and is therefore inseparable from action. We will conceptualize meaning as follows. It is:
pragmatic, in that it is implication for action, or re-configuration of implication for action (i.e. re-configuration of the a priori interpretive / perceptual framework)
existential, in that it matters subjectively (motivationally & emotionally) and makes life worth living
normative, in that it provides a moral ought
ontological, in that it is the fundamental "stuff" of experience
relational, in that it depends on a mutual relation or interaction between subject & object, as opposed to being a 100% subjective overlay
teleological, in that it is leading you toward some end
value-ladenness and theory-ladenness (alternatives to objectivism):
value-ladenness, closely tied to goal-directedness, means that there is no value-neutral stance toward the world, not even through the scientific viewpoint. Science is sometimes said to be a "value-free" endeavor involving value-free observation and value-free reason, but even though scientists try to strip away their own subjective (idiosyncratic) values, they are still collectively applying a certain, consistent scheme of action & perception (selective attention to the intersubjectively apprehensible aspects of perception, i.e. the epistemology of scientific empiricism). Thus, the scientific method does not escape values. It is value-laden and therefore ideological (the ideology of science = scientism).
theory-ladenness is the recognition that because of value-ladenness, the ostensibly raw facts we collect are always, at least to some degree, already filtered and interpreted. There is no such thing as completely objective, uninterpreted facts. This is a Kuhnian (post-positivist) and therefore Kantian viewpoint.
narrative (an alternative to naturalism and mechanism): whereas naturalism and mechanism attempt to explain subjective experience (i.e., value and meaning) using the third-person material & efficient cause perspective of physics, the assumption of narrative instead says that life can only be understood through the first-person structure of story.
purity criticism (an alternative to reification): purity criticism is map-territory criticism, and it is sought in at least two ways. First, there is a skepticism of received categories (abstractions; interpretations; assumptions), and specifically, a persistent attempt to ground them in concrete action & perception. Second, there is an attempt to constrain philosophy through science. Traditionally, it has worked more the other way around, but it's also possible to critique philosophical abstractions using our best scientific knowledge of evolution, the brain, body, and nervous system.
holism (an alternative to reductionism): wholes are more than the sum of their parts, which means they cannot be fully understood by decomposing them and studying their parts in isolation.
relationality (an alternative to atomism): the relations between things are just as real, and in fact prior to, the divisions between things. Relations are not secondary subjective add-ons to pre-existing, fixed objects, but rather, objects are abstractions from whole situations/events. Put another way, process precedes structure. This also means that there is no attempt to identify any abstracted feature of experience as exclusively (absolutely) an object, stimulus, response, etc., outside of a defined context. Where you choose to draw boundaries depends on your goals. It may be useful to consider something a stimulus (cause) from one perspective, but a response (effect) from another.
the natural theory of categories (an alternative to the classical theory of categories): Second-generation cognitive science research in the 1970s-1980s discovered—empirically—that our "natural" categories (i.e., the categories that we rapidly and unconsciously use) have a fuzzy structure (with a margin and center) and are not defined by formal logic (Lakoff, 1987). Natural categories are instead based on central tendency primarily related to contextual (functional, goal-directed) meaning & emotional valence, and only secondarily on objective sensory property. For example, if a fire starts in your kitchen, you will immediately start looking around for anything that could put it out (e.g., fire extinguisher, jug of lemonade, pot lid to smother the flames, open window to throw the burning item out of, etc). None of these things are objectively similar, but they all work the same way relative to your current goal, and thus all stand out in your field of attention. Wittgenstein called this "family resemblance". From this perspective, scientific (classical) categories are a derivative kind of category, not the fundamental kind.

Peterson noticed many different fields were all pointing toward the "same thing". More specifically, what he noticed is that all the following constructs are approximately the "same thing" (or slightly different aspects of the same thing) (?): contexts, parts/wholes, stories, narratives or myths, acts or activities, natural categories, affordances, tools, means/ends, and what the Kantian a priori categories anticipated. Here are even more "synonyms": the universal categories of thought, the innate concepts the rationalists were looking for, a frame, a structure of mind, a discrimination, a differentiation, a distinction, a unity, a representation of the thing-in-itself, a model, an understanding, an interpretation, a pattern, a chunk, a black box, a Jungian complex or archetype, the Heideggerian "Dasein", a presupposition, an assumption (as in "hidden assumptions"), an action-oriented representation, an object that the subject is already 'in', a theory, a perspective, an axiom, a cybernetic unit, a system, a triadic semiotic unit, a desired expectancy/prediction, an element of self (identity), a hypothesis, a plan, an event, a paradigm, a gestalt, a functional simplification of complexity, a habit, a structure that intermediates between fact and value, a belief, a game (i.e. a shared belief), a motivation-action-perception (MAP) schema, a map of meaning, or a sub-personality. In other words: a fundamental frame of reference.

Meaningful contexts: functional categories
What we've called abstractionism is the traditional philosophical search for context-independent, i.e., decontextualized, self-sufficient explanatory abstractions. In response, contextualism says that decontextualized abstractions (e.g., objects, facts, essences) are neither the most fundamental nor the only kind of abstraction. Instead, categorization or abstraction is most fundamentally about selecting what matters (is meaningful) for a certain goal-directed purpose (what's contextually relevant), while excluding what doesn't matter (what's contextually irrelevant). When you think about what something means, you necessarily think about it in relationship to some larger objective situation and/or your subjective goals — that is just how meaning works. This "natural" kind of categorization is functional or context-dependent categorization. Unlike the abstractionist attempt to pin down meaning as either fixed or nonexistent, under contextualism the key feature of meaning is its context-dependent mutability. And notice that another word for context is "whole". Thus, in contextualism the fundamentally real are parts of larger (functional) wholes.

Peterson (2013b) says:

"The objects and categories we use are neither things nor labels for things. Instead, “objects” are entities bounded by their [contextual] affective relationship to a goal. We perceive meaningful phenomena, not the objective world."

Meaningful contexts: ontological categories
Meaningful contexts or functional categories are therefore groups of phenomena (patterns of experience) segregated out through selective attention, i.e. formed by a co-constitution of "subject" & "object" (or knower/known, interpreter/interpreted). To be more specific, the category would be the perceptual or conceptual experience that results from the superimposition of the subject's top-down goal-bounded concept (motivated expectancy) upon the world's (metaphorical) bottom-up object or "thing-in-itself". (i.e., the Kantian constructivist formula: phenomenal experience = subject X noumenon). The key idea here is that contexts aren't "contained inside" or "after the fact of" experience like classical categories, but rather they constitute experience from the start.

Peterson (1999a) in fact explicitly claims that story (i.e. meaningful context or functional category) just is the structure of knowing-that memory, neuropsychologically (i.e., of our frames of reference — of our most fundamental representations):

". . . [knowing-that (representational) memory constitutes] . . . a permanent but modifiable four-dimensional (spatial and temporal) representational model of the experiential field, in its present and potential future manifestations. This model, I would propose, is a story."

and Peterson (2018b) says, in the same vein:

"For Jung, the Kantian a priori categories were personalities. The importance of this can hardly be overstated. Jung and the psychoanalysts insisted that the structure intermediating between facts and values was alive."

Peterson says:

"I should say this over and over and over: a value system is the precondition for perception itself."

This means that the frame problem is the meta-problem lurking behind all of these more specific philosophical problems:

The One & the Many: how a multiplicity becomes a unity; what makes things be of the same kind; what makes things similar or different to one another.
Being & Becoming: permanence among change (structure among process)
The problem of induction: the future will not necessarily be like the past.
The fact-value problem: how should facts be turned into values, i.e. how should relevant facts be selected from the infinite sea of facts?
David Hume's is-ought problem: the facts don't tell you what to do with the facts.
The problem of morality: how to act
The problem of perception: how perceived objects should be segregated from one another
The problem of postmodernism: potentially infinite ways to read a text or interpret a situation.
The quest for certainty: the impossible wish to transcend the anxiety inherent to human existence once and for all (i.e., to finally categorize experience once and for all).

Peterson argues that we are biologically & neuropsychologically adapted not to the objective world as defined by the categories of science, or even by the categories of traditional philosophy, but rather to the natural (meaningful) categories of the known (order), unknown (chaos), and knower (Logos).

Peterson further argues that the two hemispheres of the brain both morphologically and functionally reflect these mythological categories of known (order) and unknown (chaos) (1999a):

"One set of the systems that comprise our brain and mind governs activity, when we are guided by our plans—when we are in the domain of the known. Another appears to operate when we face something unexpected—when we have entered the realm of the unknown."
. . .
"The left hemisphere . . . seems at its best when what is and what should be done are no longer questions; when tradition governs behavior, and the nature and meaning of things has been relatively fixed . . ."
. . .
"The right hemisphere appears to come “on-line” when a particular situation is rife with uncertainty . . . [it] appears integrally involved in the initial stages of analysis of the unexpected or novel—and its a priori hypothesis is always this: this (unknown) place, this unfamiliar space, this unexplored territory is dangerous, and therefore partakes in the properties of [i.e. is in the same functional category as] all other known dangerous places and territories, and all those that remain unknown, as well."

And Iain McGilchrist (2019) helpfully elaborates on the same theme in The Master & His Emissary:

". . . each hemisphere brings into being a world that has different qualities. These could be characterised in the simplest possible terms something like this."

"In the case of the left hemisphere, a world of things that are familiar, certain, fixed, isolated, explicit, abstracted from context, disembodied, general in nature, quantifiable, known by their parts, and inanimate."

"In the case of the right hemisphere, a world of Gestalten, forms and processes that are never reducible to the already known or certain, never accounted for by dissolution into parts, but always understood as wholes that both incorporate and are incorporated into other wholes, unique, always changing and flowing, interconnected, implicit, understood only in context, embodied and animate."

"The left hemisphere is a world of atomistic elements; the right hemisphere one of relationships. Most importantly the world of the right hemisphere is the world that presences to us, that of the left hemisphere a re-presentation: the left hemisphere a map, the right hemisphere the world of experience [territory] that is mapped."

Not matter, but what matters
Peterson argues backwards from this convergence ("consilience") of evidence regarding myth, the brain hemispheres, and functional categorization, "reverse engineering" the implication for ontology—the fundamental metaphysical categories of the real. And the implication is this: the reality we are evolutionarily adapted to is not matter (the world of objects), but rather what matters (the world of meaning). He says (1999b):

"What if it were the case that human beings were adapted to the significance of things, rather than to “things” themselves? Wouldn't that suggest that the significance or meaning of things was more “real” than the things themselves (allowing that “what is adapted to” constitutes reality, which only means accepting as valid a basic implicit axiom of evolutionary theory: that the “organism” adapts to the “environment”)."

## Logos 

Logos is an active spirit—which includes the potential to become conscious of its own hidden assumptions. That is, Logos is capable of making explicit its own implicit contexts in Kantian transcendental fashion, criticizing them, and adapting by predicating alternative assumptions. This is what computers can't do.

Importantly, however, Logos must at some point have consciously predicated the assumption that its nature is to predicate assumptions. In other words, it must adopt the Kantian, Heideggerian, Kuhnian, Jungian, mythological, or pragmatic insight: that theory is always prior to data (values are always prior to facts; ought is always prior to is). Otherwise, Logos is forever doomed to unconsciously project its own highest value into the black boxes of objects and/or reason

## Morality 

Most importantly, what all this means is: reason and moral philosophy don't discover, invent, or prove values, but instead presuppose values. To say it again: any argument about how facts or reason get us from is to ought in an ostensibly value-free (objective) way presupposes what it purports to prove. What moral philosophy actually does is re-express implicit, embodied knowing-how wisdom on the more explicit (more abstract) knowing-that level. Moral philosophy is therefore a low resolution representation of our high-resolution central pattern of action. It's values all the way down, and all the way up. This is why Peterson can say:

Morality requires a metaphysics grounded in the mythic.

## Highest Value 

All action, perception, & cognition is 100% value-laden
Let's further specify the concept of value-ladenness.

In the abstractionist paradigm, values—like ideas in general—are metaphorically conceptualized as (reified as) thing-like abstract objects that exist "inside" the mind. Values are "held" (e.g., philosophical or religious principles). In other words, values are thought to exist "after the fact of" perception & cognition. This is an ontic conceptualization of value.

By contrast, value in the contextualist paradigm pertains to the whole knower from the start. This is an ontological conceptualization of value. From this perspective, consider that:

perception is motivated, selective (value-laden) abstraction from experience;
action is motivated, selective (value-laden) transformation of experience;
higher cognition (reason) is built on top of, and for, action & perception.
Therefore, from the contextualist perspective, all perception, action, and cognition is 100% value-laden. This means that another word for a frame of reference (context; functional category; schema of action & perception, sub-personality; etc) is: a value.

Because action & perception are 100% value-laden, merely living & acting already implies a value structure. That is, whatever we move toward—consciously or unconsciously—is what we value. The end point of every story is, by definition and by logical implication, more highly valued than the starting point. If it were not more highly valued, then we would not have moved toward it. Further, because belief is hierarchical, each of us is necessarily moving toward some highest goal—whether we know what it is or not. Therefore, each of us inescapably has a highest value—whether we are conscious of what it is or not.

## Truth 

Newtonian or Correspondence Theory of Truth 

Darwinian truth comes into play, which is almost the same thing as what Peterson & Pageau (2023) recently called subsidiary identity.

Basically, the OG pragmatists did have problems with normativity: they could never quite explain what prevents pragmatic truth from sliding into relativism, subjectivism, or a short-sighted utilitarianism. Peterson says that what they were missing is that "what works" has to be nested under the hero pattern (i.e. the meta-context), across the dimensions of both space & time. That is, a thing or fact is intrinsically good (true) in the sufficient-condition (Darwinian) sense just insofar as it fits into the subsidiary hierarchy. When this constraint is added, instrumentalism no longer degenerates into relativism.

As you ascend levels of abstraction in the category hierarchy, or move forward into the future, goals become more comprehensive: lower level stories (parts; means) serve higher level stories (wholes; ends), all the way from the tiniest unconscious actions & perceptions, up through everyday tasks, projects, careers, individual lives, families, functional hierarchies, societies, civilizations, humanity as a whole, and then hypothetically all the way up to the cosmic level (the greatest whole). The greatest possible truth condition is therefore when the entire cosmos is ordered truthfully in this functional part/whole manner, bottom to top. This is Darwinian truth or subsidiary identity. This psychic & cosmic (micro & macro; "as above, so below") hierarchy of value—this hierarchy of subsidiary identity—is what the people of the Middle Ages were intuitively apprehending, as was everyone who historically felt that teleological explanation was compelling.

The correspondence theory of truth is therefore nested under (subsidiary to) Darwinian truth in precisely the same manner that the abstractionist paradigm (world of objects) is nested inside the contextualist paradigm (world of meaning), or that the ontic is nested inside the ontological, or that fact is nested inside value, or that the left hemisphere (emissary) must not usurp the right hemisphere (master). It's all the same pattern.

If you get this pattern backwards, e.g., if you're a materialist whose highest god is objective reality, then of course you will reify the correspondence theory of truth and believe that truth is something entirely separate & different from ethics. In fact, to posit objective reality as a highest value just is an archetypal manifestation of the religious tendency to posit a "higher" realm—that is, the pattern of the sacred & profane (e.g., Plato's Being & Becoming, appearance vs. reality, subjective vs. objective, etc.). Eliade (1959) even explicitly associates the sacred with the modern concept of 'objective reality':

"Religious man's desire to live in the sacred is in fact equivalent to his desire to take up his abode in objective reality, not to let himself be paralyzed by the never-ceasing relativity of purely subjective experiences, to live in a real and effective world, and not in an illusion."

On the other hand, if your god is the ideal personality (which is the correct God to have), then the correspondence theory instead takes its rightful place as one powerful tool at your disposal in service of the highest truth—but not itself the highest truth.

## Hero Myth 

he only truly sufficient condition of truth is embodying the hero pattern. Simply put, correspondence is just not enough for something as important as a theory of truth. Truth can only be the whole truth when it includes ethics (action & perception) in addition to objective representation. Therefore, against Peterson's critics, to say something is "true" in the Darwinian sense is actually a much stronger (not weaker) claim than to say it's true in the correspondence (Newtonian) sense.

To put this all a slightly different way: science is a method for developing tools (means) to help us pursue our goals (ends), i.e., pragmatically predicting & controlling. That's all it is. It's only when we expect science to be capable of making truth claims (i.e. when we reify scientism and/or the correspondence theory of truth) that the correspondence theory seems to be so important (Slife, 2004b). When we recognize that science is just a method for making tools, then we no longer need to expect a method to produce truth for us. Truth doesn't come from repeatable method, but rather from active spirit—Logos's dialectical ability to predicate and re-predicate new values (categories) to act & perceive "for the sake of".

## Meaning 

Meaning: the telos of life
Consider, even further, that the hero pattern, the highest value, God, subsidiarity, truth, and the sacred are all also the "same thing" as the real (what matters). That is: the way you faithfully identify with the hero is by following meaning without any self-deception—thus staying on the narrow path and continually bringing about even deeper meaning. This is the upward spiral.

Peterson's claim is that meaning is our fundamental guide for how to perceive & act (i.e., how to categorize experience). He says:

"Meaning appears to exist as the basis for radical functional simplification of an infinitely perceivable world."

In other words: meaning is the built-in human "ought", in Aristotelian final cause (teleological) fashion. Meaning is what directs our attention to those aspects of the unknown that we are personally best suited to confront, because everyone has different interests & talents ('gifts of the spirit') and is oriented toward solving different problems. Meaning, in the hands of an apprenticed individual, when not distorted by chronic or acute self-deception, is the pointer toward both 1) personal interest (the symbolically feminine aspect of meaning; the Jungian "anima") and 2) personal responsibility (the symbolically masculine aspect of meaning; conscience). Therefore, meaning is what makes life worth living (the antidote to nihilism). Peterson (1999a) says:

"Interest is meaning. Meaning is manifestation of the divine individual adaptive path."
. . .
"Interest manifests itself where an assimilable but novel phenomenon exists: where something new hides in a partially comprehensible form." [i.e. a symbol]
. . .
"Loyalty to personal interest is equivalent to identification with the archetypal hero . . ."
. . .
"The problems that grip you are the portal to your destiny."

The highest form of meaning therefore emerges on the border of order and chaos, when we are exploring the unknown (recategorizing experience; solving the frame problem; identifying with the hero) in the correct manner (voluntarily and without deception) at the optimal rate (flow state), where interest & responsibility are balanced (on the narrow path).

The religious question is what must be the highest value. Since the unknown must be continually encountered for the cosmos to endure (i.e., the frame problem must be eternally solved and re-solved), true religion must therefore be faith in the process of creative exploration. Living a truly religious life is thus the "same thing" as identifying with the hero.

## Evolution 

The telos (?) of evolution
The ideal was both naturally and sexually selected for. Naturally, because the hero pattern is the most generally adaptive solution to every problem. Sexually, in the manner that Peterson (2018d) describes:

". . . the men all get together and vote on the good [i.e. heroic] men, and the good men are then chosen by the women, and those are the people who propagate. And so it’s like men are voting on which men get to reproduce, and women are going along with the vote, and being even more stringent in their choices. Then what you get is the consciousness that, through its acted expression, transforms the potential of the world into actuality and also selects the direction of evolution . . . And that’s where the meme—Dawkins's term—turns into the biological reality."

## Adversary Myth 

The second archetype of response to the unknown is the path of the adversary.

This is the path of deception (lying), and particularly self-deception. This is the process that exists in absolute opposition to Logos.

Self-deception means refusing to undergo the emotionally demanding, exploratory descent into chaos when the frame problem re-presents itself (i.e. when you fail to attain your goals). In other words, it means continuing to impose no-longer-functional categories on the world. Peterson (2001) says:

"Category systems that impose themselves on the world, regardless of the world, do not make the world conform, merely because of their imposition – at least not for long."

As categories crumble, the self-deceptive archetype creates a positive feedback loop that spirals down: error → self-deception → categorical instability → difficulty of correction increases → incentive to continue self-deceiving increases → reification of existing assumptions is reinforced → repeat. In other words, "doing what already failed but even harder this time." (This is the opposite of the negative feedback loop of the hero pattern, which instead spirals up and binds ever more tightly onto the narrow path.) In this way, it eventually becomes necessary to use manipulation and force on others to artificially prop up the collapsing situation.

Since your category system constitutes your very thoughts and perceptions, automatizing self-deceptive beliefs (i.e. habitual lying) gradually reconfigures your value structure in a way that you can't detect — because you cannot stand radically apart from yourself. This means that if you continually lie to yourself, you will eventually corrupt your ability to detect your own corruption.

Peterson argues that not knowing your telos necessarily (inevitably) leads to adoption of the adversarial personality, because life without meaning is intolerable.

When the self-deceptive archetype gains control over a whole society or civilization, the result is a demonic rebellion against God, a descent into hell, and a flood.

## Conclusion 

To summarize all this as plainly as possible:

The good is the true, and the true is the good; they are the same thing.
The battle between good & evil is the battle between truth and self-deception (both commission & omission) in one's own psyche.
The primary reasons for mental illness and its collective equivalent are self-deception and nihilism.
The choice between hero & adversary is the choice between 1) remaking the self according to the ideal vs. 2) deconstructing the ideal to avoid remaking the self.
Peterson (2007) says:

"If the world of experience is made of chaos and order, then the choice between the path of Cain and the path of Abel is the most important choice that anyone can ever make. If everything is merely material, by contrast, the choice does not even exist."

The implicit meta-context
If we're really operating in the evolutionary/mythological/religious context, and if the ideal is really built into us at every level of our being, then Logos is necessarily the meta-context (context of contexts) against which all perceptions, value judgments, thoughts, and actions are implicitly made, no matter what you say you're doing. This is true even if you're an atheist, a secularist, a neo-communist, an empiricist putting it into the black box of "objects", or a rationalist putting it into "rationality". Everything depends on prior value judgments, all the way up the hierarchy of contexts. The only reason we don't notice this is because our deepest presuppositions are the most invisible.

Making the implicit explicit
On the first page of Maps of Meaning, Peterson quotes the Bible verse Matthew 13:35:

"I will utter things which have been kept secret from the foundation of the world."

This is not some grandiose statement, but rather very seriously what the book does. The hero myth is the deepest, most implicit context by which we do & must live, and Maps of Meaning makes it explicit. It brings to light the nature of the a priori interpretive structure. It brings our self-representation further up the chain of abstraction, on par with our current level of consciousness and scientific knowledge. It makes the invisible, visible. Jung would call this "making the unconscious conscious."

Abstractionism vs. contextualism: summary

Idealism
All of this implies that both metaphysical and epistemological idealism are true.

Metaphysically, all reality is made of consciousness. There is only one ontological primitive (the real), alternately called: experience, meaning, mind, subjectivity, story, the unknown, chaos, the absolute, the sacred, the totality, the One, the cosmic whole, psyche, God, etc (?). (Because the other two constituent elements of experience—knower & known—originally arise from the unknown, they are therefore ultimately the same in ontological kind.) So, no matter what word you use, you're talking about the "same thing." As Kastrup (2014) says: ". . . God is the unifying experiential ﬁeld at the ground of all reality, including ourselves."
Epistemologically, there is a qualitative transition between the absolute and how we as limited beings perceive the absolute (knower dependence). This is akin to Kant's transcendental idealism, except in this case, the noumenon is also phenomena (i.e. the metaphorical "thing-in-itself" is also made of consciousness). The transition is in the qualitative nature of consciousness, not in ontological kind (Kastrup, 2014).
That is to say: "matter" is not a separate & different reality on par with, and dualistically opposed to, mind. As Kastrup (2019a) points out, matter is not on the same level of explanatory abstraction as mind. Rather, matter is an intersubjective abstraction from mind. This means that the "hard problem of consciousness", explaining mind in terms of matter, was never a genuine problem to begin with (Kastrup, 2014).

Importantly, this kind of idealism does not in any way mean that what is conventionally conceptualized as the objective is a mere fantasy or can be gerrymandered or socially constructed away. What it does mean is that:

Our experience is inescapably embodied and therefore knower-dependent (which is not the same thing as "purely idiosyncratic" or "purely at the whims of our individual subjective wills");
Just because we can conceptually reify abstractions of consciousness as self-sufficient entities like an "objective realm" or "matter" and make useful pragmatic inferences using those metaphors, does not mean that they actually have a literal, separate existence;
And it also means that when physicists believe they are studying an independent physical reality outside of themselves, what they are actually studying is their perceptions of the shared portions of experience that we don't identify with—and leaving the implications of that implicit (unconscious; unaccounted for; projected) (Kastrup, 2019a).
Rule #8
In my opinion, Peterson's rule #8: "Tell the truth – or, at least, don't lie" is the cornerstone of his (and Jung's) ideas. This is the rule that ultimately subsumes everything else.

The person "telling himself the truth" (the whole truth) necessarily just is confronting chaos, is solving the frame problem, is acting out the hero pattern, is balancing interest & responsibility, is pursuing the meaning of life, is aligned with the highest value, is encountering the sacred, is participating in subsidiary identity, is "living his life as Christ lived his", is on the narrow path, is working for the good, is rescuing his father from the belly of the whale, is curing himself. It is all the same thing.

This is the deepest assumption (highest value) that we must all consciously predicate and act & perceive "for the sake of". This is the pattern that reconciles all dialectics. This is the true "categorical imperative".

When you figure out what Jung & Peterson are circumambulating, it is an epiphany experience and a mental revolution. Peterson (2014) says:

"[A] hallmark of truth, is that it snaps things together. People write to me all the time and say that: 'It's as if things were coming together in my mind.' It's like, well, that's what archetypes do. Archetypes glue things together."

The revolution is understanding that the secret of life is simply not lying, following meaning, and honestly re-evaluating means & ends when your categories fail. In this way, you will find out where meaning takes you in the cosmic part/whole hierarchy (the adventure of life). This is your telos. This puts you in touch with what is most real.

The truth is not some impenetrable academic idea. Scholars and scientists don't have privileged access to it. Living in truth simply means telling the truth to others, and especially to yourself, as best you can. Anyone can do it.

This is the one narrative that unifies in a bottom-up manner without any top-down coercion, and reconnects us to the source of our civilization. The truth fixes this.



