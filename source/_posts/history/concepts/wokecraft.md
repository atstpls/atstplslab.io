---
title: wokecraft
date: 2023-04-11 10:05:25
tags: [history]
categories: [ecosystem, events]
---

<br>

Each woke person has been hardened:

1) They reason using the woke language          ( instead of a sell line, it's a sell language designed to be deceitful )
2) They resist the fact they've been duped      ( it's easier to fool someone than convince them they've been fooled )

Therefore, only solution:

1) Be precise with specific examples            ( woke language obscures ideology, precise language exposes it )
2) Never try to convince, only plant the seed   ( give information only, they will do the math eventually )  


<br>

Two broad principles of applied post-modernism
1. Knowledge principle - Knowledge is socially constructed and culturally contingent
2. Political principle - There are power dynamics aasociated with the social constructions, creates a political bias that must be corrected for, oppressor class can't be disinfranchised


The postmodern knowledge principle insists that objective knowledge is not possible and favors specialized “knowledges” that arise from the lived experience of individuals of a certain identity, positioned in a specific way by society. The postmodern political principle is, in essence, a call to identity politics, which requires adopting an identity as part of some marginalized group or being assigned to a relatively privileged one. This is supported by the postmodern theme of fragmenting the universal and replacing the individual with the group. 

foundational postmodern principles held that objective knowledge is impossible, that knowledge is a construct of power, and that society is made up of systems of power and privilege that need to be deconstructed. As we discussed in chapters 2–7, this view was made actionable in the applied phase in the 1980s and 1990s, which saw postmodernism fragment into postcolonial Theory, queer Theory, critical race Theory, intersectional feminism, disability studies, and fat studies. Subsequently, especially since 2010, these postmodern ideas have become fully concretized in the combined intersectional Social Justice scholarship and activism and have begun to take root in the public consciousness as allegedly factual descriptions of the workings of knowledge, power, and human social relations.

The result is that the belief that society is structured of specific but largely invisible identity-based systems of power and privilege that construct knowledge via ways of talking about things is now considered by social justice scholars and activists to be an objectively true statement about the organizing principle of society. Does this sound like a metanarrative? That’s because it is. Social Justice scholarship and its educators and activists see these principles and conclusions as The Truth According to Social Justice—and they treat it as though they have discovered the analogue of the germ theory of disease, but for bigotry and oppression.

Standpoint theory operates on two assumptions. One is that people occupying the same social positions, that is, identities—race, gender, sex, sexuality, ability status, and so on—will have the same experiences of dominance and oppression and will, assuming they understand their own experiences correctly, interpret them in the same ways. From this follows the assumption that these experiences will provide them with a more authoritative and fuller picture. The other is that one’s relative position within a social power dynamic dictates what one can and cannot know: thus the privileged are blinded by their privilege and the oppressed possess a kind of double sight, in that they understand both the dominant position and the experience of being oppressed by itStandpoint theory operates on two assumptions. One is that people occupying the same social positions, that is, identities—race, gender, sex, sexuality, ability status, and so on—will have the same experiences of dominance and oppression and will, assuming they understand their own experiences correctly, interpret them in the same ways. From this follows the assumption that these experiences will provide them with a more authoritative and fuller picture. The other is that one’s relative position within a social power dynamic dictates what one can and cannot know: thus the privileged are blinded by their privilege and the oppressed possess a kind of double sight, in that they understand both the dominant position and the experience of being oppressed by it

Roughly, the idea is that members of dominant groups experience a world organized by and for dominant groups, while members of oppressed groups experience the world as members of oppressed groups in a world organized by and for dominant groups. Thus, members of oppressed groups understand the dominant perspective and the perspective of those who are oppressed, while members of dominant groups only understand the dominant perspective. Standpoint theory can be understood by analogy to a kind of color blindness, in which the more privileged a person is, the fewer colors she can see. A straight white male—being triply dominant—might thus see only in shades of gray. A black person would be able to see shades of red; a woman would be able to see shades of green; and a LGBT person could see shades of blue; a black lesbian could see all three colors—in addition to the grayscale vision everyone has. Medina refers to this as a “kaleidoscopic consciousness” and “meta-lucidity.”25 Thus, having oppressed identities allows extra dimensions of sight. This gives the oppressed a richer, more accurate view of reality26—hence we should listen to and believe their accounts of it.

They generally get around this accusation by arguing that the theory does not assume all members of the same group have the same nature but that they experience the same problems in an unjust society, although they can choose which discourses they wish to contribute to. Members of these groups who disagree with standpoint theory—or even deny that they are oppressed—are explained away as having internalized their oppression (false consciousness) or as pandering in order to gain favor or reward from the dominant system 


Instead of science, Social Justice scholarship advocates for “other ways of knowing,” derived from Theoretical interpretations of deeply felt lived experience. It argues that reason and evidence-based knowledge are unfairly favored over tradition, folklore, interpretation, and emotion because of the power imbalances baked into them. Without the slightest awareness of the racist and sexist implications, Theory views evidence and reason to be the cultural property of white Western men.

knowledge is constructed in the service of power, which is rooted in identity, and that this can be uncovered through close readings of how we use language. Therefore, in Social Justice scholarship, we continually read that patriarchy, white supremacy, imperialism, cisnormativity, heteronormativity, ableism, and fatphobia are literally structuring society and infecting everything

all white people are racist, all men are sexist, racism and sexism are systems that can exist and oppress absent even a single person with racist or sexist intentions or beliefs (in the usual sense of the terms), sex is not biological and exists on a spectrum, language can be literal violence, denial of gender identity is killing people, the wish to remedy disability and obesity is hateful, and everything needs to be decolonized

two postmodern principles: knowledge is a social construct, and society consists of systems of power and privilege.

it is common to address negative attitudes towards obesity alongside racism, sexism, homophobia, transphobia, disableism, and imperialism, even though there is strong evidence that obesity is a result of consistently consuming more calories than are needed and carries significant health risks

emphasizing the value of health is cast as a problematic ideology called healthism.

Healthism is bolstered by nutritionism, which is an allegedly excessive focus on the relevance of the nutritional value of foods to the study of nutrition and dietetics (diet and its impacts on health).

Sex gender sexuality are artificial categories not based on biological reality, but wholly on the way we talk about them.

It asserts the objective truth of socially constructed knowledge and power hierarchies with absolute certainty

Oppression flows from categorization which arises when language constructs a sense of what is normal

post colonialism and queer theory have a solid underlying point
- we have changed the way we see sexuality profoundly
- many legal and cultural barriers have been removed

Homosexuality was something you did, now it's who you are 

claim is LGBT (lesbian, gay, bisexual, and trans) identities are stable categories
LGBT statuses are social constructs built by the powerful in the service of dominance and oppression?

challenge what is called normativity—that some things are more common or regular to the human condition, thus more normative from a social (thus moral) perspective, than others. The main industry of queer Theorists is to intentionally conflate two meanings of “normative,” and deliberately make strategic use of the moral understanding of the term to contrive problems with its descriptive meaning. Normativity is considered pejoratively by queer Theorists and is often preceded by a prefix like hetero- (straight), cis- (gender and sex match), or thin- (not obese). By challenging normativity in all its manifestations, queer Theory therefore seeks to unite the minority groups who fall outside of normative categories under a single banner: “queer.” This project is understood to be liberating for people who do not fall neatly into sex, gender, and sexuality categories, along with those who wouldn’t if they hadn’t been socialized into them and weren’t constrained by social enforcement. It produces a de facto coalition of minority gender and sexual identities under the appropriately unstable set of acronyms that tend to begin with LGBTQ

adical distrust of language and would violate its ambition to avoid all categorization, including of itself

 “queer” as “whatever is at odds with the normal, the legitimate, the dominant. There is nothing in particular to which it necessarily refers. It is an identity without an essence.

  resists categorization and distrusts language,
  resistant to definition in the usual sense, but also to functional definitions based on what it doe
  its definitional indeterminacy, its elasticity, is one of its constituent characteristics.”10 The incoherence of queer Theory is an intentional feature, not a bug

Nearly everyone accepts that some combination of human biology and culture comes together to create expressions of sex, gender, and sexuality. As evolutionary biologist E. O. Wilson states, “No serious scholar would think that human behavior is controlled the way animal instinct is, without the intervention of culture

 overwhelming proportion of Homo sapiens are either male-or female-sexed and that gender expression in humans is overwhelmingly bimodal in nature and strongly correlated with sex

 knowledge is socially constructed by discourses, in the service of power—and was particularly concerned with “biopower”—how the biological sciences legitimize the knowledge that the powerful use to maintain their dominance

 unjust power is everywhere, always, and it manifests in biases that are largely invisible because they have been internalized as “normal.”19 Consequently, speech is to be closely scrutinized to discover which discourses it is perpetuating, under the presumption that racism, sexism, homophobia, transphobia, or other latent prejudices must be present in the discourses and thus endemic to the society that produces them. (This is circular reasoning.)

 language, as the means by which power disguised as knowledge infiltrates all levels of society and establishes what is accepted as normal.

the idea that heterosexuality is a social construct completely neglects the reality that humans are a sexually reproducing species. The idea that homosexuality is a social construct neglects the plentiful evidence that it is also a biological reality. 

It does not tend to make for productive activism to be dismissive, ironic, antiscientific, and largely incomprehensible by design. It also doesn’t help people who wish to have their sex, gender, or sexuality accepted as normal to be continually rescued from any sense of normalcy by arguing that considering things normal is problematic.

Critical race Theory holds that race is a social construct that was created to maintain white privilege and white supremacy

The word critical here means that its intention and methods are specifically geared toward identifying and exposing problems in order to facilitate revolutionary political change. 

overarching idea of intersectionality reduces everything to one single variable, one single topic of conversation, one single focus and interpretation: prejudice, as understood under the power dynamics asserted by Theory. Thus, for example, disparate outcomes can have one, and only one, explanation, and it is prejudicial bigotry. 


ttempting to “respect” all marginalized identities at once, as unique voices with the inherent, unquestionable wisdom connected to their cultural groups, can produce conflict and contradiction.
 keeps intersectionalists busy, internally argumentative, and divided, but it is all done in the service of uniting the various Theoretically oppressed groups into a single meta-group, “oppressed” or “other,” under an overarching metanarrative of Social Justice

Always believing that one will be or is being discriminated against, and trying to find out how, is unlikely to improve the outcome of any situation. It can also be self-defeating. In The Coddling of the American Mind, attorney Greg Lukianoff and social psychologist Jonathan Haidt describe this process as a kind of reverse cognitive behavioral therapy (CBT), which makes its participants less mentally and emotionally healthy than before.60 The main purpose of CBT is to train oneself not to catastrophize and interpret every situation in the most negative light, and the goal is to develop a more positive and resilient attitude towards the world, so that one can engage with it as fully as possible. If we train young people to read insult, hostility, and prejudice into every interaction, they may increasingly see the world as hostile to them and fail to thrive in it.

The core problems with critical race Theory are that it puts social significance back into racial categories and inflames racism, tends to be purely Theoretical, uses the postmodern knowledge and political principles, is profoundly aggressive, asserts its relevance to all aspects of Social Justice, and—not least—begins from the assumption that racism is both ordinary and permanent, everywhere and always. 

Consequently, every interaction between a person with a dominant racial identity and one with a marginalized one must be characterized by a power imbalance (the postmodern political principle). The job of the Theorist or activist is to draw attention to this imbalance—often described as racism or white supremacy—in order to begin dismantling it. It also sees racism as omnipresent and eternal, which grants it a mythological status, like sin or depravity.64 Because the member of the marginalized racial group is said to have a unique voice and a counternarrative that, under Theory, must be regarded as authoritative to the degree that it is Theoretically “authentic” (the postmodern knowledge principle), there is no real way to dispute her reading of the situation. 

 In scholarship, this leads to theories built only upon theories (and upon Theory), and no real means of testing or falsifying them. Meanwhile, adherents actively search for hidden and overt racial offenses until they find them, and they allow of no alternative or mitigating explanations—racism is not only permanently everywhere and latent in systems; it is also utterly unforgivable. This can lead to mob outrage and public shamings, and it tends to focus all our attention on racial politics, which inevitably become increasingly sensitive and fraught.

  It is bad psychology to tell people who do not believe that they are racist—who may even actively despise racism—that there is nothing they can do to stop themselves from being racist—and then ask them to help you. It is even less helpful to tell them that even their own good intentions are proof of their latent racism. Worst of all is to set up double-binds, like telling them that if they notice race it is because they are racist, but if they don’t notice race it’s because their privilege affords them the luxury of not noticing race, which is racist

  diversity theorists They want a shift toward “mutual respect” and “affirmation of difference,” that is, a sense of solidarity and allyship among marginalized groups.35 Note that this is a respect for differences between social and cultural groups—not for individuals with different viewpoints. They do not defend the right to express different ideas, but affirm the value of those ideas that are marked out as belonging to certain groups. This requires cultural relativism and standpoint theory—the view that belonging to a marginalized group provides special access to truth, by allowing members insight into both dominance and their own oppression.

new Theory is actually overly simplistic—everything is problematic somehow, because of power dynamics based on identity. It is also functionally impossible, a characteristic that is misinterpreted as extreme complexity.

one’s status as privileged is assessed intersectionally, using the appropriate applied postmodern Theories. This attempt to flip the script by strategically redefining the absence of discrimination and disenfranchisement as unjust and problematic has arguably been a catastrophe for left-leaning politics throughout the developed world.

As intersectionality developed and became dominant in both mainstream political activism and scholarship, it became increasingly common to hear that “straight, white, cisgendered men” were the problem. F

1.Gender is highly significant to the way power is structured in society;

2.Gender is socially constructed;

3.Gendered power structures privilege men;

4.Gender is combined with other forms of identity, which must be acknowledged and that knowledge is relative and attached to identity.

This analytical framework has had some benefits. It complicated the simplistic radical and materialist feminist metanarratives—in which women were an oppressed class and men their oppressors—by recognizing that power does not work in such a simple and intentionally binary way. This opened the door to a more nuanced analysis.

Finally, the attempt to make all analyses of gender intersectional, to focus relentlessly on a simplistic concept of societal privilege, rooted overwhelmingly in identity (and not in economics) and to incorporate elements of critical race Theory and queer Theory, results in a highly muddled, Theoretical, and abstract analysis that makes it difficult—if not impossible—to reach any conclusions other than the oversimplification that straight white men are unfairly privileged and need to repent and get out of everyone else’s way



 ##### Truth

 different sexualities exist naturally and some of them have been unfairly discriminated against.

##### occupying contradictory realities at the same time

 Sedgwick is saying that a productive movement could incorporate all the ideas to be found in LGBT scholarship and activism—even mutually contradictory approaches—without needing to resolve ideological differences. She argues that the contradictions themselves would be politically valuable, not least because they would make the thinking behind the activism very difficult to understand and thus to criticize. This is, of course, very queer.



- language creates the categories, enforces them, and scripts people into them
- boundaries are arbitrary, oppressive, and can be erased by blurring them 
- subvert or reject anything considered normal and innate in favor of the queer
- queer values incoherence, illogic, and intelligibility
- it is obscure by design and largely irrelevant, except to itself


##### Objective Reality

- societial systems of power and privilege construct all knowledge is objectively true
- social construction and oppression of identity is objectively true
- power hierarchies decide what can be known and how

##### Weaponize over-reactions

- social injustice is caused by legitimizing bad discourses
- social justice can be achieved by delegitimizing them and replacing them with better ones
- 

##### Blur Boundaries

- Blur boundaries and categories that previous thinkers widely accepted
- Complicate and problematize to deny such categories any objective validity
- Use this to disrupt systems of power that may exist across them

<br>

##### Language Deconstruction

- language is believed to have enormous power to control society and how we think
- discourses create and maintain oppression so monitor and deconstruct them
- look for internal inconsistencies (aporia) in which a text contradicts and undermines itself and its own purposes when the words are examined closely enough
- Deconstruct words in order to deliberately miss the point

<br>

##### Weaponize Criticism

- Criticism and skepticism about objective truth helps build new culture
- Objective knowledge was constructed by current culture to wield power
- Dismiss criticism from any position as delusional or the realities of oppression
- Cultural critique is hopeless except for marginalized or oppressed who weaponize it

<br>

##### Help is Really Hate

- Scholars and activists in these fields insist instead that the understanding of disability or obesity as a physical problem to be treated and corrected where possible is itself a social construct born of systemic hatred of disabled and fat people

<br>


##### Individuals Don't Exist

- The individual is a product of powerful discourses and culturally constructed knowledge
- Concept of universal is naive and an attempt to enforce dominant discourses on everybody
- Focus on small, local groups as the producers of knowledge, values, and discourses
- Rely on people who are understood to be positioned in the same way—by race, sex, or class, for example—and have the same experiences and perceptions due to this positioning
- knowledge claims and values of all cultures are equally valid and intelligible only on their own terms, and that collective experience trumps individuality and universality
- 
<br>

##### Critique and Disrupt

- describe the world critically in order to change it
- objective reality cannot be known, truth is socially constructed through language games
- truth is local to a particular culture, and knowledge functions to protect and advance the interests of the privileged
- critically examine discourses,to expose and disrupt the political power dynamics
- convince people to reject them and initiate an ideological revolution

<br>



NEGATIVE THEOLOGY - That which is greater than that, not that, that but also that

Politics pretending to be science 

Remove conditions of first reality (negate the real) to impose the fake 

Find ways to make reality negligible or evil so you can impose the fake

Inverse dialectic is saying your thing is same as mine but yours is all wrong (without establishing what theirs is)

Negate the truth, so they can install the artificial 

BLUE PILL - they tell you reality from your senses is wrong, and their way to interpret the world

Replacing your reality of your experience with a second reality of imaginative construction

Happens over and over, they die and come back, old as the Bible, Geneses 3 tells that story, 

Everything is a social construction, same language, same concepts, all throughout history

Love thy neighbor

Communism is the riddle of history to be solved, and it knows itself to be the solution >  Gnosticism > Gnosis

Project thing into the future that you want, you simulate it, you force people until it's socially constructified, reified, and is real/actual

Trajectory/negative thinking from Hegel/Marx to Critical Theorists.

Max Horkheimer, crated critical theory, one cannot determine what is good, what a good, free society would look liek from within the society

Criticize every aspect til you hate it and it destroys

Normal brain > bad results > makes adjustments to mental model > improves

Woke brain   > bad results > makes adjustments to mental model 

Feiere father of woke, conscientize, be aware, denounce without saying what's better, not that, not that either


critical constructivist 

--------------------------------------------------------------------------------------------

WOKECRAFT 

--------------------------------------------------------------------------------------------

Ideological movement taking control of western institutions such as education and universities 

Woke or Critical Social Justice - systemic power dynamics that order society and create stratifications like upper and lower classes

Uses a Marxian flavor, tries to change from within, uses subtle strategic manipulations of language and occupying positions of influence and policymaking 


Woke worldview encompasses CRT, Queer Theory, Post-colonial Theory and Fat Studies 

Knowledge is socially constructed by oppressor groups and helps maintain oppressive roles 

Knowledge is biased and can't be accurate representation of reality

Different cultures have different worldviews, none is authoritative than any other

worldviews are epistemically equivalent 

so logic, argument, evidence, hypotheses, controlled experiments, serve to perpetuate oppression 

Principle: individuals defined by their group identity ( oppressor or oppressed based on identified group )

people behavior is a function of group identity, perpetuates oppressive systems unconsciously 

Individuals are responsible for actions other individuals in their group does, this is valid across time

moral necessity to oppose oppression and epistemic skepticism lead to activism 

antiquated worldviews and flawed models

if you're not actively opposing, your are complicit in oppression

Challenge every interaction, problematize it, oppose it

Oppression is characteristically seen as aggregate and on a continuum. Matrices of oppression used to evaluate given individual suffers to their overlapping group membership

Continuum of privilege and oppression

Equity - just redistribution of resources according to group identity to redress historical imbalances


Woke Proximate - agree broadly with DEI initiatives such as affirmative actions and reparations 



WOKE: A CULTURE WAR 

Woke supposed to advance equity

Equity - an administered political economy in which shares are adjusted so that citizens are made equal
It's exactly like socialism.  Only thing different is type of shares.
Redistribute social and culture capital in addition to economic and material capital.

Woke is Maoism with American characteristics. Maoism is Marxism and Leninism with Chinese characteristics.

Woke is Marxism

Marxism is genus of ideological thought
species:
- classical marxism
- radical ffeminism
- critical race theory
- queer theory   - queer is an identity without an essence, an identity that is strictly oppositional to the concept of the normal 
- post-colonial theory

Binded together by intersectionality.
People can come together and channel resentment and try to claim power

Marx often talked about economics, but he was a theologian.
His hypothesis is must sieze means of production to bring socialism to the world
It's not just about human capital, economic capital.
Marx says man is incomplete, his social nature is forgotten, created by economic conditions and society 
History is made by man, which is driven by economic activity,

Marx's inversion of praxis
Economic production doesn't just produce goods and services, it produces society itself, and society produces man

Seize means of production ( how we construct ourselves as human beings )
So we can remember we're social beings and have socialist society, perfect communism that transcends private property

Marx wanted to understand and control how man produces himself, and decided it was economic production

Said economy was material determinism
Society is organized by private property, all our thoughts are driven by private property

Private property causes classes where one oppresses and exploits another
Awaken the proletariat and have them revolt to transform society
Communism is the abolishment of private property 

Marxism is always seize means of control of the production of man, history, and society.
Then it was economic means, now it is socio-cultural means

marxism               - people who have property and people who don't
                      - they control economic means of production 
                      - they have control of society

critical race theory  - white privilege is property and must be abolished
                      - they control of cultural means of production
                      - they have control of society 

queer theory          - people who set norms and people who don't
                      - they control normal cultural means of production 
                      - they control society 

post-colonial theory  - West is oppressor
                      - they control material and cultural wealth of the world
                      - they have control of society, remove every aspect of Western culture
                      - decolonize the curriculum, remove cultural significance of artifacts because they are offensive to us 



After 1917 and Europeans didn't follow
Gramasci, Lukacs wrote about Cultural Marxism aka Western Marxism 
Need to change culture and change from within 

Germans from Frankfurt school evolved (Horkhiemer) created Critical Marxism ( Critical Theory )
Marx was wrong, capitalism allows worker to build a better life
American universities adopted these German professors
They said capitalism delivers the goods, make workers comfortable happy, no longer our base of revolution
Opened the door for marxists to cooperate with corporations
Working class is irrelevant, the energy is in the outsiders: 
racial minorities, sexual minorities, feminists, etc. have energy for marxist revolution 

Studied the culture industry for 30 years, seized the means of production of the culture industry
Transform culture to sell racial, sexual, gender, 
Terms like cultural appropriations, cultural relevance, 


WOKE - identity-based Marxism
They unite around the idea that they are liberation from oppression
Think they're nations, they all have flags, hang them on buildings and streets 

Western Marxism
use Maoist approach, evolved to attack West at weakest points through our best traits
tolerance, acceptance, openness, generosity

Mao knew identity politics
- created 10 identities ( 5 red communist, 5 black fascist )
- social pressure for youth to identify as revolutionaries ( red communist )
- youth led revolution because of this

If you're white you're bad.. if you're gay you will be celebrated and supported 
Radicalize youth in U.S. same as in China but with different identity categories

The point is to destroy Western Civilization from within using Maoist techniques

Mao in 1942, strategy for transforming China is Unity, Criticize, Unity 
1. Try create desire for unity
2. Criticize people for not living up to that
3. Bring them into unity under a new standard

Only words have changed... inclusion is the goal, but you're racist, fix that and then you can belong 


### WOKE MICROTACTICS

- Subverting Liberal Decision-making
  - woke insistence on the informal (flexibility in an attempt to avoid bureaucratic)
                                    (but they will use formal when ends justify means)
  - enmity towards secret ballot voting (makes it easier to bully)
  - running down clock (delaying discussions)
  - recruiting woke allies (numbers used, justified with diversity/inclusion)
  - emphasizing emotion/experience (feelings, oppression, sympathy)
  - demand charity but don't extend it (abandon good faith in discussions)
- woke bullying tactics
  - making things awkward (not letting things go, forced consensus)
  - ad hominem attacks (complicit with oppression, preserving privilege,)
  - assume guilt, expect proof of innocence
  - intentional misinterpretation 
  - using consensus as coercion ( disagreement is seen as being devisive )
  - piling on (struggle sessions)
  - canceling/deplatforming
- Reverse Motte & Bailey Trojan Horse
  - motte - reinforced tower on a mound that is easy to defend, but unpleasant to stay in
  - bailey - courtyard below motte surrounded by protected ditch, less secure, more pleasant
  - person argues for extreme position (bailey), but when challenged retreats to motte
  - BAILEY:  Whites are irredeemably racist
  - MOTTE:  What you don't think racism exists?
  - False equivalency fallacy of both claims
  - Trojan horse is when a term is inserted for the motte, then used to get there by dual meaning
- telegraph, project, and subvert/invert
  - delegitimize original argument against racial hiring practices
  - invert argument by advocating for something they accuse opponent for advocating (legitimizing racism)
  - imply that was intention (projecting)
  - advocate against proposal (subversion)
- Others
  - moral hubris and high ground 
  - always support woke ally
  - obfuscation with technical jargon
  - DEI as cover for recruiting woke participants
  - dramatic departure
  - passive agressive opposition
- summary
  - subterfuge, exaggerating support & quelling dissent
  - subterfuge
    - crossover words
    - dog whistles
    - emphasizing emotion/experience
    - subverting liberal decision-making
    - passive-aggressive
    - motte of reverse motte-bailey trojan horse
  - exaggerating support
    - active recruiting allies
    - staking out moral high ground
    - bailey of reverse motte & bailey trojan horse
    - supporting woke allies
    - using technical jargon
    - dramatic departure
  - Quelling dissent
    - forceful bullying
    - ad hominem
    - consensus as coercion
    - piling on
    - canceling/deplatforming
    - firing
  

Projection (victim, rescuer, persecutor) is a tool which protects us from having to take personal responsibility and look at ourselves. The problem is always “out there,” “in them,” “over there,” but never “in me.”


-----

International relations through mutual benefits and cooperation rather than national self-interest

------------------------------------------------------------------------------------------------------------
Know your enemy, new communist movement

Discernment, discern linguistic lies and manipulation meant to pervert institutions 

Then discerningly apply it

neo-marxist - power structure is everything
Identity marxism - disability, gender, race, 

public-private partnerships - fascism or corporatism - corporate facism 

stakeholder model vs shareholder model 

crackpot socialism, disguised as science 

power dynamics that structure society (systemic racism), impacts of those determine the character of people

-----------------------------------------------------------------------------------------

build adaptive confidence 
possible gay is genetic flaw, we don't understand it so we say it's normal
sexual orientation is discrimination
morally dangerous texts, they hate art because it pushes past propaganda
perceive with obsessive, moralistic, identitarian lens
problematize, moral detectives, arrogance, reshaping to fit ideology
humanistic, anti-science movement
generate conceptual schemes to deal with the complexity of the world
critical social justice movement is like fundamentalist religion
moral purity, in their world to deviate in any way is unacceptable
framework is simple to put them in power, can't acknowledge flaws
dark triak - left is just as susceptible to corruption as right
no evidence a hyper-radicalized focused first and foremost on group identity will make a less racist society 
canada mandating highly contested policies simply based on beliefs
infantized world, academics build activests, applying untested theories
framework is simple to put them in power, can't acknowledge flaws
woke substitute self for God
Immature, unsocialized, emotional whims, incapable of developing a shared frame of reference
alienated, no friends, no development, aggressive
unsocialized, immature motivational and emotional demands, with tantrums and insistence that whims be granted
love to problematize, impose on liberties
they won't reason, just regurgitate ideology
once we understand it, was it so good and natural?
Catholic leans toward authoritarianism, Protestant lean towards individualism
shallow premises and deep premises, freedom of speech is built on by many other
focus on problem solving deescalates
corruption, incompetence, negligence is money problem, use bitcoin
collusion, capture, complacency is product choice problem, use choice
censorship is a technology problem, use decentralized 


--------------------------------------------------------------------

Struggle sessions (psychological torture through social channels) 
  - used by Mao in thought-reform prisons, schools, 
  - achieve new unity on a new basis (unity, criticism'struggle, new unity)
  - extreme guilt, shame, psychosocial pressure on the target
  - initiate pressure from target's friends/social circles
  - extract confessions to imaginary crimes
  - 

  I see you're at the top of your value structure

  The more responsibility you have, the more meaningful your life

---------------------------------------------------------------

- It's not about the thing, go one level up and it's about that
  - example, Dub afraid of dark... it's not about fear, it's about he's coming to you for help

- Listen, actually listen to what the other speaker is saying
  - Try to truly understand

- There is no teacher, there is no pupil
  - We can only share knowledge/experience, we don't teach others, only ourselves
  - Only when you transform does teaching/change happen, and that must be done by you

