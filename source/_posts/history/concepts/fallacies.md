---
title: fallacies
date: 2023-01-18 03:00:42
tags: [trading]
category: [ecosystem, concepts] 
---
<br>

<b>Fallacies</b> are mistakes of reasoning, as opposed to making mistakes that are of a factual nature. 

<br>

##### Mechanics

Broadly speaking, we might divide fallacies into four kinds.

- Fallacies of inconsistency: cases where something inconsistent or self-defeating has been proposed or accepted.
- Fallacies of inappropriate presumption: cases where we have an assumption or a question presupposing something that is not reasonable to accept in the relevant conversational context.
- Fallacies of relevance: cases where irrelevant reasons are being invoked or relevant reasons being ignored.
- Fallacies of insufficiency: cases where the evidence supporting a conclusion is insufficient or weak.


<br>

##### Analysis

Inductive reasoning, or scientific reasoning is empirical. This means that it depends on observation and evidence, not logical principles and often deals with confirmation and disconfirmation.

<br>

##### Mechanics

All branches of science, such as chemistry, biology, physics, etc. use four main components in scientific research:

1. Theories – these are the hypotheses, laws, and facts about the empirical world.
2. The world – all the different objects, processes, and properties of the universe.
3. Explanations and predictions – we use theories to explain what is going on in the world and make predictions. Most predictions are about the future, but we can also make predictions about the past (retrodictions). For example, a geological theory about the earth's history may predict that certain rocks contain a high percentage of iron. A crucial part of scientific research is to test a theory by checking whether our predictions are correct or not.
4. Data (evidence) – the information we gather during observations or experiments. We use data to test our theories and inspire new directions in research.

<br>

##### Analysis

- Scientists cannot prove most scientific claims because they are not 100 percent certain
- But this does not mean that accepting science is solely a matter of faith – we can still have strong evidence to support scientific theories
- We need to be content with probability, not absolute certainty
- For example, we cannot prove with 100 percent certainty that you will die if you jump out of an airplane without a parachute
- In fact, a few people have survived. However, it would be foolish to try it just because we lack this proof.

<br>

The Hypothetical-Deductive Method
- Identify the hypothesis to be tested.
- Generate predications from the hypothesis.
- Use experiments to check whether predictions are correct.
- If the predictions are correct, then the hypothesis is confirmed. If not, then the hypothesis is disconfirmed.

