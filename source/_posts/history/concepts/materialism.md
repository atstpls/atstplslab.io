---
title: materialism
date: 2023-01-15 17:00:42
tags: [resiliency]
category: [ecosystem, concepts]
---
<br>


Definition of Scientific Materialism
 Briefly, scientific materialism has five major ideas:

1. Objective Realism. The world exists objectively “out there” without any need for or even the mere existence of human mind or consciousness or spirituality.

2. Metaphysical realism. Reality is governed by a set of laws that can be described mathematically yet can never be directly perceived in human experience.

3. The closure principle. There is nothing in nature that cannot be explained by purely material causes.

4. Universalism. The laws of nature apply equally to everything everywhere.

5. Physical reductionism. Nature can be reduced to physical entities and their functions, each totally isolated from the rest, having no connection to each other except through the patterns proposed by the laws of nature.

A definition of science, in contrast to scientific materialism, is provided in the appendix of this article.

The" X-Club”
 The X-Club was a group of British and European scientists who met from 1864 to 1892.

 The X Club => 1864–92

http://bit.ly/3hbOSSq

These were among the most prestigious and influential scientists of that time. They very aggressively promoted the ideas of scientific materialism, as described in the writing here, and in B. Alan Wallace’s book listed above.

Jeffrey Koperski: How the ‘X-Club’ Played a Pivotal Role in Dividing Science and Religion

https://bit.ly/3UDqu9L (not an endorsement of Mr. Koperski's full article)

“It was not an accident, not a conceptual drift over time. In my view, the most important players in this story were a group of British thinkers… called the X-Club.”

“One prong of the strategy was clearly this flood of publication and lectures. But the other was getting members of the X-Club into positions of influence, like the Presidency of the World Society and lots of other organizations. The X-Club cultivated allies in publishing, in the church and in education, and they controlled a new school for the training of science teachers.”

Adding to the general belief in "Closure" and "Universalism", in 1897, the physicist William Thomson, Lord Kelvin looked at all the tremendous advancements in electricity, astronomy and biology that marked his age and concluded: "There is nothing new to be discovered in physics now. All that remains is more and more precise measurement."

https://www.nbcnews.com/id/wbna52111904

This was just before the advent of quantum mechanics and general relativity.

The Religious Origins in scientific materialism
This topic is covered in Wallace’s book above. However, I paraphrase those ideas here with some of my own interpretations.

- The founders of modern science, Copernicus, Galileo, Descartes, Newton, Leibniz, were Christian believers, so it is not surprising that they formulated a philosophy of science that resonated with Christian teachings.

- “Objective Realism” is related to the Book of Genesis, in which God created the universe without any mention of the “observer” needed in quantum mechanics.

- “Metaphysical Realism” is related to the idea that God “governs” the universe, although without being active in every detail. This is also related to Plato’s concept that “ideals” which are beyond direct perception determine the world.

- “Universalism” is related to the idea that God governs the entire universe.

- “The Principle of Closure” is related to the idea that God’s laws are complete, there are no further laws, those laws do not evolve or change.

- “Physical Reductionism” is related to the idea that God’s laws are really there, and so those laws can be figured out, and in so doing be used to explain everything that happens, in detail.

 Again, from Jeffrey Koperski:

“You can’t understand Descartes, Newton, or Leibniz if you take the theology out of it. I don’t mean you can’t understand their philosophical writings; you can’t understand their physics. Theism was not a useless appendage that gets tacked onto early modern science. It was an integral part of the big picture.”

“The early moderns thought there were laws of nature because there was a divine lawgiver…. You just don’t get laws for all of nature without theism.”

Social Ills Related to Scientific Materialism
The arguments and conflicts between scientific materialism (not science) and religion are too long and varied to be recounted here. And again, as I stated up front, I’m not taking a stand either way in those conflicts (except to say that it would be of great benefit to humanity if they could be sorted out.)

 Unfortunately, in my opinion scientific materialism has had severe negative impacts on human life on earth, including:

– Amorality, based on the assertion that there is no mind or spirituality, meaning there is no essential connectedness between people, so there is no basis for moral human behavior, or any fundamental reason for social responsibility.

- Cult of self, that is, excessive focus on the individual and on one's own being, due to lack of any "real" connection to other people.

 – Reinforcement of religious fundamentalism, both as a reflexive response to scientific materialism’s denial of spirituality and also as an adaptation of the same type of absolutist thinking used by scientific materialism (although it could be said that such absolutist thinking started with religion, see "origins in religion" above.)

– Rejection of valid science. This is promoted by the proponents of scientific materialism ignoring or actively rejecting the findings of quantum mechanics (QM) and general relativity (GR). The problem for scientific materialism is that those findings throw into question a) the existence of a purely objective material reality (that is, a reality not dependent on an "observer" from QM) and that b) such a reality is fully explainable (i.e. because there is no accepted theory that unifies QM and GR).

- Encouragement of pseudoscience, because scientific materialism is widely promoted as "the truth" without itself being backed up by actual science.

 – Misinformation campaigns, again because acceptance of scientific materialism says that is ok to assert things without explanation or follow-up.

 – Disinterest in science careers, because, due to the promotion of scientific materialism, science is perceived to be narrow, fixed and rigid, and the fact that it is difficult to get funding for exploration outside of accepted scientific materialist dogma.

 – Ignoring or active suppression of understanding and explanation of core psychological phenomena like consciousness, feelings, and empathy, because scientific materialism asserts that they are merely “illusions”.

 - Taboos, denigration, shame and lack of compassion connected to "mental illness", and discrimination against neuro-diversity, because scientific materialist views limit exploration and understanding of mental phenomena.

 - Intellectual rigidity and argumentativeness, due to an insistence on “provability” of appearances without deep examination.

 In my opinion, scientific materialism is also a major philosophical justification for pure laissez-faire/growth-focused capitalism, which is a major systemic cause of destruction of life on earth.

 It is unlikely to be an accident that the X-Club’s promotion of scientific materialism from 1864 to 1892 coincided with the rise of the industrial age, along with child factory labor.

 Conclusion

 A core challenge is that compared to the non-intuitive fuzziness of quantum mechanics and the mind boggling twists of general relativity, the principles of scientific materialism look pretty good.

There is also the greater problem that the human mind really does seem to be at least soft-wired at a deep level to perceive phenomena as "out there" and separate from the act of perception or conceptualization. This pervasive mental inclination reinforces the belief that phenomena are solid and reliable and analyzable in an absolute sense.

While that belief is functionally true and useful in navigating everyday life (and probably much easier for our "software minds" to handle, as kind of a mental shortcut), that belief is relatively easy to override intellectually if you think about it.

However it is difficult (and potentially unsettling and disturbing) to overcome that perception and belief in daily life experience. This may be in part why there is a "natural" wide-spread resistance to introspection and self-awareness.

All of which reinforces the inclination to accept the principles of scientific materialism.

Again, the problem is that while they may be comforting, the principles of scientific materialism have no scientific basis. They are assumptions only, unproven and full of holes when examined closely. Yet they are widely tacitly assumed without question, and so it is difficult to challenge those assumptions in order to undo the harmful effects that they cause.

 However, in any case, change starts with understanding what scientific materialism is and how it is different than science itself.



