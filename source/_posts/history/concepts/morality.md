---
title: morality
date: 2023-01-18 03:00:42
tags: [trading]
category: [ecosystem, concepts] 
---
<br>



biologic basis of moral development

Violence and aggression is not the fundamental basis of social interaction, 

But reconciliation, competence, empathy, long-term planning, and conflict resolution 

Morality arose from <a href="agent/legacy-banks" style="font-size:18px">PLAY</a> and <a href="agent/legacy-banks" style="font-size:18px">RECIPROCITY</a>.

Competence and reciprocal cooperation outperforms compulsion by force and self-serving strategies over time.

Grounded in solid scientific literature



###### Morality

Both the Greek and Hebrew derivations of the word sin are related to archery.

The Greek term hamartia is an archery term which means to miss the mark. The Hebrew derivation  relies on the same imagery.

Sinning is related to the ability to aim or the lack thereof. To miss the target, to aim wrong, or to miss the mark.

There are a variety of ways to miss the mark:

    - don't aim at all
    - assume there's no such thing as aim
    - assume all aims are equal

<br>

Jordan and Dan Crenshaw 

- Multiple dimensions
- Without a goal, there is no happiness. Happiness marks a movement towards a valued goal
- The higher the goal, the more value there is in the movement towards it
- What's the highest goal
- Do your best for the best
- Means for you in the future, next week, next month, next year 
- Serving yourself only works for today's you...
- But serving your future self and others works



Peterson vs Harris 2 

- Facts cannot be derived from values without n intermediary of process and structure ( like Kant demonstrated ) and such intermediary is reliant on the action of fundamental axioms which are not in themselves facts or derivable from facts and description that intermediary as "rationality" is radically insufficient
- Hume's ought from is 
- If goodness is synonymous with whatever gives people pleasure, everything may not be good
- It requires putting stake in sand, Harris does it with universal undesirability of suffering which is an act of faith, not fact. It's an axiom that delimits infinite regress.  Good life prefereable to bad life is story-like.
- Harris' good/bad life has narrative and conceptual simliarity with Heaven and Hell 
He's trying to find alternative of a single revealed truth (fundamentalism) and moral relativism (nihilism)
- Brain is adapted to a world where chaos and order are fundamental realities 
- Morality is necessarily and inevitably expressed in narrative, and as an expression of the value hierarchy which cannot be derived from facts.
- The value hierarchy would be needed to make sense of those facts.
- How is value structure derived? Not by rational observation by individual.  Too complex.
- It's an invisible hand issue. Value structure established by consensus, mediated by reason and truth.
- Stories are moral heuristics, general to apply across many situations as opposed to commandments.

<br> 
  
Jordan Peterson


<br>

