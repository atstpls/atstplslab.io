---
title: government corruption
date: 2023-05-18 01:00:42
tags: [resiliency, government]
category: [ecosystem, resources]
---

<br>


Captured governments, NGOs, and corporations are undermining democratic institutions in the name of democracy.

Most importantly, government agencies like DOJ perpetuated a reign of terror by *criminalizing the virtue of truth telling*.

<br>

#### Covid 19 Vaccines 

|||
|-|-|
|Vol 1 - Media: The Unvaccinated are Scum |  <iframe id="covid-retrospective-vol-1" width="640" height="360" scrolling="no" frameborder="0" style="border: none;" src="https://www.bitchute.com/embed/kbS9zMQPS262/" allowfullscreen></iframe>|
|Vol 2 - Experts: Kids Are Going to Thrive in Facemasks! |<iframe width="640" height="360" scrolling="no" frameborder="0" style="border: none;" src="https://www.bitchute.com/embed/H3sxNpFJP6jY/"></iframe>|
|Vol 3 - Reality is a Conspiracy | <iframe id="covid-retrospective-vol-3" width="640" height="360" scrolling="no" frameborder="0" style="border: none;" src="https://www.bitchute.com/embed/vkdX7EZ9Bal6/" allowfullscreen></iframe>|
|Vol 4 - Anthony Fauci, America’s Covid Disinformation Agent | <iframe id="covid-retrospective-vol-4" width="640" height="360" scrolling="no" frameborder="0" style="border: none;" src="https://www.bitchute.com/embed/SdjIXQT35728/" allowfullscreen></iframe> |
|Vol 5 - Rules Are For The Lab Rats |<iframe id="covid-retrospective-vol-5" width="640" height="360" scrolling="no" frameborder="0" style="border: none;" src="https://www.bitchute.com/embed/OA4hR0ZGC5VH/" allowfullscreen></iframe> |

<br>

#### PR Campaign

|[Pfizer Vaccine instead of Ivermectin](Pfizer-Vaccine-instead-of-Ivermectin)|FDA, CDC, AMA, APHA and corporate controlled media

[Court of Appeals](https://covid19criticalcare.com/wp-content/uploads/2023/02/FLCCC-Apter-v-HHS-Amicus-2-13-23-FINAL.pdf)

Ivermectin threatened both the EUA for the vaccines and the global vaccine market (north of a $100 billion). It also threatened the markets for all the competing pricey, patented, pipeline pharmaceuticals like Remdesivir, Paxlovid, molnupiravir and the monoclonal antibodies (also massive global markets in the many billions).

Pharma’s greatest weapon to attack ivermectin is the FDA. Pharma (and especially Pfizer) has near complete control of the FDA (and the CDC and the NIH). But the FDA couldn’t do it all by themselves so they called in the CDC to do some dirty work: 5 days after the FDA tweet the CDC sent out a warning advisory to all the state medical boards (which was then forwarded to every licensed physician in the country)

FDA couldn’t do it all by themselves so they called in the CDC to do some dirty work: 5 days after the FDA tweet the CDC sent out a warning advisory to all the state medical boards (which was then forwarded to every licensed physician in the country

3 days after the CDC memo they then trotted Fauci out on national TV to state absurd, easily disprovable lies

three major professional societies to call for an end to using fentanyl (err, I mean ivermectin)  AMA, APhA, ASHP  
FDA also went on the warpath across all other major social media. For example, check out what they now have to pull down from the internet and not republish (from the actual settlement document)

devised and executed by Weber Shandwick, the massive PR firm that simultaneously works for the CDC, Moderna, and Pfizer (at the risk of foreshadowing, I also believe, without evidence, that the entire reason the FDA settled this case is because discovery would be severely damaging to many people involved).

FDA’s “opinion” was misleading and deceptive - once a drug receives FDA approval for a disease, it can legally be used to treat any other disease, a practice called “off-label” prescribing. The FDA knows this full well. They knew that no physician needed a Covid-specific “approval” or “authorization.”

Now when the lawsuit was first filed, obviously the FDA moved immediately to dismiss, and they did so by arguing that they cannot be sued because they have “sovereign immunity.” You can’t make this stuff up. What you also can’t make up is that the District Court judge… agreed with them and… dismissed the case. What?

But Boyden Gray doesn’t play. He immediately appealed the case because he knew that, although Federal law actually does gives the government immunity against legal actions, there are some exceptions such as “ultra vires,” a term describing when an official acts outside their authority. Plaintiffs challenging the acts must show that the official was “acting without any authority whatever,” or without any “colorable basis for the exercise of authority.”

Our amazing FLCCC lawyer, Alan Dumoff wrote an amicus brief submitted by our organization (found here on the FLCCC website) and although extremely long, the table of contents lays out the arguments powerfully:

“FDA can inform, but it has identified no authority allowing it to recommend consumers ’stop' taking medicine”

“FDA is not a physician. It has authority to inform, announce, and apprise—but not to endorse, denounce, or advise.”

“Even tweet-sized doses of personalized medical advice are beyond FDA’s statutory authority” (Ed: I love this one).



[Timeline](https://pierrekorymedicalmusings.com/p/the-fda-settled-with-us-because-they)


##### Vaccine Effectiveness 

- Lied about vaccine effectiveness (Fauci)

##### Authority



##### Virus Origin

- Lied about the origin of COVID and carried out disinformation campaign to cover up


##### Vaccine Approval

  
<b>Food & Drug Administration (FDA)</b> can use Emergency Use Authorization (EUA) to authorize unapproved products if:
- the product will be effective
- the benefits outweigh the risks
- there are no adequate, approved, and available alternatives

##### Virus Treatment

- BMFG, WellCome Trust, GAVI & CEPI ran disinformation campaigns to suppress effective treatments and encourage vaccine use (mutations, side effects, effectiveness)
- Suppressed information about effective treatments to COVID (ivermectin, vitamin D)


##### Virus Side Effects

BLGF, GAVI, The Wellcome Trust, and CEPI:

- Suppressed information about vaccine side effects to prevent vaccine hesitancy
- These 4 along with USG claimed scientific consensus while censoring opposing expert opinions.

##### Vaccine Liability

BLGF, GAVI, The Wellcome Trust, and CEPI:

- Paid WHO (1.5 billion) to develop plan for developing and dissmenating vaccines
- Paid USG (8.3 million) to pass regulations giving liability immunity for creators and distributors of vaccines
- BMGF, WellCome Trust, GAVI & CEPI paid WHO to declare that vaccines were the answer and paid USG to avoid lawsuits.

##### Transparency 

- <b>Bill and Melinda Gates Foundation (BMGF)</b> is a private foundation created in 2000 by Bill Gates.
- BMGF created <b>GAVI Vaccine Alliance (GAVI)</b> to innoculate people in low-income nations.
- British research foundation <b>The Wellcome Trust</b> and BLGF created vaccine R&D group <b>CEPI</b> in 2017.
- 80 percent of WHO budget is earmarked contributions--they're driven by donor interests.



#### Collusion 

Between 2006 and 2019, 9 out of 10 FDA commissioners went on to work for the pharmaceutical companies they were in charge of regulating.

Approximately 65% of the FDA's drug review budget comes directly from the pharmaceutical industry.

Americans are the sickest people on the planet, and our health agencies are blatantly compromised.

- Mark McClellan FDA Commissioner tasked with regulating Johson and Johnson becomes member of Board of Directors of Johnson & Johnson 
- Scott Gottlieb FDA Commissioner in charge of regulating Pfizer becomes member of the Board of Directors of Pfizer 
- Stephen Hahn FDA Commissioner in charge of regulating Moderna becomes Chief Medical Officer of Flagship Pioneering - the venture capital firm behind Moderna 
- James C. Smith CEO of Reuters charge of informing people about COVID shots becomes member of Board of Directors of Pfizer 
- Anthony Fauci NIAID Director under NIH becomes funder of bioweapons research on GoF bat coronaviruses at Wuhan Institute of Virology 

