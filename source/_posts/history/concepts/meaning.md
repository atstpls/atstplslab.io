---
title: meaning
date: 2023-04-10 09:53:32
tags: [money]
category: [ecosystem, resources]
---

<br>

<iframe id="odysee-iframe" style="width:60%; aspect-ratio:16 / 9;" src="https://odysee.com/$/embed/Meaning-In-Speech-And-Action:2?r=B6f1mjUy7ezh82xyUa4Q5pLkEiipwm5K&autoplay=true" allowfullscreen></iframe>

## First Concepts:

### EVERYTHING IS BASED ON VALUES

    Everything we think, say, and do is based on values.

    - Reason, understanding, and action are the methods by which human beings act.
    - Humans act because we have the ability to discover causal relations

### VALUE REQUIRES SCARCITY AND UTILITY 

    Value is determined by scarcity and utility
    Scarcity 
    Utility is subjective and always changing, cannot be measured in units 
    - Marginal utility declines as the quantity of the good rises

    Time is valuable because of its irreversibility and our inability to create/destroy/modify it.
        two broad categories: labor and leisure
        - The opportunity cost of labor is leisure and vice versa 

    - There is a difference between valuing in the aggregate and valuing at the margin.

### FREE MARKETS REWARD DOERS

    Free markets operate under natural law and reward those that provide the most value to others.
    - Mises - economics is not quantitative and does not measure because there are no constants and no constant relationships.

        Given the same resources, areas with more people will be more productive than less people because more non-rival ideas, more technology, leads to faster rate of growth 
        - The law of returns asserts that for the combination of economic goods of the higher orders, there exists an optimum.

        Value is not interchangable with a constant unit of measurement


    
Each individual is of devine worth, each person has something of value to bring into the world that no one else can bring in.

If you fail in that duty, if you forswear your responsibility to the highest good, then you deprive the world of something, you tear something out of the fabric of being that is of inestimable value, and you will pay the price, which is hell.

"And if you don't think hell is real, you haven't thought about it enough. It isn't a matter of other people believing foolish things, it's a matter of you being so naive and so well protected that you have no idea how the world is constituted."

faith is a form of courage, "It's incumbunt up on you and actually in your best interest and everyone else's to maintain faith in the fundamental goodness of existence, including your own, despite the evidence to the contrary."

<br>

###### Meaning

Most people find the cardinal meanings in their life not as a consequence of the development of their self, but in service to other people, in the adoption of responsibility.

You build yourself a community. You sacrifice yourself in the service of other people. Not only because you should, but because that is where meaning is to be found and you need a deep meaning to sustain you through tragedy and suffering.

There is also nothing more intrinsicly rewarding than when you put out a manifestation of what you truly believe and the consequences of that is part of the redemptive process for someone else.

<br>

###### Purpose

We're always moving towards a place that in principle has an advantage over the place we're at--we're always pursuing value.

We're social creatures so we compete and cooperate with others in pursuit of value which produces hierarchy.  

Individuals in groups will vary in their ability to manage that pursuit effectively and efficiently which produces inequality.


### PROJECTION 

Projection (victim, rescuer, persecutor) is a tool which protects us from having to take personal responsibility and look at ourselves. The problem is always “out there,” “in them,” “over there,” but never “in me.”



Universal Truths:

1. Myth 

    Everything we do is based on values.
    What's implicitly accepted by everyone is reified (hardened, made real) into what's actually right and true 
    Meaning crisis is a result of reified hidden assumptions about categorization--what makes a thing what it is



2. Myth to Rationality 

    Greeks:  the cosmos might be rationally comprehensible
             Fueled by rapid growth of consciousness (ability to categorize, abstract, differentiate, separate)
    
    Plato:  hierarchy of eternal/universal categories of thought are needed to gain real knowledge about complex world 
            The Idea of the Bird, the Idea of the Chair, the Idea of Justice
            the actual objects on Earth are lesser, shadowy imitations of the real ideas
            The absolute ideas are what give things their conceptual identity
            Parable of the Cave showed we must turn away from the shadows and emerge to see the sun 

    Aristotle: Plato's protege, preferred the concrete, everyday realm over the non-empirical realm, but tried to account for both
            Theory of four causes:
                1. Material - substance of a thing
                2. Efficient - construction of a thing
                3. Formal - essential pattern or identity of a thing
                4. Final - purpose of a thing

            1 & 2 were more concrete, 3 & 4 more mind and conceptual identity

            Teleology - uncontroversial, collectively built-in human ought, an answer to the question of meaning and purpose
            Incorporated into Christianity in the Middle Ages and proved as a psychologically acceptable way of guaranteeing the objectivity of values

    Enlightenment:  artificial Aristotelian teleological overlay was labeled an illusion and stripped away
                    The real facts were the mechanistic, mathematical basis of nature, the worldview of science, or the world of objects
                    Formal and final causes were dispensed with and material & efficient causes became the sole basis for explanation 

                    This created the problem of value again. Teleological explanation was no longer universally accepted 
                    Humanities (studying world of meaning or value) split from the sciences (studying world of objects)

    Descartes:      reified this schism in his theory of metaphysical dualism.  
                    Reality consists of two entirely different and separate realms:
                        1. The objective realm ( body, world, the physical ) - (higher reality) unchanging, no value, no meaning, no context, has intrinsic structure independent of anyone's subjective interpretation of it
                        2. The subjective realm ( mind, the mental ) - (lower reality) value, meaning, interpretation, and context, free will, opinions, feelings, memories, biases, imagination, identities

    Enlightment epistemology split into

            1. empiricists - focus on world or object side, knowledge comes from objective sensory perception/observation 
            2. rationalists - focus on mind side, knowledge rooted in our objective thought or reason, world of sensory perception is too deceiving, too changeable, unfit to be true source of knowledge

    Empricism beat rationalism

    Modern reifications:

    metaphysical realism: a higher, knower-independent real reality or true world really exists, completely separate from all Humans
    subject-object dualism - world is divided into subjective and objective realms, entirely independent of one another

    abstractionism - things are most real when they are divorced from their surrounding contexts, 
    Universally correct ideas (abstractions) that represent or mirror the objective state of affairs out there in real reality


    THE FRAME PROBLEM

    Perceiving objects presents the frame problem. Nothing can be understood in the absence of subjective frame of reference.

    there's simply no way to computationally add subjective context (meaning; relevance; significance) to inherently meaningless objects. The number of potential contexts you have to account for is unlimited. No final stopping rule is possible.


    the frame problem doesn't merely occur when trying to add subjective context onto independent objects, but rather, perceiving objects in the first place already presents the frame problem! That is, when an artificial vision system tries to discriminate objects from their surroundings based on objective sensory properties, it's immediately faced with the question of which similarities & differences in the visual data are the currently relevant ones (a value judgment, which requires a specifying context). This merely pushes the frame problem one step backward. Importantly, this wasn't just a philosophical (conceptual) problem — it was a bona fide scientific (empirical) problem. Peterson & Flanders (2002) draw out the implication:




Peterson explains how nothing whatsoever can be understood in the absence of a subjective frame of reference:

    "Something is a table at a particular and isolated level of analysis, specified by the nature of the observer. In the absence of this observer, one might ask, what is it that is being apprehended? Is the proper level of analysis and specification subatomic, atomic or molecular (or all three at once)? Should the table be considered an indistinguishable element of the earth upon which it rests, or of the solar system, which contains the earth, or of the galaxy itself? The same problem obtains from the perspective of temporality. What is now table was once tree; before that, earth—before that, rock; before that, star. What is now table also has before it an equally complex and lengthy developmental history waiting in “front” of it; it will be, perhaps, ash, then earth, then—far enough in the future—part of the sun again (when the sun finally re-envelops the earth). The table is what it “is” only at a very narrow span of spatial and temporal resolution (the span that precisely characterizes our consciousness). So what is the table as an “independent object”—“free,” that is, of the restrictions that characterize the evidently limited human viewpoint? What is it that can be conceptualized at all spatial and temporal levels of analysis simultaneously? Does the “existence” of the thing include its interactions with everything it influences, and is influenced by, gravitationally and electromagnetically? 
    
    Is that “thing” everything it once was, everything it is, and everything it will be, all at the same time? Where then are its borders? How can it be distinguished from other things? And without such distinction, in what manner can it be said to exist? Question: what is an object, in the absence of a frame of reference? Answer: it is everything conceivable, at once—is something that constitutes the union of all currently discriminable opposites (and something that cannot, therefore, be easily distinguished from nothing)."



Whereas virtually all previous philosophers had put the universal categories of thought (e.g., ideas, essences) into the object, Kant moved them into the subject. He said that the mind comes equipped with universal "a priori" categories of understanding, such as unity, plurality, possibility, necessity, etc. These internal categories shape incoming sensory data from the start and thereby make the world intelligible to us. Against Descartes, Kant said we don't, and can't, experience the independent object "as it truly is" (what Kant called the "thing-in-itself" or the noumenon), even for a moment. The only reality we can ever know — the totality of our phenomenal experience — is inescapably knower-dependent. This was Kant's rationalist "Copernican revolution".

Kant called his philosophy "transcendental idealism". This was an investigation into what must be the case in order for human experience to be the way it is. In other words, transcendental investigation was the philosophical method of attempting to make implicit (hidden) assumptions explicit (that is: the very same thing we are doing in this document).

However, Kant made a key abstractionist error: he conceived of the a priori categories as decontextualized (self-sufficient, disembodied, ahistorical, etc). That is, for Kant the nature of the body & brain (including meaning, value, emotions) and all other meaningful human contexts, played no determinative role in the construction of phenomenal experience. 

But the biggest leap happened in the 20th century when philosophers (e.g., pragmatists, phenomenologists, existentialists) and psychologists (e.g., Jung, Piaget) added meaning, embodiment, motivation, emotion, and function back into the picture.

e commonalities between these fields provides the basis for a new philosophical meta-assumption. This is the perspective that underpins Maps of Meaning, and is an alternative to abstractionism:

    contextualism: the meta-assumption that what is most real are not "things" themselves (i.e., self-sufficient abstractions like objects, facts, essences, etc), but rather their contextual meanings.

Contextualism can be considered a return to the world of meaning, but 'on a higher level.' Whereas the abstractionist paradigm eventually narrowed down Aristotle's four causes to only material & efficient causes (the two types unrelated to meaning), the contextualist paradigm brings back formal & final causes. And it does this through the construct of contexts or wholes, to which the Kantian a priori categories were the precursor.

Importantly, contextualism does not simply discard or reject the abstractionist worldview. Rather, the abstractionist worldview is theorized to be nested inside (a part of) the contextualist worldview (the whole). Peterson (1999a) puts it this way:

    "The world can be validly construed as forum for action, or as place of things . . . No complete world-picture can be generated without use of both modes of construal."


    pragmatism (Peirce, James, Dewey): The key idea in pragmatism is that all thinking and ideas ultimately only matter in terms of their practical effect (i.e., their meaning when cashed out in action). In other words: "you will know them by their fruits". Pragmatism is a nondualist philosophy focused on re-conceptualizing traditional dualisms as contingent, instrumental abstractions rather than reified ontological dichotomies. It is Darwinian (evolutionary), in that it denies a radical gap between animal and human cognition, and in that it views all cognition (including scientific inquiry) as embodied and fundamentally devoted to practical problem-solving (adaptation) rather than objective representation for its own sake.
    phenomenology (Heidegger, Merleau-Ponty): Phenomenology is the philosophy of first-person experience & meaning. Essentially, it dispenses with the Kantian noumenon and focuses only on the phenomenon. It therefore inverts the traditional focus on the objective, instead starting from the premise that life is fundamentally lived subjectively (although phenomenologists tend to reject the term "subjective" because they wish to undermine the entire subject-object dichotomy). Like pragmatism, phenomenology is skeptical of explanatory abstractions and instead redirects attention to concrete action & perception. Heidegger's phenomenology, ontological hermeneutics, specifically emphasized that the deepest ground of existence is a contextual/interpreted relational matrix of meanings, not a world of physical matter. He said objects are more like "equipment in order to", i.e., tools. Merleau-Ponty was known as the phenomenologist of embodiment. He rejected the Cartesian cogito ("I think therefore I am"), and instead suggested that human beings are characterized by something much more like 'I am because I can' — a focus on action over detached cognition.

##### Existentialism
    
     (Kierkegaard, Nietzsche, Heidegger)
    - Existentialism aimed to close the gap between philosophy and life. It critiqued the traditional fixation on the abstract and unchanging (e.g., the realm of Being, essences, subject-object dualism, substances, mechanisms), instead pointing out that the allegedly "lesser", changing realm of Becoming (existence) is actually the fundamental setting in which we live. Philosophy should therefore not be an attempt to mentally escape into a hypothetical "true world", but rather an investigation into concrete existence to help us live more meaningful lives. Existentialism emphasizes particularity over universality, and shares with phenomenology a general emphasis on first-person experience.
    
philosophy of language (Wittgenstein): In the first half of his career, Wittgenstein advocated an extreme abstractionist, dyadic picture theory of meaning: words get their meaning through direct mappings onto empirically observed reality. But in the second half of his career, he renounced this view and instead said that the meaning of words is their use. In other words, to understand the meaning of words, you have to look at the everyday practices or contexts in which they're used.


    ecological psychology & enactivism (Gibson, Rosch): J.J. Gibson was an ecological psychologist influenced by the pragmatists and the phenomenologists. His key insight was that our perceptual systems are evolutionarily adapted not to the micro-level described by the categories of physics, but rather the middle ("basic") level defined by body size and features. This level is made of action opportunities (affordances; tools; meanings; objects viewed relative to subjects' embodiment and desires). He argued that perception is not primarily for objective representation but rather for adaptive action, and therefore that perception & action are tightly coupled. The enactivists had similar ideas, but started more from the perspective of the subject/animal rather than the object/environment.

    process philosophy (Whitehead): Process philosophy, like existentialism, views becoming (change) as the default state of affairs, and being (permanence) as only provisionally established within that broader matrix of change. Process, change, events, and relationships are its preferred categories of analysis, rather than structure, substance, essence, object, etc.
    4E cognitive science (Rosch, Lakoff, Johnson etc): As we've seen, first-generation cognitivism was based on abstractionist assumptions that ran straight into the frame problem. Second-generation "4E" (embodied, embedded, enacted, extended) cognitive science brings the body, action, meaning, and the environment back into the scope of study.

Key contextualist assumptions

Here are the key assumptions of Peterson's contextualism (derived from the above-mentioned fields), contrasted with their abstractionist foils:

    constructivism or interpretivism (an alternative to metaphysical realism): The only reality we can ever hope to know is the raw phenomenal experience that we help construct or interpret — and it is precisely our participation that allows us to know. The difference between constructivism and interpretivism is that the former theorizes that the knower and the unknown start separately and then are put together (e.g., Kant), whereas the latter says they start together and can only be provisionally separated (e.g., Heidegger). In other words, constructivism asserts that a noumenal "realm" (thing-in-itself) literally exists, whereas interpretivism denies it. We will ultimately side with the interpretivists. Therefore, when I use the terms "subjective" or "objective" in the remainder of this document, it is not meant to invoke an absolute ontological Cartesian dichotomy. Rather, it's a shorthand way to focus attention on (provisionally abstract out) the relative contributions of one side or the other, never forgetting that as soon as you talk about one, you are also necessarily talking about the other—and to an ultimately unknown degree.

    embodied mind (an alternative to disembodied mind and faculty psychology): Descartes reified mind as a disembodied inner realm, but mind—body dualism is false. Mind is evolutionary, embodied, cultural, historical, enactive, emotional, & motivated. The way we think, and the categories we have, are shaped by our bodies and the meaningful contexts we inhabit. In other words, abstract thought is built on sensorimotor and perceptual capacities. Essentially, mind is more abstract body, not a separate and different ontological kind. Nor is mind made of discrete "modules". For example, sensorimotor abilities and perception are not two completely separate and different "faculties". Rather, they overlap one another, and are processed in the same areas of the brain.

    pragmatic (knowing-how) theory of knowledge (an alternative to the propositional theory of knowledge): knowing in contextualism is not primarily about an isolated and self-contained subject passively observing and propositionally 'mirroring' a pre-formed and independent object. Instead, our most fundamental relationship with the world is theorized to be nondual and action-oriented. (Very similar to Heidegger’s “ready-to-hand”, a mode of being in which we are at one with our environment, and interact with the "objects" around us not as targets of detached, scientific observation, but rather as tools available for fluid use in the activities & practices we are involved in, as if they were natural extensions of our bodies). Knowing is therefore a practice or activity of adaptive problem solving. We "know" when we can successfully act to transform our experience in desired ways, and correctly anticipate the meanings of things for future experience. "Knowing" thus means an ongoing mutual reconstruction of knower and world (i.e., an activity or process). The referents of abstract concepts are thus not primarily objects, but rather actions & perceptions. The problem of skepticism in Enlightenment thought — how the subject "in here" comes to know the world "out there", and how that gets guaranteed through rational epistemological arguments (e.g., foundationalism) — was never a genuine problem in the first place.




    instrumentalism/anti-representationalism (an alternative to representationalism): Ideas are abstract tools rather than mirrors of reality, which means the ultimate purpose of abstraction is not objective representation as an end in itself, but rather to help produce desired transformations of experience. In other words, the purpose of abstraction is the exact same as the purpose of action.

    value-ladenness and theory-ladenness (alternatives to objectivism):
        value-ladenness, closely tied to goal-directedness, means that there is no value-neutral stance toward the world, not even through the scientific viewpoint. Science is sometimes said to be a "value-free" endeavor involving value-free observation and value-free reason, but even though scientists try to strip away their own idiosyncratic values, they are still collectively applying a certain, consistent scheme of action & perception (selective attention to the intersubjectively apprehensible aspects of perception, i.e. the epistemology of scientific empiricism). Thus, the scientific method does not escape values. It is value-laden and therefore ideological (the ideology of science = scientism).
        theory-ladenness is the recognition that because of value-ladenness, the ostensibly raw facts we collect are always, at least to some degree, already filtered and interpreted. There is no such thing as completely objective, uninterpreted facts. This is a Kuhnian (post-positivist) and therefore Kantian viewpoint.

    pragmatic theory of truth: (an alternative to the correspondence theory of truth): truth is not primarily a matter of how well a proposition or other internal representation corresponds to the objective state of affairs in the external world, but rather how well something works to achieve a desired end (predicting & controlling). The typical concern with a pragmatic theory of truth is that it's nothing but a fancy way of saying "ends justify the means". We will discuss later why this is not necessarily the case.

    triadic theory of meaning (an alternative to the dyadic theory of meaning): in contextualism, meaning is not fundamentally linguistic, cognitive, theoretical, representational, nor "all in the head". Any of those things can be meaningful, but they are not what meaning most fundamentally is. Rather, meaning is a property of a triadic semiotic system, which means that it cuts across subject & object (that is, it's a property of experience as a whole, not just of a disembodied inner mind) and is therefore inseparable from action. We will conceptualize meaning as follows. It is:
        pragmatic, in that it is implication for action, or re-configuration of implication for action (i.e. re-configuration of the a priori interpretive / perceptual framework)
        existential, in that it matters subjectively (motivationally & emotionally) and makes life worth living
        normative, in that it provides a moral ought
        ontological, in that it is the fundamental "stuff" of experience
        relational, in that it depends on a mutual relation or interaction between subject & object, as opposed to being a 100% subjective overlay
        teleological, in that it is leading you toward some end
    narrative (an alternative to naturalism and mechanism): whereas naturalism and mechanism attempt to explain subjective experience (i.e., value and meaning) using the third-person material & efficient cause perspective of physics, the assumption of narrative instead says that life can only be understood through the first-person structure of story.
    
    purity criticism (an alternative to reification): purity criticism is map-territory criticism, and it is sought in at least two ways. First, there is a skepticism of received categories (abstractions; interpretations; assumptions), and specifically, a persistent attempt to ground them in concrete action & perception. Second, there is an attempt to constrain philosophy through science. Traditionally, it has worked more the other way around, but it's also possible to critique philosophical abstractions using our best scientific knowledge of evolution, the brain, body, and nervous system.
    
    holism (an alternative to reductionism): wholes are more than the sum of their parts, which means they cannot be fully understood by decomposing them and studying their parts in isolation.

    relationality (an alternative to atomism): the relations between things are just as real, and in fact prior to, the divisions between things. Relations are not secondary subjective add-ons to pre-existing, fixed objects, but rather, objects are abstractions from whole situations/events. Put another way, process precedes structure. This also means that there is no attempt to identify any abstracted feature of experience as exclusively (absolutely) an object, stimulus, response, etc., outside of a defined context. Where you choose to draw boundaries depends on your goals. It may be useful to consider something a stimulus (cause) from one perspective, but a response (effect) from another.
    the natural theory of categories (an alternative to the classical theory of categories): Second-generation cognitive science research in the 1970s-1980s discovered—empirically—that our "natural" categories (i.e., the categories that we rapidly and unconsciously use) have a fuzzy structure (with a margin and center) and are not defined by formal logic (Lakoff, 1987). Natural categories are instead based on central tendency primarily related to contextual (functional, goal-directed) meaning & emotional valence, and only secondarily on objective sensory property. For example, if a fire starts in your kitchen, you will immediately start looking around for anything that could put it out (e.g., fire extinguisher, jug of lemonade, pot lid to smother the flames, open window to throw the burning item out of, etc). None of these things are objectively similar, but they all work the same way relative to your current goal, and thus all stand out in your field of attention. Wittgenstein called this "family resemblance". From this perspective, scientific (classical) categories are a derivative kind of category, not the fundamental kind.


the one single idea that ties all of this together is: the primacy of meaningful contexts. 

noticed is that all the following constructs are approximately the "same thing" (or slightly different aspects of the same thing): contexts, parts/wholes, stories, narratives or myths, acts or activities, natural categories, affordances, tools, means/ends, and what the Kantian a priori categories anticipated. Here are even more "synonyms": the universal categories of thought, the innate concepts the rationalists were looking for, a frame, a structure, a discrimination, a differentiation, a distinction, a unity, a representation of the thing-in-itself, a model, an understanding, an interpretation, a pattern, a chunk, a black box, a Jungian complex or archetype, the Heideggerian "Dasein", a presupposition, an assumption (as in "hidden assumptions"), an action-oriented representation, an object that the subject is already 'in', a theory, a perspective, an axiom, a cybernetic unit, a system, a triadic semiotic unit, a desired expectancy/prediction, an element of self (identity), a hypothesis, a plan, an event, a paradigm, a gestalt, a functional simplification of complexity, a habit, a structure that intermediates between fact and value, a belief, a game (i.e. a shared belief), a motivation-action-perception (MAP) schema, a map of meaning, or a sub-personality. In other words: a fundamental frame of reference.

What we've called abstractionism is the traditional philosophical search for context-independent, i.e., decontextualized, self-sufficient explanatory abstractions. In response, contextualism says that decontextualized abstractions (e.g., objects, facts, essences) are neither the most fundamental nor the only kind of abstraction. Instead, categorization or abstraction is most fundamentally about selecting what matters (is meaningful) for a certain goal-directed purpose (what's contextually relevant), while excluding what doesn't matter (what's contextually irrelevant). When you think about what something means, you necessarily think about it in relationship to some larger objective situation and/or your subjective goals — that is just how meaning works. This "natural" kind of categorization is functional or context-dependent categorization. Unlike the abstractionist attempt to pin down meaning as either fixed or nonexistent, under contextualism the key feature of meaning is its context-dependent mutability. And notice that another word for context is "whole". Thus, in contextualism the fundamentally real are parts of larger (functional) wholes.


Meaningful contexts or functional categories are therefore groups of phenomena (patterns of experience) segregated out through selective attention, i.e. formed by a co-constitution of "subject" & "object" (or knower/known, interpreter/interpreted). To be more specific, the category would be the perceptual or conceptual experience that results from the superimposition of the subject's top-down goal-bounded concept (motivated expectancy) upon the world's (metaphorical) bottom-up object or "thing-in-itself". (i.e., the Kantian constructivist formula: phenomenal experience = subject X noumenon). The key idea here is that contexts aren't "contained inside" or "after the fact of" experience like classical categories, but rather they constitute experience from the start.

Peterson (1999a) in fact explicitly claims that story (i.e. meaningful context or functional category) just is the structure of knowing-that memory, neuropsychologically (i.e., of our frames of reference — of our most fundamental representations):

    ". . . [knowing-that (representational) memory constitutes] . . . a permanent but modifiable four-dimensional (spatial and temporal) representational model of the experiential field, in its present and potential future manifestations. This model, I would propose, is a story."



Suppose we metaphorically represented the functional category of "all experience we don't understand" (the unknown as it is encountered in actuality; chaos) using images of the feminine and of nature (the Great Mother), because they are pragmatically apt symbols of creation (the Good Mother) as well as destruction (the Terrible Mother). That is, the unknown is intrinsically threatening, but simultaneously the source of all creation that renews the current known (the existing order), and therefore must be continually approached and conquered for life to continue.

Suppose we metaphorically represented the functional category of "all experience we do understand" (the known; order) using images of the masculine and of culture (the Great Father), because they are pragmatically apt symbols of well-ordered experiential/categorical structure (the King) as well as oppressively-ordered structure (the Tyrant). That is, humans need the protection and adaptive capability of individual & collective category systems (tools), but those structures are in constant danger of becoming either too rigid (too "conservative") or too experimental (too "liberal").

Suppose we metaphorically represented the functional category of "that which understands" (the knower; the Logos) using images of the child (the Divine Son), because it is a pragmatically apt symbol of "that which unites the old (the known) with the new (the unknown)". That is, the self or individual (each one of us, but especially men) is the active conscious spirit that continually builds and rebuilds the Great Father (order) out of the Great Mother (chaos). The individual either acts righteously (the Hero; bringing forth the (+) aspect of order and the (+) aspect of chaos) or demonically (the Adversary; bringing forth the (-) aspect of order and the (-) aspect of chaos).

Suppose we metaphorically represented "that which existed prior to everything else" (the uroboros; the absolute unknown in the absence of any knower or frame of reference whatsoever) using the image of a self-devouring serpent, because something completely indiscriminable is a thing and its opposite at the same time. Essentially, the uroboros is the symbol you inevitably end up with when you pile on more & more metaphors attempting to make sense of the absolute source of everything (the beginning of the cosmos; the ultimate human origin; what an object is like in the absence of a subject; etc). The concept of the Kantian noumenon (the unknowable thing-in-itself), as well as Peterson's analysis of what a table is in the absence of human consciousness, are both "archetypal manifestations" of the uroboros pattern.

"In the case of the left hemisphere, a world of things that are familiar, certain, fixed, isolated, explicit, abstracted from context, disembodied, general in nature, quantifiable, known by their parts, and inanimate."

"In the case of the right hemisphere, a world of Gestalten, forms and processes that are never reducible to the already known or certain, never accounted for by dissolution into parts, but always understood as wholes that both incorporate and are incorporated into other wholes, unique, always changing and flowing, interconnected, implicit, understood only in context, embodied and animate."

"The left hemisphere is a world of atomistic elements; the right hemisphere one of relationships. Most importantly the world of the right hemisphere is the world that presences to us, that of the left hemisphere a re-presentation: the left hemisphere a map, the right hemisphere the world of experience [territory] that is mapped."

Not matter, but what matters





Axiomatic categories

#### Chaos

"Chaos is the manner in which anomaly or the complexity of the world manifests itself before it can or has been perceived or conceptualized. It is what there is when what there is is as of yet unknown. It makes itself known in the absence of an expected outcome (a situation that is registered by a system of dedicated nervous system components). It signals the unspecified inadequacy of one or more currently explicit or implicit axioms (generally the latter) [i.e., inadequacy of current order]. Finally, it is processed sequentially by unconscious motor, affective and motivational systems. By the time an anomaly is perceived, or partially perceived—let alone conceptualized, which implies a more abstracted level of processing—it has already been transformed in large part into order."

Chaos is therefore anomalous, bivalent (+ and -) a priori meaning that must be functionally categorized (perceived; abstracted; contextualized; filtered; actualized; understood) in order to constrain it to something particular & usable, for some purpose, for some limited period of time (Peterson, 1999a). This meaning is "a priori" because it is always true of everything that is unknown, and cannot be ignored or ever gotten rid of once and for all.

The mythological category represented by the Dragon of Chaos is therefore the "same thing" as the frame problem in modern cognitive science. That is: the frame problem is the re-emergence of the unknown.

Since psychological models fail when they start with the abstractionist assumptions of realism, empiricism, objectivism, and disembodiment, let's flip all that around. Let's instead adopt the metaphysical assumptions suggested by the hero myth, and a rationalist/contextualist epistemology in which experience is theorized to be interpreted by the knower's a priori internal structure from the start. That is, let's assume that meaning (potential) — not objects or facts — is the ontological "stuff" of the frame problem. The frame problem is therefore no longer the computationally impossible challenge of discriminating objects and then adding meaning onto them using algorithms/rules, but oppositely, how to functionally categorize (reduce) the a priori meaningful whole (the totality) into functional parts/wholes (the particular, the actual).

Peterson says that a structure and a process evolved to solve the frame problem (to confront chaos). 

#### Order  

Suppose that the structure that answers the frame problem (that turns facts into values) is not the disembodied mind or the narrow Enlightenment faculty of reason, but rather, the entire evolved human body, brain, mind, & nervous system that undergirds consciousness, additionally grouped into functional hierarchies and enmeshed in culture. In other words, suppose that the human being in all its contexts just is the hierarchy of frames that solves the 'infinite regress' that the cognitivists ran into. Call this structure order (the known). Peterson (2007) says:

    "Order, as contrasted to chaos, is the current domain of axiomatic systems, hierarchically ordered. It is the explicit and implicit superposition of this domain onto the underlying chaotic substrate that allows for perception, conception and action. Being itself [including both "objective" and "subjective" experience] is a consequence of this superposition."

Hierarchy

The interpretive structure is hierarchical because it mirrors the hierarchy of social status (hierarchy of valued personalities) that it evolved in. Peterson (1999a) says:

    "Our stories—our frames of reference—appear to have a “nested” or hierarchical structure. At any given moment, our attention occupies only one level of that structure. This capacity for restricted attention gives us the capability to make provisional but necessary judgments about the valence and utility of phenomena. However, we can also shift levels of abstraction—we can voluntarily focus our attention, when necessary, on stories that map out larger or smaller areas of space-time."

The elements of a single frame

Each individual story (category; context; whole; frame; belief; etc) comprises the following:

starting point: point a; our interpretation of present sensory experience; what is
motivated goal: point b; end; desired future; a more highly valued frame; what should be
7 +/- 2 sub-stories (chunks): 1st part of means; currently relevant tools/obstacles
actions (plans): 2nd part of means
ground: the other 99% of the world currently being ignored; everything except the current "figure"
emotional evaluation: whether the means are working to achieve the end

Chunking & levels of abstraction

We can use our attention to "zoom in" to a lower level of abstraction (higher level of resolution) and break a frame down into its component parts (make explicit; make conscious; un-abstract). Or oppositely, "zoom out" (i.e. "go meta" to a lower level of resolution) to view things in a wider context, i.e., collapse parts into a whole. Phenomena that are abstracted away are chunked; implicit; folded; hidden; outside of awareness; packed up; simplified; unconscious; ignored; not perceived; become "ground" as opposed to "figure". When we focus our attention on any given level of experience, all other levels (both "inside" and "outside" the 7 +/- 2 currently relevant objects) are collapsed in this manner.

Each level of belief is therefore a pragmatic hypothesis that all its constituent parts will behave, for the current task, as the functional whole (tool) you've modeled it as. In other words, each larger whole provides an ideal value (a functional constraint) for all levels below it. Peterson & Driver-Linn explain:

    ". . . a belief is a presupposition that diverse elements [i.e. parts] of experience may be treated for the purposes of current activity as if they were functionally equivalent [i.e. a whole]."

and Peterson & Flanders (2002) provide an example:

    ". . . a “car” has constituent elements: motor, transmission, body; as the motor, transmission and body are pistons and valves, gears and shafts, windows and doors) all packed up into a “unity” whose structure as a unity is violated whenever something that is not desired occurs."

Which is why Peterson (2001) says:

    "The complexity of the world is nested inside our concepts." [i.e., our categories are the solution to the frame problem]


Mind meets body hierarchically

Categories are most fundamentally motor actions (embodied in procedure; least abstract; least explicit; highest level of resolution); however we can unpack them (to a degree) using our attention, and then abstractly represent them in image (more abstract) or in word (most abstract; most explicit; lowest level of resolution) (Peterson & Flanders, 2002). This means that knowing-that knowledge is ultimately made of, and cashes out in, knowing-how. Carver & Scheier (1998) depict this nicely:

Principles    "Be goals"   Be thoughtful 
Programs      "Do goals"   Prepare dinner 
Sequences     "Motor control goals"  Slice broccoli

This means that mind & body are not two separate ontological categories (dualism), but rather, mind is more abstract body (monism). Peterson (2001) says:

    "The mind meets the body in hierarchical fashion . . . a large-scale plan [category; context; story] consists of smaller plans, which consist of even smaller plans, which eventually grounds out in muscle movement . . . abstraction [mind] thus turns into embodiment . . . [this is a] tentative, engineering-based solution to the age-old “mind-body” problem."



The cybernetic/neuropsychological control structure that undergirds this hierarchy is itself hierarchical, as is the outer social structure that provides ideal constraints at the individual and group levels. It looks like this, from bottom to top:

    Reflexes directly map "facts" (sensory patterns) onto values (actions). Simplistic organisms don't develop past this point (they don't have psychology or perception). In developing animals & human children, reflexes are integrated into (hierarchically "solved by") motivational systems. Goals are initially embedded in reflexes.
    Motivation is the baseline psychological strategy. It non-deterministically influences current desire (current good; what is currently valued). In this way, it influences our goals [final causes] and directs our attention, thus providing the fundamental organizing principles [formal causes] for our perceptual & cognitive categories. In other words, motivation parses up a world (a category; a story) that is functionally organized according to current desired expectations. Motivation is qualitatively multi-dimensional: e.g., hunger, thirst, lust, aggression, joy, loneliness, playfulness, exploration, resentment, envy, fear of the unknown, etc. This means that the perceptual sequence is not object → cognition → action, but rather, complexity (frame problem) → motivated goal → functional category (tool/obstacle) → "object". Perception is motivated (value-laden) and therefore "objects" are constructed / interpreted ("theory-laden") from the start.
    Emotion hierarchically solves (abstracts) the multi-dimensionality of motivation into just two dimensions (positive or negative valence; is the current motivated goal getting closer or further away? is the situation promising or threatening? should you approach or withdraw?)
    Cognition lets us set abstract goals, thus separating motivation & emotion from immediate surrounding context. This lets us pursue ends that are distant in space & time, to more effectively satisfy emotional criteria by making more intelligent sacrifices. (This is why Peterson says information is "meta-food".) In this way, cognition neither invents nor escapes values, but rather presupposes and instrumentally implements values. Basically, motivation & emotion set the target (make the decision) and then cognition works to rationalize the choice and intelligently implement plans to attain it. This means that cognition is ultimately for contextualized action (knowing-how), not decontextualized formal/logical reasoning (knowing-that). Our bounded rational & empirical models always work within motivated perceptual frames, never outside of them (Agnew & Brown, 1989). Cognition or rationality is value-laden, 'all the way down'.
    Identity solves the problem of cognition by providing higher-order (more integrated) goals. Identity is the context of an individual's entire life story (a belief hierarchy, contextualized by a past and oriented toward some future trajectory). The self is implicit in identity, which is why Peterson says that reorganizing your frames is literally 'dying micro-deaths'.
    Intermediary functional hierarchies into which individuals organize themselves to solve the problem of individual identity (e.g., families, workplaces, industries, organizations, communities, cities, governments, etc — i.e. games). These hierarchies integrate individual effort into higher-order social goals, ideally conforming to the principles of downward delegation and least authority.
    Meta-identity, i.e., the ideal human personality, solves the problems of personal identity & functional hierarchical unity by providing a single transcendent unifying value, so that fighting among individuals & groups is unnecessary, and civilization as a whole can productively encounter the unknown (confront chaos).


Suppose that the process that updates the structure that solves the frame problem just is what we now call consciousness (the knower; agency, free will, subjectivity; etc). Peterson (2006a) says:

Logos normally inhabits the mundane (profane) everyday realm of particulars & actuality: the Being that is continually becoming (the totality of our phenomenal, interpreted experience). In a formula: Being = Order (current hierarchy of interpretive categories) X Chaos (the absolute unknown substrate).
Categorization & recategorization





Archetypes of response to the unknown

We are creatures with a front and a back, one consciousness, and one motor output system. Fundamentally we move either forward or backward:

    forward, when in the domain of order (or on the border of order & chaos), applying & tuning our current representations as we go, or;
    backward, when we encounter chaos and our categories no longer work to achieve desired goals.

In the latter case, we are faced with two choices, per our fundamental 2-D nature:

    Confront chaos (explore) and repair our category systems such that they are once again functional, or;
    Flee from chaos in a necessarily self-defeating attempt to maintain our no-longer functional categories.

These are the two archetypes of response to the unknown: the story of Abel vs. Cain, Christ vs. Satan, hero vs. adversary, or the upward spiral vs. the downward spiral.
The hero

The first archetype of response to the unknown is the path of the hero.
Error & exploration: the central pattern

In contextualism, the fundamental "dualism" of interest is not subject—object, but rather the thesis-antithesis-synthesis pattern of action—abstraction. Peterson describes this cycle as exploration → error → exploration. This is the central pattern of human action (the hero archetype; the process of continually solving the frame problem by recategorizing experience). In other words, the embodied pattern of Logos.
Error

Error is the re-emergence of the frame problem, i.e. the re-emergence of chaos (the unknown). Error means that some belief(s) are no longer functional i.e. no longer successfully work to attain their self-defined goal(s). In other words, when we encounter error, it means that we have categorized something incorrectly.

Error is first signaled by anxiety, which is provisional re-categorization of a phenomenon as unknown (i.e. bivalent: threating but potentially promising). Anxiety makes us pause and withdraw, before cautiously approaching to explore. This is the orienting reflex in action, which can itself be considered a sub-personality: the exploratory personality whose goal is to functionally recategorize experience.

However, although anxiety signals error, it does not tell us the magnitude or location of the error. It may be means or ends that are wrong (which are the "same thing", because ends are always means to further ends). And it may be any part of motivation, action, perception, or cognition that is wrong—and potentially at any level of analysis.

To discover the true magnitude and location of the error, we have to explore the anomaly.



Christ is the same force that God used to 


Exploration

Exploration is the process of investigating an anomaly, locating the source of the error, and then repairing relevant beliefs (recategorizing; re-solving the frame problem). Exploration thus involves an abstractive backpedaling in which we divorce ourselves from our habitual beliefs in order to rework them. It can happen concretely in the physical world, or abstractly in thought (e.g., as we are currently doing using the hidden assumptions/critical thinking framework).

Whether done concretely or abstractly, recategorization is a process of un-chunking (un-abstracting; making explicit; making conscious; unfolding) relevant categories, updating them, and then re-chunking them (re-abstracting; making implicit; making unconscious; making hidden; re-folding; automatizing) through practice. (Of course, this happens on multiple levels of resolution at once, and consciousness does not attend to all of them.)

Depending on the depth of the error, the reworking may occur near-instantaneously and unconsciously (e.g., catching something you dropped before it hits the ground), or, at the opposite extreme, years of conscious thought may be necessary (e.g., traumatic experiences). That is, minor mistakes can often be resolved in an online flow manner, whereas more serious or novel error situations call for effortful, offline, abstract thought.

Importantly, the end of exploration (the stopping rule for recategorization) is contextual emotional re-regulation, i.e. reduction of anxiety, not any absolute, objective (correspondence) knowledge criterion. This pragmatic, functional condition is met when means once again work to achieve ends, or when ends have been altered to be more realistic. And this had better be the case, because truly objective rather than functional categorization is not possible:

    because no stopping rule is possible for purely objective categorization.
    because the world is too complex to objectively perceive or internally model.
    because cognition is action-oriented and value-laden (motivated) from the start.
    because perception of the facts is value- and theory-laden (motivated) from the start.

Simply put, we are never privy to objective, literal truth in the first place—so it just cannot be the standard. 


###################################################################################################################################### 



Everything unknown is chaos itself, we don't understand it, we act in it, we don't understand why we act 

As we encounter the unknown, we continually categorize & recategorize experience, building upon functional order.





    "The staggering and as-of-yet unresolved difficulty of solving the frame problem taught AI researchers a very profound lesson: even apparently simple events [or objects] are not bounded in any simple way. Events are simple and distinct only insofar as their relevant features are framed, a priori, by the constraints of an operative [subjective] context."




There is a principle that even the leader is subordinate to... Christianity, president, military, 

If you don't have a noble aim, it won't sustain you through suffering and the difficulty of life 

Bible is a dramatic record of self-realization or abstraction

Many people wrote it, many edited it, many assembled it 


the frame problem: 

    A value system is the precondition for perception itself.

    How do we categorize/abstract experience without a value system










<br>



The word manifests itself to us in the form of meaning.

<br>

##### Mechanics

1st class - Known, familiar, or determinate world ( KOED )
          - meanings of individual and social identity that simplify and structure the world

2nd class - Unknown, foreign or indeterminate world, anomaly, novelty, unexplored
          - those that arise to challenge the integrity of our current known, determinate world

3rd class - Conjunction of the known and unknown, result from exploratory behavior
          - existential meanings intrinsic to individual experience

<br>

##### Analysis

Things change on every timeframe and every scale eventually, so perception is never final

Life forms vary with endless transformations of the world, more fail than succeed.

Some forms and strategies have proved themselves
    Symbiosis between mitochondrea and eukaryotic cell has lasted for several hundred million years
    Dominance hierarchy structure governing individual relationships ruled for at least 100 million years




    #### Beginnings

A story is a narrative of a hierarchy of values 

LGBTXXXXXX all have one thing in common.. they're attempting to define themselves in terms of sexual behavior, and that doesn't work 

Feeding people poision, managing their sickness

Media normalizes eating garbage and getting sick

Mandated by fiat to anchor your expectations of what is normal in ways that help them sell profitable cheap garbage by keeping you hungry, addicted, and sick.


Insulin-resistance is driver of majority of modern diseases.

Processed foods spike insulin and develop insulin resistance.

1. Polyunsaturated and hydrogenated vegetable and seed oils (canola, rapeseed, soybean, corn, safflower, sunflower)
2. Processed corn
3. Soy
4. Low-fat foods
5. Refined flour and sugar

80 percent of food and calories in a modern supermarket are made from these poisons.


Medical Industrial Complex 



Short-term thinking vs long-term thinking 

damage to our conscience from exploiting sentient beings for food

damage to our resources, both plants and animals from misusing/wasting them

damage to our bodies from outsourcing digestion and prioritizing easy over right

the way we troubleshoot, the way we think through things




<br>

Great Divorce - illustrate how individual perspective and personal choices determine how happy (or miserable) we humans are in this life

Heaven is not a place of comfort. As Len says, there’s joy there, but also a great deal of work on the journey.

We cannot remain who we are... we can’t keep the corrupt parts of ourselves, the selfishness, the self-focus, and enter into the joy of Heaven.

As C.S. Lewis wrote in The Problem of Pain, "We are therefore at liberty... to think of [a] bad man’s perdition not as a sentence imposed on him but as the mere fact of being what he is."


##### Mechanics

Parkour maps meaning into the world, you see a wall which means a place you can't go in.

Now you see a thing you can run up, jump or flip off of, wall becomes a source of reward, increases in value 

You're acting out the hero archetype, you approach jumps which are challenging.  They have promise but also potential danger.

Play with and recognize body dealing with fear.

<br>

##### Analysis

Temptations of chaos, hopelessness, self-grandisement, aims to power...

The arc of evolution is this:

<b>Service to others is the deep meaning you need to sustain you through the inherent suffering of life</b>



A demon is a high order malevolent spirit that aggregates 


Fundamental motivational forces (lust, anger, fear, resentment) are best conceptualized as personalities 

When you're possessed by the spirit of anger, it frames your perceptions and emotions, your views of the past and future 

Demons are a fragmentary personality that aggregates emotional states and grips you in a powerful way  




We open the door to unclean spirits 

Humans are a microcosm, structural similarities between humans and the cosmos, truths about nature of the cosmos as a whole may be inferred from truths about human nature, and vice versa 

Anger is something that grips you, it has a reality independent of you 

There are forces in you that give you the ability to control it

You dont have to give into anger when it beckons to you 

