---
title: philosophy
date: 2023-03-16 20:00:42
tags: [resiliency]
category: [ecosystem, concepts]

---

<br>

##### Theory of Forms

The English word "form" may be used to translate two distinct concepts that concerned Plato – the outward "form" or appearance of something, and "Form" in a new, technical nature, that never

...assumes a form like that of any of the things which enter into her; ... But the forms which enter into and go out of her are the likenesses of real existences modelled after their patterns in a wonderful and inexplicable manner....

The objects that are seen, according to Plato, are not real, but literally mimic the real Forms. In the Allegory of the Cave expressed in Republic, the things that are ordinarily perceived in the world are characterized as shadows of the real things, which are not perceived directly. That which the observer understands when he views the world mimics the archetypes of the many types and properties (that is, of universals) of things observed.

In the Allegory of the Cave, the objects that are seen are not real, according to Plato, but literally mimic the real Forms.

##### Plato's Republic

The Republic (Greek: Πολιτεία, translit. Politeia; Latin:  De Republica) is a Socratic dialogue, authored by Plato around 375 BC, concerning justice (δικαιοσύνη), the order and character of the just city-state, and the just man. It is Plato's best-known work, and has proven to be one of the world's most influential works of philosophy and political theory, both intellectually and historically.

In the dialogue, Socrates talks with various Athenians and foreigners about the meaning of justice and whether the just man is happier than the unjust man. They consider the natures of existing regimes and then propose a series of different, hypothetical cities in comparison, culminating in Kallipolis (Καλλίπολις), a utopian city-state ruled by a philosopher-king. They also discuss the theory of forms, the immortality of the soul, and the role of the philosopher and of poetry in society. The dialogue's setting seems to be during the Peloponnesian War.

##### Locke's Social Order

John Locke was the English philosopher who theorized that government was the manifestation of a general will of "the governed" that allowed the governed to change their governors at will. His book, Treatises on Civil Government, was very influential in the American revolution.

As order was established and new economic patterns emerged, people began to question the king's right to rule. For example, John Locke, an eighteenth century English philosopher, theorized that the right to rule came from the "consent of the governed". Montesquieu wrote with admiration about three "branches" of government that checked one another's power. Rousseau believed that communities were most justly governed by the "general will" or majority rule of their citizens. Though the philosophes believed that rulers were important for maintaining order, they questioned the sacrifice of individual freedom that they saw under European monarchs.

: in order to preserve the public good, the central function of government must be the protection of private property. Consider how human social life begins, in a hypothetical state of nature: Each individual is perfectly equal with every other, and all have the absolute liberty to act as they will, without interference from any other. What prevents this natural state from being a violent Hobbesian free-for-all, according to Locke, is that each individual shares in the use of the faculty of reason, so that the actions of every human agent - even in the unreconstructed state of nature - are bound by the self-evident laws of nature.

Understood in this way, the state of nature vests each reasonable individual with an independent right and responsibility to enforce the natural law by punishing those few who irrationally choose to violate it. Because all are equal in the state of nature, the proportional punishment of criminals is a task anyone may undertake. Only in cases when the precipitate action of the offender permits no time for appeal to the common sense, reason, and will of others, Locke held, does this natural state degenerate into the state of war of each against all.

The same principle of appropriation by the investment of labor can be extended to control over the surface of the earth as well, on Locke's view. Individuals who pour themselves into the land - improving its productivity by spending their own time and effort on its cultivation - acquire a property interest in the result. The plowed field is worth more than the virgin prairie precisely because I have invested my labor in plowing it; so even if the prairie was held in common by all, the plowed field is mine. This personal appropriation of natural resources can continue indefinitely, Locke held, so long as there is "enough, and as good" left for others with the gumption to do the same.
Within reasonable limits, then, individuals are free to pursue their own "life, health, liberty, and possessions". Of course the story gets more complicated with the introduction of a monetary system that makes it possible to store up value in excess of what the individual can responsibly enjoy. The fundamental principle still applies: labor is the ultimate source of all economic value. But the creation of a monetary system requires an agreement among distinct individuals on the artificial "value" frozen in what is, in itself, nothing more than a bit of "colored metal". This need for agreement, in turn, gives rise to the social order.

##### Kant on Metaphysics of Morals

Morals are individual perceptions of right and wrong


 from Immanuel Kant's Fundamental Principles of the Metaphysics of Morals. Kant says five things are clear:

1. The origin of moral concepts is entirely a priori in reason.
2. Moral concepts cannot be abstracted from empirical knowledge.
3. The non-empirical, pure, nature of moral concepts dignify them as being supreme practical principles.
4. This value of moral concepts as pure and thus good practical principles is reduced if any empirical knowledge is added in.
5. One must derive for oneself and apply these moral concepts also from pure reason ­– unmixed with empirical knowledge.


German philosopher Immanuel Kant (1724–1804) accepted the basic proposition that a theory of duties – a set of rules telling us what we're obligated to do in any particular situation – was the right approach to ethical problems. What he set out to add, though, was a stricter mechanism for the use of duties in our everyday experience. He wanted a way to get all these duties we've been talking about to work together, to produce a unified recommendation, instead of leaving us confused between loyalty to one principle and another. At least on some basic issues, Kant set out to produce ethical certainty.

Lying is about as primary as issues get in ethics, and the Madoff case is shot through with it:

Bernie Madoff always claimed that the Ponzi scheme wasn't the original idea. He sought money from investors planning to score big with complicated financial maneuvers. He took a few losses early on, though, and faced the possibility of everyone just taking their cash and going home. That's when he started channeling money from new investors to older ones, claiming the funds were the fruit of his excellent stock dealing. He always intended, Madoff says, to get the money back, score some huge successes, and they'd let him get on the straight and narrow again. It never happened. But that doesn't change the fact that Madoff thought it would. He was lying temporarily, and for the good of everyone in the long run.
Sheryl Weinstein had a twenty-year affair with Madoff. She also invested her family's life savings with him. When the Ponzi scheme came undone, she lost everything. To get some money back, she considered writing a tell-all, and that led to a heart-wrenching decision between money and her personal life. Her twenty-year dalliance was not widely known, and things could have remained that way: her husband and son could've gone on without the whole world knowing that the husband was a cuckold and the son the product of a poisoned family. But they needed money because they'd lost everything, including their home, in Madoff's scam. So does she keep up the false story or does she turn the truth into a profit opportunity?

What does Kant say about all this? The answer is his categorical imperative. An imperative is something you need to do. A hypothetical imperative is something you need to do, but only in certain circumstances; for example, I have to eat, but only in those circumstances where I'm hungry. A categorical imperative, by contrast, is something you need to do all the time: there are ethical rules that don't depend on the circumstances, and it's the job of the categorical imperative to tell us what they are. Here, we will consider two distinct expressions of Kant's categorical imperative, two ways that guidance is provided.

The first expression of the categorical imperative – act in such a way that the rule for your action could be universalized – is a consistency principle. Like the golden rule (treat others as you'd like to be treated), it forces you to ask how things would work if everyone else did what you're considering doing.

Second expression is a dignity principle: treat others with respect and as holding value in themselves. You will act ethically, according to Kant, as long as you never accept the temptation to treat others as a way to get something else.

The first expression of Kant's categorical imperative requires that ethical decisions be universalizable.
The second expression of Kant's categorical imperative requires that ethical decisions treat others as ends and not means.
Kant's conception of ethical duties can provide clear guidance but at the cost of inflexibility: it can be hard to make the categorical imperative work in everyday life.

##### Hobbes Leviathan

Hobbes' Leviathan. He describes what people are like in the absence of authority, especially government authority. Hobbes finds that life before a social contract is inherently negative but that people will tend to seek social contracts and peace. He finds that there are laws of nature, and these laws of nature will mitigate our destructive tendencies in the end.

Locke and Hobbes were both social contract theorists, and both natural law theorists (Natural law in the sense of Saint Thomas Aquinas, not Natural law in the sense of Newton), but there the resemblance ends. All other natural law theorists assumed that man was by nature a social animal. Hobbes assumed otherwise, thus his conclusions are strikingly different from those of other natural law theorists.  In addition to his unconventional conclusions about natural law, Hobbes was infamous for producing numerous similarly unconventional results in physics and mathematics.  The leading English mathematician of that era, in the pages of the Proceedings of the Royal Academy, called Hobbes a lunatic for his claim to have squared the circle.

####### Premises

<br>

|Issue|Locke|Hobbes|
|-|-|-|
|Human nature|Man is by nature a social animal.|Man is not by nature a social animal, society could not exist except by the power of the state.|
|The state of nature|In the state of nature men mostly kept their promises and honored their obligations, and, though insecure, it was mostly peaceful, good, and pleasant. |"no society; and which is worst of all, continual fear, and danger of violent death; and the life of man, solitary, poor, nasty, brutish, and short".|
|Knowledge of natural law|Humans know what is right and wrong, and are capable of knowing what is lawful and unlawful well enough to resolve conflicts. In particular, and most importantly, they are capable of telling the difference between what is theirs and what belongs to someone else. Regrettably they do not always act in accordance with this knowledge.|Our knowledge of objective, true answers on such questions is so feeble, so slight and imperfect as to be mostly worthless in resolving practical disputes. In a state of nature people cannot know what is theirs and what is someone else’s. Property exists solely by the will of the state, thus in a state of nature men are condemned to endless violent conflict. In practice morality is for the most part merely a command by some person or group or God, and law merely the momentary will of the ruler.|
|Epistemology|The gap between our ideas and words about the world, and the world itself, is large and difficult, but still, if one man calls something good, while another man calls it evil, the deed or man referred to still has real qualities of good or evil, the categories exist in the world regardless of our names for them, and if one man’s word does not correspond to another mans word, this a problem of communication, not fundamental arbitrariness in reality.|It is the naming, that makes it so. Sometimes Hobbes comes close to the Stalinist position that truth itself is merely the will of the ruler.|
|Conflict|Peace is the norm, and should be the norm. We can and should live together in peace by refraining from molesting each other’s property and persons, and for the most part we do.|Men cannot know good and evil, and in consequence can only live in peace together by subjection to the absolute power of a common master, and therefore there can be no peace between kings. Peace between states is merely war by other means.|

<br>

####### Conclusions

<br>

|Issue|Locke|Hobbes|
|-|-|-|
|The Social Contract|We give up our right to ourselves exact retribution for crimes in return for impartial justice backed by overwhelming force. We retain the right to life and liberty, and gain the right to just, impartial protection of our property|If you shut up and do as you are told, you have the right not to be killed, and you do not even have the right not to be killed, for no matter what the Sovereign does, it does not constitute violation of the contract.|
|Violation of the social contract|If a ruler seeks absolute power, if he acts both as judge and participant in disputes, he puts himself in a state of war with his subjects and we have the right and the duty to kill such rulers and their servants.|No right to rebel. "there can happen no breach of covenant on the part of the sovereign; and consequently none of his subjects, by any pretence of forfeiture, can be freed from his subjection." The ruler’s will defines good and evil for his subjects. The King can do no wrong, because lawful and unlawful, good and evil, are merely commands, merely the will of the ruler.|
|Civil Society|Civil society precedes the state, both morally and historically. Society creates order and grants the state legitimacy.|Civil society is the application of force by the state to uphold contracts and so forth. Civil society is a creation of the state. What most modern people would call civil society is "jostling", pointless conflict and pursuit of selfish ends that a good government should suppress.|
|Rights|Men have rights by their nature|You conceded your rights to the government, in return for your life|
|Role of the State|The only important role of the state is to ensure that justice is seen to be done|Whatever the state does is just by definition. All of society is a direct creation of the state, and a reflection of the will of the ruler.|
|Authorized use of force|Authorization is meaningless, except that the authorization gives us reason to believe that the use of force is just. If authorization does not give us such confidence, perhaps because the state itself is a party to the dispute, or because of past lawless acts and abuses by the state, then we are back in a state of nature.|The concept of just use of force is meaningless or cannot be known. Just use of force is whatever force is authorized|

<br>

##### John Rawles Just War

In his book The Law of Peoples published in 1999, Rawls addresses how it could have been that societies with a democratic history did indeed come to wage war against each other. Rawls shares with Kant and others the view that democratic societies have no reason to go war with each other, but in dissecting the causes of World War II he finds that some democratic states devolved into what he calls "outlaw states". In response to outlaw states, Rawls argues, the military of democratic societies may justifiably under extreme circumstances directly attack civilian populations. I will critically examine this assertion, which Rawls calls a "supreme emergency exemption".

Democratic societies, Rawls maintains, have a right to protect themselves and their democratic principles against outlaw states. This right is based on the aim of democratic states and the principles upon which democratic states are built. The aim of democratic states, Rawls proposes, is to create a sustainable society within a sustainable global community. The basic principles of democratic states are agreed upon in a social contract that reflects a fair-minded point of view.


Rawls' Principles Regarding Conduct in War
Rawls has referred, in principle seven above, to limits or restrictions in the conduct of war. Here is a summary of Rawls' principles restricting the conduct of war:

1. The aim of a just war waged by a just well-ordered people is a just and lasting peace among peoples, and especially with the people's present enemy.

2. Well-ordered peoples do not wage war against each other, but only against non-well-ordered states whose expansionist aims threaten the security and free institutions of well-ordered regimes and bring about the war.

3. In the conduct of war, well-ordered peoples must carefully distinguish three groups: the outlaw state's leaders and officials, its soldiers, and its civilian population.

4. Well-ordered peoples must respect, so far as possible, the human rights of the members of the other side, both civilians and soldiers.

5. Well-ordered peoples are by their actions and proclamations, when feasible, to foreshadow during a war both the kind of peace they aim for and the kind of relations they seek.

6. Practical means-end reasoning must always have a restricted role in judging the appropriateness of an action or policy.

<br>

Religious Just War Theory

The best of religion, I suggest, includes the religious ideal of the pursuit of peace and the promotion of a good or just state. Religion also invites self-examination. These ideals, however, can degenerate into the worst of religion when just war theory is used in the political arena to justify militarism and crusades. Christianity in its origins was a non-violent movement to counter the colonial extremes of Rome that culminated in the dispersion of Jews from Judea during the last decades of the first century C.E. The Christian advocacy of non-violence during this era sought to pique the consciences of the Roman leaders to put an end to Rome's genocidal policies. This advocacy by the Christians was so threatening to Roman imperial power that imprisonment and execution were practiced extensively by the Romans against the early Christians.

Non-violent movements with religious roots have occurred in various settings. The advocacy of non-violence has been a response to the arbitrary exercise of the state's coercive power - examples of which are the movements led by Mohandas Gandhi in India, Nelson Mandela and Desmond Tutu in South Africa, and Martin Luther King in the U.S.

The worst of religion is illustrated by crusades and inquisitions. Thomas Aquinas developed his theory of just war in a time when Christianity was an established religion. During the Christian crusades in the medieval period, the language of fighting evil and aiming at the good (a good way of life, for example) became part of Aquinas' just war theory. The theory of Aquinas was used to support militarism and crusades against infidels and apostates.

<br>

Democratic Just War Theory

Writes in regard to the supreme emergency exemption: "We must proceed here with caution". Let me offer a reason to reinforce Rawls' caution. When historically the philosophical principles of just war have been used by a political establishment, as those of Aquinas were used, they have supported militarism and crusades.

When Rawls in his philosophy leaves open the possibility that the targeting of a civilian population is justifiable, he is talking about a practice that has come to be associated with military states. His philosophy, which aims to place restraints on the arbitrary use of coercive state power, is all too readily interpreted by the political establishment in a military state as a justification for the targeting of civilians.

Rawls' expression "outlaw states" figures significantly in his justification for targeting civilian populations. The same idea is expressed in the political arena as "rogue states" – meaning unprincipled states.

Targeting of civilians is more readily justified when a state is labeled as an outlaw or rogue state. When a regime is categorized as evil or demonic, the justification becomes easier still. These labels echo the language of the medieval philosophers in their justification of war against non-believers.

The terms "evil" and "demonic" – terms that Rawls applies to Hitler's regime - have been used by U.S. political leaders to justify military actions. The expressions "axis of evil," "outposts of tyranny," and "state sponsors of terrorism" to describe the so-called rogue states have appeared in recent political parlance. On September 12, 2001, George W. Bush described the U.S. response to the events of 9/11 as a crusade. The medieval term "crusade," a term linked to established religion, is echoed in Bush's statement. The following comment from Gandhi provides a reply to such language: "Religion itself is outraged when outrage is done in the name of religion".

The criteria of a just war may serve to remind a people of their own principles. When one's country violates these principles, the categories "rogue or unprincipled state," "state sponsors of terrorism," and so forth may well apply to one's own country.

The use of remote release weaponry – bombs dropped from planes, missiles, drones carrying explosives, and other devices – has taken a heavy toll on innocent civilians during the past century. The civilian casualties are described with the euphemism "collateral damage," but the deaths of massive numbers of innocent civilians have been inevitable with the widespread use of remote release weaponry.

The aim of just war theory among medieval Christian philosophers was to preserve a degree of humanity in the midst of the inhumanity of war. Their theories, however, were used to justify brutalities inflicted in crusades. Rawls' theory had a similar intention, but his supreme emergency exemption could be used to justify brutalities inflicted on innocents.

Is there any hope for those who advocate for non-violence on the grounds of democratic just war theory? I propose that the worst features of democratic just war theory can be turned into positive features: the self-reflection that the ideals make possible, I have suggested, can lead one to ask whether the terms "rogue state," "state sponsors of terrorism," and the like, can be applied to one's own state – in my case, to the U.S. To this question I would answer a resounding yes, and the hope I see lies in the vigorous application of democratic ideals, as expressed in part by Rawls' principles, to the massive violence the U.S. perpetrates in our names. Rawls' six principles of the conduct in war, however, like all such principles, raise a presumption in favor of war and have been used to justify misconduct in war in the political arena.

Richard McSorley recognized this presumption in favor of war when he wrote: "[the just/unjust war theory] is used as a front for accepting all wars".

The hope for democratic theory is that it reminds us that, when rights are created, we choose worlds rather than merely choosing particular actions or policies. This reminder lies behind Rawls' warning about means-end reasoning, whether that reasoning is utilitarian, cost-benefit, or the weighing of national interest.

The great power of Rawls' analysis – which helps to explain the favorable reception Rawls' theory has received – is that it taps into a capacity that humans possess and that has been part of human decision-making for millennia. This is the capacity to envision more than the likely consequences of an action or policy. Humans have the capacity to picture different possible worlds: they do so when they turn the cards face down, as it were, and decide on rules from the point of view of anyone affected by the rules – including the least well off. They can, for example, imagine a world in which slavery is legal and a world with no legalized slavery. In choosing a world without slavery, they choose more than a particular action or policy: they choose a world that places slavery out of bounds.

<br>

##### Aristotle Ethics

Ethics are rules by external sources

Aristotle describes how the ethically educated person, who has a proper character has an obligation to participate in their government (city-states in Aristotle's time). The moral health of one's city-state depends on the moral health of all the individuals who live in it and who participate in its political process. Because the city-state has such an interest in the moral health of each citizen, the government does have the right and the obligation to set laws that can be described as "paternalistic", laws that help people to control their own behavior even in private. In this sense Aristotle can be contrasted with modern day libertarian ethics.

Virtue ethics
(or aretaic ethics /ˌærəˈt eɪ.ɪk/, from Greek ἀρετή (arete)) are normative ethical theories which emphasize virtues of mind, character and sense of honesty. Virtue ethicists discuss the nature and definition of virtues and other related problems that focus on the consequences of action. These include how virtues are acquired, how they are applied in various real life contexts, and whether they are rooted in a universal human nature or in a plurality of cultures.

A virtue is generally agreed to be a character trait, such as a habitual action or settled sentiment. Specifically, a virtue is a positive trait that makes its possessor a good human being. A virtue is thus to be distinguished from single actions or feelings. Rosalind Hursthouse says:

Practical wisdom is an acquired trait that enables its possessor to identify the thing to do in any given situation. Unlike theoretical wisdom, practical reason results in action or decision. As John McDowell puts it, practical wisdom involves a "perceptual sensitivity" to what a situation requires

Although eudaimonia was first popularized by Aristotle, it now belongs to the tradition of virtue theories generally. For the virtue theorist, eudaimonia describes that state achieved by the person who lives the proper human life, an outcome that can be reached by practicing the virtues. A virtue is a habit or quality that allows the bearer to succeed at his, her, or its purpose. The virtue of a knife, for example, is sharpness; among the virtues of a racehorse is speed. Thus, to identify the virtues for human beings, one must have an account of what is the human purpose.

Aristotle's list
Aristotle identifies approximately eighteen virtues that enable a person to perform their human function well. He distinguished virtues pertaining to emotion and desire from those relating to the mind. The first he calls "moral" virtues, and the second intellectual virtues (though both are "moral" in the modern sense of the word). Each moral virtue was a mean (see golden mean) between two corresponding vices, one of excess and one of deficiency. Each intellectual virtue is a mental skill or habit by which the mind arrives at truth, affirming what is or denying what is not. In the Nicomachean Ethics he discusses about 11 moral virtues:

<br>

Moral Virtues

1. Courage in the face of fear
2. Temperance in the face of pleasure and pain
3. Liberality with wealth and possessions
4. Magnificence with great wealth and possessions
5. Magnanimity with great honors
6. Proper ambition with normal honors
7. Truthfulness with self-expression
8. Wittiness in conversation
9. Friendliness in social conduct
10. Modesty in the face of shame or shamelessness
11. Righteous indignation in the face of injury

<br>

|SPHERE OF ACTION OR FEELING|EXCESS|MEAN|DEFICIENCY|
|-|-|-|-|
|Fear and Confidence|Rashness|Courage|Cowardice|
|Pleasure and Pain|Licentiousness/Self-indulgence|Temperance|Insensibility|
|Getting and Spending(minor)|Prodigality|Liberality|Illiberality/Meanness|
|Getting and Spending(major)|Vulgarity/Tastelessness|Magnificence|Pettiness/Stinginess|
|Honour and Dishonour(major)|Vanity|Magnanimity|Pusillanimity|
|Honour and Dishonour(minor)|Ambition/empty vanity|Proper ambition/pride|Unambitiousness/undue humility|
|Anger|Irascibility|Patience/Good temper|Lack of spirit/unirascibility|
|Self-expression|Boastfulness|Truthfulness|Understatement/mock modesty|
|Conversation|Buffoonery|Wittiness|Boorishness|
|Social Conduct|Obsequiousness|Friendliness|Cantankerousness|
|Shame|Shyness|Modesty|Shamelessness|
|Indignation|Envy|Righteous indignation|Malicious enjoyment/Spitefulness|

<br>

Intellectual virtues
Nous (intelligence), which apprehends fundamental truths (such as definitions, self-evident principles)
Episteme (science), which is skill with inferential reasoning (such as proofs, syllogisms, demonstrations)
Sophia (theoretical wisdom), which combines fundamental truths with valid, necessary inferences to reason well about unchanging truths.
Aristotle also mentions several other traits:

Gnome (good sense) – passing judgment, "sympathetic understanding"
Synesis (understanding) – comprehending what others say, does not issue commands
Phronesis (practical wisdom) – knowledge of what to do, knowledge of changing truths, issues commands
Techne (art, craftsmanship)
Aristotle's list is not the only list, however. As Alasdair MacIntyre observed in After Virtue, thinkers as diverse as: Homer; the authors of the New Testament; Thomas Aquinas; and Benjamin Franklin; have all proposed lists.

<br>

##### Nietzsche and the End of Traditional Ethics

"God is dead," the declaration attributed to Friedrich Nietzsche, stands along with "I think, therefore I am" (René Descartes, 1641) as philosophy’s most popularized - and parodied - phrases. The t-shirt proclaiming "Nietzsche is dead, signed, God" is funny, but it doesn’t quite answer what Nietzsche was saying in the late 1800s. What Nietzsche meant to launch was not only an assault on a certain religion but also a suspicion of the idea that there’s one source of final justice for all reality. Nietzsche proposed that different cultures and people each produce their own moral recommendations and prohibitions, and there’s no way to indisputably prove that one set is simply and universally preferable to another. The suspicion that there’s no final appeal—and therefore the values and morality practiced by a community can’t be dismissed as wrong or inferior to those practiced elsewhere — is called cultural relativism.

Cultural relativism is the suspicion that values and morality are culture specific—they’re just what the community believes and not the result of universal reason.
For cultural relativists, because all moral guidelines originate within specific cultures, there’s no way to dismiss one set of rules as wrong or inferior to those developed in another culture.

<br>

###### Cultural Ethics

What Is Cultural Ethics?
Culturalists embrace the idea that moral doctrines are just the rules a community believes, and they accept that there’s no way to prove one society’s values better than another. Culturalists don’t, however, follow Nietzsche in taking that as a reason to turn away from all traditional moral regulation; instead, it’s a reason to accept and endorse whichever guidelines are currently in effect wherever you happen to be. The old adage, "when in Rome, do as the Romans do," isn’t too far from where we’re at here.

<br>

##### Virtue Ethics

there's agreement that the world is too diverse and changing to be controlled by lists of recommendations and prohibitions. So proponents of virtue suggest that we change the focus of our moral investigations. Instead of trying to form specific rules for everyone to follow – don't bribe, don't exploit the deceased on TV – they propose that we build virtuous character. The idea is that people who are good will do the good and right thing, regardless of the circumstances: whether they're at home or abroad, whether they're trying to win new clients or making a decision about what kind of images are appropriate for public TV.

<br>

##### Socrates

The most interesting and influential thinker in the fifth century was Socrates, whose dedication to careful reasoning transformed the entire enterprise. Since he sought genuine knowledge rather than mere victory over an opponent, Socrates employed the same logical tricks developed by the Sophists to a new purpose, the pursuit of truth. Thus, his willingness to call everything into question and his determination to accept nothing less than an adequate account of the nature of things make him the first clear exponent of critical philosophy.

Although he was well known during his own time for his conversational skills and public teaching, Socrates wrote nothing, so we are dependent upon his students (especially Xenophon and Plato) for any detailed knowledge of his methods and results. The trouble is that Plato was himself a philosopher who often injected his own theories into the dialogues he presented to the world as discussions between Socrates and other famous figures of the day. Nevertheless, it is usually assumed that at least the early dialogues of Plato provide a (fairly) accurate representation of Socrates himself.

<br>

Postmodernism is radical skepticism about the accessibility of objective truth, all claims to truth have value, all claims to truth are constructs of culture

<br>

##### Mechanics

Metanarratives 
- sweeping explanations of how things work
- Victimization and Power
- Marxism - social conditioning to change society
