---
title: economics
date: 2023-10-18 06:00:42
tags: [trading]
category: [ecosystem, concepts]

---

<br>

<b>Economics</b> is the study of <b>human choices under scarcity</b> and their consequences. It is about individuals, their meanings, and their actions.

Human actions determine what happens, not policy.

<br>

##### Mechanics

"Even in total opposition, hate is largely or wholly irrelevant to the public enemy"

- WrthyHouse

Then why have states, throughout all of history, bred and encouraged hate for the sole purpose of facilitating violence, terrorism, massacres, and genocide.

Pure ignorance.

Dissemination of propaganda, indoctrination of youth, eradication of noncompliance, using systems of informers to weed out dissidents, calling for revenge over injustices and humiliations...

All part of constructing an evil image of the "public enemy".

Dissemination of propaganda, indoctrination of youth, eradication of noncompliance, using systems of informers to weed out dissidents, calling for revenge over injustices and humiliations...

ALl part of constructing an evil image of a "public enemy".

Thinking hate is irrelevant to a "public enemy" is beyond ignorant.

<br>

##### Analysis

Goods, commodities, and wealth are not elements of nature, they are elements of human meaning and conduct. Therefore, we can't use quantitative mathematics on human behavior as there are no constants in individual actions.

Natural sciences can make precise predictions based on formulas, constants, and methods of measurement.  In physics, if we know volume, temperature, pressure, we can make accurate predictions of velocity.

Social Sciences are different.  In economics, we can't take an individual's money, his location, his job, his life, and predict what that individual is going to choose.

Our economy has become more abstract, more service oriented.


1. Friend/Enemy is a false dilemma

Human nature is inherently complex and contradictory, there is opportunity for learning and redemption, negotiation, compromise, and compassion. 
Individuals debate, compromise, unite...continually find more elegant solutions than the wholesale destruction of another.


2. Friend/Enemy is a false premise

States can't kill, they are collectives that motivate men who can kill.  When there's no war, it's just around the corner because politics is tied to human nature.  Man is a tribal animal who seeks friends, makes enemies, and is willing to die and kill for his friends and those to whom he believes he belongs.  If man is good, he doesn't need state to organize and order for him.

killing is personal.  ( Schmitt says private hatred is an apolitical emotion)
   
For all his talk of sovereign decisions, he cannot escape the fact that, at the moment of actual killing, it is the soldier who decides whether or not to pull the trigger. It is the soldier who decides whether or not to suspend the order of communication and to make the decisive friend-enemy distinction. This is true, likewise, in every instance of policing, which is, of course, a small instance of civil warfare. The decision to shoot or to talk simply is the decision on whether to continue or suspend the legal order. This decision, the "sovereign decision," is made just as decisively whichever way the soldier decides. The decision to talk is no less real, no less "sovereign," than the decision to shoot. He holds the whole order in his hands at that moment—the moment of possible enmity that is, for Schmitt, the political. The individual soldier or policeman is, therefore, sovereign either way he chooses; and Schmitt’s politics of sovereignty dissolves precisely in actual combat, its supposed ground. 


1. Schmitt wants you to believe Jesus spoke of loving personal enemies, not public.  

Schmitt reduces the divine command to an artificial rule about private life and separates love from the use of force for everyone else. 
As a result, war, coercion, and fighting become realms in which love is excluded a priori. 

We're told to "love our enemies" and are given the wisdom required to do so 
Everyone is a "very good" creation of God, beloved by God destined for unity with Him, and prayed for by the Church unto salvation
Adversary always has some degree of likeness, however small, by which he is "by nature"" a friend, and through which he may become "in act" a friend once more. 

Schmitt reduces the divine command to a fictitious "private sphere" and leaves the "political enemy" (and thus politics as a whole) to a pagan, graceless world. 

or Schmitt, the form of politics is always the form of the decision on war, on the friend-enemy distinction. 
Friends are inside the law and enemies are outside the law, so the decision by which one determines who is friend and who is enemy is always made, ultimately, from beyond the law. 
Any such decision suspends the normal political order for reasons that do not emerge from within the order and through a power that is not derived from the order. (The ability to efficaciously make this decision is sovereignty.) 

The sovereign decides who are friends (and should be loved) and who are enemies (and should be hated).  
Anything different would be establishing a new sovereignty, an act of rebellion.
Christ’s command to love one’s enemies, the very heart of Christianity, becomes nothing more than the assertion of the practical "divinity" of some men over others. 
Through his positing of reading the truth of Christianity as functioning only within the sovereign majesty of men, Schmitt reduces the Church to the perfect form of the merely political: Christianity is true and truth is ultimately whatever the sovereign says it is. In Schmitt’s "political theology" all the emphasis is on "political." Schmitt loves the Church, this comes through in his writing, but it is a Church that is really an Empire. 

Schmitt never defines friendship—only enmity. Political groupings of "friends" are (by implication) defined as groupings oriented against enemies. The friend is never anything more than the one with whom you are not currently at war. One’s friend could, however, gain enough power to declare the friend-enemy grouping against the fighting collectivity to which you belong, in which case he would become your enemy.  So, the friend is a threat and must be prevented from gaining power: the friend is nothing other than a potential enemy. Therefore, everyone is an enemy and there is no friend/enemy grouping—the distinction collapses into a fluid contest for mastery and there is no meaningful content to "friend" other than simply the one you are not currently fighting, the one who happens to be aligned with you against a mutual enemy. Which is another way of saying that the friend is merely one who has submitted to the same sovereign, handing over his power.

What this means is that the political is not grounded in the "safety" of collective violence, but in the exposure, the risk, of collective friendship. In friendship there is no ever-present possibility of physical killing that produces anxiety and so Schmittian politics. A turn toward killing is always a surprise, always a betrayal, always unanticipated—because friends are real. Friendship is grounded not in fear but in hope. Schmitt is unjustified in his supposition that because man is "a dangerous being" the logic of enmity must dominate.  He is wrong because love is essentially dangerous (as faith is foolish and hope is naïve), and therefore always involves having the hope and courage to face this danger. In contrast, fear seeks to control; a cowardly person seeks to reduce the object of his fear to something smaller than himself, something he can understand and control, something devoid of mystery, devoid of power. A fearful person looks up with trembling and down with contempt and so he attempts to build ever more particular, simple worlds of little things. He seeks to close himself to the universal because the universal is always beyond his understanding, beyond his control, and thus frightening. But man’s fear is never total. It is always parasitic on his loves, his beliefs, and his hopes: movements which boldly reach beyond his control. What is fundamental in man as a spiritual being is his participation in that which is higher than he; and valor, not fear, is characteristic of this participation. The boldness of this valor is our un-grounded and profoundly humble elevation beyond the particular and into the universal: our love carries us beyond ourselves. Such courageous reaching-beyond in faith, hope, and charity (or at least their anticipations) is what it means to be a spiritual being. 

An enemy exists only when, at least potentially, one fighting collectivity of people confronts a similar collectivity. 

Schmitt would say that such an activity is not truly "political" unless it is open to "the real possibility of physical killing," which "as an ever present possibility ... is the leading presupposition which determines in a characteristic way human action and thinking and thereby creates a specifically political behavior."  
Axiom derived from observation that those things we call "political" all have to do with the justified use of coercive force, the ultimate expression of which is physical killing. 
Also the drive, desire or will for self-preservation is the most important motivation within human existence, making the one who can threaten physical killing the ultimate motivator of human action.

this axiom is not fundamental to politics, but, at best, describes a limited sphere of politics, one revealed in its limitation by martyrdom and the willingness to die for others, political acts which take love and the common good as primary motivations, over and above the possibility of physical killing.

But this would imply that in order for there to be a real possibility of physical killing (i.e. politics) there must be a disavowal of politics, which (according to Schmitt) is essentially concerned with the enemy defined as a "collectivity." The climax of politics would also be the eclipse of politics, because "the enemy is solely the public enemy," and physical killing "privatizes" the public.

A gentler reading of Schmitt might argue that, by reducing the enemy to the public enemy, he is arguing that only those enemies that seem to pose a threat to the whole society become the object of politics, motivating the coercive action of those who have as their primary charge care for the whole society. On this reading, Schmitt is not articulating the essence of the political, he is merely describing what pragmatically works within political systems: enemies must be described as "public enemies" in order to justify police or military action. But if this is what Schmitt is arguing, it contradicts his claim that enmity gives action its specifically political character. Rather, the relation of activities (including enemies) to the whole society (what the Aristotelian tradition would call the common good) gives them their political character, and this is both prior and incidental to the possibility that these activities might be related to enmity and the possibility of physical killing. This description of the political would have the effect of collapsing the distinction between private and public enemies, insofar as every enemy (indeed, every thing) is related to the common good, meaning there could be no private enmity that was not, in some sense, political. Likewise, insofar as the "common good" can only be attacked in and through an attack on some particular, private good, we would also have to say that there is no public enemy who is not a private enemy: a point obviously admitted by the victims "personally affected" by an enemy deemed "public." In all likelihood, Schmitt simply does not care about this ontological question, and his attempt at delineating the "essence" of the political is just a pose aimed at justifying violent control.

Schmitt asserts that what "characterizes [a sovereign] exception is principally unlimited authority, which means the suspension of the entire existing order."  However, such a final, total suspension actually occurs not when one fighting collective lines up against another, but only at the moment of killing and so only when one man confronts another man in the most primordial, Hobbesian "state of nature."

And, on second thought, it doesn’t really happen even there. Even at the point of decisive combat, the possibility of communication remains; the possibility of talking, of coming to share an ordered human world, of friendship, remains. This has become a trope of war movies, a standard scene in which two soldiers who stumble upon each other across the hell of no-man’s land recognize their shared humanity. The friendship that underwrites this cliché is the reason why surrender is always possible. The soldier makes the decision to pull the trigger only within the norm of communication. The deliberative resources that he brings to bear on the problem are the linguistic, moral, normative content of the political order of friendship. He decides on the enemy within the matrix of a more profound friendship. The decision on the exception is, then, not a suspension of "the entire existing order" at all; it is not the case that "the norm is destroyed in the exception."  Rather, the exception can only be made within the greater norm. War, then, is possible only because the politics of peace is unavoidable.

It is, therefore, only when one party is finally dead that the political, as friendship, completely vanishes. It is only with a corpse that one can have, final, total enmity. But, of course, the moment of death is the moment that one no longer views the other as a man at all. Pure enmity vanishes; it has no real existence. The enemy is only ever the estranged friend. Or to say it another way, the enemy is always a partial friend. If for Schmitt enemies and potential enemies are all that really exist, we must assert against him the very opposite, that, in fact, friends and partial friends are all that exist. 

Thus turning Schmitt on his head, we can assert definitively that the enemy is not the foundation of politics; the enemy is not, finally, possible. The enemy is not the content of politics, but the vanishing point of politics. 
This is the very opposite of Schmitt’s "The political is the most intense and extreme antagonism, and every concrete antagonism becomes that much more political the closer it approaches the most extreme point, that of the friend-enemy grouping" (The Concept of the Political, 29).

Schmitt Quotes

The state has supreme power to declare enemies, make exceptions, and wield force 

War is highest expression of politics.  War is the existential negation of the enemy.

Politics is to have an enemy and wield violence against the enemy.

Liberalism                      The Political
- Individualism                 - Political Unity
- Ethics, economy culture       - Friend/enemy distinction
- Private                       - Public
- State = security of property  - State = existential decisions about defense

Triumph of bolshevism in Russia, the ideas of militant socialism found their constitution in the antiliberal form of the political 

One camp (Thoma, Anschutz, Krabbe, Kelsen) defined state in terms of positive law and institutions, in virtue of reason, focused on formal study of laws, state, constitutions

Other camp (Schmitt,Smend,Triepel, Kaufmann, Heller) thought reliance on formal definitions and distinctions was root of confusion which excluded social and political content of legal concepts and reliance on logical analysis alone increased uncertainty in the law.

The state is a status: the political unity of a people living in a closed territory

The political is primary and state is dependent upon it. This makes discussion to be about essence of the political which emphasizes its precedence over the legal or formal

Liberal jurispudence set principle of is/ought above data of empirical politics in a way that made it fruitless.

if "the link between de facto and de jure power is the fundamental problem of sovereignty", then maintaining a categorical separation of the normative from the empirical world ends in an antipolitical theory.

Schmitt said first question of a constitutional theory must be the political question of unity and not the question of the logical unity of the law

3 life-spheres ( moral, aesthetic, economic ) have distinctive criteria.
Political sphere is relatively independent of other criteria  not based on them, cannot be derived from them
The political delimits a sphere of conflict and potential conflict, but has no substance
It can be anything that people disagree about so strongly that war is possible
The enemy is the other.  War is extreme realization of enmity.

Friend/Enemy distinction is not a private matter.  

The enemy is not a person's or group's opponent. It is always a public question because it challenges the existence of the political unity of the people.

The people exist as an aggregate before and above it, as teh active element (voters) and as public opinion.

Two aspects of the public: the poeple as a political agent and and the people as beings whose lives are at stake in the primary meaning of the political

People is the subject of politics:  its real initiator (the constitutional people) but also its object (the people of a state's foreign policy)

Schmitt 1) refutes liberal conception of politics ( which assumes the possibility of rational will formation ) because of friend/enemy distinction
  1) development of mass democracy has made parliament an obsolete institution

Schmitt says state is subordinate to various social and economic associations, that it is an aggregate of compromises between heterogeneous groups, as the sum total of their agreements
Integrity and unity of state are undermined, and parliament loses its ability to rationally mediate and integrate divergent interests

----------------------------------------------------------------------------

However, we should not take this moral demand too far, and falsely claim that recognizing any man or opposing group as enemy is inherently dehumanizing. Schmitt’s most famous thoughts revolved around the absolute necessity of defining one’s enemy, after all. He expressed no concern this was inherently dehumanizing — quite the contrary. For Schmitt, what leads to dehumanization is man’s attempts to reach utopia. Ideological claims, those derived from abstractions and offering self-contained systems, tend in that direction. Recognition of the friend/enemy distinction, which is grounded in objective reality, without moral content, the opposite of an attempt to build heaven on earth, does not tend in that direction.

Recognition of the friend/enemy distinction, which is grounded in objective reality

The state is what pertains to the political. 

What's the political?  Easy, just work backwards. Morality has good/evil, aesthetics beautiful/ugly, AND SO FORTH LOL. 

Anything that forces groups into the position of friend and enemy is political.  

And the state decides on the friend-enemy distinction.

"The specific political distinction to which political actions and motives can be reduced is that between friend and enemy." The enemy is "the other, the stranger." The enemy "is, in a specially intense way, existentially something different and alien, so that in "the extreme case conflicts with him are possible. These can neither be decided by a previously determined general norm nor by the judgment of a disinterested and therefore neutral third party." Such conflict is "the extreme case," but only the "actual participants can . . . judge the concrete situation and settle" the conflict. The participants base this decision on whether "the adversary intends to negate his opponent’s way of life and therefore must be repulsed or fought in order to preserve one’s own form of existence." The specific reasons that drive this decision vary; the essence is that the distinction among two groups exists.

Thus, we come to a definition of the political. "The political is the most intense and extreme antagonism, and every concrete antagonism becomes that much more political the closer it approaches the most extreme point, that of the friend-enemy grouping." "The phenomenon of the political can be understood only in the context of the ever present possibility of the friend-and-enemy grouping, regardless of the aspects which this possibility implies for morality, aesthetics, and economics." Any antithesis 

Peterson's order and chaos vs Schmitt's friend and enemy 
Environment/Behavior vs Person/Group

<b>Trade</b> is an exchange where each party benefits. Each individual receives <b>something they value more</b> than what they gave up. If that wasn't the case, then why trade?

<br>

##### Analysis

Market prices are not used for value, rather they give us indicators of upper or lower bounds of individual valuation. A buyer pays ten dollars for a book because he values the book more than he values ten dollars.  Likewise, the seller values the book less than ten dollars.

Money solves the many problems trading presents:

|Problem|Solution|
|-|-|
|Goods are not the same|One|
|Want to sell today, buy in future|Durable|
|Want to sell large, buy something small|Divisible|
|Want to sell in one location, buy in another|Transportable|

<br>

People are free to choose anything as money, but over time some things will function better. Certain commodities came to be used as a medium of exchange naturally.

<b>Gold</b> became superior money because it does not corrode, cannot be synthesized, and has the highest stock-to-flow ratio (annual production for any year is tiny compared to stockpiles) which resists debasement through overproduction.






<b>Economics</b> is the study of <b>human choices under scarcity</b> and their consequences. It is about individuals, their meanings, and their actions.

Human actions determine what happens, not policy. 

<br>

##### Mechanics

Goods have utility.  Economic goods have utility and value. Consumption goods are aquired for their own sake. Capital goods are acquired to produce consumption goods. Money is acquired to be exchanged for a consumption good or capital good. 

Time is limited and every economic choice has opportunity cost. Individuals reason they can improve the quality and quantity of their lives, their situation. Civilization happens when individuals lower their time preference and think more and provide more for future self and others.

<br>

##### Analysis

Goods, commodities, and wealth are not elements of nature, they are elements of human meaning and conduct. Therefore, we can't use quantitative mathematics on human behavior as there are no constants in individual actions.

Natural sciences can make precise predictions based on formulas, constants, and methods of measurement.  In physics, if we know volume, temperature, pressure, we can make accurate predictions of velocity.

Social Sciences are different.  In economics, we can't take an individual's money, his location, his job, his life, and predict what that individual is going to choose.

Our economy has become more abstract, more service oriented.

Debt goes to consumption rather than capital yielding more than cost of borrowing 

1) Constantly finding ways to produce improved goods and services in the cheapest most efficient way possible 

- In democracy winner imposes their will, loser gets nothing  
- In free market economy, you can find someone who agrees to exchange 
- Everyone votes, everyone gets to decide, everyone wins 
- Free market economy leads to inequality 
- People who produce goods that are more in demand will be more successful and have more wealth 
- Success in free market economy is a measure of how well you are able to serve others 
- Having produced things that people value more, having followed price signals accurately 
- In political realm, it's the opposite... coercion, corruption, 
- Inequality is good because it encourages everyone to become more and more productive 
- Losers benefit from the winners producing things
- All individuals in an economy are in cooperation with each other. 
- You can improve the lives of others with the productive work you engage in 
- A wealthy individual can preserve his wealth only by continuing to server the consumers in most efficient way 
- We act more rationally rather than impulsively.
- We build superior, long-term goods rather than goods for immediate consumption
- We improve production of future goods, creating capital goods We improve knowledge, our living standards, we are more cooperative
- Cheating/lying/stealing has short-term rewards but long-term disasters Humans spent more time building spears and fishing rods than hunting
- This improved productivity and created civilization
- Time preference and the marshmallow experiment by Walter Mischel in late 60s Save money rather than spend it
- Invest in acquiring skills rather than taking immediate job for lower wage
- Studying twice a week rather than cramming for test night before
- Our lives are largely determined by the trades between ourselves and our future selves
- Capital accumulation allows humans to achieve enormous goals Security affects this.
- If money can be stolen/confiscated at any time, we prioritize short-term
- Expected future value of money does too.
- If losing value, why think long-term? Why save?
- Society with soft money saves less, accumulates less capital, decision making is more present-oriented.  
- Sound money is an essential requirement for individual freedom
- Central banks increase the money supply to pay off debt.
- Government can confiscate it, devalue it, tax it
- Engineers, physicists, are all limited by laws, but economists are not
- Satoshi engineered a money system.
- We're moving from a politically motivated money system to an engineering motivated money system 
- Socio-political energy is captured by govt and becomes imperfect 23:00
- Exchange rate fluctuations
- Devaluing currency makes exports cheaper, incentivizes countries for higher GDP millions have a vested interest in this, specialize in managing it, exploiting it
- To make exports grow you develop a competitive advantage that makes your products have a global demand we need global unit of account and measure of value
- Under a sound monetary system, governments must be fiscally responsible
- Central banks increase the money supply to pay off government debt
- Keynesians - measure of economic health is aggregate spending when individuals collectively spend, it incentivizes producers to produce more, which increases employment
- Too much spending causes inflation and rise in prices Too little spending makes producers stop producing, causes unemployment and recession


scarcity 
    - we VALUE
          - goods           (value)
          - economic goods  (utility + value)
    - we PRODUCE
          - land
          - labor           ( work towards goods )
          - capital         ( work towards higher productivity )
          - technology


