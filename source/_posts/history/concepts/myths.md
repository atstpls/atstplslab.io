---
title: myths
date: 2023-01-18 03:00:42
tags: [trading]
category: [ecosystem, concepts] 
---
<br>

<iframe id="odysee-iframe" style="width:50%; aspect-ratio:16 / 9;" src="https://odysee.com/$/embed/Rescuing-The-Father:0?r=B6f1mjUy7ezh82xyUa4Q5pLkEiipwm5K&autoplay=true" allowfullscreen></iframe>

#### Myths

In the hero myth, the universal mythological template, there are four universal symbolic characters:

    The Great Mother
    The Great Father
    The Divine Son
    The Dragon of Chaos


Our longest-lasting evolutionary surroundings were social hierarchies populated by men, women, and children. 

Man, woman, and child are therefore the three oldest natural categories built into our nervous systems as pre-conditions for understanding.

As we explored & abstracted, we applied the social-cognitive categories we already had in an attempt to symbolically (metaphorically) represent the fundamental ontology of phenomenal experience. 

In other words, we projected human personalities onto our experience of the nonhuman world, to try to make sense of it (to functionally categorize it).

We applied what we already understood to what we did not yet understand. 

This is exaptation: evolutionary recruitment of an existing structure for a new purpose. And since this was long before the development of the scientific method, suppose these categories were representing ontology from the perspective of raw (i.e. first-person "subjective") experience, not from the scientific, objective perspective. 

In other words, suppose that myth was phenomenological, pragmatic philosophy.

The convergence of evidence regarding myth, the brain hemispheres, and functional categorization leads to 

Reverse engineering the implication for ontology—the fundamental metaphysical categories of the real. 

And the implication is this: the reality we are evolutionarily adapted to is not matter (the world of objects), but rather what matters (the world of meaning). 


He says (1999b):

    "What if it were the case that human beings were adapted to the significance of things, rather than to "things" themselves? Wouldn't that suggest that the significance or meaning of things was more "real" than the things themselves (allowing that "what is adapted to" constitutes reality, which only means accepting as valid a basic implicit axiom of evolutionary theory: that the "organism" adapts to the "environment")."

What this additionally implies is that we are creatures adapted to the unknown, as such. That is, we evolved to solve any challenge — to face any problem — not just a specific sub-class of problems. 


The upshot is therefore that the hero myth is not a pre-rational, childlike, unscientific coping mechanism, but rather an entirely abstract, meta-functional categorization scheme that works as a pragmatic guide to action & perception in all contexts, regardless of particular situations, goals, problems, times, or meanings. That is, the hero myth is the one and only complete story. What's more, the fact that our ancestors really lived by it for nearly all human history (far before anyone could afford "luxury beliefs"), means that it is evolutionarily vetted. That we're alive now is powerful evidence, if not outright proof, that it worked.



   "Order, as contrasted to chaos, is the current domain of axiomatic systems, hierarchically ordered. It is the explicit and implicit superposition of this domain onto the underlying chaotic substrate that allows for perception, conception and action. Being itself [including both "objective" and "subjective" experience] is a consequence of this superposition."

   
"The structure through which we look at the world is a story." 

"The empiricists and rationalists were wrong. We cannot derive a picture of the world through mere reference to the facts. Because there is one fact per phenomenon and there's too many phenomena."

Need to arrange them hierarchly, that's what a story is 
What's the correct story?  It's not Cain.  But poeple are playing that out bitter resentful chaotic nihilistic, angry at system, vengeful anger

People suffer but this way makes things worse.

story validates itself through actions 



