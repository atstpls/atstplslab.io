---
title: ethics
tags: [money, government, currency]
category: [ecosystem, concepts]
---

<br>


<br>

##### Mechanics

There are three categories of ethical theories:
- Normative ethics
- Meta ethics
- Applied ethics

Normative theories tell us not only what we ought to do, but also why we do things that in some instances may appear counterintuitive to what we think an ethical decision would be. Such theories are often called ethical systems because they provide a system that allows people to determine ethical actions that individuals should take. Evans and Macmillan define normative ethics as "theories of ethics that are concerned with the norms, standards or criteria that define principles of ethical behaviour". The most common examples of normative ethical theories are utilitarianism, Kantian duty-based ethics (deontology), and divine command theory, which are described later in this chapter. These systems are used by individuals to make decisions when confronted with ethical dilemmas.

Meta-ethics does not address how we ought to behave; rather, meta-ethics is related more to the study of ethical theory itself. Here the interest is in evaluating moral and ethical theories and systems. For example, moral relativism is a meta-ethical theory because it interprets discussions around ethics; a question asked within moral relativism is "is ethics culturally relative?" Evans and Macmillan define meta-ethics as "theories of ethics concerned with the moral concepts, theories, and the meaning of moral language. Pollock further defines meta-ethics as "a discipline that investigates the meaning of ethical systems and whether they are relative or are universal, and are self-constructed or are independent of human creation". More concisely, meta-ethics concerns an interpretation and evaluation of the language used within normative ethical theories.

Applied ethics describes how we apply normative theories to specific issues, usually related to work or belonging to an organization; for example, policies and procedures of organizations or ethical codes of outlaw bikers versus ethical codes of police officers. Evans and Macmillan define applied ethics as "theories of ethics concerned with the application of normative ethics to particular ethical issues". An example is knowing and practising the code of ethics for BC Corrections as an employee of BC Corrections or following the British Columbia Police Code of Ethics as a police officer.

<br>

##### Utilitarian ethics

is a normative ethical system that is primarily concerned with the consequences of ethical decisions; 

    Measuring happiness is difficult. 
    Utilitarian ethics is concerned about the consequences of our actions, regardless of the action itself.
    Desired ethical consequences that actually result from our actions do not always happen immediately. 
    Happiness should not be the only consequence or goal that matters in some ethical dilemmas.


##### Deontology

was formulated by Immanuel Kant. Kant believed that the end result is not of primary importance; rather, the real importance is in determining the moral intent of a decision or action itself. Kant would assess the morality of one's action and disregard the consequences. He further believed that we have duties that are imperative and that these duties must never be abandoned, regardless of the anticipated outcome.


##### Virtue ethics

has its historical background in ancient Greece and was primarily developed by Aristotle. For the purposes of law enforcement, the major foundation in virtue ethics is the idea that if you are a good person, you will do good things, and to be good, you must do good. In essence, we do not do good things because of an analysis of the end result or of an equation to decide how many people to help versus harm. Rather, we do the right thing, or good thing, because of our good character as demonstrated throughout our life. Therefore, the good act is an automatic response requiring little thought. However, when faced with complex ethical dilemmas, the person who has demonstrated a life of good character will show good character, using temperance and intellect. The real question for Aristotle was not, "what should I do?" but rather "what type of person ought I be?" When our answer is that we ought to be a virtuous person, we are likely to act in a virtuous manner, and therefore in an ethical manner.


Executive virtues are examples of "strength of will," such as courage and perseverance.
Moral virtues are related to moral goodness. Examples are compassion, generosity, truthfulness, and good temper.
Intellectual virtues are related to the ability to consider options. Examples are wittiness, wisdom, and understanding.


Religion or Divine Command Theory
Religion is often considered the most widely used system to make ethical decisions and to conduct moral reasoning. Throughout the world, people rely on a variety of religions to help them determine the most ethical action to take. While divine command theory is widely used throughout the world, there are differences: the application of the theory may differ from religion to religion, and it may differ within each religion.

One of the basic tenets for divine command theory is to use God as the source for all principles. In this way, to rely upon divine command theory, a person must believe that there is a willful and rational god that has provided the direction toward an ethical outcome. It is from God's commands that actions are determined to be right or wrong and, because of this, divine command theory provides an objective assessment of what is ethical or moral. However, there is ambiguity in the way in which some scripture is interpreted.

Different religions that believe different things
If God is omnipotent, and is also the basis of morality:
How can we rationalize the suffering of innocent children in developing countries?
Is this God's plan to allow this to happen? If it is, how can we call this moral?

<br>

##### Natural Law

Natural law was espoused by Saint Thomas Aquinas, who viewed the world as being created by God and understood that humans are rational beings capable of using their intellect to comprehend the world. By extension, God enabled humans to reason in a natural way to make ethical choices. Aquinas viewed the first principle of natural law as: "good is to be done and promoted, and evil is to be avoided". Simply put, natural law asserts that what is good is natural, and what is natural is good. Unlike Thomas Hobbes' cynical view in the social contract theory, Aquinas viewed humans as being naturally inclined to do good rather than evil.

Because of the natural inclination toward doing good, Aquinas viewed morality as a universal set of rights and wrongs that are shared across cultures. He delineated two basic human inclinations:

To preserve one's own life
To preserve the human species
Followers of natural law would suggest that the decision is moral if it furthers human life or preserves one's own life. Should the decision go against human life or preserving your own life, the decision is immoral.

Q. How can natural law assist law enforcement in moral dilemmas?
Natural law can reaffirm in officers the importance of their job, that being to preserve their own life and the human species. Officers could be reminded that property is not as important as life and that their sole function should be public safety, rather than the protection of property, which is one of the common law duties of police officers.

Officers could also use natural law as a reminder of the importance to preserve their own lives when confronted with dangerous situations, and that is natural to want to protect oneself.

Criticisms of Natural Law
A problem inherent in natural law is defining what is natural. A proponent of natural law may deduce that homosexuality is unnatural because it does not preserve the human species. However, biologists have documented many different species that engage in homosexual behaviour. Many people consider homosexual behaviour as unnatural; however, it is seen among a variety of animal species, therefore the case for this being a natural activity is strong.

<br>

##### Rawls' Theory of Justice

John Rawls was a contemporary philosopher who studied theories surrounding justice. His theories are not focused on helping individuals cope with ethical dilemmas; rather they address general concepts that consider how the criminal justice system ought to behave and function in a liberal democracy. It is for this reason that it important that all law enforcement personnel be aware of Rawls' theories of justice or at least have a general understanding of the major concepts that he puts forth.

Rawls' theory is oriented toward liberalism and forms the basis for what law enforcement, and the criminal justice system, should strive for in a pluralistic and liberal society. Borrowing from some concepts of social contract theory, Rawls envisions a society in which the principles of justice are founded in a social contract. However, Rawls identifies problems with the social contract that do not allow fairness and equality to exist among members of society and therefore proposes a social contract which is negotiated behind a "veil of ignorance". Here the negotiating participants have no idea what their race, gender, education, health, sexual orientation, and other characteristics are so that the social contract is fair. Ultimately, Rawls argues that the primary concern of justice is fairness, and within this paradigm Rawls identifies two principles:

"Each person is to have an equal right to the most extensive basic liberty compatible with a similar liberty for others". Rawls goes further by allowing each person to engage in activities, as long as he or she does not infringe on the rights of others.
"Social and economic inequalities are to be arranged so that they are both (a) reasonably expected to be to everyone's advantage (b) attached to positions and offices open to all…". Likewise, everyone should share in the wealth of society and everyone should receive benefits from the distribution of wealth. Rawls does not argue that everyone should be paid the same, but rather that everyone should have benefit from a fair income and have access to those jobs that pay more.

<br>

##### Moral Relativism

The principles of morality can be viewed as either relativist or absolutist. Moral relativism refers to the differences in morality from culture to culture. A moral relativist's perspective would state that what is moral in one culture may not be moral in another culture, depending upon the culture. This is important for police officers to understand in a pluralistic society in which many cultures and religions make up the community where the police officer works. It is important for officers to appreciate that what may be immoral in a traditional Canadian sense may not be moral to members of a culture traditionally found outside of Canada. In realizing this, officers may be able to withhold judgment, or at the very least empathize with the members from that community for what in the officer's perspective may be an immoral act.
