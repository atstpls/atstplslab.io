---
title: coopetition
date: 2023-05-20 01:00:42
tags: [resiliency, bitcoin]
category: [ecosystem, resources]
---

<br>

Coopetition occurs where organizations both compete and cooperate with each other (cooperative competition).

For example, Netflix is a customer of Amazon AWS and Amazon Prime competes with Netflix Prime.

<br>

##### Mechanics

Other issues 

Examples in big pharma, Train disaster in East Palestine..

All costs, healthcare, welfare from lost jobs, declining home values in the community 

Train companies paying slap on the wrist fines, American tax payer pays all the costs

No incentive to mitigate accidents/mistakes 

They privatize the rewards/profits but socialize the costs   


<br>

- Breakdown in the system of checks and balances
- Civil society organizations, the media, private industry and the government, they’re all supposed to have different interests
- They have a check on each other
- But what we’re seeing is, underneath the surface, they’re engaged in anti-competitive behavior
- Basically media, these internet censors, the enforcement agencies and NGOs, all acting in concert against the population as opposed to checking each other.
- When these institutions break down, when they don’t work anymore

<br>

##### Analysis

American Heart Association inudstry nutrition forum members:
- Canola
- Cargill
- General Mills
- McDonalds
- Quaker
- Unilever
- United Soybean Board




<b>BlackRock</b> is an investment firm which owns more stock in major companies than any other organization.

<br>

##### Mechanics

- <b>Larry Fink</b> joined Wall Street in 1976, pioneered idea of debt securitization packaging together loans as bonds
- Ran trading desk for Mortgage Backed Securities that led to 2008 crisis
- Lost 90M due to interest rates, used 5M loan from BlackStone to start Blackrock to focus on risk management and client trust
- Deep political connections, manage 9T worth of assets
- Client buys BR ETF/active fund, receives receipt for % ownership, tracks value and performance of underlying assets
- USG used them to manage toxic assets they took from Bear Stearns
- USG used them to buy corporate bonds to try countering 2020 crisis
- FDIC used them to manage SDC securities portfolio sales in 2023
- Joined Fidelity and others to invest 400M in Circle
- Alladin is to Blackrock what AWS is to Amazon
- They inherit the voting votes and have huge sway in boardrooms, can influence company policy, regulations, corporate governance, etc.

<br>

##### Analysis



##### THE GREAT NARRATIVE FOR A BETTER FUTURE

Centralization of power, we face multiple existential crisis, therefore we need greater global cooperation, which of course will be managed by stakeholders

1. Force all corporations to adopt ESG standards through top-down manipulation of private-public partnership (govts & big business) with NGOs being coordinating entities

2. Transform the youth culture so that youth demands ESG, and will only work for and buy from ESG-compliant companies

3. Rewrite social contract to accept transition from an economy of production and consumption (capitalism) to an economy of caring and sharing (communism)

<br>
