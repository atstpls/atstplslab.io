---
title: government overreach
date: 2023-01-16 16:00:42
tags: [fiat, crisis]
category: [ecosystem, events]
---

<br>

Government exceeds its authority

<br>

##### Mechanics

Trucker protests in Canada were against COVID-19 restrictions (namely vaccine mandates) organised by long-haul truckers who used their trucks to shut down roads.

Canadian Prime Minister Justin Trudeau invoked The Emergencies Act that vastly expands the powers of the government.

For a period of 30 days, the government got the power to essentially circumvent the entire democratic process of Canada.

- both Commons and the Senate (both houses of the Canadian parliament) would have to approve this, -
- the Senate never did.
- making it illegal for people to gather, and arresting 230 people for disobeying. 
- Dozens of trucks were taken, and they will never be returned to their (previous?) owners.
- They froze and seized protesters’ personal bank accounts, where people kept their own money
- Government can’t even confirm it didn’t freeze random citizens’ accounts just because they donated to the protesters. They say it’s “unlikely” that “small” donors were affected by the freeze, but that’s as much as we get in terms of assurance.
- banned hundreds of people from accessing their only sources of money
- Financially incapacitated hundreds without any specific criteria, without any due process, without anyone being charged with any crime.

The Canadian banking system was either powerless or uninterested in stopping the government.

<br>

### Russia Ukraine 

Russia invaded Ukraine in February 2022

- Russian banks banned from SWIFT payments
- EU banned imports

The sanctions that squeezed Russia's dollar currency reserves "increased the perceived risk that those debt assets can be frozen in the way that they've been frozen for Russia,"

Two days was all it took for the Ukrainian government to ask the public for crypto donations. They were the ones on the ground, they were the ones that understood the risks. They were the ones that saw this incredible tech as a way to get money quickly, safely, and without strings attached.

“Wire transfers slowed, taking days to reach the country. Wise, a cross-border payments company, lowered its cap for transfers into Ukraine from $14,000 to as low as $200 (though it has since lifted it again). But as the need for fast funds grew Ukraine has looked to crypto assets to help its war effort.”

Can you imagine waiting days for a wire transfer when your country is getting invaded?

It’s insanity.

Crypto allowed the Ukrainian government to ensure their day-to-day operations remained funded as they moved to a war-time economy. Ukrainian deputy minister for digital transformation Alex Bornyakov makes it quite clear:

“Our banks were limited, there were restrictions on our use of fiat currencies and we were rapidly running out of supplies,” he said. “Even if you manage to pay in fiat, a wire transfer takes a few days to reach the recipient. In the crypto world, it takes minutes.”

The speed and reliability of these transfers and payments may have made the difference for Ukraine’s resistance.

With the country having already received close to $100M worth of donations in crypto, which doesn’t account for all the money donated to refugees and other causes, it’s safe to say Bornyakov and the Ukrainian government are sure to not regret their decision to embrace tech.


<br>

9/11 led to surveillance, COVID led to compliance, what's next?

<br>

##### Foreign Influence Task Force

After 2016 Trump victory
- scapegoat social media companies as a way to get control over them
- DHS declares election infrastructure and media environment as needing protection
- Cybersecurity and Infrastructure Security Agency within DHS to protect media environment from foreign influence
- Created FITF  Foreign Influence Task Force with FBI to police domestic speech on social media platforms
- Organized social media

<br>

##### OSHA

OSHA overreach mask mandates

##### NIH

a federal award suspended or terminated by a federal agency. 

In July 2020, the National Institutes of Health (NIH) suspended a grant to EcoHealth that was being used to pay the WIV, in part, because the organization would not provide information about the research being supported with the grant. Since that time, EcoHealth has collected nearly $20 million in government grants.

In August 2022, NIH terminated the portion of the EcoHealth grant supporting the risky research with WIV altogether. While NIH acknowledges the requested documents may never be turned over,the Biden administration reinstated the suspended NIH grant to EcoHealth this year.

EcoHealth’s violations of federal laws and regulations and misspending of taxpayer money were outlined in aletter from the NIH and an audit conducted by the Department of Health and Human Services Office of Inspector General.

“Like China, EcoHealth will not cooperate with scientific investigations into the origins of COVID-19, and, as a result, we may never know what was happening inside the Wuhan Institute and the possible connection to the pandemic

