---
title: symbols
date: 2023-01-18 03:00:42
tags: [trading]
category: [ecosystem, concepts] 
---
<br>

<iframe id="odysee-iframe" style="width:60%; aspect-ratio:16 / 9;" src="https://odysee.com/$/embed/Sacrificial-Relationship:0?r=B6f1mjUy7ezh82xyUa4Q5pLkEiipwm5K" allowfullscreen></iframe>

#### symbols

A symbol is a partial representation of something not yet fully understood — a "best effort" attempt to understand something beyond one's current level of comprehension. 

A symbol can be expressed in words, but it can also be expressed in image, art, dance, ritual, myth, etc. 

The instrument of symbol is metaphor and can be expressed & understood in nearly any cultural representational medium, not just language. 

Metaphors in their simplest form are pragmatic as-if comparisons: a resembles (acts like) b; in other words, “a” is in the same natural category as “b” and therefore has the same functional meaning as “b”. 

When you're trying to understand something new, first you compare it to things you already understand, and then you effortfully differentiate it from there (i.e., solve et coagula). 

This is symbolic understanding or analogical cognition. 

Even something as primitive as a caveman acting out an animal's behavior to his fellow cavemen can be a symbol from this perspective, because it's an attempt to (pragmatically; functionally) understand something better by metaphorically representing it.



symbolism takes multiplicity, things disconnected from each other, brings them together in unity 

Symbol means two things joined to gether, two rivers meet at a symbol (greek word)

Symbols are not pointers , they are concentration of multiplicity into unity 

