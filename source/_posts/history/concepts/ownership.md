---
title: ownership
date: 2023-01-15 20:00:42
tags: [resiliency]
category: [ecosystem, concepts]

---

<br>

What does it mean to truly own something?

ownership is ability to exercise full control of something. property is sum of goods at an individual's command, wealth is sum of economic goods at individual's command.

property helps us meet our future needs and also prevents conflicts over scarce resources

without private property rights, people use goods without expectation of using them in the future, deprioritizing future state of goods
heavy discounting of the future

<br>

##### PROPERTY

Four types: consumer goods, durable goods, capital goods, monetary goods

With patents and copyrights, the government assigns to the holder the right to control the property of others.

##### TRUE OWNERSHIP

<br>

- you have full control over your property at all times
- nothing can be done to your property without your permission
  
<br>
  
Does your house fall into this category? Your car? Your savings, investments, the cash in your pocket?

It can all be confiscated. It can all be destroyed. Without your permission.

<br>

Whenever you deposit money into a bank, whatever bank it is, you surrender your legal title to the cash. The bank has no obligation to safekeep it. They can do whatever they want with it. They can invest it, lend it out, put it into a fund, use it to pay salaries, or use it in any way they see fit. No one’s checking.

This is called fractional reserve banking and it’s how the vast majority of banks operate. It’s also 100% legal. As long as the bank has a minimum amount in reserves, enough to ensure that they can respond to normal withdrawal patterns, then whatever’s in your checking account counts as money. This is what’s called a deposit liability.

