---
title: truth
date: 2023-01-18 03:00:42
tags: [trading]
category: [ecosystem, concepts] 
---
<br>

<b>Truth</b> is ironclad and irrefutable.

Thinking is the ability to think clearly and rationally about what to do or what to believe. It includes the ability to engage in reflective and independent thinking. 

Someone with critical thinking skills is able to do the following:

understand the logical connections between ideas;
identify, construct and evaluate arguments;
detect inconsistencies and common mistakes in reasoning;
solve problems systematically;
identify the relevance and importance of ideas;
reflect on the justification of one's own beliefs and values.
Critical thinking is not a matter of accumulating information. A person with a good m

A good thinker is able to deduce consequences from what he knows, and he knows how to make use of information to solve problems, and to seek relevant sources of information to inform himself.

Objects --> States --> Frames --> Claims 

Before communication, there were no claims, only perception

<br>

###### Truth

Most people have an agenda. they craft their image and say what they need to say to get what they think they need.

When you do this you falsify words to get your desired end while using the other person as the target of your manipulation.

But what do you know about what you need?

Don't falsify your words, they are divine.

Even though it looks bad for me, that doesn't mean it's bad. It just means I don't see the whole picture.

If you believe the truth sets you free, and there is such a thing as truth, and that truth is redemptive, then you're stuck with that conclusion.

And when you do that, you will have your adventure. What adventure are you having if you tell someone else's story? It's certainly not yours.  

And if it's not yours, maybe it's not good enough for you. Then you suffer, then you're bitter, then you're cruel, and then you become resentful, or you become arrogant.

Tell the truth, and have faith that whatever happens is the best possible outcome. Because the truth brings about what is best.

<br>


Cryptosovereignty - personal power, economic liberty, and political praxis that exist in bitcoin directly, crypto assets generally, and the internet widely.

It is the power of any single human — no matter their station of birth, class of wealth, or creed of faith — to choose to put their economic, social, and political rights into a new digital common-wealth that is inviolable and beyond the power of any and all governments to violate.

crypto totally disengages from traditional forms of sovereignty, law, and prohibition. Crypto creates a novel, new form of sovereignty though cryptographic systems which do not need any form of physical force to support them — just the code alone. This causes for traditional systems of sovereign power to unravel against crypto, as they cannot find a foothold from which they can execute their physical power

Cryptosovereignty is the newfound ability for any single human to choose to put their economic, social, and political power into a new crypto-commonwealth where the rules of the system can never be broken or violated; unlike all forms of contemporary sovereign law

Cryptosovereignty is the newly formed political power that each and every human has to refuse the transgressions and violations of state powers, and to choose to abandon these antiquated systems to create something better together using crypto.

What the Cyphernet does is replace the authority that guarantees law (which always constitutes with violence), with a system of truth that cannot profane itself, nor use violence to enforce its actions. Through exploring the relationship of the supremacy of blockchain truth-bearing capacity over state authoritarianism, and the majesty of mutual agreement to build power consensually through crypto alliances, over the subjugating violence of sovereign state law, we can start to understand the novel new form of power that the Cyphernet is constituting.

Only in a world in which all governments rule from the mantra of auctoritas, non veritas facit legem (authority, not truth makes legitimacy) does the power of Truth become revolutionary.

Only in a world where states regards themselves as Gods who are not accountable to truth would they be able to create fiat money. And only in a world where they regard themselves as Gods with the sole right to violence and excuse themselves from Truth of their own monstrous injustices could we understand how they create their monopoly money. It is not majesty, nor grandeur, nor even law that allows for them to rule; but raw, barbaric violence that grows from the barrel of a gun and the edge of a knife.

Crypto inverts Hobbes dictum of sovereign power (“Auctoritas, non veritas facit legem” (Authority, not truth makes legitimacy) in De Cive)

into

"Veritas, non auctoritas facit legem." (Truth, not authority makes legitimacy)


### Meaning 



WHAT IS RELIGION    Pageau and Boghossian 


Pageau 


The reason why we consider truth claims important are because of what they offer downstream.  Not arbitrary truth claims.  The Christian story is a model for being, it becomes the structure for our world, structures our perception, our morality, our ethics, all we care about 

Supply and demand is like care of truth claims 

--------------------------------------------------------------------

Air is free because it's unlimited.

Truth claim of clean air in Iceland is unimportant because no fallout.

BUT 

Air is expensive if it's limited supply. 

Truth claim of clean air in Iceland is important because fallout. 

--------------------------------------------------------------------

Question is always what level of factuality is necessary for you to believe something is true 

    - factual evidence 
    - my wife loves me 


Why I believe in religion 

    1. It's important (what it affords me)
    2. It's true 

Why I believe my wife loves me 

    1. It's important ()
    2. It's true 

* BENEFITS COME FIRST and makes you see the value of it

There are millions of things I don't care about, but certain things offer value and I care about them. 

If wife cheats, I don't believe she loves me, but I still want it to be true


Scale of Participation 

Sidewalk will hold me up... but something might happen to make me question that. 

Super triggered/cautious  -- Thomas Jefferson had slaves so America is evil. 
Super committed  --- It would take a lot more than that 


Resurrection 

"I can prove to you that resurrection is impossible"

Super triggered/cautious  --  ok, you're right 
Super skeptical          ---  what are your intentions,  



There are people that fake miracles all the time, lie about miracles, 

"Every time I hear a story of someone getting healed or some miracle or whatever... I'm like eehh ya  know i don't know I don't necessarily have an opinion on it. It doesn't mean that I don't believe miracles are possible."


How do you establish causality between meaning and events. 

What is the placebo effect?

Speech, care, attention, are related to healing.  Humans find healing in connection to meaning. Prayer is an invocation of participation in higher meaning, calling upon something that is higher than us, asking it to act with meaning down on being.  A reflection of the care of the people, you love them, care for them, 


It is because of meaning that I believe in God.  Same question. 

Meaning stacks vertically and moves up towards unity, there are meanings that transcend, they are agents, and act down. 

Whole is always more than the sum of its parts.  You and I have relationship, which is transient but it's real. 

All groups have something that pops up above it which becomes an identity which is causal vertically, it binds that group together 

That's a god, or angel, an active agent that binds multiplicity together into unity.  

Hierarchy of gods culminates into the absolute, you can see it scale up.  Little gods, bigger gods.  People can hold together in a family, city, state, continue to scale. 

The meaning-making agents, the agentic beings, the world lays itself out in hierarchies of beings, 

God is that which selects 

----

You propose some arbitrary being (slug) and ask what if it flew (then it would need to have wings)

The Telos that you're thinking about 

There's a variablility 

Human is microcosms, made in the image of God, not arbitrary, 

Why are you speculating about worlds that don't exist to explain this one?

We both suck at chess, and you say "what if there was a game where...."

-----

We see that meaning and purpose and patterns exist.  Not in the same way that things exist, but the way they bind things together. 


Bugs fly, bats fly, birds fly.. now we have a category, it's a purpose. A telos, sharing something in common towards a purpose. 

They have a coherence that resembles itself across species.  An objective pattern that if certain constraints are placed on certain things, that bring a being into a certain telos.  

That Telos pre-exists.  Because it is independent of the multiple, individual instantiations.  It's not dependent on the instantiations.  

A being perpetuates itself, autopoietic, and agentic.  

autopoietic - a asystem that produces and reproduces its own elements as well as its own structures.



----

You have stuff.  You notice patterns that are objective real like math is real, like proportions are real, like ordered relationships are real. 

It's agentic because they bind things into it for reasons and purposes.  Beings enter into a pattern based on characteristics it adapted to itself.  

Agentic patterns run through groups of people, they have a coherence.  War is the pattern, many different countries assume that pattern. 

That's what God is. 

Form of transcendent agency that is coherent and that binds groups of people into behaviors, that none of the people control, but that nonetheless plays itself out with a purpose, that no one can fully hold on to, that you can recognize across different spheres.  That's what you would call a god of war. A little god. 

The gods don't always want the same thing.  The tribes don't either, and they fight with each other.  Religion doesn't cause the war, the agency in one conflicts with the agency in others.  Lust, hunger, pride, recognizable across people, 

There is something that you can bind all those things together.  Being itself seems to align all these agencies together.  All knowing, all infinite, binds all multiplicity into unity 



Boghossian 

Religions have truth claims, sports teams don't  

---  sports have aspects that are true or we wouldn't care about them 
---  you wouldn't make a sport of someone rubbing the side of a table for 2 hours 


----------------------------------------------------------


Images and symbols from Old Testament   we've lost the mythological language, we don't know what they're about, we just 

Jordan shows coherence of the narrative, explain psychological point of view and how they represent how people exist in the world 

We can't speak to metaphysical, but psychologically they are accurate about the way we are and the way the world works 

Forensic idea of reality,  scientific description of world, alienates us from ourselves, the world is just an objective series of facts that you collate together 

The world collates facts towards purposes, 

There are animals we eat, those we tame, those we can't.  That's not a scientific description, 

"Humans eat rocks"
"Some humans eat rocks"
"Some humans eat some rocks"
"Some humans eat some rocks and get injured"
"Some humans eat some rocks and some get injured"
"Some humans eat some rocks, some get injured and some die"
"Some humans eat some rocks, some get injured, some die, some are fine"


"Rocks are not for eating, it's dangerous to eat them"
"It's dangerous to eat things that are 





