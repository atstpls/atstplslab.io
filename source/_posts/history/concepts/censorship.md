---
title: censorship
date: 2023-01-19 18:05:42
tags: [fiat, crisis]
category: [ecosystem, events]
---

<br>

Censorship prevents you from reaching agreement and disagreement

<br>

##### Mechanics

- First Amendment to the U.S. Constitution establishes government has no power to restrict expression because of its message, its ideas, its subject matter, or its content.
- Labeling speech as misinformation does not strip it of it's First Amendment protection.
- Some false statements are inevitable if there is to be open and vigorous expression of views in public and private conversation.
- Right to receive ideas follows from the sender's right to send them.
- CISA formed MDM Subcommittee (Misinformation adn Disinformatoin) which is disbanded now.
- Made up of Dr. Kate Starbird, Vijaya Gadde, former chief legal officer of twitter, suzanne spaulding, former assistant genereal counsel and legal adviser for CIA
- CISA recommneded use social media platforms, media, cable news, talk radio, and other online resources.
- CIS operates the MS-ISAC which acted as singgular conduit for election officials to report false or misleading information to platforms
- NewsGuard (tax-payer funded) urged advertisers to boycott disfavored publications and direct funding to favored ones
- Global Disinformation Index (tax-payer funded) urged advertisers to boycott disfavored publications and direct funding to favored ones

<br>

##### Analysis

- Censorship is used by those who do not wish to publicly engage with their opponents in an open exchange of ideas
- Rather, they create block-lists of disfavored people and then pressuring, cajoling, and demanding that social media platforms censor, deamplify, and even ban the people on these blocklists
- Run influence operations aimed at discrediting factual information under the guise of "fact-checking"

