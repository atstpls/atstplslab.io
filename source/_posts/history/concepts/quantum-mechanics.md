---
title: quantum mechanics
date: 2023-01-14 14:00:42
tags: [debate]
category: [reference]
---

<br>

Einstein's derivations only worked with speeds that were less than the speed of light 

Neils Bohr, Erwin Scheorder, Max Borne, Corderauc 

Individual particles hae a spread out wavelike quality, it's not an electromagnetic wave, it's a quantum wave, which is a probability wave

All we can do is predict the likelihood the probability of a particle being here or there 

There's quantum uncertainty, so the best you can do is predict probability 

<br>

## Conventional Quantum Mechanics 

Your intuition based on everyday experience has misled you into thinking you can talk about the position and speed of objects.

You can talk of one or the other or approximately about each, but you can't delineate both simultaneously with total precision. 

Macroscopic experience is a misleading guide to how the microscopic world works. 



Rule #8  KEEP YOUR TEAM in the loop 


