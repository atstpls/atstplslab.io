---
title: critical theory
date: 2023-04-11 13:11:47
tags: [money]
category: [ecosystem, resources]
---

<br>

<b>Critical Theory</b> is concerned with revealing hidden biases and underexamined assumptions and identifying ways in which society and the systems that it operates upon are going wrong. It's a Marxian theory, designed to rearrange and transform society to obtain more power.

<br>

##### Mechanics

- A belief that racism created by white people for their own benefit defines power and creates oppression in society
- Post modernism skeptical of science and culturally dominant ways of exxplaining the world in sweeping statements called metanarratives
- Rejection of objective reality known through scientific method
- They reject belief in objective knowledge, universal truth, science ( or evidence more broadly ) as a method for obtaining objective knowledge, the power of reason, ability to communicate straightforwardly via language, a universal human nature, and individualism

<br>

Four pillars:

1. Identity is socially constructed
2. Morality is not found but made    - based on cultureal or religious tradition, but is constructed by dialogue and choice
3. Deconstruction in art and culture -
4. Globalization - borders are social constructions

<br>

2 principles

1. Knowledge - Radical skepticism about whehter objective knowledge or truth is obtainable and a commitment to cultural constructivism
2. Political - Society is formed of systems of power and hierarchies which decide what can be known and how

<br>

Four main themes:

1. The blurring of boundaries - every discipline is intentionally complicated and problematized to deny objective validity and disrupt existing power systems
2. The power of language - language controls society and how we think
3. Cultural relativism
4. The loss of the individual and the universal

<br>

- Political focus on power as the guiding and structuring force of society, codependent on the denial of objective knowledge
- Power and knowledge are seen as inextricably entwined. Power decides what is factually correct but also what is morally good
- The powerful have organized society to benefit them and perpetuate their power by legitimating certain ways of talking about things as true
- These things spread throughout society, creating societal rules that are viewed as common sense and perpetuated on all levels
- Power is thus constantly reinforced through discourses legitimized or mandated within society
- The social system and its inherent power dynamics are the causes of oppression, not the willful individual agents
- Wants to deconstruct and resist all ways of thinking that support oppressive structures of power
- Wants to prioritieze narratives, systems, and knowledges of marginalized groups
- Rejects objective truth and reason, refuses to substantiate itself and cannot therefore be argued with
- Postmodern theory seeks not to be factually true, but to be strategically useful.. to bring about its own aims, morally virtuous and poitically useful by its own definitions
- Intersectionality accurately recognizes that it is possible to uniquely discriminate against someone who falls within an "intersection" of oppressed identities—say black and female—and that contemporary discrimination law was insufficiently sensitive to address this.

<br>

##### Analysis

Equity: the retributive redistribution of resources according to identity. The flow of the desired redistribution is from oppressor to oppressed identities, where identities are defined by skin color, primary sexual characteristics, sexual orientation, etc.

Individual micro-tactics come in three main categories of increasing forcefulness: subterfuge, exaggerating support and quelling dissent. These tactics are supplemented by the subversion of liberal decision-making norms and the insistence on informality in decision-making, which renders it vulnerable to intimidation. The forcefulness of tactics used is influenced primarily by the degree to which the Critical Social Justice perspective is entrenched in a given situation. The more Woke participants there are, the more the perspective is entrenched. The more it is entrenched, the more forceful and aggressive the tactics will be.

Micro-tactics are harnessed and used together towards the tactical goal of overwhelming every situation with Woke participants and in the ultimate service of the grand tactic of "Woke Viral Infection." It is through Woke Viral Infection that Critical Social Justice is spread from department to department, through university administrations, across disciplines and into funding agencies and governments. Woke Viral Infection is successful thanks the use of Woke crossover words such as "critical" and "diversity" that allow the Woke perspective to be introduced into situations where it was absent before.

Currently, there are just three terms that have been profoundly subverted and are being used to transform our society by Communists for Communist ends. These are inclusion, democracy, and citizenship. All three are being redefined together. Understanding how this manipulation is taking place is key to neutralizing it, and that starts with understanding how these three words are being strategically misused.

Critical Theory, if you don’t know, is the operative tool of the dominant strain of twentieth century Marxist Theory, which is sometimes called "Critical Marxism" and is associated with the Frankfurt School (Institute for Social Research). What it refers to is identifying and reframing concepts in terms of Marxist structural analysis.

In general, when Critical Marxists, including the Woke Marxists of today, say they are applying "criticism" to something, or considering it in a "critical" way, what they mean is that they are strategically redefining the key terms involved so that they are understood in terms of Marxist sociopolitical analysis. In other words, they’re reframing the meanings of words so that they are to be understood in terms of alleged exclusionary "power dynamics" that benefit one class of people and oppress another class of people

<br>

Justice

The notion of justice has to be shifted from a perspective of individuals finding fair treatment under the law in an impartial way to a new perspective where "fairness" takes into account a belief that the law is not and cannot be impartial and thus favors certain groups over others in its very construction. As a result, the law and its application has to be tilted in favor of those groups who are theorized to be structurally disadvantaged by the existing system, so justice follows from an underlying but hidden assumption of a need for partiality to "level the playing field." The concept of meting out justice remains intact, but impartiality under the law is replaced by intentional partiality under the law so that those Marxist Theory claims are "structurally disadvantaged" are given additional privileges relative to everyone else.

<br>

Education

The concept of education has to be understood and then redesigned according to this paradigm also—as does everything in society. Critical Marxism, you see, believes that the very terms of society itself are corrupt and structurally unjust and thus must be retooled to move the marginalized to the center and vice versa. Thus, "education" is criticized for being an unjust credentialing mechanism that allows those who accept the (unjust, corrupt) terms of the existing society to move into positions of power and authority from which they can ensure it reproduces itself. A genuine "education," or a "critical" education, then, is a political education that teaches people to understand society this way and to reject it. At present, we have been miseducating our children on these terms for at least thirty years.

<br>

Inclusion

For them, free societies with impartial laws don’t address the underlying structural power dynamics that create de facto exclusion or a sense of not being fully welcome. For example, racial and sexual minorities (and especially Woke activists within those groups) might be made to feel excluded, they insist, by virtue of a belief that straight, white men are the "defaults" in many positions of authority. Or, women might be excluded as a matter of circumstantial fact by the demands of motherhood, which quickly and neatly explains many of their views about abortion "rights" and the recently published notion in the New York Times that the maternal instinct is a myth created by men—that is, in Marxian terms, an oppressive ideological narrative that supports structural patriarchy.

When huge entities like the World Economic Forum (WEF) and United Nations (UN) say that their agenda is to transform the world to achieve a more sustainable and inclusive future, this is the subversion they’re relying upon. People who Marxist Theory says have been or are being excluded—i.e., for them, Leftists—must be actively included, which requires excluding everyone else, either by limits to occupancy or by deliberate censorship and purges.

<br>

Democracy

The underlying belief is that society is exploitative of certain groups (who are in the majority), and thus those people aren’t equal participants in the democratic process. They have to be made equal (the contemporary term for this adjustment of enfranchisement, opportunity, and privilege is equity; the term in Lenin’s day, which still has major purchase was Democratic Socialism).

When we hear players in politics or the media say that open discussion threatens to create "misinformation" that threatens "our democracy," this is very likely to be what they are talking about. (Never mind that we live in a republic, not a democracy.) They view their democracy—the only legitimate democracy—as "inclusive democracy" and thus one that must adjust shares, i.e., discriminate and suppress, in order to achieve its aims. As Lenin pointed out, it’s not that they want to be unfair; it’s that they have to be in order to get their way so that their evils can "wither away of their own accord" when they’re no longer needed anymore (when Communism arrives). (Spoiler: Communism never arrives; it’s fake.)

<br>

Citizenship

By redefining citizenship, though, Leftist activists can successfully subvert society entirely by rewriting its so-called social contract: the agreement between the State and its citizens that holds society together. Citizenship, as a concept, writes the terms of the social contract. By replacing citizenship with "inclusive citizenship," Leftists create a social contract that inherently advantages Leftism while intentionally disadvantaging everyone that opposes Leftism. This, in turn, creates the Socialist Democracy Lenin insisted would pave the way to Communism, at which point true inclusion will finally arrive—and we will finally have Social Justice.

Klaus Schwab, executive chairman of the World Economic Forum, wrote in his 2022 book The Great Narrative for a Better Future: The Great Reset, Book 2, that his explicit goal is to rewrite the social contracts of societies around the globe.

What they should change to, he writes, would favor sustainability and inclusivity as primary values, and the purpose of the "Great Narrative" is to foster this fundamental change in values at all levels of society in every society at once so that they can better cooperate on solving what he calls existential global challenges. Inclusive citizenship, in other words, will give way to global citizenship, which will have to be inclusive and redistributive by definition.

<br>

CRT

- Theory says systems have power, and it oppresses
- Activist movement based on systemic racism, and how that power causes oppression
- Mission is to transform relationship among race, racism, and power
- They don't want to understand it, they only want to change it   ( Marxian Theory  self-serving objectives )

<br>

Critical theories are parasitic constructs that mimic theories
- mimic the stucture and content of theories
- adapting the language and structure of real theories
- and complaining about their contents in marxian ways
- to force marxian theory into everything
- wants to label current thought as racist, sexist, ableist, homophobic, chauvenistic, eurocentric, oppressive 
- institutional virus
- If any inequality appears, it is because of racism
- Unconscious bias, since all the analytical tools are bent to favor whiteness, racial justice can't be conceived of in existing terms
- Self-serving claims.... we need CRT experts to uncover and judge what is racist and what is not
- "The master's tools can never dismantle the master's house"
- Only they can see the true state of social reality
- All other views are believed to uphold systemic racism.
- Socially constructed, imposed and maintained by white people to uphold systemic racism to benefit whites even though they don't know it
- Racial inequity is evidence of racial policy 
- CRT is a fundamentalist faith - interprets everything through a lens rooted in a particular text.. all analysis within the text 
- Anyone not doing their bidding is has selfish motives that they aren't conscious of
- White practices are unconscious habits that contribute to and reinforce racist systems
