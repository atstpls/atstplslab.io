---
title: kayfabe
date: 2023-01-14 14:00:42
tags: [debate]
category: [reference]
---

<br>

<a href="#debate">Kayfabe</a> is the organized structure of lies that undergirds the sport of professional wrestling and also in <a href="#debate">politics</a>. It's carnival speak for the word fake. A code word for everyone inside a promotion to maintain the deception for everyone outside the promotion.

<br>

##### Mechanics

<a href="#debate">Kayfabrication</a> is the process of transition from reality toward Kayfabe. Real activities that are dangerous and/or boring tend to get sanitized tend to get sanitized by the participants so that they can present an as if product to those outside the structure. Born out of general monotony and extreme danger. Elaborate fakery intended to produce stable careers for the participants and create an engaging and regular product for those in the audience. Happens in war, media, and journalism. 

A <a href="#debate">work</a> is a scripted match or sequence

A <a href="#debate">shoot</a> is a break in kayfabe, admitting fakery, or engaging in real competition 

A <a href="#debate">worked shoot</a> is a scripted shoot, a scripted break in kayfabe 

The audience believes and disbelieves what it sees at the same time. Eventually it becomes unclear where kayfabe stops and the real world begins.

Embedded Growth Obligations (EGO) - 

Kayfabe provides the most complete examlpe of the process by which a wide class of important endeavors transition from failed reality to successful fakery.

Kayfabrication arises out of attempts to deliver a dependably engaging product for a mass audience while removing the unpredictable upheavals that imperil the participants. It illustrates the limits of disbelief the human minds capable of successfully suspending before 