---
title: reciprocity
date: 2023-04-11 09:53:32
tags: [money]
category: [ecosystem, resources]
---

<br>

A mutual exchange of support, emotional investment, care, and love.

<br>

##### Mechanics

The ethic of reciprocity is built into our biology.

From a very young age, we begin to learn moral rules of engagement by playing games with others.

If a child is fun to play with, other kids will want to play with him, adults will want to teach him things.

A child that learns to put others before himself will be invited to future games, make more friends, learn more, improve more

Life is a series of games over time. Because of this, strategies that enable you to do well in all games work better than those designed to win specific games.

The best of us focus significantly more on building others up and helping them succeed than on trying to win individual games.

Rough play... self-handicapping so that both players benefit from the game 

Animals do it instinctively

<br>

##### Analysis

When you put winning first, you put winning the game above everything and everyone else.

You not only waste opportunities to build character and improve as an individual, but also waste opportunities to contribute to team goals and demonstrate your commitment towards the development of others.

You compromise your ability to win at life for the sake of winning a single game.



The dominance hierarchy is in fact a form of extended cooperation
    Establishes the frame for within-hierarchy striving
    Aggression is counterbalanced by two regulatory processes. One is innate and internal; the other, emergent and social. The
    internal process is empathy, the ability to feel another’s experiences

When human children are socialized, they learn socialized alternatives to violence, which serve as more effective means to social status. 
    They do not simply inhibit the primal aggressive circuits. 
    Instead, they integrate these circuits into more sophisticated behavioural games. 
    The child organizes her primary impulses into higher-order, low-resolution MAP schema, within the confines of the dominance hierarchies she inhabits.

Such organization is mediated by empathy, and then by play. 
    Play is early social cognition: when children play, they adapt their actions to each other. 
    They produce and then share a perspective, and work towards a common goal. 
    They embody the same MAP schema, to the benefit of both. 
    The capacity to do so unfolds developmentally, starting with the body, in direct physical contact with others’ bodies
    The maturing child begins by constructing small-scale motor patterns, designed to attain individually-motivated ends

As the child progresses, complex social understanding emerges. 
    The child imitates himself, using procedure to map procedure, at the initial, embodied stage of genuine representation
    Any successful MAP schema is immediately replicated, practiced, automatized and readied for future employment
    Imitation then extends to others
    Patterned social interactions begin to emerge, as the play partners’ exchange information about which (re)actions are desirable, and a prototypical morality emerges

Higher-order, more explicit, cooperative morality emerges around 7
    Each child now tries to win, to dominate the hierarchy of game achievement
    At first glance, this appears competitive
    However, all disagreements about the game have to be resolved before any attempt to play, let alone win, can begin, and all striving must remain civilized enough that the game can continue

It is the ability to establish these joint schemas that allows for the modulation of motivation and emotion toward some shared end
    Good games facilitate joint gain and there is little need for violence
    Well-socialized adults add their opinions to the process, insisting that the players’ play fair, and act as good sports

A personal MAP schema specifies a starting place, goal, objects of perception, and implication for emotion. 
    Construction integrates perception across individuals.  
    Diverse individuals inhabit the same space, cooperating to reach a goal and maintain the space's integrity. 
    This is how fundamental agreements emerge, nullifying the necessity for aggression, or for terror. 
    Emotion remains controlled when everyone plays the same game, with the same rules, at the same time.

We have neural mechanisms for imitation and abstraction of imitation.


<br>

Scrum is lightweight framework that uses adaptive solutions for complex problems.

It uses empirical thinking, cross-functionality, and self-management.

Its rules are designed to produce high-value increments and guide relationships and interactions.

<br>

##### Mechanics

Empiricism asserts that knowledge comes from experience and making decisions on what is observed.

Incremental approach to optimize predictability and control risk.

4 formal events to INSPECT and ADAPT:

5. events provides cadence:

5 values:

1. Commitment
2. Focus
3. Openness
4. Respect
5. Courage


Scrum Team is Scrum Master, Product Owner and Developers.  There are no sub-teams or hierarchies.

Important decisions are based on state of artifacts.

Lean thinking (respect, ) and Empiricism (using experience to make best decisions)

Don't go to customers for the solution, you need from the job that they need to get done

Cone of uncertainty.  Further you look out, more wrong you may be 

Waterfall analysis is a predictive approach. Reductionism to better understand things. 

Helps you build something usable or working in a month 

Mechanical systems are predictable/deterministic

Organic systems are uncertain/adaptive/empirical with holistic lens

Stacey Matrix shows simple, complicated, complex, chaotic 

