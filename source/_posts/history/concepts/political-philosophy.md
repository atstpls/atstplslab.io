---
title: political philosophy
date: 2023-03-15 20:00:42
tags: [resiliency]
category: [ecosystem, concepts]

---

<br>

Political philosophy or political theory is the philosophical study of government, addressing questions about the nature, scope, and legitimacy of public agents and institutions and the relationships between them. Its topics include politics, liberty, justice, property, rights, law, and the enforcement of laws by authority: what they are, if they are needed, what makes a government legitimate, what rights and freedoms it should protect, what form it should take, what the law is, and what duties citizens owe to a legitimate government, if any, and when it may be legitimately overthrown, if ever.

##### Liberatarianism

Libertarianism (from French: libertaire, "libertarian"; from Latin: libertas, "freedom") is a political philosophy that upholds liberty as a core value. Libertarians seek to maximize autonomy and political freedom, and minimize the state's encroachment on and violations of individual liberties; emphasizing the rule of law, pluralism, cosmopolitanism, cooperation, civil and political rights, bodily autonomy, freedom of association, free trade, freedom of expression, freedom of choice, freedom of movement, individualism, and voluntary association. Libertarians are often skeptical of or opposed to authority, state power, warfare, militarism and nationalism, but some libertarians diverge on the scope of their opposition to existing economic and political systems. Various schools of Libertarian thought offer a range of views regarding the legitimate functions of state and private power, often calling for the restriction or dissolution of coercive social institutions. Different categorizations have been used to distinguish various forms of Libertarianism. Scholars distinguish libertarian views on the nature of property and capital, usually along left–right or socialist–capitalist lines. Libertarians of various schools were influenced by liberal ideas.

Libertarianism originated as a form of left-wing politics such as anti-authoritarian and anti-state socialists like anarchists, especially social anarchists, but more generally libertarian communists/Marxists and libertarian socialists. These libertarians seek to abolish capitalism and private ownership of the means of production, or else to restrict their purview or effects to usufruct property norms, in favor of common or cooperative ownership and management, viewing private property as a barrier to freedom and liberty. While all libertarians support some level of individual rights, left-libertarians differ by supporting an egalitarian redistribution of natural resources. Left-libertarian ideologies include anarchist schools of thought, alongside many other anti-paternalist and New Left schools of thought centered around economic egalitarianism as well as geolibertarianism, green politics, market-oriented left-libertarianism and the Steiner–Vallentyne school. Around the turn of the 21st century, libertarian socialism grew in popularity and influence as part of the anti-war, anti-capitalist and anti-globalisation movements.

<br>

##### Communism

(from Latin communis, 'common, universal') is a left-wing to far-left sociopolitical, philosophical, and economic ideology within the socialist movement, whose goal is the establishment of a communist society, a socioeconomic order centered around common ownership of the means of production, distribution, and exchange that allocates products to everyone in the society based on need. Communist societies also promote the absence of private property and social classes, and ultimately money and the state. Communists often seek a voluntary state of self-governance but disagree on the means to this end. This reflects a distinction between a more libertarian approach of communization, revolutionary spontaneity, and workers' self-management, and a more authoritarian vanguardist or Communist party-driven approach through the development of a constitutional socialist state followed by the withering away of the state. As one of the main ideologies on the political spectrum, communism is placed on the left-wing alongside socialism, and communist parties and movements have been described as radical left or far left.[note 1]

<br>

##### Socialism

Socialism is a political philosophy and movement encompassing a wide range of economic and social systems which are characterised by social ownership of the means of production, as opposed to private ownership. As a term, it describes the economic, political, and social theories and movements associated with the implementation of such systems. Social ownership can be public, community, collective, cooperative, or employee. While no single definition encapsulates the many types of socialism, social ownership is the one common element, and is considered left-wing. Different types of socialism vary based on the role of markets and planning in resource allocation, on the structure of management in organizations, and from below or from above approaches, with some socialists favouring a party, state, or technocratic-driven approach. Socialists disagree on whether government, particularly existing government, is the correct vehicle for change.

<b>Socialism</b> is a post-capitalist society with no ruling class, no top-down control, where whole society collectively owns and controls natural resources, industries, etc.

<br>

##### Mechanics

- people work voluntarily to provide the goods and services society needs
- everything is owned, free access, money is obsolete
- class-free, state-free, money-free society
- why will people do it?  because they want to  
- why won't people take more than they need?  because they can't resell for profit
- who will be in charge ?  everyone... also elected administrators, 
- who will deal with selfish and antisocial and criminal behavior if there is no state?   there won't be any of those 

<br>

##### Fascism

is a far-right, authoritarian, ultranationalist political ideology and movement, characterized by a dictatorial leader, centralized autocracy, militarism, forcible suppression of opposition, belief in a natural social hierarchy, subordination of individual interests for the perceived good of the nation and race, and strong regimentation of society and the economy.

Fascism rose to prominence in early 20th-century Europe. The first fascist movements emerged in Italy during World War I, before spreading to other European countries, most notably Germany. Fascism also had adherents outside of Europe. Opposed to anarchism, democracy, pluralism, liberalism, socialism, and Marxism, fascism is placed on the far-right wing within the traditional left–right spectrum.

Fascists saw World War I as a revolution that brought massive changes to the nature of war, society, the state, and technology. The advent of total war and the mass mobilization of society erased the distinction between civilians and combatants. A military citizenship arose in which all citizens were involved with the military in some manner. The war resulted in the rise of a powerful state capable of mobilizing millions of people to serve on the front lines and providing logistics to support them, as well as having unprecedented authority to intervene in the lives of citizens.

Fascism rejects assertions that violence is inherently bad and views imperialism, political violence and war as means to national rejuvenation. Fascists often advocate for the establishment of a totalitarian one-party state, and for a dirigiste economy, with the principal goal of achieving autarky (national economic self-sufficiency) through protectionist and economic interventionist policies. Fascism's extreme authoritarianism and nationalism often manifests as belief in racial purity or a master race, usually blended with some variant of racism or bigotry against a demonized "Other", such as Jews. These ideas have motivated fascist regimes to commit genocides, massacres, forced sterilizations, mass killings, and forced deportations.

<br>

##### Conservatism

is a cultural, social, and political philosophy that seeks to promote and to preserve traditional institutions, practices, and values. The central tenets of conservatism may vary in relation to the culture and civilization in which it appears. In Western culture, depending on the particular nation, conservatives seek to promote a range of social institutions such as the nuclear family, organized religion, the military, property rights, and monarchy. Conservatives tend to favor institutions and practices that guarantee stability and evolved gradually. Adherents of conservatism often oppose certain aspects of modernity (for example mass culture and secularism) and seek a return to traditional values, though different groups of conservatives may choose different traditional values to preserve.

The first established use of the term in a political context originated in 1818 with François-René de Chateaubriand during the period of Bourbon Restoration that sought to roll back the policies of the French Revolution. Historically associated with right-wing politics, the term has since been used to describe a wide range of views. There is no single set of policies regarded as conservative because the meaning of conservatism depends on what is considered traditional in a given place and time. Conservative thought has varied considerably as it has adapted itself to existing traditions and national cultures. For example, some conservatives advocate for greater economic intervention, while others advocate for a more laissez faire free-market economic system. Thus, conservatives from different parts of the world—each upholding their respective traditions—may disagree on a wide range of issues. Edmund Burke, an 18th-century politician who opposed the French Revolution but supported the American Revolution, is credited as one of the main theorists of conservatism in the 1790s.

<br>

##### Marxism

is a left-wing to far-left method of socioeconomic analysis that uses a materialist interpretation of historical development, better known as historical materialism, to understand class relations and social conflict and a dialectical perspective to view social transformation. It originates from the works of 19th-century German philosophers Karl Marx and Friedrich Engels. As Marxism has developed over time into various branches and schools of thought, no single, definitive Marxist theory exists.

In addition to the schools of thought which emphasize or modify elements of classical Marxism, various Marxian concepts have been incorporated and adapted into a diverse array of social theories leading to widely varying conclusions. Alongside Marx's critique of political economy, the defining characteristics of Marxism have often been described using the terms dialectical materialism and historical materialism, though these terms were coined after Marx's death and their tenets have been challenged by some self-described Marxists.

Marxism seeks to explain social phenomena within any given society by analysing the material conditions and economic activities required to fulfill human material needs. It assumes that the form of economic organisation, or mode of production, influences all other social phenomena, including broader social relations, political institutions, legal systems, cultural systems, aesthetics and ideologies. These social relations and the economic system form a base and superstructure. As forces of production (i.e. technology) improve, existing forms of organising production become obsolete and hinder further progress. Karl Marx wrote: "At a certain stage of development, the material productive forces of society come into conflict with the existing relations of production or—this merely expresses the same thing in legal terms—with the property relations within the framework of which they have operated hitherto. From forms of development of the productive forces these relations turn into their fetters. Then begins an era of social revolution."

These inefficiencies manifest themselves as social contradictions in society which are, in turn, fought out at the level of class struggle. Under the capitalist mode of production, this struggle materialises between the minority who own the means of production (the bourgeoisie) and the vast majority of the population who produce goods and services (the proletariat). Starting with the conjectural premise that social change occurs due to the struggle between different classes within society who contradict one another, a Marxist would conclude that capitalism exploits and oppresses the proletariat; therefore, capitalism will inevitably lead to a proletarian revolution. In a socialist society, private property—as the means of production—would be replaced by cooperative ownership. A socialist economy would not base production on the creation of private profits but on the criteria of satisfying human needs—that is, production for use. Friedrich Engels explained that "the capitalist mode of appropriation, in which the product enslaves first the producer, and then the appropriator, is replaced by the mode of appropriation of the products that is based upon the nature of the modern means of production; upon the one hand, direct social appropriation, as means to the maintenance and extension of production—on the other, direct individual appropriation, as means of subsistence and of enjoyment."

<br>

##### Anarchism

is a political philosophy and movement that is skeptical of all justifications for authority and seeks to abolish the institutions it claims maintain unnecessary coercion and hierarchy, typically including, though not necessarily limited to, governments, nation states, and capitalism. Anarchism advocates for the replacement of the state with stateless societies or other forms of free associations. As a historically left-wing movement, this reading of anarchism is placed on the farthest left of the political spectrum, it is usually described as the libertarian wing of the socialist movement (libertarian socialism).

Humans have lived in societies without formal hierarchies long before the establishment of states, realms, or empires. With the rise of organised hierarchical bodies, scepticism toward authority also rose. Although traces of anarchist ideas are found all throughout history, modern anarchism emerged from the Enlightenment. During the latter half of the 19th and the first decades of the 20th century, the anarchist movement flourished in most parts of the world and had a significant role in workers' struggles for emancipation. Various anarchist schools of thought formed during this period. Anarchists have taken part in several revolutions, most notably in the Paris Commune, the Russian Civil War and the Spanish Civil War, whose end marked the end of the classical era of anarchism. In the last decades of the 20th and into the 21st century, the anarchist movement has been resurgent once more, growing in popularity and influence within anti-capitalist, anti-war and anti-globalisation movements.

Anarchists employ diverse approaches, which may be generally divided into revolutionary and evolutionary strategies; there is significant overlap between the two. Evolutionary methods try to simulate what an anarchist society might be like, but revolutionary tactics, which have historically taken a violent turn, aim to overthrow authority and the state. Many facets of human civilization have been influenced by anarchist theory, critique, and praxis.





Capitalism is built on the concept of private property. In capitalism, everyone benefits if an entrepreneur does well, and the entrepreneur suffers if he fails.

<br>

##### Mechanics

- Capitalism is an entrepreneurial system, not a managerial system.  So problems arise when we attempt to manage it.
- Capital should be controlled by the people who own it.
- Capital owners are those who stand to gain the most from its success and also stand to lose the most from its failure.

<br>

##### Analysis

- Free markets operate under natural law and reward those that provide the most value to others.
- Given the same resources, areas with more people will be more productive than less people
- It is because more non-rival ideas, more technology, leads to faster rate of growth 
- Human reason is the essential ingredient to technological progress
- Marginal utility declines as the quantity of the good rises
- The law of returns asserts that for the combination of economic goods of the higher orders, there exists an optimum.
- Time is the ultimate resource because of its irreversibility.  
- The scarcity of time is dedicated to two broad categories: labor and leisure
- The opportunity cost of labor is leisure and vice versa 
- Scarcity gives value to time.  Time is an economic good because it is scarce.
- utility is subjective and is always changing
- There are no units to measure utility
- Can't measure the value they give you into something that is interchangable with a constant unit of measurement
- Mises - economics is not quantitative and does not measure because there are no constants and no constant relationships.
- Reason, understanding, and action are the methods by which human beings act.
- There is a difference between valuing in the aggregate and valuing at the margin.
- Humans act because we have the ability to discover causal relations
- Natural sciences contain restraints and social sciences do not.
- The price of capital is determined by time preference
- originary interest - ratio of present good to a future identical good
- the loan market adjusts loan interest rate to be equal to originary interest
- only expectation of a real return
- capital is only accumulated in processes of production that are longer more productive
- lenders only lend if offered a return that compensates foregoing present goods
- capital good value is purely derived from value of the consumption goods they provide


Prices are a reflection of the actions of individuals 



Bureaucracies have helped develop nation states.  The unification and governance of large territories, capability for national defense, and building infrastructure are key functions. These required the creation of tax systems.

1900s saw goverments delivering a wide range of public services to their citizens such as health care, welfare, education and social security.

For much of the post war period, the treatment of bureaucracy was tied to the more general absence of a nuanced understanding of what made states effective in supporting
economic development, with more attention paid to what good policies looked like rather than how purposeful government could bring them about.

1. "meritocratic recruitment" Are appointment decisions in the state administration are based on skills and merit as opposed to personal and political connections?
2. “rigorous and impartial public administration” and is based on an assessment of whether public officials generally abide by the law and treat like cases alike, avoiding arbitrariness and bias


- We need to delineate more clearly the roles of politicians and bureaucrats. They often perform different roles and are subject to different accountability mechanisms. Since bureaucrats are, in principle, accountable to politicians, making politics work better is potentially crucial to increasing bureaucratic effectiveness.
- The relationship between the private sector and bureaucrats requires more attention. There are many examples where the two work together to promote private sector development, but also where the private sector has “captured” politicians and bureaucrats.
- There are multiple dimensions to the relationship between bureaucrats and the private sector. The privatisation of utilities and the collaboration between bureaucrats and the private sector in the provision of infrastructure.
- The COVID-19 pandemic has uncovered large heterogeneity in the capacity of bureaucracies to respond. If we think about future challenges, such as climate change, it is clear that one needs to identify what characteristics of bureaucracies are needed to respond to these key challenges developing countries will face which may be different to the challenges faced in the last century.
- Bureaucratic corruption as a determinant of capital controls.
- Capital control is an instrument of fmancial repressioan--it entails efficiency loss for the economy, it also generates implicit revenue for the government
- The more severe the bureaucratic corruption in a country, the more difficult it is to collect formal taxes. As a result, the government has to rely more on capital controls/financial repression