---
title: luciferian intellect
date: 2023-01-04 19:00:42
tags: [analysis, media]
category: [ecosystem, actors]
---

<br>

The Luciferian intellect is holding rationality as the highest value... which produces false arrogance.

We are agents playing "games of incomplete information" while also making transcendent claims based solely on "rational intuition".

The pride of the intellect puts what we want (demand for a truth) above what is (truth). 

Another example is putting the letter of the law above the spirit of the law.

This can result in dogmatic adherence to rational frameworks or legalistic interpretations, prioritizing the 'letter' over the 'spirit' of the law, thus missing the broader, more profound truths or intentions.