---
title: free speech
date: 2023-04-10 09:53:32
tags: [money]
category: [ecosystem, resources]
---

<br>


We mostly think in words, we pose ourselves questions in speech and receive answers in speech 

You use internal speech to dissect the answer, layout dialogue between oppositional speaker for the proposition and critic

Or you can talk to others, to organize ourselves, consciously engaging with possibility 

Societies that are going to function, must let their citizens use free speech to collectively grapple with complexity 

Society depends on our ability to adapt to the changing horizon of the future on the free thought of the people who compose it   

Mediated through speech 


|||
|-|-|
|Gordon Klein|Klein, a UCLA professor, was suspended from his job for politely asking why it made sense to give black students special treatments on exams|
|Stu Peters|BBC radio host Peters lost his job for question white privilege and saying that "all lives matter"|
|James Bennett|NYT editor lost his job for running an editorial from a senator suggesting that the federal government quell riots.Almost every NYT journalist posted on social media that this editorial put black NYT staff in danger|
|Stephen Hsu|Hsu lost his position at MSU for committing the crime of science (he shared data that there may be racial intelligence differences). Steven Pinker, Jonathan Haidt, and thousands of academics signed a petition in support of Hsu, but it wasn't enough to keep his job.|
|Rev. Daniel Patrick Moloney|Moloney, a chaplain at MIT, lost his job for the sin of saying George Floyd's life wasn't virtuous.|
|Cardboard|Wizards of the Coast bans 7 Magic cards from play for being racist|
|Terese Nielsen|Wizards of the Coast fires long-time illustrator Terese Nielsen for liking tweets in support of Donald Trump.Nielsen was a gay woman who wrote this heartfelt letter, but it wasn't enough to save her long career|
|David Shor|Shor, a Democratic researcher, lost his job for sharing data that said riots would hurt the ability for Democrats to win elections. His employer said that sharing such data was "tone deaf"|
|Tiffany Riley|Riley, a Vermont school principal, lost her job for believing that "Black Lives Matter, but questioning some of their methods. The school board was "uniformly appalled" by this statement: “I firmly believe that Black Lives Matter, but I DO NOT agree with the coercive measures taken to get to this point across; some of which are falsified in an attempt to prove a point. While I want to get behind BLM, I do not think people should be made to feel they have to choose black race over human race. While I understand the urgency to feel compelled to advocate for black lives, what about our fellow law enforcement? What about all others who advocate for and demand equity for all? Just because I don’t walk around with a BLM sign should not mean I am a racist [sic]”. Upadate: Riley won a $650k settlement|
|Holy Land Hummus|Holy Land Hummus, a midwest food chain, lost its distribution deal with Costco and at least one lease over posts the CEO's daughter made in 2012 when she was 14.|
|Thomas Jefferson|Statues of Jefferson, Washington, and other famous white men of history, were pulled out of schools and towns around the country|
|Aleksandar Katai|Katai, a professional soccer player for the LA Galaxy, had his contract terminated after his wife called rioters disgusting and suggested it was okay to shoot them.|
|Howard Uhlig|Uhlig lost his job as the editor of the Journal of Political Economy, after left economists Paul Krugman and Justin Wolfers led an attack on social media against him.|
|Sue Schafer|Sue Schafer was a reporter at the Washington Post that lost her job for wearing blackface specifically to make fun of people wearing blackface. Two of her own colleagues took her out.|
|Michael Korenberg|Michael Korenberg lost his position on the board of the University of British Columia for liking tweets from Republicans|
|Melissa Rolfe|Rolfe is the stepmother of Garrett Rolfe, the police officer who killed Rayshard Brooks in Atlanta. The company discovered that she "created an uncomfortable working environment" and let her go shortly after the incident|
|W. Ajax Peris|Peris, a lecturer at UCLA, was condemend and investigated for reading MLK's "Letter from a Birmingham Jail" out loud (it contains racial slurs)|
|Lee Fang|Lee Fang, a writer at the Intercept, interviewed a black man named Max Fr. In the interview, Max said: "I always question, why does a Black life matter only when a white man takes it?... Like, if a white man takes my life tonight, it’s going to be national news, but if a Black man takes my life, it might not even be spoken of… It’s stuff just like that that I just want in the mix." Tens of thousands of people decried him as a racist for sharing this interview, including his own colleagues, and was investigated by HR at the Intercept.
Lee was able to keep his job by taking a knee and promising not to publish anything in the future that might upset other "journalists"|
|Mike McCulloch|McCulloch, a popular physicist and blogger, was formally investigated by the University of Plymouth for liking tweets.|
|Looped Rope|Thanks to Bubba Wallace, it won't be safe to tie a looped pull cord in America for some time.|
|University of Michigan professor|Removed from post after woke freshman complains about him showing Oscar-nominated acting legend Laurence Olivier in blackface in 1965 film Othello|




