---
title: disinformation campaigns
date: 2023-06-18 01:00:42
tags: [resiliency, government]
category: [ecosystem, resources]
---

<br>

Government agencies, charitable philantropies, NGOs, social media platforms, academic resesarch institutions discrediting factual information in order to persuade people to dismiss legitimate theories and consider false theories.

<br>

##### Trump-Russian Collusion Conspiracy Theory, 2016–2019

<b>A conspiracy theory that Donald Trump colluded with Vladimir Putin and the Russian government to steal the 2016 election.</b>




<br>

##### The Hunter Biden Laptop Conspiracy Theory, 2020–2021

[Biden Laptop](./biden-laptop)

<br>

##### Delegitimizing the COVID Lab Leak Theory, 2020–2021

<b>An organized effort by government to discredit theory that COVID-19 originated in a Chinese laboratory"

- Humanized mice were used to discover how a coronavirus, in particular, could be made more deadly, more pathogenic, and more transmissible
- USG banned gain of function research in 2014, NIH continued researching it, Fauci knew it was still occurring and being funded by USG
- The NIH funded EcoHealth Alliance which in turn gave money to Wuhan Institute of Virology (WIV) for gain-of-function research
- Emails show severak researchers told NIH head Francis Collins and NIAID's Anthony Fauci in February 2020 that a lab leak was possible and likely
- WHO sent team to china headed by Daszak to investigate the pandemic’s origins who reported it was extremely unlikely virus had been released from a laboratory
- Daszak and the WIV spread false information that the lab leak theory had been debunked and that it was a conspiracy theory
- Lancet, Science, Nature, NIH, and WHO all went along with dismissing the lab leak hypothesis
- Those institutions all allowed Daszak, who had a clear and obvious conflict of interest, to drive the investigation and communications around COVID’S origin
- mainstream news media dismissed the COVID lab leak hypothesis as a conspiracy theory
- week after Biden took office, Facebook announced, "we are expanding our efforts to remove false claims… This includes claims such as… COVID-19 is man-made or manufactured."
- Facebook/Instagram censored claims the virus escaped from a lab in China
- Claim was advanced by NIH head Francis Collins and NIAID’s Anthony Fauci, who oversaw the U.S. government’s response to COVID
- Collins and Fauci publicly dismissed the lab leak theory as a conspiracy theory even though they knew it wasn’t
- Fauci was instrumental in offshoring this research to Wuhan after Obama banned it on U.S. soil
- Media debunked it, Facebook censored people who shared it

<br>

Facebook (Meta) now being sued, and has justified censoring accurate vaccine information by claiming it constituted an opinion, even though it attached a fact-check label to it.

Meta, said in response to the lawsuit that
Facebook’s “fact-checks” are just “opinion” and thus immune from
defamation charges.153

<br>

##### Vaccine as Only Solution

<b>Biden Administration pressured social media companies to censor accurate information that could cause vaccine hesitancy.</b>

<br>

- Food & Drug Administration (FDA) can use Emergency Use Authorization (EUA) to authorize unapproved products for use.
- Product required to be effective, safe (benefits outweigh the risks), and only available option
- 80 percent of WHO budget is earmarked contributions--they're driven by donor interests
- BMGF, WellCome Trust, GAVI & CEPI paid WHO to declare that vaccines were the answer and paid USG to avoid lawsuits
- These 4 along with USG claimed scientific consensus while censoring opposing expert opinions.
- Ran disinformation campaigns to suppress effective treatments and encourage vaccine use (mutations, side effects, effectiveness)
- White House pressured Facebook to censor accurate COVID vaccine side effects information because it didn't want to cause vaccine hesitancy
- Fauci stated all three vaccines are 100 percent safe effective against death and hospitalization
- Study performed by employers of Pfizer
- Vaccine not proven to prevent transmission of infection

<br>

DOE and FBI now believe laboratory leak was more likely than natural causes to have caused the coronavirus pandemic.

<br>

##### Mask Efficacy

- Twitter removed a tweet by a member of the White House’s coronavirus task force who questioned the efficacy of masks
- White House Press Secretary Jen Psaki said the Biden administration was identifying "problematic" COVID posts for Facebook to censor
- YouTube removed a video of Harvard and Stanford scientists telling Florida’s governor that children should not be required to wear masks
- Facebook censored former New York Times journalist John Tierney for accurately reporting on evidence of the harm to children from wearing masks




Disinformation campaigns undermining democratic institutions in the name of democracy.

Government agencies, charitable philantropies, NGOs, social media platforms, academic resesarch institutions.

<br>

##### Mechanics

The words "conspiracy theory" and "disinformation" were used to persuade people to dismiss legitimate theories and/or stop considering alternative hypotheses.


- Global Disinformation Index (tax-payer funded) spread disinformation supporting COVID lab leak and Hunter Biden laptop
- NewsGuard (tax-payer funded) spread disinformation supporting COVID lab leak and Hunter Biden laptop


Bill Gates Foundation given 319 million to media outlets

https://thegrayzone.com/2021/11/21/bill-gates-million-media-outlets-global-agenda/



France's Digital Minister threatens to ban Twitter from entire EU, free speech having impact on far-left governments and institutions

Elon withdrew Twitter from EU's voluntary code of practice against disinformation

EU wants to decide what disinformation on Ukraine/Russia, Gender Ideology, Climate Change, Pharmaceutical products, government regulations



#### Martin Luther King Jr 




#### Jean Seberg 



