---
title: inequality
date: 2023-01-14 15:00:42
tags: [inequality]
category: [ecosystem, concepts]
---

<br>

People get paid based on the scarcity of their talent.  Some coaches and athletes make more than doctors and lawyers.

<br>

##### Mechanics

- Inequality is built into nature via hierarchies, and we must manage it. Throughout history there have always been hierarchies which produced inequality.
- Modern hierarchies are based on competency rather than power. People choose to hire a plumber based on knowledge and skill rather than out of fear and intimidation.
- Economic inequality poses a destabalizing threat to society. When individuals cannot find a path to attain status through competency, they often resort to using power.
- We need to structure social systems so that non-violent means of attaining status are available (equality of opportunity).

<br>

##### Analysis

ANNALS OF ECONOMICS AND FINANCE 14-1, 111–128 (2013)
Categorical Segregation from a Game Theoretical Approach*
Antoni Rub´ı-Barcel´o

http://aeconf.com/Articles/May2013/aef140106.pdf

Information asymmetry
Does social interactions & categorization affect accuracy of predictions about others' qualitative feature.

<br>

- Individuals organize themselves in communities to solve problems.
- Each player produces for community, consumes what community produces, and player utility becomes dependent on composition of own community.
- Each player has physical, observable characteristic that is payoff irrelevant.
- Each player has a qualitative aspect that may be ignored by others and whose value affects others' payoffs.
- Information from social interactions is stored by observer in folders so scarce that force categorizing more coarsely the experiences with the members of less frequent social groups.
- Minority social identities might be systematically excluded from community including high productivity agents as a consequence of being more coarsely sorted due to their scarcity.
- Segregation can arise among self-interested players even when they have no reason to discriminate by social identity, because preferences are not affected by this aspect.
- Equal oppotunity laws are premised on notion that intergroup bias is malevolent in origin.
- But this shows discrimination and segregation can result from things other than descriminatory intent.

###### Experiment

- Information asymmetry
- Does social interactions & categorization affect accuracy of predictions about others' qualitative feature.
- Individuals organize themselves in communities to solve problems.
- Each player produces for community, consumes what community produces, and player utility becomes dependent on composition of own community.
- Each player has physical, observable characteristic that is payoff irrelevant.
- Each player has a qualitative aspect that may be ignored by others and whose value affects others' payoffs.
- Information from social interactions is stored by observer in folders so scarce that force categorizing more coarsely the experiences with the members of less frequent social groups.
- Minority social identities might be systematically excluded from community including high productivity agents as a consequence of being more coarsely sorted due to their scarcity.
- Segregation can arise among self-interested players even when they have no reason to discriminate by social identity, because preferences are not affected by this aspect.
- Equal oppotunity laws are premised on notion that intergroup bias is malevolent in origin.

<br>

CONCLUSION:  Discrimination and segregation can result from things other than descriminatory intent.

<br>

