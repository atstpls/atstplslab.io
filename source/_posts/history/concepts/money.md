---
title: money
date: 2023-01-18 02:00:42
tags: [history, tools]
category: [ecosystem, concepts]
---
<br>

In a true free market, individuals choose the best money, which becomes <b>the most marketable commodity</b>. Money allows all prices to be expressed in terms of one good and makes economic decisions much easier.

<b>We acquire money so we can trade it for other goods</b>. This is how we plan and provide for the future.  Since money represents value, the best money is the most durable, the most reliable, the most resistant to theft and manipulation.

<br>

##### Mechanics

Money must meet 3 requirements:

<br>

1. medium of exchange
2. a store of value
3. a unit of account.  

<b>Soft money</b> loses its value over time and undermines the ability to store wealth. It cannot provide true property rights and so will not prevent theft and manipulation.

<br>

<b>Hard money</b> protects value across time which gives people a bigger incentive to think of their future. It allows for trade to be based on a stable unit of measurement, free from government control and coercion, and eliminates the need to trust third parties.

##### Analysis

The primary function of money is ensuring every member of the economy is accountable to the real wealth that is being produced.


- Hard money is hard to produce.USD is easy to produce. Gold is better but it's not hard enough. Gold miners can control supply.
- Soft money undermines the ability to store wealth to plan and provide for the future.A currency that loses its value over time makes storing wealth extremely difficult.A currency that cannot provide true property rights will not prevent theft and unauthorized use of our wealth.
- Main function of money is more than just allowing exchange of goods, It's making sure that every member of economy is accountable to the real wealth that's being produced.
- Money has always been something we're confident other people will value in the future.When money is gold, cooperation between people and nature is sustainable. -Roy Sebag Inflation is an indication that we must produce more real things.
- Real money must be a MEASURE and a REWARD and fiat is neither.Gold is a good measure, attributes like weight are constant, hard to produce, reliable.Gold is a good reward, it has many uses, has demand.

<br>

Prices are a reflection of the actions of individuals.  They are driven by human nature.
They give us indicators of upper or lower bounds of individual valuation.





Market prices are not used for value, rather they give us indicators of upper or lower bounds of individual valuation. A buyer pays ten dollars for a book because he values the book more than he values ten dollars.  Likewise, the seller values the book less than ten dollars.

Money solves the many problems trading presents:

|Problem|Solution|
|-|-|
|Goods are not the same|One|
|Want to sell today, buy in future|Durable|
|Want to sell large, buy something small|Divisible|
|Want to sell in one location, buy in another|Transportable|

<br>

People are free to choose anything as money, but over time some things will function better. Certain commodities came to be used as a medium of exchange naturally.

[Gold](/gold.md) became superior money because it does not corrode, cannot be synthesized, and has the highest stock-to-flow ratio (annual production for any year is tiny compared to stockpiles) which resists debasement through overproduction.



<b>Production</b> is a process with a plan for combining land, labor, capital, and technology to produce a final good.

<br>

##### Mechanics

Human tools for increasing production:

|||
|-|-|
|Labor|Doing things not for their own sake, but for the outcomes they provide us|
|Capital|Creating not for consumption, but for the production of other goods (higher order goods)|
|Technology|A non-rival, non-physical form of capital. Concepts and ideas we use to achieve economic outcomes|
|Division of Labor|Specializing in producing goods that can be produced at low cost and trading to get other goods|
|Money|Solves the coincidence of wants problem... you can't build an economy on barter|

<br>

##### Analysis

The production process is designed to increase the subjective value of goods and services for society. There is a scale of production at which efficiency is maximized which results in profit. Individuals succeed by having superior foresight and judgement, finding undervaluation of certain factors by the market.

This results in surplus/proft.



<b>Cryptocurrency</b> is digital currency that uses encryption instead of banks to verify transactions

<br>

##### Mechanics


- Blockchains are P2P network of nodes where each contain copy of code and data
- Code is smart contracts, programs that run on the chain

<br>

- Store of value - Bitcoin
- Smart contract - Ethereum, Binance  
- Oracle - bring real world data to DApps (Chainlink)
- Payment - fast and easy payments (BCH, Dash, TEL)
- Privacy - Monero, Zcash  
- Exchange - Binance, KuCoin  
- Meme - Doge, Shiba

<br>

Gen1 - Bitcoin  (store of value)
Gen2 - Ethereum, Binance  (smart contracts)  have utility  
Gen3 - Cardano, IOTA

- Code is smart contracts, programs that run on the chain

<br>

- Decentralized ledger tracks accounts, balances, and transactions, all nodes keep a copy 
- Every transaction is sent to all other nodes and added to the blockchain 
- Miners compete to solve complex math problems which require computing power. Winner gets to write the block into the ledger. (Proof of work) (ETH 1.0)
- Staking deposit funds on a node, nodes compete to forge a block, random winner gets coins as reward (Proof of stake) (ETH 2.0)
- Ethereum is a platform for running dApps built on blockchain , apps will run on ETH network rather than one single system 
- Allows individuals to conduct business directly without central authority. Smart contracts are if/then statements that ethereum executes. 
- Wallets hold private keys that allow access to resources on the blockchain 
- Sign transactions with key (digital signature) and send to network 
- DAPP runs client code which talks to blockchain instead of web server 

<br>

While standard web applications like Twitter and Uber run on the computer systems of their organizations, which have full control over the apps, dApps run on an immutable blockchain and are inherently censorship-resistant.

Storing it? Split it up, memorize or make permanent somehow

Mainframe > PC > Cloud > Blockchain

<br>

###### ETHEREUM

Ethereum is a platform for running dApps built on blockchain. Apps run on ETH network rather than one single system. This allows individuals to conduct business directly without central authority.

Smart contracts are if/then statements that ethereum executes
- Wallets hold private keys that allow access to resources on the blockchain
- Sign transactions with key (digital signature) and send to network
- DAPP runs client code which talks to blockchain instead of web server

While standard web applications like Twitter and Uber run on the computer systems of their organizations, which have full control over the apps, dApps run on an immutable blockchain and are inherently censorship-resistant.

<br>

NFTs are tokens that we can use to represent ownership of unique items. They let us tokenise things like art, collectibles, even real estate.

They are secured by the blockchain they are minted on---no one can modify the record of ownership or copy/paste a new NFT into existence.

