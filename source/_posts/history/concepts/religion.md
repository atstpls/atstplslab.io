---
title: religion
tags: [resiliency]
category: [ecosystem, concepts]
---
<br>



Religion is a mode of binding, an extension of the way we recognize that multiplicity joins into unity

A nation needs to know and participate that you're a nation and exist as one.  Washington DC monuments, participate, remember, parades, pledges, flags, songs, all help us know and celebrate the unity we participate in.  

Person, family, community, city, churches, clubs, sports teams, street gangs, all have this,   ritual of a church is the highest level because it tries to bind us to the highest possible thing.     

To bind yourself at the highest level, offers identity, morality, ways to come together in marriage and death, 

Flying spaghetti monster is impossible religion, it doesn't have the characteristics to bind people.  


<br>
<br>

The current scientific worldview doesn't match the biblical worldview.

If you're looking at world as heap of meaningless atoms and energy, how can you understand the Bible

##### Symbols 

A symbol is a collection of facts expressing a spiritual identity ( collection of letters forming a word )

Symbolism is humanity's most ancient and universal language... it's based on combination of cognitive processes:

Spiritual reality is united with corporeal reality to form symbols 


<br>

Moses parts the Red Sea... explain miracle with mechanical causality? 

Many physical marks ( matter ) are organized to express higher meaning ( by rules of alphabet, vocabulary, and grammar )

Many facts are organized to express higher language as meaningful symbols ( by rules of linguistics )

In other words, don't ask why there is a T in TABLE to understand what a table is... the physical structure of the word has nothing to do with the concept of a table 


Vehicle ( abstract principle )   and boat and chariot ( two instances of this idea )



Forms are the non-physical, timeless, absolute, and unchangeable essences of all things, of which Objects
and matter in physical space are merely imitations.

Problem of universals: Should the properties an object has in common with other objects 
such as color and shape, be considered to exist beyond those objects?

And if a property exists separately from objects, what is the nature of that existence?

Universals are qualities or relations found in two or more entities.

Circularity is a universal property of cup holders. 



Relationships between mind and matter, cause and effect, substance and attribute, and potentiality and actuality.

Four main branches of philosophy:

1. Metaphysics
2. Logic
3. Epistemology
4. Ethics 



<br>

##### Analogies 

Imagine you have 1 week to crack an encrypted file.

Brute force has a 100% success rate, but if long/complex password, could take years.

Other methods ( lists, rules, masks, hybrid ) could possibly crack it in less than a week.

Do you go with brute force or other methods?

Imagine you have 1 life to decide belief of God.

Scientific method has a 100% success rate, but if difficult/impossible to prove, could take centuries.

Other methods ( discourse, reasoning, art/religion/history analysis, personal experience ) could possibly enlighten you within your lifetime.

Do you go with scientific method or other methods?

<br>

Imagine you need to pick a trail guide to lead you out of a vast wilderness

Would you want one who is humble and will sacrifice making mistakes, looking dumb, listen to others in order to learn, get better, and reach destination

Or one who is always right, claims to be always right, thinks other opinions are stupid, has learned everything.

A leader needs a specific vision and strategy to be successful long-term 

That is truth, and it appears to scale infinitely upward


Faith is the kind of courage that allows you to welcome the possibilities of the future with open arms

<br>

















##### Mechanics

We contend with what could be, we put ourselves on the line, acting and being are intrinsically good

Vision should provide us with hope and security, and we should act in a way that makes that vision possible


## Symbolism

Symbolism is a language, the language of perception itself -- the context in which all language is embedded.

Up associated with goodness, purpose, light, air, cleanliness

Down associated with badness, confusion, darkness, weight, dirty 

Primal experiences undergird the symbolic categories of the Bible, through them we can better understand stories, and our world 

Bible writes thought and wrote with categories very different than our own which confuses our understanding 

But we project our own biases into the text  

Stories are told because they have meaning, maybe not happened exactly how they're told, images compressed into types, categorized, abstracted 

Thousands of years of people telling these stories, their legacy 


