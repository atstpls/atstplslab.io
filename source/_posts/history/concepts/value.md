---
title: value
date: 2023-01-18 05:00:42
tags: [tools]
category: [ecosystem, concepts]
---

<br>

<b>Value</b> is a <b>judgement</b> made about the <b>importance of goods</b> driven by their scarcity. It is subjective and depends on the time and place at which valuation happens.

Value cannot be measured and expressed objectively.  It must be valued ordinally, or in relation to one another. A is more valuable than B. B is more valuable than C.

<br>

##### Mechanics

We evaluate and interpret the world and select what we should value. Our world and human life is incredibly complicated, so we regularly reevaluate and reframe context to simplify.

Oil wasn't valuable at one time.  After the invention of the combustion engine, it gained enormous value.

<br>

##### Analysis

There are different things that people assign value--gold, land, buildings, corporate stocks, savings accounts, bonds, etc.

Commodities are objectively true, real.  Stocks? changing, corporate entities, they don't clear immediately, don't get weighed like commodoties

Relationships between the elements are steady and reliable, their abundance, their utility, their demand.

Things that can affect value:
     - taxes
     - zoning
     - laws
     - politics
     - technology

<br>

{% blockquote Saifedean Ammous, ' Principles of Economics' %}
It is not the inherent nature of goods that makes them valuable to us, but only our assessment of their suitability for meeting our needs. As their ability to satisfy our needs changes, so does their value to us. Value, then, is not a physical or chemical property of economic goods; it is a psychic property they attain only when humans assess them. In Menger's famous words, **"Value does not exist outside the consciousness of men."**
{% endblockquote %}



Technology is the driver of long-term economic growth.

Labor is expensive and at the cost of leisure.  More income grows, more able to afford leisure.

Captial is expensive and at the cost of  increasingly valuable consumption, which inevitably runs into diminishing returns without technical advancement.

Technological improvement creates new more intensive divisions of labor, increasing specialization and allowing for increased productivity.

Technological innovation is the driver of long-run growth

Physical capital is constantly decaying


Intellectual property, patent laws, world trade organization, limits sharing of knowledge.
  - China gets it anyway 
  - People copy it anyway 
  - Internet shares it anyway 

<br>

Russia got around sanctions, parallel trading to get iphones 

With patents and copyrights, the government assigns to the holder the right to control the property of others.


GOLD

<br>

##### Mechanics

- Rarer than all the other elements
- Will take more energy to attain it... natural scarcity
- Gold doesn't react with air
- Money is needed for cooperation.  Farmer produces food for _____. ____ produces for farmer.
- Money is a tool that allows farmer to produce a surplus and trade for other goods and builder to build surplus to trade for something else.
- Gold is money.  Laws of nature.  Civilizations have always used gold and silver as money.

<br>

https://www.hillsdale.edu/educational-outreach/free-market-forum/2012-archive/the-argument-for-returning-to-the-gold-standard-the-lessons-of-contemporary-history/

Gold, it's physical:
- It can be mined and dumped on the market, inflating  
- It can be stolen by randoms or regimes
- It can be kept from moving
- It can be taxed
- It can be sold in warrants, derivatives without being held hypothecation 
- It can be seized by government
- can be mined and dumped on the market, inflating
- can be stolen by randoms or siezed regimes/govts
- can be kept from moving from one place to another
- can be taxed
- can be sold in warrants, derivatives without being held ( hypothecation )
- can not easily be lent or rented
- can not easily verified
- can not be be used to generate yield in excess of maintenance
- can not easily be used to start a business, buy other assets
- final settlement would take weeks
- Gold supply increases 1-2% per year
- Gold controllers can print more notes than there are gold
- Bitcoin is a truly global asset. It's a store of value. Decentralization combined with cryptography makes it most secure network in world.
- can not increase in supply
- can not be kept from moving
- can not be seized (true custody when done right)
- is a universal market
- can be easily and instantly converted to other assets
- can serve as a long duration safehaven store of value
- can liquidate millions in any currency in seconds
- can economically empower everyone in the world
- final settlement takes minutes
- can mine using electricity from anywhere (isolated energy plant with satellite internet)
- actually incentivizes this... miners can't profit on normal electric grids



|ABILITY|USD|GOLD|BTC|
|-|-|-|-|
|<b>Ability for Conversion </b>| <b style="color: green">HIGH</b> | <b style="color: red">LOW</b> | <b style="color: green">HIGH</b> |
|<b>Speed of Settlement</b> | <b style="color: yellow">MED</b> | <b style="color: red">LOW</b> | <b style="color: green">HIGH</b> |
|<b>Resistance to Destruction</b>      | <b style="color: red">LOW</b> | <b style="color: green">HIGH</b> | <b style="color: green">HIGH</b> |
|<b>Ability to Save</b> | <b style="color: red">LOW</b> | <b style="color: green">HIGH</b> | <b style="color: green">HIGH</b> |
|<b>Resistance to Corruption</b> | <b style="color: red">LOW</b> | <b style="color: green">HIGH</b> | <b style="color: green">HIGH</b> |
|<b>Global Accessibility/Availability</b> | <b style="color: red">LOW</b> | <b style="color: yellow">MED</b> | <b style="color: green">HIGH</b> |
|<b>Independence from 3rd parties</b>| <b style="color: red">LOW</b> | <b style="color: yellow">MED</b> | <b style="color: green">HIGH</b> |
|<b>Resistance to Inflation</b>| <b style="color: red">LOW</b> | <b style="color: yellow">MED</b> | <b style="color: green">HIGH</b> |
|<b>Level of Transparency</b> | <b style="color: red">LOW</b> | <b style="color: red">LOW</b> | <b style="color: green">HIGH</b> |
|<b>Resistance to Seizure/Theft</b>| <b style="color: red">LOW</b></b> | <b style="color: red">LOW</b> | <b style="color: green">HIGH</b> |
|<b>Resistance to Government Control</b> | <b style="color: red">LOW</b> | <b style="color: red">LOW</b> | <b style="color: green">HIGH</b> |

<br>

##### Ability for Conversion

##### Speed of Settlement

##### Resistance to Destruction

##### Ability to Save

##### Resistance to Corruption

##### Global Accessibility/Availablility

##### Independence from 3rd Parties

##### Resistance to Inflation

##### Level of Transparency

##### Resistance to Seizure/Theft

##### Resistance to Government Control

People have been confiscating gold for centuries

Gold's physiality makes it subject to government control

Moving it around was difficult, so centralized made sense, but made confiscation easier 

Cryptographic security makes cost of defending property and information far <b style="color: red">LOW</b>er than cost of attacking

Individual soveriegnty - send large amounts of money whenever to whoever anywhere in the word without asking anyone's permission

Cannot be inflated, so good store of value 

Separated from country's economy. Neutral btc or usd which is subject to economic performance like global reserve currency

no counterparty risk and no reliance on third party

gold counterparty risk is <b style="color: red">LOW</b> but ease of transfer 

btc counterparty risk <b style="color: red">LOW</b> based on cryptographic security, verifiable record and no third parties

money is an extension of the natural standard which should reflect ecological accountability

service economy artificially ignores accountability for a time, which become parasitic

Energy sources are food, fuel and elemental substances.

Nature is a world of conservation and use of energy and entropy

Modern economic theory doesn't abide by laws of natural world

Real economy is dependent on natural standards of measure and rewards 

chain of energy dependencies... real economy uses sources to generate energy and service economy consumes them


Physical integrity linking system to physics of the real world is best. All govts, corps, systems run by mankind will eventually be corrupted

It's difficult to maintain integrity, absolute power corrupts

Nature self-corrects, cleanses irrationality, sloth. See no fat, dumb birds because they don't last

Strongest asset which is dollar is using its purchasing power at a rate of 15% a year


Storing value is essential for human civilization. We need a way to transfer value into the future to reduce uncertainty.

|||Risks|
|-|-|-|
|Banks|Allow us to make valid exchanges:<br> - without having physical possession of currency<br> - without being in the same physical location as the payee<br> - without knowing/trusting the payee<br> - safekeep our money to reduce the risk of theft, loss, destruction, etc.<br>||
|Stocks|Corporations issue stocks (shares) to raise funds to operate their businesses<br>The shareholder buys a piece of the corporation or a claim to part of its assets and earnings<br>Shareholders do not own company assets and usually receive nothing after a bankruptcy<br>Shareholders get voting rights and sometimes dividends<br></br>||
|Bonds|Debt instruments used by govt/corps to raise capital<br> - Govt/corp issues bond, lender lends money, borrower repays full amount plus interest<br> - A bond with 1K face value, 4% coupon, 20 years maturity:<br> - Borrower pays lender 40 a year for 20 years, then full 1,000 at 20 year mark<br>Coupon rate can be variable and raise with interest rates<br>With stock you own equity, with bonds you only get the principal and interest<br></br>| - credit/default: The creditworthiness of the borrower<br> - interest rate: As interest rate rises, existing bond prices fall (because of their now lower interest rate)<br> - inflation: As money loses value, so does principal and interest<br> - Zombie companies pay more in debt servicing costs than they make in profits<br>  - When interest rates rise, some will default<br> |
|Gold|virtually impossible to destroy and impossible to create from other materials<br>can be mined and dumped on the market, inflating<br>can be stolen by randoms or siezed regimes/govts<br>can be kept from moving from one place to another<br>can be taxed<br>can be sold in warrants, derivatives without being held ( hypothecation )<br>can not easily be lent or rented<br>can not easily verified<br>can not be be used to generate yield in excess of maintenance<br>can not easily be used to start a business, buy other assets<br>final settlement would take weeks<br>|Gold supply increases 1-2% per year<br>Gold controllers can print more notes than there are gold<br>|
|Bitcoin|A global asset and store of value<br>Decentralization combined with cryptography makes it most secure network in world<br>can not increase in supply<br>can not be kept from moving<br>can not be seized (true custody when done right)<br>is a universal market<br>can be easily and instantly converted to other assets<br>can serve as a long duration safehaven store of value<br>can liquidate millions in any currency in seconds<br>can economically empower everyone in the world<br>final settlement takes minutes<br>can mine using electricity from anywhere (isolated energy plant with satellite internet)<br>actually incentivizes this... miners can't profit on normal electric grids<br>|


