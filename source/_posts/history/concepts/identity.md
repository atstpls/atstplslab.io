---
title: identity
date: 2023-01-14 16:00:42
tags: [identity]
category: [ecosystem, concepts]
---
<br>

# one

## two

### three

#### four

##### five 

###### six 


Identity is the emergent, unconscious, automatic consequence of the co-operative/competitive generation, sequencing and rank-ordering of MAP schemas.  This organization manifests itself intrapsychically and socially as the dominance hierarchy.

Identity is the proper union of faith and responsibility.  It unites them in a subsidiary ( each note serves the phrase, each phrase serves the melody ) and hierarchical manner.

Jacob's Ladder - finite to infinite, Earth to Heaven, multi-dimensional harmony, responsibility, and beauty

Vision is approximal beginnings of what the spirit of goodness would look like

Identity is all of that stretching from lowest to highest at once.

Mental health is not subjective. It's harmony exists when entire subsidiary functions are performing as they should be.  

Meaning is not found in self-serving goals and drives, doesn't fulfill social obligations, doesn't integrate with future you and others

Meaning is found in the eternal dance between chaos and order, in positive attitude in the midst of your stupidity and suffering, in service to others, which is in service to the harmony that makes up Jacob's Ladder

We're not built to pursue our base needs, 

We each are <b>unique individuals</b> with a wide array of traits and talents. We each have special abilities which we can use to solve different kinds of problems. Humans cooperate in societies by solving many kinds of problems together.

Identities are socially negotiated, formed.

<br>

##### Mechanics

Our differences determine our memberships in countless groups (ethnicity, race, sex). Viewing people using group identity before individual identity is known as identity politics which emerged in France in 1970s, has a Marxist basis, manifests itself in post-modernism. 

There are two primary narratives about how the world is constructed and how we see ourselves, our history, and our social being:

<br>

|Narrative|Description|
|-|-|
|<b>Collective Tribal View</b>|Tribal allegiances are paramount. You belong to many different "tribes": your gender, your sex, your race, your ethnicity, etc.|
|<b>Sovereign Individual View</b>|The individual is of intrinsic value. Although you are a member of many tribes, you are regarded as a sovereign individual, and you are to treat other people that way as well, in regards to their rights and responsibilities.|

<br>

Collective tribal outlook degenerates into group guilt and a tribal mentality.

Sovereign individual outlook produces functional, peaceful, and successful societies that generate wealth as well as inequality.

<br>

##### Analysis

Celebration of denial of reality, celebration of confusion, LGBTQ

- People send their kids to school to teach them fundamental skills needed for life, not to be indoctrinated into the particular value system of their leftist teachers.
- woke | disorder in which narcissistic emotional fragility with a pathological urge to virtue signal attempts to masquerade as compassion
- Mainstream institutions - Psychologist, medical, mental health, schools, hospitals have lost our trust 
- We need stable identities, to navigate natural and social environments
- "You are what you feel"  wrong and unethical, you must rely on multiple converging sources of evidence to make a diagnosis also impractical, you need to be nested socially at multiple levels of the social hierarchy in order to have an identity to replace that with something that's questioning



ROGD - Rapid Onset Gender Disphoria  - psychogenic epidemic, a Social contagion fueled by social media, lisa littman  

      - causes trauma to kids and parents, parents blamed for child's mental health for not affirming right away 
      - finland, switzerland, england did 180
      - CONTRADICTION:  long term studies show early treatment is best for kids 
      -                 if you don't start early, likely result is suicide
      -                 They can't both be true


They pressure, they claim consensus, they fail us 



Visionary

- Use vision to move through life in the world, pathways, tools, obstacles
- Derive what is by observing with your vision 
- We ignore things that are changing or normal, we are drawn to abnormal, changing
- We can imagine our future selves acting in the future and evaluate it
- generate variant selves, play it out internally, and pick best one 
- seek and you shall find, ask and you shall receive,     except white privilege
- incoherence causes anxiety which causes hopelessness, negative emotion 
- is there a mode of vision that enables resiliency in the face of ultimate catastrophe, atrocity, suffering, and death

1. we know face the possibility of the catastrophy, expose yourself 

stations of the cross - opportunities to expose yourself, to prepare

i you cannot envision yourself prevailing against the mob, you will be a victim of the mob 

voluntary exposure to challenge is curative

the better you can face catastrophe of life, the better you will be 

identity crisis

gender idealogues - can't be precise, can't test/measure, something you feel, OK 

   - i can be anything i want from moment to moment / that doesn't integrate well with others

   - most are going through mental problems, no friends, because their way or highway, 

   - affirming care pushes away from reciprocity   

construction
- feelings lie, they aren't an accurate guide, aneorexic women feel like they're fat 
- you need construct validation to find truth, not subjective thoughts 
- the idea that identity is subjectively defined and can change on a whim is preposterous 
- they demand treatment based solely on their narrow whim based on feelings 
- opposite of reciprocity 
- don't know how to play, can't negotiate an identity that's desirable to others, so they become sad, miserable, alienated, unpopular
- get isolated, not fun, too insistent their subjective state dominate the social space 
- i feel this way, you better treat me this way or else,  juvenille dilusion 
- narcissistic, virtue-signalling parents, didn't help guide to establish a healthy identity
- identity is socially negotiated constantly... offer thoughts, engage in honest dialogue, you keep what works and drop what doesn't
- I in the moment? (2 yo/emotionally-driven) or I in the future (next week, year)
- won't participate in normal institutions, you abandon that responsibility, tyrants scoop it up 
- you want to be of service to others, need to get your life together
- you can share thoughts, play, foster development, improve together, work to face catastrophe together
- educate yourself to the limits of your intelligence. books, podcasts, projects, creativity, 
- ritualization of time and space, get what iterates right, things that stabilize you 
- deception and manipulation are not effective long-term strategies


identity is socially negotiated constantly
To be of service to others you need to get our life together 


<br>

Custom Pronouns
- make conversation more difficult
- allow those who demand the use of them to manipulate others more easily
- prompt conversations about oppression, discrimination, and systemic hate
- promotes division, confusion, and acceptance of the artificial



<br>

Subjective identity is the highest form of narcisism. 

"I am who I say I am, and if you don't accept that you're evil."

<br>

##### Mechanics

Aligns with 2-4 year old development stage when toddlers learn to play with others, reciprocate, and negotiate their identity.

Tantrums when toddlers are told no, emotional outrage when trans is not accepted or their artificial world is disturbed.

Until 2 years old, infants are very vulnerable and require care that is 100% unquestioned compassion.

Trans also seek and require this type of care as they are stuck in development.

Thinking about yourself directly aligns with being miserable.

##### Analysis

- To say biological sex is redundant.  It's the only type of sex that exists.
- Social emotional learning, started with Alice, Annie Besent, Mao, Gramshi
- Comprehensive Sexual Education - Groomer school, trying to apply queer theory to developmental psychology, so they can say sex can be age appropriate for 5 year olds
- They do not respect norms, they violate every norm they can
- Every person is accepted as a sexual being, age and stage appropriate, not just about pregnancy and medical health
- Using dog whistles and code language
- Really about teaching kids about their sexual and reproductive rights, sexual citizenship, agency to dispel myths, access to resources and services, 
- Sex is hundreds of millions of years old, those unable to make distinction between two don't propagate
- Sex is the most basic distinction of anything real in the history of the world
- Older than nervous system, older than up and down
- Our identity crisis is caused by gender idealogues who can't be precise, can't test/measure, say that it's something you feel 
- The belief "I can be anything i want from moment to moment" doesn't integrate well with others
- Most are going through mental problems, have little/no friends, because it's their way or the highway
- Affirming care pushes away from reciprocity
- Feelings lie, they aren't an accurate guide, aneorexic women feel like they're fat
- We need construct validation to find the truth, not subjective thought
- The idea that identity is subjectively defined and can change on a whim is preposterous
- They demand treatment based solely on their narrow whim based on feelings, which is the opposite of reciprocity
- They don't know how to play, can't negotiate an identity that's desirable to others, so they become sad, miserable, alienated, unpopular
- They get isolated, not fun, too insistent their subjective state dominate the social space
- I feel this way, you better treat me this way or else,  juvenille dilusion
- Narcissistic, virtue-signalling parents, didn't help guide to establish a healthy identity
- Identity is socially negotiated constantly... offer thoughts, engage in honest dialogue, you keep what works and drop what doesn't
- I in the moment? (2 yo/emotionally-driven) or I in the future (next week, year)
- Won't participate in normal institutions, you abandon that responsibility, tyrants scoop it up 



Seratonin makes one less aggressive (because they're less irritable) and more dominant. Seratonin makes a lobster who has lost a battle more likely to fight again. There is a realm of values (humanities) and a realm of facts (science is king). Humanities are visual arts, performing arts, literary arts, philosophy

Children with anxiety, depression, autism, ADHD, who have experienced trauma, or who may be gay can actually do much worse when they attempt social transition. Reinforcing the delusion can distract from the underlying cause of dysphoria, and delays the efforts needed to address to diagnose and treat those issues. Social transition can isolate the child further from peers and hobble the opportunity to develop healthy friendships or romantic relationships. Balancing these concerns is what the family, along with a psychologist, is best qualified to do. No school representative could ever have the breadth of knowledge needed to make that decision for a child.



You can't jump from good stories to a thriving narrative identity. 

An identity must be socially negotiated. You think/speak/act and depending how others respond, you make adjustments. 

If you can't negotiate an identity that's desirable to others, you don't get accepted, you become isolated/sad/bitter.

And you're never in a position where you can serve others---which is the fundamental calling for every hero in every story.


 
Dr. Miriam Grossman 

- Kids isolated, surfing internet, withdrawn, claimed they're boys demanded testosterone 
- Think they're in the wrong body, and can't be happy without pharmaceuticals and surgeries 
- American Academy of Pediatrics, American Academy of Child and Adolescent Psychiatry that delusion is normal 
- reocmmend treatment that will disfigure and sterilize their body 
- Sex is established at moment of conception, sex is permanent, 
- Profession has been hijacked by a political crusade, medical associations are mouthpieces, 
- if she feels she's a boy, she's a boy.  Horomones and surgery are the only solutions to gender dysphoria. It's a right.
- Gender dysphoria increased 5000%  
- 30 years ago, 55 subjects, primary requirement was severe gender dysphoria since early childhood 
- no control group, never repeated, some subjects had modest improvement after 18 months 
- Dutch protocol flawed and unreliable, never intended to help all individuals, but it became the basis for gender medicine aroudnt eh world 
- manmade catastrophe, vulnerable kids are exploited for money and furthering ideology, 
- NHS Great Britain, Hillary Cass, analyzed data from largest clinic (Cass Report), demonstrates there's no evidence medical interventions benefit children 
- Focusing on minds instead of bodies 
- Sex reassignment in minors is the greatest medical scandal in human history 
- Sacrificing children for perverse political agenda 

<br>

Qatar - Sponsor of Sunni jihadism aaand principal benefactor of Hamas 
      - bought 3.1 bn weapons from Britain 

Saudi Arabia - bloody war in Yemen 1.9 bn 
Turkey - crushed Kurds 799m 
UAE dictatorship - 416m 
Egyptian police state - 318m Islamism and corruption 

Israel - Middle East's sole democracy and only power to respect rights of women and minorities 
       - locked in existential struggle against jihadism, 83m 



Pandemic pushed 500 million people into poverty: WHO 

Most unpopular VP in history to shining star in 2 weeks 

Russians tried to settle, Boeing, Raytheon, GD, LM, wanted to add countries to NATO 

Then country has to conform its military purchases, they get more sales 

March 22, 113 bn , then 24 bn, then 60 bn... then we'll need to rebuild everything that was destroyed 

Mitch McConnell said don't worry it's not really going to Ukraine, it's going to American Defense Manufacturers (money laudering)

Blackrock owns them.  Tim Scott said don't worry it's not a gift, it's a loan.  (impose loan conditions)

1. extreme austerity program - if you're poor, you'll be poor forever 
2. Ukraine has to put up all its assets, agricultural land, richest farmlands, for sale to multinational corporations. Dupont, Cargill, Monsanto have bought 30% of Ukraine's.  500K Ukrainians have died to keep that belonging to Ukraine.

December, Biden announced contract to rebuild Ukraine went to Blackrock.

They want to suppress free speech and control political discourse 

Want socialism but don't want to redistribute their wealth 

They blame others for misinformation while enabling it and mandating $

They blame others for weaponizing DoJ while doing it themselves 

Blind allegiance to an ideology 

Fauci - "When people are vaccinated they're not going to get infected"
Biden - "You're not going to get COVID if you have these vaccinations"
Walensky (CDC Director) - "Vaccinated people do not carry the virus and don't get sick"
Maddow (MSNBC) - "Now we know the vaccines work well enough that the virus stops"
Bill Gates - "Everyone who takes the vaccine is reducing their transmission"
Albert Bourla (Pfizer CEO) - "There is no variant that escapes the protection of our vaccines"

NEW CATEGORY - THOSE THAT THEY HAVE ADMITTED WERE FALSE 


The worst people claim very loudly to be acting on behalf of the highest cause.  That's how they camouflage their predation and parasitism.
The eternal Pharises, dark tetrad types 

You support women's rights and Hamas/Taliban/Jihadists/Shia law 

All this corruption, no one is going to prison:  FBI, USSS, etc...

Protests paid for by Soros, 

They lie over and over and over (Mayorkas, FBI, etc., etc. )

supports decriminalizes illegal immigration, tax-payer funded insurance to illegal aliens, taking away health insurance on the job for americans, banning gas cars, confiscating firearms  

Religious freedom for everyone except ourselves 
Praise every tradition except for our own 
Promote every belief system except for the one that got us here 

Trump goals 

1. Stop migrant invasion, deport illegal immigrants
2. End inflation, keep USD world reserve currency
3. Make U.S. dominant energy producer and manufacturing superpower 
4. Tax cuts for workers, no tax on tips, 
5. Defend Constitution, Bill of Rights 
6. Restore peace in Europe/MiddleEast, build iron dome missile defense shield 
7. End weaponization of government 
8. Build strongest military in the world 
9. Cancel EV mandate, CRT, Gender ideology, Men in Women sports, 
10. Deport Pro-Hamas radicals, make college campuses safe  
11. Secure elections, same-day voting, voter ID, paper ballots, proof of citizenship 

[Pfizer documents](https://phmpt.org/pfizer-court-documents/) with thousands of side effects that they tried to have sealed and hidden for 75 years 

Feeling sorry for someone doesn't make you a good person 
Diligent upward-oriented work makes you a good person

Biden senator for 36 years, VP for 8 years, president for 4 years... blames Trump for America's problems 


Aleksandr Solzhenitsyn, commencement speech 1978

Truth eludes us as we lose focus, and it is seldom sweet 

Deep splits bear the danger of disaster for all of us, a kingdom divided against itself cannot stand 

Cultures constitute self-contained worlds, we mistakenly measure other countries with a Western yardstick 

West bases state policy on weakness and cowardice instead of strength and courage 

Some outbursts of "boldness and inflexibility" when the target is soft or unsupported 

But fail to rise to the occasion when dealing "with powerful governments and threatening forces, with aggressors and international terrorists"

West claims governments are meant to serve man and that man lives in order to be free and pursue happiness 

This has permitted the welfare state. More things, a better life become the focus, not sacrifice for the common good 

Biology tells us a high degree of habitual well-being is not advantageous to living organisms 

Law has become viewed as the ultimate solution, "if one is right from a legal point of view, nothing more is required"

Voluntary self-restraint is almost unheard of. Everybody strives toward further expansion to the extreme limit of legal frames.

"A society without any objective legal scale is a terrible one indeed. But a society with no other scale but the legal one is also less than worthy of man."

"A society based on the letter of the law and never reaching any higher fails to take advantage of the full range of human possibilities."

"Whenever the tissue of life is woven of legalistic relationships, this creates an atmosphere of spiritual mediocrity that paralyzes man's noblest impulses."

Tilt of freedom toward evil stems from man not bearing any evil within himself, and all the defects of life are caused by misguided social systems, which must therefore be corrected. 

"Hastiness and superficiality--these are the psychic diseases of the twentieth century and more than anywhere else this is manifested in the press. n-depth analysis of a problem is anathema to the press; it is contrary to its nature. The press merely picks out sensational formulas."



" An example is the selfdeluding interpretation of the state of affairs in the contemporary world that functions as a sort of a petrified armor around people’s minds, to such a degree that human voices from seventeen countries of Eastern Europe and Eastern Asia cannot pierce it. It will be broken only by the inexorable crowbar of events."

" historical analysis demonstrating that socialism of any type and shade leads to a total destruction of the human spirit and to a leveling of mankind into death. Shafarevich’s book was published in France almost two years ago and so far no one has been found to refute it."

"The complex and deadly crush of life has produced stronger, deeper, and more interesting personalities than those generated by standardized Western well-being."


"However, the most cruel mistake occurred with the failure to understand the Vietnam war. Some people sincerely wanted all wars to stop just as soon as possible; others believed that the way should be left open for national, or Communist, self-determination in Vietnam (or in Cambodia, as we see today with particular clarity). But in fact, members of the US antiwar movement became accomplices in the betrayal of Far Eastern nations, in the genocide and the suffering today imposed on thirty million people there. Do these convinced pacifists now hear the moans coming from there? Do they understand their responsibility today? Or do they prefer not to hear?"


"To defend oneself, one must also be ready to die; there is little such readiness in a society raised in the cult of material well-being. Nothing is left, in this case, but concessions, attempts to gain time, and betrayal."

"The turn introduced by the Renaissance was probably inevitable historically: The Middle Ages had come to a natural end by exhaustion, having become an intolerable despotic repression of man’s physical nature in favor of the spiritual one. But then we recoiled from the spirit and embraced all that is material, excessively and incommensurately. The humanistic way of thinking, which had proclaimed itself our guide, did not admit the existence of intrinsic evil in man, nor did it see any task higher than the attainment of happiness on earth. It started modern Western civilization on the dangerous trend of worshiping man and his material needs. Everything beyond physical well-being and the accumulation of material goods, all other human requirements and characteristics of a subtler and higher nature, were left outside the area of attention of state and social systems, as if human life did not have any higher meaning. Thus gaps were left open for evil, and its drafts blow freely today. Mere freedom per se does not in the least solve all the problems of human life and even adds a number of new ones."

"And yet in early democracies, as in American democracy at the time of its birth, all individual human rights were granted on the ground that man is God’s creature. That is, freedom was given to the individual conditionally, in the assumption of his constant religious responsibility. Such was the heritage of the preceding one thousand years. Two hundred or even fifty years ago, it would have seemed quite impossible, in America, that an individual be granted boundless freedom with no purpose, simply for the satisfaction of his whims. Subsequently, however, all such limitations were eroded everywhere in the West; a total emancipation occurred from the moral heritage of Christian centuries with their great reserves of mercy and sacrifice. State systems were becoming ever more materialistic. The West has finally achieved the rights of man, and even to excess, but man’s sense of responsibility to God and society has grown dimmer and dimmer. In the past decades, the legalistic selfishness of the Western approach to the world has reached its peak and the world has found itself in a harsh spiritual crisis and a political impasse. All the celebrated technological achievements of progress, including the conquest of outer space, do not redeem the twentieth century’s moral poverty, which no one could have imagined even as late as the nineteenth century.

"

" Liberalism was inevitably pushed aside by radicalism, radicalism had to surrender to socialism, and socialism could not stand up to communism. The Communist regime in the East could endure and grow due to the enthusiastic support from an enormous number of Western intellectuals who (feeling the kinship!) refused to see communism’s crimes, and when they no longer could do so, they tried to justify these crimes. The problem persists: In our Eastern countries, communism has suffered a complete ideological defeat; it is zero and less than zero. And yet Western intellectuals still look at it with considerable interest and empathy, and this is precisely what makes it so immensely difficult for the West to withstand the East."

"I am referring to the calamity of an autonomous, irreligious humanistic consciousness.

It has made man the measure of all things on earth—imperfect man, who is never free of pride, self-interest, envy, vanity, and dozens of other defects. We are now paying for the mistakes which were not properly appraised at the beginning of the journey. On the way from the Renaissance to our days we have enriched our experience, but we have lost the concept of a Supreme Complete Entity which used to restrain our passions and our irresponsibility. We have placed too much hope in politics and social reforms, only to find out that we were being deprived of our most precious possession: our spiritual life. It is trampled by the party mob in the East, by the commercial one in the West. This is the essence of the crisis: The split in the world is less terrifying than the similarity of the disease afflicting its main sections."


------------

West has been gradually losing the will and intellectual ability to defend itself, not so much against foreign armies as it may have appeared in 1978, but against an army of internal critics determined to demolish everything the West used to stand for



--------------

Douglas Murray War on the West 

RACE 

Lived experience used to contradict reality... Conveniently, it can't be fully understood, is "my truth" 

2001, CRT admits it uses economics, history, context group and self interest, feelings and the unconscious 

It questions equality theory, legal reasoning, enlightenment rationalism, and neutral principles of constitutional law 

It has an activist dimension, It tries not to understand it, but to transform it 

------------------------



-----------------------

parched with the thirst of attention 

to fuel the fire of your self obsession 

we'll watch you crumble 



bow to the stronger element 

the truth will outweigh you 

------------------------------------------

July 24th 

Hawley in Senate Homeland Security Committee 

Whistleblowers 

Almost everything we know is because of whistleblowers 

- Law enforcement were assigned to the roof of American Glass Research building 6 
- Law enforcement were assigned to patrol perimiter of building 6, they abandoned it and went inside 
- USSS director lied and said no one was assigned there 




Psychopaths are prisoners of their momentary whims 

They betray themselves, are motivated by power, very manipulative, in it for the gratification of their own whims 

Do poorly over any reasonable period of time , completely incapable of learning from experience, constantly betray themselves 

Things that are escapist or pleasurable in the moment all it does is drive you downhill across time 


Treating yourself as a community across time, giving, reciprocating, be useful and generous to other people 

Self sacrificing in a reciprocal manner, and generous, others want to interact with you , you are the beneficiary of their generosity 

It's a better approach than trying to maximize your momentary whim 


------------------------




## Konstantin Kisin 

### Instituional Corruption 

It's possible we have the least corrupt institutions ever and our visibility has increased (NYT once had complete control over narrative)

Fragmentation of reality has reached unprecedented levels 

Names don't mean the same thing to people.  Trump, Sam Harris, are different things to different people.

Wokification - glorification of victimhood, hypersensitivity, attempting to restrict speech of those who disagree 

Outrage and the hottest take 



#### Tucker Carlson 

Trip to russia claiming groceries are cheaper, pandering, playing to his audience, 











Reductionist Thinking 

***********

People fail to price in uncertainty for everything they see/hear.. what happened before/after/context   innocent walker or just planted IED 




What agent doesn't have agency? 

If an agent has agency, when would it not have an arena?

What is the purpose of three separate aspects that require each other and only ever exist simultaneously?




Democrat Hack Judge Juan Merchan, daughter made millions off the case 

Will Scharf Trump attorney 

1 of 2 cases brought by E. Jean Carroll 

story is implausible, there is no corraboration for any claims, no police report, no corraborating witnesses, no dna ,

Didn't know when, no surveillance or witnesses confirming anything about her story 

Her attorneys introduced propensity witnesses that shouldn't have never been allowed 

Jessica Leeds claims in 1979 Trump assaulted her, no corraboration 

Natasha Stoyan  same deal 

Judge that allowed this in...  


Trump prevented from cross-examining them on crucial issues, political connections, motivations, 

Coordinated by George Conway, funded by Reed Hoffman (Biden/Harris),  

limited in evidence able to prevent 

Weaponization of legal system and courts to unlawfully/unconstitionally prevent him from running for President 

Nicholas Biase, Chief, Public Affairs, DOJ - SDNY 



You never intended to answer the question

Only to contribute under false pretense 

And get a pat on the back from yourself 

If only you knew what you looked like 




Yeah sure man, you can embrace victimhood

Sit back and watch how far that will take you 

Nowhere



Lawfare, banana republic under military rule

weaponizing the law against your enemies 

Charges against trump are miscategorization of business expenses 

statute of limitations, making things of felony without evidence of such 

selective prosecution, Soros-backed prosecutors pick cases where there isn't organized opposition 

That's what happened in San Francisco, Kamala was DA and used selective prosecution to ruin SF 

Advocated for George Castone by not prosecuting criminals and going after business owners 

Weaponizing justice--hillary bleachbit, hunter biden laptop, headed towards one party state like China, North Korea 

Lawfare: Alvin Bragg runs on the explicit campaign to take down Trump, hunting for any charges in the most favorable judges/jury/part of country 

Control evidence, control narrative 




Iran negotiations 

Diplomats make concessions as if the deal is an achievement. 

The deal (inadequate verification) is with untrusted (four decade proxy war) and so they don't adhere to it. 

Worse than the agreement was the sanctions relief that went along with
$100B dollars to Iranians which went into Islamic Revolutionary Guards Ward ? 
Hamas, Hezbollah, proxy army in Syria, hastashabi militias in Iraq, Houthis, 
They went back to same playbook, with same people 
- undesignated the Houthis 
- didn't enforce Trump sanctions ($80B into coffers)
- 6B in gutter to be transferred to Iran on eve of Oct 7th 



































-----------------------



-----------------------

On Sept 3, arraignment for chinese spy Linda Sun who worked for Andrew Cuomo
On Sept 4, Merrick Garland 





-----------------
protect and preserve civilization 

"There are no solutions, only trade-offs"  Thomas Sowell 

These problems are internal.  All we can do is tinker at the edges and change aspects at the expense of others 

Free speech comes at a cost 

Lockdowns come at a cost 



What are Western Values 

- The sanctity of life, of the individual, universal human rights 
- freedom of expression, free market, right to start a business, have it thrive 
- power should be diffused down, alpha chimps are smaller, not as aggressive, rules by building coalitions of mutual reciprocation  
- while others like CHina, Russia power accumulates at the top 


They don't necessarily work well in other parts of the world, exporting democracy 

- Dr. John Littel kicked out of Sarasota Memorial Hospital Board Meeting after testifying to the effectiveness of Ivermectin to treat COVID-19 


Poor people can't care about long-term sustainability and iterability, they're trying to get food, clean water, access to basic hygiene facilities 

They sacrifice the future for the present so they can survive 

The more their conditions improve, the more they think about and contribute towards the future 






