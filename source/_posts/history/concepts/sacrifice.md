---
title: sacrifice
date: 2023-01-14 14:00:42
tags: [debate]
category: [reference]
---


###### Sacrifice

I can have what I want if I'm willing to make the proper sacrifices.

Develop a vision. You must make it real.

If you don't aim at it, you're not going to hit it.

<br>

##### Prayer

Asking means I am willing to give up everything that I'm doing wrong so I can put things right if I could know what right was.

If you knock and you wanted to walk through, wanted to learn, and sought because you wanted to find, maybe you would receive and find.  But not without aim.

You have to know what it is to work towards it, to recognize it.

<br>


