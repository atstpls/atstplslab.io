---
title: unity
date: 2023-01-16 19:30:42
layout: post
tags: [money, government, currency]
category: [ecosystem, concepts]
---

<br>





There is moral confusion. People are unmoored from objective moral values.

Incoherence in micro-narratives and macro-narratives ( me too is good, but Taliban is ok )

Action requires coherence and unification... there is a hierarchy of uniting structure

"There are no universal truths, but tolerance is better than intolerance" - Postmodern woke

Systems of ideas work together to attract and facilitate evil


Unity—purposeful essence—
and multiplicity define each other. The parts of wholes are building blocks for what they participate in
at a higher level, while simultaneously possessing a reality, specific to the level of the part, defined in
the same way. 

We are all bound into friendships, families, clubs, churches, sports teams, projects, and companies while we
participate, simultaneously, in cities, states, and countries. We encounter united multiplicities at every
recognisable level of being, all constituting their own united multiplicities.

Unity or identity is not something that happens accidentally within this dynamism of relationship
between parts and wholes. It is not a bureaucratic affair, mandated arbitrarily as power from the top
down, by fiat, by simple declaration of rules and laws. Nor does it come about by establishing arbitrary
borders, or by the mere fact of shared space. Two people in the same territory can be seeing to their
private and independent affairs and ignoring each other—even fighting an all-out war against each
other. The joint habitation of a given space is not sufficient to confer identity.

Unity comes from sharing a common point of attention, establishing common purpose or goals, sharing
a common origin, or embodying a common story. These are vectors of identity. To participate in unity
is to sacrifice some aspect of multiplicity to its purpose. It is an exchange of direct, deliberate will and
attention between the individual and any collective. 